package com.app.websterz.warfarechat.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import de.hdodenhof.circleimageview.CircleImageView;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.NewChatModelClass;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.nkzawa.emitter.Emitter;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.forgotPassword.ForgotPassword;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import static android.content.ContentValues.TAG;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Iterator;

import static com.app.websterz.warfarechat.homeScreen.activities.HomeScreen.is_user_connected;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class Login extends AppCompatActivity implements View.OnClickListener {
    Button loginBtn;
    EditText loginUsernameField, loginPasswordField;

    MySingleTon mySingleTon;
 //   CustomDialogError customDialogError;
    ImageView loginUsernameIv, loginPasswordIv, PasswrdVisibility;
    Animation animShake;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String Tag = "Token: ", token;
    NewChatModelClass newChatModelClass;
    CheckBox RemeberMeChkBox;
    String RememberMePreferenceString;
    int PsdVisibiliyy = 0;
    TextView loginForgotPasswordBtn;
    LinearLayout signUpGoogleLogin,faceBookLoginLayout;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 10;
    private RelativeLayout mainLayout, bluringBackgroundRelativeLayout;
    String userLoginType;
    LoginButton login_button;
    CallbackManager callbackManager ;
    LinearLayout loader_imageview_Layout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialization();

        if (mSocket != null)
            is_user_connected = 0;
        RememberMePreferenceString = GettingRememberMePreferences();
        initView();
        faceBookLoginCode();

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w(TAG, "getInstanceId failed", task.getException());
                    return;
                }
                // Get new Instance ID token
                token = task.getResult().getToken();
                SavingTokenValue(token);
                Log.e("TokenValue", token);

            }
        });

        PasswrdVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordVisibility();
            }
        });
        signUpGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginFromGoogle();
            }
        });

        faceBookLoginLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_button.performClick();
            }
        });
    }

    private void initialization() {
        signUpGoogleLogin = findViewById(R.id.signUpGoogleLogin);
        faceBookLoginLayout = findViewById(R.id.faceBookLoginLayout);
        bluringBackgroundRelativeLayout = findViewById(R.id.bluringBackgroundRelativeLayout);
        login_button=findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
    }

    private void initView() {
        mainLayout = findViewById(R.id.mainLayout);
        editor = getSharedPreferences("myPrefs", 0).edit();
        animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        loginUsernameIv = findViewById(R.id.loginUsernameIv);
        loginPasswordIv = findViewById(R.id.loginPasswordIv);
        PasswrdVisibility = findViewById(R.id.PasswrdVisibility);
        //customDialogError = new CustomDialogError();

        mySingleTon = MySingleTon.getInstance();
        loginUsernameField = findViewById(R.id.loginUsernameField);
        loginPasswordField = findViewById(R.id.loginPasswordField);
        loginForgotPasswordBtn = findViewById(R.id.loginForgotPasswordBtn);
        loginForgotPasswordBtn.setOnClickListener(this);
        loginBtn = findViewById(R.id.loginBtn);
        RemeberMeChkBox = findViewById(R.id.RemeberMeChkBox);
        WritingIDPasswrdFromPreference();
        loginBtn.setOnClickListener(this);


    }

    private void PasswordVisibility() {
        if (PsdVisibiliyy == 0) {
            PasswrdVisibility.setBackgroundResource(R.drawable.paswrd_visibility_off);
            loginPasswordField.setTransformationMethod(null);
            PsdVisibiliyy = 1;
        } else {
            PasswrdVisibility.setBackgroundResource(R.drawable.paswrd_visibility);
            loginPasswordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
            PsdVisibiliyy = 0;
        }
    }

    private void loginFromGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(Login.this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            if (mSocket.connected()) {
                Login.this.stopService(new Intent(Login.this, Foreground_service.class));
                showingLoader();
                DeletingOldPreferencesForMineUserNameAndMinePaswrd();
                DeletingOldPreferencesForsavingEmailAndId();
                savingEmailAndId(account.getEmail(),account.getId());
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("email", account.getEmail());
                    jsonObject.put("social_id", account.getId());
                    jsonObject.put("login_type", "google");
                    jsonObject.put("token", token);
                    jsonObject.put("agent", "ANDROID1.0");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                loginBtn.setEnabled(false);
                if (bluringBackgroundRelativeLayout.getVisibility() == View.GONE){
                    bluringBackgroundRelativeLayout.setVisibility(View.VISIBLE);}
                userLoginType="google";
                mSocket.emit(MySocketsClass.EmmittersClass.ADD_USER, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.LOGIN, onLogin);
                mSocket.on(MySocketsClass.ListenersClass.ERROR_MESSAGE, loginError);
                mSocket.on("signUpResponse", signUpResponse);
                mSocket.off(MySocketsClass.ListenersClass.LOAD_OFFLINE_MESSAGES);
                mSocket.emit(MySocketsClass.EmmittersClass.CHECK_OFFLINE_MESSAGES);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_OFFLINE_MESSAGES, loading_offline_messages);
                mSocket.off(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP, getting_unread_total_notifications);
                mGoogleSignInClient.signOut();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hidingLoader();
                    }
                }, 30000);
            } else {

                Toast.makeText(this, "Cannot connect to server", Toast.LENGTH_SHORT).show();
                hidingLoader();
                MakingSocketConnected();
            }

        } catch (ApiException e) {
            Log.e("gogleTestingLogin", "excptn >" + e.getMessage());
        }
    }

    private void faceBookLoginCode(){
        final String EMAIL = "email";
        login_button.setPermissions(Collections.singletonList(EMAIL));
        //  login_button.setPermissions("email","public_profile","user_friends");

        Log.e("faceBokLogin","inMethod faceBookLoginCode");

        login_button.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        Toast.makeText(Login.this, "Login Success", Toast.LENGTH_SHORT).show();
                        Log.e("faceBokLogin","Success "+loginResult.toString());


                        GraphRequest graphRequest = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        if (object != null) {
                                            try {
                                                Log.e("fbTest",object.toString());
                                                String fName = object.getString("first_name");
                                                fName=fName.toLowerCase();
                                                Log.e("fbTest",fName);
                                                String lName = object.getString("last_name");
                                                Log.e("fbTest",lName);
                                                String email = object.getString("email");
                                                Log.e("fbTest",email);
                                                String social_id = object.getString("id");
                                                Log.e("fbTest",social_id);
                                                String image = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";

                                                if (mSocket.connected()) {
                                                    Login.this.stopService(new Intent(Login.this, Foreground_service.class));
                                                    showingLoader();
                                                    DeletingOldPreferencesForMineUserNameAndMinePaswrd();
                                                    DeletingOldPreferencesForsavingEmailAndId();
                                                    savingEmailAndId(email,social_id);
                                                    userLoginType="FaceBook";
                                                    JSONObject jsonObject = new JSONObject();
                                                    try {
                                                        jsonObject.put("email", email);
                                                        jsonObject.put("social_id",social_id);
                                                        jsonObject.put("login_type", "facebook");
                                                        jsonObject.put("token", token);
                                                        jsonObject.put("agent", "ANDROID1.0");

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    loginBtn.setEnabled(false);
                                                    if (bluringBackgroundRelativeLayout.getVisibility() == View.GONE){
                                                        bluringBackgroundRelativeLayout.setVisibility(View.VISIBLE);}
                                                    mSocket.emit(MySocketsClass.EmmittersClass.ADD_USER, jsonObject);
                                                    mSocket.on(MySocketsClass.ListenersClass.LOGIN, onLogin);
                                                    mSocket.on(MySocketsClass.ListenersClass.ERROR_MESSAGE, loginError);
                                                    mSocket.on("signUpResponse", signUpResponse);
                                                    mSocket.off(MySocketsClass.ListenersClass.LOAD_OFFLINE_MESSAGES);
                                                    mSocket.emit(MySocketsClass.EmmittersClass.CHECK_OFFLINE_MESSAGES);
                                                    mSocket.on(MySocketsClass.ListenersClass.LOAD_OFFLINE_MESSAGES, loading_offline_messages);
                                                    mSocket.off(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP);
                                                    mSocket.on(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP, getting_unread_total_notifications);
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            hidingLoader();
                                                        }
                                                    }, 30000);
                                                } else {

                                                    Toast.makeText(Login.this, "Cannot connect to server", Toast.LENGTH_SHORT).show();
                                                    hidingLoader();
                                                    MakingSocketConnected();
                                                }


                                                LoginManager.getInstance().logOut();

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Log.e("fbTest","exceptn >"+e.getMessage());
                                            }
                                        } else {
                                            LoginManager.getInstance().logOut();
                                            Toast.makeText(Login.this, "Can't connect with facebook right now. Please try again later or use other way to login.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                        );
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "email,last_name,first_name");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Toast.makeText(Login.this, "cancelled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("fbTest","excptn  "+exception.toString());
                        Toast.makeText(Login.this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginForgotPasswordBtn: {
                startActivity(new Intent(Login.this, ForgotPassword.class));
                break;
            }
            case R.id.loginBtn: {

                if (validateForm()) {
                    mySingleTon.usernameTextFromField = loginUsernameField.getText().toString().trim();
                    mySingleTon.passwordTextFromField = loginPasswordField.getText().toString().trim();
                    signinUser();
                    break;
                }
            }
        }
    }

    private boolean validateForm() {
        if (loginUsernameField.getText().toString().trim().equals("")) {
            Toast.makeText(Login.this, "Please enter username", Toast.LENGTH_SHORT).show();
            loginUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            loginUsernameIv.setColorFilter(ContextCompat.getColor(Login.this, R.color.colorRed), PorterDuff.Mode.SRC_IN);
            loginUsernameIv.startAnimation(animShake);
            return false;
        } else {
            loginUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            ImageViewCompat.setImageTintList(loginUsernameIv, null);
        }
        if (loginPasswordField.getText().toString().trim().equals("")) {
            Toast.makeText(Login.this, "Please enter password", Toast.LENGTH_SHORT).show();
            loginPasswordIv.setImageResource(R.drawable.new_theme_input_border);
            loginPasswordIv.setColorFilter(ContextCompat.getColor(Login.this, R.color.colorRed), PorterDuff.Mode.SRC_IN);
            loginPasswordIv.startAnimation(animShake);
            return false;
        } else {
            loginPasswordIv.setImageResource(R.drawable.new_theme_input_border);
            ImageViewCompat.setImageTintList(loginPasswordIv, null);
        }
        return true;
    }

    private void signinUser() {
        if (mSocket.connected()) {
            Login.this.stopService(new Intent(Login.this, Foreground_service.class));
            showingLoader();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("username", loginUsernameField.getText().toString());
                jsonObject.put("password", loginPasswordField.getText().toString());
                jsonObject.put("token", token);
                jsonObject.put("agent", "ANDROID1.0");
                jsonObject.put("login_type", "login");


            } catch (JSONException e) {
                e.printStackTrace();
            }
            loginBtn.setEnabled(false);
            if (bluringBackgroundRelativeLayout.getVisibility() == View.GONE){
                bluringBackgroundRelativeLayout.setVisibility(View.VISIBLE);}
            userLoginType="login";
            mSocket.emit(MySocketsClass.EmmittersClass.ADD_USER, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LOGIN, onLogin);
            mSocket.on(MySocketsClass.ListenersClass.ERROR_MESSAGE, loginError);
            mSocket.on("signUpResponse", signUpResponse);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_OFFLINE_MESSAGES);
            mSocket.emit(MySocketsClass.EmmittersClass.CHECK_OFFLINE_MESSAGES);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_OFFLINE_MESSAGES, loading_offline_messages);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP, getting_unread_total_notifications);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hidingLoader();
                }
            }, 30000);

        } else {

            Toast.makeText(this, "Cannot connect to server", Toast.LENGTH_SHORT).show();
            hidingLoader();
            MakingSocketConnected();
        }
    }

    public void MakingSocketConnected() {
        mSocket.connect();
    }

    private void ShowingTotalofflineMessagesToast(String allMsgs) {
        TextView toast_text;
        CircleImageView toast_image;
        LinearLayout layout_presence_toast;
        LayoutInflater inflater = (LayoutInflater) Login.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastView = inflater.inflate(R.layout.presence_toast, null);
        Toast toast = new Toast(Login.this);
        toast.setView(toastView);
        toast_text = toastView.findViewById(R.id.toast_text);
        toast_image = toastView.findViewById(R.id.toast_image);
        layout_presence_toast = toastView.findViewById(R.id.layout_presence_toast);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) toast_text.getLayoutParams();
        layoutParams.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
        toast_text.setLayoutParams(layoutParams);
        toast_image.setVisibility(View.GONE);
        layout_presence_toast.setBackgroundResource(R.drawable.offline_toast_msg_background);
        toast_text.setTextColor(getResources().getColor(R.color.colorWhite));
        toast_text.setPadding(10, 0, 10, 0);
        toast_text.setText("You have " + allMsgs + " offline messages.");
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 135);
        toast.show();
    }

    private void CheckingRemeberMeOption() {

        if (RemeberMeChkBox.isChecked()) {
            SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
            prefEditor55.putString("RememberMe", "Yes|" + loginUsernameField.getText().toString() + "|" + loginPasswordField.getText().toString());
            prefEditor55.apply();
        } else {
            DeletingPreferencesForRememberMe();
        }

    }

    private void SavingUserPreferences() {

        if (userLoginType.equals("login")) {
            DeletingPreferencesForLoginType();
            DeletingOldPreferencesForMineUserNameAndMinePaswrd();
            DeletingOldPreferencesForsavingEmailAndId();
            savingLoginType("login");
            savingUserMineNameAndMinePasswrdPreference(loginUsernameField.getText().toString(),loginPasswordField.getText().toString());
        }
        else if (userLoginType.equals("google")){
            DeletingPreferencesForLoginType();
            savingLoginType("google");
        }
        else if (userLoginType.equals("FaceBook")){
            DeletingPreferencesForLoginType();
            savingLoginType("FaceBook");
        }
    }

    private void savingUserMineNameAndMinePasswrdPreference( String minNameUser, String mineUserPaswrd){
        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        prefEditor55.putString("mine_user_name", minNameUser);
        prefEditor55.putString("mine_password", mineUserPaswrd);
        prefEditor55.apply();
    }

    private void savingOnlyUserMineNamePreference( String minNameUser){
        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        prefEditor55.putString("mine_user_name", minNameUser);
        prefEditor55.apply();
    }

    private String GettingRememberMePreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Login.this);
        return preferences.getString("RememberMe", "");

    }

    private void WritingIDPasswrdFromPreference() {
        if (RememberMePreferenceString != null && RememberMePreferenceString.split("\\|")[0].equals("Yes")) {
            loginUsernameField.setText(RememberMePreferenceString.split("\\|")[1]);
            loginPasswordField.setText(RememberMePreferenceString.split("\\|")[2]);
            RemeberMeChkBox.setChecked(true);
        } else {
            loginUsernameField.setText("");
            loginPasswordField.setText("");
        }
    }

    private void DeletingPreferencesForRememberMe() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        DeletingPreferences.remove("RememberMe");
        DeletingPreferences.apply();
    }

    private void DeletingPreferencesForLoginType() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        DeletingPreferences.remove("userLoginType");
        DeletingPreferences.apply();
    }

    private void SavingTokenValue(String TokenValue) {
        SharedPreferences.Editor SavingTokenPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        SavingTokenPreferences.putString("MyTokenValue", TokenValue);
        SavingTokenPreferences.apply();
    }

    private void savingLoginType(String loginType){
        SharedPreferences.Editor SavingLoginTypePreferences = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        SavingLoginTypePreferences.putString("userLoginType", loginType);
        SavingLoginTypePreferences.apply();
    }

    private void savingEmailAndId(String email,String userID){
        Log.e("MainTest", "savingEmailAndId > email>  " + email + " userID > " + userID);
        SharedPreferences.Editor SavingEmailAndIdPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        SavingEmailAndIdPreferences.putString("userEmail", email);
        SavingEmailAndIdPreferences.putString("userSpecialId", userID);
        SavingEmailAndIdPreferences.apply();
    }

    private void DeletingOldPreferencesForsavingEmailAndId() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        DeletingPreferences.remove("userEmail");
        DeletingPreferences.remove("userSpecialId");
        DeletingPreferences.apply();
    }

    private void DeletingOldPreferencesForMineUserNameAndMinePaswrd() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
        DeletingPreferences.remove("mine_user_name");
        DeletingPreferences.remove("mine_password");
        DeletingPreferences.apply();
    }

    private void showingLoader(){
        loader_imageview_Layout=MySocketsClass.custom_layout.Loader_image(Login.this, mainLayout);
    }

    private void hidingLoader(){
        if (loader_imageview_Layout != null && loader_imageview_Layout.getVisibility() == View.VISIBLE) {
            loader_imageview_Layout.setVisibility(View.GONE);
        }
    }

    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Login.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username, role, dp, statusMsg, status, sID, presence, age, gender, coins;
                    int theme;
                    try {
                        username = data.getString("username");
                        role = data.getString("role");
                        dp = data.getString("dp");
                        statusMsg = data.getString("statusMsg");
                        status = data.getString("status");
                        sID = data.getString("sID");
                        presence = data.getString("presence");
                        age = data.getString("age");
                        gender = data.getString("gender");
                        coins = data.getString("coins");
                        theme = Integer.parseInt(data.getString("theme"));

                        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Login.this).edit();
                        prefEditor55.putString("user_name", username);
                        prefEditor55.putString("role", role);
                        prefEditor55.putString("dp", dp);
                        prefEditor55.putString("statusMsg", statusMsg);
                        prefEditor55.putString("status", status);
                        prefEditor55.putString("sID", sID);
                        prefEditor55.putString("presence", presence);
                        prefEditor55.putString("age", age);
                        prefEditor55.putString("gender", gender);
                        prefEditor55.putString("coins", coins);
                        prefEditor55.putString("theme", String.valueOf(theme));
                        prefEditor55.apply();

                        editor.putString("username", username);
                        editor.putString("mine_password", mySingleTon.passwordTextFromField);
                        mySingleTon.username = username;
                        mySingleTon.role = role;
                        mySingleTon.dp = dp;
                        mySingleTon.statusMsg = statusMsg;
                        mySingleTon.status = status;
                        mySingleTon.sID = sID;
                        mySingleTon.presence = presence;
                        mySingleTon.age = age;
                        mySingleTon.gender = gender;
                        mySingleTon.coins = coins;
                        editor.apply();
                        CheckingRemeberMeOption();
                        SavingUserPreferences();
                        savingOnlyUserMineNamePreference(username);
                        Intent intent = new Intent(Login.this, HomeScreen.class);
                        startActivity(intent);
                        finish();
                    } catch (JSONException e) {
                        Log.d("me", "error  " + e.getMessage());
                        return;
                    }
                }
            });
        }
    };

    private Emitter.Listener signUpResponse = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Login.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
 //                   CustomDialogError customDialogError = new CustomDialogError();
                    try {
                        MySocketsClass.getInstance().showErrorDialog(Login.this, jsonObject.getString("title"), jsonObject.getString("message"), "verificationRequired", "");
                        hidingLoader();
                        mSocket.off("signUpResponse");
                        loginBtn.setEnabled(true);
                        if (bluringBackgroundRelativeLayout.getVisibility() == View.VISIBLE)
                            bluringBackgroundRelativeLayout.setVisibility(View.GONE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.w("run", "run: " + jsonObject.toString());
                }
            });
        }
    };

    private Emitter.Listener loginError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String title, message;
                    try {
                        title = data.getString("title");
                        message = data.getString("message");
                        hidingLoader();
                        loginBtn.setEnabled(true);
                        if (bluringBackgroundRelativeLayout.getVisibility() == View.VISIBLE){
                            bluringBackgroundRelativeLayout.setVisibility(View.GONE);
                        }
                        MySocketsClass.getInstance().showErrorDialog(Login.this, title, message, "invalidCredentials", "");
                        mSocket.off(MySocketsClass.ListenersClass.ERROR_MESSAGE);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.d("error test", data.toString());
                }
            });
        }
    };

    Emitter.Listener getting_unread_total_notifications = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                JSONArray notificationsArray = jsonObject.getJSONArray("notifications");
                MySocketsClass.static_objects.TotalNotificationsInsideApp = 0;
                for (int i = 0; i < notificationsArray.length(); i++) {
                    if (notificationsArray.getJSONObject(i).getString("message").split("\\|")[2].equals("0")) {
                        MySocketsClass.static_objects.TotalNotificationsInsideApp++;
                    }
                }
                Log.e("totlNotifctn", "Total notifctns unread in login screen >" + MySocketsClass.static_objects.TotalNotificationsInsideApp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("totlNotifctn", jsonObject.toString());
            Log.e("totlNotifctn", "In login activity");
        }
    };

    Emitter.Listener loading_offline_messages = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("oflineMessages", "InListener");
            MySocketsClass.static_objects.OfflineMessagesRedDotView = "visible";
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                final String MessangerUsername = jsonObject.getString("username");
                final int totalMsgsuserName = jsonObject.getInt("totalMsgs");
                final String user_dp = jsonObject.getString("user_dp");
                final String allMsgs = jsonObject.getString("allMsgs");
                final String ThePresence = jsonObject.getString("presence");
                final String LastMessage = jsonObject.getString("last_message");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ShowingTotalofflineMessagesToast(allMsgs);
                    }
                });


                if (!(MessangerUsername.equalsIgnoreCase("System"))) {
                    Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                    while (MyIterator.hasNext()) {
                        NewChatModelClass newChatModelClass2 = MyIterator.next();

                        if (newChatModelClass2.getMessangerName().equals(MessangerUsername))
                            MyIterator.remove();

                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {

                            if (!MySocketsClass.static_objects.hashMap.containsKey(MessangerUsername)) {
                                MySocketsClass.static_objects.hashMap.put(MessangerUsername, totalMsgsuserName);
                            }
                            newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, LastMessage,
                                    ThePresence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                            MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                            Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                            MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                            if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                            }
                        }

                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}
