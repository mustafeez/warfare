package com.app.websterz.warfarechat.activities.SendAttacks;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class SendAttackActivity extends AppCompatActivity {

    RelativeLayout parentLinearLayout;
    RecyclerView recyclerView;
    SendAttackModelClass sendAttackModelClass;
    ArrayList<SendAttackModelClass> SendingAttackList;
    SendAttackRecyclerViewAdapter sendAttackRecyclerViewAdapter;
    String Image  ,attack_name , attack_description, attack_cost, attack_slug ;
    ImageView RocketSend;
    LinearLayout returning_imageview=null;
    Context context;
    String chatroom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_attack);
        context = SendAttackActivity.this;
        initializations();
        initview();

    }

    public void initializations(){
        MySocketsClass.static_objects.MyStaticActivity = SendAttackActivity.this;
        SendingAttackList=new ArrayList<>();
        parentLinearLayout = findViewById(R.id.parent_layout_attack);
        MySocketsClass.static_objects.global_layout=parentLinearLayout;
        recyclerView = findViewById(R.id.sendAtackRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    public void initview(){
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        try {
            Intent intent = getIntent();
            chatroom = intent.getStringExtra("chatroom");
            jsonObject.put("chat_type", 1);
            jsonObject.put("target", "warfare");
            jsonObject1.put("chatroom", chatroom);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit(MySocketsClass.EmmittersClass.JOIN_ROOM,jsonObject1);
        mSocket.emit(MySocketsClass.EmmittersClass.GET_ATTACKS, jsonObject);
        returning_imageview= MySocketsClass.custom_layout.Loader_image(SendAttackActivity.this,parentLinearLayout);
        mSocket.on(MySocketsClass.ListenersClass.GET_ATTACK_RESPONSE, get_attack_response);
    }

    private Emitter.Listener get_attack_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        final JSONObject jsonObject = (JSONObject) args[0];

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (returning_imageview.getVisibility()==View.VISIBLE)
                    {
                        returning_imageview.setVisibility(View.GONE);
                    }
                    jsonObject.getString("target");

                    for(int i =0; i < jsonObject.getJSONArray("attacks").length(); i++){

                        Image  = jsonObject.getJSONArray("attacks").getJSONObject(i).getString("img");
                        attack_name  = jsonObject.getJSONArray("attacks").getJSONObject(i).getString("name");
                        attack_description  = jsonObject.getJSONArray("attacks").getJSONObject(i).getString("description");
                        attack_cost  = jsonObject.getJSONArray("attacks").getJSONObject(i).getString("cost");
                        attack_slug  = jsonObject.getJSONArray("attacks").getJSONObject(i).getString("slug");

                        sendAttackModelClass = new SendAttackModelClass(Image,attack_name,attack_description,attack_cost,attack_slug);
                        SendingAttackList.add(sendAttackModelClass);

                    }
                    setAdapter();

                    jsonObject.getString("chat_type");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        }
    };

    private void setAdapter() {
        sendAttackRecyclerViewAdapter = new SendAttackRecyclerViewAdapter(SendingAttackList,  this,chatroom);
        recyclerView.setAdapter(sendAttackRecyclerViewAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mSocket.off(MySocketsClass.ListenersClass.GET_ATTACK_RESPONSE);
        mSocket.off(MySocketsClass.ListenersClass.GET_ROOM_USERS_RESPONSE);
    }

    public void RocketLaunchAnimation(Activity activity){
        if (MySocketsClass.static_objects.attack_success!=null){

            if(MySocketsClass.static_objects.attack_success.equals("1")){
                RocketSend = MySocketsClass.custom_layout.Send_Attack_Image(activity, MySocketsClass.static_objects.global_layout);
                mSocket.off(MySocketsClass.ListenersClass.GET_ATTACK_LAUNCH_RESPONSE);

            }
            else{
                Toast.makeText(SendAttackActivity.this, "Attack Launcher Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void FinishShopActivity(Activity activity){
        activity.finish();
    }
}
