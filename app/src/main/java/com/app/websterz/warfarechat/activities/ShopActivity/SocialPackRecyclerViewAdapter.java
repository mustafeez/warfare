package com.app.websterz.warfarechat.activities.ShopActivity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import xyz.schwaab.avvylib.AvatarView;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;

public class SocialPackRecyclerViewAdapter extends RecyclerView.Adapter<SocialPackRecyclerViewAdapter.ViewHolder> {

    ArrayList<SocialPackModelClass> SocialPackList ;
    String ProtectionRemaining;
    String SocialPackCost,SocialPackName;
    Context context;
    int SocialPackNamePosition;

    public SocialPackRecyclerViewAdapter(ArrayList<SocialPackModelClass> socialpackList, Context context) {
        SocialPackList = socialpackList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.social_pack_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String remaining = SocialPackList.get(position).getSocialPackRemaining();
        String socialpackdate = SocialPackList.get(position).getSocialPackDate();

        //Add Avatar Loader
        holder.socialpackLoader.setVisibility(View.VISIBLE);
        holder.socialpackLoader.setAnimating(true);
        String Image = SocialPackList.get(position).getSocialPackImageView();
//        Picasso.get().load(baseUrl + Image).into(holder.socialpackImageView, new Callback() {
//            @Override
//            public void onSuccess() {
//                holder.socialpackLoader.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError(Exception e) {
//                holder.socialpackLoader.setVisibility(View.GONE);
//            }
//        });

        GlideApp.with(holder.itemView.getContext()).load(baseUrl + Image).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.socialpackLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(holder.socialpackImageView);

        holder.socialpackName.setText(SocialPackList.get(position).getSocialPackName() );
        holder.socialPackDes.setText(SocialPackList.get(position).getSocialPackDetail());

        double remainingCheck;
        try {
            remainingCheck = Double.parseDouble(remaining);
            if(remainingCheck > 0 ){
                holder.coinIMG.setVisibility(View.GONE);
                holder.socialpackBuyButton.setVisibility(View.GONE);
                holder.socialpackCost.setVisibility(View.GONE);
                long time = Long.parseLong(socialpackdate);
                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                cal.setTimeInMillis(time * 1000);
                String date = DateFormat.format("EEEE dd, MMMM yyyy", cal).toString();
                String today_time = DateFormat.format("hh:mm:ss a", cal).toString();
                String date2 = DateFormat.format("yyyy / MM / d", cal).toString();
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / d");
                String current_date = mdformat.format(calendar.getTime());

                if (current_date.equals(date2)){
                    holder.socialpackValidity.getLayoutParams().width = MATCH_PARENT;
                    holder.socialpackValidity.setText("Expires Today ");}
                else {
                    holder.socialpackValidity.getLayoutParams().width = MATCH_PARENT;
                    holder.socialpackValidity.setText("Expires in " + remaining + " days on " +date);}
            }
            else
            {
                holder.socialpackValidity.setText("valid for " +
                        SocialPackList.get(position).getSocialPackValidity()+" days");
            }
        }
        catch (NumberFormatException e){

        }



        holder.socialpackCost.setText(SocialPackList.get(position).getSocialPackCost());




        holder.socialpackBuyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SocialPackNamePosition = position;
                SocialPackCost = SocialPackList.get(SocialPackNamePosition).SocialPackCost;
                SocialPackName = SocialPackList.get(SocialPackNamePosition).getSocialPackName();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
                        giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity, null,
                                "SocialPack", SocialPackName,
                                SocialPackList.get(SocialPackNamePosition).getSocialPackSlug(),
                                SocialPackCost);
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return SocialPackList.size() ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView socialpackImageView,coinIMG;
        TextView socialpackName,socialpackValidity,socialpackCost;
        Button socialpackBuyButton;
        ExpandableTextView socialPackDes;
        AvatarView socialpackLoader;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            socialpackLoader = itemView.findViewById(R.id.SocialPackAvatarLoader);
            coinIMG = itemView.findViewById(R.id.SocialPackCoinIV);
            socialpackImageView = itemView.findViewById(R.id.socialPackImageView);
            socialpackName = itemView.findViewById(R.id.socialPackNameTextView);
            socialpackValidity = itemView.findViewById(R.id.SocialPackValidityTextView);
            socialpackCost = itemView.findViewById(R.id.SocialPackCostTextView);
            socialPackDes = itemView.findViewById(R.id.tv_socialPack);

            socialpackBuyButton = itemView.findViewById(R.id.SocialPackBuyBtn);

        }
    }
}
