package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses;

public class Liking_DislikingShoutDetailsModelClass {
    private String dp;
    private String userName;
    private String date;

    public Liking_DislikingShoutDetailsModelClass(String dp, String userName, String date) {
        this.dp = dp;
        this.userName = userName;
        this.date = date;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
