package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses;

public class MakeNewFriendsModelClass {
    private String FriendSuggestionUsername;
    private String ReqStatus;
    private String age;
    private String dp;
    private String role;
    private String country;
    private String add_requests;
    private String gender;
    private String FullName;

    public MakeNewFriendsModelClass(String friendSuggestionUsername, String reqStatus, String age, String dp, String role, String country, String add_requests, String gender, String fullName) {
        FriendSuggestionUsername = friendSuggestionUsername;
        ReqStatus = reqStatus;
        this.age = age;
        this.dp = dp;
        this.role = role;
        this.country = country;
        this.add_requests = add_requests;
        this.gender = gender;
        FullName = fullName;
    }

    public String getFriendSuggestionUsername() {
        return FriendSuggestionUsername;
    }

    public String getReqStatus() {
        return ReqStatus;
    }

    public String getAge() {
        return age;
    }

    public String getDp() {
        return dp;
    }

    public String getRole() {
        return role;
    }

    public String getCountry() {
        return country;
    }

    public String getAdd_requests() {
        return add_requests;
    }

    public String getGender() {
        return gender;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFriendSuggestionUsername(String friendSuggestionUsername) {
        FriendSuggestionUsername = friendSuggestionUsername;
    }

    public void setReqStatus(String reqStatus) {
        ReqStatus = reqStatus;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAdd_requests(String add_requests) {
        this.add_requests = add_requests;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

}
