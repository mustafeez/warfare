package com.app.websterz.warfarechat.DataBaseChatRooms;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface ChatRoomDBAO {
    @Insert
    public void addChatRoom_Chat(DataBaseChatRoomTables dataBaseChatRoomTables);

    @Query("select * from chat_room_table where chat_room_name=:chatroom_name")
    public List<DataBaseChatRoomTables> getChatRoomChat(String chatroom_name);


    @Query("DELETE FROM chat_room_table WHERE chat_room_name=:chatroom_name")
    public void deleteAlreadyPresentChatRoomChat(String chatroom_name);

    @Query("DELETE FROM chat_room_table")
    public void deleteAllDataChatRoom();

    @Query("DELETE FROM chat_room_table WHERE chat_room_name=:chatroom_name AND ROWID IN (SELECT ROWID FROM chat_room_table WHERE chat_room_name=:chatroom_name ORDER BY ROWID ASC LIMIT 1)")
    public void deleteStartRowsOfChatRoom(String chatroom_name);

}
