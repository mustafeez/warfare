package com.app.websterz.warfarechat.my_profile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.EditMyProfileActivity;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.app.websterz.warfarechat.signup.SignUp;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class EditPaswrdEmailProfile extends AppCompatActivity {

    RelativeLayout parentLinearLayout;
    AvatarView avatarLoader;
    CircleImageView users_dp;
    MySingleTon mySingleTon;
    EditText user_email_edittext, user_password_edittext;
    ImageView user_email_changer, user_password_changer_button;
    String userEmailed;
    String new_email, social_google,social_faceBook;
    LinearLayout googleSignInConnectionButton,facebookSignInConnectionButton;
    TextView googleConnectionState,facebookConnectionState;
    GoogleSignInClient mGoogleSignInClient;
    LoginButton login_button;
    CallbackManager callbackManager ;
    int RC_SIGN_IN = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_paswrd_email_profile);

        initializations();
        myCode();
        faceBookLoginCode();

        user_email_changer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_email_changer();
            }
        });
        user_password_changer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_password_changer();
            }
        });

        googleSignInConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionDisconnectingGoogleAccount();
            }
        });

        facebookSignInConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionDisconnectingFacebookAccount();
            }
        });
    }

    private void user_password_changer() {
        if (user_password_edittext.getText().toString().length() < 6) {
            hideKeyboard(EditPaswrdEmailProfile.this);
            Snackbar.make(parentLinearLayout, "Require minimum 6 alphabets", Snackbar.LENGTH_LONG).show();
        } else {
            String new_password = user_password_edittext.getText().toString();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("new_password", new_password);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("change password", jsonObject);
            mSocket.on("change password response", change_password_response);
        }
    }

    private void initializations() {
        mySingleTon = MySingleTon.getInstance();
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        avatarLoader = findViewById(R.id.MyProfileLoader);
        users_dp = findViewById(R.id.users_dp);
        user_email_edittext = findViewById(R.id.user_email_edittext);
        user_email_changer = findViewById(R.id.user_email_changer);
        user_password_edittext = findViewById(R.id.user_password_edittext);
        user_password_changer_button = findViewById(R.id.user_password_changer_button);
        googleSignInConnectionButton = findViewById(R.id.googleSignInConnectionButton);
        facebookSignInConnectionButton = findViewById(R.id.facebookSignInConnectionButton);
        login_button=findViewById(R.id.login_button);
        googleConnectionState = findViewById(R.id.googleConnectionState);
        facebookConnectionState = findViewById(R.id.facebookConnectionState);
        callbackManager = CallbackManager.Factory.create();
    }

    private void myCode() {
        Intent intent = getIntent();
        userEmailed = intent.getStringExtra("profileuserEmailed");
        social_google = intent.getStringExtra("social_google");
        social_faceBook = intent.getStringExtra("social_faceBook");

        user_email_edittext.setText(userEmailed);
        user_email_edittext.setSelection(user_email_edittext.getText().length());

        if (social_google.equals("")) {
            googleConnectionState.setText("Connect");
        } else {
            googleConnectionState.setText("Disconnect");
        }

        if (social_faceBook.equals("")) {
            facebookConnectionState.setText("Connect");
        } else {
            facebookConnectionState.setText("Disconnect");
        }

        avatarLoader.setVisibility(View.VISIBLE);
        avatarLoader.setAnimating(true);
        GlideApp.with(EditPaswrdEmailProfile.this).load(baseUrl + mySingleTon.dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                avatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(users_dp);

    }

    private void user_email_changer() {
        if (user_email_edittext.getText().toString().equals("")) {
            Snackbar.make(parentLinearLayout, "Enter email.", Snackbar.LENGTH_LONG).show();
        } else {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            new_email = user_email_edittext.getText().toString();
            if (new_email.matches(emailPattern)) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("new_email", new_email);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mSocket.emit("change email", jsonObject);
                mSocket.on("change email response", change_email_response);
            } else {
                Snackbar.make(parentLinearLayout, "Invalid email.", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = EditPaswrdEmailProfile.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
    }

    private void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void connectionDisconnectingGoogleAccount() {
        if (social_google.equals("")) {
            //  conctkrwana user
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(EditPaswrdEmailProfile.this, gso);
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } else {
            //disconctKrwanaUser
            Log.e("gogTets", "In else*");
            mSocket.off(MySocketsClass.ListenersClass.DISCONNECT_GOOGLE_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.DISCONNECT_GOOGLE);
            mSocket.on(MySocketsClass.ListenersClass.DISCONNECT_GOOGLE_RESPONSE, disconnect_google_response);
        }
    }

    private void connectionDisconnectingFacebookAccount(){
        if (social_faceBook.equals("")) {
            //  conctkrwana user
            login_button.performClick();
        } else {
            //disconctKrwanaUser
            Log.e("gogTets", "In else*");
            mSocket.off(MySocketsClass.ListenersClass.DISCONNECT_FACEBOOK_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.DISCONNECT_FACEBOOK);
            mSocket.on(MySocketsClass.ListenersClass.DISCONNECT_FACEBOOK_RESPONSE, disconnect_facebook_response);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("email", account.getEmail());
                jsonObject.put("social_id", account.getId());
                mSocket.off(MySocketsClass.ListenersClass.CONNECT_GOOGLE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.CONNECT_GOOGLE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.CONNECT_GOOGLE_RESPONSE, connect_hoogle_response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (ApiException e) {
            Log.e("gogleTestingLogin", "excptn >" + e.getMessage());
        }
    }

    private void faceBookLoginCode(){
        final String EMAIL = "email";
        login_button.setPermissions(Collections.singletonList(EMAIL));

        Log.e("faceBokLogin","inMethod faceBookLoginCode");

        login_button.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        Toast.makeText(EditPaswrdEmailProfile.this, "Login Success", Toast.LENGTH_SHORT).show();
                        Log.e("fbTest","Success "+loginResult.toString());


                        GraphRequest graphRequest = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        if (object != null) {
                                            try {
//                                                Log.e("fbTest",object.toString());
//                                                String fName = object.getString("first_name");
//                                                fName=fName.toLowerCase();
//                                                Log.e("fbTest",fName);
//                                                String lName = object.getString("last_name");
//                                                Log.e("fbTest",lName);
//                                                String email = object.getString("email");
//                                                Log.e("fbTest",email);
                                                String social_id = object.getString("id");
//                                                Log.e("fbTest",social_id);
//                                                String image = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";

                                                JSONObject jsonObject = new JSONObject();
                                                try {
                                                    Log.e("fbTest","In 1st Try");
                                                    jsonObject.put("social_id", social_id);
                                                    mSocket.off(MySocketsClass.ListenersClass.CONNECT_FACEBOOK_RESPONSE);
                                                    mSocket.emit(MySocketsClass.EmmittersClass.CONNECT_FACEBOOK, jsonObject);
                                                    mSocket.on(MySocketsClass.ListenersClass.CONNECT_FACEBOOK_RESPONSE, connect_facebook_response);
                                                } catch (JSONException e) {
                                                    Log.e("fbTest","In 1st catch  "+ e.getMessage());
                                                    e.printStackTrace();
                                                }

                                                LoginManager.getInstance().logOut();

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Log.e("fbTest","In 2nd catch  "+e.getMessage());
                                            }
                                        } else {
                                            LoginManager.getInstance().logOut();
                                            Toast.makeText(EditPaswrdEmailProfile.this, "Can't connect with facebook right now. Please try again later or use other way to login.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                        );
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "email,last_name,first_name");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        // App code

                        Toast.makeText(EditPaswrdEmailProfile.this, "cancelled", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("fbTest","excptn  "+exception.toString());
                        Toast.makeText(EditPaswrdEmailProfile.this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

    }

    private void socialLoginConnectionBroadcastMethod(String connectionState,String connectionType){
        LocalBroadcastManager localBroadcastManager=LocalBroadcastManager.getInstance(EditPaswrdEmailProfile.this);
        Intent intent=new Intent("SocialLoginConnection");
        intent.putExtra("SocialConnectionType",connectionType);
        intent.putExtra("SocialConnectionState",connectionState);
        localBroadcastManager.sendBroadcast(intent);
    }

    Emitter.Listener change_email_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            hideKeyboard(EditPaswrdEmailProfile.this);
            Log.e("emails", "listner");
            try {
                int success = jsonObject.getInt("success");
                final String message = jsonObject.getString("message");

                if (success == 1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("emails", "if");
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("result", new_email);
                            setResult(Activity.RESULT_OK, returnIntent);
                            Snackbar.make(parentLinearLayout, message, Snackbar.LENGTH_LONG).show();
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_CANCELED, returnIntent);
                            Snackbar.make(parentLinearLayout, message, Snackbar.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener change_password_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject jsonObject = (JSONObject) args[0];
            hideKeyboard(EditPaswrdEmailProfile.this);
            try {
                int success = jsonObject.getInt("success");
                final String message = jsonObject.getString("message");
                if (success == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(parentLinearLayout, message, Snackbar.LENGTH_LONG).show();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(parentLinearLayout, message, Snackbar.LENGTH_LONG).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener disconnect_google_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e("gogTets", "In listnr disconct*");
                        String success = jsonObject.getString("success");
                        if (success.equals("1")) {
                            Log.e("gogTets", "In if listener disconct*");
                            googleConnectionState.setText("Connect");
                            social_google = "";
                            socialLoginConnectionBroadcastMethod("","Google");

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("gogTets", "In excptn disconct*"+e.getMessage());
                    }
                }
            });
        }
    };

    Emitter.Listener disconnect_facebook_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String success = jsonObject.getString("success");
                        if (success.equals("1")) {
                            facebookConnectionState.setText("Connect");
                            social_faceBook = "";
                            socialLoginConnectionBroadcastMethod("","Facebook");


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener connect_hoogle_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("gogTets", "In listnr conct*");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String success = jsonObject.getString("success");
                        if (success.equals("1")) {
                            String email = jsonObject.getString("email");
                            googleConnectionState.setText("Disconnect");
                            social_google = "notEmpty";
                            user_email_edittext.setText(email);
                            mGoogleSignInClient.signOut();
                            socialLoginConnectionBroadcastMethod("notEmpty","Google");
                        }
                    } catch (JSONException e) {
                        Log.e("gogTets", "In listnexcptnr conct*"+e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener connect_facebook_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String success = jsonObject.getString("success");
                        if (success.equals("1")) {
                            facebookConnectionState.setText("Disconnect");
                            social_faceBook = "notEmpty";
                            socialLoginConnectionBroadcastMethod("notEmpty","Facebook");
                        }
                    } catch (JSONException e) {
                        Log.e("gogTets", "In listnexcptnr conct*"+e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }
    };

}
