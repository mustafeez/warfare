package com.app.websterz.warfarechat.about;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.customDialog.CustomDialog;

public class GetSupport extends AppCompatActivity implements View.OnClickListener {

    ImageView getSupportInfoBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_support);

        initView();
    }

    private void initView()
    {
        getSupportInfoBtn=findViewById(R.id.getSupportInfoBtn);
        getSupportInfoBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.getSupportInfoBtn:
            {
                CustomDialog customDialog= new CustomDialog();
                customDialog.showDialog(GetSupport.this,1);
                break;
            }
        }
    }
}
