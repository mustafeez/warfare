package com.app.websterz.warfarechat.services;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.DataBaseChatRooms.DataBase_ChatRooms;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.TestingPackage.MessageModelClass;
import com.app.websterz.warfarechat.TestingPackage.UserModelClass;
import com.app.websterz.warfarechat.activities.AgeUpActivity;
import com.app.websterz.warfarechat.activities.MyDialogActivity;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.activities.RewardActivity;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.activities.ChatRoomsChatActivity;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.adapter.FriendRequestAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.NewChatModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.app.websterz.warfarechat.landingScreen.MainActivity;
import com.app.websterz.warfarechat.myRoomDatabase.UserPrivateChatTables;
import com.app.websterz.warfarechat.myRoomDatabase.WarfareDatabase;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.emojione.tools.Client;
import com.example.stevenyang.snowfalling.SnowFlakesLayout;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import androidx.core.content.ContextCompat;
import androidx.room.Room;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static android.content.ContentValues.TAG;
import static com.app.websterz.warfarechat.TestingPackage.NewPrivateChat.privateChatRecyclerView;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;
import static com.app.websterz.warfarechat.subApplication.chaneel_id;

public class Foreground_service extends Service {

    MySingleTon mySingleTon;
    String username_service, username_passwrd, InternetStateForConnectionBar, serviceCategryType;
    int i = 0;
    ContactsFragment contactsFragment;
    NotificationCompat.Builder notification2;
    UserPrivateChatTables userPrivateChatTables = null;
    MediaPlayer mediaPlayer;
    public WarfareDatabase warfareDatabase;
    FriendRequestAdapter friendRequestAdapter = null;
    ImageView RocketReceive, BlastReceive, FreezedBottomIV, FreezedCornerIV;
    SnowFlakesLayout snowfall;
    String Tag = "Token: ", token, loginTypeOfLastUser, userEmailForGoogleLogin, userSpecialIdForGoogleLogin;
    NewChatModelClass newChatModelClass;
    DataBase_ChatRooms dataBase_chatRooms;
    PowerManager.WakeLock MyWakelock;
    PowerManager MyPowerManger;
 //   CustomDialogError customDialogError;
    Client MyClient;
    private final int MyNotificationId = 555;
    Notification.InboxStyle MyInboxStyle;
    Bitmap MyBitmap = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent!=null && intent.getStringExtra("ServiceType")!=null &&
                intent.getStringExtra("ServiceType").equals("SimpleService")) {
            serviceCategryType = "SimpleService";
        } else {
            serviceCategryType = intent.getExtras().getString("ServiceType");
        }
        Initializations();
        if (MyInboxStyle != null) {
            MyInboxStyle = null;
        }
        MyInboxStyle = new Notification.InboxStyle();
        Wakelocking();
        if (serviceCategryType != null && serviceCategryType.equals("ForeGroundService")) {
            my_init();
        }
        contactsFragment = new ContactsFragment();
        ReceivingListeners();

        if (serviceCategryType != null && serviceCategryType.equals("ForeGroundService")) {
            return START_STICKY;
        } else {
            return START_NOT_STICKY;
        }
    }

    public void my_init() {
        gettingUserNamePwd();
        PendingIntent pendingIntent;

        SharedPreferences alrdy_onlined = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this);
        String user_online_already = alrdy_onlined.getString("user_online", "offline");
        if (user_online_already != null && user_online_already.equals("online")) {
            Intent intent1 = new Intent(Foreground_service.this, MainActivity.class);
            pendingIntent = PendingIntent.getActivity(Foreground_service.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        } else {
            Intent intent1 = new Intent(Foreground_service.this, MainActivity.class);
            pendingIntent = PendingIntent.getActivity(Foreground_service.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        }

        Notification notification = new NotificationCompat.Builder(this, chaneel_id)
                .setContentTitle("Socio")
                .setContentText("Logged in as: " + username_service)
                .setSmallIcon(R.drawable.socio_icon)
                .setContentIntent(pendingIntent)
                .setDefaults(0)
                .build();
        startForeground(1, notification);
    }

    public void NotificationBelowOreo(JSONObject jsonObject) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        //  @SuppressLint("WrongConstant") PendingIntent pendingIntent=PendingIntent.getActivity(getBaseContext(),0,myIntent,Intent.FLAG_ACTIVITY_NEW_TASK);

        int nid = 0;
        Context context = getApplicationContext();
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, i,
                intent, 0);
        Notification.Builder builder = null;

        try {

            builder = new Notification.Builder(context)
                    .setContentTitle("Socio")
                    .setContentText(jsonObject.getString("message").split("\\|")[0])
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.socio_icon)
                    .setDefaults(Notification.DEFAULT_ALL);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), notification);

        i = i + 1;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void NotificationForOreo(JSONObject jsonObject) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        //  @SuppressLint("WrongConstant") PendingIntent pendingIntent=PendingIntent.getActivity(getBaseContext(),0,myIntent,Intent.FLAG_ACTIVITY_NEW_TASK);
        String cid = "my_channel_01";

// The user-visible name of the channel.
        CharSequence name = "Chanel Name2";

// The user-visible description of the channel.
        String description = "Channel Description2";

        int importance = NotificationManager.IMPORTANCE_HIGH;

        NotificationChannel mChannel = new NotificationChannel(cid, name, importance);

// Configure the notification channel.
        mChannel.setDescription(description);

        mChannel.enableLights(true);
// Sets the notification light color for notifications posted to this
// channel, if the device supports this feature.
        mChannel.setLightColor(Color.RED);
        // mChannel.setSound();

        mChannel.enableVibration(true);

        int nid = 0;
        Context context = Foreground_service.this;
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, i,
                intent, 0);

        Notification.Builder builder = null;
        try {

            builder = new Notification.Builder(context)
                    .setContentTitle("Socio")
                    .setContentText(jsonObject.getString("message").split("\\|")[0])
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.socio_icon)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setChannelId(cid);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
        notificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), notification);

        i = i + 1;

    }

    public void ChattingNotification(JSONObject jsonObject) {
        if (checkingNotificationMuteStatus().equalsIgnoreCase("UnMuted")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ChatNotificationForOreo(jsonObject);
            } else {
                ChatNotificationBelowOreo(jsonObject);
            }
        }
    }

    public void ChatNotificationBelowOreo(JSONObject jsonObject) {
        try {
            //   MyValue++;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("RSSPullService");
            Intent intent = new Intent(Foreground_service.this, HomeScreen.class);
            intent.setAction("OpenChatTab");
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    intent, PendingIntent.FLAG_CANCEL_CURRENT);

            NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            Notification.Builder builder = new Notification.Builder(Foreground_service.this);
            builder.setContentTitle("Warfare");
            builder.setContentText("New Messages");
            builder.setSmallIcon(R.drawable.socio_icon);
            builder.setLargeIcon(MyBitmap);
            builder.setDefaults(Notification.DEFAULT_ALL);
            builder.setAutoCancel(true);

            MyInboxStyle.setBigContentTitle("Warfare");
            if (jsonObject.getString("message_type").equals("2")){
                MyInboxStyle.addLine(jsonObject.getString("username") + ": " + "Image");
            }
            else if (jsonObject.getString("message_type").equals("3")){
                MyInboxStyle.addLine(jsonObject.getString("username") + ": " + "Voice");
            }
            else {
                MyInboxStyle.addLine(jsonObject.getString("username") + ": " + MyClient.shortnameToUnicode(jsonObject.getString("message")));}
            builder.setStyle(MyInboxStyle).setContentIntent(pendingIntent);

            nManager.notify("App Name", MyNotificationId, builder.build());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void ChatNotificationForOreo(JSONObject jsonObject) {

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        String cid = "my_channel_01";
        CharSequence name = "Chanel Name2";
        String description = "Channel Description2";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(cid, name, importance);
        mChannel.setDescription(description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        Context context = Foreground_service.this;
        Intent intent = new Intent(context, HomeScreen.class);
        intent.setAction("OpenChatTab");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);


        Notification.Builder builder = null;
        try {

            builder = new Notification.Builder(context)
                    .setContentTitle("Warfare")
                    .setContentText("New Messages")
                    .setSmallIcon(R.drawable.socio_icon)
                    .setLargeIcon(MyBitmap)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setChannelId(cid);
            MyInboxStyle.setBigContentTitle("Socio");
            if (jsonObject.getString("message_type").equals("2")){
                MyInboxStyle.addLine(jsonObject.getString("username") + ": " + "Image");
            }
            else if (jsonObject.getString("message_type").equals("3")){
                MyInboxStyle.addLine(jsonObject.getString("username") + ": " + "Voice");
            }
            else {
            MyInboxStyle.addLine(jsonObject.getString("username") + ": " + MyClient.shortnameToUnicode(jsonObject.getString("message")));}
            builder.setStyle(MyInboxStyle).setContentIntent(pendingIntent);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
        notificationManager.notify(MyNotificationId, notification);
    }

    private String checkingNotificationMuteStatus(){
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(this);
        return GettingInternetState.getString("MuteNotificationStats", "UnMuted");
    }
    @Override
    public void onDestroy() {
        mSocket.off(MySocketsClass.ListenersClass.RECEIVE_REWARD);
        mSocket.off(MySocketsClass.ListenersClass.RECEIVE_ATTACK);
        mSocket.off(MySocketsClass.ListenersClass.AGE_UP_RECEIVE_REWARD);
        mSocket.off(MySocketsClass.ListenersClass.RELOAD_CONTACT_LIST);
        mSocket.off(MySocketsClass.ListenersClass.RECEIVING_BUZZ);
        mSocket.off(MySocketsClass.ListenersClass.DISCONNECT);
        mSocket.off(MySocketsClass.ListenersClass.RECONNECT);
        mSocket.off(MySocketsClass.ListenersClass.RECONNECTERROR);
        mSocket.off(MySocketsClass.ListenersClass.LOAD_FRIEND_REQUESTS);
        mSocket.off(MySocketsClass.ListenersClass.RECEIVE_LIKE);
        mSocket.off(MySocketsClass.ListenersClass.RECEIVE_PRIVATE);
        mSocket.off(MySocketsClass.ListenersClass.RECEIVE_GIFTS);
        mSocket.off(MySocketsClass.ListenersClass.RECEIVE_NOTIFICATION);
        mSocket.off(MySocketsClass.ListenersClass.UPDATE_COINS);
        mSocket.off(MySocketsClass.ListenersClass.LOGIN_FROM_ANOTHER_LOCATION);
        mSocket.off(MySocketsClass.ListenersClass.ADD_NEW_CONTACT);
        mSocket.off(MySocketsClass.ListenersClass.USER_PRESENCE_CHANGE);
        mSocket.off(MySocketsClass.ListenersClass.SUSSCESS_MESSAGE);
        mSocket.off(MySocketsClass.ListenersClass.RECONNECT_ATTEMPT_POLLING);
        mSocket.off(MySocketsClass.ListenersClass.NEW_FRIEND_SUGGESTION);
        mSocket.off(MySocketsClass.ListenersClass.NEW_ROOM_SUGGESTION);
    }

    public void vibration() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
// Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
    }

    public void sound(String TuneType) {

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {

            if (TuneType.equals("Notification")) {
                mediaPlayer = MediaPlayer.create(this, R.raw.notification_in_app);
            } else if (TuneType.equals("buzz")) {
                mediaPlayer = MediaPlayer.create(this, R.raw.buzz_tune);
            } else if (TuneType.equals("FriendRequest")) {
                mediaPlayer = MediaPlayer.create(this, R.raw.add_request);
            }
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer.release();
                }
            });

        }


    }

    public void savingUserPreferenceOffline() {
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        prefuseronline.putString("user_online", "offline");
        prefuseronline.apply();
    }

    public void savingUserPreferenceOnline() {
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        prefuseronline.putString("user_online", "online");
        prefuseronline.apply();
        Log.e("MyTest", "6");
    }

    public void gettingUserNamePwd() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this);
        username_service = prefs.getString("mine_user_name", null);
        username_passwrd = prefs.getString("mine_password", null);
        Log.e("MyTest", "2");
    }

    public void gettingUserDataForReconnection() {
        Log.e("MainTest", "InMethodGettingUserNamePasswrd");
        loginTypeOfLastUser = GettingLoginTypePreference();
        Log.e("MainTest", "InMethodGettingUserNamePasswrd> loginTypeOfLastUser> " + loginTypeOfLastUser);
        if (loginTypeOfLastUser != null && loginTypeOfLastUser.equals("login")) {
            Log.e("MainTest", "InMethodGettingUserNamePasswrd> in if");
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this);
            username_service = prefs.getString("mine_user_name", null);
            username_passwrd = prefs.getString("mine_password", null);
            Log.e("MainTest", "username > " + username_service + " pwd > " + username_passwrd);
        } else if (loginTypeOfLastUser != null && loginTypeOfLastUser.equals("google")) {
            Log.e("testingLoginss", "InMethodGettingUserNamePasswrd> in else");
            gettingEmailAndUSerIdFromPreference();
        } else if (loginTypeOfLastUser != null && loginTypeOfLastUser.equals("FaceBook")) {
            Log.e("testingLoginss", "InMethodGettingUserNamePasswrd> in else");
            gettingEmailAndUSerIdFromPreference();
        }
    }

    private void gettingEmailAndUSerIdFromPreference() {

        SharedPreferences GettingPreferenc = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this);
        userEmailForGoogleLogin = GettingPreferenc.getString("userEmail", null);
        userSpecialIdForGoogleLogin = GettingPreferenc.getString("userSpecialId", null);

    }

    private String GettingLoginTypePreference() {
        SharedPreferences GettingPreferenc = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this);
        return GettingPreferenc.getString("userLoginType", null);
    }

    public void ReceivingListeners() {
        if (mSocket != null) {
            InternetStateForConnectionBar = GettingInternetStateForDisconnectionBar();
            if (!(InternetStateForConnectionBar.equals("WithouNet"))) {
                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_REWARD);
                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_ATTACK);
                mSocket.off(MySocketsClass.ListenersClass.AGE_UP_RECEIVE_REWARD);
                mSocket.off(MySocketsClass.ListenersClass.RELOAD_CONTACT_LIST);
                mSocket.off(MySocketsClass.ListenersClass.DISCONNECT);
                mSocket.off(MySocketsClass.ListenersClass.RECONNECT);
                mSocket.off(MySocketsClass.ListenersClass.LOAD_FRIEND_REQUESTS);
                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_LIKE);
                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_PRIVATE);
                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_GIFTS);
                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_NOTIFICATION);
                mSocket.off(MySocketsClass.ListenersClass.UPDATE_COINS);
                mSocket.off(MySocketsClass.ListenersClass.LOGIN_FROM_ANOTHER_LOCATION);
                mSocket.off(MySocketsClass.ListenersClass.ADD_NEW_CONTACT);
                mSocket.off(MySocketsClass.ListenersClass.ERROR_MESSAGE);
                mSocket.off(MySocketsClass.ListenersClass.USER_PRESENCE_CHANGE);
                mSocket.off(MySocketsClass.ListenersClass.SUSSCESS_MESSAGE);
                mSocket.off(MySocketsClass.ListenersClass.RECONNECT_ATTEMPT_POLLING);
                mSocket.off(MySocketsClass.ListenersClass.NEW_FRIEND_SUGGESTION);
                mSocket.off(MySocketsClass.ListenersClass.NEW_ROOM_SUGGESTION);


                mSocket.on(MySocketsClass.ListenersClass.RECEIVE_REWARD, receiving_reward);
                mSocket.on(MySocketsClass.ListenersClass.RECEIVE_ATTACK, receiving_attack);
                mSocket.on(MySocketsClass.ListenersClass.AGE_UP_RECEIVE_REWARD, receiving_AgeUp_reward);
                mSocket.on(MySocketsClass.ListenersClass.RELOAD_CONTACT_LIST, reloadContacts);
                mSocket.on(MySocketsClass.ListenersClass.RECEIVING_BUZZ, receiving_buzz);
                mSocket.on(MySocketsClass.ListenersClass.DISCONNECT, disconnecting);
                mSocket.on(MySocketsClass.ListenersClass.RECONNECT, reconnecting);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_FRIEND_REQUESTS, load_friend_requests);
                mSocket.on(MySocketsClass.ListenersClass.RECEIVE_LIKE, like_receive);
                mSocket.on(MySocketsClass.ListenersClass.RECEIVE_PRIVATE, receiving_private_chat);
                mSocket.on(MySocketsClass.ListenersClass.RECEIVE_GIFTS, receiving_gifts);
                mSocket.emit(MySocketsClass.EmmittersClass.LOAD_NOTIFICATION);
                mSocket.on(MySocketsClass.ListenersClass.RECEIVE_NOTIFICATION, receiveNotification);
                mSocket.on(MySocketsClass.ListenersClass.UPDATE_COINS, updatingCoin);
                mSocket.on(MySocketsClass.ListenersClass.LOGIN_FROM_ANOTHER_LOCATION, login_from_another_location);
                mSocket.on(MySocketsClass.ListenersClass.ADD_NEW_CONTACT, newAddContact);
                mSocket.on(MySocketsClass.ListenersClass.ERROR_MESSAGE, loginError);
                mSocket.on(MySocketsClass.ListenersClass.USER_PRESENCE_CHANGE, user_presence_change);
                mSocket.on(MySocketsClass.ListenersClass.SUSSCESS_MESSAGE, success_msg);
                mSocket.on(MySocketsClass.ListenersClass.NEW_FRIEND_SUGGESTION, new_friend_suggestion);
                mSocket.on(MySocketsClass.ListenersClass.NEW_ROOM_SUGGESTION, new_room_suggestion);
            }
        }
    }

    public String GettingCurrentDtaeMethod() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String format = tsLong.toString();
        return format;
    }

    public void Wakelocking() {
        MyWakelock = MyPowerManger.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myapp:mywakelocktag");
        MyWakelock.acquire(120 * 60 * 1000L /*20 minutes*/);
    }

    public void Initializations() {
        mySingleTon = MySingleTon.getInstance();
        userPrivateChatTables = new UserPrivateChatTables();
        warfareDatabase = Room.databaseBuilder(Foreground_service.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(Foreground_service.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        MyPowerManger = (PowerManager) getSystemService(Context.POWER_SERVICE);
  //      customDialogError = new CustomDialogError();
        MyClient = new Client(Foreground_service.this);
        MyBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.socio_icon);

    }

    private void ChattingIconRedDot() {
        SharedPreferences.Editor chattingredDot = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        chattingredDot.putString("chattingredDot", "newMessage");
        chattingredDot.apply();
    }

    private void Userreconnection() {
        gettingUserDataForReconnection();
        JSONObject jsonObject = new JSONObject();
        try {
            if (loginTypeOfLastUser.equals("login")) {
                jsonObject.put("username", username_service);
                jsonObject.put("password", username_passwrd);
                jsonObject.put("agent", "ANDROID1.0");
                jsonObject.put("token", token);
                jsonObject.put("login_type", "login");
            } else if (loginTypeOfLastUser.equals("google")) {
                jsonObject.put("email", userEmailForGoogleLogin);
                jsonObject.put("social_id", userSpecialIdForGoogleLogin);
                jsonObject.put("login_type", "google");
                jsonObject.put("token", token);
                jsonObject.put("agent", "ANDROID1.0");
            } else if (loginTypeOfLastUser.equals("FaceBook")) {
                jsonObject.put("email", userEmailForGoogleLogin);
                jsonObject.put("social_id", userSpecialIdForGoogleLogin);
                jsonObject.put("login_type", "facebook");
                jsonObject.put("token", token);
                jsonObject.put("agent", "ANDROID1.0");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectingSocketIfNotConnected();
        mSocket.emit(MySocketsClass.EmmittersClass.ADD_USER, jsonObject);
        mSocket.on(MySocketsClass.ListenersClass.LOGIN, onLogin);

    }

    private void ConnectingSocketIfNotConnected() {
        if (mSocket == null) {
            mSocket = AppConstants.mSocket;
            try {
                mSocket = IO.socket(baseUrl);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        if (!mSocket.connected()) {
            mSocket.connect();
        }
        Log.e("MyTest", "4");
    }

    private void SavingWithoutnetForDisconnectionBar() {
        SharedPreferences.Editor SavingPreferenceWithoutNet = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        SavingPreferenceWithoutNet.putString("UseropenState", "WithouNet");
        SavingPreferenceWithoutNet.apply();
    }

    private void SavingInternetForDisconnectionBar() {
        SharedPreferences.Editor SavingPreferenceWithNet = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        SavingPreferenceWithNet.putString("UseropenState", "");
        SavingPreferenceWithNet.apply();
    }

    private String GettingInternetStateForDisconnectionBar() {
        String MyInternetState;
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this);
        MyInternetState = GettingInternetState.getString("UseropenState", "");
        return MyInternetState;
    }

    private void ApplicationDisconnectedNotification(String DisconectedFromAnotherLocationText) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            DisconnectedNotificationForOreo(DisconectedFromAnotherLocationText);
        } else {
            DisconnectedNotificationBelowOreo(DisconectedFromAnotherLocationText);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DisconnectedNotificationForOreo(String DisconectedFromAnotherLocationText) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        String cid = "my_channel_01";
        CharSequence name = "Chanel Name2";
        String description = "Channel Description2";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(cid, name, importance);
        mChannel.setDescription(description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setStyle(new Notification.BigTextStyle(builder)
                .bigText(DisconectedFromAnotherLocationText)
                .setBigContentTitle("Logged out!")
                .setSummaryText("Socio"))
                .setContentTitle("Socio")
                .setContentText("Login from another location!")
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.socio_icon)
                .setChannelId(cid);


        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
        notificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), notification);

    }

    private void DisconnectedNotificationBelowOreo(String DisconectedFromAnotherLocationText) {
        CharSequence ShowingMessage = DisconectedFromAnotherLocationText;
        Context context = Foreground_service.this;
        Notification.Builder builder = new Notification.Builder(this);
        builder.setStyle(new Notification.BigTextStyle(builder)
                .bigText(DisconectedFromAnotherLocationText)
                .setBigContentTitle("Logged out!")
                .setSummaryText("Socio"))
                .setContentTitle("Socio")
                .setContentText("Login from another location!")
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.socio_icon);

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(2, notification);
    }

    private void AllNotificationsClear() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void DeletingPreferencesForDisconnectionBar() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        DeletingPreferences.remove("UseropenState");
        DeletingPreferences.apply();
    }

    private void SavingPreferenceFromAnotherLogin() {
        SharedPreferences.Editor SavingAnotherLocationPreference = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        SavingAnotherLocationPreference.putString("AnotherLocationPreference", "Yes");
        SavingAnotherLocationPreference.apply();
    }

    private void suggestedFriendToast(final String suggestedUserName, final String suggestedDp, final String suggestedMessage) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                final Snackbar snackbar = Snackbar.make(MySocketsClass.static_objects.global_layout, "", 15000);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                TextView textView = (TextView) layout.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setVisibility(View.INVISIBLE);

                LayoutInflater inflater = (LayoutInflater) Foreground_service.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View snackView = inflater.inflate(R.layout.friend_request_toast_layout, layout);
                snackView.setBackgroundColor(getResources().getColor(R.color.transparent));
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackView.getLayoutParams();
                params.gravity = Gravity.TOP;
                snackView.setPadding(0, 100, 0, 0);
                snackView.setLayoutParams(params);

                CircleImageView suggestedUserImageview = (CircleImageView) snackView.findViewById(R.id.suggestedUserDp);
                TextView suggestedUserMessage = (TextView) snackView.findViewById(R.id.suggestedUserMessage);
                Button suggestedUserButton = (Button) snackView.findViewById(R.id.suggestedUserButton);
                ImageView close_btn = (ImageView) snackView.findViewById(R.id.close_btn);
                final AvatarView ProfileavatarLoader = (AvatarView) snackView.findViewById(R.id.ProfileavatarLoader);

                ProfileavatarLoader.setVisibility(View.VISIBLE);
                ProfileavatarLoader.setAnimating(true);
                suggestedUserButton.setText("Add");
                suggestedUserMessage.setText(suggestedMessage);
                Glide.with(Foreground_service.this).load(baseUrl + suggestedDp).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        ProfileavatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).into(suggestedUserImageview);

                suggestedUserButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addingAFriend(suggestedUserName);
                        Toast.makeText(Foreground_service.this, "Friend Added", Toast.LENGTH_SHORT).show();
                    }
                });
                close_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
                snackbar.show();
            }
        });
    }

    private void suggestedChatRoomToast(final String suggestedChatRoom, final String suggestedDp, final String suggestedMessage) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                final Snackbar snackbar = Snackbar.make(MySocketsClass.static_objects.global_layout, "", 15000);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                TextView textView = (TextView) layout.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setVisibility(View.INVISIBLE);

                LayoutInflater inflater = (LayoutInflater) Foreground_service.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View snackView = inflater.inflate(R.layout.friend_request_toast_layout, layout);
                snackView.setBackgroundColor(getResources().getColor(R.color.transparent));
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackView.getLayoutParams();
                params.gravity = Gravity.TOP;
                params.setMargins(0, 100, 0, 0);
                snackView.setLayoutParams(params);

                CircleImageView suggestedUserImageview = (CircleImageView) snackView.findViewById(R.id.suggestedUserDp);
                ImageView close_btn = (ImageView) snackView.findViewById(R.id.close_btn);
                TextView suggestedUserMessage = (TextView) snackView.findViewById(R.id.suggestedUserMessage);
                Button suggestedUserButton = (Button) snackView.findViewById(R.id.suggestedUserButton);
                final AvatarView ProfileavatarLoader = (AvatarView) snackView.findViewById(R.id.ProfileavatarLoader);

                ProfileavatarLoader.setVisibility(View.VISIBLE);
                ProfileavatarLoader.setAnimating(true);
                suggestedUserButton.setText("Join");
                suggestedUserMessage.setText(suggestedMessage);
                Glide.with(Foreground_service.this).load(baseUrl + suggestedDp).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        ProfileavatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).into(suggestedUserImageview);


                suggestedUserButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        joiningChatRoom(suggestedChatRoom);
                        snackbar.dismiss();
                        //  Toast.makeText(Foreground_service.this, "Join Chatroom", Toast.LENGTH_SHORT).show();
                    }
                });

                close_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });

                snackbar.show();
            }
        });
    }

    private void joiningChatRoom(String chatRoomName) {
        if (MySocketsClass.static_objects.MyStaticActivity.getClass().equals(ChatRoomsChatActivity.class)) {
            Log.e("chtRoomTest", "AlreadyOpened");
            MySocketsClass.static_objects.MyStaticActivity.finish();
            Toast.makeText(Foreground_service.this, "AlreadyOpened", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("chtRoomTest", "NotOpened");
            Toast.makeText(Foreground_service.this, "NotOpened", Toast.LENGTH_SHORT).show();
        }
        Intent intent = new Intent(Foreground_service.this, ChatRoomsChatActivity.class);
        intent.putExtra("countryName", chatRoomName);
        intent.putExtra("TaglineName", "This is a test Tagline");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void logingOutUser() {
        if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
        }
        HomeScreen.is_user_connected = 0;
        MySocketsClass.static_objects.MyTotalCoin = "";
        warfareDatabase.myDataBaseAccessObject().deleteAllData();
        dataBase_chatRooms.chatRoomDBAO().deleteAllDataChatRoom();
        DeletingPreferencesForUserNameAndPaswrd();
        DeletingPreferenceForEmailAndSpecialId();
        MySocketsClass.getInstance().deletingPreferencesForNotifications(Foreground_service.this);
        AllNotificationsClear();
        MainActivity.one_time_emit = 1;
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        prefuseronline.putString("user_online", "offline");
        prefuseronline.apply();
        MySocketsClass.static_objects.NewUserMessageList.clear();
        MySocketsClass.static_objects.hashMap.clear();
        DeletingPreferencesForDisconnectionBar();
        DeletingPreferencesForRememberMe();
        Intent intent = new Intent(Foreground_service.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private void DeletingPreferencesForUserNameAndPaswrd() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        DeletingPreferences.remove("mine_user_name");
        DeletingPreferences.remove("mine_password");
        DeletingPreferences.apply();
    }

    private void DeletingPreferencesForRememberMe() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        DeletingPreferences.remove("RememberMe");
        DeletingPreferences.apply();
    }

    private void DeletingPreferenceForEmailAndSpecialId() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
        DeletingPreferences.remove("userEmail");
        DeletingPreferences.remove("userSpecialId");
        DeletingPreferences.apply();
    }

    private void addingAFriend(String friendName) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("add_username", friendName);
            mSocket.off("add contact response");
            mSocket.emit("add contact", jsonObject);
            mSocket.on("add contact response", adding_contact_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    Emitter.Listener reloadContacts = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            contactsFragment.getContacts();
        }
    };

    Emitter.Listener receiving_reward = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("CoinsTest", "mmmhere recvReward");
            try {
                String rewardpopup = jsonObject.getString("popup");
                String total_coins = jsonObject.getString("total_coins");

                if (!rewardpopup.equals(MySocketsClass.static_objects.one_time_reward)) {
                    MySocketsClass.static_objects.one_time_reward = rewardpopup;
                    if (rewardpopup.equals("1")) {
                        //Open Reward Activity
                        if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                            Intent intent = new Intent(Foreground_service.this, RewardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("rewardpopup", jsonObject.toString());
                            startActivity(intent);
                        }
                    } else {
                        mySingleTon.coins = total_coins;
                        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
                        prefEditor55.putString("coins", total_coins);
                        prefEditor55.apply();
                    }
                } else {
                    if (rewardpopup.equals("1")) {
                        //Open Reward Activity
                        if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {

                            Intent intent = new Intent(Foreground_service.this, RewardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("rewardpopup", jsonObject.toString());
                            startActivity(intent);
                        }
                    }
                    mySingleTon.coins = total_coins;
                    SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
                    prefEditor55.putString("coins", total_coins);
                    prefEditor55.apply();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener receiving_attack = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (i == 0) {
                i = 1;
                final JSONObject jsonObject = (JSONObject) args[0];
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String RecievePopUp = jsonObject.getString("shield");
                            String AttackType = jsonObject.getString("attack");
                            if (RecievePopUp.equals("0")) {
                                if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                                    if (AttackType.equals("Super Kick") || AttackType.equals("Multi Super Kick")) {
                                        RocketReceive = MySocketsClass.custom_layout.Rocket_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                        BlastReceive = MySocketsClass.custom_layout.Blast_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout, "SuperKick||MultiSuperKick");
                                    } else if (AttackType.equals("Disconnect")) {
                                        RocketReceive = MySocketsClass.custom_layout.Rocket_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                        BlastReceive = MySocketsClass.custom_layout.Blast_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout, "Disconnect");
                                    } else {
                                        if (MySocketsClass.static_objects.one_time_reward.matches("chatroom")) {
                                            RocketReceive = MySocketsClass.custom_layout.Rocket_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                            snowfall = MySocketsClass.custom_layout.Freeze_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                            FreezedBottomIV = MySocketsClass.custom_layout.Freeze_Attack_Bottom_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                            FreezedCornerIV = MySocketsClass.custom_layout.Freeze_Attack_Corner_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                        } else if (MySocketsClass.static_objects.one_time_reward.matches("notinchatroom")) {
                                            RocketReceive = MySocketsClass.custom_layout.Rocket_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                            snowfall = MySocketsClass.custom_layout.Freeze_Attack_Image(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                                        }
                                    }

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            i = 0;
        }
    };

    Emitter.Listener receiving_AgeUp_reward = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            //Open Reward Activity
            if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                Intent intent = new Intent(Foreground_service.this, AgeUpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("AgeUpReward", jsonObject.toString());
                startActivity(intent);
            }
        }
    };

    Emitter.Listener receiving_buzz = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        String user_buzzed = jsonObject.getString("username");
                        if (!user_buzzed.equals(MySocketsClass.static_objects.one_time_buzzer)) {
                            MySocketsClass.static_objects.one_time_buzzer = user_buzzed;
                            String dp = jsonObject.getString("dp");
                            Intent intent = new Intent(Foreground_service.this, MyDialogActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("type", "buzz");
                            intent.putExtra("userbuzzed", user_buzzed);
                            intent.putExtra("userdp", dp);
                            startActivity(intent);
                        } else {
                            vibration();
                            sound("buzz");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    PowerManager.WakeLock Wakelock = MyPowerManger.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "myapp:mywakelocktag");
                    Wakelock.acquire(5 * 1000L);
                }
            });
        }
    };

    Emitter.Listener disconnecting = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    MySocketsClass.static_objects.DisconnectionTextview = MySocketsClass.custom_layout.DisconnectionSnackbar(Foreground_service.this, MySocketsClass.static_objects.global_layout);
                    mSocket.off(MySocketsClass.ListenersClass.LOGIN);
                    savingUserPreferenceOffline();
                    if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
                        MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.VISIBLE);
                    }
                    if (serviceCategryType != null && serviceCategryType.equals("ForeGroundService")) {
                        PendingIntent pendingIntent;
                        Intent intent = new Intent(Foreground_service.this, HomeScreen.class);
                        pendingIntent = PendingIntent.getActivity(Foreground_service.this, 0, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);

                        Notification notification = new NotificationCompat.Builder(Foreground_service.this, chaneel_id)
                                .setContentTitle("Socio")
                                .setContentText("No user logged in.")
                                .setSmallIcon(R.drawable.socio_icon)
                                .setDefaults(0)
                                .setContentIntent(pendingIntent)
                                .build();
                        startForeground(1, notification);
                    }
                    SavingWithoutnetForDisconnectionBar();
                }
            });
        }
    };

    Emitter.Listener reconnecting = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Userreconnection();
        }
    };

    Emitter.Listener like_receive = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        final String friend_name = jsonObject.getString("username");
                        snowfall = MySocketsClass.custom_layout.LikeReceiving(Foreground_service.this, MySocketsClass.static_objects.global_layout);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener receiving_gifts = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                final JSONObject jsonObject = (JSONObject) args[0];

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
                        giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity, jsonObject, "Gifts", "", "", "");
                    }
                });

            }
        }
    };

    Emitter.Listener load_friend_requests = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    if (args.length == 0) {
                        JSONArray jsonArray = new JSONArray();
                        friendRequestAdapter = new FriendRequestAdapter(jsonArray);
                        MySocketsClass.static_objects.friendRequestRecycler.setAdapter(friendRequestAdapter);
                        friendRequestAdapter.notifyDataSetChanged();
                        if (jsonArray.length() == 0) {
                            MySocketsClass.static_objects.homeScreenFriendRequestTv.setVisibility(View.GONE);
                        } else {
                            MySocketsClass.static_objects.homeScreenFriendRequestTv.setVisibility(View.VISIBLE);
                            MySocketsClass.static_objects.homeScreenFriendRequestTv.setText(String.valueOf(jsonArray.length()));
                        }
                    } else {
                        JSONArray jsonArray = (JSONArray) args[0];
                        friendRequestAdapter = new FriendRequestAdapter(jsonArray);
                        MySocketsClass.static_objects.friendRequestRecycler.setAdapter(friendRequestAdapter);
                        friendRequestAdapter.notifyDataSetChanged();
                        if (jsonArray.length() == 0) {
                            MySocketsClass.static_objects.homeScreenFriendRequestTv.setVisibility(View.GONE);
                        } else {
                            MySocketsClass.static_objects.homeScreenFriendRequestTv.setVisibility(View.VISIBLE);
                            MySocketsClass.static_objects.homeScreenFriendRequestTv.setText(String.valueOf(jsonArray.length()));
                        }
                    }
                }
            });
        }
    };

    Emitter.Listener receiving_private_chat = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("ntsssa","here");
            try {
                final String message;
                final String MessangerUsername = jsonObject.getString("username");
                final String userid = jsonObject.getString("userid");
                final String user_dp = jsonObject.getString("user_dp");
                String role = jsonObject.getString("role");
                if (jsonObject.getString("message").contains("www.youtube.com") ||
                        jsonObject.getString("message").contains("https://youtu.be/")) {
                    message = jsonObject.getString("message");
                } else {
                    message = MyClient.shortnameToUnicode(jsonObject.getString("message"));
                }
                String chat_type = jsonObject.getString("chat_type");
                String direction = jsonObject.getString("direction");
                String gender = jsonObject.getString("gender");
                String age = jsonObject.getString("age");
                final String message_type = jsonObject.getString("message_type");
                final String statusMsg = jsonObject.getString("statusMsg");
                final String presence = jsonObject.getString("presence");

                if (MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                    ChattingIconRedDot();
                    ChattingNotification(jsonObject);
                    if (!(MessangerUsername.equalsIgnoreCase("System"))) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {

                                Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                                while (MyIterator.hasNext()) {
                                    NewChatModelClass newChatModelClass2 = MyIterator.next();

                                    if (newChatModelClass2.getMessangerName().equals(MessangerUsername))
                                        MyIterator.remove();

                                }

                                if (!MySocketsClass.static_objects.hashMap.containsKey(MessangerUsername)) {
                                    MySocketsClass.static_objects.hashMap.put(MessangerUsername, 0);
                                }

                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(MessangerUsername))) {
                                    MySocketsClass.static_objects.hashMap.put(MessangerUsername,
                                            MySocketsClass.static_objects.hashMap.get(MessangerUsername) + 1);
                                }

                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(MessangerUsername)))
                                    MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);

                                if (message_type.equals("1")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, message, presence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                                } else if (message_type.equals("3")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, "Voice", presence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                                } else {
                                    newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, "Image", presence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                                }
                                MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                                Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                                MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                                if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                    MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                                }
                            }

                        });
                    } else {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {

                                Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                                while (MyIterator.hasNext()) {
                                    NewChatModelClass newChatModelClass2 = MyIterator.next();

                                    if (newChatModelClass2.getMessangerName().equals(userid))
                                        MyIterator.remove();
                                }

                                if (!MySocketsClass.static_objects.hashMap.containsKey(userid)) {
                                    MySocketsClass.static_objects.hashMap.put(userid, 0);
                                }
                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(userid))) {
                                    MySocketsClass.static_objects.hashMap.put(userid,
                                            MySocketsClass.static_objects.hashMap.get(userid) + 1);
                                }

                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(userid)))
                                    MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);


                                if (message_type.equals("1")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, userid, message, presence, MySocketsClass.static_objects.hashMap.get(userid), "Chat");
                                } else if (message_type.equals("3")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, userid, "Voice", presence, MySocketsClass.static_objects.hashMap.get(userid), "Chat");
                                } else {
                                    newChatModelClass = new NewChatModelClass(user_dp, userid, "Image", presence, MySocketsClass.static_objects.hashMap.get(userid), "Chat");
                                }
                                MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                                Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                                MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                                if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                    MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                                }

                            }

                        });
                    }
                    //saving in db
                    if (MessangerUsername.equalsIgnoreCase("System")) {
                        userPrivateChatTables.setChatWith(userid);
                        userPrivateChatTables.setMessageDate("SystemMsg");
                    } else {
                        userPrivateChatTables.setChatWith(MessangerUsername);
                        userPrivateChatTables.setMessageDate(GettingCurrentDtaeMethod());
                    }
                    userPrivateChatTables.setChatMessage(message);
                    userPrivateChatTables.setChatMessageType(message_type);
                    userPrivateChatTables.setMessageUserDp(user_dp);
                    userPrivateChatTables.setChatStatus("otherUser");
                    warfareDatabase.myDataBaseAccessObject().addChat(userPrivateChatTables);
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            if (MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(MessangerUsername) ||
                                    (MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(userid) &&
                                            MessangerUsername.equalsIgnoreCase("system"))) {
                                if (message_type.equals("1")) {
                                    //simple text message k lye
                                    if (privateChatRecyclerView.canScrollVertically(1)) {
                                        // Agr scroll hua va hy recyclerview to scrolling nahi krani new msg pe
                                        MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass("otherUser",
                                                        new UserModelClass("otherUser", MessangerUsername, null, true), message),
                                                false);
                                    } else {
                                        // Agr scroll ni hua va hy recyclerview to scrolling krani new msg pe
                                        MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass("otherUser",
                                                        new UserModelClass("otherUser", MessangerUsername, null, true), message),
                                                true);
                                    }
                                } else if (message_type.equals("3")) {
                                    //voice message k lye
                                    if (privateChatRecyclerView.canScrollVertically(1)) {
                                        // Agr scroll hua va hy recyclerview to scrolling nahi krani new msg pe
                                        MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(generateRandomNumber(),
                                                        new UserModelClass("otherUser", MessangerUsername, null, true), "MyVoice_Note" + "|" + message),
                                                false);
                                    } else {
                                        // Agr scroll ni hua va hy recyclerview to scrolling krani new msg pe
                                        MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(generateRandomNumber(),
                                                        new UserModelClass("otherUser", MessangerUsername, null, true), "MyVoice_Note" + "|" + message),
                                                true);
                                    }

                                } else {
                                    //image message k lye
                                    if (privateChatRecyclerView.canScrollVertically(1)) {
                                        // Agr scroll hua va hy recyclerview to scrolling nahi krani new msg pe
                                        MessageModelClass messageModelClass = new MessageModelClass("otherUser",
                                                new UserModelClass("otherUser", MessangerUsername, null, true), null);

                                        messageModelClass.setImage(new MessageModelClass.Image(baseUrl + message));
                                        MySocketsClass.static_objects.MessageListAdapter.addToStart(messageModelClass, false);
                                    } else {
                                        // Agr scroll ni hua va hy recyclerview to scrolling krani new msg pe
                                        MessageModelClass messageModelClass = new MessageModelClass("otherUser",
                                                new UserModelClass("otherUser", MessangerUsername, null, true), null);

                                        messageModelClass.setImage(new MessageModelClass.Image(baseUrl + message));
                                        MySocketsClass.static_objects.MessageListAdapter.addToStart(messageModelClass, true);
                                    }
                                }
                            } else {
                                TextView toast_text;
                                CircleImageView toast_image;
                                LayoutInflater inflater = (LayoutInflater) Foreground_service.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View toastView = inflater.inflate(R.layout.presence_toast, null);
                                Toast toast = new Toast(Foreground_service.this);
                                toast.setView(toastView);
                                toast_text = toastView.findViewById(R.id.toast_text);
                                toast_image = toastView.findViewById(R.id.toast_image);
                                toast_image.setBorderColor(getResources().getColor(R.color.colorOnline));
                                Glide.with(Foreground_service.this).load(baseUrl + user_dp).into(toast_image);
                                int maxLength = 14;
                                InputFilter[] fArray = new InputFilter[1];
                                fArray[0] = new InputFilter.LengthFilter(maxLength);
                                toast_text.setFilters(fArray);
                                if (message_type.equals("2")){
                                    toast_text.setText("Image");
                                }
                                else if (message_type.equals("3")){
                                    toast_text.setText("Voice");
                                }
                                else {
                                toast_text.setText(message);}
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP, 0, 135);
                                toast.show();

                            }
                        }

                    });

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {

                            Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                            while (MyIterator.hasNext()) {
                                NewChatModelClass newChatModelClass2 = MyIterator.next();

                                if (MessangerUsername.equalsIgnoreCase("System")) {
                                    if (newChatModelClass2.getMessangerName().equals(userid))
                                        MyIterator.remove();
                                } else {
                                    if (newChatModelClass2.getMessangerName().equals(MessangerUsername))
                                        MyIterator.remove();
                                }

                            }

                            if (!MessangerUsername.equalsIgnoreCase("System")) {
                                //Agr system Msg nhi to username k nam sy sb list etc bnaana
                                if (!MySocketsClass.static_objects.hashMap.containsKey(MessangerUsername)) {
                                    MySocketsClass.static_objects.hashMap.put(MessangerUsername, 0);
                                }
                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(MessangerUsername))) {
                                    MySocketsClass.static_objects.hashMap.put(MessangerUsername,
                                            MySocketsClass.static_objects.hashMap.get(MessangerUsername) + 1);
                                }

                                if (MySocketsClass.static_objects.FragmentPosition == 0 &&
                                        !(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(MessangerUsername))
                                        || MySocketsClass.static_objects.FragmentPosition == 1 &&
                                        !(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(MessangerUsername))
                                        || MySocketsClass.static_objects.FragmentPosition == 2 &&
                                        !(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(MessangerUsername))) {
                                    MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);
                                }

                                if (message_type.equals("1")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, message, presence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                                } else if (message_type.equals("3")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, "Voice", presence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                                } else {
                                    newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, "Image", presence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                                }
                                MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                                Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                                MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                                if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                    MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                                }
                            } else {
                                //Agr system Msg hy to userId k nam sy sb list etc bnaana
                                if (!MySocketsClass.static_objects.hashMap.containsKey(userid)) {
                                    MySocketsClass.static_objects.hashMap.put(userid, 0);
                                }
                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(userid))) {
                                    MySocketsClass.static_objects.hashMap.put(userid,
                                            MySocketsClass.static_objects.hashMap.get(userid) + 1);
                                }

                                if (MySocketsClass.static_objects.FragmentPosition == 0 && !(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(userid)) || MySocketsClass.static_objects.FragmentPosition == 1 && !(MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(userid))) {
                                    MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);
                                }

                                if (message_type.equals("1")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, userid, message, presence, MySocketsClass.static_objects.hashMap.get(userid), "Chat");
                                } else if (message_type.equals("3")) {
                                    newChatModelClass = new NewChatModelClass(user_dp, userid, "Voice", presence, MySocketsClass.static_objects.hashMap.get(userid), "Chat");
                                } else {
                                    newChatModelClass = new NewChatModelClass(user_dp, userid, "Image", presence, MySocketsClass.static_objects.hashMap.get(userid), "Chat");
                                }
                                MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                                Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                                MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                                if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                    MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                                }
                            }
                        }

                    });

                    //saving in db
                    if (MessangerUsername.equalsIgnoreCase("System")) {
                        userPrivateChatTables.setChatWith(userid);
                        userPrivateChatTables.setMessageDate("SystemMsg|" + GettingCurrentDtaeMethod());
                        Log.e("DbSavingTest", userid);
                    } else {
                        userPrivateChatTables.setChatWith(MessangerUsername);
                        userPrivateChatTables.setMessageDate(GettingCurrentDtaeMethod());
                    }
                    userPrivateChatTables.setChatMessage(message);
                    userPrivateChatTables.setChatMessageType(message_type);
                    userPrivateChatTables.setMessageUserDp(user_dp);
                    userPrivateChatTables.setChatStatus("otherUser");
                    warfareDatabase.myDataBaseAccessObject().addChat(userPrivateChatTables);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private String generateRandomNumber() {
            Random random = new Random();
            return String.valueOf(random.nextInt());
        }
    };

    Emitter.Listener receiveNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("totlNotifctn", "In Service");
            Log.e("notificationTest", "In Service");
            JSONObject jsonObject = (JSONObject) args[0];
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    MySocketsClass.static_objects.TotalNotificationsInsideApp++;
                    Log.e("notificationTest", "Total notficatoin In Service" + MySocketsClass.static_objects.TotalNotificationsInsideApp);
                    MySocketsClass.static_objects.homeScreenNotificationTv.setVisibility(View.VISIBLE);
                    MySocketsClass.static_objects.homeScreenNotificationTv.setText(String.valueOf(MySocketsClass.static_objects.TotalNotificationsInsideApp));
                    sound("Notification");
                }
            });
            if (MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                if (checkingNotificationMuteStatus().equalsIgnoreCase("UnMuted")) {
                    notification2 = new NotificationCompat.Builder(Foreground_service.this);
                    notification2.setAutoCancel(true);
                    notification2.setSmallIcon(R.drawable.socio_icon);
                    notification2.setTicker("Socio");
                    notification2.setContentTitle("Friend Request");
                    try {
                        notification2.setContentText(jsonObject.getString("message").split("//|")[0]);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), notification2.build());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationForOreo(jsonObject);
                        } else {
                            NotificationBelowOreo(jsonObject);
                        }
                        notification2 = new NotificationCompat.Builder(Foreground_service.this);
                        notification2.setAutoCancel(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    Emitter.Listener updatingCoin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("CoinsTest", "mmmhere updateCoin");
            try {
                MySocketsClass.static_objects.MyTotalCoin = jsonObject.getString("total_coins");
                mySingleTon.coins = jsonObject.getString("total_coins");
                SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
                prefEditor55.putString("coins", jsonObject.getString("total_coins"));
                prefEditor55.apply();
                Log.e("CoinsTest", "mmmhere");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            String username, role, dp, statusMsg, status, sID, presence, age, gender, coins;
            int theme;
            SharedPreferences.Editor editor;
            editor = getSharedPreferences("myPrefs", 0).edit();
            Log.e("recovering", "Service");
            try {
                username = data.getString("username");
                role = data.getString("role");
                dp = data.getString("dp");
                statusMsg = data.getString("statusMsg");
                status = data.getString("status");
                sID = data.getString("sID");
                presence = data.getString("presence");
                age = data.getString("age");
                gender = data.getString("gender");
                coins = data.getString("coins");
                theme = Integer.parseInt(data.getString("theme"));
//                MySocketsClass.static_objects.theme = theme;

                SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(Foreground_service.this).edit();
                prefEditor55.putString("user_name", username);
                prefEditor55.putString("role", role);
                prefEditor55.putString("dp", dp);
                prefEditor55.putString("statusMsg", statusMsg);
                prefEditor55.putString("status", status);
                prefEditor55.putString("sID", sID);
                prefEditor55.putString("presence", presence);
                prefEditor55.putString("age", age);
                prefEditor55.putString("gender", gender);
                prefEditor55.putString("coins", coins);
                prefEditor55.putString("theme", String.valueOf(theme));
                prefEditor55.apply();

                editor.putString("username", username);
                mySingleTon.username = username;
                mySingleTon.role = role;
                mySingleTon.dp = dp;
                mySingleTon.statusMsg = statusMsg;
                mySingleTon.status = status;
                mySingleTon.sID = sID;
                mySingleTon.presence = presence;
                mySingleTon.age = age;
                mySingleTon.gender = gender;
                mySingleTon.coins = coins;
//                mySingleTon.theme = String.valueOf(theme);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            savingUserPreferenceOnline();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    if (MySocketsClass.static_objects.DisconnectionTextview != null)
                        MySocketsClass.static_objects.DisconnectionTextview.setVisibility(View.GONE);
                }
            });
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }
                    token = task.getResult().getToken();
                }
            });
            Wakelocking();
            if (serviceCategryType != null && serviceCategryType.equals("ForeGroundService")) {
                PendingIntent pendingIntent;
                Intent intent = new Intent(Foreground_service.this, HomeScreen.class);
                pendingIntent = PendingIntent.getActivity(Foreground_service.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
                Notification notification = new NotificationCompat.Builder(Foreground_service.this, chaneel_id)
                        .setContentTitle("Socio")
                        .setContentText("Logged in as: " + username_service)
                        .setSmallIcon(R.drawable.socio_icon)
                        .setDefaults(0)
                        .setContentIntent(pendingIntent)
                        .build();
                startForeground(1, notification);
            }
            SavingInternetForDisconnectionBar();
            mSocket.on(MySocketsClass.ListenersClass.RECONNECT_ATTEMPT_POLLING, reconnect_attempt_polling);
        }
    };

    Emitter.Listener reconnect_attempt_polling = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        }
    };

    Emitter.Listener login_from_another_location = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            try {
                if (jsonObject.getString("type").equals("2")) {
                    mSocket.off(MySocketsClass.ListenersClass.RECONNECT);
                    SavingPreferenceFromAnotherLogin();
                }
                if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
                            giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity, jsonObject, "AnotherLocation", "", "", "");
                        }
                    });
                } else {
                    if (jsonObject.getString("type").equals("2")) {
                        ApplicationDisconnectedNotification(jsonObject.getString("message"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener newAddContact = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.e("AFriendRequest", "New Request");
                    JSONObject jsonObject = (JSONObject) args[0];
                    String MyaddContactUsername = null;
                    try {
                        sound("FriendRequest");
                        MyaddContactUsername = jsonObject.getString("username");
 //                       CustomDialogError customDialogError = new CustomDialogError();
                        MySocketsClass.getInstance().showErrorDialog(MySocketsClass.static_objects.MyStaticActivity,
                                "", "You received a new friend request from " + MyaddContactUsername,
                                "newAddContact", MyaddContactUsername);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener loginError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.e("error","hereee");
                    JSONObject data = (JSONObject) args[0];
                    String title, message;
                    try {
                        title = data.getString("title");
                        message = data.getString("message");
 //                       logingOutUser();
                        MySocketsClass.getInstance().showErrorDialog(MySocketsClass.static_objects.MyStaticActivity, title, message, "invalidCredentials", "");
//                        if (customDialogError != null)
//                            customDialogError.showErrorDialog(MySocketsClass.static_objects.MyStaticActivity, title, message, "invalidCredentials", "");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            });
        }
    };

    Emitter.Listener user_presence_change = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        String presence = jsonObject.getString("presence");
                        String dp = jsonObject.getString("dp");
                        String username = jsonObject.getString("username");

                        for (int i = 0; i < MySocketsClass.static_objects.NewUserMessageList.size(); i++)
                            if (MySocketsClass.static_objects.NewUserMessageList.get(i).getMessangerName().equals(username)) {
                                MySocketsClass.static_objects.NewUserMessageList.get(i).setMessangerPresence(presence);
                            }
                        if (presence.equals("0")) {
                            TextView toast_text;
                            CircleImageView toast_image;
                            LayoutInflater inflater = (LayoutInflater) Foreground_service.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View toastView = inflater.inflate(R.layout.presence_toast, null);
                            Toast toast = new Toast(Foreground_service.this);
                            toast.setView(toastView);
                            toast_text = toastView.findViewById(R.id.toast_text);
                            toast_image = toastView.findViewById(R.id.toast_image);
                            toast_image.setBorderColor(getResources().getColor(R.color.colorOffline));
                            Glide.with(Foreground_service.this).load(baseUrl + dp).into(toast_image);
                            toast_text.setText(username + " is offline");
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 0, 135);
                            toast.show();
                            mSocket.emit("get contact list");
                        } else if (presence.equals("1")) {
                            TextView toast_text;
                            CircleImageView toast_image;
                            LayoutInflater inflater = (LayoutInflater) Foreground_service.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View toastView = inflater.inflate(R.layout.presence_toast, null);
                            Toast toast = new Toast(Foreground_service.this);
                            toast.setView(toastView);
                            toast_text = toastView.findViewById(R.id.toast_text);
                            toast_image = toastView.findViewById(R.id.toast_image);
                            toast_image.setBorderColor(getResources().getColor(R.color.colorOnline));
                            GlideApp.with(Foreground_service.this).load(baseUrl + dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    return false;
                                }
                            }).
                                    into(toast_image);
                            toast_text.setText(username + " is online");
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 0, 135);
                            toast.show();
                            mSocket.emit("get contact list");
                        } else {
                            mSocket.emit("get contact list");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            });
        }
    };

    Emitter.Listener success_msg = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        String MyTitle = jsonObject.getString("title");
                        String MyMessage = jsonObject.getString("message");
                        String MyType = jsonObject.getString("type");
                        if (MyType.equals("1")) {
//                            if (customDialogError != null)
                            MySocketsClass.getInstance().showErrorDialog(MySocketsClass.static_objects.MyStaticActivity, MyTitle, MyMessage, "SuccessMsg", "");
//                        } else {
//                            Snackbar.make(MySocketsClass.static_objects.global_layout, MyMessage, Snackbar.LENGTH_LONG).show();
                       }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener new_friend_suggestion = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                final String suggestedUserName = jsonObject.getString("username");
                final String suggestedDp = jsonObject.getString("dp");
                final String suggestedMessage = jsonObject.getString("message");
                if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                    suggestedFriendToast(suggestedUserName, suggestedDp, suggestedMessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener new_room_suggestion = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                Log.e("sugstd", "Listenng>");
                final String suggestedChatRoom = jsonObject.getString("chatroom");
                final String suggestedDp = jsonObject.getString("dp");
                final String suggestedMessage = jsonObject.getString("message");
                if (!MySocketsClass.getInstance().isAppIsInBackground(Foreground_service.this)) {
                    suggestedChatRoomToast(suggestedChatRoom, suggestedDp, suggestedMessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener adding_contact_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {


                    final AlertDialog MyalertDialog;
                    LinearLayout ParentLayout;
                    TextView errorDialogText1, errorDialogText2;
                    ImageView friend_added_imageview;
                    Button errorDialogTryAgainBtn;

                    JSONObject jsonObject = (JSONObject) args[0];
                    LayoutInflater layoutInflater = LayoutInflater.from(Foreground_service.this);
                    View myview = layoutInflater.inflate(R.layout.connection_error_dialog, null);
                    final AlertDialog.Builder adb = new AlertDialog.Builder(MySocketsClass.static_objects.MyStaticActivity);
                    MyalertDialog = adb.create();
                    MyalertDialog.setView(myview);
                    try {
                        if (jsonObject.getString("success").equals("1")) {
                            ParentLayout = myview.findViewById(R.id.ParentLayout);
                            ParentLayout.setBackgroundResource(R.drawable.square_new_theme_green);
                            errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                            errorDialogText1.setVisibility(View.GONE);
                            friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                            friend_added_imageview.setVisibility(View.VISIBLE);
                            friend_added_imageview.setImageResource(R.drawable.add_user_green);
                            friend_added_imageview.setColorFilter(ContextCompat.getColor(Foreground_service.this, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                            errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                            try {
                                errorDialogText2.setText(jsonObject.getString("message"));
                                errorDialogText2.setTextColor(Foreground_service.this.getResources().getColor(R.color.black));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                            errorDialogTryAgainBtn.setText("Ok");
                            errorDialogTryAgainBtn.setBackground(Foreground_service.this.getResources().getDrawable(R.drawable.error_dialog_green_btn));
                        } else {
                            ParentLayout = myview.findViewById(R.id.ParentLayout);
                            ParentLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);
                            errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                            errorDialogText1.setVisibility(View.GONE);
                            friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                            friend_added_imageview.setVisibility(View.VISIBLE);
                            friend_added_imageview.setImageResource(R.drawable.sad_icon);
                            errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                            try {
                                errorDialogText2.setText(jsonObject.getString("message"));
                                errorDialogText2.setTextColor(Foreground_service.this.getResources().getColor(R.color.black));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                            errorDialogTryAgainBtn.setText("Ok");
                            errorDialogTryAgainBtn.setBackground(Foreground_service.this.getResources().getDrawable(R.drawable.error_dialog_red_btn));
                        }
                        errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MyalertDialog.dismiss();
                                ContactsFragment contactsFragment = new ContactsFragment();
                                contactsFragment.getContacts();

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!((Activity) MySocketsClass.static_objects.MyStaticActivity).isFinishing())
                        MyalertDialog.show();

                }
            });
        }
    };


}

