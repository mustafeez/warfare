package com.app.websterz.warfarechat.homeScreen.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

public class CoinDetailsAdapter extends RecyclerView.Adapter<CoinDetailsAdapter.ViewHolder> {
    JSONObject jsonObject;
    JSONArray jsonArray;
    String message;

    public CoinDetailsAdapter(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        jsonArray = new JSONArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coins_recycler_row_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            String user_total_coins = jsonObject.getString("total_coins");
            String amount = jsonObject.getJSONArray("logs").getJSONObject(position).getString("amount");
            String sender_username = jsonObject.getJSONArray("logs").getJSONObject(position).getString("sender");
            String total = jsonObject.getJSONArray("logs").getJSONObject(position).getString("total");
            String direction = jsonObject.getJSONArray("logs").getJSONObject(position).getString("direction");
            String date_json = jsonObject.getJSONArray("logs").getJSONObject(position).getString("date");

            if (jsonObject.getJSONArray("logs").getJSONObject(position).has("message")) {
                message = jsonObject.getJSONArray("logs").getJSONObject(position).getString("message");
            }
            if (!date_json.equalsIgnoreCase("false")) {
                long time = Long.parseLong(date_json);
                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                cal.setTimeInMillis(time * 1000);
                String date = DateFormat.format("EEE d, MMM yyyy hh:mm:ss a", cal).toString();

                if (direction.equals("in")) {
                    holder.coins_sender_recever_image.setImageResource(R.drawable.coins_direction_in);
                    holder.coins_details_text.setText(amount + " points received from " + sender_username + " Total points " + total);
                    holder.coins_timing_text.setText(date);
                } else if (direction.equals("out")) {
                    holder.coins_sender_recever_image.setImageResource(R.drawable.coins_direction_out);
                    holder.coins_details_text.setText(message + " remaining points " + total);
                    holder.coins_timing_text.setText(date);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            jsonArray = jsonObject.getJSONArray("logs");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView coins_sender_recever_image;
        TextView coins_details_text, coins_timing_text;


        public ViewHolder(View itemView) {
            super(itemView);
            coins_sender_recever_image = itemView.findViewById(R.id.coins_sender_recever_image);
            coins_details_text = itemView.findViewById(R.id.coins_details_text);
            coins_timing_text = itemView.findViewById(R.id.coins_timing_text);
        }
    }
}
