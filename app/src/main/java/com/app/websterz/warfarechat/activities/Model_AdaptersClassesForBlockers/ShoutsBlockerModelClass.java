package com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers;

import android.os.Parcel;
import android.os.Parcelable;

public class ShoutsBlockerModelClass implements Parcelable {
    private String userName,realName,ShoutId,UserAge,UserGender,UserRole,ShoutTime,ShoutText,MUserDp;

    protected ShoutsBlockerModelClass(Parcel in) {
        userName = in.readString();
        realName = in.readString();
        ShoutId = in.readString();
        UserAge = in.readString();
        UserGender = in.readString();
        UserRole = in.readString();
        ShoutTime = in.readString();
        ShoutText = in.readString();
        MUserDp = in.readString();
    }

    public static final Creator<ShoutsBlockerModelClass> CREATOR = new Creator<ShoutsBlockerModelClass>() {
        @Override
        public ShoutsBlockerModelClass createFromParcel(Parcel in) {
            return new ShoutsBlockerModelClass(in);
        }

        @Override
        public ShoutsBlockerModelClass[] newArray(int size) {
            return new ShoutsBlockerModelClass[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getShoutId() {
        return ShoutId;
    }

    public void setShoutId(String shoutId) {
        ShoutId = shoutId;
    }

    public String getUserAge() {
        return UserAge;
    }

    public void setUserAge(String userAge) {
        UserAge = userAge;
    }

    public String getUserGender() {
        return UserGender;
    }

    public void setUserGender(String userGender) {
        UserGender = userGender;
    }

    public String getUserRole() {
        return UserRole;
    }

    public void setUserRole(String userRole) {
        UserRole = userRole;
    }

    public String getShoutTime() {
        return ShoutTime;
    }

    public void setShoutTime(String shoutTime) {
        ShoutTime = shoutTime;
    }

    public String getShoutText() {
        return ShoutText;
    }

    public void setShoutText(String shoutText) {
        ShoutText = shoutText;
    }

    public String getUserDp() {
        return MUserDp;
    }

    public void setUserDp(String userDp) {
        MUserDp = userDp;
    }

    public ShoutsBlockerModelClass(String userName, String realName, String shoutId, String userAge,
                                   String userGender, String userRole, String shoutTime, String shoutText, String UserDp) {
        this.userName = userName;
        this.realName = realName;
        ShoutId = shoutId;
        UserAge = userAge;
        UserGender = userGender;
        UserRole = userRole;
        ShoutTime = shoutTime;
        ShoutText = shoutText;
        MUserDp = UserDp;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(realName);
        dest.writeString(ShoutId);
        dest.writeString(UserAge);
        dest.writeString(UserGender);
        dest.writeString(UserRole);
        dest.writeString(ShoutTime);
        dest.writeString(ShoutText);
        dest.writeString(MUserDp);
    }
}
