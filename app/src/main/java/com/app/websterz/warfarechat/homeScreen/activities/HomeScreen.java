package com.app.websterz.warfarechat.homeScreen.activities;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.preference.PreferenceManager;

import com.app.websterz.warfarechat.BroadCastReceiver.AlarmReceiver;
import com.app.websterz.warfarechat.DataBaseChatRooms.DataBase_ChatRooms;
import com.app.websterz.warfarechat.Rest.ApiClient;
import com.app.websterz.warfarechat.Rest.ApiInterface;
import com.app.websterz.warfarechat.activities.BuyShields.BuyShieldActivity;
import com.app.websterz.warfarechat.activities.ShopActivity.ShopActivity;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.HomeFragment;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ScrollingClass.MediaLoader;
import com.app.websterz.warfarechat.myRoomDatabase.WarfareDatabase;
import com.app.websterz.warfarechat.my_profile.ProfileImage;
import com.app.websterz.warfarechat.webView.MyWebView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.ads.rewarded.ServerSideVerificationOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.MoreSettings;
import com.app.websterz.warfarechat.homeScreen.adapter.NotificationsAdapter;
import com.app.websterz.warfarechat.homeScreen.coin_details.Coins_details;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.app.websterz.warfarechat.homeScreen.fragments.chatRooms.fragments.ChatRoomsFragment;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.ChatsFragment;
import com.app.websterz.warfarechat.landingScreen.MainActivity;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.my_profile.My_profile;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.github.nkzawa.emitter.Emitter;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.schwaab.avvylib.AvatarView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.one_time_emit;
import static com.app.websterz.warfarechat.mySingleTon.MySocketsClass.static_objects.friendRequestRecycler;

public class HomeScreen extends AppCompatActivity implements
        TabLayout.OnTabSelectedListener,
        View.OnClickListener,
        ContactsFragment.FriendRequestInterface {

    ViewPager homeScreenViewPager;
    JSONObject jsonObject_notification = null;
    String updated_status_msg, error_status_msg, UserConnectionState;
    int success_updated_status, presence, presence_success, TotalNotificationOnResume = 0;
    LinearLayout presence_online, presence_offline, presence_busy, presence_away, coins_details, homeScreenHeader1, parent_alert;
    TabLayout tabs_homeScreen;
    RecyclerView notificationsRecycler;
    View view1, view2, view3, view4, homeScreenPresenceView1, homeScreenPresenceView2, homeScreenPresenceView3, homeScreenPresenceView4;
    MySingleTon mySingleTon;
    TextView homeScreenUserName, homeScreenUserStatus, homeScreenCoinsTv;
    CircleImageView homeProfileIv, homeScreenRoleIcon, homeScreenAgeIcon;
    RelativeLayout upperTopView, fragment_relativelayout, homeScreenHeader, homeScreenNotificationsRl, homeScreenFriendRequestRl, homeScreenSettingsRl, homeScreenPresenceRl, parent_layout_home;
    ImageView upperViewVisiblityIcon, homeScreenNotificationsIcon, homeScreenFriendRequestIcon, errorDialogVerificationImg, homeScreenTrophiesIcon, homeScreenSettingsIcon, homeScreenSettingsIcon1,
            homeScreenSettingsIcon2, homeScreenSettingsIcon3, homeScreenSettingsIcon4, homeScreenSettingsShareIcon, errorDialogHeaderImg, my_profile, homeScreenSettingsIcon5;
    View homeScreenSettingsIcon1View, homeScreenSettingsIcon2View, homeScreenSettingsIcon3View, homeScreenSettingsShareIconView,
            homeScreenSettingsIcon4View, homeScreenSettingsIcon5View;
    boolean isSettingsOpen = false, isNotificationsOpen = false, isFriendRequestOpen = false;
    JSONArray notificationsArray, friendRequestArray;
    Button update_status, cancel_update_status, dp_editor;
    TextView validation_textview_status;
    public static int is_user_connected = 0;
    String username_service;
    String AutoThemeTime = "06:00:00 pm", InternetStateForConnectionBar;
    SharedPreferences.Editor editor;
    AvatarView avatarLoader, avatarRole, avatarAge;
    @SuppressLint("WrongConstant")
    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
    WarfareDatabase warfareDatabase;
    DataBase_ChatRooms dataBase_chatRooms;
    AppBarLayout homeScreenAppbar;
    Animation bottomToTop;
    int theme;
    private boolean forcefullyHideUpperLayout = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);


        InternetStateForConnectionBar = GettingInternetStateForDisconnectionBar();
        if (!(InternetStateForConnectionBar.equals("WithouNet"))) {
            disableBroadcastReceiver();
            initializations();
            DeletingPreferencesForAnotherLocationLogin();
            setupTabIcons();
            oncreate_code();
            set_Values();
            click_listeners();
            OpeningrecentChatsTab();
  //          loadingVideoAd(HomeScreen.this);
            LocalBroadcastManager.getInstance(HomeScreen.this).registerReceiver(hidingAllLayouts, new IntentFilter("HidingLayouts"));
            upperViewVisiblityIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (homeScreenHeader.getVisibility() == View.VISIBLE) {
                        hidingUpperLayout();
                        forcefullyHideUpperLayout = true;
                    } else {
                        showingUpperLayout();
                        forcefullyHideUpperLayout = false;
                    }
                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    startActivity(new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:" + getApplicationContext().getPackageName())));
                } catch (Exception e) {
                }
            }
            homeScreenSettingsShareIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stoppingMediaPlayerOfAudioShouts();
                    sharing();
                }
            });

        } else {
            initializations2();
            setupTabIcons();
            ShowingDisconnectionBar();
        }
    }

    private void hidingUpperLayout() {
        homeScreenHeader.setVisibility(View.GONE);
        upperViewVisiblityIcon.setImageResource(R.drawable.aroow_down_key);

    }

    private void showingUpperLayout() {
        homeScreenHeader.setVisibility(View.VISIBLE);
        upperViewVisiblityIcon.setImageResource(R.drawable.aroow_up_view);
    }

    public void initializations() {
        MySocketsClass.static_objects.MyStaticActivity = HomeScreen.this;
        mySingleTon = MySingleTon.getInstance();
        coins_details = findViewById(R.id.coins_details);
        homeScreenPresenceView1 = findViewById(R.id.homeScreenPresenceView1);
        homeScreenPresenceView2 = findViewById(R.id.homeScreenPresenceView2);
        homeScreenPresenceView3 = findViewById(R.id.homeScreenPresenceView3);
        homeScreenPresenceView4 = findViewById(R.id.homeScreenPresenceView4);
        homeScreenAppbar = findViewById(R.id.homeScreenAppbar);
        dp_editor = findViewById(R.id.dp_editor);
        homeScreenViewPager = (ViewPager) findViewById(R.id.homeScreenViewPager);
        my_profile = findViewById(R.id.my_profile);
        Glide.with(HomeScreen.this).load(R.drawable.user_profile_inactive).into(my_profile);
        avatarLoader = findViewById(R.id.ProfileavatarLoader);
        avatarRole = findViewById(R.id.RoleAvatarLoader);
        avatarAge = findViewById(R.id.AgeAvatarLoader);
        parent_layout_home = findViewById(R.id.parent_layout_home);
        parent_layout_home.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);

        GlideApp.with(this).load(R.drawable.socio_background_low).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                parent_layout_home.setBackground(resource);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {

            }
        });


        editor = getSharedPreferences("myPrefs", 0).edit();
        friendRequestArray = new JSONArray();
        notificationsArray = new JSONArray();
        if (mSocket != null)
            mSocket.on(MySocketsClass.ListenersClass.UPDATE_USER_DP, UpdateMyDP);
        homeScreenSettingsRl = findViewById(R.id.homeScreenSettingsRl);

        MySocketsClass.static_objects.homeScreenFriendRequestTv = findViewById(R.id.homeScreenFriendRequestTv);
        homeScreenFriendRequestIcon = findViewById(R.id.homeScreenFriendRequestIcon);
        Glide.with(HomeScreen.this).load(R.drawable.friend_requests_inactive).into(homeScreenFriendRequestIcon);
        homeScreenFriendRequestIcon.setOnClickListener(this);
        homeScreenFriendRequestRl = findViewById(R.id.homeScreenFriendRequestRl);
        MySocketsClass.static_objects.homeScreenNotificationTv = findViewById(R.id.homeScreenNotificationTv);
        homeScreenNotificationsIcon = findViewById(R.id.homeScreenNotificationsIcon);
        Glide.with(HomeScreen.this).load(R.drawable.notify_bell_inactive).into(homeScreenNotificationsIcon);
        homeScreenNotificationsIcon.setOnClickListener(this);
        homeScreenNotificationsRl = findViewById(R.id.homeScreenNotificationsRl);

        notificationsRecycler = findViewById(R.id.notificationsRecycler);
        notificationsRecycler.setLayoutManager(layoutManager);
        notificationsRecycler.setItemViewCacheSize(20);
        //  notificationsRecycler.getRecycledViewPool().setMaxRecycledViews(0, 0);
        friendRequestRecycler = findViewById(R.id.friendRequestRecycler);
        friendRequestRecycler.setLayoutManager(new LinearLayoutManager(HomeScreen.this));

        homeScreenSettingsIcon1 = findViewById(R.id.homeScreenSettingsIcon1);
        Glide.with(HomeScreen.this).load(R.drawable.settings_more_settings).into(homeScreenSettingsIcon1);
        homeScreenSettingsIcon2 = findViewById(R.id.homeScreenSettingsIcon2);
        Glide.with(HomeScreen.this).load(R.drawable.settings_shield).into(homeScreenSettingsIcon2);
        homeScreenSettingsIcon3 = findViewById(R.id.homeScreenSettingsIcon3);
        Glide.with(HomeScreen.this).load(R.drawable.settings_shop).into(homeScreenSettingsIcon3);
        homeScreenSettingsShareIcon = findViewById(R.id.homeScreenSettingsShareIcon);
        Glide.with(HomeScreen.this).load(R.drawable.share_app).into(homeScreenSettingsShareIcon);
        homeScreenSettingsIcon4 = findViewById(R.id.homeScreenSettingsIcon4);
        Glide.with(HomeScreen.this).load(R.drawable.user_logout_btn).into(homeScreenSettingsIcon4);
        homeScreenSettingsIcon5 = findViewById(R.id.homeScreenSettingsIcon5);
        Glide.with(HomeScreen.this).load(R.drawable.settings_support).into(homeScreenSettingsIcon5);
        homeScreenSettingsIcon1.setOnClickListener(this);
        homeScreenSettingsIcon4.setOnClickListener(this);
        homeScreenSettingsIcon1View = findViewById(R.id.homeScreenSettingsIcon1View);
        homeScreenSettingsIcon2View = findViewById(R.id.homeScreenSettingsIcon2View);
        homeScreenSettingsIcon3View = findViewById(R.id.homeScreenSettingsIcon3View);
        homeScreenSettingsIcon4View = findViewById(R.id.homeScreenSettingsIcon4View);
        homeScreenSettingsIcon5View = findViewById(R.id.homeScreenSettingsIcon5View);
        homeScreenSettingsShareIconView = findViewById(R.id.homeScreenSettingsShareIconView);
        homeScreenSettingsIcon = findViewById(R.id.homeScreenSettingsIcon);
        homeScreenTrophiesIcon = findViewById(R.id.homeScreenTrophiesIcon);
        Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);
        Glide.with(HomeScreen.this).load(R.drawable.botton_trophy_unchecked).into(homeScreenTrophiesIcon);
        homeScreenSettingsIcon.setOnClickListener(this);
        homeScreenUserName = findViewById(R.id.homeScreenUserName);
        homeScreenUserStatus = findViewById(R.id.homeScreenUserStatus);
        homeScreenCoinsTv = findViewById(R.id.homeScreenCoinsTv);
        homeProfileIv = findViewById(R.id.homeProfileIv);
        homeScreenRoleIcon = findViewById(R.id.homeScreenRoleIcon);
        homeScreenAgeIcon = findViewById(R.id.homeScreenAgeIcon);
        upperViewVisiblityIcon = findViewById(R.id.upperViewVisiblityIcon);
        bottomToTop = AnimationUtils.loadAnimation(this, R.anim.bottomtotop);


        setUpViewPager(homeScreenViewPager);
        tabs_homeScreen = (TabLayout) findViewById(R.id.tabs_homeScreen);
        tabs_homeScreen.setupWithViewPager(homeScreenViewPager);

        tabs_homeScreen.addOnTabSelectedListener(this);
        AllNotificationsClear();
        checkingServiceTypeAndStartingService();
        homeScreenHeader = findViewById(R.id.homeScreenHeader);
        homeScreenHeader1 = findViewById(R.id.homeScreenHeader1);
        fragment_relativelayout = findViewById(R.id.fragment_relativelayout);
        warfareDatabase = Room.databaseBuilder(HomeScreen.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(HomeScreen.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        MySocketsClass.static_objects.shoutCategryShowing = "SHOWING FROM PUBLIC";
    }

    public void initializations2() {
        MySocketsClass.static_objects.MyStaticActivity = HomeScreen.this;
        mySingleTon = MySingleTon.getInstance();
        coins_details = findViewById(R.id.coins_details);
        homeScreenPresenceView1 = findViewById(R.id.homeScreenPresenceView1);
        homeScreenPresenceView2 = findViewById(R.id.homeScreenPresenceView2);
        homeScreenPresenceView3 = findViewById(R.id.homeScreenPresenceView3);
        homeScreenPresenceView4 = findViewById(R.id.homeScreenPresenceView4);
        homeScreenAppbar = findViewById(R.id.homeScreenAppbar);
        dp_editor = findViewById(R.id.dp_editor);
        homeScreenViewPager = (ViewPager) findViewById(R.id.homeScreenViewPager);
        my_profile = findViewById(R.id.my_profile);
        Glide.with(HomeScreen.this).load(R.drawable.user_profile_inactive).into(my_profile);
        avatarLoader = findViewById(R.id.ProfileavatarLoader);
        avatarRole = findViewById(R.id.RoleAvatarLoader);
        avatarAge = findViewById(R.id.AgeAvatarLoader);
        parent_layout_home = findViewById(R.id.parent_layout_home);
        parent_layout_home.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        GlideApp.with(this).load(R.drawable.socio_background_low).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                parent_layout_home.setBackground(resource);
            }
        });
        MySocketsClass.static_objects.global_layout = parent_layout_home;

        editor = getSharedPreferences("myPrefs", 0).edit();
        friendRequestArray = new JSONArray();
        notificationsArray = new JSONArray();
        homeScreenSettingsRl = findViewById(R.id.homeScreenSettingsRl);

        MySocketsClass.static_objects.homeScreenFriendRequestTv = findViewById(R.id.homeScreenFriendRequestTv);
        homeScreenFriendRequestIcon = findViewById(R.id.homeScreenFriendRequestIcon);
        Glide.with(HomeScreen.this).load(R.drawable.friend_requests_inactive).into(homeScreenFriendRequestIcon);
        homeScreenFriendRequestIcon.setOnClickListener(this);
        homeScreenFriendRequestRl = findViewById(R.id.homeScreenFriendRequestRl);
        MySocketsClass.static_objects.homeScreenNotificationTv = findViewById(R.id.homeScreenNotificationTv);
        homeScreenNotificationsIcon = findViewById(R.id.homeScreenNotificationsIcon);
        Glide.with(HomeScreen.this).load(R.drawable.notify_bell_inactive).into(homeScreenNotificationsIcon);
        homeScreenNotificationsIcon.setOnClickListener(this);
        homeScreenNotificationsRl = findViewById(R.id.homeScreenNotificationsRl);

        notificationsRecycler = findViewById(R.id.notificationsRecycler);
        notificationsRecycler.setLayoutManager(layoutManager);
        notificationsRecycler.setItemViewCacheSize(20);
        //  notificationsRecycler.getRecycledViewPool().setMaxRecycledViews(0, 0);
        friendRequestRecycler = findViewById(R.id.friendRequestRecycler);
        friendRequestRecycler.setLayoutManager(new LinearLayoutManager(HomeScreen.this));

        homeScreenSettingsIcon1 = findViewById(R.id.homeScreenSettingsIcon1);
        Glide.with(HomeScreen.this).load(R.drawable.settings_more_settings).into(homeScreenSettingsIcon1);
        homeScreenSettingsIcon2 = findViewById(R.id.homeScreenSettingsIcon2);
        Glide.with(HomeScreen.this).load(R.drawable.settings_shield).into(homeScreenSettingsIcon2);
        homeScreenSettingsIcon3 = findViewById(R.id.homeScreenSettingsIcon3);
        Glide.with(HomeScreen.this).load(R.drawable.settings_shop).into(homeScreenSettingsIcon3);
        homeScreenSettingsIcon4 = findViewById(R.id.homeScreenSettingsIcon4);
        Glide.with(HomeScreen.this).load(R.drawable.user_logout_btn).into(homeScreenSettingsIcon4);
        homeScreenSettingsIcon5 = findViewById(R.id.homeScreenSettingsIcon5);
        Glide.with(HomeScreen.this).load(R.drawable.settings_support).into(homeScreenSettingsIcon5);
        homeScreenSettingsIcon1.setOnClickListener(this);
        homeScreenSettingsIcon4.setOnClickListener(this);
        homeScreenSettingsIcon1View = findViewById(R.id.homeScreenSettingsIcon1View);
        homeScreenSettingsIcon2View = findViewById(R.id.homeScreenSettingsIcon2View);
        homeScreenSettingsIcon3View = findViewById(R.id.homeScreenSettingsIcon3View);
        homeScreenSettingsIcon4View = findViewById(R.id.homeScreenSettingsIcon4View);
        homeScreenSettingsIcon5View = findViewById(R.id.homeScreenSettingsIcon5View);
        homeScreenSettingsShareIconView = findViewById(R.id.homeScreenSettingsShareIconView);
        homeScreenSettingsIcon = findViewById(R.id.homeScreenSettingsIcon);
        homeScreenTrophiesIcon = findViewById(R.id.homeScreenTrophiesIcon);
        Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);
        Glide.with(HomeScreen.this).load(R.drawable.botton_trophy_unchecked).into(homeScreenTrophiesIcon);
        homeScreenSettingsIcon.setOnClickListener(this);
        homeScreenUserName = findViewById(R.id.homeScreenUserName);
        homeScreenUserStatus = findViewById(R.id.homeScreenUserStatus);
        homeScreenCoinsTv = findViewById(R.id.homeScreenCoinsTv);
        homeProfileIv = findViewById(R.id.homeProfileIv);
        homeScreenRoleIcon = findViewById(R.id.homeScreenRoleIcon);
        homeScreenAgeIcon = findViewById(R.id.homeScreenAgeIcon);
        upperViewVisiblityIcon = findViewById(R.id.upperViewVisiblityIcon);
        bottomToTop = AnimationUtils.loadAnimation(this, R.anim.bottomtotop);

        setUpViewPager(homeScreenViewPager);
        tabs_homeScreen = (TabLayout) findViewById(R.id.tabs_homeScreen);
        tabs_homeScreen.setupWithViewPager(homeScreenViewPager);

        tabs_homeScreen.addOnTabSelectedListener(this);
        AllNotificationsClear();
        homeScreenHeader = findViewById(R.id.homeScreenHeader);
        homeScreenHeader1 = findViewById(R.id.homeScreenHeader1);
        fragment_relativelayout = findViewById(R.id.fragment_relativelayout);
        warfareDatabase = Room.databaseBuilder(HomeScreen.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(HomeScreen.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
    }

    public void oncreate_code() {
        getAppTheme();
        savingUserPreferenceOnline();
        homeScreenHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidingSettingMenu();
                hidingnotificationLayout();
            }
        });
        homeScreenHeader1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidingSettingMenu();
                hidingnotificationLayout();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Glide.with(HomeScreen.this).load(R.drawable.user_profile_inactive).into(my_profile);
        Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);
        Glide.with(HomeScreen.this).load(R.drawable.botton_trophy_unchecked).into(homeScreenTrophiesIcon);
        homeScreenCoinsTv.setTextColor(getResources().getColor(R.color.black));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("ResumingTest", "resumeHomeAScreen");
        getAppTheme();
        // homeScreenViewPager.setCurrentItem(0);
        CheckingNewMessageRedDot();
        Log.e("totlNotifctn", "HomeScreen > " + MySocketsClass.static_objects.TotalNotificationsInsideApp);
        if (MySocketsClass.static_objects.TotalNotificationsInsideApp > 0) {
            MySocketsClass.static_objects.homeScreenNotificationTv.setText(String.valueOf(MySocketsClass.static_objects.TotalNotificationsInsideApp));
            MySocketsClass.static_objects.homeScreenNotificationTv.setVisibility(View.VISIBLE);
        }
        MySocketsClass.static_objects.global_layout = parent_layout_home;
        MySocketsClass.static_objects.MyStaticActivity = HomeScreen.this;

//        if ((MySocketsClass.static_objects.MyTotalCoin != null) && (!MySocketsClass.static_objects.MyTotalCoin.equals(""))) {
//            DecimalFormat formatter = new DecimalFormat("#,###,###");
//            String formated_user_total_coins = formatter.format(Double.parseDouble(MySocketsClass.static_objects.MyTotalCoin));
//            homeScreenCoinsTv.setText(formated_user_total_coins);
//        } else {
        SharedPreferences CoinPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        mySingleTon.coins = CoinPreferences.getString("coins", "");
        if (!mySingleTon.coins.equals("")) {
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String formated_user_total_coins = formatter.format(Double.parseDouble(mySingleTon.coins));
            homeScreenCoinsTv.setText(formated_user_total_coins);
        } else {
            homeScreenCoinsTv.setText("");
        }
        hidingnotificationLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stoppingMediaPlayerOfAudioShouts();
        LocalBroadcastManager.getInstance(HomeScreen.this).unregisterReceiver(hidingAllLayouts);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void updating_status() {

        final Dialog dialog;
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.statusmsg_dialog);

//        if (MySocketsClass.static_objects.theme == 0) {
//
//            dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//        } else if (MySocketsClass.static_objects.theme == 1) {
//            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//            Date systemDate = Calendar.getInstance().getTime();
//            String myDate = sdf.format(systemDate);
//            try {
//                Date Date1 = sdf.parse(myDate);
//                Date Date2 = sdf.parse(AutoThemeTime);
//                if (Date1.getTime() < Date2.getTime()) {
//                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                } else if (Date1.getTime() > Date2.getTime()) {
//                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                }
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//
//        } else if (MySocketsClass.static_objects.theme == 2) {
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
        //       }
        cancel_update_status = dialog.findViewById(R.id.cancel_update_status);
        cancel_update_status.setBackground(ContextCompat.getDrawable(dialog.getContext(), R.drawable.error_dialog_white_btn));
        update_status = dialog.findViewById(R.id.update_status);
        update_status.setBackground(ContextCompat.getDrawable(dialog.getContext(), R.drawable.error_dialog_green_btn));
        errorDialogVerificationImg = dialog.findViewById(R.id.errorDialogVerificationImg);
        validation_textview_status = dialog.findViewById(R.id.validation_textview_status);
        errorDialogHeaderImg = dialog.findViewById(R.id.errorDialogHeaderImg);
        errorDialogHeaderImg.setImageResource(R.drawable.status_update_icon);
        final EditText edit_text_status_update = dialog.findViewById(R.id.edit_text_status_update);
        parent_alert = dialog.findViewById(R.id.parent_alert);
        parent_alert.setBackgroundResource(R.drawable.square_new_theme_green);


        update_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final JSONObject jsonObject = new JSONObject();
                updated_status_msg = edit_text_status_update.getText().toString();
                if (updated_status_msg.isEmpty()) {
                    validation_textview_status.setVisibility(view.VISIBLE);
                    Animation animShake = AnimationUtils.loadAnimation(HomeScreen.this, R.anim.shake);
                    errorDialogVerificationImg.startAnimation(animShake);
                    errorDialogVerificationImg.setImageResource(R.drawable.new_theme_input_border);
                    errorDialogVerificationImg.setColorFilter(ContextCompat.getColor(HomeScreen.this, R.color.colorRed), PorterDuff.Mode.SRC_IN);

                } else {
                    try {
                        jsonObject.put("statusMsg", updated_status_msg);
                        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
                        prefEditor55.putString("statusMsg", updated_status_msg);
                        prefEditor55.apply();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSocket.emit("update status", jsonObject);
                    mSocket.on("update status response", update_status_response);
                    dialog.dismiss();
                }
            }

        });
        cancel_update_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    private void click_listeners() {
        dp_editor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkingUserConnectionState();
                if (UserConnectionState.equals("offline")) {
                    ShowingDisconnectedUserPopup();
                } else {
                    if (CheckPermissions()) {

                        if (ActivityCompat.checkSelfPermission(HomeScreen.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        } else {

                            openGallery();
                        }
                    } else {
                        requestPermissions();
                    }
                    hidingSettingMenu();
                }
            }
        });
        coins_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hidingSettingMenu();
                stoppingMediaPlayerOfAudioShouts();
                homeScreenCoinsTv.setTextColor(getResources().getColor(R.color.black));
                Intent intent = new Intent(HomeScreen.this, Coins_details.class);
                startActivity(intent);
            }
        });
        homeScreenUserStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updating_status();
            }
        });
        my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNotificationsOpen || isFriendRequestOpen || isSettingsOpen) {
                    isNotificationsOpen = false;
                    isFriendRequestOpen = false;
                    isSettingsOpen = false;
                    try {
                        if (jsonObject_notification != null)
                            notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);
                Glide.with(HomeScreen.this).load(R.drawable.friend_requests_inactive).into(homeScreenFriendRequestIcon);
                Glide.with(HomeScreen.this).load(R.drawable.notify_bell_inactive).into(homeScreenNotificationsIcon);
                Glide.with(HomeScreen.this).load(R.drawable.botton_trophy_unchecked).into(homeScreenTrophiesIcon);

                homeScreenNotificationsRl.setVisibility(View.GONE);
                homeScreenFriendRequestRl.setVisibility(View.GONE);
                homeScreenSettingsRl.setVisibility(View.GONE);

                stoppingMediaPlayerOfAudioShouts();
                Glide.with(HomeScreen.this).load(R.drawable.user_profile).into(my_profile);
                Intent intent = new Intent(HomeScreen.this, My_profile.class);
                startActivity(intent);
            }
        });

        homeScreenSettingsIcon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSettingsOpen) {
                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(v.GONE);
                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                }
                stoppingMediaPlayerOfAudioShouts();
                Intent intent = new Intent(HomeScreen.this, BuyShieldActivity.class);
                startActivity(intent);

            }
        });

        homeScreenSettingsIcon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSettingsOpen) {
                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(v.GONE);
                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                }
                stoppingMediaPlayerOfAudioShouts();
                Intent intent = new Intent(HomeScreen.this, ShopActivity.class);
                startActivity(intent);

            }
        });

        homeScreenSettingsIcon5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSettingsOpen) {
                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(v.GONE);
                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                }
                stoppingMediaPlayerOfAudioShouts();
                Intent intent = new Intent(HomeScreen.this, MyWebView.class);
                intent.putExtra("webview", 3);
                startActivity(intent);

            }
        });

    }

    private void set_Values() {
        String singleton_username, singleton_role, singleton_dp, singleton_statusMsg, singleton_status, singleton_sID, singleton_presence, singleton_age, singleton_gender, singleton_coins;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        singleton_username = prefs.getString("mine_user_name", null);
        //Add AvatarView Loader
        avatarLoader.setVisibility(View.VISIBLE);
        avatarLoader.setAnimating(true);

        GlideApp.with(HomeScreen.this).load(baseUrl + mySingleTon.dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                avatarLoader.setVisibility(View.GONE);
                avatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(homeProfileIv);


//        Picasso.get().load(baseUrl + mySingleTon.dp).into(homeProfileIv, new com.squareup.picasso.Callback() {
//            @Override
//            public void onSuccess() {
//                avatarLoader.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError(Exception e) {
//                Log.e("dajhadj", e.getMessage());
//            }
//        });

        if (mySingleTon.presence.equals("0")) {
            homeProfileIv.setBorderColor(getResources().getColor(R.color.colorOffline));
        } else if (mySingleTon.presence.equals("1")) {
            homeProfileIv.setBorderColor(getResources().getColor(R.color.colorOnline));
        } else if (mySingleTon.presence.equals("2")) {
            homeProfileIv.setBorderColor(getResources().getColor(R.color.colorBusy));
        } else if (mySingleTon.presence.equals("3")) {
            homeProfileIv.setBorderColor(getResources().getColor(R.color.colorAway));
        }
        homeScreenUserName.setText(singleton_username);
        homeScreenUserStatus.setText(mySingleTon.statusMsg);
        if ((MySocketsClass.static_objects.MyTotalCoin != null) && (!MySocketsClass.static_objects.MyTotalCoin.equals(""))) {
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String formated_user_total_coins = formatter.format(Double.parseDouble(MySocketsClass.static_objects.MyTotalCoin));
            homeScreenCoinsTv.setText(formated_user_total_coins);
        } else {
            if (!mySingleTon.coins.equals("")) {
                DecimalFormat formatter = new DecimalFormat("#,###,###");
                String formated_user_total_coins = formatter.format(Double.parseDouble(mySingleTon.coins));
                homeScreenCoinsTv.setText(formated_user_total_coins);
            } else {
                homeScreenCoinsTv.setText("");
            }
            ;
        }
        avatarRole.setVisibility(View.VISIBLE);
        avatarRole.setAnimating(true);
        if (mySingleTon.role.equals("1")) {
            Glide.with(HomeScreen.this).load(R.drawable.user_icon).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }
            }).into(homeScreenRoleIcon);
        } else if (mySingleTon.role.equals("2")) {
            Glide.with(HomeScreen.this).load(R.drawable.mod_icon).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }
            }).into(homeScreenRoleIcon);
        } else if (mySingleTon.role.equals("3")) {
            Glide.with(HomeScreen.this).load(R.drawable.staff_icon).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }
            }).into(homeScreenRoleIcon);
        } else if (mySingleTon.role.equals("4")) {
            Glide.with(HomeScreen.this).load(R.drawable.mentor_icon).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }
            }).into(homeScreenRoleIcon);
        } else if (mySingleTon.role.equals("5")) {
            Glide.with(HomeScreen.this).load(R.drawable.merchant_icon).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    avatarRole.setVisibility(View.GONE);
                    return false;
                }
            }).into(homeScreenRoleIcon);
        }
        avatarAge.setVisibility(View.VISIBLE);
        avatarAge.setAnimating(true);
        if (mySingleTon.age.equals("1")) {
            if (mySingleTon.gender.equals("M")) {
                Glide.with(HomeScreen.this).load(R.drawable.born_male).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            } else {
                Glide.with(HomeScreen.this).load(R.drawable.born_female).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            }
        } else if (mySingleTon.age.equals("2")) {
            if (mySingleTon.gender.equals("M")) {
                Glide.with(HomeScreen.this).load(R.drawable.teen_male).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            } else {
                Glide.with(HomeScreen.this).load(R.drawable.teen_female).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            }
        } else if (mySingleTon.age.equals("3")) {
            if (mySingleTon.gender.equals("M")) {
                Glide.with(HomeScreen.this).load(R.drawable.young_male).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            } else {

                Glide.with(HomeScreen.this).load(R.drawable.young_female).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            }
        } else if (mySingleTon.age.equals("4")) {
            if (mySingleTon.gender.equals("M")) {
                Glide.with(HomeScreen.this).load(R.drawable.adult_male).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            } else {
                Glide.with(HomeScreen.this).load(R.drawable.adult_female).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            }
        } else if (mySingleTon.age.equals("5")) {
            if (mySingleTon.gender.equals("M")) {
                Glide.with(HomeScreen.this).load(R.drawable.mature_male).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            } else {
                Glide.with(HomeScreen.this).load(R.drawable.mature_female).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        avatarAge.setVisibility(View.GONE);
                        return false;
                    }
                }).into(homeScreenAgeIcon);
            }

        }

        homeProfileIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hidingSettingMenu();
                LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
                view = layoutInflater.inflate(R.layout.presence_dialog, null);
                homeScreenPresenceRl = view.findViewById(R.id.homeScreenPresenceRl);
                presence_online = view.findViewById(R.id.linear1);
                presence_offline = view.findViewById(R.id.linear2);
                presence_busy = view.findViewById(R.id.linear3);
                presence_away = view.findViewById(R.id.linear4);
                final android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(view.getContext());
                final android.app.AlertDialog alertDialog = adb.create();
                alertDialog.setView(view);
                homeScreenPresenceRl.setVisibility(View.VISIBLE);
                presence_online.setVisibility(View.VISIBLE);
                presence_offline.setVisibility(View.VISIBLE);
                presence_busy.setVisibility(View.VISIBLE);
                presence_away.setVisibility(View.VISIBLE);
                presence_online.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presence = 1;
                        if (mySingleTon.presence.equals(String.valueOf(presence))) {
                            alertDialog.dismiss();
                        } else {
                            presece(presence);
                            alertDialog.dismiss();
                        }

                    }
                });
                presence_offline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presence = 0;
                        if (mySingleTon.presence.equals(String.valueOf(presence))) {
                            alertDialog.dismiss();
                        } else {
                            presece(presence);
                            alertDialog.dismiss();
                        }
                    }
                });
                presence_busy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presence = 2;
                        if (mySingleTon.presence.equals(String.valueOf(presence))) {
                            alertDialog.dismiss();
                        } else {
                            presece(presence);
                            alertDialog.dismiss();
                        }
                    }
                });
                presence_away.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presence = 3;
                        if (mySingleTon.presence.equals(String.valueOf(presence))) {
                            alertDialog.dismiss();
                        } else {
                            presece(presence);
                            alertDialog.dismiss();
                        }
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void setupTabIcons() {

        Log.e("MainTest", "in setup Tab icon start Home screen");
        view1 = getLayoutInflater().inflate(R.layout.tabs_icon_layout_1, null);
        view1.findViewById(R.id.tab_ic_1).setBackgroundResource(R.drawable.icon_user_profile_unselected);
        tabs_homeScreen.getTabAt(1).setCustomView(view1);

        view2 = getLayoutInflater().inflate(R.layout.tabs_layout_icon_2, null);
        view2.findViewById(R.id.tab_ic_2).setBackgroundResource(R.drawable.icon_chatrooms_unselected);
        tabs_homeScreen.getTabAt(2).setCustomView(view2);

        view3 = getLayoutInflater().inflate(R.layout.tabs_layout_icon_3, null);
        view3.findViewById(R.id.tab_ic_3).setBackgroundResource(R.drawable.icon_chats_unselected);
        MySocketsClass.static_objects.FragmentNewMessageIcon = view3.findViewById(R.id.fragment_new_message_icon);
        tabs_homeScreen.getTabAt(3).setCustomView(view3);

        view4 = getLayoutInflater().inflate(R.layout.tabs_layout_icon_4, null);
        view4.findViewById(R.id.tab_ic_4).setBackgroundResource(R.drawable.icon_home_selected);
        tabs_homeScreen.getTabAt(0).setCustomView(view4);

        Log.e("MainTest", "in setup Tab icon end Home screen");
    }

    private void setUpViewPager(ViewPager matches_viewpager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "");
        adapter.addFragment(new ContactsFragment(), "");
        adapter.addFragment(new ChatRoomsFragment(), "");
        adapter.addFragment(new ChatsFragment(), "");
        matches_viewpager.setAdapter(adapter);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if (tabs_homeScreen.getSelectedTabPosition() == 1) {
            MySocketsClass.static_objects.FragmentPosition = 1;
            stoppingMediaPlayerOfAudioShouts();

            view1.findViewById(R.id.tab_ic_1).setBackgroundResource(R.drawable.user_active);
            tabs_homeScreen.getTabAt(1).setCustomView(view1);


            view2.findViewById(R.id.tab_ic_2).setBackgroundResource(R.drawable.icon_chatrooms_unselected);
            tabs_homeScreen.getTabAt(2).setCustomView(view2);

            view3.findViewById(R.id.tab_ic_3).setBackgroundResource(R.drawable.icon_chats_unselected);
            MySocketsClass.static_objects.FragmentNewMessageIcon = view3.findViewById(R.id.fragment_new_message_icon);
            tabs_homeScreen.getTabAt(3).setCustomView(view3);

            view4.findViewById(R.id.tab_ic_4).setBackgroundResource(R.drawable.icon_home_unselected);
            tabs_homeScreen.getTabAt(0).setCustomView(view4);

            hidingSettingMenu();

        } else if (tabs_homeScreen.getSelectedTabPosition() == 2) {
            MySocketsClass.static_objects.FragmentPosition = 2;
            stoppingMediaPlayerOfAudioShouts();

            view1.findViewById(R.id.tab_ic_1).setBackgroundResource(R.drawable.icon_user_profile_unselected);
            tabs_homeScreen.getTabAt(1).setCustomView(view1);


            view2.findViewById(R.id.tab_ic_2).setBackgroundResource(R.drawable.icon_users_active);
            tabs_homeScreen.getTabAt(2).setCustomView(view2);

            view3.findViewById(R.id.tab_ic_3).setBackgroundResource(R.drawable.icon_chats_unselected);
            MySocketsClass.static_objects.FragmentNewMessageIcon = view3.findViewById(R.id.fragment_new_message_icon);
            tabs_homeScreen.getTabAt(3).setCustomView(view3);

            view4.findViewById(R.id.tab_ic_4).setBackgroundResource(R.drawable.icon_home_unselected);
            tabs_homeScreen.getTabAt(0).setCustomView(view4);
            hidingSettingMenu();


        } else if (tabs_homeScreen.getSelectedTabPosition() == 3) {
            MySocketsClass.static_objects.FragmentPosition = 3;
            stoppingMediaPlayerOfAudioShouts();

            view1.findViewById(R.id.tab_ic_1).setBackgroundResource(R.drawable.icon_user_profile_unselected);
            tabs_homeScreen.getTabAt(1).setCustomView(view1);

            view2.findViewById(R.id.tab_ic_2).setBackgroundResource(R.drawable.icon_chatrooms_unselected);
            tabs_homeScreen.getTabAt(2).setCustomView(view2);

            view3.findViewById(R.id.tab_ic_3).setBackgroundResource(R.drawable.icon_chats_active);
            MySocketsClass.static_objects.FragmentNewMessageIcon = view3.findViewById(R.id.fragment_new_message_icon);
            MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.GONE);
            MySocketsClass.static_objects.OfflineMessagesRedDotView = "gone";
            tabs_homeScreen.getTabAt(3).setCustomView(view3);

            view4.findViewById(R.id.tab_ic_4).setBackgroundResource(R.drawable.icon_home_unselected);
            tabs_homeScreen.getTabAt(0).setCustomView(view4);

            hidingSettingMenu();
            ChattingIconRedDot();
        } else if (tabs_homeScreen.getSelectedTabPosition() == 0) {
            MySocketsClass.static_objects.FragmentPosition = 0;

            view1.findViewById(R.id.tab_ic_1).setBackgroundResource(R.drawable.icon_user_profile_unselected);
            tabs_homeScreen.getTabAt(1).setCustomView(view1);


            view2.findViewById(R.id.tab_ic_2).setBackgroundResource(R.drawable.icon_chatrooms_unselected);
            tabs_homeScreen.getTabAt(2).setCustomView(view2);

            view3.findViewById(R.id.tab_ic_3).setBackgroundResource(R.drawable.icon_chats_unselected);
            MySocketsClass.static_objects.FragmentNewMessageIcon = view3.findViewById(R.id.fragment_new_message_icon);
            tabs_homeScreen.getTabAt(3).setCustomView(view3);

            view4.findViewById(R.id.tab_ic_4).setBackgroundResource(R.drawable.icon_home_selected);
            tabs_homeScreen.getTabAt(0).setCustomView(view4);
            hidingSettingMenu();


        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    private void DeletingPreferencesForUserNameAndPaswrd() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingPreferences.remove("mine_user_name");
        DeletingPreferences.remove("mine_password");
        DeletingPreferences.apply();
    }

    private void DeletingPreferencesForNotifications() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingPreferences.remove("MuteNotificationStats");
        DeletingPreferences.apply();
    }

    private void DeletingPreferenceForEmailAndSpecialId() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingPreferences.remove("userEmail");
        DeletingPreferences.remove("userSpecialId");
        DeletingPreferences.apply();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.homeScreenSettingsIcon: {
                if (!isSettingsOpen) {
                    if (isNotificationsOpen || isFriendRequestOpen) {
                        isNotificationsOpen = false;
                        isFriendRequestOpen = false;
                        try {
                            if (jsonObject_notification != null)
                                notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Glide.with(HomeScreen.this).load(R.drawable.icon_settings_active).into(homeScreenSettingsIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.friend_requests_inactive).into(homeScreenFriendRequestIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.notify_bell_inactive).into(homeScreenNotificationsIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.user_profile_inactive).into(my_profile);
                    Glide.with(HomeScreen.this).load(R.drawable.botton_trophy_unchecked).into(homeScreenTrophiesIcon);

                    isSettingsOpen = true;
                    homeScreenNotificationsRl.setVisibility(View.GONE);
                    homeScreenFriendRequestRl.setVisibility(View.GONE);

                    homeScreenSettingsRl.bringToFront();
                    homeScreenSettingsRl.setVisibility(View.VISIBLE);
                    homeScreenSettingsIcon1.setVisibility(View.VISIBLE);
                    homeScreenSettingsIcon2.setVisibility(View.VISIBLE);
                    homeScreenSettingsIcon3.setVisibility(View.VISIBLE);
                    homeScreenSettingsIcon5.setVisibility(View.VISIBLE);
                    homeScreenSettingsIcon4.setVisibility(View.VISIBLE);
                    homeScreenSettingsIcon1.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_55));
                    homeScreenSettingsIcon1View.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_55));
                    homeScreenSettingsIcon2.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_105));
                    homeScreenSettingsIcon2View.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_105));
                    homeScreenSettingsIcon3.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_155));
                    homeScreenSettingsIcon3View.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_155));
                    homeScreenSettingsIcon5.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_205));
                    homeScreenSettingsIcon5View.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_205));
                    homeScreenSettingsShareIcon.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_255));
                    homeScreenSettingsShareIconView.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_255));
                    homeScreenSettingsIcon4.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_305));
                    homeScreenSettingsIcon4View.animate().translationY(HomeScreen.this.getResources().getDimension(R.dimen.standard_305));

                } else {
                    Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);

                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(View.GONE);

                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                }
                break;
            }
            case R.id.homeScreenNotificationsIcon: {
                if (!isNotificationsOpen) {
                    if (isSettingsOpen || isFriendRequestOpen) {
                        isSettingsOpen = false;
                        isFriendRequestOpen = false;
                    }

                    mSocket.emit(MySocketsClass.EmmittersClass.LOAD_NOTIFICATION);
                    mSocket.on(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP, loadNotifications);
                    mSocket.emit(MySocketsClass.EmmittersClass.READ_ALL_NOTIFICATIONS);

                    isNotificationsOpen = true;
                    MySocketsClass.static_objects.homeScreenNotificationTv.setVisibility(View.GONE);
                    MySocketsClass.static_objects.TotalNotificationsInsideApp = 0;
                    homeScreenSettingsRl.setVisibility(View.GONE);
                    homeScreenFriendRequestRl.setVisibility(View.GONE);

                    homeScreenNotificationsRl.setVisibility(View.VISIBLE);
                    homeScreenNotificationsRl.bringToFront();
                    Glide.with(HomeScreen.this).load(R.drawable.icon_notify_bell_active).into(homeScreenNotificationsIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.friend_requests_inactive).into(homeScreenFriendRequestIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.user_profile_inactive).into(my_profile);
                    Glide.with(HomeScreen.this).load(R.drawable.botton_trophy_unchecked).into(homeScreenTrophiesIcon);
                    MySocketsClass.static_objects.TotalNotificationsInsideApp = 0;
                    try {
                        if (jsonObject_notification != null)
                            notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    isNotificationsOpen = false;
                    homeScreenNotificationsRl.setVisibility(View.GONE);
                    try {
                        if (jsonObject_notification != null)
                            notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Glide.with(HomeScreen.this).load(R.drawable.notify_bell_inactive).into(homeScreenNotificationsIcon);

                }
                break;
            }
            case R.id.homeScreenFriendRequestIcon: {
                if (!isFriendRequestOpen) {
                    if (isNotificationsOpen || isSettingsOpen) {
                        isSettingsOpen = false;
                        isNotificationsOpen = false;
                        try {
                            if (jsonObject_notification != null)
                                notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    isFriendRequestOpen = true;
                    homeScreenNotificationsRl.setVisibility(View.GONE);
                    try {
                        if (jsonObject_notification != null)
                            notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    homeScreenSettingsRl.setVisibility(View.GONE);
                    homeScreenFriendRequestRl.setVisibility(View.VISIBLE);
                    homeScreenFriendRequestRl.bringToFront();
                    Glide.with(HomeScreen.this).load(R.drawable.icon_friend_requests).into(homeScreenFriendRequestIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.notify_bell_inactive).into(homeScreenNotificationsIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);
                    Glide.with(HomeScreen.this).load(R.drawable.user_profile_inactive).into(my_profile);
                    Glide.with(HomeScreen.this).load(R.drawable.botton_trophy_unchecked).into(homeScreenTrophiesIcon);

                } else {
                    hidingfrndRequestLayout();
                }
                break;
            }

            case R.id.homeScreenSettingsIcon4: {
                if (isSettingsOpen) {
                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(view.GONE);
                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                    try {
                        if (jsonObject_notification != null)
                            notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                stoppingMediaPlayerOfAudioShouts();
                final Dialog dialogLogout;
                dialogLogout = new Dialog(this);
                dialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogLogout.setCancelable(true);
                dialogLogout.setContentView(R.layout.statusmsg_dialog);
//                if (MySocketsClass.static_objects.theme == 0) {
//                    dialogLogout.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                } else if (MySocketsClass.static_objects.theme == 1) {
//                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                    Date systemDate = Calendar.getInstance().getTime();
//                    String myDate = sdf.format(systemDate);
//                    try {
//                        Date Date1 = sdf.parse(myDate);
//                        Date Date2 = sdf.parse(AutoThemeTime);
//                        if (Date1.getTime() < Date2.getTime()) {
//                            dialogLogout.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                        } else if (Date1.getTime() > Date2.getTime()) {
//                            dialogLogout.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                        }
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                } else if (MySocketsClass.static_objects.theme == 2) {
                dialogLogout.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //               }
                cancel_update_status = dialogLogout.findViewById(R.id.cancel_update_status);
                cancel_update_status.setBackground(ContextCompat.getDrawable(dialogLogout.getContext(), R.drawable.error_dialog_gray_btn));
                update_status = dialogLogout.findViewById(R.id.update_status);
                update_status.setBackground(ContextCompat.getDrawable(dialogLogout.getContext(), R.drawable.error_dialog_red_btn));
                errorDialogVerificationImg = dialogLogout.findViewById(R.id.errorDialogVerificationImg);
                errorDialogVerificationImg.setVisibility(View.INVISIBLE);
                validation_textview_status = dialogLogout.findViewById(R.id.validation_textview_status);
                final EditText edit_text_status_update = dialogLogout.findViewById(R.id.edit_text_status_update);
                edit_text_status_update.setVisibility(View.GONE);
                errorDialogHeaderImg = dialogLogout.findViewById(R.id.errorDialogHeaderImg);
                errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                errorDialogHeaderImg.getLayoutParams().width = 160;
                errorDialogHeaderImg.getLayoutParams().height = 160;
                errorDialogHeaderImg.setScaleType(ImageView.ScaleType.FIT_XY);
                validation_textview_status.setVisibility(View.VISIBLE);
                validation_textview_status.setText("Are you sure to logout?");
                validation_textview_status.setTextColor(getResources().getColor(R.color.black));
                validation_textview_status.setTextSize(20);

                cancel_update_status.setText("Cancel");
                update_status.setText("Logout");
                parent_alert = dialogLogout.findViewById(R.id.parent_alert);
                parent_alert.setBackgroundResource(R.drawable.square_full_body_red_popups);
                update_status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogLogout.dismiss();
                        stopService(new Intent(HomeScreen.this, Foreground_service.class));
                        mSocket.emit("logout");
                        mSocket.disconnect();
                        if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
                            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
                        }
                        is_user_connected = 0;
                        MySocketsClass.static_objects.MyTotalCoin = "";
                        warfareDatabase.myDataBaseAccessObject().deleteAllData();
                        dataBase_chatRooms.chatRoomDBAO().deleteAllDataChatRoom();
                        DeletingPreferencesForUserNameAndPaswrd();
                        DeletingPreferenceForEmailAndSpecialId();
                        DeletingPreferencesForNotifications();
                        MySocketsClass.getInstance().deletingPreferencesForNotifications(HomeScreen.this);
                        AllNotificationsClear();
                        one_time_emit = 1;
                        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
                        prefuseronline.putString("user_online", "offline");
                        prefuseronline.apply();
                        ChattingIconRedDot();
                        DeltinTokenPreferences();
                        DeltinServicePreferences();
                        enableBroadcastReceiver();
                        MySocketsClass.static_objects.NewUserMessageList.clear();
                        MySocketsClass.static_objects.hashMap.clear();
                        DeletingPreferencesForDisconnectionBar();
                        // DeletingPreferenceForHostBase();
                        Intent intent = new Intent(HomeScreen.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Log.e("LogoutIssue", "activity");
                    }
                });
                cancel_update_status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogLogout.dismiss();
                    }
                });

                dialogLogout.show();
                break;
            }
            case R.id.homeScreenSettingsIcon1: {
                if (isSettingsOpen) {
                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(view.GONE);
                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                }
                stoppingMediaPlayerOfAudioShouts();
                Intent intent = new Intent(HomeScreen.this, MoreSettings.class);
                startActivity(intent);
                break;
            }
            case R.id.homeScreenSettingsIcon2: {
                if (isSettingsOpen) {
                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(view.GONE);
                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                }
                homeScreenSettingsIcon2.bringToFront();
                Intent intent = new Intent(HomeScreen.this, BuyShieldActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.homeScreenSettingsIcon3: {
                if (isSettingsOpen) {
                    isSettingsOpen = false;
                    homeScreenSettingsRl.setVisibility(view.GONE);
                    homeScreenSettingsIcon1.animate().translationY(0);
                    homeScreenSettingsIcon1View.animate().translationY(0);
                    homeScreenSettingsIcon2.animate().translationY(0);
                    homeScreenSettingsIcon2View.animate().translationY(0);
                    homeScreenSettingsIcon3.animate().translationY(0);
                    homeScreenSettingsIcon3View.animate().translationY(0);
                    homeScreenSettingsIcon5.animate().translationY(0);
                    homeScreenSettingsIcon5View.animate().translationY(0);
                    homeScreenSettingsShareIcon.animate().translationY(0);
                    homeScreenSettingsShareIconView.animate().translationY(0);
                    homeScreenSettingsIcon4.animate().translationY(0);
                    homeScreenSettingsIcon4View.animate().translationY(0);
                }
                homeScreenSettingsIcon2.bringToFront();
                Intent intent = new Intent(HomeScreen.this, ShopActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void requests(JSONArray jsonArray, int index, int type) {

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);

        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public void onBackPressed() {

        if (isNotificationsOpen) {
            hidingnotificationLayout();
        } else if (isSettingsOpen) {
            isSettingsOpen = false;
            Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);
            homeScreenSettingsRl.setVisibility(View.GONE);
        } else if (isFriendRequestOpen) {
            hidingfrndRequestLayout();
        } else {
            if (tabs_homeScreen.getSelectedTabPosition() != 0) {

                homeScreenViewPager.setCurrentItem(0);

            } else {
//                if (MySocketsClass.static_objects.shoutsCategryOpened != null &&
//                        MySocketsClass.static_objects.shoutsCategryOpened.equalsIgnoreCase("public")) {
//                    MySocketsClass.static_objects.shoutsCategryOpened = null;
//                    moveTaskToBack(true);
//                    finishAffinity();
//                } else {
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(HomeScreen.this);
                    Intent intent = new Intent("ClozingSocio");
                    localBroadcastManager.sendBroadcast(intent);
 //               }
            }

        }
    }

    private void hidingfrndRequestLayout() {
        isFriendRequestOpen = false;
        homeScreenFriendRequestRl.setVisibility(View.GONE);
        Glide.with(HomeScreen.this).load(R.drawable.friend_requests_inactive).into(homeScreenFriendRequestIcon);

    }

    private void hidingnotificationLayout() {
        if (isNotificationsOpen) {
            isNotificationsOpen = false;
            MySocketsClass.static_objects.homeScreenNotificationTv.setVisibility(View.GONE);
            Glide.with(HomeScreen.this).load(R.drawable.notify_bell_inactive).into(homeScreenNotificationsIcon);
            homeScreenNotificationsRl.setVisibility(View.GONE);
        }
    }

    public void hidingSettingMenu() {
        if (isSettingsOpen) {
            Glide.with(HomeScreen.this).load(R.drawable.settings_icon_inactive).into(homeScreenSettingsIcon);

            isSettingsOpen = false;
            homeScreenSettingsRl.setVisibility(View.GONE);

            homeScreenSettingsIcon1.animate().translationY(0);
            homeScreenSettingsIcon1View.animate().translationY(0);
            homeScreenSettingsIcon2.animate().translationY(0);
            homeScreenSettingsIcon2View.animate().translationY(0);
            homeScreenSettingsIcon3.animate().translationY(0);
            homeScreenSettingsIcon3View.animate().translationY(0);
            homeScreenSettingsIcon5.animate().translationY(0);
            homeScreenSettingsIcon5View.animate().translationY(0);
            homeScreenSettingsShareIcon.animate().translationY(0);
            homeScreenSettingsShareIconView.animate().translationY(0);
            homeScreenSettingsIcon4.animate().translationY(0);
            homeScreenSettingsIcon4View.animate().translationY(0);
        }
    }

    public void presece(int presence) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("presence", presence);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("change presence", jsonObject);
        mSocket.on("change presence response", presence_response);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("imagePath", "onActivityResult");
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
                String username_service = prefs.getString("mine_user_name", null);
                String Socket_ID = prefs.getString("sID", null);

                avatarLoader.setVisibility(View.VISIBLE);
                avatarLoader.setAnimating(true);

                List<MultipartBody.Part> parts = new ArrayList<>();
                parts.add(prepareFilePart("image", getRealPathFromURI(getImageUri(HomeScreen.this, loadBitmap(resultUri.toString())))));

                ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                Call<ProfileImage> call = apiInterface.uploadImage(parts, createPartFromString(username_service),
                        createPartFromString("upload_dp"), createPartFromString(Socket_ID));
                call.enqueue(new Callback<ProfileImage>() {
                    @Override
                    public void onResponse(Call<ProfileImage> call, Response<ProfileImage> response) {

                        ProfileImage profileImage = response.body();
                        final String ImageStatus = profileImage.getMessage();
                        final String URL = profileImage.getUrl();
                        int Success = profileImage.getSuccess();

                        if (Success == 1) {
                            Glide.with(HomeScreen.this).load(baseUrl + URL).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    avatarLoader.setVisibility(View.GONE);
                                    Snackbar.make(parent_layout_home, ImageStatus, Snackbar.LENGTH_LONG).show();
                                    SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
                                    prefEditor55.putString("dp", URL);
                                    prefEditor55.apply();
                                    mySingleTon.dp = URL;
                                    //broadcasting qk shouts me dp change krni user ki...
                                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(HomeScreen.this);
                                    Intent intent = new Intent("ChangingDp");
                                    intent.putExtra("newDpUrl", URL);
                                    localBroadcastManager.sendBroadcast(intent);
                                    return false;
                                }
                            }).into(homeProfileIv);
                        } else {
                            avatarLoader.setVisibility(View.GONE);
                            Snackbar.make(parent_layout_home, ImageStatus, Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfileImage> call, Throwable t) {

                        Toast.makeText(HomeScreen.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        avatarLoader.setVisibility(View.GONE);

                    }
                });

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    private void openGallery() {

        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .build());

        Album.image(this)
                .singleChoice()
                .camera(false)
                .columnCount(3)
                .onResult(result -> {
                    if (result.size() > 0) {
                        Log.e("selector", result.toString());
                        String ImagePath = result.get(0).getPath();
//                        String filePath;
//
//                        if (selectedImage.contains(".gif")) {
//                            filePath = selectedImage;
//                        } else {
//                            filePath = compressImage(selectedImage);
//                        }
                        Uri selectedImage = Uri.parse("file://" + ImagePath);
                        Log.e("imagePath", selectedImage.toString());
                        // start cropping activity for pre-acquired image saved on the device
                        CropImage.activity(selectedImage)
                                .start(this);
                    }
                })
                .start();
    }

    public Bitmap loadBitmap(String url) {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String fileUri) {

        File file = new File(fileUri);

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        file
                );

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public String getPath(Uri uri) {
        int column_index;
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    private BroadcastReceiver hidingAllLayouts = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getStringExtra("LayoutType").equalsIgnoreCase("NotUpperLayout")) {
                hidingfrndRequestLayout();
                hidingnotificationLayout();
                hidingSettingMenu();
            } else if (intent.getStringExtra("LayoutType").equalsIgnoreCase("hideLayout")) {
                if (!forcefullyHideUpperLayout) {
                    hidingUpperLayout();
                }
            } else if (intent.getStringExtra("LayoutType").equalsIgnoreCase("ShowLayout")) {
                if (!forcefullyHideUpperLayout) {
                    showingUpperLayout();
                }
            }
        }
    };

    public static boolean isServiceRunningInForeground(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    public void ShowingDisconnectedUserPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);
        builder.setTitle("Disconnected");
        builder.setMessage("You are disconnected! Move to Login Screen");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mSocket.disconnect();
                HomeScreen.this.stopService(new Intent(HomeScreen.this, Foreground_service.class));
                is_user_connected = 0;
                Intent intent = new Intent(HomeScreen.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
                prefEditor55.putString("mine_user_name", null);
                prefEditor55.apply();
                one_time_emit = 1;
                SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
                prefuseronline.putString("user_online", "");
                prefuseronline.apply();
            }
        });
        builder.create().show();
    }

    public void checkingUserConnectionState() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        UserConnectionState = prefs.getString("user_online", null);
    }

    private void CheckingNewMessageRedDot() {
        SharedPreferences checkingRedDot = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        String IsRedDot = checkingRedDot.getString("chattingredDot", null);
        Log.e("redDot", "Dot outside" + MySocketsClass.static_objects.OfflineMessagesRedDotView);
        if (IsRedDot != null && IsRedDot.equals("newMessage") || MySocketsClass.static_objects.OfflineMessagesRedDotView.equals("visible")) {
            MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);
            Log.e("redDot", "Dot " + MySocketsClass.static_objects.OfflineMessagesRedDotView);
        }
    }

    private void ChattingIconRedDot() {
        SharedPreferences.Editor chattingredDot = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        chattingredDot.putString("chattingredDot", "no_newMsg");
        chattingredDot.apply();
    }
    private void TotalNotifications() {
        MySocketsClass.static_objects.TotalNotification++;
        Log.e("abcdef", "homescreen_totalNotification");
        SharedPreferences.Editor chattingredDot = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        chattingredDot.putInt("Notification", MySocketsClass.static_objects.TotalNotification);
        chattingredDot.apply();
    }

    private void checkingTotalNotifications() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        TotalNotificationOnResume = prefs.getInt("Notification", 0);
    }

    private void WakeLocking() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myapp:mywakelocktag");
        wl.acquire();
    }

    private void getAppTheme() {
//        int theme2;
//        SharedPreferences Themeprefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
//        theme2 = Integer.parseInt(Themeprefs.getString("theme", null));
//        mySingleTon.theme = String.valueOf(theme2);
//        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
//        prefEditor55.putString("theme", String.valueOf(theme2));
//        prefEditor55.apply();
        //    MySocketsClass.static_objects.theme = theme2;
        //        if (theme2 == 0) {
        //            parent_layout_home.setBackgroundResource(R.drawable.main_background);
        //            homeScreenHeader.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        //            homeScreenHeader1.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        //            homeScreenViewPager.setBackgroundColor(getResources().getColor(R.color.colorBlackTransparent));
        //            homeScreenAppbar.setBackgroundColor(getResources().getColor(R.color.colorBlackTransparent));
        //            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        //                homeScreenAppbar.setOutlineProvider(null);
        //            }
        //
        //        } else if (theme2 == 1) {
        //            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        //            Date systemDate = Calendar.getInstance().getTime();
        //            String myDate = sdf.format(systemDate);
        //            try {
        //                Date Date1 = sdf.parse(myDate);
        //                Date Date2 = sdf.parse(AutoThemeTime);
        //                if (Date1.getTime() < Date2.getTime()) {
        //                    parent_layout_home.setBackgroundResource(R.drawable.socio_background);
        //                    homeScreenHeader.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent));
        //                    homeScreenHeader1.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent));
        //                    homeScreenViewPager.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent));
        //                    homeScreenAppbar.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent));
        //                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        //                        homeScreenAppbar.setOutlineProvider(null);
        //                    }
        //                } else {
        //
        //                    parent_layout_home.setBackgroundResource(R.drawable.main_background);
        //                    homeScreenHeader.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        //                    homeScreenHeader1.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        //                    homeScreenViewPager.setBackgroundColor(getResources().getColor(R.color.colorBlackTransparent));
        //                    homeScreenAppbar.setBackgroundColor(getResources().getColor(R.color.colorBlackTransparent));
        //                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        //                        homeScreenAppbar.setOutlineProvider(null);
        //                    }
        //                }
        //            } catch (ParseException e) {
        //                e.printStackTrace();
        //            }
        //
        //
        //        } else if (theme2 == 2) {
        //    parent_layout_home.setBackgroundResource(R.drawable.socio_background);
        homeScreenHeader.setBackgroundColor(getResources().getColor(R.color.new_theme_gray_Tranparent));
        //  homeScreenHeader.setBackgroundResource(R.drawable.square_main_page_upper_curves);
        //  homeScreenHeader1.setBackgroundColor(getResources().getColor(R.color.colorOrange));
        homeScreenHeader1.setBackgroundResource(R.drawable.square_main_page_lowercurves);
        homeScreenViewPager.setBackgroundColor(getResources().getColor(R.color.new_theme_gray_Tranparent));
        //  homeScreenAppbar.setBackgroundColor(getResources().getColor(R.color.new_theme_gray_Tranparent));
        homeScreenAppbar.setBackgroundResource(R.drawable.square_main_page_upper_curves);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            homeScreenAppbar.setOutlineProvider(null);
        }
    }

    private void disableBroadcastReceiver() {
        ComponentName receiver = new ComponentName(HomeScreen.this, AlarmReceiver.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void enableBroadcastReceiver() {
        ComponentName receiver = new ComponentName(this, AlarmReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void OpeningrecentChatsTab() {
        if (getIntent().getAction() != null && (getIntent().getAction().equals("OpenChatTab"))) {
            homeScreenViewPager.setCurrentItem(3);
        }
    }

    private void AllNotificationsClear() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void savingUserPreferenceOnline() {
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        prefuseronline.putString("user_online", "online");
        prefuseronline.apply();
    }

    private void ShowingDisconnectionBar() {
        MySocketsClass.static_objects.DisconnectionTextview = MySocketsClass.custom_layout.DisconnectionSnackbar(HomeScreen.this, MySocketsClass.static_objects.global_layout);
    }

    private String GettingInternetStateForDisconnectionBar() {
        String MyInternetState;
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        MyInternetState = GettingInternetState.getString("UseropenState", "");
        return MyInternetState;
    }

    private void DeletingPreferencesForDisconnectionBar() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingPreferences.remove("UseropenState");
        DeletingPreferences.apply();
    }

    private void DeletingPreferenceForHostBase() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingPreferences.remove("MainBaseURL");
        DeletingPreferences.apply();
    }

    private void SavingPreferenceForPrecense(String Value) {
        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        prefEditor55.putString("presence", Value);
        prefEditor55.apply();
    }

    private void DeletingPreferencesForAnotherLocationLogin() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingPreferences.remove("AnotherLocationPreference");
        DeletingPreferences.apply();
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private void performCrop(Uri picUri) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, 2);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    Uri getImageUri2(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void sharing() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Socio");
            String sAux = "Socio Text here.\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Choose one"));
        } catch (Exception e) {
            Toast.makeText(HomeScreen.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean CheckPermissions() {
        int writeStoragePermissionResult = ContextCompat.checkSelfPermission(HomeScreen.this, WRITE_EXTERNAL_STORAGE);
        int readStoragePermissionResult = ContextCompat.checkSelfPermission(HomeScreen.this, READ_EXTERNAL_STORAGE);
        int micPermissionResult = ContextCompat.checkSelfPermission(HomeScreen.this, RECORD_AUDIO);
        return writeStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                readStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                micPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(HomeScreen.this, new String[]{
                WRITE_EXTERNAL_STORAGE,
                READ_EXTERNAL_STORAGE,
                RECORD_AUDIO,

        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                    boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (write_str && read_str && record_str) {

                        Toast.makeText(HomeScreen.this, "Permission Granted", Toast.LENGTH_LONG).show();
                        //  D ownloadingImage();
                    } else {
                        Toast.makeText(HomeScreen.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }

    private void DeltinTokenPreferences() {
        SharedPreferences.Editor DeletingTokenPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingTokenPreferences.remove("MyTokenValue");
        DeletingTokenPreferences.apply();
    }

    private void DeltinServicePreferences() {
        SharedPreferences.Editor DeletingTokenPreferences = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
        DeletingTokenPreferences.remove("ServiceStats");
        DeletingTokenPreferences.apply();
    }

    private String checkingRunningServiceStatus() {
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        return GettingInternetState.getString("ServiceStats", "NotRunForegroundService");
    }

    private void checkingServiceTypeAndStartingService() {
        String serviceTypeToRun = checkingRunningServiceStatus();
        Intent intent = new Intent(HomeScreen.this, Foreground_service.class);
        if (serviceTypeToRun.equals("NotRunForegroundService")) {
            intent.putExtra("ServiceType", "SimpleService");
        } else {
            intent.putExtra("ServiceType", "ForeGroundService");
        }
        startService(intent);
    }

    private void stoppingMediaPlayerOfAudioShouts() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(HomeScreen.this);
        Intent intent = new Intent("StopShoutsAudioMediaPlayer");
        localBroadcastManager.sendBroadcast(intent);
    }

//    private void loadingVideoAd(Context context) {
//
//        Log.e("AdTest", "onVideoAdLoadCalled");
//
//        if (MySocketsClass.static_objects.rewardedAd == null || !MySocketsClass.static_objects.rewardedAd.isLoaded()) {
//
//            Log.e("AdTest", "onVideoAdLoaded");
//
//            MySocketsClass.static_objects.rewardedAd = new RewardedAd(context, context.getResources().getString(R.string.test_reward_ad_id));
//            RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
//                @Override
//                public void onRewardedAdLoaded() {
//                    Log.e("AdTest", "onGoogleVideoAdLoaded");
//                    ServerSideVerificationOptions options = new ServerSideVerificationOptions.Builder()
//                            .setCustomData(gettingUserName()+"|"+gettingSocketId())
//                            .build();
//                    MySocketsClass.static_objects.rewardedAd.setServerSideVerificationOptions(options);
//                }
//
//                @Override
//                public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
//                    super.onRewardedAdFailedToLoad(loadAdError);
//                    Log.e("AdTest", "onGoogleVideoAdLoadFailed");
//                }
//            };
//            MySocketsClass.static_objects.rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
//
//        } else {
//            Log.e("AdTest", MySocketsClass.static_objects.rewardedAd == null ? "reward ad is null" : "already loaded video Ad");
//        }
//    }

    private String gettingUserName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        return prefs.getString("mine_user_name", null);
    }
    private String gettingSocketId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
        return prefs.getString("sID", null);
    }


    private Emitter.Listener UpdateMyDP = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            ContactsFragment contactsFragment = new ContactsFragment();
            contactsFragment.getContacts();
        }
    };

    Emitter.Listener update_status_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject1 = (JSONObject) args[0];
                    try {
                        if (jsonObject1.getInt("success") == 0) {
                            success_updated_status = 0;
                            error_status_msg = jsonObject1.getString("message");
                            AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);

                            builder.setTitle("Error!");
                            builder.setMessage(error_status_msg);
                            builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog diag = builder.create();
                            diag.show();
                        } else if (jsonObject1.getInt("success") == 1) {
                            success_updated_status = 1;
                            homeScreenUserStatus.setText(updated_status_msg);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener loadNotifications = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            HomeScreen.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    jsonObject_notification = (JSONObject) args[0];
                    Log.e("totlNotifctn", "In home activity");

                    try {
                        notificationsArray = jsonObject_notification.getJSONArray("notifications");

                        notificationsRecycler.setAdapter(new NotificationsAdapter(notificationsArray));
                        notificationsRecycler.scrollToPosition(jsonObject_notification.getJSONArray("notifications").length() - 1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        if (!friendRequestArray.toString().contains(notificationsArray.getJSONObject(0).getString("user")))
                            friendRequestArray.put(notificationsArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSocket.off(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP);
                }
            });
        }
    };

    Emitter.Listener presence_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        presence_success = jsonObject.getInt("success");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (presence_success == 0) {
                        Toast.makeText(HomeScreen.this, "Error", Toast.LENGTH_SHORT).show();
                    } else if (presence_success == 1 && presence == 0) {
                        homeProfileIv.setBorderColor(getResources().getColor(R.color.colorOffline));
                        mySingleTon.presence = "0";
                        SavingPreferenceForPrecense("0");
                    } else if (presence_success == 1 && presence == 1) {
                        homeProfileIv.setBorderColor(getResources().getColor(R.color.colorOnline));
                        mySingleTon.presence = "1";
                        SavingPreferenceForPrecense("1");
                    } else if (presence_success == 1 && presence == 2) {
                        mySingleTon.presence = "2";
                        homeProfileIv.setBorderColor(getResources().getColor(R.color.colorBusy));
                        SavingPreferenceForPrecense("2");
                    } else if (presence_success == 1 && presence == 3) {
                        homeProfileIv.setBorderColor(getResources().getColor(R.color.colorAway));
                        mySingleTon.presence = "3";
                        SavingPreferenceForPrecense("3");
                    }
                }
            });
        }
    };

    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            HomeScreen.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username, role, dp, statusMsg, status, sID, presence, age, gender, coins;
                    try {
                        username = data.getString("username");
                        role = data.getString("role");
                        dp = data.getString("dp");
                        statusMsg = data.getString("statusMsg");
                        status = data.getString("status");
                        sID = data.getString("sID");
                        presence = data.getString("presence");
                        age = data.getString("age");
                        gender = data.getString("gender");
                        coins = data.getString("coins");

                        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this).edit();
                        prefEditor55.putString("user_name", username_service);
                        prefEditor55.putString("role", role);
                        prefEditor55.putString("dp", dp);
                        prefEditor55.putString("statusMsg", statusMsg);
                        prefEditor55.putString("status", status);
                        prefEditor55.putString("sID", sID);
                        prefEditor55.putString("presence", presence);
                        prefEditor55.putString("age", age);
                        prefEditor55.putString("gender", gender);
                        prefEditor55.putString("coins", coins);
                        prefEditor55.putString("theme", String.valueOf(theme));
                        prefEditor55.apply();

                        editor.putString("username", username);
                        mySingleTon.username = username;
                        mySingleTon.role = role;
                        mySingleTon.dp = dp;
                        mySingleTon.statusMsg = statusMsg;
                        mySingleTon.status = status;
                        mySingleTon.sID = sID;
                        mySingleTon.presence = presence;
                        mySingleTon.age = age;
                        mySingleTon.gender = gender;
                        mySingleTon.coins = coins;
//                        mySingleTon.theme = String.valueOf(theme);
                        editor.apply();
                        Log.e("ALiAhmad", "I Am Reconnected");
                    } catch (JSONException e) {
                        Log.d("me", "error  " + e.getMessage());
                        return;
                    }
                }
            });
        }
    };
}
