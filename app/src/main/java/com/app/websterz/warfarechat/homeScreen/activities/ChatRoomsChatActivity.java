package com.app.websterz.warfarechat.homeScreen.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.ChatKitClasses.MessagesListAdapter_newChatrooms;
import com.app.websterz.warfarechat.ChatKitClasses.MessagesList_chatrooms;
import com.app.websterz.warfarechat.ChatRoomPakage.ChatRoomChatModelClass;
import com.app.websterz.warfarechat.ChatRoomPakage.ChatRoomCurrentlyUsersOnlineAdapter;
import com.app.websterz.warfarechat.ChatRoomPakage.ChatRoomCurrentlyUsersOnlineViewModelClass;
import com.app.websterz.warfarechat.ChatRoomPakage.ChatRoomGiftsModelClass;
import com.app.websterz.warfarechat.ChatRoomPakage.ChatRoomGiftsRecyclerAdapter;
import com.app.websterz.warfarechat.ChatRoomPakage.MessageModelClass_ChatRoom;
import com.app.websterz.warfarechat.ChatRoomPakage.UserModelClass_ChatRoom;
import com.app.websterz.warfarechat.DataBaseChatRooms.DataBaseChatRoomTables;
import com.app.websterz.warfarechat.DataBaseChatRooms.DataBase_ChatRooms;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.SendAttacks.SendAttackActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.NewChatModelClass;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;
import static com.app.websterz.warfarechat.mySingleTon.MySocketsClass.static_objects.chatsRecyclerViewAdapter;
import static com.app.websterz.warfarechat.mySingleTon.MySocketsClass.static_objects.messagelist_imageLoader;


public class ChatRoomsChatActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    String countryName;
    ImageView emoji_display_btn, total_online_users_button, chatroom_msg_send_btn, chatroom_attack;
    MySingleTon mySingleTon;
    TextView chatsFragmentCountryNameTv, chatRoomsTotalUsersTv, chatRoomsTagLineTv, chatroom_totaluserslayout_counter;
    View view;
    ImageView gifts_display_btn, MoreOptions, ScrollToBottomImageView;
    EmojIconActions emojIcon;
    ArrayList<ChatRoomChatModelClass> chatRoomMessagesList;
    ChatRoomChatModelClass chatRoomChatModelClass;
    MessagesList_chatrooms chat_room_recycler_chats;
    RecyclerView chatroom_gifts_recyclerview;
    @SuppressLint("WrongConstant")
    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

    @SuppressLint("WrongConstant")
    RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    // ChatRoomsRecyclerChatAdapter chatRoomsRecyclerChatAdapter;
    LinearLayout chatroom_gifts_view;
    ArrayList<ChatRoomGiftsModelClass> chatRoomGiftList;
    ChatRoomGiftsModelClass chatRoomGiftsModelClass;
    ChatRoomGiftsRecyclerAdapter chatRoomGiftsRecyclerAdapter;
    RelativeLayout chatroom_currently_users_online_layout;
    RecyclerView chatroom_totaluserslayot_recyclerview;
    ChatRoomCurrentlyUsersOnlineViewModelClass chatRoomCurrentlyUsersOnlineViewModelClass;
    ArrayList<ChatRoomCurrentlyUsersOnlineViewModelClass> TotalOnlineUsersList;
    ChatRoomCurrentlyUsersOnlineAdapter chatRoomCurrentlyUsersOnlineAdapter;
    RelativeLayout ParentLayout;
    Animation rightToLeft, LefttoRight;
    public DataBase_ChatRooms dataBase_chatRooms;
    DataBaseChatRoomTables dataBaseChatRoomTables = null;
    NewChatModelClass newChatModelClass;
    String TagLineName;
    View UpperView;
    Button RejoinBtnChatRoom;
    com.app.websterz.warfarechat.ChatKitClasses.MessagesListAdapter_newChatrooms<MessageModelClass_ChatRoom> MessagesListAdapter_newChatrooms = new MessagesListAdapter_newChatrooms<>("currentappUser", messagelist_imageLoader);
    Client MyClient;
    LinearLayout chatRoomsTotalUsersLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_rooms_chat);
        Initializations();
        MyCode();

        chat_room_recycler_chats.addOnScrollListener(new MessagesList_chatrooms.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    ScrollToBottomImageView.setVisibility(View.GONE);
                } else {
                    ScrollToBottomImageView.setVisibility(View.VISIBLE);
                }
            }
        });
        ScrollToBottomImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_room_recycler_chats.scrollToPosition(0);
                ScrollToBottomImageView.setVisibility(View.GONE);
            }
        });

    }

    public void Initializations() {
        dataBase_chatRooms = Room.databaseBuilder(ChatRoomsChatActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        dataBaseChatRoomTables = new DataBaseChatRoomTables();

        ParentLayout = findViewById(R.id.parent_layout);
        MySocketsClass.static_objects.one_time_reward = "chatroom";

        emoji_display_btn = findViewById(R.id.emoji_display_btn);
        ScrollToBottomImageView = findViewById(R.id.ScrollToBottomImageView);
        MySocketsClass.static_objects.StaticEmojiconEditText = findViewById(R.id.emojicon_edit_text);
        chatRoomsTagLineTv = findViewById(R.id.chatRoomsTagLineTv);
        countryName = getIntent().getExtras().getString("countryName");
        chatRoomsTotalUsersTv = findViewById(R.id.chatRoomsTotalUsersTv);
        total_online_users_button = findViewById(R.id.total_online_users_button);
        chatsFragmentCountryNameTv = findViewById(R.id.chatsFragmentCountryNameTv);
        chat_room_recycler_chats = findViewById(R.id.chat_room_recycler_chats);
        gifts_display_btn = findViewById(R.id.gifts_display_btn);
        chatroom_gifts_view = findViewById(R.id.chatroom_gifts_view);
        chatroom_gifts_recyclerview = findViewById(R.id.chatroom_gifts_recyclerview);
        chatroom_msg_send_btn = findViewById(R.id.chatroom_msg_send_btn);
        chatRoomMessagesList = new ArrayList<>();
        mySingleTon = MySingleTon.getInstance();
        chatRoomGiftList = new ArrayList<>();
        GridLayoutManager giftslayoutmanager = new GridLayoutManager(ChatRoomsChatActivity.this, 4);
        chatRoomGiftsRecyclerAdapter = new ChatRoomGiftsRecyclerAdapter(chatRoomGiftList, chatroom_gifts_recyclerview, ChatRoomsChatActivity.this, countryName);
        chatroom_gifts_recyclerview.setLayoutManager(giftslayoutmanager);
        chatroom_gifts_recyclerview.setAdapter(chatRoomGiftsRecyclerAdapter);
        chatroom_currently_users_online_layout = findViewById(R.id.chatroom_currently_users_online_layout);
        chatroom_totaluserslayot_recyclerview = findViewById(R.id.chatroom_totaluserslayot_recyclerview);
        TotalOnlineUsersList = new ArrayList<>();
        chatroom_totaluserslayot_recyclerview.setLayoutManager(layoutManager2);
        chatroom_totaluserslayout_counter = findViewById(R.id.chatroom_totaluserslayout_counter);
        chatroom_attack = findViewById(R.id.chatroom_attack);
        rightToLeft = AnimationUtils.loadAnimation(this, R.anim.righttoleft);
        LefttoRight = AnimationUtils.loadAnimation(this, R.anim.total_users_left_to_right);
        MoreOptions = findViewById(R.id.privateChatSettingIcon);
        chatRoomsTotalUsersLinearLayout = findViewById(R.id.chatRoomsTotalUsersLinearLayout);
        UpperView = findViewById(R.id.uper_view);
        MySocketsClass.static_objects.chatRoomsDisconnectionFooter = findViewById(R.id.chatRoomsDisconnectionFooter);
        RejoinBtnChatRoom = findViewById(R.id.RejoinBtnChatRoom);
        MyClient = new Client(ChatRoomsChatActivity.this);
    }

    private void MyCode() {
        mSocket.off(MySocketsClass.ListenersClass.MESSAGE);
        mSocket.off(MySocketsClass.ListenersClass.SYSTEM_MESSAGE);
        mSocket.off(MySocketsClass.ListenersClass.USER_LEFT);
        mSocket.off(MySocketsClass.ListenersClass.USER_JOINED);
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        Intent intent = getIntent();
        TagLineName = intent.getStringExtra("TaglineName");
        emojIcon = new EmojIconActions(this, viewGroup, MySocketsClass.static_objects.StaticEmojiconEditText, emoji_display_btn);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        MySocketsClass.static_objects.StaticEmojiconEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (chatroom_currently_users_online_layout.getVisibility() == View.VISIBLE) {
                    chatroom_currently_users_online_layout.clearAnimation();
                    chatroom_currently_users_online_layout.startAnimation(LefttoRight);
                    chatroom_currently_users_online_layout.setVisibility(View.GONE);
                    MySocketsClass.static_objects.StaticEmojiconEditText.setFocusableInTouchMode(true);
                } else if (chatroom_gifts_view.getVisibility() == View.VISIBLE) {
                    chatroom_gifts_view.setVisibility(View.GONE);
                    mSocket.off(MySocketsClass.ListenersClass.LOAD_GIFTS_RESPONSE, load_gifts_response);
                    chatRoomGiftList.clear();
                    gifts_display_btn.setImageResource(R.drawable.gift_solid);
                }
                return false;
            }
        });

        chatsFragmentCountryNameTv.setText(countryName);
        MySocketsClass.static_objects.CurrrentPrivateChatRoomname = countryName;
        chatRoomsTagLineTv.setText(TagLineName);
        chatRoomMessagesList.clear();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("chatroom", countryName);
            mSocket.emit(MySocketsClass.EmmittersClass.JOIN_ROOM, jsonObject);

            mSocket.on(MySocketsClass.ListenersClass.USER_JOINED, user_joined);
            mSocket.emit(MySocketsClass.EmmittersClass.ONLINE_USER_COUNTER, jsonObject);
            mSocket.emit(MySocketsClass.EmmittersClass.GET_TOTAL_ROOM_USER, jsonObject);
            Log.e("Lasting","Emitting From Another Class->"+countryName);


            List<DataBaseChatRoomTables> TotalChatRoomsInRecentChats = MySocketsClass.getInstance().getChatRoomChats(countryName, ChatRoomsChatActivity.this);
            if (TotalChatRoomsInRecentChats.size() > 0) {
                List<DataBaseChatRoomTables> _UserChatRoomTables = MySocketsClass.getInstance().getChatRoomChats(countryName, ChatRoomsChatActivity.this);
                for (DataBaseChatRoomTables UserChatRoomTables : _UserChatRoomTables) {
                    String MessangerUsernamedb = UserChatRoomTables.getMessangerName();
                    String MessangerRoledb = UserChatRoomTables.getMessangerRank();
                    String MessangerGenderdb = UserChatRoomTables.getMessangerGender();
                    String MessangerAgedb = UserChatRoomTables.getMessangerAge();
                    String MessangerMessagedb = UserChatRoomTables.getMessangerMessage();
                    String MessangerDatedb = UserChatRoomTables.getMessangerDate();
                    String MessangerDpdb = UserChatRoomTables.getMessangerDp();
                    String MessangerChatroomNamedb = UserChatRoomTables.getMessangerChatRoomName();
                    Log.e("DeletionDb",MessangerMessagedb);

                    if (MessangerDpdb.equals("systemm_dp_msgs")) {
                        MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                new UserModelClass_ChatRoom("otherUser", MessangerChatroomNamedb, null, true),
                                MessangerMessagedb, DateConvertor(Long.valueOf(MessangerDatedb) * 1000), "system_msgs",
                                MessangerRoledb, null, MessangerAgedb), true);
                        chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                    } else {
                        if (MessangerUsernamedb.equals(mySingleTon.username)) {
                            MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("currentappUser",
                                    new UserModelClass_ChatRoom("currentappUser", MessangerChatroomNamedb, MessangerDpdb, true), MessangerMessagedb, DateConvertor(Long.valueOf(MessangerDatedb) * 1000), MessangerUsernamedb, MessangerRoledb, MessangerAgedb, MessangerGenderdb), true);
                            chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                        } else {
                            MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                    new UserModelClass_ChatRoom("otherUser", MessangerChatroomNamedb, MessangerDpdb, true), MessangerMessagedb, DateConvertor(Long.valueOf(MessangerDatedb) * 1000), MessangerUsernamedb, MessangerRoledb, MessangerAgedb, MessangerGenderdb), true);
                            chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                        }
                    }

                }

            }
            mSocket.on(MySocketsClass.ListenersClass.ROOM_USERS, room_users);
            mSocket.on(MySocketsClass.ListenersClass.MESSAGE, receiving_message);
            mSocket.on(MySocketsClass.ListenersClass.USER_LEFT, user_left);
            mSocket.on(MySocketsClass.ListenersClass.SYSTEM_MESSAGE, system_message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        chatRoomsTotalUsersLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TotalOnlineUsersView();
            }
        });
        chatroom_msg_send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageSending();
            }
        });
        gifts_display_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendingGifts();
            }
        });
        chatroom_attack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatRoomAttack();
            }
        });
        MoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   LeavingChatRoom();
                ShowingLevingChatroomPopup(v);
            }
        });
        RejoinBtnChatRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RejoiningChatRoom();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = ChatRoomsChatActivity.this;
        MySocketsClass.static_objects.global_layout = ParentLayout;
    }

    @Override
    public void onBackPressed() {
        if (chatroom_gifts_view.getVisibility() == View.VISIBLE) {
            chatroom_gifts_view.setVisibility(View.GONE);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_GIFTS_RESPONSE, load_gifts_response);
            chatRoomGiftList.clear();
            gifts_display_btn.setImageResource(R.drawable.gift_solid);
        } else if (chatroom_currently_users_online_layout.getVisibility() == View.VISIBLE) {
            chatroom_currently_users_online_layout.clearAnimation();
            chatroom_currently_users_online_layout.startAnimation(LefttoRight);
            chatroom_currently_users_online_layout.setVisibility(View.GONE);
        } else {
            MySocketsClass.static_objects.CurrrentPrivateChatRoomname = "";
            mSocket.off(MySocketsClass.ListenersClass.LOAD_CHATROOM_RESPONSE);
            MySocketsClass.static_objects.one_time_reward = "notinchatroom";
            finish();
        }
    }

    public void MessageSending() {
        if (ScrollToBottomImageView.getVisibility() == View.VISIBLE) {
            ScrollToBottomImageView.setVisibility(View.GONE);
        }
        JSONObject jsonObject = new JSONObject();
        String messageText = MySocketsClass.static_objects.StaticEmojiconEditText.getText().toString();
        MySocketsClass.static_objects.StaticEmojiconEditText.setText("");
        if (messageText.equals("")) {
            Toast.makeText(this, "Message is empty.", Toast.LENGTH_SHORT).show();
        } else {
            try {
                jsonObject.put("chatroom", countryName);
                jsonObject.put("message", messageText);
                jsonObject.put("chat_type", 1);
                mSocket.emit(MySocketsClass.EmmittersClass.SEND, jsonObject);

                if (!messageText.startsWith("/")) {
                    MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("currentappUser",
                            new UserModelClass_ChatRoom("currentappUser", countryName, mySingleTon.dp, true), messageText, DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()), mySingleTon.username, mySingleTon.role, mySingleTon.age, mySingleTon.gender), true);
                    chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                    //Saving On Database...
                    dataBaseChatRoomTables.setMessangerName(mySingleTon.username);
                    dataBaseChatRoomTables.setMessangerRank(mySingleTon.role);
                    dataBaseChatRoomTables.setMessangerGender(mySingleTon.gender);
                    dataBaseChatRoomTables.setMessangerAge(mySingleTon.age);
                    dataBaseChatRoomTables.setMessangerMessage(messageText);
                    dataBaseChatRoomTables.setMessangerDate(GettingCurrentDtaeMethod());
                    dataBaseChatRoomTables.setMessangerDp(mySingleTon.dp);
                    dataBaseChatRoomTables.setMessangerChatRoomName(countryName);
                    dataBase_chatRooms.chatRoomDBAO().addChatRoom_Chat(dataBaseChatRoomTables);


                    List<DataBaseChatRoomTables> TotalChatRoomsInRecentChats = MySocketsClass.getInstance().getChatRoomChats(countryName, ChatRoomsChatActivity.this);
                    if (TotalChatRoomsInRecentChats.size() > 200) {
                        dataBase_chatRooms.chatRoomDBAO().deleteStartRowsOfChatRoom(countryName);
                        Log.e("DeletionDb","Row deleted");
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!messageText.startsWith("/")) {
            Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
            while (MyIterator.hasNext()) {
                NewChatModelClass newChatModelClass2 = MyIterator.next();

                if (newChatModelClass2.getMessangerName().equals(countryName))
                    MyIterator.remove();
            }
            newChatModelClass = new NewChatModelClass("ChattingRoomDp", countryName, messageText, "1", 0, "ChatRoom");
            MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
            Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
            chatsRecyclerViewAdapter.notifyDataSetChanged();
        }
        messageText = null;

    }

    public void sendingGifts() {
        if (chatroom_currently_users_online_layout.getVisibility() == View.VISIBLE) {
            chatroom_currently_users_online_layout.clearAnimation();
            chatroom_currently_users_online_layout.startAnimation(LefttoRight);
            chatroom_currently_users_online_layout.setVisibility(View.GONE);
        }
        if (chatroom_gifts_view.getVisibility() == View.VISIBLE) {
            chatroom_gifts_view.setVisibility(View.GONE);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_GIFTS_RESPONSE, load_gifts_response);
            chatRoomGiftList.clear();
            gifts_display_btn.setImageResource(R.drawable.gift_solid);
        } else {
            gifts_display_btn.setImageResource(R.drawable.gift_solid_clicked);
            chatroom_gifts_view.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userid", countryName);
                jsonObject.put("type", "chatroom");
                mSocket.emit(MySocketsClass.EmmittersClass.LOAD_GIFTS, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_GIFTS_RESPONSE, load_gifts_response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void TotalOnlineUsersView() {
        if (chatroom_gifts_view.getVisibility() == View.VISIBLE) {
            chatroom_gifts_view.setVisibility(View.GONE);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_GIFTS_RESPONSE, load_gifts_response);
            chatRoomGiftList.clear();
            gifts_display_btn.setImageResource(R.drawable.gift_solid);
        }
        if (chatroom_currently_users_online_layout.getVisibility() == View.VISIBLE) {
            chatroom_currently_users_online_layout.clearAnimation();
            chatroom_currently_users_online_layout.startAnimation(LefttoRight);
            chatroom_currently_users_online_layout.setVisibility(View.GONE);
        } else {
            chatroom_currently_users_online_layout.clearAnimation();
            chatroom_currently_users_online_layout.startAnimation(rightToLeft);
            chatroom_currently_users_online_layout.setVisibility(View.VISIBLE);
        }
    }

    public String GettingCurrentDtaeMethod() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String format = tsLong.toString();
        return format;

    }

    public void ChatRoomAttack() {
        Intent intent = new Intent(ChatRoomsChatActivity.this, SendAttackActivity.class);
        intent.putExtra("chatroom", countryName);
        startActivity(intent);
    }

    private void LeavingChatRoomClicked(String LeavingCountry) {


        dataBase_chatRooms = Room.databaseBuilder(ChatRoomsChatActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        List<DataBaseChatRoomTables> totalDataChatRoomsList = MySocketsClass.getInstance().getChatRoomChats(LeavingCountry, ChatRoomsChatActivity.this);
        if (totalDataChatRoomsList.size() > 0) {
            try {
                dataBase_chatRooms.chatRoomDBAO().deleteAlreadyPresentChatRoomChat(LeavingCountry);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("chatroom", LeavingCountry);
                mSocket.emit(MySocketsClass.EmmittersClass.LEAVE_ROOM, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        MySocketsClass.static_objects.hashMap.remove(LeavingCountry);
        for (int i = 0; i < MySocketsClass.static_objects.NewMessageChat.size(); i++) {
            if (MySocketsClass.static_objects.NewMessageChat.get(i).getMessangerName().contains(MySocketsClass.static_objects.CurrrentPrivateChatRoomname)) {
                MySocketsClass.static_objects.NewMessageChat.remove(i);
                //  chatsRecyclerViewAdapter.notifyItemRemoved(i);
            }
        }
        chatsRecyclerViewAdapter.notifyDataSetChanged();
        finish();

    }

    private void ShowingLevingChatroomPopup(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.leaving_chatroom_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.leave_room:
                LeavingChatRoomClicked(countryName);
                return true;
            default:
                return false;
        }
    }

    private void RejoiningChatRoom() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("chatroom", countryName);
            mSocket.emit(MySocketsClass.EmmittersClass.JOIN_ROOM, jsonObject);
            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ChattingIconRedDot() {
        SharedPreferences.Editor chattingredDot = PreferenceManager.getDefaultSharedPreferences(ChatRoomsChatActivity.this).edit();
        chattingredDot.putString("chattingredDot", "newMessage");
        chattingredDot.apply();
    }

    private Date DateConvertor(long timeStamp) {
        Date d = new Date(timeStamp);
        return d;
    }

    Emitter.Listener user_joined = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            Log.e("ChatRoomTesting", "join");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final String username_joined = jsonObject.getString("username");
                        String username_joined_role = jsonObject.getString("role");
                        String username_joined_gender = jsonObject.getString("gender");
                        final String username_joined_chatroom = jsonObject.getString("chatroom");
                        String username_joined_message = jsonObject.getString("message");
                        String username_joined_direction = jsonObject.getString("direction");
                        String username_joined_chat_type = jsonObject.getString("chat_type");
                        String username_joined_age = jsonObject.getString("age");


                        if (countryName.equals(username_joined_chatroom)) {
                            if (chat_room_recycler_chats.canScrollVertically(1)) {
                                MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                        new UserModelClass_ChatRoom("otherUser", username_joined_chatroom, null, true),
                                        "has joined", DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()),
                                        username_joined, username_joined_role, username_joined_age, username_joined_gender), false);
                                // chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                            } else {
                                MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                        new UserModelClass_ChatRoom("otherUser", username_joined_chatroom, null, true),
                                        "has joined", DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()),
                                        username_joined, username_joined_role, username_joined_age, username_joined_gender), true);
                                chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                            }

                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {


                                Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                                while (MyIterator.hasNext()) {
                                    NewChatModelClass newChatModelClass2 = MyIterator.next();

                                    if (newChatModelClass2.getMessangerName().equals(username_joined_chatroom))
                                        MyIterator.remove();

                                }

                                if (!MySocketsClass.static_objects.hashMap.containsKey(username_joined_chatroom)) {
                                    MySocketsClass.static_objects.hashMap.put(username_joined_chatroom, 0);
                                }
                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(username_joined_chatroom))) {
                                    MySocketsClass.static_objects.hashMap.put(username_joined_chatroom,
                                            MySocketsClass.static_objects.hashMap.get(username_joined_chatroom) + 1);
                                }

                                if (MySocketsClass.static_objects.FragmentPosition == 0 &&
                                        !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(username_joined_chatroom))
                                        || MySocketsClass.static_objects.FragmentPosition == 1 &&
                                        !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(username_joined_chatroom))) {
                                    MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);
                                }
                                if (MySocketsClass.getInstance().isAppIsInBackground(ChatRoomsChatActivity.this)) {
                                    ChattingIconRedDot();
                                }
                                newChatModelClass = new NewChatModelClass("ChattingRoomDp", username_joined_chatroom, username_joined + " has joined", "10", MySocketsClass.static_objects.hashMap.get(username_joined_chatroom), "ChatRoom");

                                MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                                Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                                chatsRecyclerViewAdapter.notifyDataSetChanged();
                                if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                    MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                                }


                                List<DataBaseChatRoomTables> TotalChatRoomsInRecentChats = MySocketsClass.getInstance().getChatRoomChats(username_joined_chatroom, ChatRoomsChatActivity.this);
                                if (TotalChatRoomsInRecentChats.size() > 200) {
                                    dataBase_chatRooms.chatRoomDBAO().deleteStartRowsOfChatRoom(username_joined_chatroom);
                                    Log.e("DeletionDb","Row deleted");
                                }
                            }

                        });

                        //Saving On Database
                        dataBaseChatRoomTables.setMessangerName(username_joined);
                        dataBaseChatRoomTables.setMessangerRank(username_joined_role);
                        dataBaseChatRoomTables.setMessangerGender(username_joined_gender);
                        dataBaseChatRoomTables.setMessangerAge(username_joined_age);
                        dataBaseChatRoomTables.setMessangerMessage("has joined");
                        dataBaseChatRoomTables.setMessangerDate(GettingCurrentDtaeMethod());
                        dataBaseChatRoomTables.setMessangerDp(mySingleTon.dp);
                        dataBaseChatRoomTables.setMessangerChatRoomName(username_joined_chatroom);
                        dataBase_chatRooms.chatRoomDBAO().addChatRoom_Chat(dataBaseChatRoomTables);




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    Emitter.Listener user_left = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            Log.e("chetrommMsg","in user_left");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final String usernameleft = jsonObject.getString("username");
                        String usernameleft_role = jsonObject.getString("role");
                        String usernameleft_gender = jsonObject.getString("gender");
                        final String usernameleft_chatroom = jsonObject.getString("chatroom");
                        String usernameleft_message = jsonObject.getString("message");
                        String usernameleft_direction = jsonObject.getString("direction");
                        String usernameleft_chat_type = jsonObject.getString("chat_type");
                        String usernameleft_age = jsonObject.getString("age");

                        if (countryName.equals(usernameleft_chatroom)) {
                            if (usernameleft_message.contains("disconnected")) {

                                if (chat_room_recycler_chats.canScrollVertically(1)) {

                                    MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                            new UserModelClass_ChatRoom("otherUser", usernameleft_chatroom, null, true),
                                            "disconnected", DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()), usernameleft, usernameleft_role, usernameleft_age, usernameleft_gender), false);
                                    //  chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                                } else {
                                    MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                            new UserModelClass_ChatRoom("otherUser", usernameleft_chatroom, null, true),
                                            "disconnected", DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()), usernameleft, usernameleft_role, usernameleft_age, usernameleft_gender), true);
                                    chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                                }
                            } else {
                                if (chat_room_recycler_chats.canScrollVertically(1)) {
                                    MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                            new UserModelClass_ChatRoom("otherUser", usernameleft_chatroom, null, true),
                                            "has left", DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()), usernameleft, usernameleft_role, usernameleft_age, usernameleft_gender), false);
                                    //  chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                                } else {
                                    MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                            new UserModelClass_ChatRoom("otherUser", usernameleft_chatroom, null, true),
                                            "has left", DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()), usernameleft, usernameleft_role, usernameleft_age, usernameleft_gender), true);
                                    chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                                }
                            }

                        }

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {

                                Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                                while (MyIterator.hasNext()) {
                                    NewChatModelClass newChatModelClass2 = MyIterator.next();

                                    if (newChatModelClass2.getMessangerName().equals(usernameleft_chatroom))
                                        MyIterator.remove();
                                }

                                if (!MySocketsClass.static_objects.hashMap.containsKey(usernameleft_chatroom)) {
                                    MySocketsClass.static_objects.hashMap.put(usernameleft_chatroom, 0);
                                }
                                if (!(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(usernameleft_chatroom))) {
                                    MySocketsClass.static_objects.hashMap.put(usernameleft_chatroom,
                                            MySocketsClass.static_objects.hashMap.get(usernameleft_chatroom) + 1);
                                }

                                if (MySocketsClass.static_objects.FragmentPosition == 0 && !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(usernameleft_chatroom))
                                        || MySocketsClass.static_objects.FragmentPosition == 1 && !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(usernameleft_chatroom))) {
                                    MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);
                                }
                                if (MySocketsClass.getInstance().isAppIsInBackground(ChatRoomsChatActivity.this)) {
                                    ChattingIconRedDot();
                                }
                                newChatModelClass = new NewChatModelClass("ChattingRoomDp", usernameleft_chatroom, usernameleft + " has left", "10", MySocketsClass.static_objects.hashMap.get(usernameleft_chatroom), "ChatRoom");

                                MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                                Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                                chatsRecyclerViewAdapter.notifyDataSetChanged();
                                if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                    MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                                }


                                List<DataBaseChatRoomTables> TotalChatRoomsInRecentChats = MySocketsClass.getInstance().getChatRoomChats(usernameleft_chatroom, ChatRoomsChatActivity.this);
                                if (TotalChatRoomsInRecentChats.size() > 200) {
                                    dataBase_chatRooms.chatRoomDBAO().deleteStartRowsOfChatRoom(usernameleft_chatroom);
                                    Log.e("DeletionDb","Row deleted");
                                }
                            }

                        });

                        //Saving On Database
                        dataBaseChatRoomTables.setMessangerName(usernameleft);
                        dataBaseChatRoomTables.setMessangerRank(usernameleft_role);
                        dataBaseChatRoomTables.setMessangerGender(usernameleft_gender);
                        dataBaseChatRoomTables.setMessangerAge(usernameleft_age);
                        dataBaseChatRoomTables.setMessangerMessage("has left");
                        dataBaseChatRoomTables.setMessangerDate(GettingCurrentDtaeMethod());
                        dataBaseChatRoomTables.setMessangerDp(mySingleTon.dp);
                        dataBaseChatRoomTables.setMessangerChatRoomName(usernameleft_chatroom);
                        dataBase_chatRooms.chatRoomDBAO().addChatRoom_Chat(dataBaseChatRoomTables);




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };
    Emitter.Listener room_users = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("chetrommMsg","in room_users");
            if (TotalOnlineUsersList.size() > 0) {
                TotalOnlineUsersList.clear();
            }
            try {
                String roomUser_chatroom = jsonObject.getString("chatroom");
                String roomUser_own_username = jsonObject.getString("username");
                final String roomUser_roomUsers = jsonObject.getString("roomUsers");

                if (roomUser_chatroom.equalsIgnoreCase(countryName)) {

                    for (int i = 0; i < jsonObject.getJSONArray("usersList").length(); i++) {
                        String ChatRoom_userlist_username = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("username");
                        String ChatRoom_userlist_username_role = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("role");
                        String ChatRoom_userlist_username_dp = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("dp");
                        String ChatRoom_userlist_username_age = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("age");
                        String ChatRoom_userlist_username_gender = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("gender");

                        chatRoomCurrentlyUsersOnlineViewModelClass = new ChatRoomCurrentlyUsersOnlineViewModelClass(ChatRoom_userlist_username,
                                ChatRoom_userlist_username_role,
                                ChatRoom_userlist_username_dp,
                                ChatRoom_userlist_username_age,
                                ChatRoom_userlist_username_gender);
                        TotalOnlineUsersList.add(chatRoomCurrentlyUsersOnlineViewModelClass);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            chatroom_totaluserslayout_counter.setText("Users (" + roomUser_roomUsers + ")");
                            chatRoomsTotalUsersTv.setText("(" + roomUser_roomUsers + ")");
                            chatRoomCurrentlyUsersOnlineAdapter = new ChatRoomCurrentlyUsersOnlineAdapter(TotalOnlineUsersList, ChatRoomsChatActivity.this, countryName);


                            chatroom_totaluserslayot_recyclerview.setAdapter(chatRoomCurrentlyUsersOnlineAdapter);
                            chatRoomCurrentlyUsersOnlineAdapter.notifyDataSetChanged();
                        }
                    });

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    Emitter.Listener receiving_message = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("chetrommMsg","in receiving_message");

            try {
                final String message;
                final String username = jsonObject.getString("username");
                final String user_dp = jsonObject.getString("user_dp");
                if (jsonObject.getString("message").contains("www.youtube.com") ||
                        jsonObject.getString("message").contains("https://youtu.be/")) {
                    message = jsonObject.getString("message");
                } else {
                    message = MyClient.shortnameToUnicode(jsonObject.getString("message"));
                }
                final String chatroom = jsonObject.getString("chatroom");
                final String role = jsonObject.getString("role");
                final String age = jsonObject.getString("age");
                final String gender = jsonObject.getString("gender");
                String direction = jsonObject.getString("direction");
                final String chat_type = jsonObject.getString("chat_type");

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {

                        Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                        while (MyIterator.hasNext()) {
                            NewChatModelClass newChatModelClass2 = MyIterator.next();

                            if (newChatModelClass2.getMessangerName().equals(chatroom)) {
                                MyIterator.remove();
                            }
                        }

                        if (!MySocketsClass.static_objects.hashMap.containsKey(chatroom)) {
                            MySocketsClass.static_objects.hashMap.put(chatroom, 0);
                        }
                        if (!(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(chatroom))) {
                            MySocketsClass.static_objects.hashMap.put(chatroom,
                                    MySocketsClass.static_objects.hashMap.get(chatroom) + 1);
                        }

                        if ((MySocketsClass.static_objects.FragmentPosition == 0 &&
                                !MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(chatroom)) ||
                                (MySocketsClass.static_objects.FragmentPosition == 1 &&
                                        !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(chatroom)) ||
                                        (MySocketsClass.static_objects.FragmentPosition == 2 &&
                                                !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(chatroom))))) {

                            MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);
                        }

                        if (MySocketsClass.getInstance().isAppIsInBackground(ChatRoomsChatActivity.this)) {
                            ChattingIconRedDot();
                        }

                        newChatModelClass = new NewChatModelClass("ChattingRoomDp", chatroom, message, "10",
                                MySocketsClass.static_objects.hashMap.get(chatroom), "ChatRoom");
                        MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                        Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                        chatsRecyclerViewAdapter.notifyDataSetChanged();
                        if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                            MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                        }


                        List<DataBaseChatRoomTables> TotalChatRoomsInRecentChats = MySocketsClass.getInstance().getChatRoomChats(chatroom, ChatRoomsChatActivity.this);
                        if (TotalChatRoomsInRecentChats.size() > 200) {
                            dataBase_chatRooms.chatRoomDBAO().deleteStartRowsOfChatRoom(chatroom);
                            Log.e("DeletionDb","Row deleted");
                        }

                    }

                });

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (countryName.equals(chatroom)) {

                            if (chat_room_recycler_chats.canScrollVertically(1)) {
                                // Agr scroll hua va hy recyclerview to scrolling nahi krani new msg pe
                                MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                        new UserModelClass_ChatRoom("otherUser", chatroom, user_dp, true), message,
                                        DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()), username, role, age, gender), false);
                                //   chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                                Log.e("ChatRoomScrolling", "No Scroll");
                            } else {
                                // Agr scroll ni hua va hy recyclerview to scrolling krani new msg pe
                                MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                        new UserModelClass_ChatRoom("otherUser", chatroom, user_dp, true), message,
                                        DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()), username, role, age, gender), true);
                                chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                                Log.e("ChatRoomScrolling", "Scroll");
                            }

                        }
                    }
                });

                //Saving In Database
                dataBaseChatRoomTables.setMessangerName(username);
                dataBaseChatRoomTables.setMessangerRank(role);
                dataBaseChatRoomTables.setMessangerGender(gender);
                dataBaseChatRoomTables.setMessangerAge(age);
                dataBaseChatRoomTables.setMessangerMessage(message);
                dataBaseChatRoomTables.setMessangerDate(GettingCurrentDtaeMethod());
                dataBaseChatRoomTables.setMessangerDp(user_dp);
                dataBaseChatRoomTables.setMessangerChatRoomName(chatroom);
                dataBase_chatRooms.chatRoomDBAO().addChatRoom_Chat(dataBaseChatRoomTables);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    Emitter.Listener system_message = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("chetrommMsg","in system_message");
            try {

                final String systemMsg_username = jsonObject.getString("username");
                final String systemMsg_chatroom = jsonObject.getString("chatroom");
                final String systemMsg_role = jsonObject.getString("role");
                final String systemMsg_message = jsonObject.getString("message");
                String systemMsg_direction = jsonObject.getString("direction");
                final String systemMsg_chat_type = jsonObject.getString("chat_type");
                final String systemMsg_message_type = jsonObject.getString("message_type");
                Log.e("chatRoomMessageTest",systemMsg_message);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("systemMsg","in thread");
                        if (countryName.equals(systemMsg_chatroom)) {
                            if (chat_room_recycler_chats.canScrollVertically(1)) {
                                MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                        new UserModelClass_ChatRoom("otherUser", systemMsg_chatroom, null, true),
                                        systemMsg_message, DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()),
                                        "system_msgs", systemMsg_role, null, systemMsg_message_type), false);
                            } else {
                                MessagesListAdapter_newChatrooms.addToStart(new MessageModelClass_ChatRoom("otherUser",
                                        new UserModelClass_ChatRoom("otherUser", systemMsg_chatroom, null, true),
                                        systemMsg_message, DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp()),
                                        "system_msgs", systemMsg_role, null, systemMsg_message_type), true);
                                chat_room_recycler_chats.setAdapter(MessagesListAdapter_newChatrooms);
                            }

                        }
                    }
                });

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {

                        Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                        while (MyIterator.hasNext()) {
                            NewChatModelClass newChatModelClass2 = MyIterator.next();

                            if (newChatModelClass2.getMessangerName().equals(systemMsg_chatroom)) {
                                MyIterator.remove();
                            }

                        }

                        if (!MySocketsClass.static_objects.hashMap.containsKey(systemMsg_chatroom)) {
                            MySocketsClass.static_objects.hashMap.put(systemMsg_chatroom, 0);
                        }
                        if (!(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(systemMsg_chatroom))) {
                            MySocketsClass.static_objects.hashMap.put(systemMsg_chatroom,
                                    MySocketsClass.static_objects.hashMap.get(systemMsg_chatroom) + 1);
                        }

                        if (MySocketsClass.static_objects.FragmentPosition == 0 &&
                                !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(systemMsg_chatroom))
                                || MySocketsClass.static_objects.FragmentPosition == 1 &&
                                !(MySocketsClass.static_objects.CurrrentPrivateChatRoomname.equals(systemMsg_chatroom))) {
                            MySocketsClass.static_objects.FragmentNewMessageIcon.setVisibility(View.VISIBLE);
                        }
                        if (MySocketsClass.getInstance().isAppIsInBackground(ChatRoomsChatActivity.this)) {
                            ChattingIconRedDot();
                        }

                        newChatModelClass = new NewChatModelClass("ChattingRoomDp", systemMsg_chatroom, systemMsg_message, "10",
                                MySocketsClass.static_objects.hashMap.get(systemMsg_chatroom), "ChatRoom");
                        MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);

                        Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                        chatsRecyclerViewAdapter.notifyDataSetChanged();
                        if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                            MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                        }



                        List<DataBaseChatRoomTables> TotalChatRoomsInRecentChats = MySocketsClass.getInstance().getChatRoomChats(systemMsg_chatroom, ChatRoomsChatActivity.this);
                        if (TotalChatRoomsInRecentChats.size() > 200) {
                            dataBase_chatRooms.chatRoomDBAO().deleteStartRowsOfChatRoom(systemMsg_chatroom);

                        }
                    }

                });

                //saving on database
                dataBaseChatRoomTables.setMessangerName(systemMsg_username);
                dataBaseChatRoomTables.setMessangerRank(systemMsg_role);
                dataBaseChatRoomTables.setMessangerGender(systemMsg_role);
                dataBaseChatRoomTables.setMessangerAge(systemMsg_message_type);
                dataBaseChatRoomTables.setMessangerMessage(systemMsg_message);
                dataBaseChatRoomTables.setMessangerDate(GettingCurrentDtaeMethod());
                dataBaseChatRoomTables.setMessangerDp("systemm_dp_msgs");
                dataBaseChatRoomTables.setMessangerChatRoomName(systemMsg_chatroom);
                dataBase_chatRooms.chatRoomDBAO().addChatRoom_Chat(dataBaseChatRoomTables);




            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    Emitter.Listener load_gifts_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String command = null, img = null, cost = null, name = null;
                        String username = jsonObject.getString("username");
                        String type = jsonObject.getString("type");
                        for (int i = 0; i < jsonObject.getJSONArray("gifts").length(); i++) {
                            command = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("command");
                            img = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("img");
                            cost = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("cost");
                            name = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("name");

                            chatRoomGiftsModelClass = new ChatRoomGiftsModelClass(command, img, cost, name, countryName);
                            chatRoomGiftList.add(chatRoomGiftsModelClass);
                            chatRoomGiftsRecyclerAdapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });


        }
    };

}