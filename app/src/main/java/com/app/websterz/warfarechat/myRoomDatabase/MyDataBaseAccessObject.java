package com.app.websterz.warfarechat.myRoomDatabase;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface MyDataBaseAccessObject {
    @Insert
    public void addChat(UserPrivateChatTables userPrivateChatTables);

    @Query("select * from user_chats where chat_with=:username")
    public List<UserPrivateChatTables> getUserChat(String username);

    @Query("DELETE FROM user_chats WHERE chat_with=:username")
    public void deleteAlreadyPresentChat(String username);

    @Query("DELETE FROM user_chats")
    public void deleteAllData();

}
