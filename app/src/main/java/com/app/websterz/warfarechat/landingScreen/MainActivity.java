package com.app.websterz.warfarechat.landingScreen;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import de.hdodenhof.circleimageview.CircleImageView;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.BroadCastReceiver.AlarmReceiver;
import com.app.websterz.warfarechat.DataBaseChatRooms.DataBase_ChatRooms;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.about.GetSupport;
import com.app.websterz.warfarechat.activities.SrttingsActivityForChangingURL;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.NewChatModelClass;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.myRoomDatabase.WarfareDatabase;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.app.websterz.warfarechat.signup.SignUp;
import com.app.websterz.warfarechat.webView.MyWebView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;

import static com.stfalcon.chatkit.messages.MessageHolders.baseUrlChatKit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView landingLogo;
    TextView landingTermsTv, landingPrivacyTv, landingStoryTv;
    Animation zoomin, leftToRight, rightToLeft, buttonZoomIn, buttonZoomOut;
    int count = 0;
    Button landingJoinBtn, landingLoginBtn;
    MySingleTon mySingleTon;
    String username_service, username_passwrd, checkNotification;
    String userEmailForGoogleLogin, userSpecialIdForGoogleLogin, loginTypeOfLastUser;
    public static int one_time_emit = 1;
    AppConstants appConstants;
    public static Socket mSocket;
    String user_online_already;
    WarfareDatabase warfareDatabase;
    DataBase_ChatRooms dataBase_chatRooms;
    String MyToken;
    ImageView SupportIconBtn, SettingIconBtn;
    private RelativeLayout bluringBackgroundRelativeLayout,mainLayout;
    private LinearLayout loader_imageview_Layout;
    // public static String baseUrl="https://5.39.63.226:2019/";
    public static String baseUrl = null;
    NewChatModelClass newChatModelClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        baseUrl = GettingBaseURL();
        baseUrlChatKit = baseUrl;

      //  loadingVideoAd(MainActivity.this);
        EnableBroadcastReceiver();
        Initializations();
        gettingUsernamePasswrd();
        removingOldFCMNotificationsHistory();

        //    NewCodeStart............................
        if ((username_service != null && username_passwrd != null)
                || (userEmailForGoogleLogin != null && userSpecialIdForGoogleLogin != null)) {
            showingLoader();
            Log.e("MainTest", "UserAlreadyLoggedIn ");

            String IsUserLoggedInFromAnotherLocation = CheckingLoginFromAnotherLocationPreference();
            if (IsUserLoggedInFromAnotherLocation != null && IsUserLoggedInFromAnotherLocation.equals("Yes")) {
                AlreadyLoggedIn_LogoutUser();

                Log.e("MainTest", "UserSavedAndLoginFromAnotherLocationYes");
                if (mSocket != null && !mSocket.connected()) {
                    Log.e("MainTest", "InSocketNotConnectedIf");
                    ConnectingSocketIfNotConnected();
                    Log.e("MainTest", "InSocketNotConnectedIf2");
                    SendinEmitForLoginUser();
                } else {
                    Log.e("MainTest", "UserAlreadyLoggedIn & SocketConnectedAlready");
                    ConnectingSocketIfNotConnected();
                    SendinEmitForLoginUser();
                }
                makingButtonDisabled();
                ChattingIconRedDot();

            } else {
                Log.e("MainTest", "UserSavedButNotLoginFromAnotherLocation");
                if (mSocket != null && mSocket.connected()) {
                    Log.e("MainTest", "UserSavedButNotLoginFromAnotherLocation->SocketConnectedAlready");
                    already_loggedin_user();
                    makingButtonDisabled();
                } else {
                    Log.e("MainTest", "UserSavedButNotLoginFromAnotherLocation->SocketNotConnected");
                    if (isNetworkAvailable()) {
                        Log.e("MainTest", "UserAlreadyLoggedInButNoSocketConnection/Crashed etc NetAvailable");
                        if (mSocket != null && mSocket.connected())
                            mSocket.disconnect();
                        AlreadyLoggedIn_LogoutUser();
                        ConnectingSocketIfNotConnected();
                        makingButtonDisabled();
                        SendinEmitForLoginUser();
                        ChattingIconRedDot();
                    } else {
                        Log.e("MainTest", "UserAlreadyLoggedInButNoNet");
                        AlreadyLoggedInWithNoNet();
                        makingButtonDisabled();
                    }
                }
            }
        } else {

            makingButtonEnabled();
            LogoutUser();
            connectingSocket();
            initView();
            setAnimations();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    startActivity(new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:" + getApplicationContext().getPackageName())));
                } catch (Exception e) {
                }
            }
        }

        SupportIconBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SupportingFunction();
            }
        });

        SettingIconBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsFunction();
            }
        });

    }

//    private void loadingVideoAd(Context context) {
//
//        Log.e("AdTest", "onVideoAdLoadCalled");
//
//        if (MySocketsClass.static_objects.rewardedAd == null || !MySocketsClass.static_objects.rewardedAd.isLoaded()) {
//
//            Log.e("AdTest", "onVideoAdLoaded");
//
//            MySocketsClass.static_objects.rewardedAd = new RewardedAd(context, context.getResources().getString(R.string.test_reward_ad_id));
//            RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
//                @Override
//                public void onRewardedAdLoaded() {
//                    Log.e("AdTest", "onGoogleVideoAdLoaded");
//                }
//
//                @Override
//                public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
//                    super.onRewardedAdFailedToLoad(loadAdError);
//                    Log.e("AdTest", "onGoogleVideoAdLoadFailed");
//                }
//            };
//            MySocketsClass.static_objects.rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
//
//        } else {
//            Log.e("AdTest", MySocketsClass.static_objects.rewardedAd == null ? "reward ad is null" : "already loaded video Ad");
//        }
//    }

    private void makingButtonDisabled() {
        if (landingLoginBtn != null) landingLoginBtn.setEnabled(false);
        if (landingJoinBtn != null) landingJoinBtn.setEnabled(false);
        if (SettingIconBtn != null) SettingIconBtn.setEnabled(false);
        if (bluringBackgroundRelativeLayout.getVisibility() == View.GONE)
            bluringBackgroundRelativeLayout.setVisibility(View.VISIBLE);
    }

    private void makingButtonEnabled() {
        if (landingLoginBtn != null) landingLoginBtn.setEnabled(true);
        if (landingJoinBtn != null) landingJoinBtn.setEnabled(true);
        if (SettingIconBtn != null) SettingIconBtn.setEnabled(true);
        if (bluringBackgroundRelativeLayout.getVisibility() == View.VISIBLE)
            bluringBackgroundRelativeLayout.setVisibility(View.GONE);
    }

    private void initView() {
        landingTermsTv = findViewById(R.id.landingTermsTv);
        landingTermsTv.setOnClickListener(this);
        landingPrivacyTv = findViewById(R.id.landingPrivacyTv);
        landingPrivacyTv.setOnClickListener(this);
        landingStoryTv = findViewById(R.id.landingStoryTv);
        landingStoryTv.setOnClickListener(this);
        landingLogo = findViewById(R.id.landingLogo);
        landingLoginBtn.setOnClickListener(this);
        landingJoinBtn.setOnClickListener(this);
        zoomin = AnimationUtils.loadAnimation(this, R.anim.zoomin);
        leftToRight = AnimationUtils.loadAnimation(this, R.anim.lefttoright);
        rightToLeft = AnimationUtils.loadAnimation(this, R.anim.righttoleft);
        buttonZoomIn = AnimationUtils.loadAnimation(this, R.anim.buttons_zoomin);
        buttonZoomOut = AnimationUtils.loadAnimation(this, R.anim.buttons_zoomout);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.landingLoginBtn: {
                if (count == 0) {
                    startActivity(new Intent(MainActivity.this, Login.class));
                    Log.e("hostss", baseUrl + "1");
                    mSocket.connect();
                    count++;
                } else {
                    startActivity(new Intent(MainActivity.this, Login.class));
                    Log.e("hostss", baseUrl + "2");
                    mSocket.connect();

                }
                break;
            }
            case R.id.landingJoinBtn: {
                if (count == 0) {

                    count++;
                    startActivity(new Intent(MainActivity.this, SignUp.class));

                } else {
                    startActivity(new Intent(MainActivity.this, SignUp.class));
                }
                break;
            }
            case R.id.landingTermsTv: {
                Intent intent = new Intent(MainActivity.this, MyWebView.class);
                intent.putExtra("webview", 0);
                startActivity(intent);
                break;
            }
            case R.id.landingPrivacyTv: {
                Intent intent = new Intent(MainActivity.this, MyWebView.class);
                intent.putExtra("webview", 1);
                startActivity(intent);
                break;
            }
            case R.id.landingStoryTv: {
                Intent intent = new Intent(MainActivity.this, MyWebView.class);
                intent.putExtra("webview", 2);
                startActivity(intent);
                break;
            }
        }
    }

    public void already_loggedin_user() {

        Log.e("Serviceng", "SocketConnected>");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        String username = prefs.getString("user_name", null);
        String role = prefs.getString("role", null);
        String dp = prefs.getString("dp", null);
        String statusMsg = prefs.getString("statusMsg", null);
        String status = prefs.getString("status", null);
        String sID = prefs.getString("sID", null);
        String presence = prefs.getString("presence", null);
        String age = prefs.getString("age", null);
        String gender = prefs.getString("gender", null);
        String coins = prefs.getString("coins", null);

        mySingleTon.username = username;
        mySingleTon.role = role;
        mySingleTon.dp = dp;
        mySingleTon.statusMsg = statusMsg;
        mySingleTon.status = status;
        mySingleTon.sID = sID;
        mySingleTon.presence = presence;
        mySingleTon.age = age;
        mySingleTon.gender = gender;
        mySingleTon.coins = coins;

        SavingInternetForDisconnectionBar();
        Intent intent = new Intent(MainActivity.this, HomeScreen.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void isUserOnline() {

        SharedPreferences alrdy_onlined = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        user_online_already = alrdy_onlined.getString("user_online", "offline");
    }

    public void connectingSocket() {
        ConnectingSocketIfNotConnected();
    }

    private void ConnectingSocketIfNotConnected() {

        if (mSocket == null) {
            Log.e("MainTest", "SocketNull");
            mSocket = AppConstants.mSocket;
            try {
                Log.e("MainTest", baseUrl);
                mSocket = IO.socket(baseUrl);
                Log.e("MainTest", "SocketNull2");
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        if (!mSocket.connected()) {
            Log.e("MainTest", "SocketNotconnected");
            mSocket.connect();

        }
    }

    public void gettingUsernamePasswrd() {
        Log.e("MainTest", "InMethodGettingUserNamePasswrd");
        loginTypeOfLastUser = GettingLoginTypePreference();
        Log.e("MainTest", "InMethodGettingUserNamePasswrd> loginTypeOfLastUser> " + loginTypeOfLastUser);
        if (loginTypeOfLastUser != null && loginTypeOfLastUser.equals("login")) {
            Log.e("MainTest", "InMethodGettingUserNamePasswrd> in if");
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
            username_service = prefs.getString("mine_user_name", null);
            username_passwrd = prefs.getString("mine_password", null);
            Log.e("MainTest", "username > " + username_service + " pwd > " + username_passwrd);
        } else if (loginTypeOfLastUser != null && loginTypeOfLastUser.equals("google")) {
            Log.e("testingLoginss", "InMethodGettingUserNamePasswrd> in else");
            gettingEmailAndUSerIdFromPreference();
        } else if (loginTypeOfLastUser != null && loginTypeOfLastUser.equals("FaceBook")) {
            Log.e("testingLoginss", "InMethodGettingUserNamePasswrd> in else");
            gettingEmailAndUSerIdFromPreference();
        }
    }

    private void gettingEmailAndUSerIdFromPreference() {

        SharedPreferences GettingPreferenc = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        userEmailForGoogleLogin = GettingPreferenc.getString("userEmail", null);
        userSpecialIdForGoogleLogin = GettingPreferenc.getString("userSpecialId", null);
        Log.e("MainTest", "userEmailForGoogleLogin > " + userEmailForGoogleLogin +
                " userSpecialIdForGoogleLogin > " + userSpecialIdForGoogleLogin);
    }

    private String GettingLoginTypePreference() {
        SharedPreferences GettingPreferenc = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        return GettingPreferenc.getString("userLoginType", null);
    }

    private void setAnimations() {
//        landingLogo.clearAnimation();
//        landingLogo.startAnimation(zoomin);
//        landingLoginBtn.clearAnimation();
//        landingLoginBtn.startAnimation(leftToRight);
//        landingJoinBtn.clearAnimation();
//        landingJoinBtn.startAnimation(rightToLeft);
        landingLogo.animate().rotation(landingLogo.getRotation() + 180f).setDuration(1500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setAnimations();
            }
        }, 2000);


    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = MainActivity.this;
        Log.e("testst", "OnResume");
    }

    private void DeletingPreferencesForUserNameAndPaswrd() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        DeletingPreferences.remove("mine_user_name");
        DeletingPreferences.remove("mine_password");
        DeletingPreferences.apply();
    }


    private void DeletingPreferenceForEmailAndSpecialId() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        DeletingPreferences.remove("userEmail");
        DeletingPreferences.remove("userSpecialId");
        DeletingPreferences.apply();
    }

    protected void LogoutUser() {

        warfareDatabase = Room.databaseBuilder(MainActivity.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(MainActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        warfareDatabase = Room.databaseBuilder(MainActivity.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(MainActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();

        if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
        }
        HomeScreen.is_user_connected = 0;
        warfareDatabase.myDataBaseAccessObject().deleteAllData();
        dataBase_chatRooms.chatRoomDBAO().deleteAllDataChatRoom();
        DeletingPreferenceForEmailAndSpecialId();
        DeletingPreferencesForUserNameAndPaswrd();
        MySocketsClass.getInstance().deletingPreferencesForNotifications(MainActivity.this);
        MainActivity.one_time_emit = 1;
        MySocketsClass.static_objects.MyTotalCoin = "";
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        prefuseronline.putString("user_online", "offline");
        prefuseronline.apply();

        MySocketsClass.static_objects.NewUserMessageList.clear();
        MySocketsClass.static_objects.hashMap.clear();
        MainActivity.this.stopService(new Intent(MainActivity.this, Foreground_service.class));

        if (mSocket != null) {
            mSocket.off(MySocketsClass.ListenersClass.RECEIVE_PRIVATE);
            mSocket.off(MySocketsClass.ListenersClass.RECEIVE_GIFTS);
            mSocket.off(MySocketsClass.ListenersClass.GET_ATTACK_LAUNCH_RESPONSE);
            mSocket.off(MySocketsClass.ListenersClass.RECEIVE_ATTACK);
            mSocket.disconnect();
        }
        DeletingPreferencesForDisconnectionBar();
    }

    protected void AlreadyLoggedIn_LogoutUser() {

        warfareDatabase = Room.databaseBuilder(MainActivity.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(MainActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        warfareDatabase = Room.databaseBuilder(MainActivity.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(MainActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();

        if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
        }
        HomeScreen.is_user_connected = 0;
        warfareDatabase.myDataBaseAccessObject().deleteAllData();
        dataBase_chatRooms.chatRoomDBAO().deleteAllDataChatRoom();

        one_time_emit = 1;
        MySocketsClass.static_objects.MyTotalCoin = "";

        MySocketsClass.static_objects.NewUserMessageList.clear();
        MySocketsClass.static_objects.hashMap.clear();
        DeletingPreferencesForDisconnectionBar();
    }

    private void DeletingUserDataDueToNullValue() {
        warfareDatabase = Room.databaseBuilder(MainActivity.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(MainActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        warfareDatabase = Room.databaseBuilder(MainActivity.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(MainActivity.this, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();

        if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
        }
        HomeScreen.is_user_connected = 0;
        warfareDatabase.myDataBaseAccessObject().deleteAllData();
        dataBase_chatRooms.chatRoomDBAO().deleteAllDataChatRoom();
        DeletingPreferencesForUserNameAndPaswrd();
        DeletingPreferenceForEmailAndSpecialId();
        MySocketsClass.getInstance().deletingPreferencesForNotifications(MainActivity.this);

        one_time_emit = 1;
        MySocketsClass.static_objects.MyTotalCoin = "";

        MySocketsClass.static_objects.NewUserMessageList.clear();
        MySocketsClass.static_objects.hashMap.clear();
        DeletingPreferencesForDisconnectionBar();
        hidingLoader();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CallingBroadcastReceiverForReminderNotification();
    }

    private void EnableBroadcastReceiver() {
        ComponentName receiver = new ComponentName(this, AlarmReceiver.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void CallingBroadcastReceiverForReminderNotification() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        checkNotification = prefs.getString("notification_remainder", null);
        if (checkNotification != null && checkNotification.equals("firsttime")) {
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

            Intent intent01 = new Intent("android.media.action.DISPLAY_NOTIFICATION");
            intent01.addCategory("android.intent.category.DEFAULT");
            intent01.setClass(this, AlarmReceiver.class);

            PendingIntent broadcast = PendingIntent.getBroadcast(MainActivity.this, 100, intent01, PendingIntent.FLAG_UPDATE_CURRENT);

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, 3600); //logout after 5 hour for testing


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
            } else {
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
            }
        } else {

        }
    }

    private void AlreadyLoggedInWithNoNet() {
        SavingWithoutnetForDisconnectionBar();
        Intent intent = new Intent(MainActivity.this, HomeScreen.class);
        startActivity(intent);
    }

    private void SavingWithoutnetForDisconnectionBar() {
        SharedPreferences.Editor SavingPreferenceWithoutNet = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        SavingPreferenceWithoutNet.putString("UseropenState", "WithouNet");
        SavingPreferenceWithoutNet.apply();
    }

    private void SavingInternetForDisconnectionBar() {
        SharedPreferences.Editor SavingPreferenceWithNet = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        SavingPreferenceWithNet.putString("UseropenState", "");
        SavingPreferenceWithNet.apply();
        Log.e("MainTest", "Last line");
    }

    private void DeletingPreferencesForDisconnectionBar() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        DeletingPreferences.remove("UseropenState");
        DeletingPreferences.apply();
    }

    private void DeletingPreferencesForRememberMe() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        DeletingPreferences.remove("RememberMe");
        DeletingPreferences.apply();
    }

    private void Initializations() {
        bluringBackgroundRelativeLayout = findViewById(R.id.bluringBackgroundRelativeLayout);
        mainLayout = findViewById(R.id.mainLayout);
        appConstants = new AppConstants();
        mySingleTon = MySingleTon.getInstance();
        SupportIconBtn = findViewById(R.id.SupportIconBtn);
        SettingIconBtn = findViewById(R.id.SettingIconBtn);
        landingLoginBtn = findViewById(R.id.landingLoginBtn);
        landingJoinBtn = findViewById(R.id.landingJoinBtn);
    }

    private String CheckingLoginFromAnotherLocationPreference() {
        SharedPreferences AnotherLocationPreference = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        String IsUserLoggedInFromAnotherLocation = AnotherLocationPreference.getString("AnotherLocationPreference", null);
        return IsUserLoggedInFromAnotherLocation;
    }

    private void SupportingFunction() {
        MainActivity.this.startActivity(new Intent(MainActivity.this, GetSupport.class));
    }

    private void SettingsFunction() {
        MainActivity.this.startActivity(new Intent(MainActivity.this, SrttingsActivityForChangingURL.class));
        finish();
    }

    private String GettingBaseURL() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        return sharedPreferences.getString("MainBaseURL", "https://socio.chat/");

    }

    private void ChattingIconRedDot() {
        SharedPreferences.Editor chattingredDot = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        chattingredDot.putString("chattingredDot", "no_newMsg");
        chattingredDot.apply();
    }

    private String GettingToken() {
        Log.e("MainTest", "in getting Token method");
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                Log.e("MainTest", "in method2 gettingToken");

                if (!task.isSuccessful()) {
                    Log.e("MainTest", "getInstanceId failed inGettingToken");
                    return;
                }
                // Get new Instance ID token
                MyToken = task.getResult().getToken();

            }

        });
        return MyToken;
    }

    private void SendinEmitForLoginUser() {
        Log.e("MainTest", "InSendinEmitMethod");
        String token = GettingToken();

        if (token == null) {
            Log.e("MainTest", "token is null so getting token from preference");
            token = GettingTokenFromPreference();
        } else {
            Log.e("MainTest", "token saving in preference");
            SavingTokenInPreference(token);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            Log.e("MainTest", "InSendinEmitMethod >" + username_service + username_passwrd + token);

            if (loginTypeOfLastUser.equals("login")) {
                jsonObject.put("username", username_service);
                jsonObject.put("password", username_passwrd);
                jsonObject.put("token", token);
                jsonObject.put("agent", "ANDROID1.0");
                jsonObject.put("login_type", "login");
            } else if (loginTypeOfLastUser.equals("google")) {
                jsonObject.put("email", userEmailForGoogleLogin);
                jsonObject.put("social_id", userSpecialIdForGoogleLogin);
                jsonObject.put("login_type", "google");
                jsonObject.put("token", token);
                jsonObject.put("agent", "ANDROID1.0");
            } else if (loginTypeOfLastUser.equals("FaceBook")) {
                jsonObject.put("email", userEmailForGoogleLogin);
                jsonObject.put("social_id", userSpecialIdForGoogleLogin);
                jsonObject.put("login_type", "facebook");
                jsonObject.put("token", token);
                jsonObject.put("agent", "ANDROID1.0");
            }

            Log.e("MainTest", "checking the data before sending json" + username_service + username_passwrd + token);
            if (token != null && mSocket != null) {
                Log.e("MainTest", "In if for sending json" + username_service + username_passwrd + token);
                mSocket.emit(MySocketsClass.EmmittersClass.ADD_USER, jsonObject);
                mSocket.off(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP);
                mSocket.emit(MySocketsClass.EmmittersClass.LOAD_NOTIFICATION);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_NOTIFICATIONS_IN_APP, getting_unread_total_notifications);
                mSocket.on(MySocketsClass.ListenersClass.LOGIN, onLogin);
                mSocket.on(MySocketsClass.ListenersClass.ERROR_MESSAGE, loginError);
                mSocket.emit(MySocketsClass.EmmittersClass.CHECK_OFFLINE_MESSAGES);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_OFFLINE_MESSAGES, loading_offline_messages);
            } else {

                Log.e("MainTest", "NewlyLoggedIn because token or something else is null");
                DeletingUserDataDueToNullValue();
                connectingSocket();
                initView();
                setAnimations();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    try {
                        startActivity(new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:" + getApplicationContext().getPackageName())));
                    } catch (Exception e) {
                    }
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void SavingTokenInPreference(String MyToken) {
        SharedPreferences.Editor SavingTokenPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        SavingTokenPreferences.putString("MyTokenValue", MyToken);
        SavingTokenPreferences.apply();
    }

    private String GettingTokenFromPreference() {
        SharedPreferences GettingPreferenc = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        return GettingPreferenc.getString("MyTokenValue", null);
    }

    private void ShowingTotalofflineMessagesToast(String allMsgs) {
        TextView toast_text;
        CircleImageView toast_image;
        LinearLayout layout_presence_toast;
        LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastView = inflater.inflate(R.layout.presence_toast, null);
        Toast toast = new Toast(MainActivity.this);
        toast.setView(toastView);
        toast_text = toastView.findViewById(R.id.toast_text);
        toast_image = toastView.findViewById(R.id.toast_image);
        layout_presence_toast = toastView.findViewById(R.id.layout_presence_toast);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) toast_text.getLayoutParams();
        layoutParams.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
        toast_text.setLayoutParams(layoutParams);
        toast_image.setVisibility(View.GONE);
        layout_presence_toast.setBackgroundResource(R.drawable.offline_toast_msg_background);
        toast_text.setTextColor(getResources().getColor(R.color.colorWhite));
        toast_text.setPadding(10, 0, 10, 0);
        toast_text.setText("You have " + allMsgs + " offline messages.");
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 135);
        toast.show();
    }

    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("MainTest", "OnLoginListener");

                    SharedPreferences.Editor editor = getSharedPreferences("myPrefs", 0).edit();
                    JSONObject data = (JSONObject) args[0];
                    String username, role, dp, statusMsg, status, sID, presence, age, gender, coins;
                    //   int theme;
                    try {
                        username = data.getString("username");
                        role = data.getString("role");
                        dp = data.getString("dp");
                        statusMsg = data.getString("statusMsg");
                        status = data.getString("status");
                        sID = data.getString("sID");
                        presence = data.getString("presence");
                        age = data.getString("age");
                        gender = data.getString("gender");
                        coins = data.getString("coins");
                        //  theme = Integer.parseInt(data.getString("theme"));
                        //   MySocketsClass.static_objects.theme = theme;


                        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
                        prefEditor55.putString("user_name", username);
                        prefEditor55.putString("role", role);
                        prefEditor55.putString("dp", dp);
                        prefEditor55.putString("statusMsg", statusMsg);
                        prefEditor55.putString("status", status);
                        prefEditor55.putString("sID", sID);
                        prefEditor55.putString("presence", presence);
                        prefEditor55.putString("age", age);
                        prefEditor55.putString("gender", gender);
                        prefEditor55.putString("coins", coins);
                        //   prefEditor55.putString("theme", String.valueOf(theme));
                        prefEditor55.apply();

                        editor.putString("username", username);
                        mySingleTon.username = username;
                        mySingleTon.role = role;
                        mySingleTon.dp = dp;
                        mySingleTon.statusMsg = statusMsg;
                        mySingleTon.status = status;
                        mySingleTon.sID = sID;
                        mySingleTon.presence = presence;
                        mySingleTon.age = age;
                        mySingleTon.gender = gender;
                        mySingleTon.coins = coins;
                        //    mySingleTon.theme = String.valueOf(theme);

                        editor.apply();
                        SavingInternetForDisconnectionBar();
                        Intent intent = new Intent(MainActivity.this, HomeScreen.class);
                        startActivity(intent);
                        finish();
                        //  MyLoader.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        Log.d("me", "error  " + e.getMessage());
                        return;
                    }
                }
            });
        }
    };

    private Emitter.Listener loginError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String title, message;
                    try {
                        title = data.getString("title");
                        message = data.getString("message");

//                        CustomDialogError customDialogError;
//                        customDialogError = new CustomDialogError();
                        MySocketsClass.getInstance().showErrorDialog(MainActivity.this, title, message, "invalidCredentials", "");
                        mSocket.off(MySocketsClass.ListenersClass.ERROR_MESSAGE);
                        logingOutUser();
                        initView();
                        setAnimations();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            try {
                                startActivity(new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:" + getApplicationContext().getPackageName())));
                            } catch (Exception e) {
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.d("error test", data.toString());
                }
            });
        }
    };

    private void AllNotificationsClear() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void showingLoader(){
        loader_imageview_Layout=MySocketsClass.custom_layout.Loader_image(MainActivity.this, mainLayout);
    }

    private void hidingLoader(){
        if (loader_imageview_Layout != null && loader_imageview_Layout.getVisibility() == View.VISIBLE) {
            loader_imageview_Layout.setVisibility(View.GONE);
        }
    }

    private void logingOutUser() {
        stopService(new Intent(MainActivity.this, Foreground_service.class));
        hidingLoader();
        if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
        }
        HomeScreen.is_user_connected = 0;
        MySocketsClass.static_objects.MyTotalCoin = "";
        warfareDatabase.myDataBaseAccessObject().deleteAllData();
        dataBase_chatRooms.chatRoomDBAO().deleteAllDataChatRoom();
        DeletingPreferencesForUserNameAndPaswrd();
        DeletingPreferenceForEmailAndSpecialId();
        MySocketsClass.getInstance().deletingPreferencesForNotifications(MainActivity.this);
        AllNotificationsClear();
        one_time_emit = 1;
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
        prefuseronline.putString("user_online", "offline");
        prefuseronline.apply();
        MySocketsClass.static_objects.NewUserMessageList.clear();
        MySocketsClass.static_objects.hashMap.clear();
        DeletingPreferencesForDisconnectionBar();
        DeletingPreferencesForRememberMe();
        makingButtonEnabled();
    }

    private void removingOldFCMNotificationsHistory(){
        if (MySocketsClass.static_objects.fcmNotificationsMap!=null){
            MySocketsClass.static_objects.fcmNotificationsMap.clear();
        }
    }

    Emitter.Listener loading_offline_messages = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("oflineMessages", "InListener");
            MySocketsClass.static_objects.OfflineMessagesRedDotView = "visible";
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                final String MessangerUsername = jsonObject.getString("username");
                final int totalMsgsuserName = jsonObject.getInt("totalMsgs");
                final String user_dp = jsonObject.getString("user_dp");
                final String allMsgs = jsonObject.getString("allMsgs");
                final String ThePresence = jsonObject.getString("presence");
                final String LastMessage = jsonObject.getString("last_message");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ShowingTotalofflineMessagesToast(allMsgs);
                    }
                });


                if (!(MessangerUsername.equalsIgnoreCase("System"))) {
                    Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                    while (MyIterator.hasNext()) {
                        NewChatModelClass newChatModelClass2 = MyIterator.next();

                        if (newChatModelClass2.getMessangerName().equals(MessangerUsername))
                            MyIterator.remove();

                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {

                            if (!MySocketsClass.static_objects.hashMap.containsKey(MessangerUsername)) {
                                MySocketsClass.static_objects.hashMap.put(MessangerUsername, totalMsgsuserName);
                            }
                            newChatModelClass = new NewChatModelClass(user_dp, MessangerUsername, LastMessage,
                                    ThePresence, MySocketsClass.static_objects.hashMap.get(MessangerUsername), "Chat");
                            MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                            Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                            MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                            if (MySocketsClass.static_objects.ChatsrecyclerviewInstance != null) {
                                MySocketsClass.static_objects.ChatsrecyclerviewInstance.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                            }
                        }

                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener getting_unread_total_notifications = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                JSONArray notificationsArray = jsonObject.getJSONArray("notifications");
                MySocketsClass.static_objects.TotalNotificationsInsideApp = 0;
                for (int i = 0; i < notificationsArray.length(); i++) {
                    if (notificationsArray.getJSONObject(i).getString("message").split("\\|")[2].equals("0")) {
                        MySocketsClass.static_objects.TotalNotificationsInsideApp++;
                    }
                }
                Log.e("totlNotifctn", "Total notifctns unread in main activity >" + MySocketsClass.static_objects.TotalNotificationsInsideApp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("totlNotifctn", jsonObject.toString());
        }
    };


}
