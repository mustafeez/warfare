package com.app.websterz.warfarechat.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.emoji.Emojicon;
import xyz.schwaab.avvylib.AvatarView;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.my_profile.EditPaswrdEmailProfile;
import com.app.websterz.warfarechat.my_profile.My_profile;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.emojione.tools.Callback;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class EditMyProfileActivity extends AppCompatActivity {

    EditText user_realname_edittext,aboutMeEditText;
    LinearLayout GameplayerLayout, FriendLayout, SoulmateLayout;
    RelativeLayout parentLinearLayout;
    CircleImageView users_dp;
    AvatarView avatarLoader;
    private String GameplayerValue, SoulmateValue, FriendValue;
    ImageView GameplayerImage, FriendImage, SoulmateImage, filtersButton;
    Button editButton;
    Client client;
    MySingleTon mySingleTon = MySingleTon.getInstance();
    String userFullName, userEmailed, userUpdatedName, social_google, social_faceBook, aboutMeValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_profile);

        initializations();
        myCode();

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editingProfileDetails();
            }
        });
        SoulmateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoulmateChangerMethod();
            }
        });
        FriendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendChangerMethod();
            }
        });
        GameplayerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GamePlayerChangerMethod();
            }
        });
        filtersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editingPaswrdEmail();
            }
        });
    }

    private void initializations() {
        user_realname_edittext = findViewById(R.id.user_realname_edittext);
        aboutMeEditText = findViewById(R.id.aboutMeEditText);
        GameplayerLayout = findViewById(R.id.GameplayerLayout);
        FriendLayout = findViewById(R.id.FriendLayout);
        SoulmateLayout = findViewById(R.id.SoulmateLayout);
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        users_dp = findViewById(R.id.users_dp);
        avatarLoader = findViewById(R.id.MyProfileLoader);
        editButton = findViewById(R.id.editButton);
        GameplayerImage = findViewById(R.id.GameplayerImage);
        SoulmateImage = findViewById(R.id.SoulmateImage);
        FriendImage = findViewById(R.id.FriendImage);
        filtersButton = findViewById(R.id.filtersButton);
    }

    private void myCode() {
        Intent intent = getIntent();
        GameplayerValue = intent.getStringExtra("GamePlayerValue");
        SoulmateValue = intent.getStringExtra("SoulMateValue");
        FriendValue = intent.getStringExtra("FriendValue");
        userFullName = intent.getStringExtra("userFullName");
        userEmailed = intent.getStringExtra("userEmail");
        social_google = intent.getStringExtra("social_google");
        social_faceBook = intent.getStringExtra("social_faceBook");
        aboutMeValue = intent.getStringExtra("aboutMeValue");

        user_realname_edittext.setText(userFullName);
        user_realname_edittext.setSelection(user_realname_edittext.getText().length());
      //  aboutMeEditText.setText(aboutMeValue);

        client = new Client(EditMyProfileActivity.this);
        client.shortnameToImage(aboutMeValue,
                60, new Callback() {
                    @Override
                    public void onFailure(IOException e) {
                    }

                    @Override
                    public void onSuccess(SpannableStringBuilder ssb) {


                        aboutMeEditText.setText(ssb);
                    }
                });

        if (SoulmateValue.equals("0")) {
            SoulmateImage.setBackgroundResource(R.drawable.green_heart_unlled);
        } else {
            SoulmateImage.setBackgroundResource(R.drawable.green_heart_filled);
        }
        if (GameplayerValue.equals("0")) {
            GameplayerImage.setBackgroundResource(R.drawable.green_gameplayer_unfilled);
        } else {
            GameplayerImage.setBackgroundResource(R.drawable.green_gameplayer_filled);
        }
        if (FriendValue.equals("0")) {
            FriendImage.setBackgroundResource(R.drawable.green_user_unfilled);
        } else {
            FriendImage.setBackgroundResource(R.drawable.green_user_filled);
        }


        avatarLoader.setVisibility(View.VISIBLE);
        avatarLoader.setAnimating(true);
        GlideApp.with(EditMyProfileActivity.this).load(baseUrl + mySingleTon.dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                avatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(users_dp);

        LocalBroadcastManager.getInstance(EditMyProfileActivity.this).registerReceiver(newSocialConnectionValues,new IntentFilter("SocialLoginConnection"));
    }

    private void editingPaswrdEmail() {
        Intent intent = new Intent(EditMyProfileActivity.this, EditPaswrdEmailProfile.class);
        intent.putExtra("profileuserEmailed", userEmailed);
        intent.putExtra("social_google", social_google);
        intent.putExtra("social_faceBook", social_faceBook);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK) {
                    userEmailed = data.getStringExtra("result");
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = EditMyProfileActivity.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(EditMyProfileActivity.this).unregisterReceiver(newSocialConnectionValues);
    }

    private void editingProfileDetails() {
        if (!user_realname_edittext.getText().toString().equals("")) {
            String value = user_realname_edittext.getText().toString();
            userUpdatedName = value;
            sendingSocketForChangingName(value);
            if (checkinglengthofAboutmeEditText(aboutMeEditText.getText().toString())) {
                sendingSocketForChangingAboutMe(aboutMeEditText.getText().toString());
            } else {
                Snackbar.make(parentLinearLayout, "Length exceeded.", Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(parentLinearLayout, "Enter details first.", Snackbar.LENGTH_LONG).show();
        }
    }

    private boolean checkinglengthofAboutmeEditText(String totalText) {
        int totalLength = 0;
        for (int i = 0; i < totalText.length(); i++) {
            char ch = totalText.charAt(i);
            if (ch == ' ') {
                totalLength++;
            }
        }
        if (totalLength < 200) {
            return true;
        } else {
            return false;
        }
    }

    private void sendingSocketForChangingName(String value) {
        if (value.matches("[A-Za-z ]*")) {
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("field_name", "name");
                jsonObject.put("field_value", value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);
        } else {
            Snackbar.make(parentLinearLayout, "Special chracter not allowed", Snackbar.LENGTH_LONG).show();
        }
    }

    private void sendingSocketForChangingAboutMe(String aboutMeText) {
        if (aboutMeText.matches("[A-Za-z ]*")) {
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("field_name", "about_me");
                jsonObject.put("field_value", aboutMeText);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response_about_me);
        } else {
            Snackbar.make(parentLinearLayout, "Special chracter not allowed", Snackbar.LENGTH_LONG).show();
        }
    }

    private void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void GamePlayerChangerMethod() {
        if (GameplayerValue.equals("0")) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("field_name", "pf_player");
                jsonObject.put("field_value", "1");
                mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GameplayerValue = "1";
            GameplayerImage.setBackgroundResource(R.drawable.green_gameplayer_filled);
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("field_name", "pf_player");
                jsonObject.put("field_value", "0");
                mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GameplayerValue = "0";
            GameplayerImage.setBackgroundResource(R.drawable.green_gameplayer_unfilled);
        }
    }

    private void FriendChangerMethod() {
        if (FriendValue.equals("0")) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("field_name", "pf_friend");
                jsonObject.put("field_value", "1");
                mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            FriendValue = "1";
            FriendImage.setBackgroundResource(R.drawable.green_user_filled);
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("field_name", "pf_friend");
                jsonObject.put("field_value", "0");
                mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            FriendValue = "0";
            FriendImage.setBackgroundResource(R.drawable.green_user_unfilled);
        }
    }

    private void SoulmateChangerMethod() {
        if (SoulmateValue.equals("0")) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("field_name", "pf_soulmate");
                jsonObject.put("field_value", "1");
                mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            SoulmateValue = "1";
            SoulmateImage.setBackgroundResource(R.drawable.green_heart_filled);
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("field_name", "pf_soulmate");
                jsonObject.put("field_value", "0");
                mSocket.off(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            SoulmateValue = "0";
            SoulmateImage.setBackgroundResource(R.drawable.green_heart_unlled);
        }
    }

    private BroadcastReceiver newSocialConnectionValues=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getStringExtra("SocialConnectionType").equalsIgnoreCase("Facebook")){
                social_faceBook=intent.getStringExtra("SocialConnectionState");
            }
            else {
                social_google= intent.getStringExtra("SocialConnectionState");
            }
        }
    };

    Emitter.Listener update_profile_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            hideKeyboard(EditMyProfileActivity.this);
            try {
                int success = jsonObject.getInt("success");
                if (success == 1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Snackbar.make(parentLinearLayout, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show();
                                if (userUpdatedName != null) {
                                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(EditMyProfileActivity.this);
                                    Intent intent = new Intent("ChangingUserId");
                                    intent.putExtra("newUserName", userUpdatedName);
                                    localBroadcastManager.sendBroadcast(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Snackbar.make(parentLinearLayout, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
                mSocket.off("update_profile_response");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener update_profile_response_about_me = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            hideKeyboard(EditMyProfileActivity.this);
            try {
                int success = jsonObject.getInt("success");
                if (success == 1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Snackbar.make(parentLinearLayout, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
                mSocket.off("update_profile_response");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}

