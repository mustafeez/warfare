package com.app.websterz.warfarechat.homeScreen.fragments.chats;

public class NewChatModelClass {

   private String MessangerDp,MessangerName,MessangerStatus,MessangerPresence,ChattingType;
    private int Counting;


    public NewChatModelClass(String messangerDp,String messangerName,String messangerStatus,String presence,int counting,String Chattype) {
        super();
        MessangerDp=messangerDp;
        MessangerName=messangerName;
        MessangerStatus=messangerStatus;
        MessangerPresence=presence;
        Counting=counting;
        ChattingType=Chattype;
    }

    public String getMessangerDp() {
        return MessangerDp;
    }

    public String getMessangerName() {
        return MessangerName;
    }

    public String getMessangerStatus() {
        return MessangerStatus;
    }

    public String getMessangerPresence() {
        return MessangerPresence;
    }

    public int getCounting() {
        return Counting;
    }

    public String getChattingType() {
        return ChattingType;
    }

    public void setMessangerDp(String messangerDp) {
        MessangerDp = messangerDp;
    }

    public void setMessangerName(String messangerName) {
        MessangerName = messangerName;
    }

    public void setMessangerStatus(String messangerStatus) {
        MessangerStatus = messangerStatus;
    }

    public void setMessangerPresence(String messangerPresence) {
        MessangerPresence = messangerPresence;
    }

    public void setCounting(int counting) {
        Counting = counting;
    }

    public void setChattingType(String chattingType) {
        ChattingType = chattingType;
    }
}
