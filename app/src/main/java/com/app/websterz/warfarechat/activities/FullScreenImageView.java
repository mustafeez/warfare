package com.app.websterz.warfarechat.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.ExplainingShoutActivity;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FullScreenImageView extends AppCompatActivity {
    Button closebutton,downloadbutton;
    PhotoView photoFullImageView;
    RelativeLayout parentLinearLayout;
    String folderType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image_view);
        init();
        myCode();

        closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        downloadbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadingImage();
            }
        });
    }

    private void init(){
        closebutton = findViewById(R.id.closebutton);
        photoFullImageView = findViewById(R.id.photoFullImageView);
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        downloadbutton = findViewById(R.id.downloadbutton);
    }

    private void myCode(){
        Intent intent =getIntent();
        String photoUrl=intent.getStringExtra("photoUrl");
        folderType=intent.getStringExtra("folderType");

        DisplayMetrics dis = new DisplayMetrics();
        FullScreenImageView.this.getWindowManager().getDefaultDisplay().getMetrics(dis);
        int height = dis.heightPixels;
        int width = dis.widthPixels;
        photoFullImageView.setMinimumHeight(height);
        photoFullImageView.setMinimumWidth(width);

        Glide.with(FullScreenImageView.this)
                .load(photoUrl)
                .into(photoFullImageView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = FullScreenImageView.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
    }

    private void downloadingImage(){
        if (CheckPermissions()) {

            if (ActivityCompat.checkSelfPermission(FullScreenImageView.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return;
            } else {
                Boolean success = MySocketsClass.getInstance().DownloadingImage(photoFullImageView,FullScreenImageView.this,folderType);
                if (success) {
                    Toast.makeText(FullScreenImageView.this, "Image Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(FullScreenImageView.this, "Unable to download.", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            requestPermissions();
        }
    }

    private boolean CheckPermissions() {
        int writeStoragePermissionResult = ContextCompat.checkSelfPermission(FullScreenImageView.this, WRITE_EXTERNAL_STORAGE);
        int readStoragePermissionResult = ContextCompat.checkSelfPermission(FullScreenImageView.this, READ_EXTERNAL_STORAGE);
        int micPermissionResult = ContextCompat.checkSelfPermission(FullScreenImageView.this, RECORD_AUDIO);
        return writeStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                readStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                micPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(FullScreenImageView.this, new String[]{
                WRITE_EXTERNAL_STORAGE,
                READ_EXTERNAL_STORAGE,
                RECORD_AUDIO,


        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                    boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (write_str && read_str && record_str) {
                        Toast.makeText(FullScreenImageView.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(FullScreenImageView.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }
}