package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.NewShoutSuggestedInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.NewSuggestedShoutModelClass;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NewShoutSuggestionsAdapter extends RecyclerView.Adapter<NewShoutSuggestionsAdapter.ViewHolder> {
    ArrayList<NewSuggestedShoutModelClass> newShoutSuggestedArrayalist;
    Context myContext;
    NewShoutSuggestedInterface newShoutSuggestedInterface;

    public NewShoutSuggestionsAdapter(ArrayList<NewSuggestedShoutModelClass> newShoutSuggestedArrayalist, Context myContext, NewShoutSuggestedInterface newShoutSuggestedInterface) {
        this.newShoutSuggestedArrayalist = newShoutSuggestedArrayalist;
        this.myContext = myContext;
        this.newShoutSuggestedInterface = newShoutSuggestedInterface;
    }

    @NonNull
    @Override
    public NewShoutSuggestionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_shout_suggestion, parent, false);
        return new NewShoutSuggestionsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NewShoutSuggestionsAdapter.ViewHolder holder, final int position) {
        holder.newSuggestedTextView.setText(newShoutSuggestedArrayalist.get(position).getShoutMessage());
        holder.addShoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newShoutSuggestedInterface.newShoutSentence(newShoutSuggestedArrayalist.get(position).getShoutMessage());
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return newShoutSuggestedArrayalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView newSuggestedTextView;
        RelativeLayout addShoutBtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            newSuggestedTextView=itemView.findViewById(R.id.newSuggestedTextView);
            addShoutBtn=itemView.findViewById(R.id.addShoutBtn);

        }
    }
}
