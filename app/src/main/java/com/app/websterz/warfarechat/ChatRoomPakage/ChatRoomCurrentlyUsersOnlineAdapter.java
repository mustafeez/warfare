package com.app.websterz.warfarechat.ChatRoomPakage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.TestingPackage.NewPrivateChat;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ChatRoomCurrentlyUsersOnlineAdapter extends RecyclerView.Adapter<ChatRoomCurrentlyUsersOnlineAdapter.ViewHolder> {
    ArrayList<ChatRoomCurrentlyUsersOnlineViewModelClass>TotalOnlineUsersList=new ArrayList<>();
    Context MyContext;
    MySingleTon mySingleTon=MySingleTon.getInstance();
    TextView chatroom_recentChat_user_name;
    LinearLayout chatting,kicking,profileing,adding_friend,ParentLayout;
    TextView errorDialogText1,errorDialogText2;
    ImageView friend_added_imageview;
    Button errorDialogTryAgainBtn;
    AlertDialog MyalertDialog;
    String CountryName;

    public ChatRoomCurrentlyUsersOnlineAdapter(ArrayList<ChatRoomCurrentlyUsersOnlineViewModelClass>TotalOnlineUsersList, Context context, String CountryName) {
        this.TotalOnlineUsersList=TotalOnlineUsersList;
        MyContext=context;
        this.CountryName=CountryName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatroom_currently_online_users_row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.MyAvatarLoader.setVisibility(View.VISIBLE);
        viewHolder.MyAvatarLoader.setAnimating(true);

        Glide.with(viewHolder.itemView.getContext()).load(baseUrl+TotalOnlineUsersList.get(i).getChatRoom_userlist_username_dp()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }
            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                viewHolder.MyAvatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).into(viewHolder.chatroom_users_image);
        viewHolder.chatroom_user_name.setText(TotalOnlineUsersList.get(i).getChatRoom_userlist_username());
        if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_role().equals("1")) {
            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.user_icon).into(viewHolder.chatroom_user_rank);
            viewHolder.chatroom_user_name.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorBLue));
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_role().equals("2")) {
            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mod_icon).into(viewHolder.chatroom_user_rank);
            viewHolder.chatroom_user_name.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorRed));
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_role().equals("3")) {
            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.staff_icon).into(viewHolder.chatroom_user_rank);
            viewHolder.chatroom_user_name.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorOrange));
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_role().equals("4")) {
            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mentor_icon).into(viewHolder.chatroom_user_rank);
            viewHolder.chatroom_user_name.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorGreen));
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_role().equals("5")) {
            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.merchant_icon).into(viewHolder.chatroom_user_rank);
            viewHolder.chatroom_user_name.setTextColor(viewHolder.itemView.getResources().getColor(R.color.violet));
        }

        viewHolder.chatroom_user_name.setText(TotalOnlineUsersList.get(i).getChatRoom_userlist_username());

        if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_age().equals("1")) {
            if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_gender().equals("M")) {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_male).into(viewHolder.chatroom_user_age);
            } else {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_female).into(viewHolder.chatroom_user_age);
            }
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_age().equals("2")) {
            if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_gender().equals("M")) {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_male).into(viewHolder.chatroom_user_age);
            } else {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_female).into(viewHolder.chatroom_user_age);
            }
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_age().equals("3")) {
            if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_gender().equals("M")) {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_male).into(viewHolder.chatroom_user_age);
            } else {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_female).into(viewHolder.chatroom_user_age);
            }
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_age().equals("4")) {
            if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_gender().equals("M")) {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_male).into(viewHolder.chatroom_user_age);
            } else {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_female).into(viewHolder.chatroom_user_age);
            }
        } else if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_age().equals("5")) {
            if (TotalOnlineUsersList.get(i).getChatRoom_userlist_username_gender().equals("M")) {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_male).into(viewHolder.chatroom_user_age);
            } else {
                Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_female).into(viewHolder.chatroom_user_age);
            }
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(TotalOnlineUsersList.get(i).getChatRoom_userlist_username().equals(mySingleTon.username)))
                {
                    LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
                    v = layoutInflater.inflate(R.layout.popup_windows_chatroom_currently_online_user, null);
                    final AlertDialog.Builder adb = new AlertDialog.Builder(v.getContext());
                    final AlertDialog alertDialog = adb.create();
                    alertDialog.setView(v);
                    chatting=v.findViewById(R.id.buzz);
                    chatroom_recentChat_user_name=v.findViewById(R.id.chatroom_user_name);
                    kicking=v.findViewById(R.id.block_friend);
                    profileing=v.findViewById(R.id.delete_friend);
                    adding_friend=v.findViewById(R.id.view_profile);
                    chatroom_recentChat_user_name.setText(TotalOnlineUsersList.get(i).getChatRoom_userlist_username());
                    profileing.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(viewHolder.itemView.getContext(), UserFriendProfile.class);
                            intent.putExtra("username", TotalOnlineUsersList.get(i).getChatRoom_userlist_username());
                            viewHolder.itemView.getContext().startActivity(intent);
                            alertDialog.dismiss();
                        }
                    });
                    chatting.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MySocketsClass.static_objects.SendingChatList.clear();
                            Intent intent=new Intent(viewHolder.itemView.getContext(), NewPrivateChat.class);
                            intent.putExtra("FriendChatName",TotalOnlineUsersList.get(i).getChatRoom_userlist_username());
                            intent.putExtra("BackActivity","contactsChats");
                            viewHolder.itemView.getContext().startActivity(intent);
                            alertDialog.dismiss();
                        }
                    });
                    adding_friend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject jsonObject = new JSONObject();
                            try {

                                jsonObject.put("add_username", TotalOnlineUsersList.get(i).getChatRoom_userlist_username());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.off("add contact response");
                            mSocket.emit("add contact", jsonObject);
                            mSocket.on("add contact response",adding_contact_response);
                            alertDialog.dismiss();
                        }
                    });
                    kicking.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("chatroom", CountryName);
                                jsonObject.put("message","/kick "+TotalOnlineUsersList.get(i).getChatRoom_userlist_username());
                                jsonObject.put("chat_type","1");
                                mSocket.emit(MySocketsClass.EmmittersClass.SEND,jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();

                }

            }
            Emitter.Listener adding_contact_response=new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    final JSONObject jsonObject=(JSONObject)args[0];
                    ((Activity)MyContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            LayoutInflater layoutInflater = LayoutInflater.from(MyContext);
                            View myview = layoutInflater.inflate(R.layout.connection_error_dialog, null);
                            final AlertDialog.Builder adb = new AlertDialog.Builder(myview.getContext());
                            MyalertDialog  = adb.create();
                            MyalertDialog.setView(myview);
                            try {
                                if (jsonObject.getString("success").equals("1")) {
                                    errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                                    errorDialogText1.setVisibility(View.GONE);
                                    friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                                    friend_added_imageview.setVisibility(View.VISIBLE);
                                    friend_added_imageview.setImageResource(R.drawable.add_user_green);
                                    friend_added_imageview.setColorFilter(ContextCompat.getColor(MyContext,R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                                    errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                                    ParentLayout = myview.findViewById(R.id.ParentLayout);
                                    ParentLayout.setBackgroundResource(R.drawable.square_new_theme_green);
                                    try {
                                        errorDialogText2.setText(jsonObject.getString("message"));
                                        errorDialogText2.setTextColor(viewHolder.itemView.getResources().getColor(R.color.black));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                                    errorDialogTryAgainBtn.setText("Ok");
                                    errorDialogTryAgainBtn.setBackground(viewHolder.itemView.getResources().getDrawable(R.drawable.error_dialog_green_btn));
                                }
                                else {
                                    ParentLayout = myview.findViewById(R.id.ParentLayout);
                                    ParentLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);
                                    errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                                    errorDialogText1.setVisibility(View.GONE);
                                    friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                                    friend_added_imageview.setVisibility(View.VISIBLE);
                                    friend_added_imageview.setImageResource(R.drawable.sad_icon);
                                    errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                                    try {
                                        errorDialogText2.setText(jsonObject.getString("message"));
                                        errorDialogText2.setTextColor(viewHolder.itemView.getResources().getColor(R.color.black));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                                    errorDialogTryAgainBtn.setText("Ok");
                                    errorDialogTryAgainBtn.setBackground(viewHolder.itemView.getResources().getDrawable(R.drawable.error_dialog_red_btn));
                                }
                                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        MyalertDialog.dismiss();
                                        ContactsFragment contactsFragment = new ContactsFragment();
                                        contactsFragment.getContacts();

                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(!((Activity) MyContext).isFinishing())
                            MyalertDialog.show();
                        }
                    });

                }
            };
        });
    }

    @Override
    public int getItemCount() {
        return TotalOnlineUsersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView chatroom_users_image;
        ImageView chatroom_user_age,chatroom_user_rank;
        TextView chatroom_user_name;
        AvatarView MyAvatarLoader;

        public ViewHolder(View itemView) {
            super(itemView);
            chatroom_users_image=itemView.findViewById(R.id.chatroom_users_image);
            chatroom_user_age=itemView.findViewById(R.id.chatroom_user_age);
            chatroom_user_rank=itemView.findViewById(R.id.chatroom_user_rank);
            chatroom_user_name=itemView.findViewById(R.id.chatroom_user_name);
            MyAvatarLoader=itemView.findViewById(R.id.MyAvatarLoader);
        }
    }

}
