package com.app.websterz.warfarechat.ChatRoomPakage;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.Date;

public class MessageModelClass_ChatRoom implements IMessage,
        MessageContentType.Image, /*this is for default image messages implementation*/
        MessageContentType /*and this one is for custom content type (in this case - voice message)*/ {

    private String id;
    private String text;
    private Date createdAt;
    private UserModelClass_ChatRoom user;
    private Image image;
    private Voice voice;
    private String Myname;
    private String Myrole;
    private String Mygender;
    private String Myage;


    public MessageModelClass_ChatRoom(String id, UserModelClass_ChatRoom user, String text, String Myname,String Myrole,String Mygender,String Myage) {
        this(id, user, text, new Date(), Myname, Myrole, Mygender, Myage);
    }

    public MessageModelClass_ChatRoom(String id, UserModelClass_ChatRoom user, String text, Date createdAt,String Myname,String Myrole,String Myage, String Mygender) {
        this.id = id;
        this.text = text;
        this.user = user;
        this.createdAt = createdAt;
        this.Myname = Myname;
        this.Myrole = Myrole;
        this.Mygender = Mygender;
        this.Myage = Myage;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return text;
    }






    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public String getMyname() {
        return Myname;
    }

    @Override
    public String getMyrole() {
        return Myrole;
    }

    @Override
    public String getMygender() {
        return Mygender;
    }

    @Override
    public String getMyage() {
        return Myage;
    }

    @Override
    public UserModelClass_ChatRoom getUser() {
        return this.user;
    }

    @Override
    public String getImageUrl() {
        return image == null ? null : image.url;
    }

    public Voice getVoice() {
        return voice;
    }

    public String getStatus() {
        return "Sent";
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setMyname(String Myname) {
        this.Myname = Myname;
    }
    public void setMyrole(String Myrole) {
        this.Myrole = Myrole;
    }
    public void setMygender(String Mygender) {
        this.Mygender = Mygender;
    }
    public void setMyage(String Myage) {
        this.Myage = Myage;
    }


    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }



    public void setImage(Image image) {
        this.image = image;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public static class Image {

        private String url;

        public Image(String url) {
            this.url = url;
        }
    }

    public static class Voice {

        private String url;
        private int duration;

        public Voice(String url, int duration) {
            this.url = url;
            this.duration = duration;
        }

        public String getUrl() {
            return url;
        }

        public int getDuration() {
            return duration;
        }
    }
}
