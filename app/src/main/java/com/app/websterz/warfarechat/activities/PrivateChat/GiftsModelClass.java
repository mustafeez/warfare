package com.app.websterz.warfarechat.activities.PrivateChat;

public class GiftsModelClass {
    private String FriendName,command,imgurl,cost,name,type;

    public GiftsModelClass(String command, String imgurl, String cost, String name, String friend_name) {
        this.command = command;
        this.imgurl = imgurl;
        this.cost = cost;
        this.name = name;
        FriendName=friend_name;
    }

    public String getFriendName() {
        return FriendName;
    }

    public String getCommand() {
        return command;
    }

    public String getImgurl() {
        return imgurl;
    }

    public String getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }


    public void setCommand(String command) {
        this.command = command;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFriendName(String friendName) {
        FriendName = friendName;
    }
}
