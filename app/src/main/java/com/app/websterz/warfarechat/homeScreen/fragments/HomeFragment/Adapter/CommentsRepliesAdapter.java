package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.ExplainingShoutActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.ShoutSharingInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.CommentsSectionModelClass;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;
import com.tooltip.Tooltip;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class CommentsRepliesAdapter extends RecyclerView.Adapter<CommentsRepliesAdapter.ViewHolder> {

    ArrayList<CommentsSectionModelClass> CommentsArraylist = new ArrayList<>();
    Context MyContext;
    MySingleTon mySingleTon = MySingleTon.getInstance();
    ShoutSharingInterface shoutSharingInterface;
    LinearLayout MoreOptionsLayoutInstance = null;
    private ViewHolder MyViewHolder;
    Client MyClient;
    String fullDate;
    Tooltip tooltip;
    String deletingCommentId;

    public CommentsRepliesAdapter(ArrayList CommentsArraylist, Context MyContext, ShoutSharingInterface shoutSharingInterface) {
        this.CommentsArraylist = CommentsArraylist;
        this.MyContext = MyContext;
        this.shoutSharingInterface = shoutSharingInterface;
        MyClient = new Client(MyContext);
    }

    @NonNull
    @Override
    public CommentsRepliesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shout_comments_row_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CommentsRepliesAdapter.ViewHolder holder, final int position) {

        if (CommentsArraylist.size()==0){

            holder.noCOmmentsFound.setVisibility(View.VISIBLE);
            holder.commentLayout.setVisibility(View.GONE);
        }
        else {
            holder.noCOmmentsFound.setVisibility(View.GONE);
            holder.commentLayout.setVisibility(View.VISIBLE);
        //  Picasso.get().load(baseUrl + CommentsArraylist.get(position).getDp()).into(holder.UsersPictureView);
        GlideApp.with(holder.itemView.getContext()).load(baseUrl + CommentsArraylist.get(position).getDp()).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).
                into(holder.UsersPictureView);
        holder.UserRealName.setText(CommentsArraylist.get(position).getName());
        holder.UserAccountName.setText("@" + CommentsArraylist.get(position).getUsername());
        holder.UserPostingTime.setText(GettingActualTime(Long.valueOf(CommentsArraylist.get(position).getTime())));
        if (CommentsArraylist.get(position).getText().contains("www.youtube.com") ||
                CommentsArraylist.get(position).getText().contains("https://youtu.be/")) {
            String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(CommentsArraylist.get(position).getText());
            holder.YoutubeVideoImage.setVisibility(View.VISIBLE);
            holder.YoutubeUrlImageViewCardview.setVisibility(View.VISIBLE);
            holder.UserPostedComment.setText(CommentsArraylist.get(position).getText());
            Glide.with(holder.itemView.getContext()).load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/default.jpg").into(holder.YoutubeVideoImage);
        } else {
            holder.YoutubeVideoImage.setVisibility(View.GONE);
            holder.YoutubeUrlImageViewCardview.setVisibility(View.GONE);
            holder.UserPostedComment.setText(MyClient.shortnameToUnicode(CommentsArraylist.get(position).getText()));
        }

        //region Showing user Rank
        if (CommentsArraylist.get(position).getRole().equals("1")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.user_icon).into(holder.UserRank);
        } else if (CommentsArraylist.get(position).getRole().equals("2")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.mod_icon).into(holder.UserRank);
        } else if (CommentsArraylist.get(position).getRole().equals("3")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.staff_icon).into(holder.UserRank);
        } else if (CommentsArraylist.get(position).getRole().equals("4")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.mentor_icon).into(holder.UserRank);
        } else if (CommentsArraylist.get(position).getRole().equals("5")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.merchant_icon).into(holder.UserRank);
        }
        //endregion
        //region Showing user age
        if (CommentsArraylist.get(position).getAge().equals("1")) {
            if (CommentsArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.born_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.born_female).into(holder.UserAge);
            }
        } else if (CommentsArraylist.get(position).getAge().equals("2")) {
            if (CommentsArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.teen_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.teen_female).into(holder.UserAge);
            }
        } else if (CommentsArraylist.get(position).getAge().equals("3")) {
            if (CommentsArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.young_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.young_female).into(holder.UserAge);
            }
        } else if (CommentsArraylist.get(position).getAge().equals("4")) {
            if (CommentsArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.adult_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.adult_female).into(holder.UserAge);
            }
        } else if (CommentsArraylist.get(position).getAge().equals("5")) {
            if (CommentsArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.mature_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.mature_female).into(holder.UserAge);
            }
        }
        //endregion

        holder.RowItemsMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //    MoreOptionsView(holder,position);
                MyMoreOptionsView(CommentsArraylist.get(position).getOwner(), v, position);
            }
        });

//        holder.MoreOptionsLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(MyContext, "Report Clicked", Toast.LENGTH_SHORT).show();
//            }
//        });
        holder.UserAccountName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoutSharingInterface.shoutDeletingMethod(position);
            }
        });
        holder.YoutubeUrlImageViewCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YouTubeOpening(CommentsArraylist.get(position).getText());
            }
        });
        holder.UserPostingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tooltip != null && tooltip.isShowing()) {
                    tooltip.dismiss();
                }
                if (fullDate != null) {
                    if (position != CommentsArraylist.size() - 1) {
                        tooltip = new Tooltip.Builder(v)
                                .setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.new_theme_green))
                                .setTextColor(holder.itemView.getContext().getResources().getColor(R.color.colorWhite))
                                .setText(fullDate).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (tooltip.isShowing()) {
                                    tooltip.dismiss();
                                }
                            }
                        }, 2000);
                    } else {
                        tooltip = new Tooltip.Builder(v)
                                .setGravity(Gravity.TOP)
                                .setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.new_theme_green))
                                .setTextColor(holder.itemView.getContext().getResources().getColor(R.color.colorWhite))
                                .setText(fullDate).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (tooltip.isShowing()) {
                                    tooltip.dismiss();
                                }
                            }
                        }, 2000);
                    }
                }
            }
        });
//        holder.DeleteOrReportCommentTextview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (CommentsArraylist.get(position).getOwner().equals("1")
//                ||CommentsArraylist.get(position).getRole().equals("2")
//                ||CommentsArraylist.get(position).getRole().equals("3")) {
//                    DeletingComment(CommentsArraylist.get(position).getCommentid());
//                }
//            }
//        });
    }
    }

    private void MyMoreOptionsView( String OwnerValue,View v,final int position){

        if (OwnerValue.equals("1"))
        {
            PopupMenu popupMenu = new PopupMenu(MyContext, v);
            popupMenu.inflate(R.menu.comments_row_delete_comment);
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (CommentsArraylist.get(position).getOwner().equals("1")
                            ||CommentsArraylist.get(position).getRole().equals("2")
                            ||CommentsArraylist.get(position).getRole().equals("3")) {
                        DeletingComment(CommentsArraylist.get(position).getCommentid());
                        return true;
                    }
                    return false;
                }
            });
        }
        else {
            PopupMenu popupMenu=new PopupMenu(MyContext,v);
            popupMenu.inflate(R.menu.comments_row_report_comment);
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                 //   if (CommentsArraylist.get(position).getOwner().equals("1")
//                            ||CommentsArraylist.get(position).getRole().equals("2")
//                            ||CommentsArraylist.get(position).getRole().equals("3")) {
                    ReportingShoutComments(CommentsArraylist.get(position).getCommentid(),CommentsArraylist.get(position).getShoutID());
                        return true;
                //    }
               //     return false;
                }
            });
        }
    }


    private void ReportingShoutComments(String CommentID,String ShoutID)
    {
        Toast.makeText(MySocketsClass.static_objects.MyStaticActivity, "Reporting comment", Toast.LENGTH_SHORT).show();
        GiftsReceivingActivityPopup giftsReceivingActivityPopup=new GiftsReceivingActivityPopup();
        giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity,
                null,"reporting_comment",null,CommentID,ShoutID);
    }

    private void DeletingComment(String ComentID){
        JSONObject jsonObject=new JSONObject();
        try {
            deletingCommentId=ComentID;
            jsonObject.put("id",ComentID);
            Log.e("shoutComentdTest","emiting id> "+ComentID);
            mSocket.off(MySocketsClass.ListenersClass.DELETE_COMMENTS_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.DELETE_COMMENT,jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.DELETE_COMMENTS_RESPONSE,delete_comments_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private String GettingActualTime(Long StampedDate){

        //finding Time...
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(StampedDate * 1000);
        String date = DateFormat.format("EEE d, MMM yyyy hh:mm a", cal).toString();
        String today_time = DateFormat.format("hh:mm a", cal).toString();
        String date2 = DateFormat.format("yyyy / MM / d", cal).toString();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / d");
        String current_date = mdformat.format(calendar.getTime());

        //finding day/date/days ago...
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;
        long last_online_time2 = StampedDate * 1000;
        String last_final_time_reg = null;

        long now = System.currentTimeMillis();
                final long diff2 = now - last_online_time2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "just now";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "a minute ago";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "an hour ago ";
            fullDate=today_time;
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago ";
            fullDate=today_time;
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "yesterday at "+today_time;
            fullDate=today_time;
        } else {

            long calculating_months=diff2 / DAY_MILLIS;
            if (calculating_months<30)
            {
                last_final_time_reg = diff2 / DAY_MILLIS + " days ago ";
                fullDate=date;
            }
            else if (calculating_months>=30&&calculating_months<=360)
            {
                last_final_time_reg=calculating_months/30 +" Month(s) ago ";
                fullDate=date;
            }
            else if (calculating_months>=30&&calculating_months>360)
            {
                last_final_time_reg=calculating_months/360 +" Year(s) ago ";
                fullDate=date;
            }
        }

        return last_final_time_reg;
    }

//    private void MoreOptionsView(ViewHolder holder,int position){
//        if (MoreOptionsLayoutInstance != null && holder != MyViewHolder) {
//            MoreOptionsLayoutInstance.setVisibility(View.GONE);
//            MoreOptionsLayoutInstance = null;
//        }
//        MyViewHolder = holder;
//
//        if (holder.MoreOptionsLayout.getVisibility() == View.VISIBLE) {
//            holder.MoreOptionsLayout.setVisibility(View.GONE);
//        } else {
//            if (CommentsArraylist.get(position).getOwner().equals("1")) {
//                holder.DeleteOrReportCommentTextview.setText("Delete this comment");
//            }
//            else {
//                holder.DeleteOrReportCommentTextview.setText("Report");
//            }
//            holder.MoreOptionsLayout.setVisibility(View.VISIBLE);
//        }
//
//
//        MoreOptionsLayoutInstance=holder.MoreOptionsLayout;
//
//    }
    private String GettingVideoIdFromYoutubeLink(String url) {
        if (url.contains("v=")) {
            if (url.contains("&")) {
                String[] u = url.split("v=");
                u = u[1].split("&");
                url = u[0];
            } else {
                String[] u = url.split("v=");
                url = u[1];
            }
        } else {
            String[] u = url.split("be/");
            url = u[1];
        }
        return url;
    }
    private void YouTubeOpening(String UserShoutedtext){
        try {
            MyContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(UserShoutedtext)));
        }
        catch (Exception ignore){
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int rowValue=0;
        if (CommentsArraylist.size()==0){
            rowValue=1;
        }
        else {
            rowValue=CommentsArraylist.size();
        }

        return rowValue;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView UsersPictureView;
      //  LinearLayout MoreOptionsLayout;
        CardView YoutubeUrlImageViewCardview;
        ImageView UserRank, UserAge, YoutubeVideoImage, RowItemsMoreOptions;
        RelativeLayout commentLayout;
        TextView UserRealName, UserAccountName, UserPostingTime, UserPostedComment, noCOmmentsFound;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            UsersPictureView = itemView.findViewById(R.id.UsersPictureView);
       //     MoreOptionsLayout = itemView.findViewById(R.id.MoreOptionsLayout);
            YoutubeUrlImageViewCardview = itemView.findViewById(R.id.YoutubeUrlImageViewCardview);
            UserRank = itemView.findViewById(R.id.UserRank);
            UserAge = itemView.findViewById(R.id.UserAge);
            YoutubeVideoImage = itemView.findViewById(R.id.YoutubeVideoImage);
            RowItemsMoreOptions = itemView.findViewById(R.id.RowItemsMoreOptions);
            UserRealName = itemView.findViewById(R.id.UserRealName);
            UserAccountName = itemView.findViewById(R.id.UserAccountName);
            UserPostingTime = itemView.findViewById(R.id.UserPostingTime);
            UserPostedComment = itemView.findViewById(R.id.UserPostedComment);
            noCOmmentsFound = itemView.findViewById(R.id.noCOmmentsFound);
            commentLayout = itemView.findViewById(R.id.commentLayout);
//            DeleteOrReportCommentTextview = itemView.findViewById(R.id.DeleteOrReportCommentTextview);

        }
    }

    Emitter.Listener delete_comments_response=new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            ((Activity)MyContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
            try {
                JSONObject jsonObject=(JSONObject)args[0];
                String success=jsonObject.getString("success");
                if (success.equals("1"))
                {
                    String id=jsonObject.getString("id");
                    String total=jsonObject.getString("total");
                    String shoutID=jsonObject.getString("shoutID");
                    Log.e("shoutComentdTest","id> "+id);
                    Log.e("shoutComentdTest","total> "+total);
                    Log.e("shoutComentdTest","shoutID> "+shoutID);

                    for (int i=0;i<CommentsArraylist.size();i++)
                    {
                        Log.e("shoutComentTest","i value> "+i);
                        if (id.equals(CommentsArraylist.get(i).getCommentid()))
                        {
                            Log.e("shoutComentTest","removing> "+CommentsArraylist.get(i).getShoutID()+" at "+i);
                            CommentsArraylist.remove(i);
                        }
                    }

//                    for (CommentsSectionModelClass commentsSectionModelClass : CommentsArraylist){
//                        if (commentsSectionModelClass.getCommentid().equals(id)){
//                            CommentsArraylist.remove(commentsSectionModelClass);
//                        }
//                    }

                    //iteratr type side
//                    for (Iterator<CommentsSectionModelClass> it=CommentsArraylist.iterator();it.hasNext();){
//                        CommentsSectionModelClass commentsSectionModelClass=it.next();
//                        if (commentsSectionModelClass.getShoutID().equals(shoutID))
//                        {
//                            it.remove();
//                        }
//                    }
                    LocalBroadcastManager localBroadcastManager=LocalBroadcastManager.getInstance(MyContext);
                    Intent intent=new Intent("UpdatingValueofComments");
                    intent.putExtra("newTotalCommentValues",total);
                    localBroadcastManager.sendBroadcast(intent);
                    notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
                }
            });
        }
    };
}
