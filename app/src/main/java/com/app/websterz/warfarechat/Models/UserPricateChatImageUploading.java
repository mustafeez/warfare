package com.app.websterz.warfarechat.Models;

import com.google.gson.annotations.SerializedName;
public class UserPricateChatImageUploading
{

    @SerializedName("message")
    private String message;

    @SerializedName("success")
    private int success;

    @SerializedName("url")
    private String url;

    public String getMessage() {
        return message;
    }

    public int getSuccess() {
        return success;
    }

    public String getUrl() {
        return url;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}


