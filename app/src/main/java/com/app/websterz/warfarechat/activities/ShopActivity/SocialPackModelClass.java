package com.app.websterz.warfarechat.activities.ShopActivity;

public class SocialPackModelClass {

    String SocialPackImageView, SocialPackName, SocialPackDetail, SocialPackValidity, SocialPackCost,SocialPackSlug,SocialPackDate,SocialPackRemaining;

    public SocialPackModelClass(String socialPackImageView, String socialPackName, String socialPackDetail, String socialPackValidity, String socialPackCost, String socialPackSlug, String socialPackDate, String socialPackRemaining) {
        SocialPackImageView = socialPackImageView;
        SocialPackName = socialPackName;
        SocialPackDetail = socialPackDetail;
        SocialPackValidity = socialPackValidity;
        SocialPackCost = socialPackCost;
        SocialPackSlug = socialPackSlug;
        SocialPackDate = socialPackDate;
        SocialPackRemaining = socialPackRemaining;
    }

    public void setSocialPackImageView(String socialPackImageView) {
        SocialPackImageView = socialPackImageView;
    }

    public void setSocialPackName(String socialPackName) {
        SocialPackName = socialPackName;
    }

    public void setSocialPackDetail(String socialPackDetail) {
        SocialPackDetail = socialPackDetail;
    }

    public void setSocialPackValidity(String socialPackValidity) {
        SocialPackValidity = socialPackValidity;
    }

    public void setSocialPackCost(String socialPackCost) {
        SocialPackCost = socialPackCost;
    }

    public void setSocialPackSlug(String socialPackSlug) {
        SocialPackSlug = socialPackSlug;
    }

    public void setSocialPackDate(String socialPackDate) {
        SocialPackDate = socialPackDate;
    }

    public void setSocialPackRemaining(String socialPackRemaining) {
        SocialPackRemaining = socialPackRemaining;
    }

    public String getSocialPackImageView() {
        return SocialPackImageView;
    }

    public String getSocialPackName() {
        return SocialPackName;
    }

    public String getSocialPackDetail() {
        return SocialPackDetail;
    }

    public String getSocialPackValidity() {
        return SocialPackValidity;
    }

    public String getSocialPackCost() {
        return SocialPackCost;
    }

    public String getSocialPackSlug() {
        return SocialPackSlug;
    }

    public String getSocialPackDate() {
        return SocialPackDate;
    }

    public String getSocialPackRemaining() {
        return SocialPackRemaining;
    }
}
