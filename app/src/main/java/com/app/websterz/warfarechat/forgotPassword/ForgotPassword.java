package com.app.websterz.warfarechat.forgotPassword;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.customDialog.CustomDialog;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.signup.SignUp;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    Animation animShake;
    RelativeLayout forgotPasswordProgressRl, forgotPasswordLoginBtn, forgotPasswordSignUpBtn;
    ImageView forgotPasswordInfoBtn, forgotPasswordUsernameIv,testLoader;
    EditText forgotPasswordUsernameField;
    Button forgotPasswordSendBtn;
    AppConstants appConstants;
    Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initView();
    }

    private void initView() {
        forgotPasswordProgressRl = findViewById(R.id.forgotPasswordProgressRl);
        testLoader = findViewById(R.id.testLoader);
        Glide.with(ForgotPassword.this).load(R.drawable.socio_loader).into(testLoader);
        animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        appConstants = new AppConstants();
        mSocket = AppConstants.mSocket;
        mSocket.connect();
        Log.e("connectingg","fPaswrd 51");
        forgotPasswordUsernameIv = findViewById(R.id.forgotPasswordUsernameIv);
        forgotPasswordUsernameField = findViewById(R.id.forgotPasswordUsernameField);
        forgotPasswordLoginBtn = findViewById(R.id.forgotPasswordLoginBtn);
        forgotPasswordLoginBtn.setOnClickListener(this);
        forgotPasswordSignUpBtn = findViewById(R.id.forgotPasswordSignUpBtn);
        forgotPasswordSignUpBtn.setOnClickListener(this);
        forgotPasswordInfoBtn = findViewById(R.id.forgotPasswordInfoBtn);
        forgotPasswordInfoBtn.setOnClickListener(this);
        forgotPasswordSendBtn = findViewById(R.id.forgotPasswordSendBtn);
        forgotPasswordSendBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgotPasswordLoginBtn: {
                startActivity(new Intent(ForgotPassword.this, Login.class));
                break;
            }
            case R.id.forgotPasswordSignUpBtn: {
                startActivity(new Intent(ForgotPassword.this, SignUp.class));
                break;
            }
            case R.id.forgotPasswordInfoBtn: {
                CustomDialog customDialog = new CustomDialog();
                customDialog.showDialog(ForgotPassword.this, 0);
                break;
            }
            case R.id.forgotPasswordSendBtn: {
                if (validateForm()) {
                    forgotPasswordProgressRl.setVisibility(View.VISIBLE);
                    forgotPassword();
                }
                break;
            }
        }
    }

    private boolean validateForm() {
        if (forgotPasswordUsernameField.getText().toString().trim().equals("")) {
            forgotPasswordUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            forgotPasswordUsernameIv.setColorFilter(ContextCompat.getColor(ForgotPassword.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            forgotPasswordUsernameIv.startAnimation(animShake);
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!forgotPasswordUsernameField.getText().toString().trim().substring(0, 1).toLowerCase().matches("[a-zA-Z]+")) {
            forgotPasswordUsernameIv.startAnimation(animShake);
            forgotPasswordUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            forgotPasswordUsernameIv.setColorFilter(ContextCompat.getColor(ForgotPassword.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            Toast.makeText(this, "Username must start with a character", Toast.LENGTH_SHORT).show();
            return false;
        } else if (forgotPasswordUsernameField.getText().toString().trim().length() < 5 || forgotPasswordUsernameField.getText().toString().trim().length() > 15) {
            forgotPasswordUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            forgotPasswordUsernameIv.setColorFilter(ContextCompat.getColor(ForgotPassword.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            forgotPasswordUsernameIv.startAnimation(animShake);
            Toast.makeText(this, "Username length should be between 5 and 15", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            forgotPasswordUsernameIv.setImageResource(R.drawable.new_theme_input_border);
        }

        return true;
    }

    private void forgotPassword() {
        if (mSocket.connected()) {
            mSocket.emit("forgot_password", forgotPasswordUsernameField.getText().toString().trim());

            mSocket.on("forgot_password_response", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    ForgotPassword.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            JSONObject jsonObject = (JSONObject) args[0];
                       //     CustomDialogError customDialogError = new CustomDialogError();

                            try {
                                forgotPasswordProgressRl.setVisibility(View.GONE);
                                MySocketsClass.getInstance().showErrorDialog(ForgotPassword.this, "", jsonObject.getString("message"), "passwordReset","");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.w("w", jsonObject.toString());
                        }
                    });
                }
            });

        }

    }
}
