package com.app.websterz.warfarechat.homeScreen.coin_details;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.adapter.CoinDetailsAdapter;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.ads.rewarded.ServerSideVerificationOptions;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class Coins_details extends AppCompatActivity {
    RecyclerView coins_recyclerview;
    @SuppressLint("WrongConstant")
    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    JSONArray coins_array;
    TextView total_coins, earnCoins, dailyReward;
    RelativeLayout parent_layout;
    LinearLayout returning_imageview, parent_child_layout;
    private ImageView topGreenView, coinImage;
    //   CustomDialogError customDialogError;
    Button inviteFriendsBtn;
    private RewardedAd myRewardAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coins_details);

        initializations();
        loadAd();
        myCode();
        LocalBroadcastManager.getInstance(Coins_details.this).registerReceiver(sharinReceiver, new IntentFilter("ActivatingSharingOption"));
        earnCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showingLoader();
                showAd();
            }
        });
        inviteFriendsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteFriends();
            }
        });
    }

    private BroadcastReceiver sharinReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sharing();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.global_layout = parent_layout;
        MySocketsClass.static_objects.MyStaticActivity = Coins_details.this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(Coins_details.this).unregisterReceiver(sharinReceiver);
    }

    private void initializations() {
        //  customDialogError = new CustomDialogError();
        total_coins = findViewById(R.id.total_coins);
        coins_recyclerview = findViewById(R.id.coins_recyclerview);
        coins_recyclerview.setLayoutManager(layoutManager);
        parent_layout = findViewById(R.id.parent_layout);
        parent_child_layout = findViewById(R.id.parent_child_layout);
        earnCoins = findViewById(R.id.earnCoins);
        dailyReward = findViewById(R.id.dailyReward);
        topGreenView = findViewById(R.id.topGreenView);
        coinImage = findViewById(R.id.coinImage);
        inviteFriendsBtn = findViewById(R.id.inviteFriendsBtn);
    }

    private void myCode() {
        Glide.with(Coins_details.this).load(R.drawable.green_background_top).into(topGreenView);
        Glide.with(Coins_details.this).load(R.drawable.coin_image).into(coinImage);
        //  Glide.with(Coins_details.this).load(R.drawable.newapp_loader_spinning_rocket).into(loaderGifView);

        coins_array = new JSONArray();
        mSocket.off(MySocketsClass.ListenersClass.GET_MY_COINS_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.GET_MY_COINS);
        returning_imageview = MySocketsClass.custom_layout.Loader_image(Coins_details.this, parent_layout);
        mSocket.on(MySocketsClass.ListenersClass.GET_MY_COINS_RESPONSE, get_my_coins_response);
    }

    private String gettingUserName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Coins_details.this);
        return prefs.getString("mine_user_name", null);
    }

    private String gettingSocketId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Coins_details.this);
        return prefs.getString("sID", null);
    }

    private void showingErrorPopup() {
//        CustomDialogError customDialogError=new CustomDialogError();
        MySocketsClass.getInstance().showErrorDialog(Coins_details.this, "", "", "AdLoadFailed", "");
    }

    private void sharing() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Socio");
            String sAux = "Socio Text here.\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Choose one"));
        } catch (Exception e) {
            Toast.makeText(Coins_details.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void inviteFriends() {
        MySocketsClass.getInstance().showErrorDialog(Coins_details.this, "", "", "inviteAFriend", "");
    }

    private void hidingLoader() {
        if (returning_imageview.getVisibility() == View.VISIBLE) {
            returning_imageview.setVisibility(View.GONE);
            if (!earnCoins.isEnabled() && !dailyReward.isEnabled() && !inviteFriendsBtn.isEnabled()) {
                earnCoins.setEnabled(true);
                dailyReward.setEnabled(true);
                inviteFriendsBtn.setEnabled(true);
            }
        }
    }

    private void showingLoader() {
        if (returning_imageview.getVisibility() == View.GONE) {
            returning_imageview.setVisibility(View.VISIBLE);
            if (earnCoins.isEnabled() && dailyReward.isEnabled() && inviteFriendsBtn.isEnabled()) {
                earnCoins.setEnabled(false);
                dailyReward.setEnabled(false);
                inviteFriendsBtn.setEnabled(false);
            }
        }
    }


    private void loadAd() {
        myRewardAd = new RewardedAd(this, getResources().getString(R.string.test_reward_ad_id));
        RewardedAdLoadCallback callback = new RewardedAdLoadCallback() {
            @Override
            public void onRewardedAdLoaded() {
                super.onRewardedAdLoaded();
                ServerSideVerificationOptions options = new ServerSideVerificationOptions.Builder()
                        .setCustomData(gettingUserName()+"|"+gettingSocketId())
                        .build();
                myRewardAd.setServerSideVerificationOptions(options);
                Toast.makeText(Coins_details.this, "Loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
                super.onRewardedAdFailedToLoad(loadAdError);
            }

        };
        myRewardAd.loadAd(new AdRequest.Builder().build(), callback);
    }

    private void showAd() {
        if (myRewardAd.isLoaded()) {
            RewardedAdCallback callback = new RewardedAdCallback() {
                @Override
                public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                    hidingLoader();
                }

                @Override
                public void onRewardedAdOpened() {
                    super.onRewardedAdOpened();
                }

                @Override
                public void onRewardedAdClosed() {
                    super.onRewardedAdClosed();
                    hidingLoader();
                }

                @Override
                public void onRewardedAdFailedToShow(AdError adError) {
                    super.onRewardedAdFailedToShow(adError);
                    hidingLoader();
                }
            };
            myRewardAd.show(this, callback);
        }
    }

    private Emitter.Listener get_my_coins_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        hidingLoader();
                        parent_child_layout.setVisibility(View.VISIBLE);
                        String user_total_coin = jsonObject.getString("total_coins");

                        DecimalFormat formatter = new DecimalFormat("#,###,###");
                        String formated_user_total_coins = formatter.format(Double.parseDouble(user_total_coin));


                        total_coins.setText(formated_user_total_coins);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    coins_recyclerview.setAdapter(new CoinDetailsAdapter(jsonObject));
                }
            });
        }
    };

    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
        }
        return "";
    }

}
