package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses;

public class UserWallLayoutModelClass {
    private String PostsUserDp, PostsSimpleName, PostsUsername, PostsUserRole,
            PostUserAge, gender, PostsTime, PostsUserText, PostsTotallikes, PostsTotaldislikes, MyLikeOnPost,
            MyDisLikeOnPost,PostsTotalcomments, PostsTotalreshouts, Postsfollow, PostsId;

    public UserWallLayoutModelClass(String postsUserDp, String postsSimpleName, String postsUsername, String postsUserRole,
                                    String postUserAge, String gender, String postsTime, String postsUserText,
                                    String postsTotallikes, String postsTotaldislikes, String myLikeOnPost,
                                    String myDisLikeOnPost, String postsTotalcomments, String postsTotalreshouts,
                                    String Postsfollow, String PostsId) {
        this.PostsUserDp = postsUserDp;
        this.PostsSimpleName = postsSimpleName;
        this.PostsUsername = postsUsername;
        this.PostsUserRole = postsUserRole;
        this.PostUserAge = postUserAge;
        this.gender = gender;
        this.PostsTime = postsTime;
        this.PostsUserText = postsUserText;
        this.PostsTotallikes = postsTotallikes;
        this.PostsTotaldislikes = postsTotaldislikes;
        this.MyLikeOnPost = myLikeOnPost;
        this.MyDisLikeOnPost = myDisLikeOnPost;
        this.PostsTotalcomments = postsTotalcomments;
        this.PostsTotalreshouts = postsTotalreshouts;
        this.Postsfollow = Postsfollow;
        this.PostsId = PostsId;
    }

    public String getPostsUserDp() {
        return PostsUserDp;
    }

    public String getPostsSimpleName() {
        return PostsSimpleName;
    }

    public String getPostsUsername() {
        return PostsUsername;
    }

    public String getPostsUserRole() {
        return PostsUserRole;
    }

    public String getPostUserAge() {
        return PostUserAge;
    }

    public String getGender() {
        return gender;
    }

    public String getPostsTime() {
        return PostsTime;
    }

    public String getPostsUserText() {
        return PostsUserText;
    }

    public String getPostsTotallikes() {
        return PostsTotallikes;
    }

    public String getPostsTotaldislikes() {
        return PostsTotaldislikes;
    }

    public String getPostsfollow() {
        return Postsfollow;
    }

    public String getMyLikeOnPost() {
        return MyLikeOnPost;
    }

    public String getMyDisLikeOnPost() {
        return MyDisLikeOnPost;
    }

    public String getPostsTotalcomments() {
        return PostsTotalcomments;
    }

    public String getPostsTotalreshouts() {
        return PostsTotalreshouts;
    }

    public String getPostsId() {
        return PostsId;
    }

    public void setPostsId(String postsId) {
        PostsId = postsId;
    }

    public void setPostsUserDp(String postsUserDp) {
        PostsUserDp = postsUserDp;
    }

    public void setPostsSimpleName(String postsSimpleName) {
        PostsSimpleName = postsSimpleName;
    }

    public void setPostsUsername(String postsUsername) {
        PostsUsername = postsUsername;
    }

    public void setPostsUserRole(String postsUserRole) {
        PostsUserRole = postsUserRole;
    }

    public void setPostUserAge(String postUserAge) {
        PostUserAge = postUserAge;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPostsTime(String postsTime) {
        PostsTime = postsTime;
    }

    public void setPostsUserText(String postsUserText) {
        PostsUserText = postsUserText;
    }

    public void setPostsTotallikes(String postsTotallikes) {
        PostsTotallikes = postsTotallikes;
    }

    public void setPostsTotaldislikes(String postsTotaldislikes) {
        PostsTotaldislikes = postsTotaldislikes;
    }

    public void setMyLikeOnPost(String myLikeOnPost) {
        MyLikeOnPost = myLikeOnPost;
    }

    public void setMyDisLikeOnPost(String myDisLikeOnPost) {
        MyDisLikeOnPost = myDisLikeOnPost;
    }

    public void setPostsTotalcomments(String postsTotalcomments) {
        PostsTotalcomments = postsTotalcomments;
    }

    public void setPostsTotalreshouts(String postsTotalreshouts) {
        PostsTotalreshouts = postsTotalreshouts;
    }

    public void setPostsfollow(String postsfollow) {
        Postsfollow = postsfollow;
    }
}
