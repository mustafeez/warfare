package com.app.websterz.warfarechat.homeScreen.fragments.chatRooms.adapters;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.MoreSettings;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.activities.ChatRoomsChatActivity;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ChatRoomsRecyclerAdapter extends RecyclerView.Adapter<ChatRoomsRecyclerAdapter.ViewHolder> {

    List<String> countriesList, onlineUsersList;
    View view1;
    JSONObject chatRoomsObject;
    AppConstants appConstants;
    Activity activity;

    public ChatRoomsRecyclerAdapter(Activity activity, List<String> countriesList, List<String> onlineUsersList, JSONObject jsonObject) {
        this.countriesList = countriesList;
        this.onlineUsersList = onlineUsersList;
        chatRoomsObject = jsonObject;
        this.activity = activity;
        appConstants = new AppConstants();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_chat_rooms_recycler_adapter, parent, false);
        return new ViewHolder(view1);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.chatRoomsCountryTv.setText(countriesList.get(position));

        holder.onlineUsersTv.setText(" (" + onlineUsersList.get(position) + ")");


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {

                try {

                    //coment krna
//                    if (!isServiceRunningInForeground(holder.itemView.getContext(),Foreground_service.class)) {
//                        if (checkingRunningServiceStatus().equals("Run")) {
//                            Intent intent = new Intent(holder.itemView.getContext(), Foreground_service.class);
//                            holder.itemView.getContext().stopService(intent);
//                            Intent intent1 = new Intent(view.getContext(), Foreground_service.class);
//                            intent1.putExtra("ServiceType", "ForeGroundService");
//                            view.getContext().startService(intent1);
//                        }
//                    }
                Intent intent = new Intent(view.getContext(), ChatRoomsChatActivity.class);
                intent.putExtra("countryName", holder.chatRoomsCountryTv.getText().toString().trim());
                    intent.putExtra("TaglineName",chatRoomsObject.getJSONArray("chatrooms").getJSONObject(position).getString("tagline"));
                    view.getContext().startActivity(intent);
                    holder.itemView.setEnabled(false);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            holder.itemView.setEnabled(true);
                        }
                    },400 );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String checkingRunningServiceStatus(){
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(activity);
        return GettingInternetState.getString("ServiceStats", "NotRun");
    }

    private boolean isServiceRunningInForeground(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public int getItemCount() {
        return countriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView chatRoomsCountryTv, onlineUsersTv;

        public ViewHolder(View itemView) {

            super(itemView);

            chatRoomsCountryTv = itemView.findViewById(R.id.chatRoomsCountryTv);
            onlineUsersTv = itemView.findViewById(R.id.onlineUsersTv);
        }
    }
}
