package com.app.websterz.warfarechat.homeScreen.fragments.chatRooms.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.fragments.chatRooms.adapters.ChatRoomsRecyclerAdapter;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatRoomsFragment extends Fragment {

    View view;
    RecyclerView chatRoomsRv;
    AppConstants appConstants;
    List<String> onlineUsersList, MainCountriesList;
    ChatRoomsRecyclerAdapter chatRoomsRecyclerAdapter;
    JSONObject jsonObject;
    ArrayList<String> MyChatroomNames;



    public ChatRoomsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home_fragment2, container, false);

        if (mSocket!=null)
            mSocket.off(MySocketsClass.ListenersClass.LOAD_CHATROOM_RESPONSE);
       mSocket.emit(MySocketsClass.EmmittersClass.LOAD_CHATROOMS);
        initView();
        return view;
    }

    private void initView() {
        Log.e("Lasting","InFragment");
        appConstants = new AppConstants();
        chatRoomsRv = view.findViewById(R.id.chatRoomsRv);
        chatRoomsRv.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        onlineUsersList = new ArrayList<>();
        MainCountriesList = new ArrayList<>();
        MyChatroomNames=new ArrayList<>();

        if (mSocket!=null)
        mSocket.on(MySocketsClass.ListenersClass.LOAD_CHATROOM_RESPONSE, loadChatRoomsResponse);

    }

    Emitter.Listener loadChatRoomsResponse = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                         jsonObject = (JSONObject) args[0];
                         if (MyChatroomNames.size()>0)
                         {
                             MyChatroomNames.clear();
                         }

                        Log.e("Lasting","loadChatRoomsResponse");
                        try {
                            for (int i = 0; i < jsonObject.getJSONArray("chatrooms").length(); i++) {
                                MyChatroomNames.add(jsonObject.getJSONArray("chatrooms").getJSONObject(i).getString("name"));
                                mSocket.off(MySocketsClass.ListenersClass.ONLINE_USER_COUNTER);
                                mSocket.emit(MySocketsClass.EmmittersClass.ONLINE_USER_COUNTER, new JSONObject().put("chatroom",
                                        jsonObject.getJSONArray("chatrooms").getJSONObject(i).getString("name")));
                                Log.e("Lasting","Emitting->"+jsonObject.getJSONArray("chatrooms").getJSONObject(i).getString("name"));
                                mSocket.on(MySocketsClass.ListenersClass.ONLINE_USER_COUNTER,Online_User_Counter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    Emitter.Listener Online_User_Counter=new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject jsonObject3=(JSONObject)args[0];
            try {
                String chatRoom = jsonObject3.getString("chatroom");
                String onlineUsers = jsonObject3.getString("onlineUsers");
                Log.e("Lasting","Listening->"+chatRoom+"->"+onlineUsers);

                   if ( MyChatroomNames.contains(chatRoom))
                   {

                if (MainCountriesList.contains(chatRoom)) {

                        for (int i = 0; i < MainCountriesList.size(); i++) {
                            if (MainCountriesList.get(i).equals(chatRoom)) {
                                onlineUsersList.remove(i);
                                onlineUsersList.add(i, onlineUsers);
                                if (getActivity()!=null)
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        chatRoomsRecyclerAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                    } else {
                        MainCountriesList.add(chatRoom);
                        onlineUsersList.add(onlineUsers);
                        chatRoomsRecyclerAdapter = new ChatRoomsRecyclerAdapter(getActivity(), MainCountriesList, onlineUsersList, jsonObject);
                       if (getActivity()!=null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                chatRoomsRv.setAdapter(chatRoomsRecyclerAdapter);
                                chatRoomsRecyclerAdapter.notifyDataSetChanged();


                            }
                        });
                    }
                   }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


}


