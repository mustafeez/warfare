package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses;

public class NewSuggestedShoutModelClass {
    String shoutMessage;

    public NewSuggestedShoutModelClass(String shoutMessage) {
        this.shoutMessage = shoutMessage;
    }

    public String getShoutMessage() {
        return shoutMessage;
    }

    public void setShoutMessage(String shoutMessage) {
        this.shoutMessage = shoutMessage;
    }
}
