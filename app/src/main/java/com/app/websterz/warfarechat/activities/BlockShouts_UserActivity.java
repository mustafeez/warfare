package com.app.websterz.warfarechat.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers.ShoutsBlockerAdapter;
import com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers.ShoutsBlockerModelClass;
import com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers.TotalBlockUsers;
import com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers.UsersBlockerAdapter;
import com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers.UserBlockerModelClass;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;

import java.util.ArrayList;

public class BlockShouts_UserActivity extends AppCompatActivity implements TotalBlockUsers {

    RelativeLayout parentLinearLayout;
    ArrayList<UserBlockerModelClass> MyUsersBlockerList;
    ArrayList<ShoutsBlockerModelClass>MyShoutsBlockerList;
    RecyclerView BlockersRecycleriew;
    UsersBlockerAdapter usersBlockerAdapter;
    ShoutsBlockerAdapter shoutsBlockerAdapter;
    RecyclerView.LayoutManager layoutManager;
    TextView TotalBlockedUsers;
    String CategryTupe=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_shouts__user);
        Initializations();
        MyCode();
    }
    private void Initializations(){
        parentLinearLayout=findViewById(R.id.parentLinearLayout);
        TotalBlockedUsers=findViewById(R.id.TotalBlockedUsers);
        MyUsersBlockerList=new ArrayList<>();
        MyShoutsBlockerList=new ArrayList<>();
        BlockersRecycleriew=findViewById(R.id.BlockersRecycleriew);
        layoutManager=new LinearLayoutManager(BlockShouts_UserActivity.this, RecyclerView.VERTICAL, false);

    }
    private void MyCode(){
        Intent intent=getIntent();
         CategryTupe=intent.getStringExtra("BlockerListData");
        if (CategryTupe.equals("BlockedUserss")) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) MyUsersBlockerList = bundle.getParcelableArrayList("BlockerList");
            TotalBlockedUsers.setText("You have total "+MyUsersBlockerList.size()+ " blocked users.");
            usersBlockerAdapter = new UsersBlockerAdapter(MyUsersBlockerList, BlockShouts_UserActivity.this,this);
            BlockersRecycleriew.setLayoutManager(layoutManager);
            BlockersRecycleriew.setAdapter(usersBlockerAdapter);
        }
        else {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) MyShoutsBlockerList = bundle.getParcelableArrayList("BlockerList");
            TotalBlockedUsers.setText("You have total "+MyShoutsBlockerList.size()+ " hidden/blocked shouts.");
            shoutsBlockerAdapter = new ShoutsBlockerAdapter(MyShoutsBlockerList, BlockShouts_UserActivity.this,this);
            BlockersRecycleriew.setLayoutManager(layoutManager);
            BlockersRecycleriew.setAdapter(shoutsBlockerAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity=BlockShouts_UserActivity.this;
        MySocketsClass.static_objects.global_layout=parentLinearLayout;
    }

    @Override
    public void RemainingBlockers(String BlockNumbers) {
        if (CategryTupe.equals("BlockedUserss"))
        {
            TotalBlockedUsers.setText("You have total " + BlockNumbers + " blocked users.");
        }
        else {
            TotalBlockedUsers.setText("You have total " + BlockNumbers + " hidden/blocked shouts.");
        }
    }
}
