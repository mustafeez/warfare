package com.app.websterz.warfarechat.activities.PrivateChat;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.material.snackbar.Snackbar;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class UserPrivateChatGiftsRecyclerAdpater extends RecyclerView.Adapter<UserPrivateChatGiftsRecyclerAdpater.ViewHolder> {

    ArrayList<GiftsModelClass>GiftDetailList=new ArrayList<GiftsModelClass>();
    String ImageUrlPath="images/gifts/";
    RecyclerView myRecyclerviewView;
    Context Mycontext;

    public UserPrivateChatGiftsRecyclerAdpater(ArrayList<GiftsModelClass>giftDetailList,RecyclerView recyclerView,Context context) {
        GiftDetailList=giftDetailList;
        myRecyclerviewView=recyclerView;
        Mycontext = context;
    }

    @Override
    public UserPrivateChatGiftsRecyclerAdpater.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gifts_row_adapter_layout, parent, false);
        return new UserPrivateChatGiftsRecyclerAdpater.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserPrivateChatGiftsRecyclerAdpater.ViewHolder holder, final int position) {
        holder.MyAvatarLoader.setVisibility(View.VISIBLE);
        holder.MyAvatarLoader.setAnimating(true);
        holder.GiftsPrice.setText(GiftDetailList.get(position).getCost());
        holder.GiftsName.setText(GiftDetailList.get(position).getName());
//        Picasso.get().load(baseUrl+ImageUrlPath+GiftDetailList.get(position).getImgurl()).into(holder.GiftsImage, new Callback() {
//            @Override
//            public void onSuccess() {
//                holder.MyAvatarLoader.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError(Exception e) {
//
//            }
//        });

        GlideApp.with(holder.itemView.getContext()).load(baseUrl + ImageUrlPath+GiftDetailList.get(position).getImgurl()).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.MyAvatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(holder.GiftsImage);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView MainMessage,TotalCoins;
                Button SendGift,Close;
                ImageView GiftImage;

                final Dialog dialog = new Dialog(Mycontext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.gifts_popup);
                dialog.setCancelable(true);

//                if(MySocketsClass.static_objects.theme==0){
//
//                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                }
//                else if(MySocketsClass.static_objects.theme == 1){
//                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                    Date systemDate = Calendar.getInstance().getTime();
//                    String myDate = sdf.format(systemDate);
//                    try {
//                        Date Date1 = sdf.parse(myDate);
//                        Date Date2 = sdf.parse("06:00:00 pm");
//                        if(Date1.getTime() < Date2.getTime()){
//                            dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                        }
//                        else if(Date1.getTime() > Date2.getTime()){
//                            dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                        }
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//                else if(MySocketsClass.static_objects.theme ==2){
                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
 //               }
                MainMessage=dialog.findViewById(R.id.message_text);
                TotalCoins=dialog.findViewById(R.id.total_coins);
                SendGift=dialog.findViewById(R.id.send_gift);
                Close=dialog.findViewById(R.id.close);
                GiftImage=dialog.findViewById(R.id.gift_image);
                //Listeners
                MainMessage.setText("Send "+GiftDetailList.get(position).getName()+" to "+GiftDetailList.get(position).getFriendName());
                TotalCoins.setText(GiftDetailList.get(position).getCost());
                Glide.with(Mycontext).load(baseUrl+ImageUrlPath+GiftDetailList.get(position).getImgurl()).into(GiftImage);
                Close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                SendGift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JSONObject jsonObject=new JSONObject();
                        try {
                            jsonObject.put("username",GiftDetailList.get(position).getFriendName());
                            jsonObject.put("gift",GiftDetailList.get(position).getCommand());
                            jsonObject.put("type","user");
                            mSocket.emit(MySocketsClass.EmmittersClass.SEND_GIFT,jsonObject);
                            mSocket.on(MySocketsClass.ListenersClass.SEND_GIFT_RESPONSE,send_gift_response);
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                dialog.show();

            }
        });
    }

    Emitter.Listener send_gift_response=new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject=(JSONObject)args[0];
            try {
                String message=jsonObject.getString("message");
                String success=jsonObject.getString("success");
                if (success.equals("1"))
                {
                    Snackbar.make(myRecyclerviewView,message,Snackbar.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public int getItemCount() {
        return GiftDetailList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView GiftsImage;
        TextView GiftsName,GiftsPrice;
        AvatarView MyAvatarLoader;
        public ViewHolder(View itemView) {
            super(itemView);
            GiftsImage=itemView.findViewById(R.id.gift_img);
            GiftsName=itemView.findViewById(R.id.gift_name);
            GiftsPrice=itemView.findViewById(R.id.gifts_price);
            MyAvatarLoader=itemView.findViewById(R.id.MyAvatarLoader);
        }
    }
}
