package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.coin_details.Coins_details;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.DislikeShoutDetailAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.LikesShoutDetailsAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.Liking_DislikingShoutDetailsModelClass;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class MyShoutDetailsActivity extends Activity {

    ImageView  likesImageview, dislikesImageview, commentsImageview;
    TextView userShoutedTime,commentsTextview;
    LinearLayout likesLayout, dislikesLayout, commentsLayout;
    RecyclerView likeRecyclerview, dislikeRecyclerview;
    ArrayList<Liking_DislikingShoutDetailsModelClass> likesArraylist;
    ArrayList<Liking_DislikingShoutDetailsModelClass> dislikesArraylist;
    LinearLayoutManager linearLayoutManager,linearLayoutManager2;
    LikesShoutDetailsAdapter likesShoutDetailsAdapter;
    DislikeShoutDetailAdapter dislikeShoutDetailAdapter;
    RelativeLayout parent_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shout_details);
        Initializations();
        MyCode();
        likesImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likesLayoutShowing();
            }
        });
        dislikesImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislikesLayoutShowing();
            }
        });
        commentsImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentsLayoutShowing();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.global_layout = parent_layout;
        MySocketsClass.static_objects.MyStaticActivity = MyShoutDetailsActivity.this;
    }

    private void Initializations() {
        parent_layout = findViewById(R.id.parent_layout);
        likesImageview = findViewById(R.id.likesImageview);
        dislikesImageview = findViewById(R.id.dislikesImageview);
        commentsImageview = findViewById(R.id.commentsImageview);
        userShoutedTime = findViewById(R.id.userShoutedTime);
        likesLayout = findViewById(R.id.likesLayout);
        dislikesLayout = findViewById(R.id.dislikesLayout);
        commentsLayout = findViewById(R.id.commentsLayout);
        likeRecyclerview = findViewById(R.id.likeRecyclerview);
        dislikeRecyclerview = findViewById(R.id.dislikeRecyclerview);
        commentsTextview = findViewById(R.id.commentsTextview);

        likesArraylist = new ArrayList<>();
        dislikesArraylist = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(MyShoutDetailsActivity.this);
        linearLayoutManager2 = new LinearLayoutManager(MyShoutDetailsActivity.this);
        likeRecyclerview.setLayoutManager(linearLayoutManager);
        dislikeRecyclerview.setLayoutManager(linearLayoutManager2);
        likesShoutDetailsAdapter = new LikesShoutDetailsAdapter(likesArraylist, MyShoutDetailsActivity.this);
        dislikeShoutDetailAdapter = new DislikeShoutDetailAdapter(dislikesArraylist, MyShoutDetailsActivity.this);
        likeRecyclerview.setAdapter(likesShoutDetailsAdapter);
        dislikeRecyclerview.setAdapter(dislikeShoutDetailAdapter);
    }

    private void MyCode() {
        likesImageview.setImageResource(R.drawable.heart_solid);
        likesImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
        dislikesImageview.setImageResource(R.drawable.dislike_gray);
        commentsImageview.setImageResource(R.drawable.comments_green_btn);
        commentsImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.lytGray), PorterDuff.Mode.SRC_IN);
        Intent intent = getIntent();
        String MyId = intent.getStringExtra("MyId");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", MyId);
            mSocket.off(MySocketsClass.ListenersClass.GET_SHOUT_DETAILS_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.GET_SHOUT_DETAILS, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.GET_SHOUT_DETAILS_RESPONSE, get_shout_details_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void likesLayoutShowing(){
        if (likesLayout.getVisibility()!=View.VISIBLE)
        {
            likesLayout.setVisibility(View.VISIBLE);
            dislikesLayout.setVisibility(View.GONE);
            commentsLayout.setVisibility(View.GONE);

            likesImageview.setImageResource(R.drawable.heart_solid);
            likesImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
            dislikesImageview.setImageResource(R.drawable.dislike_gray);
            dislikesImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.lytGray), PorterDuff.Mode.SRC_IN);
            commentsImageview.setImageResource(R.drawable.comments_green_btn);
            commentsImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.lytGray), PorterDuff.Mode.SRC_IN);
        }
    }

    private void dislikesLayoutShowing(){
        if (dislikesLayout.getVisibility()!=View.VISIBLE)
        {
            likesLayout.setVisibility(View.GONE);
            dislikesLayout.setVisibility(View.VISIBLE);
            commentsLayout.setVisibility(View.GONE);

            likesImageview.setImageResource(R.drawable.heart_solid);
            likesImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.lytGray), PorterDuff.Mode.SRC_IN);
            dislikesImageview.setImageResource(R.drawable.dislike_black);
            dislikesImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
            commentsImageview.setImageResource(R.drawable.comments_green_btn);
            commentsImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.lytGray), PorterDuff.Mode.SRC_IN);
        }
    }

    private void CommentsLayoutShowing(){
        if (commentsLayout.getVisibility()!=View.VISIBLE)
        {
            likesLayout.setVisibility(View.GONE);
            dislikesLayout.setVisibility(View.GONE);
            commentsLayout.setVisibility(View.VISIBLE);

            likesImageview.setImageResource(R.drawable.heart_solid);
            likesImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.lytGray), PorterDuff.Mode.SRC_IN);
            dislikesImageview.setImageResource(R.drawable.dislike_gray);
            dislikesImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.lytGray), PorterDuff.Mode.SRC_IN);
            commentsImageview.setImageResource(R.drawable.comments_green_btn);
            commentsImageview.setColorFilter(ContextCompat.getColor(MyShoutDetailsActivity.this,R.color.new_theme_green), PorterDuff.Mode.SRC_IN);

        }
    }

    private String GettingPostingTime(long time_reg){
        String last_final_time_reg = null;
        long time_reg2 = time_reg * 1000;
        long now = System.currentTimeMillis();
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "shouted just now at ";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "shouted a minute ago at ";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg ="shouted "+ diff2 / MINUTE_MILLIS + " minutes ago at ";
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "shouted an hour ago at ";
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago at ";
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "shouted yesterday at ";
        } else {

            long calculating_months=diff2 / DAY_MILLIS;
            if (calculating_months<30)
            {
                last_final_time_reg = "shouted "+ diff2 / DAY_MILLIS + " days ago on ";
            }
            else if (calculating_months>=30&&calculating_months<=360)
            {
                last_final_time_reg="shouted "+ calculating_months/30 +" Month(s) ago on ";
            }
            else if (calculating_months>=30&&calculating_months>360)
            {
                last_final_time_reg="shouted "+ calculating_months/360 +" Year(s) ago on ";
            }
        }
        return last_final_time_reg;
    }

    private String GettingExactTime(long time)
    {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("EEE, d MMM yyyy hh:mm:ss a", cal).toString();
        return date;
    }


    Emitter.Listener get_shout_details_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    likesArraylist.clear();
                    dislikesArraylist.clear();
                    try {
                        JSONObject jsonObject = (JSONObject) args[0];
                        String time = jsonObject.getString("time");
                        String comments = jsonObject.getString("comments");
                        userShoutedTime.setText(GettingPostingTime(Long.parseLong(time))+GettingExactTime(Long.parseLong(time)));
                        commentsTextview.setText("There are "+comments+" comments posted on this shout.");
                        for (int i = 0; i < jsonObject.getJSONArray("likes").length(); i++) {
                            String dp = jsonObject.getJSONArray("likes").getJSONObject(i).getString("dp");
                            String username = jsonObject.getJSONArray("likes").getJSONObject(i).getString("username");
                            String date = jsonObject.getJSONArray("likes").getJSONObject(i).getString("date");
                            likesArraylist.add(new Liking_DislikingShoutDetailsModelClass(dp, username, date));
                        }
                        for (int i = 0; i < jsonObject.getJSONArray("dislikes").length(); i++) {
                            String dp = jsonObject.getJSONArray("dislikes").getJSONObject(i).getString("dp");
                            String username = jsonObject.getJSONArray("dislikes").getJSONObject(i).getString("username");
                            String date = jsonObject.getJSONArray("dislikes").getJSONObject(i).getString("date");
                            dislikesArraylist.add(new Liking_DislikingShoutDetailsModelClass(dp, username, date));
                        }
                        likesShoutDetailsAdapter.notifyDataSetChanged();
                        dislikeShoutDetailAdapter.notifyDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };
}
