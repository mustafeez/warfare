package com.app.websterz.warfarechat.mySingleTon;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.DataBaseChatRooms.DataBaseChatRoomTables;
import com.app.websterz.warfarechat.DataBaseChatRooms.DataBase_ChatRooms;
import com.app.websterz.warfarechat.FCM.NotificationModel;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.TestingPackage.MessageModelClass;
import com.app.websterz.warfarechat.activities.FullScreenImageView;
import com.app.websterz.warfarechat.activities.PrivateChat.ChatModelClass;
//import com.app.websterz.warfarechat.activities.PrivateChat.NewUserAdapter;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.activities.SendAttacks.SendAttackActivity;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.ChatsRecyclerViewAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.NewChatModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.app.websterz.warfarechat.landingScreen.MainActivity;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.myRoomDatabase.UserPrivateChatTables;
import com.app.websterz.warfarechat.myRoomDatabase.WarfareDatabase;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.bumptech.glide.Glide;
import com.example.stevenyang.snowfalling.SnowFlakesLayout;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import android.os.Handler;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import androidx.room.Room;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static android.content.Context.CLIPBOARD_SERVICE;
import static android.content.Context.VIBRATOR_SERVICE;
import static com.app.websterz.warfarechat.homeScreen.activities.HomeScreen.is_user_connected;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class MySocketsClass {

    public static MySocketsClass getInstance() {
        return new MySocketsClass();
    }

    public static class EmmittersClass {
        public static final String GET_MY_PROFILE = "get my profile";
        public static final String GET_MY_COINS = "get my coins";
        public static final String GET_SETTINGS = "get settings";
        public static final String UPDATE_MY_PROFILE = "update my profile";
        public static final String REQUESTING_BUZZ = "send buzz";
        public static final String GETTING_USER_PROFILE = "get user profile";
        public static final String ADD_LIKE = "add like";
        public static final String START_PRIVATE_CHAT = "start private chat";
        public static final String SEND_PRIVATE = "send private";
        public static final String BLOCK_USER = "block user";
        public static final String LOAD_GIFTS = "load gifts";
        public static final String SEND_GIFT = "send gift";
        public static final String GET_ATTACKS = "get attacks";
        public static final String GET_SHIELD_PACKAGES = "get shield packages";
        public static final String GET_ROOM_USERS = "get room_users";
        public static final String LOAD_OLD_MESSAGES = "load old messages";
        public static final String GET_ATTACK_LAUNCH = "launch attack";
        public static final String JOIN_ROOM = "join room";
        public static final String LEAVE_ROOM = "leave room";
        public static final String SEND = "send";
        public static final String BUY_SHIELD = "buy shield";
        public static final String GET_SHOP_PACKAGES = "get shop packages";
        public static final String BUY_SHOP = "buy shop";
       public static final String LOAD_NOTIFICATION = "load notifications";
        public static final String LOAD_CHATROOMS = "load chatrooms";
        public static final String READ_ALL_NOTIFICATIONS = "read all notifications";
        public static final String GRAB_COINS = "grab coins";
        public static final String ADD_USER = "add user";
        public static final String CHECK_OFFLINE_MESSAGES = "check offline msgs";
        public static final String APPROVE_ADD_REQUEST = "approve add request";
        public static final String VIEW_FOOT_PRINTS = "view footprints";
        public static final String VIEW_LIKES = "view likes";
        public static final String VIEW_GIFTS = "view gifts";
        public static final String VIEW_FOLLOWERS = "view followers";
        public static final String VIEW_FRIENDS = "view friends";
        public static final String ONLINE_USER_COUNTER = "online users counter";
        public static final String GET_TOTAL_ROOM_USER = "get total room users";
        public static final String LOAD_FRIENDS = "load friends";
        public static final String SEARCH_FRIEND = "search friend";
        public static final String LOAD_POSTS = "load posts";
        public static final String ADD_SHOUT = "add shout";
        public static final String FOLLOW_USER = "follow user";
        public static final String LIKE_SHOUT = "like shout";
        public static final String DISLIKE_SHOUT = "dislike shout";
        public static final String DELETE_SHOUT = "delete shout";
        public static final String BLOCK_SHOUT = "block shout";
        public static final String BLOCK_SHOUT_USER = "block shout user";
        public static final String GET_SHOUT = "get shout";
        public static final String ADD_COMMENT = "add comment";
        public static final String DELETE_SHOUT_IMAGE = "delete shout image";
        public static final String DELETE_SHOUT_Audio = "delete shout audio";
        public static final String GET_SHOUT_BLOCKED_USERS = "get shout blocked users";
        public static final String GET_BLOCKED_SHOUTS = "get blocked shouts";
        public static final String UNBLOCK_SHOUT_USER = "unblock shout user";
        public static final String UNBLOCK_SHOUT = "unblock shout";
        public static final String DELETE_COMMENT = "delete comment";
        public static final String SEND_ABUSE = "send abuse";
        public static final String GET_SHOUT_DETAILS = "get shout details";
        public static final String SEND_AUDIO = "send audio";
        public static final String DISCONNECT_GOOGLE = "disconnect google";
        public static final String DISCONNECT_FACEBOOK = "disconnect facebook";
        public static final String CONNECT_GOOGLE = "connect google";
        public static final String CONNECT_FACEBOOK = "connect facebook";
        public static final String LOAD_TRENDING = "load trending";
        public static final String UPLOAD_AUDIO_SHOUT = "upload audio shout";
    }

    public static class ListenersClass {
        public static final String GET_MY_PROFILE_RESPONSE = "my profile response";
        public static final String GET_MY_COINS_RESPONSE = "get my coins response";
        public static final String GET_SETTINGS_RESPONSE = "get settings response";
        public static final String UPDATE_PROFILE_RESPONSE = "update profile response";
        public static final String RECEIVING_BUZZ = "receive buzz";
        public static final String USER_PROFILE_RESPONSE = "user profile response";
        public static final String ADD_LIKE_RESPONSE = "add like response";
        public static final String LOAD_FRIEND_REQUESTS = "load friend requests";
        public static final String RECEIVE_LIKE = "receive like";
        public static final String START_PRIVATE_CHAT_RESPONSE = "start private chat response";
        public static final String UPDATE_USER_DP = "update user dp";
        public static final String RECEIVE_REWARD = "receive reward";
        public static final String RECEIVE_ATTACK = "receive attack";
        public static final String AGE_UP_RECEIVE_REWARD = "age up";
        public static final String LOAD_OLD_MESSAGES = "load old messages";
        public static final String RECEIVE_PRIVATE = "receive private";
        public static final String GET_ATTACK_RESPONSE = "get attacks response";
        public static final String GET_SHIELD_PACKAGES_RESPONSE = "get shield packages response";
        public static final String GET_ATTACK_LAUNCH_RESPONSE = "attack launched success";
        public static final String GET_BUY_SHIELD_RESPONSE = "buy shield response";
        public static final String GET_ROOM_USERS_RESPONSE = "get room_users response";
        public static final String LOAD_GIFTS_RESPONSE = "load gifts response";
        public static final String SEND_GIFT_RESPONSE = "send gift response";
        public static final String RECEIVE_GIFTS = "receive gift";
        public static final String RECEIVE_NOTIFICATION = "receive notification";
        public static final String GET_SHOP_PACKAGES_RESPONSE = "get shop packages response";
        public static final String LOGIN = "login";
        public static final String ERROR_MESSAGE = "error msg";
        public static final String LOGIN_FROM_ANOTHER_LOCATION = "login from other location";
        public static final String USER_JOINED = "user joined";
        public static final String ROOM_USERS = "room_users";
        public static final String MESSAGE = "message";
        public static final String USER_LEFT = "user left";
        public static final String SYSTEM_MESSAGE = "system msg";
        public static final String BUY_SHOP_RESPONSE = "buy shop response";
        public static final String LOAD_CHATROOM_RESPONSE = "load chatrooms response";
        public static final String LOAD_NOTIFICATIONS_IN_APP = "load notifications";
        public static final String RECONNECT = "reconnect";
        public static final String DISCONNECT = "disconnect";
        public static final String RELOAD_CONTACT_LIST = "reload contact list";
        public static final String RECONNECTERROR = "reconnect_error";
        public static final String UPDATE_COINS = "update coins";
        public static final String LOAD_OFFLINE_MESSAGES = "load offline msgs";
        public static final String ADD_NEW_CONTACT = "new add contact";
        public static final String USER_PRESENCE_CHANGE = "user presence change";
        public static final String VIEW_FOOTPRINTS_RESPONSE = "view footprints response";
        public static final String VIEW_LIKE_RESPONSE = "view likes response";
        public static final String VIEW_GIFTS_RESPONSE = "view gifts response";
        public static final String VIEW_FOLLOWERS_RESPONSE = "view followers response";
        public static final String VIEW_FriendS_RESPONSE = "view friends response";
        public static final String SUSSCESS_MESSAGE = "success msg";
        public static final String RECONNECT_ATTEMPT_POLLING = "reconnect_attempt";
        public static final String ONLINE_USER_COUNTER = "online users counter";
        public static final String LOAD_FRIENDS_RESPONSE = "load friends response";
        public static final String LOAD_POSTS_RESPONSE = "load posts response";
        public static final String ADD_SHOUT_RESPONSE = "add shout response";
        public static final String FOLLOW_USER_RESPONSE = "follow user response";
        public static final String LIKE_SHOUT_RESPONSE = "like shout response";
        public static final String DISLIKE_SHOUT_RESPONSE = "dislike shout response";
        public static final String DELETE_SHOUT_RESPONSE = "delete shout response";
        public static final String BLOCK_SHOUT_RESPONSE = "block shout response";
        public static final String BLOCK_SHOUT_USER_RESPONSE = "block shout user response";
        public static final String GET_SHOUT_RESPONSE = "get shout response";
        public static final String GET_COMMENTS_RESPONSE = "get comments response";
        public static final String ADD_COMMENT_RESPONSE = "add comment response";
        public static final String DELETE_SHOUT_IMAGE_RESPONSE = "delete shout image response";
        public static final String DELETE_SHOUT_Audio_RESPONSE = "delete shout audio response";
        public static final String GET_SHOUT_BLOCKED_USERS_RESPONSE = "get shout blocked users response";
        public static final String GET_BLOCKED_SHOUT_RESPONSE = "get blocked shouts response";
        public static final String UNBLOCK_SHOUT_USER_RESPONSE = "unblock shout user response";
        public static final String UNBLOCK_SHOUT_RESPONSE = "unblock shout response";
        public static final String DELETE_COMMENTS_RESPONSE = "delete comment response";
        public static final String UPDATE_SHOUTS_DETAILS = "update shout details";
        public static final String NEW_COMMENT = "new comment";
        public static final String SEND_ABUSE_RESPONSE = "send abuse response";
        public static final String GET_SHOUT_DETAILS_RESPONSE = "get shout details response";
        public static final String AUDIO_SENT = "audio sent";
        public static final String NEW_FRIEND_SUGGESTION = "new friend suggestion";
        public static final String NEW_ROOM_SUGGESTION = "new room suggestion";
        public static final String DISCONNECT_GOOGLE_RESPONSE = "disconnect google response";
        public static final String DISCONNECT_FACEBOOK_RESPONSE = "disconnect facebook response";
        public static final String CONNECT_GOOGLE_RESPONSE = "connect google response";
        public static final String CONNECT_FACEBOOK_RESPONSE = "connect facebook response";
        public static final String LOAD_TRENDING_RESPONSE = "load trending response";
        public static final String UPLOAD_AUDIO_SHOUT_RESPONSE = "upload audio shout response";
    }

    public static class static_objects {
      //  public static int theme;
        public static String attack_success;
        public static String one_time_reward = null;
        public static String one_time_buzzer = null;
        public static String global_username_delete = null;
        public static RecyclerView friendRequestRecycler;
        public static TextView homeScreenFriendRequestTv;
        public static RelativeLayout global_layout;

        public static ChatsRecyclerViewAdapter chatsRecyclerViewAdapter = new ChatsRecyclerViewAdapter(MySocketsClass.static_objects.NewUserMessageList);
        public static ArrayList<NewChatModelClass> NewUserMessageList = new ArrayList<NewChatModelClass>();
        public static ArrayList<ChatModelClass> SendingChatList = new ArrayList<ChatModelClass>();
        public static HashMap<String, Integer> hashMap = new HashMap<>();
        public static int FragmentPosition = 0;
        public static View FragmentNewMessageIcon;
        public static RecyclerView ChatsrecyclerviewInstance;
        public static String CurrrentPrivateChatUsername = "";
        public static Activity MyStaticActivity;
        public static int TotalNotification = 0;
        public static ImageLoader messagelist_imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, @Nullable String url, @Nullable Object payload) {
                Glide.with(imageView).load(url).into(imageView);
            }
        };
        public static MessagesListAdapter<MessageModelClass> MessageListAdapter = new MessagesListAdapter<>("currentappUser", messagelist_imageLoader);
        public static int TotalNotificationsInsideApp = 0;
        public static TextView homeScreenNotificationTv;
        public static String CurrrentPrivateChatRoomname = "";
        public static ArrayList<NewChatModelClass> NewMessageChat = new ArrayList<NewChatModelClass>();
        public static LinearLayout chatRoomsDisconnectionFooter;
        public static RelativeLayout DisconnectionTextview;
        public static int TotaldatabaseMessagesHashMap = 0;
        public static String MyTotalCoin;
        public static String OfflineMessagesRedDotView = "";
        public static String shoutsCategryOpened;
        public static EmojiconEditText StaticEmojiconEditText;
        public static String shoutCategryShowing=null;
        public static RewardedAd rewardedAd;
        public static HashMap<String, ArrayList<NotificationModel>>fcmNotificationsMap = new HashMap<>();

    }

    public static class custom_layout {
        static MediaPlayer player;


        public static LinearLayout Loader_image(Context context, RelativeLayout relativeLayout) {

            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setId(linearLayout.getId());
            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setBackgroundResource(R.drawable.circle_white);
            RelativeLayout.LayoutParams layoutParams_linearlayout = new RelativeLayout.LayoutParams(
                    (int) context.getResources().getDimension(R.dimen.MyLoaderViewWidth),
                    (int) context.getResources().getDimension(R.dimen.MyLoaderViewheight)
            );
            layoutParams_linearlayout.addRule(RelativeLayout.CENTER_IN_PARENT, relativeLayout.getId());

            ImageView imageView = new ImageView(context);
            imageView.setId(imageView.getId());
            Glide.with(context).load(R.drawable.socio_loader).into(imageView);
           imageView.setPadding(8,8,8,8);
            RelativeLayout.LayoutParams layoutParams_imageview = new RelativeLayout.LayoutParams(
                    (int) context.getResources().getDimension(R.dimen.socioLoaderWidth),
                    (int) context.getResources().getDimension(R.dimen.socioLoaderheight)
            );
            linearLayout.addView(imageView, layoutParams_imageview);
            relativeLayout.addView(linearLayout, layoutParams_linearlayout);
            return linearLayout;

        }

        //Recieve Rocket Attack
        public static ImageView Rocket_Attack_Image(final Context context, RelativeLayout relativeLayout) {
            final ImageView RocketImageView = new ImageView(context);
            RocketImageView.setId(RocketImageView.getId());
            RocketImageView.setImageResource(R.drawable.rocket);
            RocketImageView.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams_imageview = new RelativeLayout.LayoutParams(
                    200,
                    300
            );
            layoutParams_imageview.addRule(RelativeLayout.CENTER_IN_PARENT);
            RocketImageView.setLayoutParams(layoutParams_imageview);
            RocketImageView.setRotation(180);
            relativeLayout.addView(RocketImageView);

            //Missile Launcher Sound
            AudioManager audioManager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
                player = MediaPlayer.create(context, R.raw.mlaunch);
                player.start();
            }

            RocketImageView.setVisibility(View.VISIBLE);
            final Animation anim = AnimationUtils.loadAnimation(context, R.anim.toptobottom);
            anim.reset();

            RocketImageView.startAnimation(anim);

            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    //missile launcher sound stop
                    AudioManager audioManager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                    if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
                        player.stop();
                    }
                    RocketImageView.setVisibility(View.GONE);
                    //Blast sound active
                    if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
                        player = MediaPlayer.create(context, R.raw.explosion);
                        player.start();
                    }
                    RocketImageView.clearAnimation();

                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

            });

            return RocketImageView;

        }


        //Recieve Blast Attack
        public static ImageView Blast_Attack_Image(final Context context, RelativeLayout relativeLayout, final String AttackName) {
            final ImageView BlastImageView = new ImageView(context);
            BlastImageView.setId(BlastImageView.getId());
            Glide.with(context).load(R.drawable.bombexplosion).into(BlastImageView);
            BlastImageView.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams_imageviewBlast = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            layoutParams_imageviewBlast.addRule(RelativeLayout.CENTER_IN_PARENT);
            BlastImageView.setLayoutParams(layoutParams_imageviewBlast);
            relativeLayout.addView(BlastImageView);

            if (GiftsReceivingActivityPopup.dialog != null)
                GiftsReceivingActivityPopup.dialog.dismiss();
            final GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Vibrator vibrator;
                    vibrator = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
                    if (GiftsReceivingActivityPopup.dialog != null)
                        GiftsReceivingActivityPopup.dialog.dismiss();
                    // Do something after 2.5s = 2500ms
                    BlastImageView.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT >= 26) {
                        vibrator.vibrate(VibrationEffect.createOneShot(1500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        vibrator.vibrate(1500);
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            BlastImageView.setVisibility(View.GONE);
                            AudioManager audioManager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                            if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
                                player.stop();
                            }
                            if (AttackName.equals("Disconnect")) {
                                mSocket.emit("logout");
                                mSocket.disconnect();
                                static_objects.MyStaticActivity.stopService(new Intent(static_objects.MyStaticActivity, Foreground_service.class));
                                if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
                                    MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
                                }
                                HomeScreen.is_user_connected = 0;
                                MySocketsClass.static_objects.MyTotalCoin = "";
                                Intent intent = new Intent(static_objects.MyStaticActivity, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                static_objects.MyStaticActivity.startActivity(intent);
                                    SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit();
                                    DeletingPreferences.remove("mine_user_name");
                                    DeletingPreferences.remove("mine_password");
                                    DeletingPreferences.remove("userEmail");
                                    DeletingPreferences.remove("userSpecialId");
                                    DeletingPreferences.apply();

                                SharedPreferences.Editor DeletingTokenPreferences=PreferenceManager.getDefaultSharedPreferences(static_objects.MyStaticActivity).edit();
                                DeletingTokenPreferences.remove("MyTokenValue");
                                DeletingTokenPreferences.apply();
                                MainActivity.one_time_emit = 1;
                                SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(static_objects.MyStaticActivity).edit();
                                prefuseronline.putString("user_online", "");
                                prefuseronline.apply();
                                SharedPreferences.Editor chattingredDot = PreferenceManager.getDefaultSharedPreferences(static_objects.MyStaticActivity).edit();
                                chattingredDot.putString("chattingredDot", "no_newMsg");
                                chattingredDot.apply();

                                MySocketsClass.static_objects.NewUserMessageList.clear();
                                MySocketsClass.static_objects.hashMap.clear();
                                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_PRIVATE);
                                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_GIFTS);
                                //                       mSocket.off(MySocketsClass.ListenersClass.RECEIVE_NOTIFICATION);
                                mSocket.off(MySocketsClass.ListenersClass.GET_ATTACK_LAUNCH_RESPONSE);
                                mSocket.off(MySocketsClass.ListenersClass.RECEIVE_ATTACK);

                            } else {

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    public void run() {
                                        if (GiftsReceivingActivityPopup.dialog != null)
                                            GiftsReceivingActivityPopup.dialog.dismiss();
                                        giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity, null,
                                                "BuyShieldProtection", "", "", "");
                                    }
                                });
                            }
                        }
                    }, 1500);
                }
            }, 2500);


            return BlastImageView;

        }

        // Recieve Freeze Attack
        public static SnowFlakesLayout Freeze_Attack_Image(final Context context, RelativeLayout relativeLayout) {


            final SnowFlakesLayout snowFlakesLayout = new SnowFlakesLayout(context);
            //Testing Blast Image view
            final ImageView BlastImageView = new ImageView(context);
            BlastImageView.setId(BlastImageView.getId());
            Glide.with(context).load(R.drawable.bombexplosion).into(BlastImageView);
            BlastImageView.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams_imageviewBlast = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            layoutParams_imageviewBlast.addRule(RelativeLayout.CENTER_IN_PARENT);
            BlastImageView.setLayoutParams(layoutParams_imageviewBlast);
            relativeLayout.addView(BlastImageView);
            //end

            snowFlakesLayout.setId(snowFlakesLayout.getId());
            snowFlakesLayout.init();
            snowFlakesLayout.setWholeAnimateTiming(3000000);
            snowFlakesLayout.setAnimateDuration(7000);
            snowFlakesLayout.setGenerateSnowTiming(300);
            snowFlakesLayout.setRandomSnowSizeRange(40, 1);
            snowFlakesLayout.setImageResourceID(R.drawable.snowflake);
            snowFlakesLayout.setEnableRandomCurving(true);
            snowFlakesLayout.setEnableAlphaFade(true);
            snowFlakesLayout.startSnowing();

            RelativeLayout.LayoutParams layoutParams_Snowfall = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );
            layoutParams_Snowfall.addRule(RelativeLayout.CENTER_IN_PARENT);
            snowFlakesLayout.setLayoutParams(layoutParams_Snowfall);
            relativeLayout.addView(snowFlakesLayout);
            snowFlakesLayout.setVisibility(View.GONE);

            //BlastImageView
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (GiftsReceivingActivityPopup.dialog != null)
                        GiftsReceivingActivityPopup.dialog.dismiss();
                    final GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
                    Vibrator vibrator;
                    vibrator = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
                    // Do something after 2.5s = 2500ms
                    BlastImageView.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT >= 26) {
                        vibrator.vibrate(VibrationEffect.createOneShot(1500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        vibrator.vibrate(1500);
                    }

                    //BlastImageView INVISIBLE and SnowFlakesLayout VISIBLE
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            BlastImageView.setVisibility(View.GONE);
                            AudioManager audioManager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                            if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
                                player.stop();
                            }
                            snowFlakesLayout.setVisibility(View.VISIBLE);

                            if (GiftsReceivingActivityPopup.dialog != null)
                                GiftsReceivingActivityPopup.dialog.dismiss();
                            //SheildProtection Dialogbox Shown
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    snowFlakesLayout.setVisibility(View.GONE);
                                    if (GiftsReceivingActivityPopup.dialog != null)
                                        GiftsReceivingActivityPopup.dialog.dismiss();
                                    giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity, null,
                                            "BuyShieldProtection", "", "", "");

                                }
                            }, 30000);

                        }
                    }, 1500);
                }
            }, 2500);

            return snowFlakesLayout;

        }

        //Like Receive
        public static SnowFlakesLayout LikeReceiving(final Context context, RelativeLayout relativeLayout) {
            final SnowFlakesLayout snowFlakesLayout = new SnowFlakesLayout(context);
            snowFlakesLayout.setId(snowFlakesLayout.getId());
            snowFlakesLayout.init();
            snowFlakesLayout.setWholeAnimateTiming(10000);
            snowFlakesLayout.setAnimateDuration(7000);
            snowFlakesLayout.setGenerateSnowTiming(300);
            snowFlakesLayout.setRandomSnowSizeRange(40, 1);
            snowFlakesLayout.setImageResourceID(R.drawable.heart_solid);
            snowFlakesLayout.setEnableRandomCurving(true);
            snowFlakesLayout.setEnableAlphaFade(true);
            snowFlakesLayout.startSnowing();

            RelativeLayout.LayoutParams layoutParams_Snowfall = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );
            layoutParams_Snowfall.addRule(RelativeLayout.CENTER_IN_PARENT);
            snowFlakesLayout.setLayoutParams(layoutParams_Snowfall);
            relativeLayout.addView(snowFlakesLayout);
            return snowFlakesLayout;
        }

        //Send Attack
        public static ImageView Send_Attack_Image(final Context context, RelativeLayout relativeLayout) {
            final ImageView SendRocketImageView = new ImageView(context);
            SendRocketImageView.setId(SendRocketImageView.getId());
            SendRocketImageView.setImageResource(R.drawable.rocket);
            SendRocketImageView.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams_imageviewBlast = new RelativeLayout.LayoutParams(
                    210,
                    310
            );
            layoutParams_imageviewBlast.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams_imageviewBlast.addRule(RelativeLayout.CENTER_HORIZONTAL);
            SendRocketImageView.setLayoutParams(layoutParams_imageviewBlast);
            relativeLayout.addView(SendRocketImageView);
            AudioManager audioManager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
                player = MediaPlayer.create(context, R.raw.mlaunch);
                if (player != null)
                    player.start();
            }
            final Animation anim = AnimationUtils.loadAnimation(context, R.anim.bottomtotop);
            anim.reset();

            SendRocketImageView.clearAnimation();
            SendRocketImageView.startAnimation(anim);

            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    AudioManager audioManager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                    if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
                        player.stop();
                    }
                    SendRocketImageView.clearAnimation();
                    SendRocketImageView.setVisibility(View.GONE);
                    SendAttackActivity sendAttackActivity = new SendAttackActivity();
                    sendAttackActivity.FinishShopActivity(MySocketsClass.static_objects.MyStaticActivity);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


            return SendRocketImageView;

        }

        //Freezed Bottom ImageView
        public static ImageView Freeze_Attack_Bottom_Image(final Context context, RelativeLayout relativeLayout) {
            final ImageView FreezedBottomImageView = new ImageView(context);
            FreezedBottomImageView.setId(FreezedBottomImageView.getId());
            FreezedBottomImageView.setImageResource(R.drawable.freezed_snow_bottom);
            FreezedBottomImageView.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams_freezedbottomIV = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    200
            );
            layoutParams_freezedbottomIV.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams_freezedbottomIV.addRule(RelativeLayout.CENTER_HORIZONTAL);
            FreezedBottomImageView.setLayoutParams(layoutParams_freezedbottomIV);
            relativeLayout.addView(FreezedBottomImageView);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.fadein);
                    anim.reset();

                    FreezedBottomImageView.setVisibility(View.VISIBLE);
                    FreezedBottomImageView.clearAnimation();
                    FreezedBottomImageView.startAnimation(anim);

                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            FreezedBottomImageView.clearAnimation();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    FreezedBottomImageView.setVisibility(View.GONE);
                                }
                            }, 27000);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }
            }, 3000);

            return FreezedBottomImageView;

        }

        //Freezed Left Corner ImageView
        public static ImageView Freeze_Attack_Corner_Image(final Context context, RelativeLayout relativeLayout) {
            final ImageView FreezedCornerImageView = new ImageView(context);
            FreezedCornerImageView.setId(FreezedCornerImageView.getId());
            FreezedCornerImageView.setImageResource(R.drawable.freezed_snow_corner);
            FreezedCornerImageView.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams_freezedcornerIV = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            layoutParams_freezedcornerIV.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            layoutParams_freezedcornerIV.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            FreezedCornerImageView.setLayoutParams(layoutParams_freezedcornerIV);
            relativeLayout.addView(FreezedCornerImageView);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    final Animation anim = AnimationUtils.loadAnimation(context, R.anim.fadein);
                    anim.reset();

                    FreezedCornerImageView.setVisibility(View.VISIBLE);
                    FreezedCornerImageView.clearAnimation();
                    FreezedCornerImageView.startAnimation(anim);

                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            FreezedCornerImageView.clearAnimation();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    FreezedCornerImageView.setVisibility(View.GONE);
                                }
                            }, 27000);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }
            }, 3000);

            return FreezedCornerImageView;

        }

        public static RelativeLayout DisconnectionSnackbar(Context context, final RelativeLayout relativeLayout) {

            //Main Full screen transparnt layout
            RelativeLayout MyRelativelayout=new RelativeLayout(context);
            MyRelativelayout.setBackgroundResource(R.color.colorBlackTransparent);
            RelativeLayout.LayoutParams layoutParams_MyrelativeLayout = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );
            MyRelativelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            MyRelativelayout.setLayoutParams(layoutParams_MyrelativeLayout);

            //linear layout contain image and text
            LinearLayout Linearlayout=new LinearLayout(context);
            Linearlayout.setGravity(Gravity.CENTER);
            Linearlayout.setOrientation(LinearLayout.HORIZONTAL);
            RelativeLayout.LayoutParams layoutParams_Linearlayout = new RelativeLayout.LayoutParams(
                    600,
                    170
            );
            Linearlayout.setBackground(context.getResources().getDrawable(R.drawable.disconnection_snackbar));
            layoutParams_Linearlayout.addRule(RelativeLayout.CENTER_IN_PARENT, relativeLayout.getId());
            Linearlayout.setLayoutParams(layoutParams_Linearlayout);

            //image
            ImageView loadingImageView=new ImageView(context);
            Glide.with(context).load(R.drawable.socio_loader).into(loadingImageView);
            RelativeLayout.LayoutParams layoutParams_loadingImageView = new RelativeLayout.LayoutParams(
                    70,
                    70
            );
            layoutParams_loadingImageView.addRule(RelativeLayout.CENTER_IN_PARENT, relativeLayout.getId());
            loadingImageView.setLayoutParams(layoutParams_loadingImageView);

            //text
            TextView textView = new TextView(context);
            textView.setText("Reconnecting...");
            textView.setTextSize(20);
            textView.setGravity(Gravity.CENTER);
            RelativeLayout.LayoutParams layoutParams_textview = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT

            );
            layoutParams_textview.addRule(RelativeLayout.CENTER_IN_PARENT, relativeLayout.getId());
            textView.setTextColor(context.getResources().getColor(R.color.colorBlack));
            textView.setPadding(10, 15, 10, 15);
          //  textView.setBackground(context.getResources().getDrawable(R.drawable.disconnection_snackbar));
            textView.setLayoutParams(layoutParams_textview);

            Linearlayout.addView(loadingImageView);
            Linearlayout.addView(textView);
            MyRelativelayout.addView(Linearlayout);
            relativeLayout.addView(MyRelativelayout);
            return MyRelativelayout;


        }

    }

    public List<UserPrivateChatTables> getUserChat(String username, Context context) {

        WarfareDatabase warfareDatabase;
        warfareDatabase = Room.databaseBuilder(context, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        List<UserPrivateChatTables> _userPrivateChatTables = warfareDatabase.myDataBaseAccessObject().getUserChat(username);

        return _userPrivateChatTables;
    }

    public List<DataBaseChatRoomTables> getChatRoomChats(String CountryName, Context context) {

        DataBase_ChatRooms dataBase_chatRooms;
        dataBase_chatRooms = Room.databaseBuilder(context, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();
        List<DataBaseChatRoomTables> userChatRoomTables = dataBase_chatRooms.chatRoomDBAO().getChatRoomChat(CountryName);

        return userChatRoomTables;
    }

    public long MakingCurrentDateTimeStamp() {
        Calendar c = Calendar.getInstance();
        long timestamp = c.getTimeInMillis();
        return timestamp;
    }

    public void showErrorDialog(Context context, String text1, final String text2, final String type,
                                final String friendRequestedUser){
        View view;
        TextView errorDialogText1, errorDialogText2;
        RelativeLayout errorDialogHeaderRl, errorDialogProgressRl,MainLayoutPopups;
        ContactsFragment contactsFragment = new ContactsFragment();
        EditText errorDialogVerificationField;
        ImageView errorDialogVerificationImg, errorDialogHeaderImg,rocketLoader;
        Button errorDialogTryAgainBtn, errorDialogCloseBtn, errorDialogIgnoreBtn;
        MySingleTon mySingleTon;
//        String usernameRecycler, addContactUsername,error_status_msg;
//        String AutoThemeTime ="06:00:00 pm";
        String FriendRequestedUser;

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.error_alertbox, null);
        final AlertDialog.Builder adb = new AlertDialog.Builder(context);
        final AlertDialog myAlertDialog = adb.create();
        myAlertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myAlertDialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
        myAlertDialog.setCancelable(true);
        myAlertDialog.setView(view);
        FriendRequestedUser = friendRequestedUser;

//        if (myAlertDialog!=null && myAlertDialog.isShowing()&& !static_objects.MyStaticActivity.isFinishing()) {
//            myAlertDialog.cancel();
//        }

//        myAlertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        myAlertDialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//        myAlertDialog.setCancelable(true);
//        myAlertDialog.setContentView(R.layout.error_alertbox);


            final Animation animShake = AnimationUtils.loadAnimation(context, R.anim.shake);
            Log.e("connectingg", "customdialogerror 83");

            mySingleTon = MySingleTon.getInstance();
            errorDialogHeaderRl = view.findViewById(R.id.errorDialogHeaderRl);
            errorDialogProgressRl = view.findViewById(R.id.errorDialogProgressRl);
            rocketLoader = view.findViewById(R.id.rocketLoader);
            //  java.lang.IllegalArgumentException: You cannot start a load for a destroyed activity
            if (!static_objects.MyStaticActivity.isFinishing()) {
                Glide.with(context).load(R.drawable.socio_loader).into(rocketLoader);
            }
            errorDialogIgnoreBtn = view.findViewById(R.id.errorDialogIgnoreBtn);
            errorDialogHeaderImg = view.findViewById(R.id.errorDialogHeaderImg);
            errorDialogText1 = view.findViewById(R.id.errorDialogText1);
            errorDialogText2 = view.findViewById(R.id.errorDialogText2);
            errorDialogVerificationImg = view.findViewById(R.id.errorDialogVerificationImg);
            errorDialogVerificationField = view.findViewById(R.id.errorDialogVerificationField);
            errorDialogTryAgainBtn = view.findViewById(R.id.errorDialogTryAgainBtn);
            errorDialogCloseBtn = view.findViewById(R.id.errorDialogCloseBtn);
            MainLayoutPopups = view.findViewById(R.id.MainLayoutPopups);

            errorDialogText1.setText(text1);
            errorDialogText2.setText(text2);


            if (type.equals("verificationRequired")) {
                errorDialogVerificationField.setVisibility(View.VISIBLE);
                errorDialogVerificationImg.setVisibility(View.VISIBLE);
                errorDialogHeaderImg.setImageResource(R.drawable.smile_icon);
                errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_green_btn));
                errorDialogTryAgainBtn.setText("Verify");
                errorDialogCloseBtn.setText("Resend");
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_orange_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#FFFFFF"));

                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (errorDialogVerificationField.getText().toString().trim().equals("")) {
                            Toast.makeText(context, "Please enter verification code", Toast.LENGTH_SHORT).show();
//                        errorDialogVerificationImg.startAnimation(animShake);
//                        errorDialogVerificationImg.setImageResource(R.drawable.input_border_red);
                        } else {
                            if (mSocket.connected()) {
                                errorDialogProgressRl.setVisibility(View.VISIBLE);
                                Log.w("verify", "onClick: 0");
                                final JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("vCode", errorDialogVerificationField.getText().toString().trim());
                                    jsonObject.put("username", mySingleTon.usernameTextFromField);

                                    Log.d("un", "onClick: " + jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mSocket.emit("verifyCode", jsonObject);

                                mSocket.on("verifyResponse", new Emitter.Listener() {
                                    @Override
                                    public void call(final Object... args) {
                                        ((Activity)context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                final JSONObject data = (JSONObject) args[0];
                                                String title, message;
                                                try {
                                                    errorDialogProgressRl.setVisibility(View.GONE);
                                                    errorDialogText1.setText("Ohhhh!");
                                                    errorDialogText2.setText(data.getString("message"));
                                                    errorDialogVerificationField.setVisibility(View.GONE);
                                                    errorDialogVerificationImg.setVisibility(View.GONE);
                                                    if (data.getString("success").equals("0")) {
                                                        errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                                                        MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                                                        errorDialogTryAgainBtn.setText("Try Again");
                                                        errorDialogCloseBtn.setText("Close");
                                                        errorDialogTryAgainBtn.setBackgroundResource(R.drawable.error_dialog_red_btn);
                                                        errorDialogCloseBtn.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                                                        errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));

                                                    } else {
                                                        errorDialogText1.setVisibility(View.GONE);
                                                        errorDialogHeaderImg.setImageResource(R.drawable.smile_icon);
                                                        errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                                                        errorDialogTryAgainBtn.setVisibility(View.GONE);
                                                        errorDialogCloseBtn.setText("Close");
                                                        MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                                                        errorDialogCloseBtn.setBackgroundResource(R.drawable.error_dialog_green_btn);
                                                        errorDialogCloseBtn.setTextColor(Color.parseColor("#FFFFFF"));
                                                    }


                                                    errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                            showErrorDialog(context, "Verification Required",
                                                                    "You have not verified your email address yet", "verificationRequired", "");
                                                        }
                                                    });
                                                    errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                        }
                                                    });


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                Log.d("verifyResponse", data.toString());
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                });

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (type.equals("verificationRequired")) {

                            errorDialogProgressRl.setVisibility(View.VISIBLE);

                            if (mSocket.connected()) {
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("username", mySingleTon.usernameTextFromField);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mSocket.emit("resend verify code", jsonObject);

                                mSocket.on("resend verify code response", new Emitter.Listener() {
                                    @Override
                                    public void call(final Object... args) {
                                        ((Activity)context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                JSONObject data = (JSONObject) args[0];
                                                String title, message;

                                                try {
                                                    errorDialogProgressRl.setVisibility(View.GONE);

                                                    if (data.getString("success").equals("1")) {

                                                        errorDialogText1.setText("Verification code resent");
                                                        errorDialogText2.setText(data.getString("message"));
                                                    } else if (data.getString("success").equals("0")) {

                                                        errorDialogText1.setVisibility(View.GONE);
                                                        errorDialogText2.setText(data.getString("message"));
                                                        errorDialogVerificationField.setVisibility(View.GONE);
                                                        errorDialogVerificationImg.setVisibility(View.GONE);
                                                        errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                                                        MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                                                        errorDialogTryAgainBtn.setText("Ok");
                                                        errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                                                        errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                                                        errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));
                                                        errorDialogCloseBtn.setText("Close");

                                                        errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                myAlertDialog.cancel();
                                                            }
                                                        });
                                                        errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                myAlertDialog.cancel();
                                                            }
                                                        });
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                Log.d("verifyCodeResponse", data.toString());
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                });


            } else if (type.equals("invalidCredentials")) {


                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                //  errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#E74C3C"));
                errorDialogTryAgainBtn.setText("Ok");
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                errorDialogCloseBtn.setBackgroundColor(Color.parseColor("#000000"));
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });
                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });

            } else if (type.equals("signupError")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                // errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#E74C3C"));
                errorDialogTryAgainBtn.setText("Ok");
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });
                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });

            } else if (type.equals("logout")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                // errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#E74C3C"));
                errorDialogTryAgainBtn.setText("Logout");
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));
                errorDialogCloseBtn.setText("Cancel");

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });
                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mSocket.emit("logout");
                        myAlertDialog.cancel();
                        static_objects.MyStaticActivity.finish();
                        mSocket.disconnect();
                        context.stopService(new Intent(context, Foreground_service.class));
                        is_user_connected = 0;


                    }
                });

            } else if (type.equals("SuccessMsg")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.smile_icon);
                errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                // errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#2ECC71"));
                errorDialogTryAgainBtn.setText("");
                errorDialogTryAgainBtn.setVisibility(View.GONE);
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_green_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#FFFFFF"));
                errorDialogCloseBtn.setText("Ok");

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });


            } else if (type.equals("passwordReset")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.smile_icon);
                errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                //  errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#2ECC71"));
                errorDialogCloseBtn.setText("Ok");
                errorDialogTryAgainBtn.setVisibility(View.GONE);
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_green_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#FFFFFF"));

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });
                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });

            } else if (type.equals("signupSuccess")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogVerificationField.setVisibility(View.VISIBLE);
                errorDialogVerificationField.setInputType(InputType.TYPE_CLASS_TEXT);
                errorDialogVerificationImg.setVisibility(View.VISIBLE);
                errorDialogHeaderImg.setImageResource(R.drawable.smile_icon);
                errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                //  errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#2ECC71"));
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_green_btn));
                errorDialogTryAgainBtn.setText("Verify");
                errorDialogCloseBtn.setText("Resend");
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_orange_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#FFFFFF"));

                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (errorDialogVerificationField.getText().toString().trim().equals("")) {
                            Toast.makeText(context, "Please enter verification code", Toast.LENGTH_SHORT).show();
//                        errorDialogVerificationImg.startAnimation(animShake);
//                        errorDialogVerificationImg.setImageResource(R.drawable.input_border_red);
                        } else {
                            if (mSocket.connected()) {

                                errorDialogProgressRl.setVisibility(View.VISIBLE);

                                final JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("vCode", errorDialogVerificationField.getText().toString().trim());
                                    jsonObject.put("username", mySingleTon.usernameTextFromField);

                                    Log.d("un", "onClick: " + jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mSocket.emit("verifyCode", jsonObject);

                                mSocket.on("verifyResponse", new Emitter.Listener() {
                                    @Override
                                    public void call(final Object... args) {
                                        ((Activity)context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                JSONObject data = (JSONObject) args[0];

                                                try {
                                                    String success = data.getString("success");
                                                    errorDialogProgressRl.setVisibility(View.GONE);

                                                    errorDialogText1.setText("Ohhhh!");
                                                    errorDialogText2.setText(data.getString("message"));
                                                    errorDialogVerificationField.setVisibility(View.GONE);
                                                    errorDialogVerificationImg.setVisibility(View.GONE);
                                                    if (success.equals("1")) {
                                                        errorDialogText1.setVisibility(View.GONE);
                                                        errorDialogHeaderImg.setImageResource(R.drawable.smile_icon);
                                                        errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                                                        MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                                                        // errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#2ECC71"));
                                                        errorDialogTryAgainBtn.setVisibility(View.GONE);
                                                    } else {
                                                        errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                                                        MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                                                        // errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#E74C3C"));
                                                        errorDialogTryAgainBtn.setText("Try Again");
                                                        errorDialogText1.setVisibility(View.VISIBLE);
                                                    }
                                                    errorDialogCloseBtn.setText("Close");
                                                    errorDialogTryAgainBtn.setBackgroundResource(R.drawable.error_dialog_red_btn);
                                                    errorDialogCloseBtn.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                                                    errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));

                                                    errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                            showErrorDialog(context, "Verification Required",
                                                                    "You have not verified your email address yet", "verificationRequired", "");
                                                        }
                                                    });
                                                    errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                            Intent intent = new Intent(context, MainActivity.class);
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                                                            context.startActivity(intent);
                                                        }
                                                    });


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                Log.d("verifyResponse", data.toString());
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                });

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (mSocket.connected()) {
                            JSONObject jsonObject = new JSONObject();
                            errorDialogProgressRl.setVisibility(View.VISIBLE);

                            try {
                                jsonObject.put("username", mySingleTon.usernameTextFromField);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.emit("resend verify code", jsonObject);

                            mSocket.on("resend verify code response", new Emitter.Listener() {
                                @Override
                                public void call(final Object... args) {
                                    ((Activity)context).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            JSONObject data = (JSONObject) args[0];
                                            String title, message;

                                            try {
                                                errorDialogProgressRl.setVisibility(View.GONE);

                                                if (data.getString("success").equals("1")) {

                                                    errorDialogText1.setText("Verification code resent");
                                                    errorDialogText2.setText(data.getString("message"));
                                                } else if (data.getString("success").equals("0")) {

                                                    errorDialogText1.setVisibility(View.GONE);
                                                    errorDialogText2.setText(data.getString("message"));
                                                    errorDialogVerificationField.setVisibility(View.GONE);
                                                    errorDialogVerificationImg.setVisibility(View.GONE);
                                                    errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                                                    MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                                                    //  errorDialogHeaderRl.setBackgroundColor(Color.parseColor("#E74C3C"));
                                                    errorDialogTryAgainBtn.setText("Ok");
                                                    errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                                                    errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                                                    errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));
                                                    errorDialogCloseBtn.setText("Close");

                                                    errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                        }
                                                    });
                                                    errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                        }
                                                    });
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            Log.d("verifyCodeResponse", data.toString());
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else if (type.equals("addFriend")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogText2.setVisibility(View.GONE);

                errorDialogVerificationField.setVisibility(View.VISIBLE);
                errorDialogVerificationField.setHint("write username");
                errorDialogVerificationField.setInputType(InputType.TYPE_CLASS_TEXT);
                errorDialogVerificationImg.setVisibility(View.VISIBLE);
                errorDialogHeaderImg.setImageResource(R.drawable.add_user_green);
                errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                // errorDialogHeaderRl.setBackgroundColor(ContextCompat.getColor(dialog.getContext(), R.color.colorSuccess));
                errorDialogTryAgainBtn.setText("Add");
                errorDialogTryAgainBtn.setBackgroundResource(R.drawable.error_dialog_green_btn);
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));
                errorDialogCloseBtn.setText("Cancel");


                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (TextUtils.isEmpty(errorDialogVerificationField.getText().toString().trim())) {
                            errorDialogText2.setVisibility(View.VISIBLE);
                            errorDialogText2.setTextColor(ContextCompat.getColor(context, R.color.colorErrorRed));
                            errorDialogText2.setText("Please provide valid username");
//                        errorDialogVerificationImg.startAnimation(animShake);
//                        errorDialogVerificationImg.setImageResource(R.drawable.input_border_red);
                        } else {
                            final JSONObject jsonObject = new JSONObject();
                            try {

                                jsonObject.put("add_username", errorDialogVerificationField.getText().toString().trim());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.emit("add contact", jsonObject);

                            mSocket.on("add contact response", new Emitter.Listener() {
                                @Override
                                public void call(final Object... args) {
                                    ((Activity)context).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            JSONObject jsonObject1 = (JSONObject) args[0];
                                            try {

                                                Log.w("friend", "run: " + jsonObject1.getString("success"));
                                                if (jsonObject1.getString("success").equals("1")) {
                                                    errorDialogTryAgainBtn.setText("Ok");
                                                    errorDialogVerificationField.setVisibility(View.GONE);
                                                    errorDialogVerificationImg.setVisibility(View.GONE);
                                                    errorDialogText1.setVisibility(View.GONE);
                                                    errorDialogText2.setVisibility(View.VISIBLE);
                                                    errorDialogText2.setTextColor(ContextCompat.getColor(context, R.color.black));
                                                    errorDialogText2.setText(jsonObject1.getString("message"));
                                                    errorDialogCloseBtn.setVisibility(View.GONE);


                                                    errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            ContactsFragment contactsFragment = new ContactsFragment();
                                                            contactsFragment.getContacts();
                                                            myAlertDialog.cancel();
                                                        }
                                                    });
                                                } else {
                                                    errorDialogTryAgainBtn.setText("Ok");
                                                    MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                                                    //  errorDialogHeaderRl.setBackgroundColor(ContextCompat.getColor(dialog.getContext(), R.color.colorErrorRed));
                                                    errorDialogTryAgainBtn.setBackgroundColor(ContextCompat.getColor(context, R.color.colorErrorRed));
                                                    errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                                                    errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.colorErrorRed), PorterDuff.Mode.SRC_IN);
                                                    errorDialogVerificationField.setVisibility(View.GONE);
                                                    errorDialogVerificationImg.setVisibility(View.GONE);
                                                    errorDialogText1.setVisibility(View.GONE);
                                                    errorDialogText2.setVisibility(View.VISIBLE);
                                                    errorDialogText2.setTextColor(ContextCompat.getColor(context, R.color.black));
                                                    errorDialogText2.setText(jsonObject1.getString("message"));
                                                    errorDialogCloseBtn.setVisibility(View.VISIBLE);

                                                    errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                        }
                                                    });
                                                    errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            myAlertDialog.cancel();
                                                        }
                                                    });
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });

                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                    }
                });
            } else if (type.equals("friendOptions")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogText2.setVisibility(View.VISIBLE);

                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.delete_friend_icon);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                //   errorDialogHeaderRl.setBackgroundColor(ContextCompat.getColor(dialog.getContext(), R.color.colorErrorRed));
                errorDialogTryAgainBtn.setText("Yes");
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));
                errorDialogCloseBtn.setText("No");

                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("username", MySocketsClass.static_objects.global_username_delete);

                            mSocket.emit("delete contact", jsonObject);

                            mSocket.on("delete contact response", new Emitter.Listener() {
                                @Override
                                public void call(Object... args) {
                                    JSONObject jsonObject1 = (JSONObject) args[0];

                                    try {
                                        if (jsonObject1.getString("success").equals("1")) {
                                            contactsFragment.getContacts();
                                            myAlertDialog.cancel();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                        contactsFragment.getContacts();

                    }
                });
            } else if (type.equals("newAddContact")) {
                errorDialogText1.setVisibility(View.GONE);
                errorDialogText2.setVisibility(View.VISIBLE);
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.add_user_green);
                errorDialogHeaderImg.setColorFilter(ContextCompat.getColor(context, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);
                // errorDialogHeaderRl.setBackgroundColor(ContextCompat.getColor(dialog.getContext(), R.color.colorSuccess));
                errorDialogTryAgainBtn.setText("Accept");
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_green_btn));
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                errorDialogCloseBtn.setTextColor(Color.parseColor("#FFFFFF"));
                errorDialogCloseBtn.setText("Reject");
                errorDialogIgnoreBtn.setVisibility(View.VISIBLE);
                errorDialogIgnoreBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                errorDialogIgnoreBtn.setTextColor(Color.parseColor("#000000"));

//            errorDialogIgnoreBtn.setWidth(10);
//            errorDialogCloseBtn.setWidth(10);
//            errorDialogTryAgainBtn.setWidth(10);
                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("username", FriendRequestedUser);
                            mSocket.emit(MySocketsClass.EmmittersClass.APPROVE_ADD_REQUEST, jsonObject);

                            myAlertDialog.cancel();
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                });
                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("username", FriendRequestedUser);
                            mSocket.emit("reject add request", jsonObject);

                            myAlertDialog.cancel();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                errorDialogIgnoreBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ContactsFragment contactsFragment = new ContactsFragment();
                        contactsFragment.getContacts();
                        myAlertDialog.cancel();
                    }
                });
            } else if (type.equals("reconnect_error_dialog")) {
                errorDialogText1.setText("Unable to connect!");
                errorDialogText2.setText("Something went wrong, please relogin.");
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);
                errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);
                // errorDialogHeaderRl.setBackgroundColor(ContextCompat.getColor(dialog.getContext(), R.color.colorErrorRed));
                errorDialogTryAgainBtn.setText("Login");
                errorDialogTryAgainBtn.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_green_btn));
                errorDialogCloseBtn.setVisibility(View.GONE);
                errorDialogIgnoreBtn.setVisibility(View.GONE);

                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myAlertDialog.cancel();
                        //finish();
                        context.startActivity(new Intent(context, Login.class));
                    }
                });
            } else if (type.equals("inviteAFriend")) {
                errorDialogText1.setVisibility(View.VISIBLE);
                errorDialogText1.setText("Share url to earn points!");

                errorDialogText2.setVisibility(View.VISIBLE);
                errorDialogText2.setText("https://socio.chat/?username=" + mySingleTon.username);
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);

                errorDialogHeaderImg.setImageResource(R.drawable.coin_image);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_new_theme_green);

                errorDialogTryAgainBtn.setText("Copy");
                errorDialogTryAgainBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_green_btn));
                errorDialogCloseBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));

                errorDialogCloseBtn.setTextColor(Color.parseColor("#000000"));
                errorDialogCloseBtn.setText("Share");

                errorDialogIgnoreBtn.setVisibility(View.GONE);
                errorDialogIgnoreBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_gray_btn));
                errorDialogIgnoreBtn.setTextColor(Color.parseColor("#000000"));

                errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String textString;
                        ClipboardManager myClipboard;
                        ClipData myClip;
                        myClipboard = (ClipboardManager) view.getContext().getSystemService(CLIPBOARD_SERVICE);
                        textString = errorDialogText2.getText().toString();

                        myClip = ClipData.newPlainText("text", textString);
                        myClipboard.setPrimaryClip(myClip);

                        Toast.makeText(view.getContext(), "Text Copied",
                                Toast.LENGTH_SHORT).show();

                        myAlertDialog.cancel();
                    }
                });
                errorDialogCloseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LocalBroadcastManager localBroadcastManager=LocalBroadcastManager.getInstance(context);
                        Intent intent=new Intent("ActivatingSharingOption");
                        localBroadcastManager.sendBroadcast(intent);
                        myAlertDialog.cancel();
                    }
                });
            }

            else if (type.equals("AdLoadFailed")) {
                errorDialogText1.setVisibility(View.VISIBLE);
                errorDialogText1.setText("Ad failed!");

                errorDialogText2.setVisibility(View.VISIBLE);
                errorDialogText2.setText("Unable to show ad right now.");
                errorDialogVerificationField.setVisibility(View.GONE);
                errorDialogVerificationImg.setVisibility(View.GONE);

                errorDialogHeaderImg.setImageResource(R.drawable.sad_icon);
                MainLayoutPopups.setBackgroundResource(R.drawable.square_full_body_red_popups);

                errorDialogTryAgainBtn.setVisibility(View.GONE);
                errorDialogCloseBtn.setVisibility(View.GONE);

                errorDialogIgnoreBtn.setVisibility(View.VISIBLE);
                errorDialogIgnoreBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.error_dialog_red_btn));
                errorDialogIgnoreBtn.setTextColor(Color.parseColor("#000000"));

                errorDialogIgnoreBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myAlertDialog.cancel();
                    }
                });
            }
            if (!((Activity) context).isFinishing()) {
                myAlertDialog.show();
            } else {
            }
    }

    public boolean DownloadingImage(ImageView ViewingLargeImageview,Context context,String folderType) {

        String MyImageDownloadedPath=null;
        if (folderType.equals("profile")){
            MyImageDownloadedPath = "/Socio/Profile Images";
        }
        else if (folderType.equals("shouts")){
            MyImageDownloadedPath = "/Socio/Shouts Images";
        }
        BitmapDrawable drawable = (BitmapDrawable) ViewingLargeImageview.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        File sdCardDirectory = new File(Environment.getExternalStorageDirectory(), MyImageDownloadedPath);

        if (!sdCardDirectory.exists()) {
            sdCardDirectory.mkdirs();
        }

        File MyImage = new File(sdCardDirectory, "/Image" + System.currentTimeMillis() + ".jpg");
        boolean success = false;
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(MyImage);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            outputStream.flush();
            outputStream.close();


            ContentValues contentValues = new ContentValues();

            contentValues.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            contentValues.put(MediaStore.MediaColumns.DATA, MyImage.getAbsolutePath());

            context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            success = true;
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return success;
    }

    public void deletingPreferencesForNotifications(Context context) {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit();
        DeletingPreferences.remove("MuteNotificationStats");
        DeletingPreferences.apply();
    }

    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }

    public void openingZoomingActivity(Context context, String imageUrl,String folderType){
        Intent intent=new Intent(context, FullScreenImageView.class);
        intent.putExtra("photoUrl",imageUrl);
        intent.putExtra("folderType",folderType);
        context.startActivity(intent);
    }


}
