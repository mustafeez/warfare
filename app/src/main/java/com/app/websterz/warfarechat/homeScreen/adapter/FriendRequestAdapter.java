package com.app.websterz.warfarechat.homeScreen.adapter;

import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestAdapter.ViewHolder> {
    JSONArray jsonArray;
    int total_length;

    public FriendRequestAdapter(JSONArray jsonArray) {
        this.jsonArray=jsonArray;
        Log.e("adapting","FriendRequestAdapter");
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_friend_request_adapter, viewGroup, false);
        Log.e("adapting","onCreateViewHolder");
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        Log.e("adapting","onBindViewHolder");
        viewHolder.avatarLoader.setVisibility(View.VISIBLE);
        viewHolder.avatarLoader.setAnimating(true);
        if (jsonArray.length()==0)
        {
            viewHolder._noRequestsFoundLl.setVisibility(View.VISIBLE);
            Log.e("adapting","onBindViewHolder if");
            viewHolder.recycler_row_layout.setVisibility(View.GONE);
        }
        else {
            viewHolder.recycler_row_layout.setVisibility(View.VISIBLE);
            viewHolder._noRequestsFoundLl.setVisibility(View.GONE);
            Log.e("adapting","onBindViewHolder else");

            try {

                final String friend_requent_name=jsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("user");
                String statusMsg=jsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("statusMsg");
                String display_picture=jsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("display_picture");
                String presence=jsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("presence");
                viewHolder.friendRequestUsernameTv.setText(friend_requent_name);
                viewHolder.friendRequestStatusTv.setText(statusMsg);
           //     Picasso.get().load(baseUrl+display_picture).into(viewHolder.friendRequestUserImage);
                GlideApp.with(viewHolder.itemView.getContext()).load(baseUrl + display_picture).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewHolder.avatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).
                        into(viewHolder.friendRequestUserImage);

                                    if (presence.equals("0")) {
                                        viewHolder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorOffline));
                    } else if (presence.equals("1")) {
                                        viewHolder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorOnline));
                    } else if (presence.equals("2")) {
                                        viewHolder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorBusy));
                    } else if (presence.equals("3")) {
                                        viewHolder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorAway));
                    }

                    viewHolder.friendRequestAcceptBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("username", friend_requent_name);
                                mSocket.emit("approve add request", jsonObject);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    jsonArray.remove(i);
                                }
                                notifyItemRemoved(i);
                                notifyItemRangeChanged(i, jsonArray.length());
                                ContactsFragment contactsFragment = new ContactsFragment();
                                contactsFragment.getContacts();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                viewHolder.friendRequestRejectBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("username", friend_requent_name);
                            mSocket.emit("reject add request", jsonObject);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                jsonArray.remove(i);
                            }
                            notifyItemRemoved(i);
                            notifyItemRangeChanged(i, jsonArray.length());
                            ContactsFragment contactsFragment = new ContactsFragment();
                            contactsFragment.getContacts();



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


            } catch (JSONException e) {
                Log.e("adapting","exception");
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        Log.e("adapting","getitemcount");
        if (jsonArray.length()==0)
        {
            total_length= 1;
        }
        else {
            total_length=jsonArray.length();
        }
        return total_length;
    }




    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView friendRequestUsernameTv, friendRequestStatusTv;
        CircleImageView friendRequestUserImage;
        Button friendRequestAcceptBtn, friendRequestRejectBtn;
        LinearLayout _noRequestsFoundLl, friendRequestHeader,recycler_row_layout;
        AvatarView avatarLoader;

        public ViewHolder(View itemView) {
            super(itemView);
            _noRequestsFoundLl = itemView.findViewById(R.id.noRequestsFoundLl);
            friendRequestHeader = itemView.findViewById(R.id.friendRequestHeader);
            friendRequestUserImage = itemView.findViewById(R.id.friendRequestUserImage);
            friendRequestStatusTv = itemView.findViewById(R.id.friendRequestStatusTv);
            friendRequestUsernameTv = itemView.findViewById(R.id.friendRequestUsernameTv);
            friendRequestAcceptBtn = itemView.findViewById(R.id.friendRequestAcceptBtn);
            friendRequestRejectBtn = itemView.findViewById(R.id.friendRequestRejectBtn);
            recycler_row_layout = itemView.findViewById(R.id.recycler_row_layout);
            avatarLoader = itemView.findViewById(R.id.avatarLoader);
        }
    }

}





















//public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestAdapter.ViewHolder> {
//
//    Socket mSocket;
//    ContactsFragment.FriendRequestInterface friendRequestInterface;
//    HomeScreen homeScreen;
//    AppConstants appConstants = new AppConstants();
//     JSONArray friendRequestsArray;
//     static int mm;
//
//    public FriendRequestAdapter(JSONArray friendRequestsArray) {
//        this.friendRequestsArray = friendRequestsArray;
//        mSocket = AppConstants.mSocket;
//        Log.e(TAG, "friend Request if1dwd");
//        homeScreen = new HomeScreen();
//
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_friend_request_adapter, parent, false);
//        friendRequestInterface = (ContactsFragment.FriendRequestInterface) view.getContext();
//        return new ViewHolder(view);
//
//    }
//
//    @Override
//    public void onBindViewHolder(final ViewHolder holder, final int position) {
//
//        try {
//            if (jsonArray.getJSONObject(position).getString("total").equals("0")) {
//                holder._noRequestsFoundLl.setVisibility(View.VISIBLE);
//            } else {
//                holder.friendRequestHeader.setVisibility(View.VISIBLE);
//                try {
//                    holder.friendRequestUsernameTv.setText(jsonArray.getJSONObject(position).getString("user"));
//                    holder.friendRequestStatusTv.setText(jsonArray.getJSONObject(position).getString("statusMsg"));
//                    Picasso.get()
//                            .load(AppConstants.baseUrl +
//                                    jsonArray.getJSONObject(position).getString("display_picture")
//                            )
//                            .into(holder.friendRequestUserImage);
//
//                    if (jsonArray.getJSONObject(position).getString("presence").equals("0")) {
//                        holder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorOffline));
//                    } else if (jsonArray.getJSONObject(position).getString("presence").equals("1")) {
//                        holder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorOnline));
//                    } else if (jsonArray.getJSONObject(position).getString("presence").equals("2")) {
//                        holder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorBusy));
//                    } else if (jsonArray.getJSONObject(position).getString("presence").equals("3")) {
//                        holder.friendRequestUserImage.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorAway));
//                    }
//
//                    holder.friendRequestAcceptBtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            JSONObject jsonObject = new JSONObject();
//                            try {
//                                jsonObject.put("username", holder.friendRequestUsernameTv.getText().toString().trim());
//                                mSocket.emit("approve add request", jsonObject);
//                                friendRequestsArray.remove(position);
//                                notifyItemRemoved(position);
//                                notifyItemRangeChanged(position, friendRequestsArray.length());
//                                friendRequestInterface.requests(friendRequestsArray, 0, 1);
//                                ContactsFragment contactsFragment = new ContactsFragment();
//                                contactsFragment.getContacts();
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//
//                    holder.friendRequestRejectBtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            JSONObject jsonObject = new JSONObject();
//                            try {
//                                jsonObject.put("username", holder.friendRequestUsernameTv.getText().toString().trim());
//                                mSocket.emit("reject add request", jsonObject);
//                                friendRequestsArray.remove(position);
//                                notifyItemRemoved(position);
//                                notifyItemRangeChanged(position, friendRequestsArray.length());
//                                friendRequestInterface.requests(friendRequestsArray, 0, 1);
//
//                                ContactsFragment contactsFragment = new ContactsFragment();
//                                contactsFragment.getContacts();
//
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return friendRequestsArray.length();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        TextView friendRequestUsernameTv, friendRequestStatusTv;
//        CircleImageView friendRequestUserImage;
//        Button friendRequestAcceptBtn, friendRequestRejectBtn;
//        LinearLayout _noRequestsFoundLl, friendRequestHeader;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//            _noRequestsFoundLl = itemView.findViewById(R.id.noRequestsFoundLl);
//            friendRequestHeader = itemView.findViewById(R.id.friendRequestHeader);
//            friendRequestUserImage = itemView.findViewById(R.id.friendRequestUserImage);
//            friendRequestStatusTv = itemView.findViewById(R.id.friendRequestStatusTv);
//            friendRequestUsernameTv = itemView.findViewById(R.id.friendRequestUsernameTv);
//            friendRequestAcceptBtn = itemView.findViewById(R.id.friendRequestAcceptBtn);
//            friendRequestRejectBtn = itemView.findViewById(R.id.friendRequestRejectBtn);
//
//        }
//    }
//
//
//}
