package com.app.websterz.warfarechat.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.appolica.flubber.Flubber;
import com.appolica.flubber.annotations.RepeatMode;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Repeatable;


public class RewardActivity extends AppCompatActivity {

    MySingleTon mySingleTon;
    Button Closebutton;
    TextView ServerMessageTextView;
    ImageView CoinImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        Closebutton = findViewById(R.id.btn_close_reward_activity);
        CoinImageView = findViewById(R.id.coinImg);
        ServerMessageTextView = findViewById(R.id.textView_server_message);
        mySingleTon = MySingleTon.getInstance();

        Flubber.with()
                .animation(Flubber.AnimationPreset.MORPH)
                .autoStart(true)
                .repeatCount(100)
                .duration(1000)
                .createFor(CoinImageView)
                .start();

        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("rewardpopup"));
            String popup = jsonObject.getString("popup");
            String ServerMsg = jsonObject.getString("message");
            String totalCoin = jsonObject.getString("total_coins");

            mySingleTon.coins = totalCoin;
            ServerMessageTextView.setText(ServerMsg);
            SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(RewardActivity.this).edit();
            prefEditor55.putString("coins", totalCoin);
            prefEditor55.apply();




        } catch (JSONException e) {
            e.printStackTrace();
        }

        Closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySocketsClass.static_objects.one_time_reward=null;
                finish();
            }
        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity=RewardActivity.this;
    }
}
