package com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.nkzawa.emitter.Emitter;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class UsersBlockerAdapter extends RecyclerView.Adapter<UsersBlockerAdapter.ViewHolder> {

    ArrayList<UserBlockerModelClass> MyBlockerList;
    Context MyContext;
    TotalBlockUsers totalBlockUsers;

    public UsersBlockerAdapter(ArrayList<UserBlockerModelClass> myBlockerList, Context myContext , TotalBlockUsers totalBlockUsers) {
        MyBlockerList = myBlockerList;
        MyContext = myContext;
        this.totalBlockUsers = totalBlockUsers;
    }

    @NonNull
    @Override
    public UsersBlockerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_blocker_row_layout, parent, false);
        return new UsersBlockerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersBlockerAdapter.ViewHolder holder, final int position) {
      //  Picasso.get().load(baseUrl + MyBlockerList.get(position).getUserDp()).into(holder.BlockedUserImage);
        GlideApp.with(holder.itemView.getContext()).load(baseUrl + MyBlockerList.get(position).getUserDp()).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).
                into(holder.BlockedUserImage);
        holder.BlockedUserName.setText(MyBlockerList.get(position).getUserName());
        holder.UnblockeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               UnBlockingUserMethod(MyBlockerList.get(position).getUserName());

            }
        });

    }

    private void UnBlockingUserMethod(String Username)
    {
        try {
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("username",Username);
            mSocket.off(MySocketsClass.ListenersClass.UNBLOCK_SHOUT_USER_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.UNBLOCK_SHOUT_USER,jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.UNBLOCK_SHOUT_USER_RESPONSE,unblock_shout_user_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return MyBlockerList.size();
    }

    Emitter.Listener unblock_shout_user_response=new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            ((Activity)MyContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
            JSONObject jsonObject=(JSONObject)args[0];
            try {
                String Success=jsonObject.getString("success");
                String username=jsonObject.getString("username");
                if (Success.equals("1"))
                {
                    for (int i=0;i<MyBlockerList.size();i++)
                    {
                        if (username.equals(MyBlockerList.get(i).getUserName()))
                        {
                            MyBlockerList.remove(i);
                            notifyDataSetChanged();
                            totalBlockUsers.RemainingBlockers(String.valueOf(MyBlockerList.size()));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
                }
            });
        }
    };

    static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView BlockedUserImage;
        TextView BlockedUserName;
        Button UnblockeButton;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            BlockedUserName = itemView.findViewById(R.id.BlockedUserName);
            BlockedUserImage = itemView.findViewById(R.id.BlockedUserImage);
            UnblockeButton = itemView.findViewById(R.id.UnblockeButton);
        }
    }
}
