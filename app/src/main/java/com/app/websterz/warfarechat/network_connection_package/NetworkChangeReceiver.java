package com.app.websterz.warfarechat.network_connection_package;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        String status = NetworkUtil.getConnectivityStatusString(context);

        if (status.equals("Not connected to Internet")) {
            Toast.makeText(context, status, Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context, status, Toast.LENGTH_LONG).show();
        }

    }

}
