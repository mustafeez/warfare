package com.app.websterz.warfarechat.my_profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.my_profile.FootPrintAdapter.ViewingFootPrintAdapter;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ViewingFootprintsResponse extends AppCompatActivity {
    RelativeLayout parentLinearLayout;
    RecyclerView FootPrintsRecyclerView;
    LinearLayout MyParentlayout;
    LinearLayout returning_imageview=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewing_footprints_response);
        Initializations();
        MyCode();
    }
    private void Initializations(){
        parentLinearLayout=findViewById(R.id.parentLinearLayout);
        FootPrintsRecyclerView=findViewById(R.id.FootPrintsRecyclerView);
        MyParentlayout=findViewById(R.id.MyParentlayout);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(ViewingFootprintsResponse.this,RecyclerView.VERTICAL,false);
        FootPrintsRecyclerView.setLayoutManager(layoutManager);

    }

    private void MyCode(){
        returning_imageview= MySocketsClass.custom_layout.Loader_image(ViewingFootprintsResponse.this,parentLinearLayout);
        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("JsonObject"));
            FootPrintsRecyclerView.setAdapter(new ViewingFootPrintAdapter(jsonObject));
            if (returning_imageview.getVisibility()== View.VISIBLE)
            {
                returning_imageview.setVisibility(View.GONE);
            }
            MyParentlayout.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity=ViewingFootprintsResponse.this;
        MySocketsClass.static_objects.global_layout=parentLinearLayout;
    }
}

