package com.app.websterz.warfarechat.homeScreen.fragments.chats;


import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;

import java.util.ArrayList;
import java.util.Collections;


public class ChatsFragment extends Fragment {
RecyclerView RecentChatsRecycler;
View view;

    public ChatsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_home_fragment3, container, false);
        Initializations();
        MyCode();
        return view;
    }
    public void Initializations(){
        RecentChatsRecycler=view.findViewById(R.id.recent_chats_recycler);
        @SuppressLint("WrongConstant")
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        RecentChatsRecycler.setLayoutManager(layoutManager);
        RecentChatsRecycler.getRecycledViewPool().setMaxRecycledViews(0, 0);

    }
    public void MyCode(){
            MySocketsClass.static_objects.chatsRecyclerViewAdapter = new ChatsRecyclerViewAdapter(MySocketsClass.static_objects.NewUserMessageList);
            RecentChatsRecycler.setAdapter(MySocketsClass.static_objects.chatsRecyclerViewAdapter);
//             MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
    }


}
