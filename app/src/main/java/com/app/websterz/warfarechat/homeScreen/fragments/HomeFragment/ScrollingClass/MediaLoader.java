package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ScrollingClass;

import android.annotation.SuppressLint;
import android.widget.ImageView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.AlbumLoader;

public class MediaLoader implements AlbumLoader {

    @Override
    public void load(ImageView imageView, AlbumFile albumFile) {
        load(imageView, albumFile.getPath());
    }

    @Override
    public void load(ImageView imageView, String url) {

        GlideApp.with(imageView)
                .load(url)
                .placeholder(R.color.dark_gray)
                .error(R.color.dark_gray)
                .fitCenter()
                .centerCrop()
                .into(imageView);
    }
}
