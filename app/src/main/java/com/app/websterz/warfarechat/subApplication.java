package com.app.websterz.warfarechat;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.MobileAds;

import androidx.multidex.MultiDex;

public class subApplication extends Application {
    public static final String chaneel_id="exampleservicechannel";

    @Override
    public void onCreate() {
        super.onCreate();
        createnotificationchannel();

        MobileAds.initialize(subApplication.this, getResources().getString(R.string.test_ad_mob_id));
        MobileAds.setAppMuted(true);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void createnotificationchannel(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel servicechannel= new NotificationChannel(
                    chaneel_id,
                    "Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );
            NotificationManager manager=getSystemService(NotificationManager.class);
            manager.createNotificationChannel(servicechannel);
        }
    }
}
