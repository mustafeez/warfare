package com.app.websterz.warfarechat.myRoomDatabase;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {UserPrivateChatTables.class},version = 1)
public abstract class WarfareDatabase extends RoomDatabase {

    public abstract MyDataBaseAccessObject myDataBaseAccessObject();

}
