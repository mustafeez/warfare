package com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ShoutsBlockerAdapter extends RecyclerView.Adapter<ShoutsBlockerAdapter.ViewHolder> {

    ArrayList<ShoutsBlockerModelClass> MyBlockerList;
    Context MyContext;
    TotalBlockUsers totalBlockUsers;

    public ShoutsBlockerAdapter(ArrayList<ShoutsBlockerModelClass> myBlockerList, Context myContext, TotalBlockUsers totalBlockUsers) {
        MyBlockerList = myBlockerList;
        MyContext = myContext;
       this.totalBlockUsers = totalBlockUsers;
    }

    @NonNull
    @Override
    public ShoutsBlockerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shouts_blocker_row_layout, parent, false);
        return new ShoutsBlockerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShoutsBlockerAdapter.ViewHolder holder, final int position) {

        if (MyBlockerList.get(position).getShoutText().contains("www.youtube.com") ||
                MyBlockerList.get(position).getShoutText().contains("https://youtu.be/")) {
            String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(MyBlockerList.get(position).getShoutText());
            holder.YoutubeVideoImage.setVisibility(View.VISIBLE);
            holder.YoutubeUrlImageViewCardview.setVisibility(View.VISIBLE);
            holder.SimpleUrlImageViewCardview.setVisibility(View.GONE);
            holder.ImageURL.setVisibility(View.GONE);
            holder.BlockedShoutPost.setText(MyBlockerList.get(position).getShoutText());
            Glide.with(holder.itemView.getContext()).load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/default.jpg").into(holder.YoutubeVideoImage);


        }

        else if (MyBlockerList.get(position).getShoutText().contains("[[") &&
                MyBlockerList.get(position).getShoutText().contains("]]"))
        {
            holder.YoutubeVideoImage.setVisibility(View.GONE);
            holder.YoutubeUrlImageViewCardview.setVisibility(View.GONE);
            holder.SimpleUrlImageViewCardview.setVisibility(View.VISIBLE);
            holder.ImageURL.setVisibility(View.VISIBLE);
            Glide.with(MyContext)
                    .load(baseUrl+MyBlockerList.get(position).getShoutText().split("\\[\\[")[1].replace("]]",""))
                    .into(holder.ImageURL);
        }

        else
        {
            holder.YoutubeVideoImage.setVisibility(View.GONE);
            holder.YoutubeUrlImageViewCardview.setVisibility(View.GONE);
            holder.SimpleUrlImageViewCardview.setVisibility(View.GONE);
            holder.BlockedShoutPost.setText(MyBlockerList.get(position).getShoutText());
            holder.ImageURL.setVisibility(View.GONE);
        }

        Glide.with(holder.itemView.getContext()).load(baseUrl + MyBlockerList.get(position).getUserDp()).into(holder.BlockedUserImage);
        holder.BlockedUserName.setText(MyBlockerList.get(position).getRealName());
        holder.BlockedUserRealName.setText("@" + MyBlockerList.get(position).getUserName());
        holder.ShoutTime.setText(GettingPostingTime(Long.parseLong(MyBlockerList.get(position).getShoutTime())));
        if (MyBlockerList.get(position).getUserRole().equals("1")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.user_icon).into(holder.UserRank);
        } else if (MyBlockerList.get(position).getUserRole().equals("2")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.mod_icon).into(holder.UserRank);
        } else if (MyBlockerList.get(position).getUserRole().equals("3")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.staff_icon).into(holder.UserRank);
        } else if (MyBlockerList.get(position).getUserRole().equals("4")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.mentor_icon).into(holder.UserRank);
        } else if (MyBlockerList.get(position).getUserRole().equals("5")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.merchant_icon).into(holder.UserRank);
        }
        if (MyBlockerList.get(position).getUserAge().equals("1")) {
            if (MyBlockerList.get(position).getUserGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.born_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.born_female).into(holder.UserAge);
            }
        } else if (MyBlockerList.get(position).getUserAge().equals("2")) {
            if (MyBlockerList.get(position).getUserGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.teen_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.teen_female).into(holder.UserAge);
            }
        } else if (MyBlockerList.get(position).getUserAge().equals("3")) {
            if (MyBlockerList.get(position).getUserGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.young_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.young_female).into(holder.UserAge);
            }
        } else if (MyBlockerList.get(position).getUserAge().equals("4")) {
            if (MyBlockerList.get(position).getUserGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.adult_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.adult_female).into(holder.UserAge);
            }
        } else if (MyBlockerList.get(position).getUserAge().equals("5")) {
            if (MyBlockerList.get(position).getUserGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.mature_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.mature_female).into(holder.UserAge);
            }
        }

        holder.UnblockeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnBlockingSHout(MyBlockerList.get(position).getShoutId());
            }
        });
    }

    private String GettingPostingTime(long time_reg){
        String last_final_time_reg = null;
        long time_reg2 = time_reg * 1000;
        long now = System.currentTimeMillis();
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "just now";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "a minute ago";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "an hour ago";
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago";
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "yesterday";
        } else {

            long calculating_months=diff2 / DAY_MILLIS;
            if (calculating_months<30)
            {
                last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
            }
            else if (calculating_months>=30&&calculating_months<=360)
            {
                last_final_time_reg=calculating_months/30 +" Month(s) ago";
            }
            else if (calculating_months>=30&&calculating_months>360)
            {
                last_final_time_reg=calculating_months/360 +" Year(s) ago";
            }
        }
        return last_final_time_reg;
    }

    private String GettingVideoIdFromYoutubeLink(String url) {
        if (url.contains("v=")) {
            if (url.contains("&")) {
                String[] u = url.split("v=");
                u = u[1].split("&");
                url = u[0];
            } else {
                String[] u = url.split("v=");
                url = u[1];
            }
        } else {
            String[] u = url.split("be/");
            url = u[1];
        }
        return url;
    }

    private void UnBlockingSHout(String ShoutID){

        try {
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("id",ShoutID);
            mSocket.off(MySocketsClass.ListenersClass.UNBLOCK_SHOUT_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.UNBLOCK_SHOUT,jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.UNBLOCK_SHOUT_RESPONSE,unblock_shout_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    @Override
    public int getItemCount() {
        return MyBlockerList.size();
    }

    Emitter.Listener unblock_shout_response=new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            Log.e("ShoutBl","Listner");

            ((Activity)MyContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {

            try {
                JSONObject jsonObject=(JSONObject)args[0];
                String Success=jsonObject.getString("success");
                if (Success.equals("1"))
                {
                    Log.e("ShoutBl","InIf");
                    String ShoutID=jsonObject.getString("id");
                    for (int i=0;i<MyBlockerList.size();i++)
                    {
                        if (ShoutID.equals(MyBlockerList.get(i).getShoutId()))
                        {
                            Log.e("ShoutBl","finded");
                            MyBlockerList.remove(i);
                            notifyDataSetChanged();
                            totalBlockUsers.RemainingBlockers(String.valueOf(MyBlockerList.size()));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

                }
            });
        }
    };


    static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView BlockedUserImage;
        TextView BlockedUserName,BlockedUserRealName,ShoutTime,BlockedShoutPost;
        ImageView UserRank,UserAge,YoutubeVideoImage,ImageURL;
        Button UnblockeButton;
        CardView YoutubeUrlImageViewCardview,SimpleUrlImageViewCardview;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            BlockedUserName = itemView.findViewById(R.id.BlockedUserName);
            BlockedUserImage = itemView.findViewById(R.id.BlockeUserImage);
            UnblockeButton = itemView.findViewById(R.id.UnblockeButton);
            BlockedUserRealName = itemView.findViewById(R.id.BlockedUserRealName);
            ShoutTime = itemView.findViewById(R.id.ShoutTime);
            BlockedShoutPost = itemView.findViewById(R.id.BlockedShoutPost);
            UserRank = itemView.findViewById(R.id.UserRank);
            UserAge = itemView.findViewById(R.id.UserAge);
            YoutubeUrlImageViewCardview = itemView.findViewById(R.id.YoutubeUrlImageViewCardview);
            YoutubeVideoImage = itemView.findViewById(R.id.YoutubeVideoImage);
            SimpleUrlImageViewCardview = itemView.findViewById(R.id.SimpleUrlImageViewCardview);
            ImageURL = itemView.findViewById(R.id.ImageURL);
        }
    }
}
