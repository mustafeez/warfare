package com.app.websterz.warfarechat.constants;

import android.content.SharedPreferences;
import android.util.Log;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;

public class AppConstants {

    public static Socket mSocket;
  // public static String baseUrl="https://5.39.63.226:2019/";
   //public static String baseUrl="http://chat.warfarechat.com/";
    public AppConstants() {
        {

             final TrustManager[] trustAllCerts= new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] {};
                }

                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };


            try {

                SSLContext mySSLContext = null;
                try {
                    mySSLContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                try {
                    mySSLContext.init(null, trustAllCerts, null);
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }

               HostnameVerifier myHostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };

                IO.Options opts = new IO.Options();
                opts.sslContext = mySSLContext;
                opts.hostnameVerifier = myHostnameVerifier;




                mSocket = IO.socket(baseUrl,opts);
                Log.e("hostss",baseUrl+"in App constants class");
            } catch (URISyntaxException e) {

                Log.d("e", e.toString());
                e.printStackTrace();
            }
        }

    }




}