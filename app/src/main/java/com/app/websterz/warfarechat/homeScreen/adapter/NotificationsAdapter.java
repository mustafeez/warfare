package com.app.websterz.warfarechat.homeScreen.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.ExplainingShoutActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.MyShoutDetailsActivity;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    JSONArray notificaitonsArray;
    LinearLayout noNotificationsFoundLl;

    public NotificationsAdapter(JSONArray notificaitonsArray) {
        this.notificaitonsArray = notificaitonsArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_nofications_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (notificaitonsArray.length() == 0) {
            noNotificationsFoundLl.setVisibility(View.VISIBLE);
        }
        else {

        try {
            String[] notificationText = notificaitonsArray.getJSONObject(position).getString("message").split("\\|");
            String regexStr = "^[0-9]*$";
            if (notificationText[1].matches(regexStr)) {
                long time = Long.parseLong(notificationText[1]);
                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                cal.setTimeInMillis(time * 1000);

                String date = DateFormat.format("EEE d, MMM yyyy hh:mm a", cal).toString();
                String today_time = DateFormat.format("hh:mm:ss a", cal).toString();
                String date2 = DateFormat.format("yyyy / MM / d", cal).toString();
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / d");
                String current_date = mdformat.format(calendar.getTime());

                if (current_date.equals(date2)) {
                    holder.notificationTimeTv.setText("Today at " + today_time);
                } else {
                    holder.notificationTimeTv.setText(date);
                }
            } else {
                holder.notificationTimeTv.setText("Invalid date");
            }

            if (notificationText[0].contains("<")) {
                holder.notificationTv.setText(notificationText[0].substring(0, notificationText[0].indexOf("<")) + notificationText[0].substring(notificationText[0].indexOf(">") + 1));
            } else {
                holder.notificationTv.setText(notificationText[0]);
            }
            if (notificationText[3].trim().equals("attack")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.bomb_icon).into(holder.notificationIcon);
              //  holder.notificationIcon.setImageResource(R.drawable.bomb_icon);
            } else if (notificationText[3].trim().equals("add") || notificationText[2].equals("signup")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.user_clock_solid).into(holder.notificationIcon);
               // holder.notificationIcon.setImageResource(R.drawable.user_clock_solid);
            } else if (notificationText[3].trim().equals("reward") || notificationText[2].equals("paid")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.coin_image).into(holder.notificationIcon);
              //  holder.notificationIcon.setImageResource(R.drawable.coins_icon);
            } else if (notificationText[3].trim().equals("like")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.heart_solid).into(holder.notificationIcon);
              //  holder.notificationIcon.setImageResource(R.drawable.heart_solid);
            } else if (notificationText[3].trim().equals("shout")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.shout_black).into(holder.notificationIcon);
              //  holder.notificationIcon.setImageResource(R.drawable.shout_black);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.info_circle_solid).into(holder.notificationIcon);
              //1  holder.notificationIcon.setImageResource(R.drawable.info_circle_solid);
            }
            //}
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.e("Notfvtb",notificaitonsArray.getJSONObject(position).getString("message").split("\\|")[3]);
                    if ( notificaitonsArray.getJSONObject(position).getString("message").split("\\|")[3].trim().equals("shout")||
                            notificaitonsArray.getJSONObject(position).getString("message").split("\\|")[3].trim().equals("comment"))
                    {
                        Toast.makeText(holder.itemView.getContext(), "clckd", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(holder.itemView.getContext(), ExplainingShoutActivity.class);
                        intent.putExtra("ShoutedText",notificaitonsArray.getJSONObject(position).getString("message").split("\\|")[4]);
                        holder.itemView.getContext().startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {

        return notificaitonsArray.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView notificationTv, notificationTimeTv;
        ImageView notificationIcon;

        public ViewHolder(View itemView) {
            super(itemView);

            noNotificationsFoundLl = itemView.findViewById(R.id.noNotificationsFoundLl);
            notificationIcon = itemView.findViewById(R.id.notificationIcon);
            notificationTv = itemView.findViewById(R.id.notificationTv);
            noNotificationsFoundLl = itemView.findViewById(R.id.noNotificationsFoundLl);
            notificationTimeTv = itemView.findViewById(R.id.notificationTimeTv);
        }
    }
}
