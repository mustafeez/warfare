package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.Rest.ApiClient;
import com.app.websterz.warfarechat.Rest.ApiInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.ExplainingShoutActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.NewShoutActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.CategrizedShoutsAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.TrendingShoutsAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.SuggestionsRowAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.UserWallAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.ShoutSharingInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.UserNameClickedSearchShout;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.WallShoutingInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.CategrizedShoutsModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.MakeNewFriendsModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.TrendingShoutsModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.UserWallLayoutModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.MultiPartsClassesForWallImagePost.WallImagePostModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ScrollingClass.EndlessRecyclerViewScrollListener;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.my_profile.My_profile;
import com.asksira.webviewsuite.WebViewSuite;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.stfalcon.chatkit.messages.MessagesList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_FIRST_USER;
import static android.app.Activity.RESULT_OK;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class HomeFragment extends Fragment implements ShoutSharingInterface, UserNameClickedSearchShout, WallShoutingInterface {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private WebViewSuite webViewSuite;
    private View view;
    private String AutoThemeTime = "06:00:00 pm";
    private TextView MainHeading, SearchTextView;
    private ImageView MainSearchIcon, MaleSearchImage, FemaleSearchImage, SoulmateSearchImage, FriendSearchImage, GamePlayerSearchImage,
            SuggestionsLayoutButton, UserWallLayoutButton, ShoutViewShowingButton, ShowingfromUserNameImage, ShowingfromFollowingImageview,
            ShowingOnlyFromMeImageview, ShowingFromPublicImageView, ImageShoutButton, ImageUploadingButtonForWall, shoutsSwipeUpButton,
            DeleteSelectedShoutImage, filtersButton, newShoutButton;
    private LinearLayout MaleSearch, FemaleSearch, SoulmateSearch, FriendSearch, GamePlayerSearch, CategriesView,
            SearchButton, WallPostingShoutView, FiltersDetailView, WallFiltersButton,
            ShowingSpecifiFromUserLayout, ShowingFromUser, ShowingFromFollowing, ShowingOnlyFromMe, ShowingFromPublic,
            ShowingFromUserNamelayout, sugestedFriendsLayoutBtn,shoutsLayoutBtn,loader_imageview_Layout;
    private RelativeLayout OptionsLayoutForSearch, first_layout, UserWallLayout,
            FriendsSuggestionsLayout, UpoadedShoutImageView;
    private RecyclerView SuggestionsRecyclerview, UsersWallRecyclerview, suggestedShoutsRecyclerView, categrizedShoutsRecyclerView;
    private ArrayList<MakeNewFriendsModelClass> FriendSuggestionArraylist = new ArrayList<>();
    private ArrayList<CategrizedShoutsModelClass> categryShoutsArrayList = new ArrayList<>();
    private SuggestionsRowAdapter suggestionsRowAdapter;
    private TrendingShoutsAdapter trendingShoutsAdapter;
    UserWallAdapter userwalladapter;
    private String MaleValue = "0", FemaleValue = "0", SoulmateValue = "0", GameplayerValue = "0", FriendValue = "0";
    private int TotalValues, OffsetValue;
    private MySingleTon mySingleTon;
    private GridLayoutManager giftslayoutmanager;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private String RVScrolling = "no";
    private CircleImageView MyDpWhilePosting;
    private EmojiconEditText WallPostDescription;
    private Button WallShoutButton;
    private CircleImageView ShowingSmileys;
    EmojIconActions emojIcon;
    LinearLayoutManager MyLinearLayout;
    private EditText SearchPostByNameEditText, ShowingFromSpecialNameEditText;
    private int TotalShoutsValue = 0;
    private int TotaloffsetValueForShouts = 0;
    private int TotalshownValueForShouts = 0;
    private String ShoutingCategoriesShowing = "public";
    private long delay = 5000; // 5 seconds after user stops typing
    private long last_text_edit = 0;
    private Handler handler = new Handler();
    int DeletingShoutId;
    String ImageShoutURL = null;
    private WallShoutingInterface wallShoutingInterface;
    private RecyclerView.LayoutManager horizontalLayoutManager, horizontal_categraize_layout_manager;
    private Button ShowingFromSpecialNameButton;
    public final int NUMBER_OF_ADS = 2;
    int offset = 9;
    int index = 9;
    private AdLoader adLoader;
    private ArrayList<Object> UserWallArrayliSt = new ArrayList<>();
    ArrayList<TrendingShoutsModelClass> trendingShoutsArraylist = new ArrayList<>();
    private ArrayList<UnifiedNativeAd> mNativeAdsList = new ArrayList<>();
    private SwipeRefreshLayout shoutsSwipeRefresh, friendSuggestionRefresh;
    private RelativeLayout mainLayout,noShoutsToDispleLayout;

    public HomeFragment() {
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(listener);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(userNameChangerListener);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(likesDislikesCommentsOnPostReceiver);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(followUserOrUnfollowUser);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(showingMyOwnShouts);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(closingTheApp);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        MySocketsClass.static_objects.shoutsCategryOpened="public";

        MainHeading = view.findViewById(R.id.MainHeading);
        MainSearchIcon = view.findViewById(R.id.MainSearchIcon);
        MaleSearch = view.findViewById(R.id.MaleSearch);
        FemaleSearch = view.findViewById(R.id.FemaleSearch);
        SoulmateSearch = view.findViewById(R.id.SoulmateSearch);
        FriendSearch = view.findViewById(R.id.FriendSearch);
        GamePlayerSearch = view.findViewById(R.id.GamePlayerSearch);
        SearchButton = view.findViewById(R.id.SearchButton);
        SearchTextView = view.findViewById(R.id.SearchTextView);
        MaleSearchImage = view.findViewById(R.id.MaleSearchImage);
        FemaleSearchImage = view.findViewById(R.id.FemaleSearchImage);
        SoulmateSearchImage = view.findViewById(R.id.SoulmateSearchImage);
        FriendSearchImage = view.findViewById(R.id.FriendSearchImage);
        GamePlayerSearchImage = view.findViewById(R.id.GamePlayerSearchImage);
        OptionsLayoutForSearch = view.findViewById(R.id.OptionsLayoutForSearch);
        SuggestionsRecyclerview = view.findViewById(R.id.SuggestionsRecyclerview);
        UsersWallRecyclerview = view.findViewById(R.id.UsersWallRecyclerview);
        mainLayout = view.findViewById(R.id.mainLayout);
        ShowingSpecifiFromUserLayout = view.findViewById(R.id.ShowingSpecifiFromUserLayout);
        first_layout = view.findViewById(R.id.first_layout);
        UserWallLayout = view.findViewById(R.id.UserWallLayout);
        FriendsSuggestionsLayout = view.findViewById(R.id.FriendsSuggestionsLayout);
        UserWallLayoutButton = view.findViewById(R.id.UserWallLayoutButton);
        SuggestionsLayoutButton = view.findViewById(R.id.SuggestionsLayoutButton);
        ShowingFromPublic = view.findViewById(R.id.ShowingFromPublic);
        ShowingOnlyFromMe = view.findViewById(R.id.ShowingOnlyFromMe);
        ShowingFromFollowing = view.findViewById(R.id.ShowingFromFollowing);
        ShowingFromUser = view.findViewById(R.id.ShowingFromUser);
        noShoutsToDispleLayout = view.findViewById(R.id.noShoutsToDispleLayout);

        shoutsSwipeUpButton = view.findViewById(R.id.shoutsSwipeUpButton);
        Glide.with(getContext()).load(getResources().getDrawable(R.drawable.shout_arrow_up)).into(shoutsSwipeUpButton);
        ShowingfromUserNameImage = view.findViewById(R.id.ShowingfromUserNameImage);
        ShowingfromFollowingImageview = view.findViewById(R.id.ShowingfromFollowingImageview);
        ShowingOnlyFromMeImageview = view.findViewById(R.id.ShowingOnlyFromMeImageview);
        ShowingFromPublicImageView = view.findViewById(R.id.ShowingFromPublicImageView);
        shoutsSwipeRefresh = view.findViewById(R.id.shoutsSwipeRefresh);
        friendSuggestionRefresh = view.findViewById(R.id.friendSuggestionRefresher);
        MyLinearLayout = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        FriendSuggestionArraylist.clear();
        UserWallArrayliSt.clear();
        mNativeAdsList.clear();
        giftslayoutmanager = new GridLayoutManager(getContext(), 3);
        SuggestionsRecyclerview.setLayoutManager(giftslayoutmanager);
        UsersWallRecyclerview.setLayoutManager(MyLinearLayout);
        userwalladapter = new UserWallAdapter(UserWallArrayliSt, getContext(), this, this, this);
        UsersWallRecyclerview.setAdapter(userwalladapter);
        ShowingFromUserNamelayout = view.findViewById(R.id.ShowingFromUserNamelayout);
        sugestedFriendsLayoutBtn = view.findViewById(R.id.sugestedFriendsLayoutBtn);
        shoutsLayoutBtn = view.findViewById(R.id.shoutsLayoutBtn);
        ShowingFromSpecialNameButton = view.findViewById(R.id.ShowingFromSpecialNameButton);
        ShowingFromSpecialNameEditText = view.findViewById(R.id.ShowingFromSpecialNameEditText);
        filtersButton = view.findViewById(R.id.filtersButton);
        CategriesView = view.findViewById(R.id.CategriesView);
        newShoutButton = view.findViewById(R.id.newShoutButton);
        suggestedShoutsRecyclerView = view.findViewById(R.id.suggestedShoutsRecyclerView);
        categrizedShoutsRecyclerView = view.findViewById(R.id.categrizedShoutsRecyclerView);
        horizontalLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        horizontal_categraize_layout_manager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        suggestedShoutsRecyclerView.setLayoutManager(horizontalLayoutManager);
        categrizedShoutsRecyclerView.setLayoutManager(horizontal_categraize_layout_manager);

        trendingShoutsAdapter = new TrendingShoutsAdapter(trendingShoutsArraylist, getContext());
        suggestedShoutsRecyclerView.setAdapter(trendingShoutsAdapter);

        suggestionsRowAdapter = new SuggestionsRowAdapter(FriendSuggestionArraylist, getContext());
        SuggestionsRecyclerview.setAdapter(suggestionsRowAdapter);
        mySingleTon = MySingleTon.getInstance();

        getAppTheme();
        if (mSocket != null) {
            MobileAds.initialize(getContext(), getString(R.string.test_native_admob_app_id));
            MainCode();
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(listener, new IntentFilter("ChangingDp"));
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(userNameChangerListener, new IntentFilter("ChangingUserId"));
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(likesDislikesCommentsOnPostReceiver, new IntentFilter("updateLikesEtcOnPost"));
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(followUserOrUnfollowUser, new IntentFilter("followORunfoolow"));
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(showingMyOwnShouts,new IntentFilter("ShowingMineShouts"));
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(closingTheApp,new IntentFilter("ClozingSocio"));

            UsersWallRecyclerview.addOnScrollListener(new MessagesList.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    //yahan hide kia view test purpose k lye, nahi chala to dlt krna hide wala code
                    hidingShowingSpecifiFromUserLayout();
                    hidingActivitiesLayoutsIfOpen("NotUpperLayout");
                    if (!recyclerView.canScrollVertically(-1)) {
                        hideButton();
                        hidingActivitiesLayoutsIfOpen("ShowLayout");
                    } else {
                        showButton();
                        hidingActivitiesLayoutsIfOpen("hideLayout");
                    }
                }
            });

            shoutsSwipeUpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    movingToFirstPosition();
                }
            });
        }
        return view;
    }

    private void showButton() {
        if (shoutsSwipeUpButton.getVisibility() == View.GONE)
            shoutsSwipeUpButton.setVisibility(View.VISIBLE);
    }

    private void hideButton() {
        if (shoutsSwipeUpButton.getVisibility() == View.VISIBLE)
            shoutsSwipeUpButton.setVisibility(View.GONE);
    }

    private void movingToFirstPosition() {
        hideButton();
        UsersWallRecyclerview.smoothScrollToPosition(0);
    }

    private BroadcastReceiver listener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            for (Object object : UserWallArrayliSt) {
                if (object instanceof UserWallLayoutModelClass) {
                    String URL = intent.getStringExtra("newDpUrl");
                    UserWallLayoutModelClass userWallLayoutModelClass = (UserWallLayoutModelClass) object;
                    if (userWallLayoutModelClass.getPostsUsername().equals(mySingleTon.username)) {
                        userWallLayoutModelClass.setPostsUserDp(URL);
                        userwalladapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };

    private BroadcastReceiver userNameChangerListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            for (Object object : UserWallArrayliSt) {
                if (object instanceof UserWallLayoutModelClass) {
                    String userNewName = intent.getStringExtra("newUserName");
                    UserWallLayoutModelClass userWallLayoutModelClass = (UserWallLayoutModelClass) object;
                    if (userWallLayoutModelClass.getPostsUsername().equals(mySingleTon.username)) {
                        userWallLayoutModelClass.setPostsSimpleName(userNewName);
                        userwalladapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };

    private BroadcastReceiver likesDislikesCommentsOnPostReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            for (Object object : UserWallArrayliSt) {
                if (object instanceof UserWallLayoutModelClass) {
                    //mheree
                    UserWallLayoutModelClass userWallLayoutModelClass = (UserWallLayoutModelClass) object;
                    if (userWallLayoutModelClass.getPostsId().equals(intent.getStringExtra("MainShoutID2"))) {
                        userWallLayoutModelClass.setPostsTotallikes(intent.getStringExtra("totalUpdatedLikesValue2"));
                        userWallLayoutModelClass.setPostsTotaldislikes(intent.getStringExtra("totalUpdatedDisLikesValue2"));
                        userWallLayoutModelClass.setPostsTotalcomments(intent.getStringExtra("totalUpdatedComentsValue2"));
                        userWallLayoutModelClass.setMyLikeOnPost(intent.getStringExtra("ismyLikeOnPostNewValue2"));
                        userWallLayoutModelClass.setMyDisLikeOnPost(intent.getStringExtra("ismyDisLikeOnPostNewValue2"));
                        userwalladapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };

    private BroadcastReceiver followUserOrUnfollowUser = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String FollowStatusRecevd = intent.getStringExtra("followStatus");
            String emitedUsername = intent.getStringExtra("emittedUserName");
            if (FollowStatusRecevd.equalsIgnoreCase("UnFollowed")) {
                for (Object object : UserWallArrayliSt) {
                    if (object instanceof UserWallLayoutModelClass) {
                        UserWallLayoutModelClass userWallLayoutModelClass = (UserWallLayoutModelClass) object;
                        if (userWallLayoutModelClass.getPostsUsername().equals(emitedUsername)) {
                            userWallLayoutModelClass.setPostsfollow("0");
                        }
                    }
                }
                userwalladapter.notifyDataSetChanged();
            } else {
                for (Object object : UserWallArrayliSt) {
                    if (object instanceof UserWallLayoutModelClass) {
                        UserWallLayoutModelClass userWallLayoutModelClass = (UserWallLayoutModelClass) object;
                        if (userWallLayoutModelClass.getPostsUsername().equals(emitedUsername)) {
                            userWallLayoutModelClass.setPostsfollow("1");
                        }
                    }
                }
                userwalladapter.notifyDataSetChanged();
            }
        }
    };

    private BroadcastReceiver showingMyOwnShouts=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            OnlymineOwnPostShowing();
        }
    };
    private BroadcastReceiver closingTheApp=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (UserWallLayout.getVisibility() == View.GONE) {
                MySocketsClass.static_objects.shoutCategryShowing = "SHOWING FROM PUBLIC";
                resetingTotalShoutValues();
                UserWallLayoutShowing();
                GettingDataForUserWall("public");
                //  UsersWallRecyclerview.scrollToPosition(0);
                Log.e("updatn", "clicked");
            }
            else if (MySocketsClass.static_objects.shoutsCategryOpened != null &&
                    !MySocketsClass.static_objects.shoutsCategryOpened.equalsIgnoreCase("public")){
                OnlyPublicPostShowing();}
            else {
                MySocketsClass.static_objects.shoutsCategryOpened = null;
                getActivity().moveTaskToBack(true);
                getActivity().finishAffinity();
            }
        }
    };

    private void MainCode() {
        Log.e("shuhu", "inOncreateMainCode");
        MainHeading.setText("Suggested for you...");
        MaleSearchImage.setBackgroundResource(R.drawable.male_unselected);
        FemaleSearchImage.setImageResource(R.drawable.female_unselected);
        SoulmateSearchImage.setBackgroundResource(R.drawable.heart_unselected);
        FriendSearchImage.setBackgroundResource(R.drawable.friendimage_unselected);
        GamePlayerSearchImage.setBackgroundResource(R.drawable.gameplayer_unselected);
        //       ShoutViewShowingButton.setImageResource(R.drawable.icon_shout);

        UserWallLayoutButton.setImageResource(R.drawable.botton_shout_checked);
        SuggestionsLayoutButton.setImageResource(R.drawable.botton_frndsugstn_unchecked);

        settingValuesForCategrizedShouts(0);
        loadingAllTrendingShouts();
        loadingAllNewWallData();

        MainSearchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchSuggestions();
            }
        });
        MaleSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaleSearching();
            }
        });
        FemaleSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FemaleSearching();
            }
        });
        SoulmateSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoulMateSearching();
            }
        });
        FriendSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendSearching();
            }
        });
        GamePlayerSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GamePlayerSearching();
            }
        });
        ShowingFromPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnlyPublicPostShowing();
            }
        });
        ShowingOnlyFromMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnlymineOwnPostShowing();
            }
        });
        ShowingFromFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnlyFollowingPostShowing();
            }
        });
        newShoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stoppingMediaPlayerOfAudioShouts();
                Intent intent = new Intent(getContext(), NewShoutActivity.class);
                startActivityForResult(intent, 12);
            }
        });
        filtersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categriesViewShowingMethod();
            }
        });
        ShowingFromSpecialNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("clicker", "clicked");
                if (!ShowingFromSpecialNameEditText.getText().toString().equals("") && ShoutingCategoriesShowing.equals("user")) {
                    hideKeyboard(getActivity());
                    ShowingLoader();
                    EmittingForGettingCategrizedData(ShowingFromSpecialNameEditText.getText().toString());
                    stoppingMediaPlayerOfAudioShouts();
                    hidingShowingSpecifiFromUserLayout();
                    settingValuesForCategrizedShouts(3);
                }
            }
        });
        ShowingFromUserNamelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnlyUserPostShowing();
            }
        });
        sugestedFriendsLayoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FriendsSuggestionsLayout.getVisibility() == View.GONE) {
                    if (FriendSuggestionArraylist.size() == 0) {
                        RVScrolling = "no";
                        ShowingLoader();
                        mSocket.off(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE);
                        mSocket.emit(MySocketsClass.EmmittersClass.LOAD_FRIENDS);
                        mSocket.on(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE, load_friend_response);
                        ShowingSuggestionslayoutShowing();
                        SuggestionsRecyclerview.scrollToPosition(0);
                        reSetingFriendSuggestionsValues();
                    } else {
                        RVScrolling = "no";
                        ShowingSuggestionslayoutShowing();
                        SuggestionsRecyclerview.scrollToPosition(0);
                    }
                    stoppingMediaPlayerOfAudioShouts();
                }
            }
        });
        shoutsLayoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserWallLayout.getVisibility() == View.GONE) {
                    MySocketsClass.static_objects.shoutCategryShowing = "SHOWING FROM PUBLIC";
                    resetingTotalShoutValues();
                    UserWallLayoutShowing();
                    GettingDataForUserWall("public");
                    //  UsersWallRecyclerview.scrollToPosition(0);
                    Log.e("updatn", "clicked");
                }
            }
        });
        SearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OffsetValue = 0;
                ShowingLoader();
                FriendSuggestionArraylist.clear();
                RVScrolling = "yes";
                CategrizedSearching();
                SearchSuggestions();

            }
        });

        shoutsSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("swipingTest", "swiped test");
                TotaloffsetValueForShouts = 0;
                TotalshownValueForShouts = 0;
                stoppingMediaPlayerOfAudioShouts();
                loadingAllTrendingShouts();
                if (ShoutingCategoriesShowing.equals("user")) {
                    if (!ShowingFromSpecialNameEditText.getText().toString().equals("")) {
                        EmittingForGettingCategrizedData(ShowingFromSpecialNameEditText.getText().toString());
                    }
                } else {
                    GettingDataForUserWall(ShoutingCategoriesShowing);
                }
            }
        });

        friendSuggestionRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (FriendsSuggestionsLayout.getVisibility() == View.VISIBLE) {
                    RVScrolling = "no";
                    ShowingLoader();
                    mSocket.off(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE);
                    mSocket.emit(MySocketsClass.EmmittersClass.LOAD_FRIENDS);
                    mSocket.on(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE, load_friend_response);
                    ShowingSuggestionslayoutShowing();
                    SuggestionsRecyclerview.scrollToPosition(0);
                    reSetingFriendSuggestionsValues();
                }

            }
        });

        SuggestionsRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                // super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = giftslayoutmanager.getChildCount();
                totalItemCount = giftslayoutmanager.getItemCount();
                pastVisiblesItems = giftslayoutmanager.findFirstVisibleItemPosition();

                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    //bottom of recyclerview
                    if (RVScrolling.equals("yes")) {
                        RVScrolling = "no";
                        refreshingShoutsData();
                    }
                }
            }
        });

        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(MyLinearLayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (TotalshownValueForShouts < TotalShoutsValue) {
                    Toast.makeText(getContext(), "Load more", Toast.LENGTH_SHORT).show();
                    TotaloffsetValueForShouts = TotaloffsetValueForShouts + 20;
                    Log.e("Walls", "NewValueOffsetOnLoadingMore->" + TotaloffsetValueForShouts);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("offset", TotaloffsetValueForShouts);
                        jsonObject.put("type", "more");
                        jsonObject.put("privacy", ShoutingCategoriesShowing);
                        if (ShoutingCategoriesShowing.equals("user")) {
                            if (!ShowingFromSpecialNameEditText.getText().toString().equals("")) {
                                jsonObject.put("username", ShowingFromSpecialNameEditText.getText().toString());
                            }
                        }
                        mSocket.off(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE);
                        mSocket.emit(MySocketsClass.EmmittersClass.LOAD_POSTS, jsonObject);
                        mSocket.on(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE, loadMore_posts_response);
                        stoppingMediaPlayerOfAudioShouts();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        UsersWallRecyclerview.addOnScrollListener(scrollListener);
    }

    private void stoppingMediaPlayerOfAudioShouts() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        Intent intent = new Intent("StopShoutsAudioMediaPlayer");
        localBroadcastManager.sendBroadcast(intent);
    }

    private void refreshingShoutsData() {
        loadMoreFriendSuggestions();
    }

    private void MyNewShoutPostSending(String ShoutingTexts) {

        JSONObject jsonObject = new JSONObject();
        try {
            if (ImageShoutURL == null) {
                jsonObject.put("text", ShoutingTexts);
            } else {
                jsonObject.put("text", ShoutingTexts + " " + "[[" + ImageShoutURL + "]]");
            }
            //   ShowingLoader();
            mSocket.off(MySocketsClass.ListenersClass.ADD_SHOUT_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.ADD_SHOUT, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.ADD_SHOUT_RESPONSE, add_shout_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //   }
        hideKeyboard(getActivity());
    }

    private void OnlyUserPostShowing() {
        Toast.makeText(getContext(), "Only User Post", Toast.LENGTH_SHORT).show();
        ShowingfromUserNameImage.setImageResource(R.drawable.tick_image);
        ShowingOnlyFromMeImageview.setImageResource(R.drawable.round_filled_white);
        ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);
        ShowingFromPublicImageView.setImageResource(R.drawable.round_filled_white);
        ShoutingCategoriesShowing = "user";
        MySocketsClass.static_objects.shoutsCategryOpened="user";
        settingValuesForCategrizedShouts(3);
        UsersWallRecyclerview.suppressLayout(true);
        TotaloffsetValueForShouts = 0;
        index = 9;
        offset = 9;
    }

    private void OnlyFollowingPostShowing() {
        Toast.makeText(getContext(), "Only Following Post", Toast.LENGTH_SHORT).show();
        ShowingFromSpecialNameEditText.setText("");
        ShowingfromUserNameImage.setImageResource(R.drawable.round_filled_white);
        ShowingOnlyFromMeImageview.setImageResource(R.drawable.round_filled_white);
        ShowingfromFollowingImageview.setImageResource(R.drawable.tick_image);
        ShowingFromPublicImageView.setImageResource(R.drawable.round_filled_white);
        ShoutingCategoriesShowing = "follow";
        MySocketsClass.static_objects.shoutsCategryOpened="follow";
        settingValuesForCategrizedShouts(1);
        TotaloffsetValueForShouts = 0;
        index = 9;
        offset = 9;
        ShowingLoader();
        UsersWallRecyclerview.suppressLayout(true);
        EmittingForGettingCategrizedData();
        stoppingMediaPlayerOfAudioShouts();
    }

    private void OnlymineOwnPostShowing() {
        if (getContext()!=null){
        Toast.makeText(getContext(), "Only Mine Own Post", Toast.LENGTH_SHORT).show();
        ShowingFromSpecialNameEditText.setText("");
        ShowingfromUserNameImage.setImageResource(R.drawable.round_filled_white);
        ShowingOnlyFromMeImageview.setImageResource(R.drawable.tick_image);
        ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);
        ShowingFromPublicImageView.setImageResource(R.drawable.round_filled_white);
        ShoutingCategoriesShowing = "my";
            MySocketsClass.static_objects.shoutsCategryOpened="my";
        settingValuesForCategrizedShouts(2);
        TotaloffsetValueForShouts = 0;
        index = 9;
        offset = 9;

        //   ifUserNameSearched=null;
        ShowingLoader();
        UsersWallRecyclerview.suppressLayout(true);
        EmittingForGettingCategrizedData();
        stoppingMediaPlayerOfAudioShouts();}
    }

    private void OnlyPublicPostShowing() {
        Toast.makeText(getContext(), "Only Public Post", Toast.LENGTH_SHORT).show();
        ShowingFromSpecialNameEditText.setText("");
        ShowingfromUserNameImage.setImageResource(R.drawable.round_filled_white);
        ShowingOnlyFromMeImageview.setImageResource(R.drawable.round_filled_white);
        ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);
        ShowingFromPublicImageView.setImageResource(R.drawable.tick_image);
        ShoutingCategoriesShowing = "public";
        MySocketsClass.static_objects.shoutsCategryOpened="public";
        settingValuesForCategrizedShouts(0);
        TotaloffsetValueForShouts = 0;
        index = 9;
        offset = 9;
        ShowingLoader();
        UsersWallRecyclerview.suppressLayout(true);
        EmittingForGettingCategrizedData();
        stoppingMediaPlayerOfAudioShouts();
    }

    private void EmittingForGettingCategrizedData() {
        UserWallArrayliSt.clear();
        mNativeAdsList.clear();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("limit", "20");
            jsonObject.put("offset", TotaloffsetValueForShouts);
            jsonObject.put("privacy", ShoutingCategoriesShowing);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.LOAD_POSTS, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE, load_posts_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void EmittingForGettingCategrizedData(String Username) {
        Toast.makeText(getContext(), "Clicked", Toast.LENGTH_SHORT).show();
        UserWallArrayliSt.clear();
        mNativeAdsList.clear();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("limit", "20");
            jsonObject.put("offset", TotaloffsetValueForShouts);
            jsonObject.put("privacy", ShoutingCategoriesShowing);
            jsonObject.put("username", Username);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.LOAD_POSTS, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE, load_posts_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ShowingFilterCategories() {
        if (WallPostingShoutView.getVisibility() == View.VISIBLE) {
            WallPostingShoutView.setVisibility(View.GONE);
        }
        if (FiltersDetailView.getVisibility() == View.VISIBLE) {
            FiltersDetailView.setVisibility(View.GONE);
            ShoutViewShowingButton.setImageResource(R.drawable.icon_shout);
        } else {
            FiltersDetailView.setVisibility(View.VISIBLE);
            FiltersDetailView.bringToFront();
        }
    }

    private void ShowingShoutView() {

        if (FiltersDetailView.getVisibility() == View.VISIBLE) {
            FiltersDetailView.setVisibility(View.GONE);
        }
        if (WallPostingShoutView.getVisibility() == View.VISIBLE) {
            WallPostingShoutView.setVisibility(View.GONE);
            ShoutViewShowingButton.setImageResource(R.drawable.icon_shout);
        } else {
            WallPostingShoutView.setVisibility(View.VISIBLE);
            ShoutViewShowingButton.setImageResource(R.drawable.icon_shout_clicked);
        }
        hideKeyboard(getActivity());
    }

    private void GettingDataForUserWall(String PostsThatShowing) {
        JSONObject jsonObject = new JSONObject();
        try {
            Log.e("ShoutingCieg", PostsThatShowing);
            jsonObject.put("limit", "20");
            jsonObject.put("offset", TotaloffsetValueForShouts);
            jsonObject.put("privacy", PostsThatShowing);

            mSocket.off(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.LOAD_POSTS, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_POSTS_RESPONSE, load_posts_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ShowingSuggestionslayoutShowing() {
        if (FriendsSuggestionsLayout.getVisibility() == View.GONE) {
            FriendsSuggestionsLayout.setVisibility(View.VISIBLE);
            UserWallLayout.setVisibility(View.GONE);
            UserWallLayoutButton.setImageResource(R.drawable.botton_shout_unchecked);
            SuggestionsLayoutButton.setImageResource(R.drawable.botton_frndsugstn_checked);
        }
    }

    private void UserWallLayoutShowing() {
        if (UserWallLayout.getVisibility() == View.GONE) {
            FriendsSuggestionsLayout.setVisibility(View.GONE);
            UserWallLayout.setVisibility(View.VISIBLE);
            UserWallLayoutButton.setImageResource(R.drawable.botton_shout_checked);
            SuggestionsLayoutButton.setImageResource(R.drawable.botton_frndsugstn_unchecked);
        }
        if (UsersWallRecyclerview != null) {
            UsersWallRecyclerview.scrollToPosition(0);
        }
    }

    private void ShowingLoader() {
        loader_imageview_Layout=MySocketsClass.custom_layout.Loader_image(getContext(), mainLayout);
    }

    private void HidingLoader() {
        if (loader_imageview_Layout != null && loader_imageview_Layout.getVisibility() == View.VISIBLE) {
            loader_imageview_Layout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSocket != null) {
            mSocket.off(MySocketsClass.ListenersClass.UPDATE_SHOUTS_DETAILS);
            mSocket.on(MySocketsClass.ListenersClass.UPDATE_SHOUTS_DETAILS, update_shout_details);
        }
    }

    private void SettingDefaultOnResuming() {
        MainHeading.setText("Suggested for you...");
        MaleSearchImage.setBackgroundResource(R.drawable.male_unselected);
        FemaleSearchImage.setImageResource(R.drawable.female_unselected);
        SoulmateSearchImage.setBackgroundResource(R.drawable.heart_unselected);
        FriendSearchImage.setBackgroundResource(R.drawable.friendimage_unselected);
        GamePlayerSearchImage.setBackgroundResource(R.drawable.gameplayer_unselected);
        mSocket.off(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.LOAD_FRIENDS);
        mSocket.on(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE, load_friend_response);
    }

    private void loadMoreFriendSuggestions() {
        OffsetValue = OffsetValue + 20;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("male", MaleValue);
            jsonObject.put("female", FemaleValue);
            jsonObject.put("love", SoulmateValue);
            jsonObject.put("friend", FriendValue);
            jsonObject.put("player", GameplayerValue);
            jsonObject.put("offset", OffsetValue);
            jsonObject.put("limit", "20");
            jsonObject.put("type", "more");
            mSocket.off(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.SEARCH_FRIEND, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE, FilteredLoadFriendsResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void CategrizedSearching() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("male", MaleValue);
            jsonObject.put("female", FemaleValue);
            jsonObject.put("love", SoulmateValue);
            jsonObject.put("player", GameplayerValue);
            jsonObject.put("offset", OffsetValue);
            jsonObject.put("limit", "20");
            mSocket.off(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.SEARCH_FRIEND, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_FRIENDS_RESPONSE, FilteredLoadFriendsResponse);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void SearchSuggestions() {

        if (OptionsLayoutForSearch.getVisibility() == View.VISIBLE) {
            OptionsLayoutForSearch.setVisibility(View.GONE);
            MainHeading.setText("Suggested for you...");
        } else {
            OptionsLayoutForSearch.setVisibility(View.VISIBLE);
            MainHeading.setText("I am looking for:");
        }
    }

    private void GamePlayerSearching() {
        if (GameplayerValue.equals("0")) {
            GameplayerValue = "1";
            GamePlayerSearchImage.setBackgroundResource(R.drawable.gameplayer_selected);
        } else {
            GameplayerValue = "0";
            GamePlayerSearchImage.setBackgroundResource(R.drawable.gameplayer_unselected);
        }
    }

    private void FriendSearching() {
        if (FriendValue.equals("0")) {
            FriendValue = "1";
            FriendSearchImage.setBackgroundResource(R.drawable.friendimage_selected);
        } else {
            FriendValue = "0";
            FriendSearchImage.setBackgroundResource(R.drawable.friendimage_unselected);
        }
    }

    private void SoulMateSearching() {
        if (SoulmateValue.equals("0")) {
            SoulmateValue = "1";
            SoulmateSearchImage.setBackgroundResource(R.drawable.heart_selected);
        } else {
            SoulmateValue = "0";
            SoulmateSearchImage.setBackgroundResource(R.drawable.heart_unselected);
        }
    }

    private void MaleSearching() {
        if (MaleValue.equals("0")) {
            MaleValue = "1";
            MaleSearchImage.setBackgroundResource(R.drawable.male_selected);
        } else {
            MaleValue = "0";
            MaleSearchImage.setBackgroundResource(R.drawable.male_unselected);
        }
    }

    private void FemaleSearching() {
        if (FemaleValue.equals("0")) {
            FemaleValue = "1";
            FemaleSearchImage.setImageResource(R.drawable.femaleselected);
        } else {
            FemaleValue = "0";
            FemaleSearchImage.setImageResource(R.drawable.female_unselected);
        }
    }

    private String GettingUserName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        return prefs.getString("mine_user_name", null);
    }

    private String GettingTheme() {
        String ThemeCode;
        String UserTheme = null;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        ThemeCode = prefs.getString("theme", null);
        if (ThemeCode.equals("0")) {
            UserTheme = "dark";
        } else if (ThemeCode.equals("2")) {
            UserTheme = "light";
        } else if (ThemeCode.equals("1")) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
                Date systemDate = Calendar.getInstance().getTime();
                String myDate = sdf.format(systemDate);
                Date Date1 = sdf.parse(myDate);
                Date Date2 = sdf.parse(AutoThemeTime);
                if (Date1.getTime() < Date2.getTime()) {
                    UserTheme = "light";
                } else {
                    UserTheme = "dark";
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return UserTheme;
    }

    private void DeletingSelectedShoutImageMethod() {
        if (ImageShoutURL != null) {
            Toast.makeText(getContext(), "DeleteClicked", Toast.LENGTH_SHORT).show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("url", ImageShoutURL);
                Log.e("Imagee", ImageShoutURL);
                mSocket.off(MySocketsClass.ListenersClass.DELETE_SHOUT_IMAGE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.DELETE_SHOUT_IMAGE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.DELETE_SHOUT_IMAGE_RESPONSE, DeleteShoutImageResponse);
                Log.e("DeletngSH", "Inemiter");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void resetingTotalShoutValues() {
        TotalShoutsValue = 0;
        TotaloffsetValueForShouts = 0;
        TotalshownValueForShouts = 0;
        index = 9;
        offset = 9;

    }

    private void hidingActivitiesLayoutsIfOpen(String layoutType){
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        Intent intent = new Intent("HidingLayouts");
        intent.putExtra("LayoutType",layoutType);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            RVScrolling = "no";
            if (UsersWallRecyclerview != null) {
                Log.e("shuhu", "scrolingto 0");
                UsersWallRecyclerview.scrollToPosition(0);
            }

            //   getAppTheme();
        }
    }

    private void getAppTheme() {
//        int theme2;
//        SharedPreferences Themeprefs = PreferenceManager.getDefaultSharedPreferences(getContext());
//        theme2 = Integer.parseInt(Themeprefs.getString("theme", null));
//        mySingleTon.theme = String.valueOf(theme2);
//        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
//        prefEditor55.putString("theme", String.valueOf(theme2));
//        prefEditor55.apply();
//        MySocketsClass.static_objects.theme = theme2;
//        if (theme2 == 0) {
//            OptionsLayoutForSearch.setBackgroundColor(getResources().getColor(R.color.transparent));
//            first_layout.setBackgroundColor(getResources().getColor(R.color.transparent));
//            WallPostingShoutView.setBackgroundColor(getResources().getColor(R.color.black));
//            FiltersDetailView.setBackgroundColor(getResources().getColor(R.color.black));
//
//        } else if (theme2 == 1) {
//            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//            Date systemDate = Calendar.getInstance().getTime();
//            String myDate = sdf.format(systemDate);
//            try {
//                Date Date1 = sdf.parse(myDate);
//                Date Date2 = sdf.parse(AutoThemeTime);
//                if (Date1.getTime() < Date2.getTime()) {
//                    OptionsLayoutForSearch.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent2));
//                    first_layout.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent2));
////                    WallPostingShoutView.setBackgroundColor(getResources().getColor(R.color.new_theme_gray));
////                    FiltersDetailView.setBackgroundColor(getResources().getColor(R.color.new_theme_gray));
//                } else {
//                    OptionsLayoutForSearch.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    first_layout.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    WallPostingShoutView.setBackgroundColor(getResources().getColor(R.color.black));
//                    FiltersDetailView.setBackgroundColor(getResources().getColor(R.color.black));
//                }
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//
//        } else if (theme2 == 2) {
        OptionsLayoutForSearch.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent2));
        first_layout.setBackgroundColor(getResources().getColor(R.color.colorLightBLueTranparent2));
        //           WallPostingShoutView.setBackgroundColor(getResources().getColor(R.color.new_theme_gray));
        //           FiltersDetailView.setBackgroundColor(getResources().getColor(R.color.new_theme_gray));
        //      }
    }

    private void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void shoutDeletingMethod(int PostIdPosition) {
        UserWallLayoutModelClass userWallLayoutModelClass = (UserWallLayoutModelClass) UserWallArrayliSt.get(PostIdPosition);
        DeletingShoutId = PostIdPosition;
        Intent intent = new Intent(getContext(), ExplainingShoutActivity.class);
        intent.putExtra("ShoutedText", userWallLayoutModelClass.getPostsId());
        //  startActivity(intent);
        startActivityForResult(intent, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("shuhu", "In Activity result");
        if (requestCode == 10) {
            Log.e("shuhu", "inRequstCode10");
            // deleting a shout from explaination shout activity
            if (resultCode == RESULT_OK) {
                Log.e("shuhu", "RESULT_OK");
                UserWallArrayliSt.remove(DeletingShoutId);
                userwalladapter.notifyDataSetChanged();
            } else if (resultCode == RESULT_FIRST_USER) {
                if (data != null) {
                    //blockingUserAllShouts
                    ArrayList<UserWallLayoutModelClass> DeletingObjectsArraylist = new ArrayList<>();
                    for (int i = 0; i < UserWallArrayliSt.size(); i++) {
                        if (UserWallArrayliSt.get(i) instanceof UserWallLayoutModelClass) {
                            if (((UserWallLayoutModelClass) UserWallArrayliSt.get(i)).getPostsUsername().equalsIgnoreCase(data.getStringExtra("blockedShoutsOfUsername"))) {
                                DeletingObjectsArraylist.add(((UserWallLayoutModelClass) UserWallArrayliSt.get(i)));
                            }
                        }
                    }
                    UserWallArrayliSt.removeAll(DeletingObjectsArraylist);
                    userwalladapter.notifyDataSetChanged();
                }
            } else {
            }
        } else if (requestCode == 1) {
            //uploading Image Shout
            if (resultCode == Activity.RESULT_OK) {
                Log.e("shuhu", "Fragment in else request code 1 and reslt ok");
                //              if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
                //Lower then Pie etc image shouting

                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
                Intent intent = new Intent("MyShoutImageData");
                intent.putExtra("MyUrlImage", "ShowLoader");
                localBroadcastManager.sendBroadcast(intent);

                if (data != null) {
                    Uri selectedImage = data.getData();
                    String filePath = compressImage(getPath(selectedImage));
                    //   String filePath = compressImage(data.getData());

                    Log.e("shuhu", "selectedImage >" + selectedImage);
                    Log.e("shuhu", "filePath >" + filePath);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                    String WallPostSenderName = prefs.getString("mine_user_name", null);
                    String Socket_ID = prefs.getString("sID", null);


                    List<MultipartBody.Part> Myparts = new ArrayList<>();
                    Myparts.add(prepareFilePart("image", filePath));
                    ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

                    Call<WallImagePostModelClass> MyCall = apiInterface.UserWallImageUploading(
                            Myparts,
                            createPartFromString("shout_image"),
                            createPartFromString(WallPostSenderName),
                            createPartFromString(Socket_ID)

                    );

                    MyCall.enqueue(new Callback<WallImagePostModelClass>() {
                        @Override
                        public void onResponse(Call<WallImagePostModelClass> call, Response<WallImagePostModelClass> response) {
                            Log.e("ShoutingImage", "Inresponse");
                            WallImagePostModelClass profileImage = response.body();
                            String WallPostReturnMessage = profileImage.getMessage();
                            String WallPostReturnURL = profileImage.getUrl();
                            int WallPostReturnSuccess = profileImage.getSuccess();
                            if (WallPostReturnSuccess == 1) {
                                Toast.makeText(getContext(), WallPostReturnMessage, Toast.LENGTH_SHORT).show();
                                //   UpoadedShoutImageView.setVisibility(View.VISIBLE);
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
                                Intent intent = new Intent("MyShoutImageData");
                                intent.putExtra("MyUrlImage", baseUrl + WallPostReturnURL);
                                localBroadcastManager.sendBroadcast(intent);
                                //  Picasso.get().load(baseUrl + WallPostReturnURL).into(ImageUploadingButtonForWall);
                                //  ImageUploadingButtonForWall.setVisibility(View.VISIBLE);
//                                    Intent intent =new Intent(getContext(),shoutingImageReceiver.class);
//                                    intent.putExtra("shoutedUrl","MyImageUrl");
//                                    getContext().sendBroadcast(intent);
                                ImageShoutURL = WallPostReturnURL;
                            }
                        }

                        @Override
                        public void onFailure(Call<WallImagePostModelClass> call, Throwable t) {
                            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
                            Intent intent = new Intent("MyShoutImageData");
                            intent.putExtra("MyUrlImage", "HideLoader");
                            localBroadcastManager.sendBroadcast(intent);
                            Toast.makeText(getContext(), "Fail to send image.", Toast.LENGTH_SHORT).show();
                            Log.e("shuhu", t.getMessage());
                        }
                    });
                }
            }
        } else if (requestCode == 12) {
            if (resultCode == RESULT_OK) {
                //load all data for new shout...
                Log.e("shohu", "here resltOk");
                loadingAllNewWallData();
            }
        }

    }

    private void settingValuesForCategrizedShouts(int shoutingCategryNumber) {
        Log.e("catgryShout", "settingCatefryShout");
        categryShoutsArrayList.clear();
        categryShoutsArrayList.add(new CategrizedShoutsModelClass("All"));
        categryShoutsArrayList.add(new CategrizedShoutsModelClass("Following"));
        categryShoutsArrayList.add(new CategrizedShoutsModelClass("My Shouts"));
        categryShoutsArrayList.add(new CategrizedShoutsModelClass("From User"));

        categrizedShoutsRecyclerView.setAdapter(new CategrizedShoutsAdapter(categryShoutsArrayList, getContext(), shoutingCategryNumber, this));
    }

    private void loadingAllNewWallData() {
        ShowingfromUserNameImage.setImageResource(R.drawable.round_filled_white);
        ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);
        ShowingOnlyFromMeImageview.setImageResource(R.drawable.round_filled_white);
        ShowingFromPublicImageView.setImageResource(R.drawable.tick_image);

        MaleValue = "0";
        FemaleValue = "0";
        SoulmateValue = "0";
        GameplayerValue = "0";
        FriendValue = "0";
        offset = 9;
        index = 9;
        RVScrolling = "no";
        ShoutingCategoriesShowing = "public";
        MySocketsClass.static_objects.shoutsCategryOpened="public";
        resetingTotalShoutValues();

        getAppTheme();
        if (mSocket != null) {
            ShowingLoader();
            GettingDataForUserWall(ShoutingCategoriesShowing);
            settingValuesForCategrizedShouts(0);
        }
        UserWallLayoutShowing();
    }

    public String getPath(Uri uri) {
        int column_index;
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);
        column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String fileUri) {

        File file = new File(fileUri);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    private void OpeningGalleryForImageShouting() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            PIE_OpeningGallery();
            Log.e("shuhu", "From pie open");
        } else {
            LowerThenPieOpeningGallery();
            Log.e("shuhu", "Home screen");
        }

    }

    private void PIE_OpeningGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, 1);
        Log.e("shuhu", "inStarting activity");

    }

    private void LowerThenPieOpeningGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, 1);
        Log.e("ImageShouting", "startingActivity");
    }

    private String compressImage(String imageUri) {
        Log.e("ComprsngLibrarytst", "Image Uri> " + imageUri);
        File file = new File(imageUri);
        String compresedImage = null;
        try {
            compresedImage = new Compressor(getContext()).compressToFile(file).getAbsolutePath();
            Log.e("ComprsngLibrarytst", "Resulted Uri> " + compresedImage);

        } catch (IOException e) {
            Log.e("ComprsngLibrarytst", "exceptn");
            e.printStackTrace();
        }
        return compresedImage;
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Socio/Shouts");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContext().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    @Override
    public void ShowingShoutsOfUsername(String UsernameName) {
        ShowingLoader();
        OnlyUserPostShowing();
        ShowingFromSpecialNameEditText.setText(UsernameName);
        EmittingForGettingCategrizedData(UsernameName);
    }

    @Override
    public void ShoutingMessage(String ShoutingMessage) {
        if (ShoutingMessage.equals("") && ImageShoutURL == null) {
            Toast.makeText(getContext(), "Write Something...", Toast.LENGTH_SHORT).show();
        } else {
            MyNewShoutPostSending(ShoutingMessage);
        }
    }

    @Override
    public void ImageShoutinMessage() {
        if (CheckPermissions()) {

            if (ActivityCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return;
            } else {

                OpeningGalleryForImageShouting();
            }
        } else {
            requestPermissions();
        }
    }

    @Override
    public void ShowingOnlyMyPosts() {
        OnlymineOwnPostShowing();
    }

    @Override
    public void ShowingFromPublic() {
        OnlyPublicPostShowing();
    }

    @Override
    public void ShowingFromFollowings() {
        OnlyFollowingPostShowing();
    }

    @Override
    public void ShowingFromdSpecialName() {
        if (ShowingSpecifiFromUserLayout.getVisibility() == View.VISIBLE) {
            ShowingSpecifiFromUserLayout.setVisibility(View.GONE);
        } else {
            ShoutingCategoriesShowing = "user";
            MySocketsClass.static_objects.shoutsCategryOpened="user";
            TotaloffsetValueForShouts = 0;
            index = 9;
            offset = 9;
            visiblingShowingSpecifiFromUserLayout();
        }
    }

    @Override
    public void uploadedShoutImageDeletion() {
        DeletingSelectedShoutImageMethod();
    }

    @Override
    public void requestingPermissions() {
        requestPermissions();
    }

    private void hidingShowingSpecifiFromUserLayout() {
        if (ShowingSpecifiFromUserLayout.getVisibility() == View.VISIBLE) {
            ShowingSpecifiFromUserLayout.setVisibility(View.GONE);
        }
    }

    private void visiblingShowingSpecifiFromUserLayout() {
        if (ShowingSpecifiFromUserLayout.getVisibility() == View.GONE) {
            ShowingSpecifiFromUserLayout.setVisibility(View.VISIBLE);
        }
    }

    private void reSetingFriendSuggestionsValues() {
        MaleValue = "0";
        FemaleValue = "0";
        SoulmateValue = "0";
        GameplayerValue = "0";
        FriendValue = "0";
        MaleSearchImage.setBackgroundResource(R.drawable.male_unselected);
        SoulmateSearchImage.setBackgroundResource(R.drawable.heart_unselected);
        FriendSearchImage.setBackgroundResource(R.drawable.friendimage_unselected);
        GamePlayerSearchImage.setBackgroundResource(R.drawable.gameplayer_unselected);
        FemaleSearchImage.setImageResource(R.drawable.female_unselected);
    }

    private boolean CheckPermissions() {
        int writeStoragePermissionResult = ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE);
        int readStoragePermissionResult = ContextCompat.checkSelfPermission(getContext(), READ_EXTERNAL_STORAGE);
        int micPermissionResult = ContextCompat.checkSelfPermission(getContext(), RECORD_AUDIO);
        return writeStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                readStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                micPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                WRITE_EXTERNAL_STORAGE,
                READ_EXTERNAL_STORAGE,
                RECORD_AUDIO,


        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                    boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (write_str && read_str && record_str) {
                        Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    private void categriesViewShowingMethod() {
        if (CategriesView.getVisibility() == View.VISIBLE) {
            CategriesView.setVisibility(View.GONE);
        } else {
            CategriesView.setVisibility(View.VISIBLE);
        }
    }

    private void loadNativeAds() {

        if (getContext() != null) {
            Log.e("testingNativeAds", "loading Native Ads method");
            AdLoader.Builder builder = new AdLoader.Builder(getContext(), getString(R.string.test_native_ad_unit_id));
            adLoader = builder.forUnifiedNativeAd(
                    new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                        @Override
                        public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                            // A native ad loaded successfully, check if the ad loader has finished loading
                            // and if so, insert the ads into the list.
                            mNativeAdsList.add(unifiedNativeAd);
                            if (!adLoader.isLoading()) {
                                Log.e("testingNativeAds", "InsertingAds in menu item");
                                insertAdsInMenuItems();
                            }
                        }
                    }).withAdListener(
                    new AdListener() {
                        @Override
                        public void onAdFailedToLoad(int errorCode) {
                            // A native ad failed to load, check if the ad loader has finished loading
                            // and if so, insert the ads into the list.
                            Log.e("testingNativeAds", "The previous native ad failed to load. Attempting to"
                                    + " load another.");
                            if (!adLoader.isLoading()) {
                                insertAdsInMenuItems();
                            }
                        }
                    }).build();

            // Load the Native ads.
            adLoader.loadAds(new AdRequest.Builder().build(), NUMBER_OF_ADS);
        }
    }

    private void insertAdsInMenuItems() {
        Log.e("testingNativeAds", "In inserting Ads In Menu Items method");
        if (mNativeAdsList.size() <= 0) {
            return;
        }
        Log.e("testingNativeAds", "walls Array size > " + UserWallArrayliSt.size() +
                "  mNative Ads size> " + mNativeAdsList.size() + "  offset>  " + offset);
        for (UnifiedNativeAd ad : mNativeAdsList) {
            Log.e("testingNativeAds", "ads come to add in arraylist on index " + index);
            if (index < UserWallArrayliSt.size()) {
                UserWallArrayliSt.add(index, ad);
                index = index + offset;
                Log.e("testingNativeAds", "added and index now> " + index);
            }
        }
    }

    private void loadingAllTrendingShouts() {
        mSocket.off(MySocketsClass.ListenersClass.LOAD_TRENDING_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.LOAD_TRENDING);
        mSocket.on(MySocketsClass.ListenersClass.LOAD_TRENDING_RESPONSE, load_trending_response);
    }

    private void showingNoShoutsToDisplyLayout(){
        if (noShoutsToDispleLayout.getVisibility()==View.GONE){
            noShoutsToDispleLayout.setVisibility(View.VISIBLE);
        }
    }

    private void hidingNoShoutsToDisplyLayout(){
        if (noShoutsToDispleLayout.getVisibility()==View.VISIBLE){
            noShoutsToDispleLayout.setVisibility(View.GONE);
        }
    }

    private void hidingShoutsLayout(){
        if (UsersWallRecyclerview.getVisibility()==View.VISIBLE){
            UsersWallRecyclerview.setVisibility(View.GONE);
        }
    }

    private void showingShoutsLayout(){
        if (UsersWallRecyclerview.getVisibility()==View.GONE){
            UsersWallRecyclerview.setVisibility(View.VISIBLE);
        }
    }

    Emitter.Listener load_friend_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            FriendSuggestionArraylist.clear();
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.e("FrendSugst", "InListener");
                            //   String country=null;
                            TotalValues = Integer.parseInt(jsonObject.getString("total"));
                            String TotalLimit = jsonObject.getString("limit");
                            String TotalOffset = jsonObject.getString("offset");

                            for (int i = 0; i < jsonObject.getJSONArray("friends").length(); i++) {
                                String FriendSuggestionUsername = jsonObject.getJSONArray("friends").getJSONObject(i).getString("username");
                                String ReqStatus = jsonObject.getJSONArray("friends").getJSONObject(i).getString("reqStatus");
                                String age = jsonObject.getJSONArray("friends").getJSONObject(i).getString("age");
                                String dp = jsonObject.getJSONArray("friends").getJSONObject(i).getString("dp");
                                String role = jsonObject.getJSONArray("friends").getJSONObject(i).getString("role");


                                //  if (jsonObject.getJSONArray("friends").getJSONObject(i).getJSONObject("country").has("emoji")) {
                                String country = jsonObject.getJSONArray("friends").getJSONObject(i).getJSONObject("country").getString("unicode");
//                            Log.e("Countriee",country.split("U\\+")[1].toLowerCase());
//                            Log.e("Countriee",country.split("U\\+")[2].toLowerCase());
                                String MainCountryURL = null;
                                if (country.equals("")) {
                                    MainCountryURL = "";
                                } else {
                                    MainCountryURL = country.split("U\\+")[1].toLowerCase() + country.split("U\\+")[2].toLowerCase();
                                }
                                //   }

                                String add_requests = jsonObject.getJSONArray("friends").getJSONObject(i).getString("add_requests");
                                String gender = jsonObject.getJSONArray("friends").getJSONObject(i).getString("gender");
                                //   String FullName = jsonObject.getJSONArray("friends").getJSONObject(i).getString("name");
//                            if (country==null)
//                            {
                                if (MainCountryURL.equals("")) {
                                    FriendSuggestionArraylist.add(new MakeNewFriendsModelClass(FriendSuggestionUsername, ReqStatus, age, dp, role, "", add_requests, gender, ""));
                                } else {
                                    FriendSuggestionArraylist.add(new MakeNewFriendsModelClass(FriendSuggestionUsername, ReqStatus, age, dp, role, MainCountryURL.replace(" ", "-"), add_requests, gender, ""));
                                }
                            }
                            suggestionsRowAdapter.notifyDataSetChanged();
                            HidingLoader();
                            friendSuggestionRefresh.setRefreshing(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("FrendSugst", "Exceptn");
                        }
                        HidingLoader();
                    }
                });

            }
        }
    };

    Emitter.Listener FilteredLoadFriendsResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TotalValues = Integer.parseInt(jsonObject.getString("total"));
                            String Soulmate = jsonObject.getString("love");
                            String player = jsonObject.getString("player");
                            String friend = jsonObject.getString("friends");
                            String male = jsonObject.getString("male");
                            String female = jsonObject.getString("female");
                            String ShownDataValues = jsonObject.getString("shown");
                            OffsetValue = Integer.parseInt(jsonObject.getString("offset"));

                            for (int i = 0; i < jsonObject.getJSONArray("friends").length(); i++) {
                                String FriendSuggestionUsername = jsonObject.getJSONArray("friends").getJSONObject(i).getString("username");
                                String ReqStatus = jsonObject.getJSONArray("friends").getJSONObject(i).getString("reqStatus");
                                String age = jsonObject.getJSONArray("friends").getJSONObject(i).getString("age");
                                String dp = jsonObject.getJSONArray("friends").getJSONObject(i).getString("dp");
                                String role = jsonObject.getJSONArray("friends").getJSONObject(i).getString("role");
                                String country = jsonObject.getJSONArray("friends").getJSONObject(i).getJSONObject("country").getString("unicode");
                                String MainCountryURL = null;
                                Log.e("mach", country);
                                if (country.equals("")) {
                                    MainCountryURL = "";
                                } else {
                                    MainCountryURL = country.split("U\\+")[1].toLowerCase() + country.split("U\\+")[2].toLowerCase();
                                }
                                String add_requests = jsonObject.getJSONArray("friends").getJSONObject(i).getString("add_requests");
                                String gender = jsonObject.getJSONArray("friends").getJSONObject(i).getString("gender");
                                String FullName = jsonObject.getJSONArray("friends").getJSONObject(i).getString("name");
                                FriendSuggestionArraylist.add(new MakeNewFriendsModelClass(FriendSuggestionUsername, ReqStatus, age, dp, role, MainCountryURL.replace(" ", "-"), add_requests, gender, FullName));
                            }
                            suggestionsRowAdapter.notifyDataSetChanged();
                            RVScrolling = "yes";
                            UsersWallRecyclerview.scrollToPosition(0);
                            HidingLoader();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener load_posts_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            UserWallArrayliSt.clear();
                            mNativeAdsList.clear();
                            TotalShoutsValue = Integer.parseInt(jsonObject.getString("total"));
                            TotaloffsetValueForShouts = Integer.parseInt(jsonObject.getString("offset"));
                            TotalshownValueForShouts = Integer.parseInt(jsonObject.getString("shown"));
                            String type = jsonObject.getString("type");


                            if (TotalShoutsValue > 0) {
                                // agr posts hyn tb adapter ko values bhjy
                                hidingNoShoutsToDisplyLayout();
                                showingShoutsLayout();
                                shoutsSwipeRefresh.setEnabled(true);
                                shoutsSwipeRefresh.setRefreshing(false);
                                for (int i = 0; i < jsonObject.getJSONArray("posts").length(); i++) {
                                    String PostsId = jsonObject.getJSONArray("posts").getJSONObject(i).getString("id");
                                    String PostsUsername = jsonObject.getJSONArray("posts").getJSONObject(i).getString("username");
                                    String PostsSimpleName = jsonObject.getJSONArray("posts").getJSONObject(i).getString("name");
                                    String PostUserAge = jsonObject.getJSONArray("posts").getJSONObject(i).getString("age");
                                    String PostsUserRole = jsonObject.getJSONArray("posts").getJSONObject(i).getString("role");
                                    String PostsUserDp = jsonObject.getJSONArray("posts").getJSONObject(i).getString("dp");
                                    String PostsUserText = jsonObject.getJSONArray("posts").getJSONObject(i).getString("text");
                                    //  String PostsDate = jsonObject.getJSONArray("posts").getJSONObject(i).getString("date");
                                    String PostsTime = jsonObject.getJSONArray("posts").getJSONObject(i).getString("time");
                                    String Postprivacy = jsonObject.getJSONArray("posts").getJSONObject(i).getString("privacy");
                                    String PostsTotallikes = jsonObject.getJSONArray("posts").getJSONObject(i).getString("likes");
                                    String PostsTotaldislikes = jsonObject.getJSONArray("posts").getJSONObject(i).getString("dislikes");
                                    String PostsTotalcomments = jsonObject.getJSONArray("posts").getJSONObject(i).getString("comments");
                                    String PostsTotalreshouts = jsonObject.getJSONArray("posts").getJSONObject(i).getString("reshouts");
                                    String gender = jsonObject.getJSONArray("posts").getJSONObject(i).getString("gender");
                                    String Postsfollow = jsonObject.getJSONArray("posts").getJSONObject(i).getString("follow");
                                    String MyLikeOnPost = jsonObject.getJSONArray("posts").getJSONObject(i).getString("like");
                                    String MyDisLikeOnPost = jsonObject.getJSONArray("posts").getJSONObject(i).getString("dislike");

                                    UserWallArrayliSt.add(new UserWallLayoutModelClass(PostsUserDp, PostsSimpleName, PostsUsername, PostsUserRole,
                                            PostUserAge, gender, PostsTime, PostsUserText, PostsTotallikes, PostsTotaldislikes, MyLikeOnPost, MyDisLikeOnPost,
                                            PostsTotalcomments, PostsTotalreshouts, Postsfollow, PostsId));
                                }
                                loadNativeAds();
                                userwalladapter.notifyDataSetChanged();
                                HidingLoader();
                                UsersWallRecyclerview.suppressLayout(false);
                                UsersWallRecyclerview.scrollToPosition(0);
                            }
                            else {
                                shoutsSwipeRefresh.setEnabled(false);
                                hidingShoutsLayout();
                                showingNoShoutsToDisplyLayout();
                                HidingLoader();
                                UsersWallRecyclerview.suppressLayout(false);

                            }
                            hidingShowingSpecifiFromUserLayout();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener loadMore_posts_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // UserWallArraylist.clear();
                            TotalShoutsValue = Integer.parseInt(jsonObject.getString("total"));
                            TotaloffsetValueForShouts = Integer.parseInt(jsonObject.getString("offset"));
                            TotalshownValueForShouts = Integer.parseInt(jsonObject.getString("shown"));
                            String type = jsonObject.getString("type");
                            for (int i = 0; i < jsonObject.getJSONArray("posts").length(); i++) {
                                String PostsId = jsonObject.getJSONArray("posts").getJSONObject(i).getString("id");
                                String PostsUsername = jsonObject.getJSONArray("posts").getJSONObject(i).getString("username");
                                String PostsSimpleName = jsonObject.getJSONArray("posts").getJSONObject(i).getString("name");
                                String PostUserAge = jsonObject.getJSONArray("posts").getJSONObject(i).getString("age");
                                String PostsUserRole = jsonObject.getJSONArray("posts").getJSONObject(i).getString("role");
                                String PostsUserDp = jsonObject.getJSONArray("posts").getJSONObject(i).getString("dp");
                                String PostsUserText = jsonObject.getJSONArray("posts").getJSONObject(i).getString("text");
                                //  String PostsDate = jsonObject.getJSONArray("posts").getJSONObject(i).getString("date");
                                String PostsTime = jsonObject.getJSONArray("posts").getJSONObject(i).getString("time");
                                String Postprivacy = jsonObject.getJSONArray("posts").getJSONObject(i).getString("privacy");
                                String PostsTotallikes = jsonObject.getJSONArray("posts").getJSONObject(i).getString("likes");
                                String PostsTotaldislikes = jsonObject.getJSONArray("posts").getJSONObject(i).getString("dislikes");
                                String PostsTotalcomments = jsonObject.getJSONArray("posts").getJSONObject(i).getString("comments");
                                String PostsTotalreshouts = jsonObject.getJSONArray("posts").getJSONObject(i).getString("reshouts");
                                String gender = jsonObject.getJSONArray("posts").getJSONObject(i).getString("gender");
                                String Postsfollow = jsonObject.getJSONArray("posts").getJSONObject(i).getString("follow");
                                String MyLikeOnPost = jsonObject.getJSONArray("posts").getJSONObject(i).getString("like");
                                String MyDisLikeOnPost = jsonObject.getJSONArray("posts").getJSONObject(i).getString("dislike");

                                Log.e("WallingTestng", PostsUserText);


                                Log.e("Likes_dislikes", "UserTextFromListener->" + PostsUserText + "Likes->" + PostsTotallikes
                                        + " Dislike->" + PostsTotaldislikes +
                                        " Comments->" + PostsTotalcomments);


                                UserWallArrayliSt.add(new UserWallLayoutModelClass(PostsUserDp, PostsSimpleName, PostsUsername, PostsUserRole,
                                        PostUserAge, gender, PostsTime, PostsUserText, PostsTotallikes, PostsTotaldislikes, MyLikeOnPost, MyDisLikeOnPost,
                                        PostsTotalcomments, PostsTotalreshouts, Postsfollow, PostsId));
                            }
                            loadNativeAds();
                            userwalladapter.notifyDataSetChanged();

                            HidingLoader();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    Emitter.Listener add_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject jsonObject = (JSONObject) args[0];
                        try {
                            String message = jsonObject.getString("message");
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                                TotaloffsetValueForShouts = 0;
                                GettingDataForUserWall(ShoutingCategoriesShowing);
                                ImageShoutURL = null;
                                UpoadedShoutImageView.setVisibility(View.GONE);

                            } else {
                                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                                ImageShoutURL = null;
                                UpoadedShoutImageView.setVisibility(View.GONE);
                                //   WallShoutText="";
                            }
                            WallPostDescription.setText("");
                            ShowingShoutView();
                            //     HidingLoader();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    Emitter.Listener DeleteShoutImageResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.e("DeletngSH", "InListener00");

            JSONObject jsonObject = (JSONObject) args[0];
            try {
                String success = jsonObject.getString("success");
                if (success.equals("1")) {
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ImageShoutURL = null;
                                UpoadedShoutImageView.setVisibility(View.GONE);
                            }
                        });

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener update_shout_details = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            JSONObject jsonObject = (JSONObject) args[0];
                            String Updated_shoutId = jsonObject.getString("shoutID");
                            String Updated_shoutType = jsonObject.getString("type");
                            String Updated_shoutValue = jsonObject.getString("value");
                            for (int i = 0; i < UserWallArrayliSt.size(); i++) {
                                // UserWallLayoutModelClass userWallLayoutModelClass=(UserWallLayoutModelClass) UserWallArraylist.get(i);

                                if (UserWallArrayliSt.get(i) instanceof UserWallLayoutModelClass) {

                                    if (Updated_shoutId.equals(((UserWallLayoutModelClass) UserWallArrayliSt.get(i)).getPostsId())) {
                                        if (Updated_shoutType.equals("total_likes")) {
                                            ((UserWallLayoutModelClass) UserWallArrayliSt.get(i)).setPostsTotallikes(Updated_shoutValue);
                                            userwalladapter.notifyDataSetChanged();
                                        } else if (Updated_shoutType.equals("total_comment")) {
                                            ((UserWallLayoutModelClass) UserWallArrayliSt.get(i)).setPostsTotalcomments(Updated_shoutValue);
                                            userwalladapter.notifyDataSetChanged();
                                        } else if (Updated_shoutType.equals("total_dislikes")) {
                                            ((UserWallLayoutModelClass) UserWallArrayliSt.get(i)).setPostsTotaldislikes(Updated_shoutValue);
                                            userwalladapter.notifyDataSetChanged();
                                        }
                                    }

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    Emitter.Listener load_trending_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    trendingShoutsArraylist.clear();
                    JSONObject jsonObject = (JSONObject) args[0];
                    Log.e("Trending", jsonObject.toString());
                    try {
                        for (int i = 0; i < jsonObject.getJSONArray("trending").length(); i++) {
                            trendingShoutsArraylist.add(new TrendingShoutsModelClass(
                                    jsonObject.getJSONArray("trending").getJSONObject(i).getString("id"),
                                    jsonObject.getJSONArray("trending").getJSONObject(i).getString("username"),
                                    jsonObject.getJSONArray("trending").getJSONObject(i).getString("dp"),
                                    jsonObject.getJSONArray("trending").getJSONObject(i).getString("date"),
                                    jsonObject.getJSONArray("trending").getJSONObject(i).getString("likes"),
                                    jsonObject.getJSONArray("trending").getJSONObject(i).getString("comments")));
                            trendingShoutsAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

}
