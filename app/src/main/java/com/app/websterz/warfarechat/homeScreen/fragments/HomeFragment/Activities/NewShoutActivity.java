package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.schwaab.avvylib.AvatarView;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.Rest.ApiClient;
import com.app.websterz.warfarechat.Rest.ApiInterface;
import com.app.websterz.warfarechat.TestingPackage.NewPrivateChat;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.CommentsRepliesAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.NewShoutSuggestionsAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.NewShoutSuggestedInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.NewSuggestedShoutModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.MultiPartsClassesForWallImagePost.WallImagePostModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ScrollingClass.MediaLoader;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chibde.visualizer.CircleBarVisualizer;
import com.chibde.visualizer.LineVisualizer;
import com.github.nkzawa.emitter.Emitter;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;
import com.yanzhenjie.album.AlbumFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class NewShoutActivity extends AppCompatActivity implements NewShoutSuggestedInterface {

    CircleImageView users_dp;
    RelativeLayout MainParentLayout, UpoadedShoutImageView, audioViewCardview, newSuggestionShoutLayout,
            voiceShoutButton, ImageShoutButton, WallGifSelectButton;
    LinearLayout loader_imageview_Layout;
    Button shoutButton;
    EmojiconEditText newShoutEditText;
    ImageView ImageUploadingButtonForWall, DeleteSelectedShoutImage, DeleteAudiShout, ShowingSmileys, loaderGif, audioPlayImageView;
    String ImageShoutURL = null, audioShoutUrl = null;
    MySingleTon mySingleTon;
    EmojIconActions emojIcon;
    AvatarView avatarLoader;
    private MediaRecorder mediaRecorder;
    CircleBarVisualizer visualizer;
    MediaPlayer mediaPlayer;
    private Handler handler = new Handler();
    private TextView textCurrentTime, textTotalDuration;
    RecyclerView newSuggestedShoutsRecyclerView;
    LinearLayoutManager myLinearLayout;
    NewShoutSuggestionsAdapter newShoutSuggestionsAdapter;
    ArrayList<NewSuggestedShoutModelClass> newShoutSuggestedArrayalist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_shout);

        initialization();
        myCode();

        shoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newShoutingMethod();
            }
        });
        WallGifSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageShoutinMessage();
            }
        });
        DeleteSelectedShoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeletingSelectedShoutImageMethod();
            }
        });
        DeleteAudiShout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletingAudioMethod();
            }
        });
        ImageShoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageShoutinMessage();
            }
        });
        audioPlayImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playingAudioFromUrl();
            }
        });
        voiceShoutButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if (CheckPermissions()) {

                    if (ActivityCompat.checkSelfPermission(NewShoutActivity.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        return false;
                    } else {

                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:

                                voiceShoutButton.setBackgroundResource(R.drawable.circle_new_blue);
                                startRecording();
                                return true;

                            case MotionEvent.ACTION_UP:
                                voiceShoutButton.setBackgroundResource(R.drawable.circle_new_green);
                                stopRecordingAndSendingEmit();
                                return true;
                        }
                        return false;
                    }
                } else {
                    requestPermissions();
                }
                return false;
            }
        });

    }

    private void initialization() {
        mySingleTon = MySingleTon.getInstance();
        MainParentLayout = findViewById(R.id.MainParentLayout);
        users_dp = findViewById(R.id.users_dp);
        shoutButton = findViewById(R.id.shoutButton);
        newShoutEditText = findViewById(R.id.newShoutEditText);
        ShowingSmileys = findViewById(R.id.ShowingSmileys);
        ImageShoutButton = findViewById(R.id.ImageShoutButton);
        voiceShoutButton = findViewById(R.id.voiceShoutButton);
        newSuggestionShoutLayout = findViewById(R.id.newSuggestionShoutLayout);
        newSuggestedShoutsRecyclerView = findViewById(R.id.newSuggestedShoutsRecyclerView);
        WallGifSelectButton = findViewById(R.id.WallGifSelectButton);
        UpoadedShoutImageView = findViewById(R.id.UpoadedShoutImageView);
        visualizer = findViewById(R.id.visualizer);
        audioPlayImageView = findViewById(R.id.audioPlayImageView);
        avatarLoader = findViewById(R.id.ProfileavatarLoader);
        ImageUploadingButtonForWall = findViewById(R.id.ImageUploadingButtonForWall);
        DeleteSelectedShoutImage = findViewById(R.id.DeleteSelectedShoutImage);
        DeleteAudiShout = findViewById(R.id.DeleteAudiShout);
        audioViewCardview = findViewById(R.id.audioViewCardview);
        loaderGif = findViewById(R.id.loaderGif);
        textCurrentTime = findViewById(R.id.textCurrentTime);
        textTotalDuration = findViewById(R.id.textTotalDuration);
        Glide.with(NewShoutActivity.this).load(R.drawable.socio_loader).into(loaderGif);
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        emojIcon = new EmojIconActions(this, viewGroup, newShoutEditText, ShowingSmileys);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        mediaPlayer = new MediaPlayer();
        newShoutSuggestedArrayalist = new ArrayList<>();
        myLinearLayout = new LinearLayoutManager(NewShoutActivity.this, RecyclerView.VERTICAL, false);
        newSuggestedShoutsRecyclerView.setLayoutManager(myLinearLayout);
        newShoutSuggestionsAdapter = new NewShoutSuggestionsAdapter(newShoutSuggestedArrayalist, NewShoutActivity.this, this);
        newSuggestedShoutsRecyclerView.setAdapter(newShoutSuggestionsAdapter);
    }

    private void myCode() {
        avatarLoader.setVisibility(View.VISIBLE);
        avatarLoader.setAnimating(true);
        Glide.with(NewShoutActivity.this).load(baseUrl + mySingleTon.dp).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                avatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).into(users_dp);
        puttingValuesForSuggestionsShouts();
    }

    private void hideLoader() {
        if (loader_imageview_Layout != null && loader_imageview_Layout.getVisibility() == View.VISIBLE) {
            loader_imageview_Layout.setVisibility(View.GONE);
        }
    }

    private void showLoader() {
        loader_imageview_Layout = MySocketsClass.custom_layout.Loader_image(NewShoutActivity.this, MainParentLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = NewShoutActivity.this;
        MySocketsClass.static_objects.global_layout = MainParentLayout;
    }

    private void newShoutingMethod() {
        if (newShoutEditText.getText().toString().equals("") && ImageShoutURL == null && audioShoutUrl == null) {
            Toast.makeText(NewShoutActivity.this, "Write Something...", Toast.LENGTH_SHORT).show();
        } else {
            showLoader();
            JSONObject jsonObject = new JSONObject();
            try {
                if (ImageShoutURL == null && audioShoutUrl == null) {
                    jsonObject.put("text", newShoutEditText.getText().toString());
                } else if (ImageShoutURL != null && audioShoutUrl == null) {
                    jsonObject.put("text", newShoutEditText.getText().toString() + " " + "[[" + ImageShoutURL + "]]");
                } else if (ImageShoutURL == null && audioShoutUrl != null) {
                    jsonObject.put("text", newShoutEditText.getText().toString() + " " + "{{" + audioShoutUrl + "}}");
                }
                mSocket.off(MySocketsClass.ListenersClass.ADD_SHOUT_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.ADD_SHOUT, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.ADD_SHOUT_RESPONSE, add_shout_response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            hideKeyboard(NewShoutActivity.this);
        }
    }

    private void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void ImageShoutinMessage() {
        if (CheckPermissions()) {

            if (ActivityCompat.checkSelfPermission(NewShoutActivity.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return;
            } else {

                openGallery();
            }
        } else {
            requestPermissions();
        }
    }

    private boolean CheckPermissions() {
        int writeStoragePermissionResult = ContextCompat.checkSelfPermission(NewShoutActivity.this, WRITE_EXTERNAL_STORAGE);
        int readStoragePermissionResult = ContextCompat.checkSelfPermission(NewShoutActivity.this, READ_EXTERNAL_STORAGE);
        int micPermissionResult = ContextCompat.checkSelfPermission(NewShoutActivity.this, RECORD_AUDIO);
        return writeStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                readStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                micPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(NewShoutActivity.this, new String[]{
                WRITE_EXTERNAL_STORAGE,
                READ_EXTERNAL_STORAGE,
                RECORD_AUDIO,


        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                    boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (write_str && read_str && record_str) {
                        Toast.makeText(NewShoutActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NewShoutActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }
//    private void takePhotoFromCamera() {
///*StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//StrictMode.setVmPolicy(builder.build());
//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
//String imageFileName = timeStamp + ".jpg";
//File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
//File file = new File(pictureImagePath);
//Uri outputFileUri = Uri.fromFile(file);
//Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
//startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);*/
//
//        Album.camera(this)
//                .image()
//                .onResult(result ->
//
//                        new Compressor(this)
//                                .compressToFileAsFlowable(new File(result), "img_" + System.currentTimeMillis() + ".jpg")
//                                .subscribeOn(Schedulers.io())
//                                .observeOn(AndroidSchedulers.mainThread())
//                                .subscribe(
//                                        file -> {
//
//                                            if (file != null) {
//                                                Constants.log(this, "image compressed path " + file.getPath());
//                                                uploadImage(file.getPath());
//                                            }
//                                            else {
//                                                Constants.log(this, "image compressed failed " + result);
//                                                uploadImage(result);
//                                            }
//
//                                        },
//                                        throwable -> {
//                                            hideProgress(ServerEnums.upload_profile_image);
//                                            displayError(throwable.getMessage());
//                                        }
//
//                                ))
//                .start();
//
//    }

    private void openGallery() {

        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .build());

        Album.image(this)
                .singleChoice()
                .camera(false)
                .columnCount(3)
                .onResult(result -> {
                    if (result.size() > 0) {
                        showLoader();
                        Log.e("selector", result.toString());
                        String selectedImage = result.get(0).getPath();
                        String filePath;
//                    if (getPath(selectedImage).contains(".gif")) {
//                        filePath = getPath(selectedImage);
//                    } else {
//                        filePath = compressImage(getPath(selectedImage));
//                    }
                        if (selectedImage.contains(".gif")) {
                            filePath = selectedImage;
                        } else {
                            filePath = compressImage(selectedImage);
                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NewShoutActivity.this);
                        String WallPostSenderName = prefs.getString("mine_user_name", null);
                        String Socket_ID = prefs.getString("sID", null);


                        List<MultipartBody.Part> Myparts = new ArrayList<>();
                        Myparts.add(prepareFilePart("image", filePath));
                        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

                        Call<WallImagePostModelClass> MyCall = apiInterface.UserWallImageUploading(
                                Myparts,
                                createPartFromString("shout_image"),
                                createPartFromString(WallPostSenderName),
                                createPartFromString(Socket_ID)
                        );

                        MyCall.enqueue(new Callback<WallImagePostModelClass>() {
                            @Override
                            public void onResponse(Call<WallImagePostModelClass> call, Response<WallImagePostModelClass> response) {
                                WallImagePostModelClass profileImage = response.body();
                                String WallPostReturnMessage = profileImage.getMessage();
                                String WallPostReturnURL = profileImage.getUrl();
                                int WallPostReturnSuccess = profileImage.getSuccess();
                                if (WallPostReturnSuccess == 1) {
                                    hidenewShoutSugestionLayout();
                                    hidingAudioLayout();
                                    makingMediaPlayerNullAndStopping();
                                    UpoadedShoutImageView.setVisibility(View.VISIBLE);
                                    Toast.makeText(NewShoutActivity.this, WallPostReturnMessage, Toast.LENGTH_SHORT).show();
                                    Log.e("teydwg", WallPostReturnMessage);
                                    hideLoader();
                                    Glide.with(NewShoutActivity.this).load(baseUrl + WallPostReturnURL).into(ImageUploadingButtonForWall);
                                    ImageShoutURL = WallPostReturnURL;
                                }
                            }

                            @Override
                            public void onFailure(Call<WallImagePostModelClass> call, Throwable t) {
                                UpoadedShoutImageView.setVisibility(View.GONE);
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(NewShoutActivity.this);
                                Intent intent = new Intent("MyShoutImageData");
                                intent.putExtra("MyUrlImage", "HideLoader");
                                localBroadcastManager.sendBroadcast(intent);
                                Toast.makeText(NewShoutActivity.this, "Fail to send image.", Toast.LENGTH_SHORT).show();
                                Log.e("shuhu", t.getMessage());
                            }
                        });
                    }

                })
                .start();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    showLoader();
                    Uri selectedImage = data.getData();
                    String filePath;
                    if (getPath(selectedImage).contains(".gif")) {
                        filePath = getPath(selectedImage);
                    } else {
                        filePath = compressImage(getPath(selectedImage));
                    }
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NewShoutActivity.this);
                    String WallPostSenderName = prefs.getString("mine_user_name", null);
                    String Socket_ID = prefs.getString("sID", null);


                    List<MultipartBody.Part> Myparts = new ArrayList<>();
                    Myparts.add(prepareFilePart("image", filePath));
                    ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

                    Call<WallImagePostModelClass> MyCall = apiInterface.UserWallImageUploading(
                            Myparts,
                            createPartFromString("shout_image"),
                            createPartFromString(WallPostSenderName),
                            createPartFromString(Socket_ID)
                    );

                    MyCall.enqueue(new Callback<WallImagePostModelClass>() {
                        @Override
                        public void onResponse(Call<WallImagePostModelClass> call, Response<WallImagePostModelClass> response) {
                            WallImagePostModelClass profileImage = response.body();
                            String WallPostReturnMessage = profileImage.getMessage();
                            String WallPostReturnURL = profileImage.getUrl();
                            int WallPostReturnSuccess = profileImage.getSuccess();
                            if (WallPostReturnSuccess == 1) {
                                hidenewShoutSugestionLayout();
                                hidingAudioLayout();
                                makingMediaPlayerNullAndStopping();
                                UpoadedShoutImageView.setVisibility(View.VISIBLE);
                                Toast.makeText(NewShoutActivity.this, WallPostReturnMessage, Toast.LENGTH_SHORT).show();
                                Log.e("teydwg", WallPostReturnMessage);
                                hideLoader();
                                Glide.with(NewShoutActivity.this).load(baseUrl + WallPostReturnURL).into(ImageUploadingButtonForWall);
                                ImageShoutURL = WallPostReturnURL;
                            }
                        }

                        @Override
                        public void onFailure(Call<WallImagePostModelClass> call, Throwable t) {
                            UpoadedShoutImageView.setVisibility(View.GONE);
                            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(NewShoutActivity.this);
                            Intent intent = new Intent("MyShoutImageData");
                            intent.putExtra("MyUrlImage", "HideLoader");
                            localBroadcastManager.sendBroadcast(intent);
                            Toast.makeText(NewShoutActivity.this, "Fail to send image.", Toast.LENGTH_SHORT).show();
                            Log.e("shuhu", t.getMessage());
                        }
                    });
                }
            }
        }
    }

    private void DeletingSelectedShoutImageMethod() {
        if (ImageShoutURL != null) {
            Toast.makeText(NewShoutActivity.this, "DeleteClicked", Toast.LENGTH_SHORT).show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("url", ImageShoutURL);
                Log.e("Imagee", ImageShoutURL);
                mSocket.off(MySocketsClass.ListenersClass.DELETE_SHOUT_IMAGE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.DELETE_SHOUT_IMAGE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.DELETE_SHOUT_IMAGE_RESPONSE, DeleteShoutImageResponse);
                Log.e("DeletngSH", "Inemiter");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void deletingAudioMethod() {
        if (audioShoutUrl != null) {
            Toast.makeText(NewShoutActivity.this, "DeleteClicked", Toast.LENGTH_SHORT).show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("url", audioShoutUrl);
                Log.e("Imagee", audioShoutUrl);
                mSocket.off(MySocketsClass.ListenersClass.DELETE_SHOUT_Audio_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.DELETE_SHOUT_Audio, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.DELETE_SHOUT_Audio_RESPONSE, DeleteShoutAudioResponse);
                Log.e("DeletngSH", "Inemiter");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String compressImage(String imageUri) {
        Log.e("ComprsngLibrarytst", "Image Uri> " + imageUri);
        File file = new File(imageUri);
        String compresedImage = null;
        try {
            compresedImage = new Compressor(NewShoutActivity.this).compressToFile(file).getAbsolutePath();
            Log.e("ComprsngLibrarytst", "Resulted Uri> " + compresedImage);

        } catch (IOException e) {
            Log.e("ComprsngLibrarytst", "exceptn");
            e.printStackTrace();
        }
        return compresedImage;
    }

    private String getPath(Uri uri) {
        int column_index;
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = NewShoutActivity.this.getContentResolver().query(uri, projection, null, null, null);
        column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    private void hidingAudioLayout() {
        if (audioViewCardview.getVisibility() == View.VISIBLE) {
            audioViewCardview.setVisibility(View.GONE);
        }
    }

    private void showingAudioLayout() {
        if (audioViewCardview.getVisibility() == View.GONE) {
            audioViewCardview.setVisibility(View.VISIBLE);
        }
    }

    private void playingAudioFromUrl() {
        if (mediaPlayer.isPlaying()) {
            handler.removeCallbacks(updater);
            mediaPlayer.pause();
            Glide.with(NewShoutActivity.this).load(R.drawable.shout_play).into(audioPlayImageView);
        } else {
            mediaPlayer.start();
            Glide.with(NewShoutActivity.this).load(R.drawable.shout_pause).into(audioPlayImageView);
            updateSeekbar();
            playingVisuals();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Glide.with(NewShoutActivity.this).load(R.drawable.shout_play).into(audioPlayImageView);

                textCurrentTime.setText("0:00 ");
                //  textTotalDuration.setText("0:00");
                //  mediaPlayer.reset();
                //  prepareMediaPlayer();
            }
        });
    }

    private void playingVisuals() {
        visualizer.setColor(ContextCompat.getColor(NewShoutActivity.this, R.color.new_theme_green));
        // visualizer.setStrokeWidth(2);
        visualizer.setPlayer(mediaPlayer.getAudioSessionId());
    }

    private void updateSeekbar() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            handler.postDelayed(updater, 1000);
        }
    }

    private void hidenewShoutSugestionLayout() {
        if (newSuggestionShoutLayout.getVisibility() == View.VISIBLE) {
            newSuggestionShoutLayout.setVisibility(View.GONE);
        }
    }

    private void visiblenewShoutSugestionLayout() {
        if (newSuggestionShoutLayout.getVisibility() == View.GONE) {
            newSuggestionShoutLayout.setVisibility(View.VISIBLE);
        }
    }

    private void puttingValuesForSuggestionsShouts() {

        newShoutSuggestedArrayalist.add(new NewSuggestedShoutModelClass(getResources().getString(R.string.shout1)));
        newShoutSuggestedArrayalist.add(new NewSuggestedShoutModelClass(getResources().getString(R.string.shout2)));
        newShoutSuggestedArrayalist.add(new NewSuggestedShoutModelClass(getResources().getString(R.string.shout3)));
        newShoutSuggestedArrayalist.add(new NewSuggestedShoutModelClass(getResources().getString(R.string.shout4)));
        newShoutSuggestedArrayalist.add(new NewSuggestedShoutModelClass(getResources().getString(R.string.shout5)));
        newShoutSuggestedArrayalist.add(new NewSuggestedShoutModelClass(getResources().getString(R.string.shout6)));

        newShoutSuggestionsAdapter.notifyDataSetChanged();
    }

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateSeekbar();
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                long currentDuration = mediaPlayer.getCurrentPosition();
                textCurrentTime.setText(milisecondsToTimer(currentDuration) + " ");
            }
        }
    };

    private String milisecondsToTimer(long milliseconds) {
        String timerString = "";
        String secondsString;

        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) (milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000;

        if (hours > 0) {
            timerString = hours + ":";

        }
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }
        timerString = timerString + minutes + ":" + secondsString;

        return timerString;
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String fileUri) {

        File file = new File(fileUri);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    private void startRecording() {
        String audioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "ShoutAudioRecording";

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(audioSavePathInDevice);

        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
            Toast.makeText(NewShoutActivity.this, "Recording", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopRecordingAndSendingEmit() {
        Log.e("RecordingTest", "stop recording and emitting");
        showLoader();
        if (mediaRecorder != null) {
            try {
                mediaRecorder.stop();
            } catch (RuntimeException stopException) {
                Toast.makeText(NewShoutActivity.this, "Hold to record, release to send.", Toast.LENGTH_SHORT).show();
                // handle cleanup here
            }
        }
        String fileFromStorage = readBytesFromFile(Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/" + "ShoutAudioRecording");

        JSONObject mainJsonObject = new JSONObject();
        JSONObject video = new JSONObject();

        try {
            video.put("type", "");
            video.put("dataURL", fileFromStorage);
            video.put("time", MySocketsClass.getInstance().MakingCurrentDateTimeStamp());
            mainJsonObject.put("video", video);

            mSocket.off(MySocketsClass.ListenersClass.UPLOAD_AUDIO_SHOUT_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.UPLOAD_AUDIO_SHOUT, mainJsonObject);
            mSocket.on(MySocketsClass.ListenersClass.UPLOAD_AUDIO_SHOUT_RESPONSE, audio_shout_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String readBytesFromFile(String filePath) {
        File file = new File(filePath);
        String img64 = null;

        try {
            FileInputStream fis = new FileInputStream(file);
            byte imgByte[] = new byte[(int) file.length()];
            fis.read(imgByte);

            //convert byte array to base64 string
            img64 = Base64.encodeToString(imgByte, Base64.NO_WRAP);
            String img643 = Base64.encodeToString(imgByte, Base64.NO_WRAP);
            //send img64 to socket.io servr
        } catch (Exception e) {
            //
        }
        return img64;
    }

    private void prepareMediaPlayer(String dataSource) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(baseUrl + dataSource);
            mediaPlayer.prepare();
            textTotalDuration.setText("/ " + milisecondsToTimer(mediaPlayer.getDuration()));
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("testing", "excpetn >" + e.getMessage());
        }
    }

    private void makingMediaPlayerNullAndStopping() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (audioShoutUrl != null) {
            audioShoutUrl = null;
        }
    }

    private void hidingUploadedImageShoutView() {
        if (UpoadedShoutImageView.getVisibility() == View.VISIBLE) {
            UpoadedShoutImageView.setVisibility(View.GONE);
        }
        if (ImageShoutURL != null) {
            ImageShoutURL = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        makingMediaPlayerNullAndStopping();
    }

    Emitter.Listener add_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        String message = jsonObject.getString("message");
                        String success = jsonObject.getString("success");
                        if (success.equals("1")) {
                            Toast.makeText(NewShoutActivity.this, message, Toast.LENGTH_SHORT).show();
                            ImageShoutURL = null;
                            audioShoutUrl = null;
                            visiblenewShoutSugestionLayout();
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();

                        } else {
                            Toast.makeText(NewShoutActivity.this, message, Toast.LENGTH_SHORT).show();
                            ImageShoutURL = null;
                            audioShoutUrl = null;
                            visiblenewShoutSugestionLayout();
                            hideLoader();
                        }
                        newShoutEditText.setText("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener DeleteShoutImageResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.e("DeletngSH", "InListener00");

            JSONObject jsonObject = (JSONObject) args[0];
            try {
                String success = jsonObject.getString("success");
                if (success.equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ImageUploadingButtonForWall.setImageResource(0);
                            ImageShoutURL = null;
                            audioShoutUrl = null;
                            UpoadedShoutImageView.setVisibility(View.GONE);
                            visiblenewShoutSugestionLayout();
                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener DeleteShoutAudioResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject jsonObject = (JSONObject) args[0];
            try {
                String success = jsonObject.getString("success");
                if (success.equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ImageUploadingButtonForWall.setImageResource(0);
                            ImageShoutURL = null;
                            audioShoutUrl = null;
                            hidingAudioLayout();
                            visiblenewShoutSugestionLayout();
                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener audio_shout_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        hidenewShoutSugestionLayout();
                        hidingUploadedImageShoutView();
                        hideLoader();
                        String audioUrl = jsonObject.getString("url");
                        audioShoutUrl = jsonObject.getString("url");
                        prepareMediaPlayer(audioUrl);
                        showingAudioLayout();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    @Override
    public void newShoutSentence(String shoutSentence) {
        newShoutEditText.setText(shoutSentence);
        newShoutEditText.setSelection(shoutSentence.length());
    }
}
