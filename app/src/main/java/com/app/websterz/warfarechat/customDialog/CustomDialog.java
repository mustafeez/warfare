package com.app.websterz.warfarechat.customDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.about.GetSupport;
import com.app.websterz.warfarechat.webView.MyWebView;


public class CustomDialog extends AppCompatActivity {
    TextView dialogTosTv, dialogPrivacyPolicyTv, dialogGetSupportTv, dialogAboutTv;
    Dialog dialog;
    Activity mActivity;
    Button dialogCancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_dialog);


    }

    public void showDialog(Activity activity, final int status) {
        dialog = new Dialog(activity);
        mActivity = activity;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.activity_custom_dialog);


        dialogTosTv = dialog.findViewById(R.id.dialogTosTv);
        dialogPrivacyPolicyTv = dialog.findViewById(R.id.dialogPrivacyPolicyTv);
        dialogGetSupportTv = dialog.findViewById(R.id.dialogGetSupportTv);
        dialogAboutTv = dialog.findViewById(R.id.dialogAboutTv);
        dialogCancelButton = dialog.findViewById(R.id.dialogCancelButton);


        dialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


        dialogTosTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getContext().startActivity(new Intent(mActivity, MyWebView.class));
                dialog.dismiss();
            }
        });

        dialogPrivacyPolicyTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getContext().startActivity(new Intent(mActivity, MyWebView.class));
                dialog.dismiss();
            }
        });

        dialogGetSupportTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (status == 0) {
                    dialog.getContext().startActivity(new Intent(mActivity, GetSupport.class));
                    dialog.dismiss();
                }
            }
        });

        dialogAboutTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getContext().startActivity(new Intent(mActivity, MyWebView.class));
                dialog.dismiss();
            }
        });


    }
}
