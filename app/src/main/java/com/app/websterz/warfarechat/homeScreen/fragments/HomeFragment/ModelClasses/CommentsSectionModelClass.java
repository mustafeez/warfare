package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses;

public class CommentsSectionModelClass {
    private String Commentid,shoutID,username,name,dp,role,age,gender,text,time,owner;

    public CommentsSectionModelClass(String id, String shoutID, String username, String name, String dp, String role, String age, String gender, String text, String time,String owner) {
        this.Commentid = id;
        this.shoutID = shoutID;
        this.username = username;
        this.name = name;
        this.dp = dp;
        this.role = role;
        this.age = age;
        this.gender = gender;
        this.text = text;
        this.time = time;
        this.owner = owner;
    }

    public void setCommentid(String commentid) {
        Commentid = commentid;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCommentid() {
        return Commentid;
    }

    public String getShoutID() {
        return shoutID;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getDp() {
        return dp;
    }

    public String getRole() {
        return role;
    }

    public String getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

    public void setId(String id) {
        this.Commentid = id;
    }

    public void setShoutID(String shoutID) {
        this.shoutID = shoutID;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
