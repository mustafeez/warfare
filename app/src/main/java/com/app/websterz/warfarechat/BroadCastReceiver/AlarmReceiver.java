package com.app.websterz.warfarechat.BroadCastReceiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Vibrator;

import androidx.core.app.NotificationCompat;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.login.Login;

public class AlarmReceiver extends BroadcastReceiver {
    String CHANNEL_ID = "MY_LOGOUT_NOTIFICATION";
    String Title = "Socio";
    String msg = "We miss you at Socio. Click to login";

    @Override
    public void onReceive(Context context, Intent intent) {


        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        Intent notificationIntent = new Intent(context, Login.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(Login.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(100, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O){
            NotificationCompat.Builder builder=new NotificationCompat.Builder(context,"1994")
                    .setSmallIcon(R.drawable.socio_icon)
                    .setContentTitle(Title)
                    .setContentText(msg)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent);
            v.vibrate(500);

            NotificationManager notificationManager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0,builder.build());
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            int notificationID = 100;// The id of the channel.
            CharSequence UserVariableNamwe = "warfare_logout_notification";// The user-visible name of the channel.
            int priority = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel myChanel = new NotificationChannel(CHANNEL_ID, UserVariableNamwe, priority);
            // Create a notification and set the notification channel.
            NotificationCompat.Builder builder=new NotificationCompat.Builder(context,"1994")
                    .setSmallIcon(R.drawable.socio_icon)
                    .setContentTitle(Title)
                    .setContentText(msg)
                    .setAutoCancel(true)
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pendingIntent);
            v.vibrate(500);

            builder.setDefaults(Notification.DEFAULT_VIBRATE);
            builder.setDefaults(Notification.DEFAULT_LIGHTS);
            builder.setDefaults(Notification.DEFAULT_SOUND);

            NotificationManager notificationManager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(myChanel);
            notificationManager.notify(notificationID,builder.build());
        }
    }

//    @Override
//    public void onReceive(Context context, Intent intent) {
//
//        Intent intents=new Intent(context,Login.class);
//        intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent= PendingIntent.getActivity(context,0,intents,0);
//
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O){
//            NotificationCompat.Builder builder=new NotificationCompat.Builder(context,"1994")
//                    .setSmallIcon(R.drawable.bomb_icon)
//                    .setContentTitle("Today's Word")
//                    .setContentText("Test")
//                    .setAutoCancel(true)
//                    .setContentIntent(pendingIntent);
//
//
//            NotificationManager notificationManager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0,builder.build());
//        }
//
//        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//            int notificationID = 100;
//            String MY_CHANNEL_ID = "my_notification_1994";// The id of the channel.
//            CharSequence UserVariableNamwe = "Dictionary";// The user-visible name of the channel.
//            int priority = NotificationManager.IMPORTANCE_HIGH;
//            NotificationChannel myChanel = new NotificationChannel(MY_CHANNEL_ID, UserVariableNamwe, priority);
//// Create a notification and set the notification channel.
//            NotificationCompat.Builder builder=new NotificationCompat.Builder(context,"1994")
//                    .setSmallIcon(R.drawable.bomb_icon)
//                    .setContentTitle("Today's Word")
//                    .setContentText("Test")
//                    .setAutoCancel(true)
//                    .setChannelId(MY_CHANNEL_ID)
//                    .setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.createNotificationChannel(myChanel);
//            notificationManager.notify(notificationID,builder.build());
//        }
//    }
}
