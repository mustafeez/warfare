package com.app.websterz.warfarechat.activities.SendAttacks;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class SendAttackRecyclerViewAdapter extends RecyclerView.Adapter<SendAttackRecyclerViewAdapter.ViewHolder> {

    ArrayList<SendAttackModelClass> AttackList ;
    Context context;
    String chatroom;
    int attackNamePosition;

    public SendAttackRecyclerViewAdapter(ArrayList<SendAttackModelClass> attackList, Context context, String chatroom) {
        AttackList = attackList;
        this.context = context;
        this.chatroom = chatroom;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.send_attack_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.attackName.setText(AttackList.get(position).getAttackName() );
        holder.attackDescription.setText(AttackList.get(position).getAttackDescription());
        holder.attackCoins.setText(AttackList.get(position).getAttackPoints() + " Points");
        String Image = AttackList.get(position).getAttackImageView();
        //Add Avatar Loader
        holder.attackAvatarLoader.setVisibility(View.VISIBLE);
        holder.attackAvatarLoader.setAnimating(true);
        Glide.with(context).load(baseUrl + Image).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.attackAvatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.attackImageView);

        holder.attackLaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attackNamePosition = position;
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("chatroom",chatroom);
                    mSocket.emit(MySocketsClass.EmmittersClass.GET_ROOM_USERS, jsonObject);
                    mSocket.on(MySocketsClass.ListenersClass.GET_ROOM_USERS_RESPONSE, get_room_users_response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    Emitter.Listener get_room_users_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];


            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
                    giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity, jsonObject,
                            "Attacks",AttackList.get(attackNamePosition).getAttackName(),
                            AttackList.get(attackNamePosition).getSlug(),"");
                    mSocket.off(MySocketsClass.ListenersClass.GET_ROOM_USERS_RESPONSE);
                }
            });

        }
    };

    @Override
    public int getItemCount() {
        return AttackList.size() ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AvatarView attackAvatarLoader;
        ImageView attackImageView;
        TextView attackName,attackDescription,attackCoins;
        Button attackLaunch;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            attackAvatarLoader = itemView.findViewById(R.id.AttackAvatarLoader);
            attackImageView = itemView.findViewById(R.id.AttackImageView);

            attackName = itemView.findViewById(R.id.attackNameTextView);
            attackDescription = itemView.findViewById(R.id.attackDescriptionTextView);
            attackCoins = itemView.findViewById(R.id.attackCoinsNumberTextView);

            attackLaunch = itemView.findViewById(R.id.attackLaunchBtn);

        }
    }
}
