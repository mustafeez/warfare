package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.MakeNewFriendsModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class SuggestionsRowAdapter extends RecyclerView.Adapter<SuggestionsRowAdapter.ViewHolder> {

    ArrayList<MakeNewFriendsModelClass>FriendSuggestionArraylist=new ArrayList<>();
    Client client;
    Context MyContext;
    AlertDialog MyalertDialog;
    TextView errorDialogText1,errorDialogText2;
    ImageView friend_added_imageview;
    Button errorDialogTryAgainBtn;
    ViewHolder MyViewHolder;
    LinearLayout ParentLayout;

    public SuggestionsRowAdapter(ArrayList<MakeNewFriendsModelClass>FriendSuggestionArraylist, Context context) {
        this.FriendSuggestionArraylist=FriendSuggestionArraylist;
        client=new Client(context);
        MyContext=context;
    }

    @NonNull
    @Override
    public SuggestionsRowAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.make_new_friends_row_layout, parent, false);
        return new SuggestionsRowAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SuggestionsRowAdapter.ViewHolder holder, final int position) {

        holder.SuggestedUserName.setText(FriendSuggestionArraylist.get(position).getFriendSuggestionUsername());
//        holder.avatarViewLoader.setVisibility(View.VISIBLE);
//        holder.avatarViewLoader.setAnimating(true);

      //  Picasso.get().load(baseUrl+FriendSuggestionArraylist.get(position).getDp()).into(holder.ProfileImage);

        Glide.with(holder.itemView.getContext()).load(baseUrl + FriendSuggestionArraylist.get(position).getDp()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                //  holder.avatarViewLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(holder.ProfileImage);



        if (FriendSuggestionArraylist.get(position).getRole().equals("1")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.user_icon).into(holder.UserRank);
        } else if (FriendSuggestionArraylist.get(position).getRole().equals("2")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.mod_icon).into(holder.UserRank);
        } else if (FriendSuggestionArraylist.get(position).getRole().equals("3")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.staff_icon).into(holder.UserRank);
        } else if (FriendSuggestionArraylist.get(position).getRole().equals("4")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.mentor_icon).into(holder.UserRank);
        } else if (FriendSuggestionArraylist.get(position).getRole().equals("5")) {
            Glide.with(holder.itemView.getContext()).load(R.drawable.merchant_icon).into(holder.UserRank);
        }

        if (FriendSuggestionArraylist.get(position).getAge().equals("1")) {
            if (FriendSuggestionArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.born_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.born_female).into(holder.UserAge);
            }
        } else if (FriendSuggestionArraylist.get(position).getAge().equals("2")) {
            if (FriendSuggestionArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.teen_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.teen_female).into(holder.UserAge);
            }
        } else if (FriendSuggestionArraylist.get(position).getAge().equals("3")) {
            if (FriendSuggestionArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.young_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.young_female).into(holder.UserAge);
            }
        } else if (FriendSuggestionArraylist.get(position).getAge().equals("4")) {
            if (FriendSuggestionArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.adult_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.adult_female).into(holder.UserAge);
            }
        } else if (FriendSuggestionArraylist.get(position).getAge().equals("5")) {
            if (FriendSuggestionArraylist.get(position).getGender().equals("M")) {
                Glide.with(holder.itemView.getContext()).load(R.drawable.mature_male).into(holder.UserAge);
            } else {
                Glide.with(holder.itemView.getContext()).load(R.drawable.mature_female).into(holder.UserAge);
            }
        }
        if (FriendSuggestionArraylist.get(position).getCountry().equals(""))
        {holder.CountryFlag.setVisibility(View.GONE);}
        else {
            holder.CountryFlag.setVisibility(View.VISIBLE);
            String CountryCode=FriendSuggestionArraylist.get(position).getCountry();
            Glide.with(holder.itemView.getContext()).load("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.2.7/assets/png/"+CountryCode+".png").into(holder.CountryFlag);
        }


        if (FriendSuggestionArraylist.get(position).getReqStatus().equals("0:0"))
        {
            holder.AddButton.setBackgroundResource(R.drawable.error_dialog_circle_green);
            holder.AddButton.setText("ADD");
        }
        else {
            holder.AddButton.setText("ADDED");
            holder.AddButton.setBackgroundResource(R.drawable.error_dialog_circle_orange);
        }
        holder.AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (FriendSuggestionArraylist.get(position).getReqStatus().equals("0:0"))
                {
                    MyViewHolder=holder;
                    JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put("add_username", FriendSuggestionArraylist.get(position).getFriendSuggestionUsername());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mSocket.off("add contact response");
                mSocket.emit("add contact", jsonObject);
                mSocket.on("add contact response", adding_contact_response);
            }

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), UserFriendProfile.class);
                intent.putExtra("username", FriendSuggestionArraylist.get(position).getFriendSuggestionUsername());
                holder.itemView.getContext().startActivity(intent);
            }
        });


//        client.toImage(FriendSuggestionArraylist.get(position).getCountry(), 30, new com.emojione.tools.Callback() {
//            @Override
//            public void onFailure(IOException e) {
//            }
//
//            @Override
//            public void onSuccess(SpannableStringBuilder ssb) {
//                holder.CountryFlag.setText(ssb);
//
//            }
//        });


    }

    Emitter.Listener adding_contact_response=new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject=(JSONObject)args[0];
            ((Activity)MyContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LayoutInflater layoutInflater = LayoutInflater.from(MyContext);
                    View myview = layoutInflater.inflate(R.layout.connection_error_dialog, null);
                    final AlertDialog.Builder adb = new AlertDialog.Builder(myview.getContext());
                    MyalertDialog  = adb.create();
                    MyalertDialog.setView(myview);
                    try {
                        if (jsonObject.getString("success").equals("1")) {
                            ParentLayout = myview.findViewById(R.id.ParentLayout);
                            ParentLayout.setBackgroundResource(R.drawable.square_new_theme_green);
                            errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                            errorDialogText1.setVisibility(View.GONE);
                            friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                            friend_added_imageview.setVisibility(View.VISIBLE);
                            friend_added_imageview.setImageResource(R.drawable.add_user_green);
                            friend_added_imageview.setColorFilter(ContextCompat.getColor(MyContext,R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                            errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                            try {
                                errorDialogText2.setText(jsonObject.getString("message"));
                                errorDialogText2.setTextColor(MyContext.getResources().getColor(R.color.black));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                            errorDialogTryAgainBtn.setText("Ok");
                            errorDialogTryAgainBtn.setBackground(MyContext.getResources().getDrawable(R.drawable.error_dialog_green_btn));
                            MyViewHolder.AddButton.setText("Added");
                            MyViewHolder.AddButton.setBackgroundResource(R.drawable.error_dialog_circle_orange);
                        }
                        else {
                            ParentLayout = myview.findViewById(R.id.ParentLayout);
                            ParentLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);
                            errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                            errorDialogText1.setVisibility(View.GONE);
                            friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                            friend_added_imageview.setVisibility(View.VISIBLE);
                            friend_added_imageview.setImageResource(R.drawable.sad_icon);
                            errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                            try {
                                errorDialogText2.setText(jsonObject.getString("message"));
                                errorDialogText2.setTextColor(MyContext.getResources().getColor(R.color.black));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                            errorDialogTryAgainBtn.setText("Ok");
                            errorDialogTryAgainBtn.setBackground(MyContext.getResources().getDrawable(R.drawable.error_dialog_red_btn));
                        }
                        errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MyalertDialog.dismiss();
                                ContactsFragment contactsFragment = new ContactsFragment();
                                contactsFragment.getContacts();

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(!((Activity) MyContext).isFinishing())
                        MyalertDialog.show();
                }
            });

        }
    };

    @Override
    public int getItemCount() {
        return FriendSuggestionArraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView UserRank,UserAge;
        TextView SuggestedUserName;
        TextView AddButton;
        ImageView CountryFlag,ProfileImage;
      //  AvatarView avatarViewLoader;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            CountryFlag=itemView.findViewById(R.id.CountryFlag);
            ProfileImage=itemView.findViewById(R.id.ProfileImage);
            UserRank=itemView.findViewById(R.id.UserRank);
          //  avatarViewLoader = itemView.findViewById(R.id.ContactavatarLoader);
            UserAge=itemView.findViewById(R.id.UserAge);
            SuggestedUserName=itemView.findViewById(R.id.SuggestedUserName);
            AddButton=itemView.findViewById(R.id.AddButton);
        }
    }
}
