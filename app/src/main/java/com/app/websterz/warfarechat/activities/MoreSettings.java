package com.app.websterz.warfarechat.activities;

import com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers.ShoutsBlockerModelClass;
import com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers.UserBlockerModelClass;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;
import com.rm.rmswitch.RMSwitch;
import com.rm.rmswitch.RMTristateSwitch;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class MoreSettings extends AppCompatActivity {

    RMSwitch friendRequestsRMS,serviceRunnerRMS,muteNotificationsRunnerRMS;
    RMTristateSwitch profileViewRMS;
    int add_requests, pf_view;
    RelativeLayout parentLinearLayout;
    LinearLayout returning_imageview;
    LinearLayout parent_child_layout;
    MySingleTon mySingleTon;
    Button BlockedUsersButton, BlockedShoutsButton;
    ArrayList<UserBlockerModelClass> UsersBlockersList;
    ArrayList<ShoutsBlockerModelClass> ShoutsBlockersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_settings);

        initializations();
        setting_up_switches();
        my_code();
        BlockedShoutsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlockedShouts("BlockedShouts");
            }
        });
        BlockedUsersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlockedUsers("BlockedUsers");
            }
        });
    }

    public void initializations() {
        mySingleTon = MySingleTon.getInstance();
        UsersBlockersList = new ArrayList<>();
        ShoutsBlockersList = new ArrayList<>();

        friendRequestsRMS = findViewById(R.id.friendRequestsRMS);
        serviceRunnerRMS = findViewById(R.id.serviceRunnerRMS);
        muteNotificationsRunnerRMS = findViewById(R.id.muteNotificationsRunnerRMS);
        profileViewRMS = findViewById(R.id.profileViewRMS);
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        parent_child_layout = findViewById(R.id.parent_child_layout);
        BlockedShoutsButton = findViewById(R.id.BlockedShoutsButton);
        BlockedUsersButton = findViewById(R.id.BlockedUsersButton);
    }

    private void setting_up_switches() {

        friendRequestsRMS.setEnabled(true);
        serviceRunnerRMS.setEnabled(true);
        muteNotificationsRunnerRMS.setEnabled(true);
        profileViewRMS.setEnabled(true);

        friendRequestsRMS.setSwitchDesign(friendRequestsRMS.DESIGN_SLIM);
        friendRequestsRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
        friendRequestsRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
        friendRequestsRMS.setSwitchToggleCheckedColor(getResources().getColor(R.color.greendark));
        friendRequestsRMS.setSwitchToggleNotCheckedColor(getResources().getColor(R.color.colorRed));
        friendRequestsRMS.setSwitchToggleCheckedDrawableRes(R.drawable.happy_switch);
        friendRequestsRMS.setSwitchToggleNotCheckedDrawableRes(R.drawable.sad_switch);



        serviceRunnerRMS.setSwitchDesign(serviceRunnerRMS.DESIGN_SLIM);
        serviceRunnerRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
        serviceRunnerRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
        serviceRunnerRMS.setSwitchToggleCheckedColor(getResources().getColor(R.color.greendark));
        serviceRunnerRMS.setSwitchToggleNotCheckedColor(getResources().getColor(R.color.colorRed));
        serviceRunnerRMS.setSwitchToggleCheckedDrawableRes(R.drawable.service_icon);
        serviceRunnerRMS.setSwitchToggleNotCheckedDrawableRes(R.drawable.service_icon);

        if (checkingRunningServiceStatus().equalsIgnoreCase("NotRunForegroundService")) {
            serviceRunnerRMS.setChecked(false);
            serviceRunnerRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
        } else {
            serviceRunnerRMS.setChecked(true);
            serviceRunnerRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
        }

        muteNotificationsRunnerRMS.setSwitchDesign(muteNotificationsRunnerRMS.DESIGN_SLIM);
        muteNotificationsRunnerRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
        muteNotificationsRunnerRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
        muteNotificationsRunnerRMS.setSwitchToggleCheckedColor(getResources().getColor(R.color.greendark));
        muteNotificationsRunnerRMS.setSwitchToggleNotCheckedColor(getResources().getColor(R.color.colorRed));
        muteNotificationsRunnerRMS.setSwitchToggleCheckedDrawableRes(R.drawable.user_profile_buzz);
        muteNotificationsRunnerRMS.setSwitchToggleNotCheckedDrawableRes(R.drawable.mute_notification);

        if (checkingNotificationMuteStatus().equals("Muted")) {
            muteNotificationsRunnerRMS.setChecked(false);
            muteNotificationsRunnerRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
        } else {
            muteNotificationsRunnerRMS.setChecked(true);
            muteNotificationsRunnerRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
        }


        profileViewRMS.setSwitchToggleLeftDrawableRes(R.drawable.pf_views_off);
        profileViewRMS.setSwitchToggleMiddleDrawableRes(R.drawable.pf_views_friends_only);
        profileViewRMS.setSwitchToggleRightDrawableRes(R.drawable.pf_views_on);
        profileViewRMS.setSwitchDesign(profileViewRMS.DESIGN_SLIM);

//        themeRMS.setSwitchToggleLeftDrawableRes(R.drawable.theme_night);
//        themeRMS.setSwitchToggleMiddleDrawableRes(R.drawable.theme_auto);
//        themeRMS.setSwitchToggleRightDrawableRes(R.drawable.theme_day);
//        themeRMS.setSwitchDesign(friendRequestsRMS.DESIGN_SLIM);
    }

    private void my_code() {

        mSocket.emit(MySocketsClass.EmmittersClass.GET_SETTINGS);
        returning_imageview = MySocketsClass.custom_layout.Loader_image(MoreSettings.this, parentLinearLayout);
        mSocket.on(MySocketsClass.ListenersClass.GET_SETTINGS_RESPONSE, get_settings_response);

        friendRequestsRMS.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked) {
                    friend_request_privacy(1);
                    friendRequestsRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
                } else {
                    friend_request_privacy(0);
                    friendRequestsRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
                }
            }
        });

        serviceRunnerRMS.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked) {
                    serviceRunnerRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
                    savingRunningServicestatus("RunForegroundService");
                    startingNewService();
                    Toast.makeText(MoreSettings.this, "Service Run", Toast.LENGTH_SHORT).show();
                } else {
                    serviceRunnerRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
                    savingRunningServicestatus("NotRunForegroundService");
                    startingNewService();
                    Toast.makeText(MoreSettings.this, "Service not Run", Toast.LENGTH_SHORT).show();
                }
            }
        });

        muteNotificationsRunnerRMS.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked) {
                    muteNotificationsRunnerRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
                    savingNotificationsstatus("UnMuted");
                    Toast.makeText(MoreSettings.this, "Notification unmuted.", Toast.LENGTH_SHORT).show();
                } else {
                    muteNotificationsRunnerRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
                    savingNotificationsstatus("Muted");
                    Toast.makeText(MoreSettings.this, "Notifications muted.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        profileViewRMS.addSwitchObserver(new RMTristateSwitch.RMTristateSwitchObserver() {
            @Override
            public void onCheckStateChange(RMTristateSwitch switchView, int state) {
                if (state == RMTristateSwitch.STATE_LEFT) {
                    viewing_profile(0);
                    profileViewRMS.setSwitchToggleLeftDrawableRes(R.drawable.pf_views_off);
                    profileViewRMS.setSwitchBkgLeftColor(getResources().getColor(R.color.colorRed));
                    profileViewRMS.setSwitchToggleLeftColor(getResources().getColor(R.color.colorRed));
                } else if (state == RMTristateSwitch.STATE_MIDDLE) {
                    viewing_profile(1);
                    profileViewRMS.setSwitchToggleMiddleDrawableRes(R.drawable.pf_views_friends_only);
                    profileViewRMS.setSwitchBkgMiddleColor(getResources().getColor(R.color.colorOrange));
                    profileViewRMS.setSwitchToggleMiddleColor(getResources().getColor(R.color.colorOrange));
                } else if (state == RMTristateSwitch.STATE_RIGHT) {
                    viewing_profile(2);
                    profileViewRMS.setSwitchToggleRightDrawableRes(R.drawable.pf_views_on);
                    profileViewRMS.setSwitchBkgRightColor(getResources().getColor(R.color.greendark));
                    profileViewRMS.setSwitchToggleRightColor(getResources().getColor(R.color.greendark));
                }
            }
        });
//        themeRMS.addSwitchObserver(new RMTristateSwitch.RMTristateSwitchObserver() {
//            @Override
//            public void onCheckStateChange(RMTristateSwitch switchView, int state) {
 //               if (state == RMTristateSwitch.STATE_LEFT) {
                  //  updating_theme(0);
                  //  MySocketsClass.static_objects.theme = Integer.parseInt(mySingleTon.theme);
//                    themeRMS.setSwitchBkgLeftColor(getResources().getColor(R.color.colorBlack));
//                    themeRMS.setSwitchToggleLeftColor(getResources().getColor(R.color.colorBlack));
//                } else if (state == RMTristateSwitch.STATE_MIDDLE) {
                  //  updating_theme(1);
                  //  mySingleTon.theme = "1";
//                    SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(MoreSettings.this).edit();
//                    prefEditor55.putString("theme", mySingleTon.theme);
//                    prefEditor55.apply();
                   // MySocketsClass.static_objects.theme = Integer.parseInt(mySingleTon.theme);
//                    themeRMS.setSwitchBkgMiddleColor(getResources().getColor(R.color.colorGray));
//                    themeRMS.setSwitchToggleMiddleColor(getResources().getColor(R.color.colorGray));
//
//                } else if (state == RMTristateSwitch.STATE_RIGHT) {
                  //  updating_theme(2);
//                    mySingleTon.theme = "2";
//                    SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(MoreSettings.this).edit();
//                    prefEditor55.putString("theme", mySingleTon.theme);
//                    prefEditor55.apply();
//                    MySocketsClass.static_objects.theme = Integer.parseInt(mySingleTon.theme);
//                    themeRMS.setSwitchBkgRightColor(getResources().getColor(R.color.LightBlueSwitch));
//                    themeRMS.setSwitchToggleRightColor(getResources().getColor(R.color.LightBlueSwitch));
//                }
//            }
//        });
    }

    private void startingNewService(){
        stopService(new Intent(MoreSettings.this, Foreground_service.class));
        String serviceTypeToRun = checkingRunningServiceStatus();
        Intent intent = new Intent(MoreSettings.this, Foreground_service.class);
        if (serviceTypeToRun.equals("NotRunForegroundService")) {
            intent.putExtra("ServiceType", "SimpleService");
        } else {
            intent.putExtra("ServiceType", "ForeGroundService");
        }
        startService(intent);
    }
    private void savingRunningServicestatus(String statusOfService){
        SharedPreferences.Editor sharedPreferences=PreferenceManager.getDefaultSharedPreferences(MoreSettings.this).edit();
        sharedPreferences.putString("ServiceStats",statusOfService);
        sharedPreferences.apply();
    }
    private void savingNotificationsstatus(String status){
        SharedPreferences.Editor sharedPreferences=PreferenceManager.getDefaultSharedPreferences(MoreSettings.this).edit();
        sharedPreferences.putString("MuteNotificationStats",status);
        sharedPreferences.apply();
    }

    private String checkingRunningServiceStatus(){
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(MoreSettings.this);
        return GettingInternetState.getString("ServiceStats", "NotRunForegroundService");
    }
    private String checkingNotificationMuteStatus(){
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(MoreSettings.this);
        return GettingInternetState.getString("MuteNotificationStats", "UnMuted");
    }

    private void friend_request_privacy(int field_value) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("field_name", "add_requests");
            jsonObject.put("field_value", field_value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
        mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);
    }

    private void viewing_profile(int field_value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("field_name", "pf_view");
            jsonObject.put("field_value", field_value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
        mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);
    }

    private void updating_theme(int field_value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("field_name", "theme");
            jsonObject.put("field_value", field_value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit(MySocketsClass.EmmittersClass.UPDATE_MY_PROFILE, jsonObject);
        mSocket.on(MySocketsClass.ListenersClass.UPDATE_PROFILE_RESPONSE, update_profile_response);
    }

    private void BlockedShouts(String BlockType) {
        BlockedShoutsButton.setEnabled(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                BlockedShoutsButton.setEnabled(true);
            }
        }, 600);
        returning_imageview.setVisibility(View.VISIBLE);
        mSocket.off(MySocketsClass.ListenersClass.GET_BLOCKED_SHOUT_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.GET_BLOCKED_SHOUTS);
        mSocket.on(MySocketsClass.ListenersClass.GET_BLOCKED_SHOUT_RESPONSE, get_blocked_shout_response);
      //  Log.e("crashingCheck")
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                HidingLoader();
            }
        },2000);
    }

    private void BlockedUsers(String BlockType) {
        BlockedUsersButton.setEnabled(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                BlockedUsersButton.setEnabled(true);
            }
        }, 600);
        returning_imageview.setVisibility(View.VISIBLE);
        mSocket.off(MySocketsClass.ListenersClass.GET_SHOUT_BLOCKED_USERS_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.GET_SHOUT_BLOCKED_USERS);
        mSocket.on(MySocketsClass.ListenersClass.GET_SHOUT_BLOCKED_USERS_RESPONSE, get_shout_blocked_user_response);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                HidingLoader();
            }
        },2000);
    }

    private void HidingLoader(){
        if (returning_imageview.getVisibility()==View.VISIBLE)
        {
            returning_imageview.setVisibility(View.GONE);
        }
    }

    Emitter.Listener get_settings_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        HidingLoader();
                        parent_child_layout.setVisibility(View.VISIBLE);
                        add_requests = jsonObject.getInt("add_requests");
                        //theme = jsonObject.getInt("theme");
                        pf_view = jsonObject.getInt("pf_view");
                        Log.e("Mustafeez", "" + add_requests + pf_view);

                        if (add_requests == 0) {
                            friendRequestsRMS.setChecked(false);
                            friendRequestsRMS.setSwitchBkgNotCheckedColor(getResources().getColor(R.color.colorRed));
                        } else {
                            friendRequestsRMS.setChecked(true);
                            friendRequestsRMS.setSwitchBkgCheckedColor(getResources().getColor(R.color.greendark));
                        }

//                        if (theme == 0) {
//                            themeRMS.setState(RMTristateSwitch.STATE_LEFT);
//                            themeRMS.setSwitchToggleLeftDrawableRes(R.drawable.theme_night);
//                            themeRMS.setSwitchBkgLeftColor(getResources().getColor(R.color.colorBlack));
//                            themeRMS.setSwitchToggleLeftColor(getResources().getColor(R.color.colorBlack));
//                        } else if (theme == 1) {
//                            themeRMS.setState(RMTristateSwitch.STATE_MIDDLE);
//                            themeRMS.setSwitchToggleMiddleDrawableRes(R.drawable.theme_auto);
//                            themeRMS.setSwitchBkgMiddleColor(getResources().getColor(R.color.colorGray));
//                            themeRMS.setSwitchToggleMiddleColor(getResources().getColor(R.color.colorGray));
//                        } else if (theme == 2) {
//                            themeRMS.setState(RMTristateSwitch.STATE_RIGHT);
//                            themeRMS.setSwitchToggleRightDrawableRes(R.drawable.theme_day);
//                            themeRMS.setSwitchBkgRightColor(getResources().getColor(R.color.LightBlueSwitch));
//                            themeRMS.setSwitchToggleRightColor(getResources().getColor(R.color.LightBlueSwitch));
//                        }
                        if (pf_view == 0) {
                            profileViewRMS.setState(RMTristateSwitch.STATE_LEFT);
                            profileViewRMS.setSwitchToggleLeftDrawableRes(R.drawable.pf_views_off);
                            profileViewRMS.setSwitchBkgLeftColor(getResources().getColor(R.color.colorRed));
                            profileViewRMS.setSwitchToggleLeftColor(getResources().getColor(R.color.colorRed));
                        } else if (pf_view == 1) {
                            profileViewRMS.setState(RMTristateSwitch.STATE_MIDDLE);
                            profileViewRMS.setSwitchToggleMiddleDrawableRes(R.drawable.pf_views_friends_only);
                            profileViewRMS.setSwitchBkgMiddleColor(getResources().getColor(R.color.colorOrange));
                            profileViewRMS.setSwitchToggleMiddleColor(getResources().getColor(R.color.colorOrange));
                        } else if (pf_view == 2) {
                            profileViewRMS.setState(RMTristateSwitch.STATE_RIGHT);
                            profileViewRMS.setSwitchToggleRightDrawableRes(R.drawable.pf_views_on);
                            profileViewRMS.setSwitchBkgRightColor(getResources().getColor(R.color.greendark));
                            profileViewRMS.setSwitchToggleRightColor(getResources().getColor(R.color.greendark));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    Emitter.Listener update_profile_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        int success = jsonObject.getInt("success");
                        String message = jsonObject.getString("message");
                        if (success == 1) {
                            Snackbar.make(parentLinearLayout, message, Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    Emitter.Listener get_shout_blocked_user_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
            UsersBlockersList.clear();
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                for (int i = 0; i < jsonObject.getJSONArray("users").length(); i++) {
                    UsersBlockersList.add(new UserBlockerModelClass(jsonObject.getJSONArray("users").getJSONObject(i).getString("dp"),
                            jsonObject.getJSONArray("users").getJSONObject(i).getString("username")));
                }
                HidingLoader();
                Intent intent = new Intent(MoreSettings.this, BlockShouts_UserActivity.class);
                intent.putParcelableArrayListExtra("BlockerList", UsersBlockersList);
                intent.putExtra("BlockerListData","BlockedUserss");
                startActivity(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }
                }
            });
        }
    };
    Emitter.Listener get_blocked_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


            ShoutsBlockersList.clear();
            try {
                JSONObject jsonObject = (JSONObject) args[0];
                for (int i = 0; i < jsonObject.getJSONArray("shouts").length(); i++) {
                    String username=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("username");
                    String name=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("name");
                    String id=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("id");
                    String mdp=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("dp");
                    String age=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("age");
                    String gender=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("gender");
                    String role=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("role");
                    String time=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("time");
                    String text=jsonObject.getJSONArray("shouts").getJSONObject(i).getString("text");
                    ShoutsBlockersList.add(new ShoutsBlockerModelClass(username,name,id,age,gender,role,time,text,mdp));
                }
                HidingLoader();
                Intent intent = new Intent(MoreSettings.this, BlockShouts_UserActivity.class);
                intent.putParcelableArrayListExtra("BlockerList", ShoutsBlockersList);
                intent.putExtra("BlockerListData","BlockedShoutss");
                startActivity(intent);
            } catch (JSONException e) {
            }
                }
            });
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = MoreSettings.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
    }
}