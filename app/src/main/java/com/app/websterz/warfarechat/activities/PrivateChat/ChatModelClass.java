package com.app.websterz.warfarechat.activities.PrivateChat;


public class ChatModelClass {

    private String myDatabaseMsg,myDatabaseDate,myDatabaseDp,myDatabaseMsgstatus,myDatabasechattingwith,ChatMessageType;

    public ChatModelClass(String myDatabaseMsg,
                          String myDatabaseDate,
                          String myDatabaseDp,
                          String myDatabaseMsgstatus,
                          String myDatabasechattingwith,
                          String messagetype) {

        this.myDatabasechattingwith=myDatabasechattingwith;
        this.myDatabaseMsg=myDatabaseMsg;
        this.myDatabaseDate=myDatabaseDate;
        this.myDatabaseDp=myDatabaseDp;
        this.myDatabaseMsgstatus=myDatabaseMsgstatus;
        this.ChatMessageType=messagetype;
    }

    public String getMyDatabaseMsg() {
        return myDatabaseMsg;
    }

    public String getMyDatabaseDate() {
        return myDatabaseDate;
    }

    public String getMyDatabaseDp() {
        return myDatabaseDp;
    }

    public String getMyDatabaseMsgstatus() {
        return myDatabaseMsgstatus;
    }

    public String getMyDatabasechattingwith() {
        return myDatabasechattingwith;
    }

    public String getChatMessageType() {
        return ChatMessageType;
    }

    public void setMyDatabaseMsg(String myDatabaseMsg) {
        this.myDatabaseMsg = myDatabaseMsg;
    }

    public void setMyDatabaseDate(String myDatabaseDate) {
        this.myDatabaseDate = myDatabaseDate;
    }

    public void setMyDatabaseDp(String myDatabaseDp) {
        this.myDatabaseDp = myDatabaseDp;
    }

    public void setMyDatabaseMsgstatus(String myDatabaseMsgstatus) {
        this.myDatabaseMsgstatus = myDatabaseMsgstatus;
    }

    public void setMyDatabasechattingwith(String myDatabasechattingwith) {
        this.myDatabasechattingwith = myDatabasechattingwith;
    }

    public void setChatMessageType(String chatMessageType) {
        ChatMessageType = chatMessageType;
    }
}
