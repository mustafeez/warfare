package com.app.websterz.warfarechat.homeScreen.fragments.contacts.adapters;

import android.app.AlertDialog;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.TestingPackage.NewPrivateChat;
import com.app.websterz.warfarechat.activities.UserFriendProfile;

import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ContactsRecyclerAdapter extends RecyclerView.Adapter<ContactsRecyclerAdapter.ViewHolder> implements Filterable {
    JSONArray jsonArray;
    JSONArray filterjsonArray;
    View view2;
    int total_rows;
    LinearLayout delete_friend, block_friend, buzz, view_profile;
    TextView friend_name, block_friend_text, validation_textview_status;
    ImageView block_unblock_friend, errorDialogVerificationImg, errorDialogHeaderImg;
    ContactsFragment.FriendRequestInterface friendRequestInterface;

    public ContactsRecyclerAdapter(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        filterjsonArray = jsonArray;
        Log.e("visbly", "ContactsRecyclerAdapter");

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_contacts_recycler_adapter, parent, false);
        friendRequestInterface = (ContactsFragment.FriendRequestInterface) view.getContext();
        Log.e("visbly", "oncreateviewhold");
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ContactsRecyclerAdapter.ViewHolder viewHolder, final int i) {

        if (filterjsonArray.length() == 0) {
            viewHolder.contacts_recycler.setVisibility(View.GONE);
            viewHolder.layout_first_contact.setVisibility(View.VISIBLE);
            viewHolder.dotted_line_row.setVisibility(View.GONE);
            Log.e("visbly", "null");
        } else {
            viewHolder.layout_first_contact.setVisibility(View.GONE);
            viewHolder.contacts_recycler.setVisibility(View.VISIBLE);
            viewHolder.dotted_line_row.setVisibility(View.VISIBLE);
            Log.e("visbly", "not null");
            try {
                Log.e("visbly", "notnull 2");
                String total_size = filterjsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("total");
                String dp = filterjsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("display_picture");
                String statusMsg = filterjsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("statusMsg");
                String presence = filterjsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("presence");
                final String username = filterjsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("user");
                final String reqStatus = filterjsonArray.getJSONObject(viewHolder.getAdapterPosition()).getString("reqStatus");
                Log.e("adapting", total_size);

                viewHolder.adapterName.setText(username);
                viewHolder.adapterStatus.setText(statusMsg);

                //Add AvatarView Loader
                viewHolder.avatarViewLoader.setVisibility(View.VISIBLE);
                viewHolder.avatarViewLoader.setAnimating(true);
//                Picasso.get().load(baseUrl + dp).into(viewHolder.adapterImg, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        viewHolder.avatarViewLoader.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onError(Exception e) {
//                        viewHolder.avatarViewLoader.setVisibility(View.GONE);
//                    }
//                });

                GlideApp.with(viewHolder.itemView.getContext()).load(baseUrl + dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewHolder.avatarViewLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).
                        into(viewHolder.adapterImg);

                if (presence.equals("0")) {
                    viewHolder.adapterImg.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorOffline));

                } else if (presence.equals("1")) {
                    viewHolder.adapterImg.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorOnline));

                } else if (presence.equals("2")) {
                    viewHolder.adapterImg.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorBusy));

                } else if (presence.equals("3")) {
                    viewHolder.adapterImg.setBorderColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorAway));
                }
                if (reqStatus.equals("0:1")) {
                    viewHolder.friendRequestAcceptBtn.setVisibility(View.VISIBLE);
//                    if(MySocketsClass.static_objects.theme==0){
//
//                        viewHolder.friendRequestAcceptBtn.setBackgroundResource(R.drawable.friend_request_round_btns);
//                    }
//                    else if(MySocketsClass.static_objects.theme == 1){
//                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                        Date systemDate = Calendar.getInstance().getTime();
//                        String myDate = sdf.format(systemDate);
//                        try {
//                            Date Date1 = sdf.parse(myDate);
//                            Date Date2 = sdf.parse("06:00:00 pm");
//                            if(Date1.getTime() < Date2.getTime()){
//                                viewHolder.friendRequestAcceptBtn.setBackgroundResource(R.drawable.friend_request_round_btns_light_theme);
//                            }
//                            else if(Date1.getTime() > Date2.getTime()){
//                                viewHolder.friendRequestAcceptBtn.setBackgroundResource(R.drawable.friend_request_round_btns);
//                            }
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                    else if(MySocketsClass.static_objects.theme ==2){
//                        viewHolder.friendRequestAcceptBtn.setBackgroundResource(R.drawable.friend_request_round_btns_light_theme);
  //                  }
                } else {
                    viewHolder.friendRequestAcceptBtn.setVisibility(View.GONE);
                }

                viewHolder.friendRequestAcceptBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("username", username);
                            mSocket.emit(MySocketsClass.EmmittersClass.APPROVE_ADD_REQUEST, jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                viewHolder.drop_down_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
                        v = layoutInflater.inflate(R.layout.my_dialog_box, null);
                        final AlertDialog.Builder adb = new AlertDialog.Builder(v.getContext());
                        final AlertDialog alertDialog = adb.create();
                        alertDialog.setView(v);

                        block_friend = v.findViewById(R.id.block_friend);
                        delete_friend = v.findViewById(R.id.delete_friend);
                        buzz = v.findViewById(R.id.buzz);
                        view_profile = v.findViewById(R.id.view_profile);
                        friend_name = v.findViewById(R.id.user_name);
                        block_friend_text = v.findViewById(R.id.block_friend_text);
                        block_unblock_friend = v.findViewById(R.id.block_unblock_friend);
                        friend_name.setText(viewHolder.adapterName.getText());
                        if (reqStatus.startsWith("2")) {
                            block_friend_text.setText("Unblock");
                            block_unblock_friend.setImageResource(R.drawable.unblock_friend);
                        } else if (reqStatus.startsWith("1") || reqStatus.startsWith("0")) {
                            block_friend_text.setText("Block");
                            block_unblock_friend.setImageResource(R.drawable.block_friend);
                        }

                        buzz.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("username", viewHolder.adapterName.getText());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mSocket.emit(MySocketsClass.EmmittersClass.REQUESTING_BUZZ, jsonObject);
                                alertDialog.dismiss();
                            }
                        });

                        view_profile.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(viewHolder.itemView.getContext(), UserFriendProfile.class);
                                intent.putExtra("username", viewHolder.adapterName.getText());
                                viewHolder.itemView.getContext().startActivity(intent);
                                alertDialog.dismiss();
                            }
                        });

                        delete_friend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MySocketsClass.static_objects.global_username_delete = (String) viewHolder.adapterName.getText();
  //                              CustomDialogError customDialogError = new CustomDialogError();
                                MySocketsClass.getInstance().showErrorDialog(viewHolder.itemView.getContext(), "", "Do you want to delete " + viewHolder.adapterName.getText().toString().trim()
                                        + " from you contact list?", "friendOptions","");
                                viewHolder.friendOptionsRl.setVisibility(View.GONE);
                                //  friendRequestInterface.requests(jsonArray, i, 0);
                                alertDialog.dismiss();
                            }
                        });
                        block_friend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                JSONObject jsonObject = new JSONObject();
                                if (reqStatus.startsWith("2")) {

                                    try {
                                        jsonObject.put("username", viewHolder.adapterName.getText().toString());
                                        jsonObject.put("type", 1);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mSocket.emit("block user", jsonObject);
                                    alertDialog.dismiss();
                                } else if (reqStatus.startsWith("0") || reqStatus.startsWith("1")) {
                                    try {
                                        jsonObject.put("username", viewHolder.adapterName.getText().toString());
                                        jsonObject.put("type", 2);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mSocket.emit("block user", jsonObject);
                                    alertDialog.dismiss();
                                }

                            }
                        });

                        alertDialog.show();


                    }
                });



            } catch (JSONException e) {
                Log.e("visbly", "exception");
                e.printStackTrace();
            }
        }
        viewHolder.btn_first_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                jsonArray = null;
//                jsonArray = new JSONArray();
 //               CustomDialogError customDialogError = new CustomDialogError();
                MySocketsClass.getInstance().showErrorDialog((HomeScreen) viewHolder.itemView.getContext(), "", "", "addFriend","");
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.layout_first_contact.getVisibility()==View.GONE) {


                    MySocketsClass.static_objects.SendingChatList.clear();
                    Intent intent = new Intent(viewHolder.itemView.getContext(), NewPrivateChat.class);
                    intent.putExtra("FriendChatName", viewHolder.adapterName.getText().toString());
                    intent.putExtra("BackActivity", "contactsChats");
                    viewHolder.itemView.getContext().startActivity(intent);

                    viewHolder.itemView.setEnabled(false);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            viewHolder.itemView.setEnabled(true);
                        }
                    },400 );

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        if (filterjsonArray.length() == 0) {
            total_rows = 1;
        } else {
            total_rows = filterjsonArray.length();
        }
        return total_rows;
        // return filterjsonArray.length();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private boolean isFabOpen = false;
        CircleImageView adapterImg;
        TextView adapterName, adapterStatus,TextCounter;
        ImageView contactAdapterDeleteIcon, contactAdapterBlockIcon, contactAdapterStartChatIcon;
        RelativeLayout drop_down_icon, friendOptionsRl, contacts_recycler,deletechatImg;
        RelativeLayout layout_first_contact, dotted_line_row;
        Button btn_first_contact, friendRequestAcceptBtn;
        AvatarView avatarViewLoader;


        public ViewHolder(View itemView) {
            super(itemView);
            dotted_line_row = itemView.findViewById(R.id.dotted_line_row);
            layout_first_contact = itemView.findViewById(R.id.layout_first_contact);
            avatarViewLoader = itemView.findViewById(R.id.ContactavatarLoader);
            adapterImg = itemView.findViewById(R.id.adapterImg);
            adapterName = itemView.findViewById(R.id.adapterName);
            contacts_recycler = itemView.findViewById(R.id.contacts_recycler);
            adapterStatus = itemView.findViewById(R.id.adapterStatus);
            TextCounter = itemView.findViewById(R.id.total_text_counter);
            TextCounter.setVisibility(View.GONE);
            contactAdapterDeleteIcon = itemView.findViewById(R.id.contactAdapterDeleteIcon);
            contactAdapterBlockIcon = itemView.findViewById(R.id.contactAdapterBlockIcon);
            friendOptionsRl = itemView.findViewById(R.id.friendOptionsRl);
            contactAdapterStartChatIcon = itemView.findViewById(R.id.contactAdapterStartChatIcon);
            drop_down_icon = itemView.findViewById(R.id.dropDownImg);
            drop_down_icon.setVisibility(View.VISIBLE);
            deletechatImg = itemView.findViewById(R.id.deletechatImg);
            deletechatImg.setVisibility(View.GONE);
            btn_first_contact = itemView.findViewById(R.id.btn_first_contact);
            friendRequestAcceptBtn = itemView.findViewById(R.id.friendRequestAcceptBtn);

        }
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            // country.clear();

            final FilterResults results = new FilterResults();
            if (charSequence.length() == 0) {
                // country.addAll(listcountry);
                filterjsonArray = jsonArray;


            } else {
                try {
                    final String filterPattern = charSequence.toString().toLowerCase().trim();

                    filterjsonArray = new JSONArray();

                    for (int i = 0; i < jsonArray.length(); i++) {

                        if (jsonArray.getJSONObject(i).getString("user").toLowerCase().contains(filterPattern)) {
                            filterjsonArray.put(jsonArray.getJSONObject(i));
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            results.values = filterjsonArray;
            results.count = filterjsonArray.length();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterjsonArray = (JSONArray) results.values;
            notifyDataSetChanged();
        }
    };


}


//public class ContactsRecyclerAdapter extends RecyclerView.Adapter<ContactsRecyclerAdapter.ViewHolder> {
//    private JSONArray jsonArray;
//    int size;
//    String blocking;
//    JSONObject jsonObject;
//    public static String username;
//    Socket mSocket;
//    AppConstants appConstants;
//    Activity context;
//    RelativeLayout new_friendoptionsr1;
//    LinearLayout delete_friend, block_friend,buzz,view_profile;
//    ContactsFragment.FriendRequestInterface friendRequestInterface;
//    TextView friend_name,block_friend_text,validation_textview_status;
//    ImageView block_unblock_friend,errorDialogVerificationImg,errorDialogHeaderImg;
//    Button cancel_update_status,update_status;
//    View Header_view;
//
//
//    public void addContext(Activity context) {
//        this.context = context;
//    }
//
//    public ContactsRecyclerAdapter() {
//
//    }
//
//    public ContactsRecyclerAdapter(JSONArray list, int size) {
//        this.jsonArray = list;
//        this.size = size;
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_contacts_recycler_adapter, parent, false);
//        appConstants = new AppConstants();
//        mSocket = AppConstants.mSocket;
//        Button cancel_update_status;
//        friendRequestInterface = (ContactsFragment.FriendRequestInterface) view.getContext();
//        jsonObject = new JSONObject();
//
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(final ViewHolder holder, final int position) {
//
//        String requestStatus;
//
//        try {
//            requestStatus = jsonArray.getJSONObject(position).getString("reqStatus");
//
//            if (requestStatus.substring(0, 1).equals("2")) {
//                holder.contactAdapterBlockIcon.setImageResource(R.drawable.unblock_friend);
//            } else if (requestStatus.substring(0, 1).equals("1")) {
//                holder.contactAdapterBlockIcon.setImageResource(R.drawable.block_friend);
//            }
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            if (jsonArray.getJSONObject(position).getString("total").equals("0"))
//            {
//                holder.contacts_recycler.setVisibility(View.GONE);
//                holder.layout_first_contact.setVisibility(View.VISIBLE);
//            }
//            else {
//
//                holder.layout_first_contact.setVisibility(View.GONE);
//                holder.adapterName.setText(jsonArray.getJSONObject(position).getString("user"));
//                holder.adapterStatus.setText(jsonArray.getJSONObject(position).getString("statusMsg"));
//
//                Picasso.get().load(AppConstants.baseUrl + jsonArray.getJSONObject(position).getString("display_picture")).into(holder.adapterImg);
//
//                if (jsonArray.getJSONObject(position).getString("presence").equals("0")) {
//                    holder.adapterImg.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorOffline));
//
//                } else if (jsonArray.getJSONObject(position).getString("presence").equals("1")) {
//                    holder.adapterImg.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorOnline));
//
//                } else if (jsonArray.getJSONObject(position).getString("presence").equals("2")) {
//                    holder.adapterImg.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorBusy));
//
//                } else if (jsonArray.getJSONObject(position).getString("presence").equals("3")) {
//                    holder.adapterImg.setBorderColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorAway));
//
//                }
//
////            holder.contactAdapterDeleteIcon.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////
////
////                    username = holder.adapterName.getText().toString().trim();
////                    CustomDialogError customDialogError = new CustomDialogError();
////                    customDialogError.showErrorDialog((HomeScreen) holder.itemView.getContext(), "", "Do you want to delete " + holder.adapterName.getText().toString().trim()
////                            + " from you contact list?", "friendOptions");
////                    holder.friendOptionsRl.setVisibility(View.GONE);
////
////                    friendRequestInterface.requests(jsonArray,position,0);
////
////
////
////                }
////            });
//
//                holder.contactAdapterBlockIcon.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        if (holder.contactAdapterBlockIcon.getDrawable().getConstantState().equals(
//                                ContextCompat.getDrawable(view.getContext(), R.drawable.unblock_friend).getConstantState())) {
//                            try {
//                                jsonObject.put("username", holder.adapterName.getText().toString());
//                                jsonObject.put("type", 1);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            mSocket.emit("block user", jsonObject);
//                        } else if (holder.contactAdapterBlockIcon.getDrawable().getConstantState().equals(
//                                ContextCompat.getDrawable(view.getContext(), R.drawable.block_friend).getConstantState())) {
//
//                            try {
//                                jsonObject.put("username", holder.adapterName.getText().toString());
//                                jsonObject.put("type", 2);
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            mSocket.emit("block user", jsonObject);
//                        }
//                    }
//                });
//
//            }} catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        holder.drop_down_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (new_friendoptionsr1 == null) {
////                    holder.isFabOpen = false;
////
////                }
////
////                if (!holder.isFabOpen) {
////                    holder.isFabOpen = true;
////
////
////                    if (new_friendoptionsr1 != null) {
////                        new_friendoptionsr1.setVisibility(View.GONE);
////                        new_friendoptionsr1 = null;
////                        holder.isFabOpen=false;
////
////                    }
////                    new_friendoptionsr1 = holder.friendOptionsRl;
////                    holder. friendOptionsRl.setVisibility(View.VISIBLE);
////                    holder.contactAdapterDeleteIcon.setVisibility(View.VISIBLE);
////                    holder.contactAdapterBlockIcon.setVisibility(View.VISIBLE);
////                    holder.contactAdapterStartChatIcon.setVisibility(View.VISIBLE);
////                    holder.contactAdapterDeleteIcon.animate().translationX(-view.getContext().getResources().getDimension(R.dimen.standard_55));
////                    holder.contactAdapterBlockIcon.animate().translationX(-view.getContext().getResources().getDimension(R.dimen.standard_105));
////                    holder.contactAdapterStartChatIcon.animate().translationX(-view.getContext().getResources().getDimension(R.dimen.standard_155));
////                }
////
////                else {
////                    holder.isFabOpen = false;
////                    new_friendoptionsr1 = null;
////                    holder.friendOptionsRl.setVisibility(View.GONE);
////                    holder.contactAdapterDeleteIcon.animate().translationX(0);
////                    holder.contactAdapterBlockIcon.animate().translationX(0);
////                    holder.contactAdapterStartChatIcon.animate().translationX(0);
////                }
//
//
////                                username = holder.adapterName.getText().toString().trim();
////                                CustomDialogError customDialogError = new CustomDialogError();
////                                customDialogError.showErrorDialog((HomeScreen) holder.itemView.getContext(), "", "Do you want to delete " + holder.adapterName.getText().toString().trim()
////                                        + " from you contact list?", "friendOptions");
////                                holder.friendOptionsRl.setVisibility(View.GONE);
////
////                                friendRequestInterface.requests(jsonArray,position,0);
//                LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
//                view = layoutInflater.inflate(R.layout.my_dialog_box, null);
//                final AlertDialog.Builder adb = new AlertDialog.Builder(view.getContext());
//                final AlertDialog alertDialog = adb.create();
//                alertDialog.setView(view);
//                try {
//                    blocking=jsonArray.getJSONObject(position).getString("reqStatus");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                block_friend = view.findViewById(R.id.block_friend);
//                delete_friend = view.findViewById(R.id.delete_friend);
//                buzz=view.findViewById(R.id.buzz);
//                view_profile=view.findViewById(R.id.view_profile);
//                friend_name=view.findViewById(R.id.user_name);
//                block_friend_text=view.findViewById(R.id.block_friend_text);
//                block_unblock_friend=view.findViewById(R.id.block_unblock_friend);
//
//                friend_name.setText(holder.adapterName.getText());
//
//                if (blocking.startsWith("2")) {
//                    block_friend_text.setText("Unblock");
//                    block_unblock_friend.setImageResource(R.drawable.unblock_friend);
//                }
//                else if(blocking.startsWith("1")||blocking.startsWith("0")) {
//                    block_friend_text.setText("Block");
//                    block_unblock_friend.setImageResource(R.drawable.block_friend);
//                }
//                buzz.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        JSONObject jsonObject=new JSONObject();
//                        try {
//                            jsonObject.put("username",holder.adapterName.getText());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        mSocket.emit(MySocketsClass.EmmittersClass.REQUESTING_BUZZ,jsonObject);
//                     //   mSocket.on(MySocketsClass.ListenersClass.RECEIVING_BUZZ,receiving_buzz);
//                        alertDialog.dismiss();
//                    }
//                });
//                view_profile.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent=new Intent(holder.itemView.getContext(), UserFriendProfile.class);
//                        intent.putExtra("username",holder.adapterName.getText());
//                        holder.itemView.getContext().startActivity(intent);
//
//                        alertDialog.dismiss();
//                    }
//                });
//                delete_friend.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        username = holder.adapterName.getText().toString().trim();
//                        CustomDialogError customDialogError = new CustomDialogError();
//                        customDialogError.showErrorDialog((HomeScreen) holder.itemView.getContext(), "", "Do you want to delete " + holder.adapterName.getText().toString().trim()
//                                + " from you contact list?", "friendOptions");
//                        holder.friendOptionsRl.setVisibility(View.GONE);
//
//                        friendRequestInterface.requests(jsonArray, position, 0);
//                        alertDialog.dismiss();
//                    }
//                });
//                block_friend.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if (blocking.startsWith("2") ) {
//                            try {
//                                jsonObject.put("username", holder.adapterName.getText().toString());
//                                jsonObject.put("type", 1);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            mSocket.emit("block user", jsonObject);
//                            alertDialog.dismiss();
//                        }
//                        else if (blocking.startsWith("0")||blocking.startsWith("1")) {
//
//                            try {
//                                jsonObject.put("username", holder.adapterName.getText().toString());
//                                jsonObject.put("type", 2);
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            mSocket.emit("block user", jsonObject);
//                            alertDialog.dismiss();
//                        }
//
//                    }
//                });
//                alertDialog.show();
//
//            }
//        });
//        holder.btn_first_contact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                jsonArray = null;
//                jsonArray = new JSONArray();
//                CustomDialogError customDialogError = new CustomDialogError();
//                customDialogError.showErrorDialog((HomeScreen) holder.itemView.getContext(), "", "", "addFriend");
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return jsonArray.length();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//
//        private boolean isFabOpen = false;
//        CircleImageView adapterImg;
//        TextView adapterName, adapterStatus;
//        ImageView contactAdapterDeleteIcon, contactAdapterBlockIcon, contactAdapterStartChatIcon;
//        RelativeLayout drop_down_icon, friendOptionsRl,contacts_recycler;
//        RelativeLayout layout_first_contact;
//        Button btn_first_contact;
//
//
//        public ViewHolder(final View itemView) {
//            super(itemView);
//            layout_first_contact = itemView.findViewById(R.id.layout_first_contact);
//            adapterImg = itemView.findViewById(R.id.adapterImg);
//            adapterName = itemView.findViewById(R.id.adapterName);
//            contacts_recycler=itemView.findViewById(R.id.contacts_recycler);
//            adapterStatus = itemView.findViewById(R.id.adapterStatus);
//            contactAdapterDeleteIcon = itemView.findViewById(R.id.contactAdapterDeleteIcon);
//            contactAdapterBlockIcon = itemView.findViewById(R.id.contactAdapterBlockIcon);
//            friendOptionsRl = itemView.findViewById(R.id.friendOptionsRl);
//            contactAdapterStartChatIcon = itemView.findViewById(R.id.contactAdapterStartChatIcon);
//            drop_down_icon = itemView.findViewById(R.id.dropDownImg);
//            btn_first_contact = itemView.findViewById(R.id.btn_first_contact);
//
//
//            contactAdapterStartChatIcon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
//
//        }
//    }
//
//    public void filterList(JSONArray filteredList) {
//        jsonArray = filteredList;
//        notifyDataSetChanged();
//    }
//
//    private void showFABMenu() {
//
//    }
//
//    private void closeFABMenu() {
//    }
////public Emitter.Listener receiving_buzz=new Emitter.Listener() {
////    @Override
////    public void call(Object... args) {
////        JSONObject jsonObject=(JSONObject)args[0];
////        Log.e("Mustafeez","Buzz");
////        try {
////            final String username=jsonObject.getString("username");
////            String dp=jsonObject.getString("dp url");
////            LayoutInflater layoutInflater = LayoutInflater.from(context);
////            View view = layoutInflater.inflate(R.layout.statusmsg_dialog, null);
////            final android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(view.getContext());
////            final android.app.AlertDialog alertDialog = adb.create();
////            alertDialog.setView(view);
////            cancel_update_status = view.findViewById(R.id.cancel_update_status);
////            cancel_update_status.setText("Close");
////            cancel_update_status.setBackground(ContextCompat.getDrawable(alertDialog.getContext(), R.drawable.error_dialog_white_btn));
////            update_status = view.findViewById(R.id.update_status);
////            update_status.setText("Buzz Back");
////            update_status.setBackground(ContextCompat.getDrawable(alertDialog.getContext(), R.drawable.error_dialog_green_btn));
////            errorDialogVerificationImg = view.findViewById(R.id.errorDialogVerificationImg);
////            validation_textview_status = view.findViewById(R.id.validation_textview_status);
////            errorDialogHeaderImg = view.findViewById(R.id.errorDialogHeaderImg);
////            Picasso.get().load(baseUrl + dp).into(errorDialogHeaderImg);
////            final EditText edit_text_status_update = view.findViewById(R.id.edit_text_status_update);
////            edit_text_status_update.setText(username);
////            Header_view = view.findViewById(R.id.Header_view);
////
////            update_status.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////                    JSONObject jsonObject=new JSONObject();
////                    try {
////                        jsonObject.put("username",username);
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
////                    mSocket.emit(MySocketsClass.EmmittersClass.REQUESTING_BUZZ,jsonObject);
////                    alertDialog.dismiss();
////                }
////            });
////            cancel_update_status.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////                    alertDialog.dismiss();
////
////                }
////            });
////
////            alertDialog.show();
////
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////    }
////};
//
//
//}
