package com.app.websterz.warfarechat.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;
import retrofit2.http.Body;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.MoreSettings;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.landingScreen.MainActivity;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.bumptech.glide.request.FutureTarget;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "Token_No";
    private static final String CHANNEL_2_ID = "chat";
    private static int i = 0;
    int SUMMARY_ID = 0;
    String GROUP_KEY = "com.app.websterz.warfarechat.FCM";
    Notification.InboxStyle MyInboxStyle;
    Bitmap MyBitmap = null;
    int myNotificationId = 667;
    private final int MyNotificationId = 555;
    PendingIntent notifctnPendingIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

           if (checkingNotificationMuteStatus().equalsIgnoreCase("UnMuted")){
        Map<String, String> data = remoteMessage.getData();
        String body = data.get("body");
        String id = data.get("id");
        Log.e("notificationTest", "title " + data.get("title") + " id " + id);
        String title;
        if (id.equals("0")) {
            title = "Socio";
        } else {
            title = data.get("title");
        }
        if (MyInboxStyle != null) {
            MyInboxStyle = null;
        }
        MyInboxStyle = new Notification.InboxStyle();
        MyBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.socio_icon);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            messageNotificationForOreo(this, title, body, id);
        } else {
            messageNotificationBelowOreo(this, title, body, id);
        }
    }
    }

    private String checkingNotificationMuteStatus(){
        SharedPreferences GettingInternetState = PreferenceManager.getDefaultSharedPreferences(this);
        return GettingInternetState.getString("MuteNotificationStats", "UnMuted");
    }

    @Override
    public void onNewToken(String token) {
        Log.e("deviceToken", "Refreshed token:> " + token);
        sendRegistrationToServer(token);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void messageNotificationForOreo(Context context, String title, String message, String userId) {

        int notificationId = Integer.parseInt(userId);
        ArrayList<NotificationModel> arrayList = MySocketsClass.static_objects.fcmNotificationsMap.get(userId);
        if (arrayList == null) arrayList = new ArrayList<>();
        NotificationModel notificationModel = new NotificationModel(title, message);
        arrayList.add(notificationModel);
        if (arrayList.size() > 5) arrayList.remove(0);


        MySocketsClass.static_objects.fcmNotificationsMap.put(userId, arrayList);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");
        String cid = "my_channel_01";
        String description = "Socio";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(cid, title, importance);
        mChannel.setDescription(description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        mChannel.setShowBadge(false);
        mChannel.enableVibration(true);

        Intent intent = new Intent(context, MainActivity.class);
        notifctnPendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, cid)
                .setContentTitle("Socio")
                .setContentText(message)
                .setSmallIcon(R.drawable.socio_icon)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setContentIntent(notifctnPendingIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_HIGH)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setNumber(new Random().nextInt())
                .setLights(Color.BLUE, 500, 500)
                .setDefaults(Notification.DEFAULT_ALL)
                .setChannelId(cid);
                Person person = new Person.Builder()
                        .setName(title)
                        .build();
                NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(person);
                for (NotificationModel s : arrayList) {
                    messagingStyle.addMessage(new NotificationCompat.MessagingStyle.Message(
                            s.getNotificationMessage(), System.currentTimeMillis(), person
                    ));
                }
                builder.setStyle(messagingStyle);

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
        notificationManager.notify(notificationId, notification);

    }

    public void messageNotificationBelowOreo(Context context, String title, String message, String userId) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");

            Intent intent = new Intent(context, MainActivity.class);
        notifctnPendingIntent = PendingIntent.getActivity(this, 0,
                    intent, PendingIntent.FLAG_ONE_SHOT);

        int nid = Integer.parseInt(userId);

        ArrayList<NotificationModel> arrayList = MySocketsClass.static_objects.fcmNotificationsMap.get(userId);
        if (arrayList == null) arrayList = new ArrayList<>();

        NotificationModel notificationModel = new NotificationModel(title, message);
        arrayList.add(notificationModel);
        if (arrayList.size() > 5) arrayList.remove(0);
        MySocketsClass.static_objects.fcmNotificationsMap.put(userId, arrayList);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, String.valueOf(nid))
                .setContentTitle(title)
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setContentIntent(notifctnPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setPriority(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? NotificationManager.IMPORTANCE_HIGH : NotificationCompat.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.socio_icon)
                .setLights(Color.BLUE, 500, 500)
                .setDefaults(Notification.DEFAULT_ALL);

                    Person person = new Person.Builder()
                            .setName(title)
                            .build();
                    NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(person);

                    for (NotificationModel s : arrayList) {

                        messagingStyle.addMessage(new NotificationCompat.MessagingStyle.Message(
                                s.getNotificationMessage(), System.currentTimeMillis(), person
                        ));

                    }
                    builder.setStyle(messagingStyle);

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;

        notificationManager.notify(nid, notification);

    }

    public void sendRegistrationToServer(String token) {

    }

}
