package com.app.websterz.warfarechat.my_profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.my_profile.LikeAndGiftAdapter.LikeGiftAdapter;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class LikesAndGiftsViewingResponse extends AppCompatActivity {
    RelativeLayout parentRelativeLayout;
    String LikeOrGift;
    ImageView MainImageView;
    RecyclerView LikeGiftRecyclerView;
    GridLayoutManager Like_giftslayoutmanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes_and_gifts_viewing_response);
        Initializations();
        MyCode();
    }

    private void Initializations() {
        parentRelativeLayout = findViewById(R.id.parentRelativeLayout);
        MainImageView = findViewById(R.id.MainImageView);
        LikeGiftRecyclerView = findViewById(R.id.LikeGiftRecyclerView);
        Like_giftslayoutmanager = new GridLayoutManager(LikesAndGiftsViewingResponse.this, 3);
        Like_giftslayoutmanager.scrollToPosition(0);
        LikeGiftRecyclerView.setLayoutManager(Like_giftslayoutmanager);
    }

    private void MyCode() {
        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("likesGiftsJsonObject"));

            Intent intent = getIntent();
            LikeOrGift = intent.getStringExtra("SocketType");

            if (LikeOrGift.equalsIgnoreCase("Likes")) {
                ViewingLikes(jsonObject);
            } else if (LikeOrGift.equalsIgnoreCase("Gifts")) {
                ViewingGifts(jsonObject);
            } else if (LikeOrGift.equalsIgnoreCase("Followers")) {
                viewingFollowers(jsonObject);
            } else if (LikeOrGift.equalsIgnoreCase("Friends")) {
                viewingFriends(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ViewingLikes(JSONObject jsonObject) {
        MainImageView.setImageResource(R.drawable.heart_solid);
        LikeGiftRecyclerView.setAdapter(new LikeGiftAdapter(jsonObject, "Likes"));
    }

    private void viewingFollowers(JSONObject jsonObject) {
        MainImageView.setImageResource(R.drawable.green_icon_followers);
        LikeGiftRecyclerView.setAdapter(new LikeGiftAdapter(jsonObject, "Followers"));
    }

    private void viewingFriends(JSONObject jsonObject) {
        MainImageView.setImageResource(R.drawable.friendimage_selected);
        LikeGiftRecyclerView.setAdapter(new LikeGiftAdapter(jsonObject, "Friends"));
    }

    private void ViewingGifts(JSONObject jsonObject) {
        MainImageView.setImageResource(R.drawable.green_icon_gift);
        LikeGiftRecyclerView.setAdapter(new LikeGiftAdapter(jsonObject, "Gifts"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = LikesAndGiftsViewingResponse.this;
        MySocketsClass.static_objects.global_layout = parentRelativeLayout;
    }
}
