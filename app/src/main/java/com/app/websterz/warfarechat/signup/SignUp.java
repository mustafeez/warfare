package com.app.websterz.warfarechat.signup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    EditText signUpUsernameField, signUpEmailField, signUpPasswordField, signUpReferralField;
    Button signupBtn;
    ImageView PasswrdVisibility,tosImageview,loaderTest;
    Animation buttonZoomIn, buttonZoomOut, animShake;
    int count = 0;
    boolean isTosEnabled = false;
    ImageView signUpUsernameIv, signUpEmailIv, signUpPasswordIv, signUpReferralIv;
    int gender = 0, tos = 0, PsdVisibiliyy=0;
    MySingleTon mySingleTon;
    AppConstants appConstants;
    RelativeLayout signUpProgressRl;
    private RelativeLayout mainLayout;
    LinearLayout signUpMaleBtn,signUpFemaleBtn,signUpTosBtn,social_login_FaceBook,signUpGoogleLogin;
    LoginButton login_button;
    CallbackManager callbackManager ;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN=10;
    String signUpWith="", facebookOrGoogleUserSocialId="",signup_Fullname="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initView();
        faceBookLoginCode();

        social_login_FaceBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               login_button.performClick();
            }
        });
        PasswrdVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordVisibility();
            }
        });
        signUpGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSigningUp();
            }
        });
        signUpProgressRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void googleSigningUp(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient= GoogleSignIn.getClient(SignUp.this,gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void initView() {
        mainLayout=findViewById(R.id.mainLayout);
        loaderTest=findViewById(R.id.loaderTest);
        login_button=findViewById(R.id.login_button);
        signUpGoogleLogin=findViewById(R.id.signUpGoogleLogin);

        callbackManager = CallbackManager.Factory.create();

        Glide.with(SignUp.this).load(R.drawable.socio_loader).into(loaderTest);
        GlideApp.with(this).load(R.drawable.socio_background_low).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                mainLayout.setBackground(resource);
                mainLayout.buildDrawingCache();
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {

            }
        });
        signUpProgressRl = findViewById(R.id.signUpProgressRl);
        mySingleTon = MySingleTon.getInstance();
        appConstants = new AppConstants();
        Log.e("connectingg", "signup 53");
        signUpUsernameIv = findViewById(R.id.signUpUsernameIv);
        signUpEmailIv = findViewById(R.id.signUpEmailIv);
        signUpPasswordIv = findViewById(R.id.signUpPasswordIv);
        signUpReferralIv = findViewById(R.id.signUpReferralIv);
        signUpUsernameField = findViewById(R.id.signUpUsernameField);
        signUpEmailField = findViewById(R.id.signUpEmailField);
        signUpPasswordField = findViewById(R.id.signUpPasswordField);
        signUpReferralField = findViewById(R.id.signUpReferralField);
        PasswrdVisibility = findViewById(R.id.PasswrdVisibility);
        signupBtn = findViewById(R.id.signupBtn);
        signupBtn.setOnClickListener(this);
        signUpTosBtn = findViewById(R.id.signUpTosBtn);
        signUpTosBtn.setOnClickListener(this);
        signUpMaleBtn = findViewById(R.id.signUpMaleBtn);
        signUpMaleBtn.setOnClickListener(this);
        signUpFemaleBtn = findViewById(R.id.signUpFemaleBtn);
        tosImageview = findViewById(R.id.tosImageview);
        signUpFemaleBtn.setOnClickListener(this);

        buttonZoomIn = AnimationUtils.loadAnimation(this, R.anim.buttons_zoomin);
        buttonZoomOut = AnimationUtils.loadAnimation(this, R.anim.buttons_zoomout);
        animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
    }

    private void faceBookLoginCode(){
        social_login_FaceBook=findViewById(R.id.MageNative_social_login_linear);
        final String EMAIL = "email";
       login_button.setPermissions(Collections.singletonList(EMAIL));

        Log.e("faceBokLogin","inMethod faceBookLoginCode");

        login_button.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        Toast.makeText(SignUp.this, "Login Success", Toast.LENGTH_SHORT).show();
                        Log.e("faceBokLogin","Success "+loginResult.toString());


                        GraphRequest graphRequest = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        if (object != null) {
                                            try {
                                                String fName = object.getString("first_name");
                                                fName=fName.toLowerCase();
                                                Log.e("gahg",fName);
                                                String lName = object.getString("last_name");
                                                Log.e("gahg",lName);
                                                if (object.has("email")){
                                                    String email = object.getString("email");
                                                    Log.e("gahg",email);
                                                    signUpEmailField.setText(email);
                                                }

                                                facebookOrGoogleUserSocialId = object.getString("id");
                                                signUpWith="FaceBook";
                                                signup_Fullname=fName+" "+lName;

  //                                              String image = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";

                                                signUpUsernameField.setText(fName);
                                                signUpUsernameField.setAllCaps(false);
                                                LoginManager.getInstance().logOut();

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Log.e("gahg","exceptn >"+e.getMessage());
                                            }
                                        } else {
                                            LoginManager.getInstance().logOut();
                                            Toast.makeText(SignUp.this, "Can't connect with facebook right now. Please try again later or use other way to login.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                        );
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "email,last_name,first_name");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        // App code

                        Toast.makeText(SignUp.this, "cancelled", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("faceBokLogin","excptn  "+exception.toString());
                        Toast.makeText(SignUp.this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        Log.e("faceBokLogin","ActivityResult  "+data.toString());
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        Log.e("gogleTesting","handlingTask");
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            signUpWith="Google";
            facebookOrGoogleUserSocialId=account.getId();
            signup_Fullname=account.getDisplayName();


            signUpUsernameField.setText(account.getDisplayName().replaceAll(" ","").toLowerCase());
            signUpUsernameField.setAllCaps(false);
            signUpEmailField.setText(account.getEmail());
            mGoogleSignInClient.signOut();

        } catch (ApiException e) {
            Log.e("gogleTesting","excptn >"+e.getMessage());

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signUpMaleBtn: {
                if (count == 0) {
                    signUpMaleBtn.startAnimation(buttonZoomIn);
                    signUpMaleBtn.setClickable(false);
                    signUpFemaleBtn.setClickable(true);
                    count++;
                    gender = 1;
                } else {
                    signUpMaleBtn.startAnimation(buttonZoomIn);
                    signUpFemaleBtn.startAnimation(buttonZoomOut);
                    signUpMaleBtn.setClickable(false);
                    signUpFemaleBtn.setClickable(true);
                    gender = 1;


                }
                break;
            }
            case R.id.signUpFemaleBtn: {
                if (count == 0) {
                    signUpFemaleBtn.startAnimation(buttonZoomIn);
                    signUpFemaleBtn.setClickable(false);
                    signUpMaleBtn.setClickable(true);
                    count++;
                    gender = 2;

                } else {
                    signUpFemaleBtn.startAnimation(buttonZoomIn);
                    signUpMaleBtn.startAnimation(buttonZoomOut);
                    signUpFemaleBtn.setClickable(false);
                    signUpMaleBtn.setClickable(true);
                    gender = 2;

                }
                break;
            }
            case R.id.signUpTosBtn: {
                if (!isTosEnabled) {
                    isTosEnabled = true;
                    signUpTosBtn.setBackgroundResource(R.drawable.square_green_corners);
                    tosImageview.setImageResource(R.drawable.smile_icon);
                   // signUpTosBtn.setImageResource(R.drawable.tos_agree);
                    tos++;
                } else {
                    isTosEnabled = false;
                    signUpTosBtn.setBackgroundResource(R.drawable.square_red_corners);
                    tosImageview.setImageResource(R.drawable.sad_icon);
                   // signUpTosBtn.setImageResource(R.drawable.tos_disagree);
                    tos++;
                }
                break;
            }
            case R.id.signupBtn: {
                if (isNetworkAvailable())
                {
                    if (validateForm()) {
                        try {
                            mySingleTon.usernameTextFromField = signUpUsernameField.getText().toString().trim();

                            signupUser();
                            signUpProgressRl.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
            }
                else {
                    Snackbar.make(mainLayout,"No internet connection.",Snackbar.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private boolean validateForm() {
        if (signUpUsernameField.getText().toString().trim().equals("")) {
            signUpUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            signUpUsernameIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpUsernameIv.startAnimation(animShake);
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!signUpUsernameField.getText().toString().trim().substring(0, 1).toLowerCase().matches("[a-zA-Z]+")) {
            signUpUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            signUpUsernameIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpUsernameIv.startAnimation(animShake);
            Toast.makeText(this, "Username must start with a character", Toast.LENGTH_SHORT).show();
            return false;
        } else if (signUpUsernameField.getText().toString().trim().length() < 5 || signUpUsernameField.getText().toString().trim().length() > 15) {
            signUpUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            signUpUsernameIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpUsernameIv.startAnimation(animShake);
            Toast.makeText(this, "Username length should be between 5 and 15", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            signUpUsernameIv.setImageResource(R.drawable.new_theme_input_border);
            ImageViewCompat.setImageTintList(signUpUsernameIv,null);
        }


        if (signUpEmailField.getText().toString().trim().equals("")) {
            signUpEmailIv.setImageResource(R.drawable.new_theme_input_border);
            signUpEmailIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpEmailIv.startAnimation(animShake);
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(signUpEmailField.getText().toString().trim()).matches()) {
            signUpEmailIv.setImageResource(R.drawable.new_theme_input_border);
            signUpEmailIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpEmailIv.startAnimation(animShake);
            Toast.makeText(this, "Please provide a valid email address", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            signUpEmailIv.setImageResource(R.drawable.new_theme_input_border);
            ImageViewCompat.setImageTintList(signUpEmailIv,null);
        }


        if (signUpPasswordField.getText().toString().trim().equals("")) {
            signUpPasswordIv.setImageResource(R.drawable.new_theme_input_border);
            signUpPasswordIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpPasswordIv.startAnimation(animShake);
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (signUpPasswordField.getText().toString().trim().length() < 6) {
            signUpPasswordIv.setImageResource(R.drawable.new_theme_input_border);
            signUpPasswordIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpPasswordIv.startAnimation(animShake);
            Toast.makeText(this, "Password length should be greater than 6", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            signUpPasswordIv.setImageResource(R.drawable.new_theme_input_border);
            ImageViewCompat.setImageTintList(signUpPasswordIv,null);
        }


        if (!(signUpReferralField.getText().toString().trim().equals("")) && signUpReferralField.getText().toString().trim().matches("^[aA-zZ]\\\\w{7,29}$")) {
            signUpReferralIv.setImageResource(R.drawable.new_theme_input_border);
            signUpReferralIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpReferralIv.startAnimation(animShake);
            Toast.makeText(this, "Please enter alphanumeric values", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(signUpReferralField.getText().toString().trim().equals("")) && signUpReferralField.getText().toString().trim().length() < 5 || signUpUsernameField.getText().toString().trim().length() > 15) {
            signUpReferralIv.setImageResource(R.drawable.new_theme_input_border);
            signUpReferralIv.setColorFilter(ContextCompat.getColor(SignUp.this,R.color.colorRed), PorterDuff.Mode.SRC_IN);
            signUpReferralIv.startAnimation(animShake);
            Toast.makeText(this, "Referral length should be between 5 and 15", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            signUpReferralIv.setImageResource(R.drawable.new_theme_input_border);
            ImageViewCompat.setImageTintList(signUpReferralIv,null);
        }

        if (gender == 0) {
            Toast.makeText(this, "Please select gender", Toast.LENGTH_SHORT).show();
            return false;
        } else if (tos == 0) {
            Toast.makeText(this, "Please agree to our terms and conditions", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    private void signupUser() throws JSONException {

        if (mSocket.connected()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", signUpUsernameField.getText().toString().trim());
            jsonObject.put("email", signUpEmailField.getText().toString().trim());
            jsonObject.put("password", signUpPasswordField.getText().toString().trim());
            jsonObject.put("referral", signUpReferralField.getText().toString().trim());

            if (gender == 1) {
                jsonObject.put("gender", "male");
            } else if (gender == 2) {
                jsonObject.put("gender", "female");
            }

            if (signUpWith.equals("Google")){
                jsonObject.put("social_google", facebookOrGoogleUserSocialId);
                jsonObject.put("social_facebook", "");
            }
            else if (signUpWith.equals("FaceBook")){
                jsonObject.put("social_google", "");
                jsonObject.put("social_facebook", facebookOrGoogleUserSocialId);
            }
            else {
                jsonObject.put("social_google", "");
                jsonObject.put("social_facebook", "");
            }
            jsonObject.put("signup_name", signup_Fullname);

            mSocket.emit("signup", jsonObject);

            mSocket.on("signUpError", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    SignUp.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject jsonObject = (JSONObject) args[0];

 //                           CustomDialogError customDialogError = new CustomDialogError();
                            try {
                                signUpProgressRl.setVisibility(View.GONE);
                                MySocketsClass.getInstance().showErrorDialog(SignUp.this, "", jsonObject.getString("errorMsg"), "signupError", "");
                                mSocket.off("signUpError");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.w("signup", "Error: " + jsonObject.toString());
                        }

                    });
                }
            });

            mSocket.on("signUpResponse", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    SignUp.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject jsonObject = (JSONObject) args[0];

 //                           CustomDialogError customDialogError = new CustomDialogError();
                            try {
                                if (jsonObject.get("success").equals("1"))
                                    signUpProgressRl.setVisibility(View.GONE);

                                MySocketsClass.getInstance().showErrorDialog(SignUp.this, "", jsonObject.getString("message"), "signupSuccess", "");
                                mSocket.off("signUpResponse");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.w("signup", "response: " + jsonObject.toString());
                        }

                    });
                }
            });
        }
    }

    private void PasswordVisibility(){
        if (PsdVisibiliyy==0)
        {
            PasswrdVisibility.setBackgroundResource(R.drawable.paswrd_visibility_off);
            signUpPasswordField.setTransformationMethod(null);
            PsdVisibiliyy=1;
        }
        else {
            PasswrdVisibility.setBackgroundResource(R.drawable.paswrd_visibility);
            signUpPasswordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
            PsdVisibiliyy=0;
        }
    }
}
