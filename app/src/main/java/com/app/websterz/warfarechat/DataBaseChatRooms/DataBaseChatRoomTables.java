package com.app.websterz.warfarechat.DataBaseChatRooms;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "chat_room_table")
public class DataBaseChatRoomTables {

    @PrimaryKey(autoGenerate = true)
    private int ChatRoomDbId;

    @ColumnInfo(name = "messanger_name")
    private String MessangerName;

    @ColumnInfo(name = "messanger_rank")
    private String MessangerRank;

    @ColumnInfo(name = "messanger_gender")
    private String MessangerGender;

    @ColumnInfo(name = "messanger_age")
    private String MessangerAge;

    @ColumnInfo(name = "messanger_message")
    private String MessangerMessage;

    @ColumnInfo(name = "messanger_date")
    private String MessangerDate;

    @ColumnInfo(name = "messanger_dp")
    private String MessangerDp;

    @ColumnInfo(name = "chat_room_name")
    private String MessangerChatRoomName;









    public int getChatRoomDbId() {
        return ChatRoomDbId;
    }

    public String getMessangerName() {
        return MessangerName;
    }

    public String getMessangerRank() {
        return MessangerRank;
    }

    public String getMessangerGender() {
        return MessangerGender;
    }

    public String getMessangerAge() {
        return MessangerAge;
    }

    public String getMessangerMessage() {
        return MessangerMessage;
    }

    public String getMessangerDate() {
        return MessangerDate;
    }

    public String getMessangerDp() {
        return MessangerDp;
    }

    public String getMessangerChatRoomName() {
        return MessangerChatRoomName;
    }








    public void setChatRoomDbId(int chatRoomDbId) {
        ChatRoomDbId = chatRoomDbId;
    }

    public void setMessangerName(String messangerName) {
        MessangerName = messangerName;
    }

    public void setMessangerRank(String messangerRank) {
        MessangerRank = messangerRank;
    }

    public void setMessangerGender(String messangerGender) {
        MessangerGender = messangerGender;
    }

    public void setMessangerAge(String messangerAge) {
        MessangerAge = messangerAge;
    }

    public void setMessangerMessage(String messangerMessage) {
        MessangerMessage = messangerMessage;
    }

    public void setMessangerDate(String messangerDate) {
        MessangerDate = messangerDate;
    }

    public void setMessangerDp(String messangerDp) {
        MessangerDp = messangerDp;
    }

    public void setMessangerChatRoomName(String messangerChatRoomName) {
        MessangerChatRoomName = messangerChatRoomName;
    }
}
