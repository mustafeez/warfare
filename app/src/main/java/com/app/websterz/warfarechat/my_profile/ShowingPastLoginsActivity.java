package com.app.websterz.warfarechat.my_profile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.my_profile.LastLoginAdapters.LastLoginsAdapter;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;

import java.util.ArrayList;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;

public class ShowingPastLoginsActivity extends AppCompatActivity {

    RelativeLayout parentLinearLayout;
    RecyclerView pastLoginsRecyclerview;
    ArrayList<String>loginArraylist=new ArrayList<>();
    AvatarView avatarLoader;
    MySingleTon mySingleTon;
    CircleImageView users_dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showing_past_logins);
        initializations();
        myCode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = ShowingPastLoginsActivity.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
    }

    private void initializations(){
        mySingleTon=MySingleTon.getInstance();
        parentLinearLayout=findViewById(R.id.parentLinearLayout);
        pastLoginsRecyclerview=findViewById(R.id.pastLoginsRecyclerview);
        avatarLoader = findViewById(R.id.MyProfileLoader);
        users_dp = findViewById(R.id.users_dp);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ShowingPastLoginsActivity.this, RecyclerView.VERTICAL, false);
        pastLoginsRecyclerview.setLayoutManager(layoutManager);
    }

    private void myCode(){
        loginArraylist = (ArrayList<String>) getIntent().getSerializableExtra("myLoginlist");
        pastLoginsRecyclerview.setAdapter(new LastLoginsAdapter(loginArraylist));
        avatarLoader.setVisibility(View.VISIBLE);
        avatarLoader.setAnimating(true);
        GlideApp.with(ShowingPastLoginsActivity.this).load(baseUrl + mySingleTon.dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                avatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(users_dp);
    }


}
