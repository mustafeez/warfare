package com.app.websterz.warfarechat.Rest;

import com.app.websterz.warfarechat.Models.UserPricateChatImageUploading;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.MultiPartsClassesForWallImagePost.WallImagePostModelClass;
import com.app.websterz.warfarechat.my_profile.ProfileImage;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @Multipart
    @POST("index.php")
    Call<ProfileImage> uploadImage(
            @Part List<MultipartBody.Part> image,
            @Part("username") RequestBody username,
            @Part("case") RequestBody upload_dp,
            @Part("socketID") RequestBody socketID
    );

    @Multipart
    @POST("index.php")
    Call<UserPricateChatImageUploading> privateChatImageSending(
            @Part("sender") RequestBody sender,
            @Part("receiver") RequestBody receiver,
            @Part List<MultipartBody.Part> image,
            @Part("socketID") RequestBody socketID,
            @Part("case") RequestBody case_
    );

    @Multipart
    @POST("index.php")
    Call<WallImagePostModelClass> UserWallImageUploading(
            @Part List<MultipartBody.Part> MyWallImage,
            @Part("case") RequestBody WallImagePostCase,
            @Part("sender")RequestBody WallImagePostSender,
            @Part("socketID")RequestBody WallImagePostSenderSocketID
            );
}
