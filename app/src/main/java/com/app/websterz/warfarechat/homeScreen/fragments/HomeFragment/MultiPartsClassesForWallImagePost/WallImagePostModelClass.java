package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.MultiPartsClassesForWallImagePost;

import com.google.gson.annotations.SerializedName;

public class WallImagePostModelClass {

    @SerializedName("success")
    private int success;

    @SerializedName("message")
    private String message;

    @SerializedName("url")
    private String url;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
