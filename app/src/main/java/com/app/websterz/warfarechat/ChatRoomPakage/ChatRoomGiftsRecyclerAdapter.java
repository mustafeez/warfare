package com.app.websterz.warfarechat.ChatRoomPakage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ChatRoomGiftsRecyclerAdapter extends RecyclerView.Adapter<ChatRoomGiftsRecyclerAdapter.ViewHolder> {

    ArrayList<ChatRoomGiftsModelClass> GiftDetailList=new ArrayList<ChatRoomGiftsModelClass>();
    String ImageUrlPath="images/gifts/";
    RecyclerView myRecyclerviewView;
    String All_chatroom_username,All_chatroom_role,All_chatroom_age,All_chatroom_gender;
    ArrayList<String>userslistInChatRoom=new ArrayList<>();
    Context MyContext;
    String GiftPopupMainMessage,GiftPopupTotalCoins,GiftPopupImage,FriendNameToGiftSend,GiftPopupGiftName,ChatroomCountryName,GiftPopupGiftNametoSend;

    public ChatRoomGiftsRecyclerAdapter(ArrayList<ChatRoomGiftsModelClass>giftDetailList, RecyclerView recyclerView, Context context,String country) {
        GiftDetailList=giftDetailList;
        myRecyclerviewView=recyclerView;
        MyContext=context;
        this.ChatroomCountryName=country;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gifts_row_adapter_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.MyAvatarLoader.setVisibility(View.VISIBLE);
        holder.MyAvatarLoader.setAnimating(true);
        holder.GiftsPrice.setText(GiftDetailList.get(position).getCost());
        holder.GiftsName.setText(GiftDetailList.get(position).getName());
//        Picasso.get().load(baseUrl+ImageUrlPath+GiftDetailList.get(position).getImgurl()).into(holder.GiftsImage, new Callback() {
//            @Override
//            public void onSuccess() {
//                holder.MyAvatarLoader.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError(Exception e) {
//
//            }
//        });

        GlideApp.with(holder.itemView.getContext()).load(baseUrl + ImageUrlPath+GiftDetailList.get(position).getImgurl()).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.MyAvatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).
                into(holder.GiftsImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GiftPopupMainMessage="Send "+GiftDetailList.get(position).getName()+" to "+GiftDetailList.get(position).getFriendName();
                GiftPopupTotalCoins=GiftDetailList.get(position).getCost();
                GiftPopupImage=GiftDetailList.get(position).getImgurl();
                GiftPopupGiftName=GiftDetailList.get(position).getName();
                GiftPopupGiftNametoSend=GiftDetailList.get(position).getCommand();
                JSONObject jsonObject=new JSONObject();
                try {
                    jsonObject.put("chatroom",ChatroomCountryName);
                    mSocket.emit(MySocketsClass.EmmittersClass.GET_ROOM_USERS,jsonObject);
                    mSocket.on(MySocketsClass.ListenersClass.GET_ROOM_USERS_RESPONSE,getting_room_users_respomse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    Emitter.Listener getting_room_users_respomse=new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            Log.e("listeners","testing");

            ((Activity) MyContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    try {
                        String chatRoomName=jsonObject.getString("chatroom");
                        userslistInChatRoom.clear();
                        for(int i =0; i < jsonObject.getJSONArray("usersList").length(); i++){
                            All_chatroom_username  = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("username");
                            userslistInChatRoom.add(All_chatroom_username);
                            All_chatroom_role  = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("role");
                            All_chatroom_age  = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("age");
                            All_chatroom_gender  = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("gender");
                        }
                        TextView MainMessage,TotalCoins,emptyChatRoom;
                        Button SendGift,Close;
                        ImageView GiftImage;
                        final Spinner username_Dropdown;

                        final Dialog MyDialog=new Dialog(MyContext);
                        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        //crashing in here
                        MyDialog.setContentView(R.layout.gifts_popup);
                        MyDialog.setCancelable(true);

                            MyDialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                        MainMessage=MyDialog.findViewById(R.id.message_text);
                        TotalCoins=MyDialog.findViewById(R.id.total_coins);
                        SendGift=MyDialog.findViewById(R.id.send_gift);
                        Close=MyDialog.findViewById(R.id.close);
                        emptyChatRoom=MyDialog.findViewById(R.id.emptyChatRoom);
                        username_Dropdown=MyDialog.findViewById(R.id.username_Dropdown);
                        if (userslistInChatRoom.size()>0) {
                            username_Dropdown.setVisibility(View.VISIBLE);
                        }
                        else {
                            emptyChatRoom.setVisibility(View.VISIBLE);
                        }
                        GiftImage=MyDialog.findViewById(R.id.gift_image);

                        MainMessage.setText(GiftPopupMainMessage);
                        TotalCoins.setText(GiftPopupTotalCoins);
                        Glide.with(MyContext).load(baseUrl+ImageUrlPath+GiftPopupImage).into(GiftImage);

                        ArrayAdapter<String> arrayAdapter;
                        arrayAdapter = new ArrayAdapter<>(MyDialog.getContext(),R.layout.simple_spinner_item_custom,userslistInChatRoom);

                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        username_Dropdown.setAdapter(arrayAdapter);

                        username_Dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                FriendNameToGiftSend = username_Dropdown.getSelectedItem().toString();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                Close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MyDialog.dismiss();
                    }
                });
                SendGift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JSONObject jsonObject=new JSONObject();
                        try {
                            jsonObject.put("username",ChatroomCountryName);
                            jsonObject.put("gift",GiftPopupGiftNametoSend);
                            jsonObject.put("type","chatroom");
                            jsonObject.put("gift_username",FriendNameToGiftSend);
                            mSocket.emit(MySocketsClass.EmmittersClass.SEND_GIFT,jsonObject);
                            mSocket.on(MySocketsClass.ListenersClass.SEND_GIFT_RESPONSE,send_gift_response);
                            MyDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                        if(!((Activity) MyContext).isFinishing()) {
                            MyDialog.show();
                        }
                        mSocket.off(MySocketsClass.ListenersClass.GET_ROOM_USERS_RESPONSE);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }
    };
    Emitter.Listener send_gift_response=new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject=(JSONObject)args[0];
            try {
                String message=jsonObject.getString("message");
                String success=jsonObject.getString("success");
                if (success.equals("1"))
                {
                    Snackbar.make(myRecyclerviewView,message,Snackbar.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public int getItemCount() {

        return GiftDetailList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView GiftsImage;
        TextView GiftsName,GiftsPrice;
        AvatarView MyAvatarLoader;
        public ViewHolder(View itemView) {
            super(itemView);
            GiftsImage=itemView.findViewById(R.id.gift_img);
            GiftsName=itemView.findViewById(R.id.gift_name);
            GiftsPrice=itemView.findViewById(R.id.gifts_price);
            MyAvatarLoader=itemView.findViewById(R.id.MyAvatarLoader);
        }
    }
}
