package com.app.websterz.warfarechat.my_profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.emojione.tools.Client;

public class AboutProfileActivity extends AppCompatActivity {
    RelativeLayout parentLinearLayout;
    String aboutTextString;
    private TextView abtTextview;
    Client MyClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_profile);
        initializations();
        myCode();
    }
    private void initializations(){
        parentLinearLayout=findViewById(R.id.parentLinearLayout);
        abtTextview=findViewById(R.id.abtTextview);
        MyClient = new Client(AboutProfileActivity.this);
    }

    private void myCode(){
        Intent intent=getIntent();
        aboutTextString=intent.getStringExtra("AboutText");
        abtTextview.setText(MyClient.shortnameToUnicode(aboutTextString));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = AboutProfileActivity.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
    }
}