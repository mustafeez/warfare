package com.app.websterz.warfarechat.ChatRoomPakage;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ChatRoomsRecyclerChatAdapter extends RecyclerView.Adapter<ChatRoomsRecyclerChatAdapter.ViewHolder> {
    ArrayList<ChatRoomChatModelClass>chatRoomMessagesList=new ArrayList<>();
    String myName,MsgsDate,MyCoinsGrabberChatRoom;
    MySingleTon mySingleTon= MySingleTon.getInstance();


    public ChatRoomsRecyclerChatAdapter(ArrayList<ChatRoomChatModelClass> chatRoomMessagesList) {
        this.chatRoomMessagesList=chatRoomMessagesList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatroom_chat_row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(viewHolder.itemView.getContext());
        myName = prefs.getString("mine_user_name", null);
        MsgsDate= DateConvertor(chatRoomMessagesList.get(i).getDate());

            if (chatRoomMessagesList.get(i).getSender().equals(myName))
            {
                //if sender is me...
                viewHolder.chatroom_system_messageText.setVisibility(View.GONE);
                viewHolder.chatroom_left_msg_layout.setVisibility(View.GONE);
                viewHolder.chatroom_main_system_messageText.setVisibility(View.GONE);
                //Sender Avatar Loader
                Glide.with(viewHolder.itemView.getContext()).load(baseUrl + mySingleTon.dp).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        viewHolder.SenderAvatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewHolder.SenderAvatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).into(viewHolder.chatroom_image_sender);

                viewHolder.RecieverRankAvatar.setVisibility(View.VISIBLE);
                viewHolder.RecieverRankAvatar.setAnimating(true);
                if (chatRoomMessagesList.get(i).getRole().equals("1")) {

                    Glide.with(viewHolder.itemView.getContext()).load(R.drawable.user_icon).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(viewHolder.my_text_rank_image);

                    viewHolder.my_text_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorBLue));
                } else if (chatRoomMessagesList.get(i).getRole().equals("2")) {

                    Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mod_icon).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(viewHolder.my_text_rank_image);
                    viewHolder.my_text_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorRed));
                } else if (chatRoomMessagesList.get(i).getRole().equals("3")) {
                    Glide.with(viewHolder.itemView.getContext()).load(R.drawable.staff_icon).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(viewHolder.my_text_rank_image);
                    viewHolder.my_text_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorOrange));
                } else if (chatRoomMessagesList.get(i).getRole().equals("4")) {

                    Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mentor_icon).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(viewHolder.my_text_rank_image);
                    viewHolder.my_text_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorGreen));
                } else if (chatRoomMessagesList.get(i).getRole().equals("5")) {
                    Glide.with(viewHolder.itemView.getContext()).load(R.drawable.merchant_icon).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.RecieverRankAvatar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(viewHolder.my_text_rank_image);
                    viewHolder.my_text_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.violet));
                }
                else
                {
                    viewHolder.my_text_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorBLue));
                }
                viewHolder.my_text_username.setText(chatRoomMessagesList.get(i).getSender());

                viewHolder.RecieverAgeAvatar.setVisibility(View.VISIBLE);
                viewHolder.RecieverAgeAvatar.setAnimating(true);
                if (chatRoomMessagesList.get(i).getAge().equals("1")) {
                    if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_male).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    } else {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_female).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    }
                } else if (chatRoomMessagesList.get(i).getAge().equals("2")) {
                    if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_male).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    } else {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_female).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    }
                } else if (chatRoomMessagesList.get(i).getAge().equals("3")) {
                    if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_male).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    } else {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_female).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    }
                } else if (chatRoomMessagesList.get(i).getAge().equals("4")) {
                    if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_male).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    } else {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_female).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    }
                } else if (chatRoomMessagesList.get(i).getAge().equals("5")) {
                    if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_male).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    } else {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_female).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.RecieverAgeAvatar.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.my_text_age_image);
                    }
                }

                viewHolder.chatroom_chat_right_msg_text_view.setText(chatRoomMessagesList.get(i).getChatMessage());
                viewHolder.chatroom_right_msg_date_text_view.setText(MsgsDate);

            }
            else {
                if (chatRoomMessagesList.get(i).getChatMessage().equalsIgnoreCase("has left")||
                        chatRoomMessagesList.get(i).getChatMessage().equalsIgnoreCase("has joined")||
                        chatRoomMessagesList.get(i).getChatMessage().equalsIgnoreCase("disconnected")) {
                    //System related msg
                    viewHolder.chatroom_system_messageText.setVisibility(View.VISIBLE);
                    viewHolder.chatroom_left_msg_layout.setVisibility(View.GONE);
                    viewHolder.chatroom_right_msg_layout.setVisibility(View.GONE);
                    viewHolder.chatroom_main_system_messageText.setVisibility(View.GONE);

                    viewHolder.SystemMsg1AvatarLoader.setVisibility(View.VISIBLE);
                    viewHolder.SystemMsg1AvatarLoader.setAnimating(true);
                    viewHolder.system_message_text1.setText(chatRoomMessagesList.get(i).getSender());
                    if (chatRoomMessagesList.get(i).getRole().equals("1")) {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.user_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.system_msg_image1);
                    } else if (chatRoomMessagesList.get(i).getRole().equals("2")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mod_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.system_msg_image1);
                    } else if (chatRoomMessagesList.get(i).getRole().equals("3")) {

                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.staff_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.system_msg_image1);
                    } else if (chatRoomMessagesList.get(i).getRole().equals("4")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mentor_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.system_msg_image1);
                    } else if (chatRoomMessagesList.get(i).getRole().equals("5")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.merchant_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SystemMsg1AvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.system_msg_image1);
                    }
                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.VISIBLE);
                    viewHolder.SystemMsg2AvatarLoader.setAnimating(true);
                    if (chatRoomMessagesList.get(i).getAge().equals("1")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("2")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        } else {

                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("3")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("4")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);

                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("5")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {

                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SystemMsg2AvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.system_msg_image2);
                        }
                    }
                    viewHolder.system_message_text2.setText(chatRoomMessagesList.get(i).getChatMessage());
                }
                else if (chatRoomMessagesList.get(i).getSender().equals("system_msgs"))
                {
                    // if system msg listener works then this system msg...
                    viewHolder.chatroom_system_messageText.setVisibility(View.GONE);
                    viewHolder.chatroom_left_msg_layout.setVisibility(View.GONE);
                    viewHolder.chatroom_right_msg_layout.setVisibility(View.GONE);
                    MyCoinsGrabberChatRoom=chatRoomMessagesList.get(i).getChatroomName();
                    if (chatRoomMessagesList.get(i).getGender().equals("coins"))
                    {
                        viewHolder.coins_catcher_button.setVisibility(View.VISIBLE);
                    }
                    viewHolder.main_system_message_text.setText(chatRoomMessagesList.get(i).getChatMessage());


                }
                // if sender is someone else
                else {
                    viewHolder.chatroom_system_messageText.setVisibility(View.GONE);
                    viewHolder.chatroom_right_msg_layout.setVisibility(View.GONE);
                    viewHolder.chatroom_main_system_messageText.setVisibility(View.GONE);
                    //ReceiverAvatarLoader
                    viewHolder.RecieverAvatarLoader.setVisibility(View.VISIBLE);
                    viewHolder.RecieverAvatarLoader.setAnimating(true);
                    Glide.with(viewHolder.itemView.getContext()).load(baseUrl + mySingleTon.dp).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.RecieverAvatarLoader.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.RecieverAvatarLoader.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(viewHolder.chatroom_image_receiver);

                    //Sender Avatar Loader
                    viewHolder.SenderRankAvatarLoader.setVisibility(View.VISIBLE);
                    viewHolder.SenderRankAvatarLoader.setAnimating(true);
                    if (chatRoomMessagesList.get(i).getRole().equals("1")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.user_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.mesanger_rank_image);
                        viewHolder.messanger_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorBLue));
                    } else if (chatRoomMessagesList.get(i).getRole().equals("2")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mod_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.mesanger_rank_image);
                        viewHolder.messanger_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorRed));
                    } else if (chatRoomMessagesList.get(i).getRole().equals("3")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.staff_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.mesanger_rank_image);
                        viewHolder.messanger_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorOrange));
                    } else if (chatRoomMessagesList.get(i).getRole().equals("4")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mentor_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.mesanger_rank_image);
                        viewHolder.messanger_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorGreen));
                    } else if (chatRoomMessagesList.get(i).getRole().equals("5")) {
                        Glide.with(viewHolder.itemView.getContext()).load(R.drawable.merchant_icon).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                viewHolder.SenderRankAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(viewHolder.mesanger_rank_image);
                        viewHolder.messanger_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.violet));
                    } else {
                        viewHolder.messanger_username.setTextColor(viewHolder.itemView.getResources().getColor(R.color.colorBLue));
                    }
                    viewHolder.messanger_username.setText(chatRoomMessagesList.get(i).getSender());

                    //Sender Age Loader
                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.VISIBLE);
                    viewHolder.SenderAgeAvatarLoader.setAnimating(true);
                    if (chatRoomMessagesList.get(i).getAge().equals("1")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.born_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("2")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.teen_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("3")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.young_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("4")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.adult_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        }
                    } else if (chatRoomMessagesList.get(i).getAge().equals("5")) {
                        if (chatRoomMessagesList.get(i).getGender().equals("M")) {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_male).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        } else {
                            Glide.with(viewHolder.itemView.getContext()).load(R.drawable.mature_female).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.SenderAgeAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(viewHolder.mesanger_age_image);
                        }
                    }

                    viewHolder.chatroom_chat_left_msg_text_view.setText(chatRoomMessagesList.get(i).getChatMessage());
                    viewHolder.chatroom_left_msg_date_text_view.setText(MsgsDate);
                }
            }

            viewHolder.coins_catcher_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject jsonObject=new JSONObject();
                    try {
                        jsonObject.put("chatroom",MyCoinsGrabberChatRoom);
                        mSocket.emit(MySocketsClass.EmmittersClass.GRAB_COINS,jsonObject);
//                        viewHolder.coins_catcher_button.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

    }

    @Override
    public int getItemCount() {
        return chatRoomMessagesList.size();
    }

    public String DateConvertor(String date) {
        String MyFinalDate;
        long time = Long.parseLong(date);
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String MyDate = DateFormat.format("EEE d, MMM yyyy hh:mm a", cal).toString();
        String today_time = DateFormat.format("hh:mm a", cal).toString();
        String date2 = DateFormat.format("yyyy / MM / d", cal).toString();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / d");
        String current_date = mdformat.format(calendar.getTime());

        if (current_date.equals(date2)){
            MyFinalDate=today_time;}
        else {
            MyFinalDate=MyDate;}
        return MyFinalDate;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatroom_left_msg_layout,chatroom_right_msg_layout,chatroom_system_messageText,chatroom_main_system_messageText;
        CircleImageView chatroom_image_receiver,chatroom_image_sender;

        TextView messanger_username,chatroom_chat_left_msg_text_view,chatroom_left_msg_date_text_view,main_system_message_text
        ,chatroom_right_msg_date_text_view,my_text_username,chatroom_chat_right_msg_text_view,system_message_text1,system_message_text2;
        ImageView mesanger_rank_image,mesanger_age_image,my_text_rank_image,my_text_age_image,system_msg_image1,system_msg_image2;

        Button coins_catcher_button;
        AvatarView SenderAvatarLoader, SenderRankAvatarLoader,SenderAgeAvatarLoader,
                   RecieverAvatarLoader,RecieverRankAvatar,RecieverAgeAvatar, SystemMsg1AvatarLoader, SystemMsg2AvatarLoader;

        public ViewHolder(View itemView) {
            super(itemView);

            SystemMsg1AvatarLoader = itemView.findViewById(R.id.SystemMsgAvatarLoader);
            SystemMsg2AvatarLoader = itemView.findViewById(R.id.SystemMsg2AvatarLoader);

            RecieverAvatarLoader = itemView.findViewById(R.id.ChatRoomReceiverAvatarLoader);
            RecieverRankAvatar = itemView.findViewById(R.id.SenderRankAvatarLoader);
            RecieverAgeAvatar = itemView.findViewById(R.id.SenderAgeAvatarLoader);

            SenderAvatarLoader = itemView.findViewById(R.id.SenderAvatarLoader);
            SenderRankAvatarLoader = itemView.findViewById(R.id.MessengerRankAvatarLoader);
            SenderAgeAvatarLoader = itemView.findViewById(R.id.MessengerAgeAvatarLoader);

            chatroom_left_msg_layout=itemView.findViewById(R.id.chatroom_left_msg_layout);
            chatroom_right_msg_layout=itemView.findViewById(R.id.chatroom_right_msg_layout);
            chatroom_system_messageText=itemView.findViewById(R.id.chatroom_system_messageText);
            chatroom_main_system_messageText=itemView.findViewById(R.id.chatroom_main_system_messageText);

            chatroom_image_receiver=itemView.findViewById(R.id.chatroom_image_receiver);
            chatroom_image_sender=itemView.findViewById(R.id.chatroom_image_sender);

            messanger_username=itemView.findViewById(R.id.messanger_username);
            chatroom_chat_left_msg_text_view=itemView.findViewById(R.id.chatroom_chat_left_msg_text_view);
            chatroom_left_msg_date_text_view=itemView.findViewById(R.id.chatroom_left_msg_date_text_view);
            chatroom_right_msg_date_text_view=itemView.findViewById(R.id.chatroom_right_msg_date_text_view);
            my_text_username=itemView.findViewById(R.id.my_text_username);
            chatroom_chat_right_msg_text_view=itemView.findViewById(R.id.chatroom_chat_right_msg_text_view);
            system_message_text1=itemView.findViewById(R.id.system_message_text1);
            system_message_text2=itemView.findViewById(R.id.system_message_text2);
            main_system_message_text=itemView.findViewById(R.id.main_system_message_text);

            mesanger_rank_image=itemView.findViewById(R.id.mesanger_rank_image);
            mesanger_age_image=itemView.findViewById(R.id.mesanger_age_image);
            my_text_rank_image=itemView.findViewById(R.id.my_text_rank_image);
            my_text_age_image=itemView.findViewById(R.id.my_text_age_image);
            system_msg_image1=itemView.findViewById(R.id.system_msg_image1);
            system_msg_image2=itemView.findViewById(R.id.system_msg_image2);

            coins_catcher_button=itemView.findViewById(R.id.coins_catcher_button);
        }
    }
}