package com.app.websterz.warfarechat.homeScreen.fragments.contacts.models;

/**
 * Created by sunny on 2/1/2019.
 */

public class DemoModel {

    private String name, status,dp;

    public DemoModel() {
    }

    public DemoModel(String name, String status, String dp) {
        this.name = name;
        this.status = status;
        this.dp = dp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBitmap() {
        return dp;
    }

    public void setBitmap(String dp) {
        this.dp= dp;
    }
}
