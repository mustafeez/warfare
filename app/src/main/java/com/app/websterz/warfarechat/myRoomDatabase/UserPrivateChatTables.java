package com.app.websterz.warfarechat.myRoomDatabase;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_chats")
public class UserPrivateChatTables {

    @PrimaryKey(autoGenerate = true)
    private int Id;

    @ColumnInfo(name = "chat_with")
    private String ChatWith;

    @ColumnInfo(name = "chat_message")
    private String ChatMessage;

    @ColumnInfo(name = "chat_message_type")
    private String ChatMessageType;

    @ColumnInfo(name = "chat_message_user_dp")
    private String MessageUserDp;

    @ColumnInfo(name = "chat_message_date")
    private String MessageDate;

    @ColumnInfo(name = "chat_status")
    private String ChatStatus;



    public int getId() {
        return Id;
    }

    public String getChatWith() {
        return ChatWith;
    }

    public String getChatMessage() {
        return ChatMessage;
    }

    public String getChatMessageType() {
        return ChatMessageType;
    }

    public String getMessageUserDp() {
        return MessageUserDp;
    }

    public String getMessageDate() {
        return MessageDate;
    }

    public String getChatStatus() {
        return ChatStatus;
    }


    public void setId(int id) {
        Id = id;
    }

    public void setChatWith(String chatWith) {
        ChatWith = chatWith;
    }

    public void setChatMessage(String chatMessage) {
        ChatMessage = chatMessage;
    }

    public void setChatMessageType(String chatMessageType) {
        ChatMessageType = chatMessageType;
    }

    public void setMessageUserDp(String messageUserDp) {
        MessageUserDp = messageUserDp;
    }

    public void setMessageDate(String messageDate) {
        MessageDate = messageDate;
    }

    public void setChatStatus(String chatStatus) {
        ChatStatus = chatStatus;
    }

}
