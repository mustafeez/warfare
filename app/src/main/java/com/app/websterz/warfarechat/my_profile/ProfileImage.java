package com.app.websterz.warfarechat.my_profile;

import com.google.gson.annotations.SerializedName;

public class ProfileImage {

    @SerializedName("image")
    private String image;

    @SerializedName("socketID")
    private String socketID;

    @SerializedName("username")
    private String username;

    @SerializedName("message")
    private String message;

    @SerializedName("success")
    private int success;

    @SerializedName("url")
    private String url;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSocketID() {
        return socketID;
    }

    public void setSocketID(String socketID) {
        this.socketID = socketID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
