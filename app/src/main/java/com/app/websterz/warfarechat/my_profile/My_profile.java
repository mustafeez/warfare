package com.app.websterz.warfarechat.my_profile;

import com.app.websterz.warfarechat.activities.EditMyProfileActivity;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.coin_details.Coins_details;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.UserWallLayoutModelClass;
import com.app.websterz.warfarechat.landingScreen.MainActivity;
import com.app.websterz.warfarechat.my_profile.FootPrintAdapter.ViewingFootPrintAdapter;
import com.app.websterz.warfarechat.my_profile.LastLoginAdapters.LastLoginsAdapter;
import com.app.websterz.warfarechat.my_profile.LikeAndGiftAdapter.LikeGiftAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.emojione.tools.Client;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class My_profile extends AppCompatActivity {
    Button editProfileBtn;
    TextView user_gender, user_country_name, user_footprints, user_heart_likes, userName, userStatus, userAccountName,
            roleText, user_gifts, user_registered_info, users_country_imgs, user_Followers, user_Shouts, user_Friends;
    LinearLayout returning_imageview = null;
    ImageView users_age, reoleIcon;
    CircleImageView users_dp;
    Client client;
    private ImageView filtersButton, topGreenView;
    String name, email, SoulmateValue, FriendValue, GameplayerValue, userAcountNameText,
            statusMsg, social_google, social_faceBook, aboutMeValue, dp;
    long current_time;

    LinearLayout parentlayout;
    RelativeLayout parentLinearLayout;
    AvatarView avatarLoader, countryAvatar, ageAavatar;
    LinearLayout FootprintLayout, LikesLayout, GiftsLayout, followersLayout, friendsLayout, shoutsLayout;
    JSONArray jsonArray;
    ArrayList<String> lastLoginArraylis = new ArrayList<>();
    private ImageView footPrintImageView, giftsImageView, heartImageView, followersImageView, shoutsImageView, friendsImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        initializations();

        filtersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showingPastLogins();
            }
        });
        editProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openingEditProfileActiviy();
            }
        });
        users_dp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dp != null) {
                    MySocketsClass.getInstance().openingZoomingActivity(My_profile.this,baseUrl+dp,"profile");
                }
            }
        });
    }

    private void initializations() {
        editProfileBtn = findViewById(R.id.editProfileBtn);
        footPrintImageView = findViewById(R.id.footPrintImageView);
        giftsImageView = findViewById(R.id.giftsImageView);
        heartImageView = findViewById(R.id.heartImageView);
        followersImageView = findViewById(R.id.followersImageView);
        shoutsImageView = findViewById(R.id.shoutsImageView);
        friendsImageView = findViewById(R.id.friendsImageView);

        avatarLoader = findViewById(R.id.MyProfileLoader);
        ageAavatar = findViewById(R.id.AgeAvatarLoader);
        countryAvatar = findViewById(R.id.CountryAvatarLoader);
        current_time = System.currentTimeMillis();

        client = new Client(My_profile.this);
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        parentlayout = findViewById(R.id.parentlayout);
        userName = findViewById(R.id.userName);
        userAccountName = findViewById(R.id.userAccountName);
        userStatus = findViewById(R.id.userStatus);
        roleText = findViewById(R.id.roleText);
        filtersButton = findViewById(R.id.filtersButton);
        topGreenView = findViewById(R.id.topGreenView);
        if (!My_profile.this.isFinishing()) {
            Glide.with(My_profile.this).load(R.drawable.green_background_top).into(topGreenView);
            Glide.with(My_profile.this).load(R.drawable.green_icon_footprint).into(footPrintImageView);
            Glide.with(My_profile.this).load(R.drawable.green_icon_gift).into(giftsImageView);
            Glide.with(My_profile.this).load(R.drawable.green_icon_heart).into(heartImageView);
            Glide.with(My_profile.this).load(R.drawable.green_icon_followers).into(followersImageView);
            Glide.with(My_profile.this).load(R.drawable.green_icon_shout).into(shoutsImageView);
            Glide.with(My_profile.this).load(R.drawable.green_user_filled).into(friendsImageView);
        }
        user_gender = findViewById(R.id.user_gender);
        user_country_name = findViewById(R.id.user_country_name);
        user_footprints = findViewById(R.id.user_footprints);
        user_heart_likes = findViewById(R.id.user_heart_likes);
        user_gifts = findViewById(R.id.user_gifts);
        user_Followers = findViewById(R.id.user_Followers);
        user_Shouts = findViewById(R.id.user_Shouts);
        user_Friends = findViewById(R.id.user_Friends);
        user_registered_info = findViewById(R.id.user_registered_info);

        users_age = findViewById(R.id.users_age);
        reoleIcon = findViewById(R.id.reoleIcon);
        users_country_imgs = findViewById(R.id.users_country_imgs);

        users_dp = findViewById(R.id.users_dp);
        FootprintLayout = findViewById(R.id.FootprintLayout);
        LikesLayout = findViewById(R.id.LikesLayout);
        GiftsLayout = findViewById(R.id.GiftsLayout);
        followersLayout = findViewById(R.id.followersLayout);
        friendsLayout = findViewById(R.id.friendsLayout);
        shoutsLayout = findViewById(R.id.shoutsLayout);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(My_profile.this, RecyclerView.VERTICAL, false);
    }

    private void initview() {
        mSocket.off(MySocketsClass.ListenersClass.GET_MY_PROFILE_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.GET_MY_PROFILE);
        returning_imageview = MySocketsClass.custom_layout.Loader_image(My_profile.this, parentLinearLayout);
        mSocket.on(MySocketsClass.ListenersClass.GET_MY_PROFILE_RESPONSE, get_my_profile_response);
    }

    private void openingEditProfileActiviy() {
        Intent intent = new Intent(My_profile.this, EditMyProfileActivity.class);
        intent.putExtra("GamePlayerValue", GameplayerValue);
        intent.putExtra("SoulMateValue", SoulmateValue);
        intent.putExtra("FriendValue", FriendValue);
        intent.putExtra("userFullName", name);
        intent.putExtra("userEmail", email);
        intent.putExtra("social_google", social_google);
        intent.putExtra("social_faceBook", social_faceBook);
        intent.putExtra("aboutMeValue", aboutMeValue);
        startActivity(intent);
    }

    private void showingPastLogins() {
        Intent intent = new Intent(My_profile.this, ShowingPastLoginsActivity.class);
        intent.putExtra("myLoginlist", lastLoginArraylis);
        startActivity(intent);
        Log.e("arytst", "araylist value " + lastLoginArraylis.size());
    }

    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    private String changingUnit(long number) {

        if (number < 1000) return Long.toString(number); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(number);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = number / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    private void ViewingFootPrints() {
        MainActivity.mSocket.off(MySocketsClass.ListenersClass.VIEW_FOOTPRINTS_RESPONSE);
        MainActivity.mSocket.emit(MySocketsClass.EmmittersClass.VIEW_FOOT_PRINTS);
        MainActivity.mSocket.on(MySocketsClass.ListenersClass.VIEW_FOOTPRINTS_RESPONSE, ViewingFootprintsResponseListener);
        SettingReturningViewGone();

    }

    private void ViewingLikes() {
        MainActivity.mSocket.off(MySocketsClass.ListenersClass.VIEW_LIKE_RESPONSE);
        MainActivity.mSocket.emit(MySocketsClass.EmmittersClass.VIEW_LIKES);
        MainActivity.mSocket.on(MySocketsClass.ListenersClass.VIEW_LIKE_RESPONSE, view_like_response);
        SettingReturningViewGone();
    }

    private void ViewingGifts() {
        MainActivity.mSocket.off(MySocketsClass.ListenersClass.VIEW_GIFTS_RESPONSE);
        MainActivity.mSocket.emit(MySocketsClass.EmmittersClass.VIEW_GIFTS);
        MainActivity.mSocket.on(MySocketsClass.ListenersClass.VIEW_GIFTS_RESPONSE, view_gift_response);
        SettingReturningViewGone();
    }

    private void ViewingFollowers() {
        MainActivity.mSocket.off(MySocketsClass.ListenersClass.VIEW_FOLLOWERS_RESPONSE);
        MainActivity.mSocket.emit(MySocketsClass.EmmittersClass.VIEW_FOLLOWERS);
        MainActivity.mSocket.on(MySocketsClass.ListenersClass.VIEW_FOLLOWERS_RESPONSE, view_followers_response);
        SettingReturningViewGone();
    }

    private void ViewingFriends() {
        MainActivity.mSocket.off(MySocketsClass.ListenersClass.VIEW_FriendS_RESPONSE);
        MainActivity.mSocket.emit(MySocketsClass.EmmittersClass.VIEW_FRIENDS);
        MainActivity.mSocket.on(MySocketsClass.ListenersClass.VIEW_FriendS_RESPONSE, view_friends_response);
        SettingReturningViewGone();
    }

    private void showingMineShoutsOnHomePage() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(My_profile.this);
        Intent intent = new Intent("ShowingMineShouts");
        localBroadcastManager.sendBroadcast(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initview();
        MySocketsClass.static_objects.MyStaticActivity = My_profile.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
        if (returning_imageview != null && returning_imageview.getVisibility() == View.VISIBLE) {
            returning_imageview.setVisibility(View.GONE);
        }
    }

    private void SettingReturningViewGone() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (returning_imageview != null && returning_imageview.getVisibility() == View.VISIBLE) {
                    returning_imageview.setVisibility(View.GONE);
                }
            }
        }, 1200);
    }

    Emitter.Listener ViewingFootprintsResponseListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            try {
                if (jsonObject.getString("success").equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(My_profile.this, ViewingFootprintsResponse.class);
                            intent.putExtra("JsonObject", jsonObject.toString());
                            startActivity(intent);
                            if (returning_imageview.getVisibility() == View.VISIBLE) {
                                returning_imageview.setVisibility(View.GONE);
                            }
                        }
                    });
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener view_like_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            try {
                if (jsonObject.getString("success").equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(My_profile.this, LikesAndGiftsViewingResponse.class);
                            intent.putExtra("likesGiftsJsonObject", jsonObject.toString());
                            intent.putExtra("SocketType", "Likes");
                            startActivity(intent);
                            if (returning_imageview.getVisibility() == View.VISIBLE) {
                                returning_imageview.setVisibility(View.GONE);
                            }
                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener view_gift_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];

            try {
                if (jsonObject.getString("success").equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(My_profile.this, LikesAndGiftsViewingResponse.class);
                            intent.putExtra("likesGiftsJsonObject", jsonObject.toString());
                            intent.putExtra("SocketType", "Gifts");
                            startActivity(intent);
                            if (returning_imageview.getVisibility() == View.VISIBLE) {
                                returning_imageview.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener view_followers_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];

            try {
                if (jsonObject.getString("success").equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(My_profile.this, LikesAndGiftsViewingResponse.class);
                            intent.putExtra("likesGiftsJsonObject", jsonObject.toString());
                            intent.putExtra("SocketType", "Followers");
                            startActivity(intent);
                            if (returning_imageview.getVisibility() == View.VISIBLE) {
                                returning_imageview.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener view_friends_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            Log.e("friendssJson", jsonObject.toString());

            try {
                if (jsonObject.getString("success").equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(My_profile.this, LikesAndGiftsViewingResponse.class);
                            intent.putExtra("likesGiftsJsonObject", jsonObject.toString());
                            intent.putExtra("SocketType", "Friends");
                            startActivity(intent);
                            if (returning_imageview.getVisibility() == View.VISIBLE) {
                                returning_imageview.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private Emitter.Listener get_my_profile_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String country_code = jsonObject.getJSONObject("country").getString("code");
                        String country_emoji = jsonObject.getJSONObject("country").getString("emoji");


                        String footprints = jsonObject.getString("footprints");
                        String gifts = jsonObject.getString("gifts");
                        String presence = jsonObject.getString("presence");
                        String likes = jsonObject.getString("likes");
                        String followers = jsonObject.getString("followers");
                        String shouts = jsonObject.getString("shouts");
                        String friends = jsonObject.getString("friends");

                        dp = jsonObject.getJSONObject("profile").getString("dp");
                        String age = jsonObject.getJSONObject("profile").getString("age");
                        String role = jsonObject.getJSONObject("profile").getString("role");
                        String gender = jsonObject.getJSONObject("profile").getString("gender");
                        long last_online_time = jsonObject.getJSONObject("profile").getLong("last_online");
                        long time_reg = jsonObject.getJSONObject("profile").getLong("time_reg");
                        name = jsonObject.getJSONObject("profile").getString("name");
                        statusMsg = jsonObject.getJSONObject("profile").getString("statusMsg");
                        userAcountNameText = jsonObject.getJSONObject("profile").getString("username");
                        email = jsonObject.getJSONObject("profile").getString("email");
                        SoulmateValue = jsonObject.getJSONObject("profile").getString("love");
                        FriendValue = jsonObject.getJSONObject("profile").getString("friend");
                        GameplayerValue = jsonObject.getJSONObject("profile").getString("game");
                        aboutMeValue = jsonObject.getJSONObject("profile").getString("about");
                        String login_ip = jsonObject.getJSONObject("profile").getString("login_ip");
                        social_google = jsonObject.getJSONObject("profile").getString("social_google");
                        social_faceBook = jsonObject.getJSONObject("profile").getString("social_facebook");

                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                        cal.setTimeInMillis((last_online_time) * 1000);
                        String last_online = DateFormat.format("EEE,d MMM yyyy 'at' hh:mm a", cal).toString();
                        cal.setTimeInMillis((time_reg) * 1000);
                        String time_resiter_date = DateFormat.format("EEE,d MMM yyyy 'at' hh:mm a", cal).toString();

                        //Add AvatarView Loader
                        avatarLoader.setVisibility(View.VISIBLE);
                        avatarLoader.setAnimating(true);
                        if (!My_profile.this.isFinishing()) {
                            Glide.with(My_profile.this).load(baseUrl + dp).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    avatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(users_dp);
                        }

                        user_country_name.setText(country_code);
                        countryAvatar.setVisibility(View.VISIBLE);
                        countryAvatar.setAnimating(true);

                        client.toImage(country_emoji, 50, new com.emojione.tools.Callback() {
                            @Override
                            public void onFailure(IOException e) {
                                countryAvatar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onSuccess(SpannableStringBuilder ssb) {
                                users_country_imgs.setText(ssb);
                                countryAvatar.setVisibility(View.GONE);

                            }
                        });

                        if (gender.equals("M")) {
                            user_gender.setText("Male");
                        } else {
                            user_gender.setText("Female");
                        }
                        ageAavatar.setVisibility(View.VISIBLE);
                        ageAavatar.setAnimating(true);

                        if (age.equals("1")) {
                            if (gender.equals("M")) {
                                if (!My_profile.this.isFinishing()) {
                                    Glide.with(My_profile.this).load(R.drawable.born_male).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                }
                            } else {
                                if (!My_profile.this.isFinishing()) {
                                    Glide.with(My_profile.this).load(R.drawable.born_female).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                }
                            }
                        } else if (age.equals("2")) {
                            if (gender.equals("M")) {
                                if (!My_profile.this.isFinishing()) {
                                    Glide.with(My_profile.this).load(R.drawable.teen_male).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                }
                            } else {
                                if (!My_profile.this.isFinishing()) {
                                    Glide.with(My_profile.this).load(R.drawable.teen_female).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                }
                            }
                        } else if (age.equals("3")) {
                            if (!My_profile.this.isFinishing()) {
                                if (gender.equals("M")) {
                                    Glide.with(My_profile.this).load(R.drawable.young_male).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                } else {
                                    Glide.with(My_profile.this).load(R.drawable.young_female).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                }
                            }
                        } else if (age.equals("4")) {
                            if (!My_profile.this.isFinishing()) {
                                if (gender.equals("M")) {
                                    Glide.with(My_profile.this).load(R.drawable.adult_male).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                } else {
                                    Glide.with(My_profile.this).load(R.drawable.adult_female).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                }
                            }
                        } else if (age.equals("5")) {
                            if (!My_profile.this.isFinishing()) {
                                if (gender.equals("M")) {
                                    Glide.with(My_profile.this).load(R.drawable.mature_male).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                } else {
                                    Glide.with(My_profile.this).load(R.drawable.mature_female).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            ageAavatar.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(users_age);
                                }
                            }
                        }
                        if (!My_profile.this.isFinishing()) {
                            if (role.equals("1")) {
                                Glide.with(My_profile.this).load(R.drawable.user_icon).into(reoleIcon);
                                roleText.setText("User");
                            } else if (role.equals("2")) {
                                Glide.with(My_profile.this).load(R.drawable.mod_icon).into(reoleIcon);
                                roleText.setText("Admin");
                            } else if (role.equals("3")) {
                                Glide.with(My_profile.this).load(R.drawable.staff_icon).into(reoleIcon);
                                roleText.setText("Staff");
                            } else if (role.equals("4")) {
                                Glide.with(My_profile.this).load(R.drawable.mentor_icon).into(reoleIcon);
                                roleText.setText("Mentor");
                            } else if (role.equals("5")) {
                                roleText.setText("Merchant");
                                Glide.with(My_profile.this).load(R.drawable.merchant_icon).into(reoleIcon);
                            }
                        }

                        userName.setText(name);
                        userAccountName.setText("@" + userAcountNameText);
                        userStatus.setText(statusMsg);
                        user_footprints.setText(changingUnit(Integer.parseInt(footprints)));
                        user_gifts.setText(changingUnit(Integer.parseInt(gifts)));
                        user_Followers.setText(changingUnit(Integer.parseInt(followers)));
                        user_Friends.setText(changingUnit(Integer.parseInt(friends)));
                        user_Shouts.setText(changingUnit(Integer.parseInt(shouts)));
                        user_heart_likes.setText(changingUnit(Integer.parseInt(likes)));

                        final int SECOND_MILLIS = 1000;
                        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
                        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
                        final int DAY_MILLIS = 24 * HOUR_MILLIS;
                        String last_online_final_time, last_final_time_reg = null;
                        long last_online_time2 = last_online_time * 1000;
                        long time_reg2 = time_reg * 1000;

                        long now = System.currentTimeMillis();
                        final long diff = now - last_online_time2;
                        if (diff < MINUTE_MILLIS) {
                            last_online_final_time = "just now";
                        } else if (diff < 2 * MINUTE_MILLIS) {
                            last_online_final_time = "a minute ago";
                        } else if (diff < 50 * MINUTE_MILLIS) {
                            last_online_final_time = diff / MINUTE_MILLIS + " minutes ago";
                        } else if (diff < 90 * MINUTE_MILLIS) {
                            last_online_final_time = "an hour ago";
                        } else if (diff < 24 * HOUR_MILLIS) {
                            last_online_final_time = diff / HOUR_MILLIS + " hours ago";
                        } else if (diff < 48 * HOUR_MILLIS) {
                            last_online_final_time = "yesterday";
                        } else {
                            last_online_final_time = diff / DAY_MILLIS + " days ago";
                        }
                        final long diff2 = now - time_reg2;
                        if (diff2 < MINUTE_MILLIS) {
                            last_final_time_reg = "just now";
                        } else if (diff2 < 2 * MINUTE_MILLIS) {
                            last_final_time_reg = "a minute ago";
                        } else if (diff2 < 50 * MINUTE_MILLIS) {
                            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
                        } else if (diff2 < 90 * MINUTE_MILLIS) {
                            last_final_time_reg = "an hour ago";
                        } else if (diff2 < 24 * HOUR_MILLIS) {
                            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago";
                        } else if (diff2 < 48 * HOUR_MILLIS) {
                            last_final_time_reg = "yesterday";
                        } else {

                            long calculating_months = diff2 / DAY_MILLIS;
                            if (calculating_months < 30) {
                                last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
                            } else if (calculating_months >= 30 && calculating_months <= 360) {
                                last_final_time_reg = calculating_months / 30 + " Month(s) ago";
                            } else if (calculating_months >= 30 && calculating_months > 360) {
                                last_final_time_reg = calculating_months / 360 + " Year(s) ago";
                            }
                        }

                        //                       user_lastlogin_info.setText("Last login " + last_online_final_time + " from " + login_ip + " on " + last_online);
                        user_registered_info.setText("Registered " + last_final_time_reg + " on " + time_resiter_date);
                        jsonArray = jsonObject.getJSONArray("logins");

                        for (int i = 0; i < jsonObject.getJSONArray("logins").length(); i++) {
                            Log.e("arytst", jsonArray.getJSONObject(i).getString("message"));
                            lastLoginArraylis.add(jsonArray.getJSONObject(i).getString("message"));
                        }

                        if (returning_imageview.getVisibility() == View.VISIBLE) {
                            returning_imageview.setVisibility(View.GONE);
                        }
                        parentlayout.setVisibility(View.VISIBLE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    FootprintLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FootprintLayout.setEnabled(false);
                            returning_imageview = MySocketsClass.custom_layout.Loader_image(My_profile.this, parentLinearLayout);
                            ViewingFootPrints();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    FootprintLayout.setEnabled(true);
                                }
                            }, 2000);
                        }
                    });

                    LikesLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LikesLayout.setEnabled(false);
                            returning_imageview = MySocketsClass.custom_layout.Loader_image(My_profile.this, parentLinearLayout);
                            ViewingLikes();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    LikesLayout.setEnabled(true);
                                }
                            }, 2000);
                        }
                    });

                    GiftsLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GiftsLayout.setEnabled(false);
                            returning_imageview = MySocketsClass.custom_layout.Loader_image(My_profile.this, parentLinearLayout);
                            ViewingGifts();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    GiftsLayout.setEnabled(true);
                                }
                            }, 2000);
                        }
                    });

                    followersLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            followersLayout.setEnabled(false);
                            returning_imageview = MySocketsClass.custom_layout.Loader_image(My_profile.this, parentLinearLayout);
                            ViewingFollowers();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    followersLayout.setEnabled(true);
                                }
                            }, 2000);
                        }
                    });

                    friendsLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            friendsLayout.setEnabled(false);
                            returning_imageview = MySocketsClass.custom_layout.Loader_image(My_profile.this, parentLinearLayout);
                            ViewingFriends();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    friendsLayout.setEnabled(true);
                                }
                            }, 2000);
                        }
                    });

                    shoutsLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showingMineShoutsOnHomePage();
                        }
                    });

                }
            });

        }
    };
}
