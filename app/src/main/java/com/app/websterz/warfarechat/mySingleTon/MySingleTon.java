package com.app.websterz.warfarechat.mySingleTon;

public class MySingleTon {

    public String username = "",
            usernameTextFromField="",
            passwordTextFromField ="",
            role = "", dp = "",
            statusMsg = "",
            status = "",
            sID = "",
            presence = "",
            age = "",
            gender = "",
            coins = "";
    private static MySingleTon mySingleTon = new MySingleTon();

    private MySingleTon() {
    }


    public static MySingleTon getInstance() {
        return mySingleTon;
    }
}
