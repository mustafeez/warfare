package com.app.websterz.warfarechat.FCM;

public class NotificationModel {
    String notificationUserName;
    String notificationMessage;

    public NotificationModel(String notificationUserName, String notificationMessage) {
        this.notificationUserName = notificationUserName;
        this.notificationMessage = notificationMessage;
    }

    public String getNotificationUserName() {
        return notificationUserName;
    }

    public void setNotificationUserName(String notificationUserName) {
        this.notificationUserName = notificationUserName;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }
}
