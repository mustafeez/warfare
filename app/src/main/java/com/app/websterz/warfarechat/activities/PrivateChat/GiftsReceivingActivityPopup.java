package com.app.websterz.warfarechat.activities.PrivateChat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.BroadCastReceiver.AlarmReceiver;
import com.app.websterz.warfarechat.DataBaseChatRooms.DataBase_ChatRooms;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.BuyShields.BuyShieldActivity;
import com.app.websterz.warfarechat.activities.SendAttacks.SendAttackActivity;
import com.app.websterz.warfarechat.activities.ShopActivity.ShopActivity;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.landingScreen.MainActivity;
import com.app.websterz.warfarechat.myRoomDatabase.WarfareDatabase;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.app.websterz.warfarechat.services.Foreground_service;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.one_time_emit;

public class GiftsReceivingActivityPopup extends AppCompatActivity {
    public static Dialog dialog;
    EditText ShoutDateailEditText;
    TextView message_text, total_coins, checkUsersInRoom, PayTextView, ShieldPurchaseMsg, ErrorOhTV;
    ImageView gift_image;
    String giftMessage, giftName, giftImg, giftSenderUser,
            Popupcat, username, role, age, gender, target_username,
            AttackUserName, ChatRoomName, Slug, CoinQuantity, shieldCoinCost, LoginFromAnotherLocationMessage, LoginFromAnotherLocationMessagetype;
    String ImageUrlPath = "images/gifts/";
    String AutoThemeTime = "06:00:00 pm";
    Button send_gift, cancel_attack;
    Spinner spinner;
    ArrayList<String> usernamesArray;
    //  View HeaderView;
    Activity activity;
    LinearLayout shieldPurchaseLinearLayout, PopUpLinearLayout;
    MySingleTon mySingleTon;
    String username_service = null, username_passwrd = null, token;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            dialog.dismiss();
            removeCallback();
        }
    };

    String Success, ServerMSG, socialPackName, socialPackCost, socialPackSlug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gifts_receiving_popup);
    }

    public void GiftsPopup(final Activity activity, JSONObject jsonObject, String PopUpCat, final String AttackName, final String AttackSlug, final String ShieldCoinCost) {

        this.activity = activity;
        this.Popupcat = PopUpCat;
        mySingleTon = MySingleTon.getInstance();


        if (PopUpCat.equals("Gifts")) {

            try {
                giftSenderUser = jsonObject.getString("username");
                giftImg = jsonObject.getString("img");
                giftName = jsonObject.getString("name");
                giftMessage = jsonObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = new Dialog(activity);
            //THEME
//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
          //  }
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);
            message_text = dialog.findViewById(R.id.message_text);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            PopUpLinearLayout.setBackgroundResource(R.drawable.square_new_theme_green);
            message_text.setText("HURRAY!");
            total_coins.setText(giftMessage);
            Glide.with(activity).load(baseUrl + ImageUrlPath + giftImg).into(gift_image);
            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();

        } else if (PopUpCat.equals("AnotherLocation")) {

            try {
                LoginFromAnotherLocationMessage = jsonObject.getString("message");
                LoginFromAnotherLocationMessagetype = jsonObject.getString("type");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = new Dialog(activity);

//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
      //      }

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);
            message_text = dialog.findViewById(R.id.message_text);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);

            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            gift_image.setVisibility(View.GONE);
            send_gift = dialog.findViewById(R.id.send_gift);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            cancel_attack.setText("Relogin");

            message_text.setText("Alert!");
            total_coins.setText(LoginFromAnotherLocationMessage);
            send_gift.setText("OK");
            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (LoginFromAnotherLocationMessagetype.equals("2")) {
                        LogoutUser();
                    }
                    dialog.dismiss();
                }
            });
            if (LoginFromAnotherLocationMessagetype.equals("2")) {
                cancel_attack.setVisibility(View.VISIBLE);
                cancel_attack.setBackgroundResource(R.drawable.error_dialog_orange_btn);
                cancel_attack.setText("USE HERE");
            }
            cancel_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    activity.startActivity(intent);
                    dialog.dismiss();
                }
            });

            if (!((Activity) activity).isFinishing())
                dialog.show();
        } else if (PopUpCat.equals("MyBuyingShield")) {
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = new Dialog(activity);

//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
      //      }

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);

            Slug = AttackSlug;
            CoinQuantity = AttackName;
            shieldCoinCost = ShieldCoinCost;

            message_text = dialog.findViewById(R.id.message_text);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            //coin=dialog.findViewById(R.id.coinIV);
            PayTextView = dialog.findViewById(R.id.payTextView);
            ShieldPurchaseMsg = dialog.findViewById(R.id.shieldPurchaseMsgTextView);
            shieldPurchaseLinearLayout = dialog.findViewById(R.id.buyShieldPopUpLinearLayout);

            if (shieldCoinCost.equals("0")) {
                ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);

                ErrorOhTV.setVisibility(View.VISIBLE);
                message_text.setVisibility(View.GONE);
                cancel_attack.setVisibility(View.VISIBLE);
                total_coins.setVisibility(View.VISIBLE);

                PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);

                ErrorOhTV.setText("Ohhhhhh!");
                total_coins.setText("Please choose attack quantity!");
                send_gift.setText("ok");

                gift_image.setImageResource(R.drawable.sad_icon);
                gift_image.getLayoutParams().width = 160;
                gift_image.getLayoutParams().height = 160;
                gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

                cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                cancel_attack.setTextColor(Color.parseColor("#000000"));
                send_gift.setBackgroundResource(R.drawable.error_dialog_red_btn);
                send_gift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            } else {
                message_text.setVisibility(View.GONE);
                cancel_attack.setVisibility(View.VISIBLE);
                PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_yellow_popyps);

                gift_image.setImageResource(R.drawable.coin_image);
                gift_image.getLayoutParams().width = 170;
                gift_image.getLayoutParams().height = 170;
                gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

                shieldPurchaseLinearLayout.setVisibility(View.VISIBLE);
                send_gift.setText("yes");
                total_coins.setVisibility(View.GONE);
                PayTextView.setText("Pay");
                PayTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                ShieldPurchaseMsg.setText(shieldCoinCost + " points for " + CoinQuantity + " attacks?");
                ShieldPurchaseMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

                cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                cancel_attack.setTextColor(Color.parseColor("#000000"));
            }

            dialog.show();

            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (shieldCoinCost.equals("0")) {
                        dialog.dismiss();
                    } else {

                        dialog.dismiss();
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("slug", Slug);
                            jsonObject.put("quantity", CoinQuantity);
                            mSocket.off(MySocketsClass.ListenersClass.GET_BUY_SHIELD_RESPONSE);
                            mSocket.emit(MySocketsClass.EmmittersClass.BUY_SHIELD, jsonObject);
                            mSocket.on(MySocketsClass.ListenersClass.GET_BUY_SHIELD_RESPONSE, get_buy_shield_response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            cancel_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });

        } else if (PopUpCat.equals("Attacks")) {

            AttackUserName = AttackName;
            try {

                usernamesArray = new ArrayList<>();

                ChatRoomName = jsonObject.getString("chatroom");

                for (int i = 0; i < jsonObject.getJSONArray("usersList").length(); i++) {

                    username = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("username");
                    usernamesArray.add(username);
                    role = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("role");
                    age = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("age");
                    gender = jsonObject.getJSONArray("usersList").getJSONObject(i).getString("gender");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialog = new Dialog(activity);
            //THEME
//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
        //    }
            //dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);

            message_text = dialog.findViewById(R.id.message_text);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            spinner = dialog.findViewById(R.id.username_Dropdown);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            checkUsersInRoom = dialog.findViewById(R.id.CheckUserAvailability);


            message_text.setVisibility(View.GONE);
            cancel_attack.setVisibility(View.VISIBLE);
            PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_yellow_popyps);

            if (AttackUserName.equals("Multi Super Kick")) {
              //  gift_image.setImageResource(R.drawable.users_active);
                Glide.with(activity).load(R.drawable.user_active).into(gift_image);
                gift_image.getLayoutParams().width = 160;
                gift_image.getLayoutParams().height = 120;
                gift_image.setScaleType(ImageView.ScaleType.FIT_XY);
                total_coins.setText("This will kick all the users from chatroom except you.");
                spinner.setVisibility(View.GONE);
            } else if (AttackUserName.equals("Super Kick") || AttackUserName.equals("Freeze") || AttackUserName.equals("Disconnect")) {
                if (usernamesArray.isEmpty()) {
                    checkUsersInRoom.setVisibility(View.VISIBLE);
                    checkUsersInRoom.setText("Chatroom is empty!");
                }
                gift_image.getLayoutParams().width = 120;
                gift_image.getLayoutParams().height = 130;
                gift_image.setScaleType(ImageView.ScaleType.FIT_XY);
               // gift_image.setImageResource(R.drawable.user_active);
                Glide.with(activity).load(R.drawable.user_active).into(gift_image);
                total_coins.setText("Choose username to attack");
                spinner.setVisibility(View.VISIBLE);
            }

            ArrayAdapter<String> arrayAdapter;
            arrayAdapter = new ArrayAdapter<>(activity.getBaseContext(), R.layout.simple_spinner_item_custom, usernamesArray);

            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(arrayAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    target_username = parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            send_gift.setText("Launch");
            cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
            cancel_attack.setTextColor(Color.parseColor("#000000"));

            dialog.show();

            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Slug = AttackSlug;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("username", target_username);
                        jsonObject.put("attack", Slug);
                        jsonObject.put("chatroom", ChatRoomName);
                        jsonObject.put("chat_type", 1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSocket.emit(MySocketsClass.EmmittersClass.GET_ATTACK_LAUNCH, jsonObject);
                    mSocket.on(MySocketsClass.ListenersClass.GET_ATTACK_LAUNCH_RESPONSE, get_attack_launch_response);
                }
            });

            cancel_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });

        } else if (PopUpCat.equals("BuyShield")) {
            Slug = AttackSlug;
            CoinQuantity = AttackName;
            shieldCoinCost = ShieldCoinCost;

            dialog = new Dialog(activity);
            //THEME
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
        //    }
            //dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);

            message_text = dialog.findViewById(R.id.message_text);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            //coin=dialog.findViewById(R.id.coinIV);
            PayTextView = dialog.findViewById(R.id.payTextView);
            ShieldPurchaseMsg = dialog.findViewById(R.id.shieldPurchaseMsgTextView);
            shieldPurchaseLinearLayout = dialog.findViewById(R.id.buyShieldPopUpLinearLayout);

            if (shieldCoinCost.equals("0")) {
                ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);

                ErrorOhTV.setVisibility(View.VISIBLE);
                message_text.setVisibility(View.GONE);
                cancel_attack.setVisibility(View.VISIBLE);
                total_coins.setVisibility(View.VISIBLE);

                PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);

                ErrorOhTV.setText("Ohhhhhh!");
                total_coins.setText("Please choose attack quantity!");
                send_gift.setText("ok");

                gift_image.setImageResource(R.drawable.sad_icon);
                gift_image.getLayoutParams().width = 160;
                gift_image.getLayoutParams().height = 160;
                gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

                cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                cancel_attack.setTextColor(Color.parseColor("#000000"));
                send_gift.setBackgroundResource(R.drawable.error_dialog_red_btn);
                send_gift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            } else {
                message_text.setVisibility(View.GONE);
                cancel_attack.setVisibility(View.VISIBLE);
                PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_yellow_popyps);

                gift_image.setImageResource(R.drawable.coin_image);
                gift_image.getLayoutParams().width = 170;
                gift_image.getLayoutParams().height = 170;
                gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

                shieldPurchaseLinearLayout.setVisibility(View.VISIBLE);
                send_gift.setText("yes");
                total_coins.setVisibility(View.GONE);
                PayTextView.setText("Pay");
                PayTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                ShieldPurchaseMsg.setText(shieldCoinCost + " points for " + CoinQuantity + " attacks?");
                ShieldPurchaseMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

                cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                cancel_attack.setTextColor(Color.parseColor("#000000"));
            }

            dialog.show();

            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (shieldCoinCost.equals("0")) {
                        dialog.dismiss();
                    } else {

                        dialog.dismiss();
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("slug", Slug);
                            jsonObject.put("quantity", CoinQuantity);
                            mSocket.off(MySocketsClass.ListenersClass.GET_BUY_SHIELD_RESPONSE);
                            mSocket.emit(MySocketsClass.EmmittersClass.BUY_SHIELD, jsonObject);
                            mSocket.on(MySocketsClass.ListenersClass.GET_BUY_SHIELD_RESPONSE, get_buy_shield_response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            cancel_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });
        } else if (PopUpCat.equals("SocialPack")) {
            socialPackName = AttackName;
            socialPackSlug = AttackSlug;
            socialPackCost = ShieldCoinCost;

            dialog = new Dialog(activity);
            //THEME
//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
  //          }
            //dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);

            message_text = dialog.findViewById(R.id.message_text);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            //coin=dialog.findViewById(R.id.coinIV);
            PayTextView = dialog.findViewById(R.id.payTextView);
            ShieldPurchaseMsg = dialog.findViewById(R.id.shieldPurchaseMsgTextView);
            shieldPurchaseLinearLayout = dialog.findViewById(R.id.buyShieldPopUpLinearLayout);

            message_text.setVisibility(View.GONE);
            cancel_attack.setVisibility(View.VISIBLE);
            PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_yellow_popyps);

            gift_image.setImageResource(R.drawable.coin_image);
            gift_image.getLayoutParams().width = 160;
            gift_image.getLayoutParams().height = 160;
            gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

            shieldPurchaseLinearLayout.setVisibility(View.VISIBLE);
            send_gift.setText("yes");
            total_coins.setVisibility(View.GONE);
            PayTextView.setText("Pay");
            PayTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            //PayTextView.setPadding(2,2,2,2);
            //ShieldPurchaseMsg.getLayoutParams().width = MATCH_PARENT;
            ShieldPurchaseMsg.setText(socialPackCost + " points to purchase " + socialPackName);
            ShieldPurchaseMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            //ShieldPurchaseMsg.setPadding(2,2,2,2);

            cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
            cancel_attack.setTextColor(Color.parseColor("#000000"));

            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("slug", socialPackSlug);
                        mSocket.emit(MySocketsClass.EmmittersClass.BUY_SHOP, jsonObject);
                        mSocket.on(MySocketsClass.ListenersClass.BUY_SHOP_RESPONSE, get_buy_shop_response);
                        dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            dialog.show();

            cancel_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });

        } else if (PopUpCat.equals("BuyShieldProtection")) {

            dialog = new Dialog(activity);
            //THEME
//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
 //           }
            //dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);

            message_text = dialog.findViewById(R.id.message_text);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            //coin=dialog.findViewById(R.id.coinIV);
            PayTextView = dialog.findViewById(R.id.payTextView);
            ShieldPurchaseMsg = dialog.findViewById(R.id.shieldPurchaseMsgTextView);
            shieldPurchaseLinearLayout = dialog.findViewById(R.id.buyShieldPopUpLinearLayout);
            ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);

            ErrorOhTV.setVisibility(View.VISIBLE);
            message_text.setVisibility(View.GONE);
            cancel_attack.setVisibility(View.VISIBLE);
            shieldPurchaseLinearLayout.setVisibility(View.GONE);

            PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);
            gift_image.setImageResource(R.drawable.settings_shield);
            gift_image.getLayoutParams().width = 180;
            gift_image.getLayoutParams().height = 200;
            gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

            send_gift.setText("buy");
            ErrorOhTV.setText("Protect Yourself!!!");
            total_coins.setVisibility(View.VISIBLE);
            total_coins.setText("Buy shield to protect yourself from attacks.");

            cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
            cancel_attack.setTextColor(Color.parseColor("#000000"));

            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, BuyShieldActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    dialog.dismiss();
                }
            });

            if (!((Activity) this).isFinishing())
                dialog.show();
            handler.postDelayed(runnable, 10000);

            cancel_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        } else if (PopUpCat.equals("reporting_shout")) {

            //  AttackUserName = AttackName;
            //          try {

            usernamesArray = new ArrayList<>();
            usernamesArray.add("Nudity");
            usernamesArray.add("Violence");
            usernamesArray.add("Harassment");
            usernamesArray.add("Suicide or Self injury");
            usernamesArray.add("Spam");
            usernamesArray.add("Unauthorized Sales");
            usernamesArray.add("Hate Speech");
            usernamesArray.add("Terrorism");
            usernamesArray.add("Something else");

            dialog = new Dialog(activity);
            //THEME
//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
 //           }
            //dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);

            message_text = dialog.findViewById(R.id.message_text);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            spinner = dialog.findViewById(R.id.username_Dropdown);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            checkUsersInRoom = dialog.findViewById(R.id.CheckUserAvailability);
            ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);
            ShoutDateailEditText = dialog.findViewById(R.id.ShoutDateailEditText);


            message_text.setVisibility(View.GONE);
            cancel_attack.setVisibility(View.VISIBLE);
            PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);

            gift_image.getLayoutParams().width = 120;
            gift_image.getLayoutParams().height = 130;
            gift_image.setScaleType(ImageView.ScaleType.FIT_XY);
            gift_image.setImageResource(R.drawable.user_active);
            total_coins.setText("Choose report type.");
            spinner.setVisibility(View.VISIBLE);
            ErrorOhTV.setVisibility(View.VISIBLE);
            ShoutDateailEditText.setVisibility(View.VISIBLE);
          //  ShoutDateailEditText.setFocusable(false);
            ErrorOhTV.setText("Report Shout");

            ArrayAdapter<String> arrayAdapter;
            arrayAdapter = new ArrayAdapter<>(activity.getBaseContext(), R.layout.simple_spinner_item_custom, usernamesArray);

            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(arrayAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    target_username = parent.getItemAtPosition(position).toString();
                    if (target_username.equals("Something else")) {
                      //  ShoutDateailEditText.setFocusable(true);
                        ShoutDateailEditText.setEnabled(true);
                    } else {
                        ShoutDateailEditText.setEnabled(false);
                        ShoutDateailEditText.setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            send_gift.setText("SEND");
            cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
            cancel_attack.setTextColor(Color.parseColor("#000000"));

            dialog.show();

            send_gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String AbusingReason = null;
                    if (target_username.equals("Something else")) {
                        if (ShoutDateailEditText.getText().equals("")) {
                            Toast.makeText(activity, "Please explain your abuse", Toast.LENGTH_SHORT).show();
                        } else {
                            AbusingReason = ShoutDateailEditText.getText().toString();
                        }
                    }
                    dialog.dismiss();
                    Slug = AttackSlug;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("type", "shout");
                        jsonObject.put("report_type", target_username);
                        if (target_username.equals("Something else")) {
                            jsonObject.put("notes", AbusingReason);
                        }
                        jsonObject.put("shoutID", ShieldCoinCost);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSocket.emit(MySocketsClass.EmmittersClass.SEND_ABUSE, jsonObject);
                    mSocket.on(MySocketsClass.ListenersClass.SEND_ABUSE_RESPONSE, send_abuse_response);
                }
            });

            cancel_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });
        } else if (PopUpCat.equals("reporting_comment")) {

            //  AttackUserName = AttackName;
            //          try {

            usernamesArray = new ArrayList<>();
            usernamesArray.add("Nudity");
            usernamesArray.add("Violence");
            usernamesArray.add("Harassment");
            usernamesArray.add("Suicide or Self injury");
            usernamesArray.add("Spam");
            usernamesArray.add("Unauthorized Sales");
            usernamesArray.add("Hate Speech");
            usernamesArray.add("Terrorism");
            usernamesArray.add("Something else");

            dialog = new Dialog(activity);
            //THEME
//            if (MySocketsClass.static_objects.theme == 0) {
//                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//            } else if (MySocketsClass.static_objects.theme == 1) {
//                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                Date systemDate = Calendar.getInstance().getTime();
//                String myDate = sdf.format(systemDate);
//                try {
//                    Date Date1 = sdf.parse(myDate);
//                    Date Date2 = sdf.parse(AutoThemeTime);
//                    if (Date1.getTime() < Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                    } else if (Date1.getTime() > Date2.getTime()) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (MySocketsClass.static_objects.theme == 2) {
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
 //           }
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.activity_gifts_receiving_popup);

            message_text = dialog.findViewById(R.id.message_text);
            cancel_attack = dialog.findViewById(R.id.cancel_attack);
            total_coins = dialog.findViewById(R.id.total_coins);
            gift_image = dialog.findViewById(R.id.gift_image);
            send_gift = dialog.findViewById(R.id.send_gift);
            spinner = dialog.findViewById(R.id.username_Dropdown);
            PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
            checkUsersInRoom = dialog.findViewById(R.id.CheckUserAvailability);
            ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);
            ShoutDateailEditText = dialog.findViewById(R.id.ShoutDateailEditText);


            message_text.setVisibility(View.GONE);
            cancel_attack.setVisibility(View.VISIBLE);
            PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);


            gift_image.getLayoutParams().width = 120;
            gift_image.getLayoutParams().height = 130;
            gift_image.setScaleType(ImageView.ScaleType.FIT_XY);
            gift_image.setImageResource(R.drawable.user_active);
            total_coins.setText("Choose report type.");
            spinner.setVisibility(View.VISIBLE);
            ErrorOhTV.setVisibility(View.VISIBLE);
            ShoutDateailEditText.setVisibility(View.VISIBLE);
         //   ShoutDateailEditText.setFocusable(false);
            ErrorOhTV.setText("Report Comment");

        ArrayAdapter<String> arrayAdapter;
        arrayAdapter = new ArrayAdapter<>(activity.getBaseContext(), R.layout.simple_spinner_item_custom, usernamesArray);

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                target_username = parent.getItemAtPosition(position).toString();
                if (target_username.equals("Something else")) {
                   // ShoutDateailEditText.setFocusable(true);
                    ShoutDateailEditText.setEnabled(true);
                } else {
                    ShoutDateailEditText.setEnabled(false);
                    ShoutDateailEditText.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        send_gift.setText("SEND");
        cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
        cancel_attack.setTextColor(Color.parseColor("#000000"));

        dialog.show();

        send_gift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String AbusingReason = null;
                if (target_username.equals("Something else")) {
                    if (ShoutDateailEditText.getText().equals("")) {
                        Toast.makeText(activity, "Please explain your abuse", Toast.LENGTH_SHORT).show();
                    } else {
                        AbusingReason = ShoutDateailEditText.getText().toString();
                    }
                }
                dialog.dismiss();
                Slug = AttackSlug;
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("type", "comment");
                    jsonObject.put("report_type", target_username);
                    if (target_username.equals("Something else")) {
                        jsonObject.put("notes", AbusingReason);
                    }
                    jsonObject.put("shoutID", ShieldCoinCost);
                    jsonObject.put("commentID", AttackSlug);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mSocket.off(MySocketsClass.ListenersClass.SEND_ABUSE_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.SEND_ABUSE, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.SEND_ABUSE_RESPONSE, send_abuse_response);
            }
        });

        cancel_attack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    }


    private void removeCallback() {
        handler.removeCallbacks(runnable);
    }

    Emitter.Listener send_abuse_response=new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
            JSONObject jsonObject=(JSONObject)args[0];
            try {
                String success=jsonObject.getString("success");
                String message=jsonObject.getString("message");
                if (success.equals("1"))
                {
                  //  Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                  //  CustomDialogError customDialogError=new CustomDialogError();
                    MySocketsClass.getInstance().showErrorDialog(MySocketsClass.static_objects.MyStaticActivity,
                            "Abuse Reported",message,"SuccessMsg",null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
                }
            });

        }
    };

    Emitter.Listener get_attack_launch_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            //dialog.dismiss();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        MySocketsClass.static_objects.attack_success = jsonObject.getString("success");
                        SendAttackActivity sendAttackActivity = new SendAttackActivity();
                        sendAttackActivity.RocketLaunchAnimation(activity);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        }
    };

    Emitter.Listener get_buy_shield_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            final JSONObject jsonObject = (JSONObject) args[0];

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    try {
                        Success = jsonObject.getString("success");
                        ServerMSG = jsonObject.getString("message");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dialog = new Dialog(activity);
                    //THEME
//                    if (MySocketsClass.static_objects.theme == 0) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                        //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//                    } else if (MySocketsClass.static_objects.theme == 1) {
//                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                        Date systemDate = Calendar.getInstance().getTime();
//                        String myDate = sdf.format(systemDate);
//                        try {
//                            Date Date1 = sdf.parse(myDate);
//                            Date Date2 = sdf.parse(AutoThemeTime);
//                            if (Date1.getTime() < Date2.getTime()) {
//                                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                            } else if (Date1.getTime() > Date2.getTime()) {
//                                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                            }
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//
//                    } else if (MySocketsClass.static_objects.theme == 2) {
                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                        //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
 //                   }
                    //dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.activity_gifts_receiving_popup);

                    message_text = dialog.findViewById(R.id.message_text);
                    cancel_attack = dialog.findViewById(R.id.cancel_attack);
                    total_coins = dialog.findViewById(R.id.total_coins);
                    gift_image = dialog.findViewById(R.id.gift_image);
                    send_gift = dialog.findViewById(R.id.send_gift);
                    PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
                    //coin=dialog.findViewById(R.id.coinIV);
                    PayTextView = dialog.findViewById(R.id.payTextView);
                    ShieldPurchaseMsg = dialog.findViewById(R.id.shieldPurchaseMsgTextView);
                    shieldPurchaseLinearLayout = dialog.findViewById(R.id.buyShieldPopUpLinearLayout);


                    ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);

                    ErrorOhTV.setVisibility(View.VISIBLE);
                    message_text.setVisibility(View.GONE);
                    cancel_attack.setVisibility(View.GONE);
                    total_coins.setVisibility(View.VISIBLE);

                    PopUpLinearLayout.setBackgroundResource(R.drawable.square_new_theme_green);

                    if (Success.equals("1")) {
                        total_coins.setText(ServerMSG);
                    } else {
                        total_coins.setText(ServerMSG);
                    }

                    ErrorOhTV.setText("Hurray!!!!");
                    total_coins.setText(ServerMSG);
                    send_gift.setText("ok");

                    gift_image.setImageResource(R.drawable.smile_icon);
                    gift_image.setColorFilter(ContextCompat.getColor(activity, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                    gift_image.getLayoutParams().width = 160;
                    gift_image.getLayoutParams().height = 160;
                    gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

                    cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                    cancel_attack.setTextColor(Color.parseColor("#000000"));
                    send_gift.setBackgroundResource(R.drawable.error_dialog_green_btn);
                    if (!((Activity) activity).isFinishing()) {
                        dialog.show();
                    }


                    send_gift.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BuyShieldActivity buyShieldActivity = new BuyShieldActivity();
                            buyShieldActivity.FinishBuyShieldActivity(activity);
                            dialog.dismiss();
                        }
                    });

                }
            });

        }
    };

    Emitter.Listener get_buy_shop_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            final JSONObject jsonObject = (JSONObject) args[0];
            Log.e("test", "12");
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    try {
                        Success = jsonObject.getString("success");
                        ServerMSG = jsonObject.getString("message");
                        Log.e("test", Success);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dialog = new Dialog(activity);
                    //THEME
//                    if (MySocketsClass.static_objects.theme == 0) {
//                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                        //popupLinearLayout.setBackgroundResource(R.drawable.popup_round_corners);
//
//                    } else if (MySocketsClass.static_objects.theme == 1) {
//                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
//                        Date systemDate = Calendar.getInstance().getTime();
//                        String myDate = sdf.format(systemDate);
//                        try {
//                            Date Date1 = sdf.parse(myDate);
//                            Date Date2 = sdf.parse(AutoThemeTime);
//                            if (Date1.getTime() < Date2.getTime()) {
//                                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
//                            } else if (Date1.getTime() > Date2.getTime()) {
//                                dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
//                            }
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//
//                    } else if (MySocketsClass.static_objects.theme == 2) {
                        dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_light_round_corners);
                        //popupLinearLayout.setBackgroundResource(R.drawable.popup_light_round_corners);
 //                   }
                    //dialog.getWindow().setBackgroundDrawableResource(R.drawable.popup_round_corners);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.activity_gifts_receiving_popup);

                    message_text = dialog.findViewById(R.id.message_text);
                    cancel_attack = dialog.findViewById(R.id.cancel_attack);
                    total_coins = dialog.findViewById(R.id.total_coins);
                    gift_image = dialog.findViewById(R.id.gift_image);
                    send_gift = dialog.findViewById(R.id.send_gift);
                    PopUpLinearLayout = dialog.findViewById(R.id.PopUpLinearLayout);
                    //coin=dialog.findViewById(R.id.coinIV);
                    PayTextView = dialog.findViewById(R.id.payTextView);
                    ShieldPurchaseMsg = dialog.findViewById(R.id.shieldPurchaseMsgTextView);
                    shieldPurchaseLinearLayout = dialog.findViewById(R.id.buyShieldPopUpLinearLayout);


                    ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);

                    ErrorOhTV.setVisibility(View.VISIBLE);
                    message_text.setVisibility(View.GONE);
                    cancel_attack.setVisibility(View.GONE);
                    total_coins.setVisibility(View.VISIBLE);

                    if (Success.equals("1")) {
                        ErrorOhTV.setText("Hurray!!!!");
                        total_coins.setText(ServerMSG);
                        send_gift.setText("ok");

                        PopUpLinearLayout.setBackgroundResource(R.drawable.square_new_theme_green);

                        gift_image.setImageResource(R.drawable.smile_icon);
                        gift_image.setColorFilter(ContextCompat.getColor(activity, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                        gift_image.getLayoutParams().width = 170;
                        gift_image.getLayoutParams().height = 170;
                        gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

                        send_gift.setBackgroundResource(R.drawable.error_dialog_green_btn);


                        send_gift.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mSocket.off(MySocketsClass.ListenersClass.BUY_SHOP_RESPONSE);
                                ShopActivity shopActivity = new ShopActivity();
                                shopActivity.FinishShopActivity(activity);
                                dialog.dismiss();
                            }
                        });
                    } else {
                        ErrorOhTV = dialog.findViewById(R.id.errorOhhTV);
                        ErrorOhTV.setVisibility(View.VISIBLE);
                        message_text.setVisibility(View.GONE);
                        cancel_attack.setVisibility(View.VISIBLE);
                        total_coins.setVisibility(View.VISIBLE);

                        PopUpLinearLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);

                        ErrorOhTV.setText("Ohhhhhh!");
                        total_coins.setText(ServerMSG);
                        send_gift.setText("ok");

                        gift_image.setImageResource(R.drawable.sad_icon);
                        gift_image.getLayoutParams().width = 170;
                        gift_image.getLayoutParams().height = 170;
                        gift_image.setScaleType(ImageView.ScaleType.FIT_XY);

                        send_gift.setBackgroundResource(R.drawable.error_dialog_red_btn);
                        send_gift.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                    }
                    cancel_attack.setBackgroundResource(R.drawable.error_dialog_gray_btn);
                    cancel_attack.setTextColor(Color.parseColor("#000000"));

                    if (!((Activity) activity).isFinishing()) {
                        dialog.show();
                    }
                    cancel_attack.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                }
            });
        }
    };

    protected void LogoutUser() {
        WarfareDatabase warfareDatabase;
        DataBase_ChatRooms dataBase_chatRooms;

        warfareDatabase = Room.databaseBuilder(activity, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        dataBase_chatRooms = Room.databaseBuilder(activity, DataBase_ChatRooms.class, "warfare_chatrooms_db").allowMainThreadQueries().build();

        MainActivity.mSocket.emit("logout");
        MainActivity.mSocket.disconnect();
        if (MySocketsClass.static_objects.chatRoomsDisconnectionFooter != null) {
            MySocketsClass.static_objects.chatRoomsDisconnectionFooter.setVisibility(View.GONE);
        }
        HomeScreen.is_user_connected = 0;
        MySocketsClass.static_objects.MyTotalCoin = "";
        warfareDatabase.myDataBaseAccessObject().deleteAllData();
        dataBase_chatRooms.chatRoomDBAO().deleteAllDataChatRoom();
        AllNotificationsClear();
        one_time_emit = 1;
        DeletingPreferenceForEmailAndSpecialId();
        DeletingPreferencesForUserNameAndPaswrd();
        MySocketsClass.getInstance().deletingPreferencesForNotifications(activity);
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(activity).edit();
        prefuseronline.putString("user_online", "offline");
        prefuseronline.apply();
        ChattingIconRedDot();

        enableBroadcastReceiver();
        MySocketsClass.static_objects.NewUserMessageList.clear();
        MySocketsClass.static_objects.hashMap.clear();

        activity.stopService(new Intent(activity, Foreground_service.class));
        activity.finishAffinity();
    }

    private void DeletingPreferencesForUserNameAndPaswrd() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(activity).edit();
        DeletingPreferences.remove("mine_user_name");
        DeletingPreferences.remove("mine_password");
        DeletingPreferences.apply();
    }

    private void DeletingPreferenceForEmailAndSpecialId() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(activity).edit();
        DeletingPreferences.remove("userEmail");
        DeletingPreferences.remove("userSpecialId");
        DeletingPreferences.apply();
    }

    private void DeletingPreferenceForHostBase() {
        SharedPreferences.Editor DeletingPreferences = PreferenceManager.getDefaultSharedPreferences(activity).edit();
        DeletingPreferences.remove("MainBaseURL");
        DeletingPreferences.apply();
    }

    private void AllNotificationsClear() {
        NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void ChattingIconRedDot() {
        SharedPreferences.Editor chattingredDot = PreferenceManager.getDefaultSharedPreferences(activity).edit();
        chattingredDot.putString("chattingredDot", "no_newMsg");
        chattingredDot.apply();
    }

    private void enableBroadcastReceiver() {
        ComponentName receiver = new ComponentName(activity, AlarmReceiver.class);
        PackageManager pm = activity.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void Relogin() {
        gettingUserNamePwd();
        ReloginUser();
    }

    private void gettingUserNamePwd() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        username_service = prefs.getString("mine_user_name", null);
        username_passwrd = prefs.getString("mine_password", null);
    }

    private void ReloginUser() {
        if ((username_service!=null) &&
                (username_passwrd!=null)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("username", username_service);
                jsonObject.put("password", username_passwrd);
                jsonObject.put("agent", "ANDROID1.0");
                jsonObject.put("token", token);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ConnectingSocketIfNotConnected();
            //  if (mSocket != null)
            MainActivity.mSocket.emit(MySocketsClass.EmmittersClass.ADD_USER, jsonObject);
            MainActivity.mSocket.on(MySocketsClass.ListenersClass.LOGIN, onLogin);
        }
    }

    public void savingUserPreferenceOnline(Context context) {
        SharedPreferences.Editor prefuseronline = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefuseronline.putString("user_online", "online");
        prefuseronline.apply();
    }

    private void ConnectingSocketIfNotConnected() {
        if (MainActivity.mSocket == null) {
            MainActivity.mSocket = AppConstants.mSocket;
            try {
                MainActivity.mSocket = IO.socket(baseUrl);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            Log.e("Tryyying", "In connectingSocket");
        }
        if (!MainActivity.mSocket.connected()) {

            MainActivity.mSocket.connect();
            Log.e("Tryyying", "In connectingSocket 2");

        }
    }

    Emitter.Listener onLogin = new Emitter.Listener() {
        @SuppressLint("CommitPrefEdits")
        @Override
        public void call(Object... args) {
            Log.e("Tryyying", "In On Login");
            JSONObject data = (JSONObject) args[0];
            String username, role, dp, statusMsg, status, sID, presence, age, gender, coins;
            int theme;
            try {
                username = data.getString("username");
                role = data.getString("role");
                dp = data.getString("dp");
                statusMsg = data.getString("statusMsg");
                status = data.getString("status");
                sID = data.getString("sID");
                presence = data.getString("presence");
                age = data.getString("age");
                gender = data.getString("gender");
                coins = data.getString("coins");
                theme = Integer.parseInt(data.getString("theme"));
              //  MySocketsClass.static_objects.theme = theme;


                SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(activity).edit();
                prefEditor55.putString("user_name", username);
//                prefEditor55.putString("mine_user_name", username);
//                prefEditor55.putString("mine_password", mySingleTon.passwordTextFromField);
                prefEditor55.putString("role", role);
                prefEditor55.putString("dp", dp);
                prefEditor55.putString("statusMsg", statusMsg);
                prefEditor55.putString("status", status);
                prefEditor55.putString("sID", sID);
                prefEditor55.putString("presence", presence);
                prefEditor55.putString("age", age);
                prefEditor55.putString("gender", gender);
                prefEditor55.putString("coins", coins);
                prefEditor55.putString("theme", String.valueOf(theme));
                prefEditor55.apply();

                mySingleTon.username = username;
                mySingleTon.role = role;
                mySingleTon.dp = dp;
                mySingleTon.statusMsg = statusMsg;
                mySingleTon.status = status;
                mySingleTon.sID = sID;
                mySingleTon.presence = presence;
                mySingleTon.age = age;
                mySingleTon.gender = gender;
                mySingleTon.coins = coins;
//                mySingleTon.theme = String.valueOf(theme);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            savingUserPreferenceOnline(activity);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    if (MySocketsClass.static_objects.DisconnectionTextview != null)
                        MySocketsClass.static_objects.DisconnectionTextview.setVisibility(View.GONE);
                }
            });
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }
                    token = task.getResult().getToken();
                }
            });
            Intent intent = new Intent(activity, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            activity.startActivity(intent);

        }
    };
}
