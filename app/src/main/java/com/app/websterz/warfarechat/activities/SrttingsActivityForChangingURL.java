package com.app.websterz.warfarechat.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.landingScreen.MainActivity;
import com.github.nkzawa.socketio.client.IO;

import java.net.URISyntaxException;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class SrttingsActivityForChangingURL extends AppCompatActivity {

    EditText EditTextURL;
    Button DefaultBtn,ChangeBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_srttings_for_changing_url);

        Initializations();
        MyCode();

        DefaultBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangingURLS();
            }
        });

        ChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddingNewURLS();
            }
        });

    }

    private void Initializations(){
        EditTextURL=findViewById(R.id.EditTextURL);
        DefaultBtn=findViewById(R.id.DefaultBtn);
        ChangeBtn=findViewById(R.id.ChangeBtn);
    }

    private void MyCode(){
        EditTextURL.setText(RetreivingBaseUrl());
    }

    private void ChangingURLS(){
        baseUrl = "https://socio.chat/";
        EditTextURL.setText(baseUrl);
        SavingMainBaseUrl("https://socio.chat/");
        if (mSocket!=null) {
            mSocket.close();
            mSocket.disconnect();
            mSocket=null;
        }
        Toast.makeText(this, "Default setting set.", Toast.LENGTH_SHORT).show();
    }

    private void AddingNewURLS(){
        if (EditTextURL.getText()!=null)
        {
            baseUrl=null;
            baseUrl=EditTextURL.getText().toString();
            SavingMainBaseUrl(EditTextURL.getText().toString());
            if (mSocket!=null) {
                mSocket.close();
                mSocket.disconnect();
                mSocket=null;
            }
            Toast.makeText(this, "Host adress changed.", Toast.LENGTH_SHORT).show();
        }


    }

    private void SavingMainBaseUrl(String URL){
        SharedPreferences.Editor sharedPreferences= PreferenceManager.getDefaultSharedPreferences(SrttingsActivityForChangingURL.this).edit();
        sharedPreferences.putString("MainBaseURL",URL);
        sharedPreferences.apply();
    }

    private String RetreivingBaseUrl(){
        SharedPreferences sharedPreferences=PreferenceManager.getDefaultSharedPreferences(SrttingsActivityForChangingURL.this);
        return sharedPreferences.getString("MainBaseURL","https://socio.chat/");
    }

    @Override
    public void onBackPressed() {
        SrttingsActivityForChangingURL.this.startActivity(new Intent(SrttingsActivityForChangingURL.this, MainActivity.class));
        finish();
    }
}

