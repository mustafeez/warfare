package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter.CommentsRepliesAdapter;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.ShoutSharingInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.CommentsSectionModelClass;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.chibde.visualizer.CircleBarVisualizer;
import com.chibde.visualizer.LineBarVisualizer;
import com.chibde.visualizer.LineVisualizer;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.tooltip.Tooltip;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ExplainingShoutActivity extends AppCompatActivity implements ShoutSharingInterface {

    CircleImageView UsersPictureView, NewCommentUserDp;
    LinearLayout LikeButton, CommentsButton, MoreOptionsLayout, firstLayout, childLayout,loader_imageview_Layout;
    RelativeLayout YoutubeUrlImageViewCardview, SimpleImageViewCardview, simpleVideoPlayerView,
            MainParentLayout, newCommentLayout, BbuttonsLayout, audioViewCardview;
    TextView UserRealName, UserAccountName, UserPostedText, PostTimeTextview, LikesTextview, DisLikesTextview,
            CommentsTextView, BlockingUser, HideOrDeleteShout, ReportingShout, ViewingProfile, textCurrentTime, textTotalDuration;
    ImageView UserRank, UserAge, RowItemsMoreOptions, DislikeImageview, LikeImageview, SimpleImageView, bt_fullscreen, bt_Mute,
             audioImageView;
    RecyclerView CommentsRecyclerview;
    MySingleTon mySingleTon;
    Button ReplyBtn;
    EditText MyCommentEditText;
    ArrayList<CommentsSectionModelClass> CommentsArraylist;
    CommentsRepliesAdapter commentsRepliesAdapter;
    LinearLayoutManager MyLinearLayout;
    Client MyClient;
    private YouTubePlayerView youTubePlayerView;
    Tooltip tooltip;
    PlayerView playerView;
    ProgressBar progressBar;
    SimpleExoPlayer simpleExoPlayer;
    boolean isFullScreen = false;
    AvatarView commentLoadergif;
    private String totalUpdatedLikesValue, totalUpdatedDisLikesValue, totalUpdatedComentsValue, ismyLikeOnPostNewValue,
            ismyDisLikeOnPostNewValue, fullDate, MainShoutID, UserNameWhoseShoutIs, UserShoutedtext;

    MediaPlayer mediaPlayer;
    private Handler handler = new Handler();
    CircleBarVisualizer circleBarVisualizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explaining_shout);

        Initializations();
        MyCode();

        ReplyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReplyingComment();
            }
        });
        RowItemsMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeletingOrHidingShout();
            }
        });
        ReportingShout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReportingShoutUser();
            }
        });
        HideOrDeleteShout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideOrDeletedShoutUSer();
            }
        });
        BlockingUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlockingShoutUser();
            }
        });
        ViewingProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewingShoutedUserProfile();
            }
        });
        LikeImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LikingShout();
            }
        });
        DislikeImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DislikingPost();
            }
        });
        PostTimeTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showingToolTip(v);
            }
        });
        YoutubeUrlImageViewCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YouTubeOpening();
            }
        });
        SimpleImageViewCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  ZoomingImage(v, UserShoutedtext.split("\\[\\[")[1].replace("]]", "").trim());
                MySocketsClass.getInstance().openingZoomingActivity(ExplainingShoutActivity.this,baseUrl+UserShoutedtext.split("\\[\\[")[1].replace("]]", "").trim(),"shouts");
            }
        });
        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Glide.with(ExplainingShoutActivity.this).load(R.drawable.shout_play).into(audioImageView);

                textCurrentTime.setText("0:00 ");
//                textTotalDuration.setText("/ 0:00");
//                mediaPlayer.reset();
              //  prepareMediaPlayer();
            }
        });
        audioImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();
                    Glide.with(ExplainingShoutActivity.this).load(R.drawable.shout_play).into(audioImageView);
                } else {
                    mediaPlayer.start();
                    updateSeekbar();
                    Glide.with(ExplainingShoutActivity.this).load(R.drawable.shout_pause).into(audioImageView);
                    circleBarVisualizer.setColor(ContextCompat.getColor(ExplainingShoutActivity.this, R.color.new_theme_green));
                    circleBarVisualizer.setPlayer(mediaPlayer.getAudioSessionId());
                }
            }
        });

    }

    private void Initializations() {
        mySingleTon = MySingleTon.getInstance();
        MyClient = new Client(ExplainingShoutActivity.this);
        childLayout = findViewById(R.id.childLayout);
        UsersPictureView = findViewById(R.id.UsersPictureView);
        UserRealName = findViewById(R.id.UserRealName);
        UserAccountName = findViewById(R.id.UserAccountName);
        UserRank = findViewById(R.id.UserRank);
        UserAge = findViewById(R.id.UserAge);
        LikeButton = findViewById(R.id.LikeButton);
        CommentsButton = findViewById(R.id.CommentsButton);
        YoutubeUrlImageViewCardview = findViewById(R.id.YoutubeUrlImageViewCardview);
        UserPostedText = findViewById(R.id.UserPostedText);
        audioViewCardview = findViewById(R.id.audioViewCardview);
        LikesTextview = findViewById(R.id.LikesTextview);
        DisLikesTextview = findViewById(R.id.DisLikesTextview);
        CommentsTextView = findViewById(R.id.CommentsTextView);
        RowItemsMoreOptions = findViewById(R.id.RowItemsMoreOptions);
        LikeImageview = findViewById(R.id.LikeImageview);
        DislikeImageview = findViewById(R.id.DislikeImageview);
        PostTimeTextview = findViewById(R.id.PostTimeTextview);
        CommentsRecyclerview = findViewById(R.id.CommentsRecyclerview);
        MainParentLayout = findViewById(R.id.MainParentLayout);
        NewCommentUserDp = findViewById(R.id.NewCommentUserDp);
        ReplyBtn = findViewById(R.id.ReplyBtn);
        circleBarVisualizer = findViewById(R.id.visualizer);
        MyCommentEditText = findViewById(R.id.MyCommentEditText);
        MoreOptionsLayout = findViewById(R.id.MoreOptionsLayout);
        BlockingUser = findViewById(R.id.BlockingUser);
        HideOrDeleteShout = findViewById(R.id.HideOrDeleteShout);
        SimpleImageView = findViewById(R.id.SimpleImageView);
        SimpleImageViewCardview = findViewById(R.id.SimpleImageViewCardview);
        ReportingShout = findViewById(R.id.ReportingShout);
        ViewingProfile = findViewById(R.id.ViewingProfile);
        youTubePlayerView = findViewById(R.id.youtube_player_view);
        simpleVideoPlayerView = findViewById(R.id.simpleVideoPlayerView);
        bt_fullscreen = findViewById(R.id.bt_fullscreen);
        firstLayout = findViewById(R.id.firstLayout);
        BbuttonsLayout = findViewById(R.id.BbuttonsLayout);
        newCommentLayout = findViewById(R.id.newCommentLayout);
        commentLoadergif = findViewById(R.id.commentLoadergif);
        audioImageView = findViewById(R.id.audioImageView);
        textCurrentTime = findViewById(R.id.textCurrentTime);
        textTotalDuration = findViewById(R.id.textTotalDuration);
        mediaPlayer = new MediaPlayer();
        bt_Mute = findViewById(R.id.bt_Mute);
        getLifecycle().addObserver(youTubePlayerView);
        CommentsArraylist = new ArrayList<>();
        MyLinearLayout = new LinearLayoutManager(ExplainingShoutActivity.this, RecyclerView.VERTICAL, false);
        CommentsRecyclerview.setLayoutManager(MyLinearLayout);
        commentsRepliesAdapter = new CommentsRepliesAdapter(CommentsArraylist, ExplainingShoutActivity.this, this);
        CommentsRecyclerview.setAdapter(commentsRepliesAdapter);
    }

    private void MyCode() {
        Intent intent = getIntent();
        MainShoutID = intent.getStringExtra("ShoutedText");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", MainShoutID);
            mSocket.off(MySocketsClass.ListenersClass.GET_SHOUT_RESPONSE);
            mSocket.off(MySocketsClass.ListenersClass.GET_COMMENTS_RESPONSE);
            mSocket.off(MySocketsClass.ListenersClass.NEW_COMMENT);
            mSocket.off(MySocketsClass.ListenersClass.UPDATE_SHOUTS_DETAILS);
            mSocket.on(MySocketsClass.ListenersClass.UPDATE_SHOUTS_DETAILS, update_shout_details);
            mSocket.emit(MySocketsClass.EmmittersClass.GET_SHOUT, jsonObject);
            showingLoader();
            mSocket.on(MySocketsClass.ListenersClass.GET_SHOUT_RESPONSE, get_shout_response);
            mSocket.on(MySocketsClass.ListenersClass.GET_COMMENTS_RESPONSE, get_comments_response);
            mSocket.on(MySocketsClass.ListenersClass.NEW_COMMENT, new_comment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Glide.with(ExplainingShoutActivity.this).load(baseUrl + mySingleTon.dp).into(NewCommentUserDp);
        LocalBroadcastManager.getInstance(ExplainingShoutActivity.this).registerReceiver(updatingCommentValueReceiver, new IntentFilter("UpdatingValueofComments"));
    }

    private void showingCommentGif() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (commentLoadergif.getVisibility() == View.GONE) {
                    commentLoadergif.setAnimating(true);
                    commentLoadergif.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void hidingCommentGif() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (commentLoadergif.getVisibility() == View.VISIBLE)
                    commentLoadergif.setVisibility(View.GONE);
            }
        });
    }

    private void ReplyingComment() {
        if (MyCommentEditText.getText().toString().equals("")) {
            Toast.makeText(ExplainingShoutActivity.this, "Please reply your comment.", Toast.LENGTH_SHORT).show();
        } else {
            showingCommentGif();
            ReplyBtn.setEnabled(false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("text", MyCommentEditText.getText().toString());
                jsonObject.put("id", MainShoutID);
                mSocket.off(MySocketsClass.ListenersClass.ADD_COMMENT_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.ADD_COMMENT, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.ADD_COMMENT_RESPONSE, add_comments_response);
                MyCommentEditText.setText("");
                hidingKeyboard(ExplainingShoutActivity.this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void hidingKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void DeletingOrHidingShout() {
        if (MoreOptionsLayout.getVisibility() == View.VISIBLE) {
            MoreOptionsLayout.setVisibility(View.GONE);
        } else {
            if (UserNameWhoseShoutIs != null) {
                if (UserNameWhoseShoutIs.equals(mySingleTon.username)) {
                    HideOrDeleteShout.setText("Delete this shout");
                    BlockingUser.setVisibility(View.GONE);
                    ViewingProfile.setVisibility(View.GONE);
                    ReportingShout.setVisibility(View.GONE);
                } else {
                    HideOrDeleteShout.setText("Hide this shout");
                }
            }
            MoreOptionsLayout.setVisibility(View.VISIBLE);
        }
    }

    private void HideOrDeletedShoutUSer() {
        if (UserNameWhoseShoutIs.equals(mySingleTon.username)) {
            Toast.makeText(ExplainingShoutActivity.this, "Delete my own post", Toast.LENGTH_SHORT).show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", MainShoutID);
                mSocket.off(MySocketsClass.ListenersClass.DELETE_SHOUT_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.DELETE_SHOUT, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.DELETE_SHOUT_RESPONSE, delete_shout_response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(ExplainingShoutActivity.this, "Hiding post", Toast.LENGTH_SHORT).show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", MainShoutID);
                mSocket.off(MySocketsClass.ListenersClass.BLOCK_SHOUT_RESPONSE);
                mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_SHOUT, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.BLOCK_SHOUT_RESPONSE, block_shout_response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void ReportingShoutUser() {
        Toast.makeText(ExplainingShoutActivity.this, "Reporting shout clicked", Toast.LENGTH_SHORT).show();
        GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
        giftsReceivingActivityPopup.GiftsPopup(ExplainingShoutActivity.this,
                null, "reporting_shout", null, null, MainShoutID);
    }

    private void BlockingShoutUser() {
        Toast.makeText(ExplainingShoutActivity.this, "Blocking User all posts", Toast.LENGTH_SHORT).show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", UserNameWhoseShoutIs);
            mSocket.off(MySocketsClass.ListenersClass.BLOCK_SHOUT_USER_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_SHOUT_USER, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.BLOCK_SHOUT_USER_RESPONSE, block_shout_user_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ViewingShoutedUserProfile() {
        Intent intent = new Intent(ExplainingShoutActivity.this, UserFriendProfile.class);
        intent.putExtra("username", UserNameWhoseShoutIs);
        startActivity(intent);
    }

    private void updateSeekbar() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            handler.postDelayed(updater, 1000);
        }
    }

    private void prepareMediaPlayer(String dataSource) {
        try {Log.e("testing", "dataSrc >" +dataSource);
            mediaPlayer.setDataSource(baseUrl+dataSource);
            mediaPlayer.prepare();
            textTotalDuration.setText("/ "+milisecondsToTimer(mediaPlayer.getDuration()));
           // updateSeekbar();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("testing", "excpetn >" + e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = ExplainingShoutActivity.this;
        MySocketsClass.static_objects.global_layout = MainParentLayout;
    }

    @Override
    public void shoutDeletingMethod(int PostIdPosition) {
        if (MyCommentEditText.getText().equals("")) {
            MyCommentEditText.setText("\"@" + CommentsArraylist.get(PostIdPosition).getUsername());
            MyCommentEditText.setSelection(CommentsArraylist.get(PostIdPosition).getUsername().length());
        } else {
            if (!MyCommentEditText.getText().equals(CommentsArraylist.get(PostIdPosition).getUsername())) {
                String UserText = MyCommentEditText.getText().toString();
                MyCommentEditText.setText(UserText + " " + CommentsArraylist.get(PostIdPosition).getUsername());
                MyCommentEditText.setSelection((UserText + " " + CommentsArraylist.get(PostIdPosition).getUsername()).length());
            } else {
                MyCommentEditText.setText("\"@" + CommentsArraylist.get(PostIdPosition).getUsername());
                MyCommentEditText.setSelection(CommentsArraylist.get(PostIdPosition).getUsername().length() + 1);
            }
        }
    }

    private String GettingPostingTime(long time_reg) {

        //finding Time...
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time_reg * 1000);
        String date = DateFormat.format("EEE d, MMM yyyy hh:mm a", cal).toString();
        String today_time = DateFormat.format("hh:mm a", cal).toString();
        String date2 = DateFormat.format("yyyy / MM / d", cal).toString();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / d");
        String current_date = mdformat.format(calendar.getTime());


        //finding day/date/days ago...
        String last_final_time_reg = null;
        long time_reg2 = time_reg * 1000;
        long now = System.currentTimeMillis();
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "just now";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "a minute ago";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
            fullDate = today_time;
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "an hour ago ";
            fullDate = today_time;
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago ";
            fullDate = today_time;
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "yesterday ";
            fullDate = today_time;
        } else {

            long calculating_months = diff2 / DAY_MILLIS;
            if (calculating_months < 30) {
                last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
                fullDate = date;
            } else if (calculating_months >= 30 && calculating_months <= 360) {
                last_final_time_reg = calculating_months / 30 + " Month(s) ago";
                fullDate = date;
            } else if (calculating_months >= 30 && calculating_months > 360) {
                last_final_time_reg = calculating_months / 360 + " Year(s) ago";
                fullDate = date;
            }
        }
        return last_final_time_reg;
    }

    private void LikingShout() {
        Toast.makeText(ExplainingShoutActivity.this, "Like post", Toast.LENGTH_SHORT).show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", MainShoutID);
            mSocket.off(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE);
            mSocket.off(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.LIKE_SHOUT, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE, like_shout_response);
            mSocket.on(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE, dislike_shout_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void DislikingPost() {
        Toast.makeText(ExplainingShoutActivity.this, "DisLike post", Toast.LENGTH_SHORT).show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", MainShoutID);
            mSocket.off(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE);
            mSocket.off(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.DISLIKE_SHOUT, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE, like_shout_response);
            mSocket.on(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE, dislike_shout_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String GettingVideoIdFromYoutubeLink(String url) {
        if (url.contains("v=")) {
            if (url.contains("&")) {
                String[] u = url.split("v=");
                u = u[1].split("&");
                url = u[0];
            } else {
                String[] u = url.split("v=");
                url = u[1];
            }
        } else {
            String[] u = url.split("be/");
            url = u[1];
        }
        return url;
    }

    private void YouTubeOpening() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(UserShoutedtext)));
        } catch (Exception ignore) {
        }
    }

    @Override
    public void onBackPressed() {
        if (isFullScreen) {
            bt_fullscreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_fullscreen));
            isFullScreen = false;
            showingOtherLayouts();
            showingSmallScreenPlayer();
        } else {
            sendingBroadcastToUpdateValues(MainShoutID, totalUpdatedLikesValue, totalUpdatedDisLikesValue,
                    totalUpdatedComentsValue, ismyLikeOnPostNewValue, ismyDisLikeOnPostNewValue);
            mSocket.off(MySocketsClass.ListenersClass.GET_SHOUT_RESPONSE);
            mSocket.off(MySocketsClass.ListenersClass.GET_COMMENTS_RESPONSE);
            mSocket.off(MySocketsClass.ListenersClass.NEW_COMMENT);
            finish();
        }
    }

    private void showingLoader() {
        loader_imageview_Layout=MySocketsClass.custom_layout.Loader_image(ExplainingShoutActivity.this, MainParentLayout);
    }

    private void hidingLoader() {
        if (loader_imageview_Layout != null && loader_imageview_Layout.getVisibility() == View.VISIBLE) {
            loader_imageview_Layout.setVisibility(View.GONE);
        }
        if (childLayout.getVisibility() == View.GONE) {
            childLayout.setVisibility(View.VISIBLE);
            newCommentLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showingToolTip(View view) {
        tooltip = new Tooltip.Builder(view)
                .setBackgroundColor(getResources().getColor(R.color.new_theme_green))
                .setTextColor(getResources().getColor(R.color.colorWhite))
                .setText(fullDate).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (tooltip.isShowing()) {
                    tooltip.dismiss();
                }
            }
        }, 2000);
    }

    private void runVideoOnVideoPlayer(String videoLink) {

        playerView = findViewById(R.id.playerView);
        progressBar = findViewById(R.id.progess_bar);
        Uri videoUrl = Uri.parse(videoLink);

        LoadControl loadControl = new DefaultLoadControl();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(ExplainingShoutActivity.this, trackSelector, loadControl);
        DefaultHttpDataSourceFactory factory = new DefaultHttpDataSourceFactory("exoplayer_video");
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        MediaSource mediaSource = new ExtractorMediaSource(videoUrl, factory, extractorsFactory, null, null);
        playerView.setPlayer(simpleExoPlayer);
        playerView.setKeepScreenOn(true);
        simpleExoPlayer.prepare(mediaSource);
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_BUFFERING) {
                    progressBar.setVisibility(View.VISIBLE);
                } else if (playbackState == Player.STATE_READY) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
        bt_fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFullScreen) {
                    bt_fullscreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_fullscreen));
                    isFullScreen = false;
                    showingOtherLayouts();
                    showingSmallScreenPlayer();
                } else {
                    bt_fullscreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_fullscreen_exit));
                    isFullScreen = true;
                    hidingOtherLayouts();
                    showingLargeScreenPlayer();
                }
            }
        });

        bt_Mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (simpleExoPlayer.getVolume() == 0) {
                    simpleExoPlayer.setVolume(100);
                    bt_Mute.setImageResource(R.drawable.ic_volume_off);
                    Log.e("volumeTest", "VolumeUYp");
                } else {
                    simpleExoPlayer.setVolume(0);
                    bt_Mute.setImageResource(R.drawable.ic_volume_up);
                    Log.e("volumeTest", "VolumeDown");
                }
            }
        });
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void showingSmallScreenPlayer() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        simpleVideoPlayerView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                convertDpToPixel(this, 200)
        ));
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void showingLargeScreenPlayer() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        simpleVideoPlayerView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));
    }

    private int convertDpToPixel(Context context, int dp) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                context.getResources().getDisplayMetrics()
        );
    }

    private void hidingOtherLayouts() {
        if (firstLayout.getVisibility() == View.VISIBLE) firstLayout.setVisibility(View.GONE);
        if (BbuttonsLayout.getVisibility() == View.VISIBLE) BbuttonsLayout.setVisibility(View.GONE);
        if (newCommentLayout.getVisibility() == View.VISIBLE)
            newCommentLayout.setVisibility(View.GONE);
        if (CommentsRecyclerview.getVisibility() == View.VISIBLE)
            CommentsRecyclerview.setVisibility(View.GONE);
        if (UserPostedText.getVisibility() == View.VISIBLE) UserPostedText.setVisibility(View.GONE);
    }

    private void showingOtherLayouts() {
        if (firstLayout.getVisibility() == View.GONE) firstLayout.setVisibility(View.VISIBLE);
        if (BbuttonsLayout.getVisibility() == View.GONE) BbuttonsLayout.setVisibility(View.VISIBLE);
        if (newCommentLayout.getVisibility() == View.GONE)
            newCommentLayout.setVisibility(View.VISIBLE);
        if (CommentsRecyclerview.getVisibility() == View.GONE)
            CommentsRecyclerview.setVisibility(View.VISIBLE);
        if (UserPostedText.getVisibility() == View.GONE) UserPostedText.setVisibility(View.VISIBLE);
    }

    private Runnable updater=new Runnable() {
        @Override
        public void run() {
         updateSeekbar();
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                long currentDuration = mediaPlayer.getCurrentPosition();
                textCurrentTime.setText(milisecondsToTimer(currentDuration) + " ");
            }
        }
    };


    private String milisecondsToTimer(long milliseconds){
        String timerString="";
        String secondsString;

        int hours=(int)(milliseconds/(1000*60*60));
        int minutes=(int)(milliseconds%(1000*60*60))/(1000*60);
        int seconds=(int)(milliseconds%(1000*60*60))%(1000*60)/1000;

        if (hours>0){
            timerString=hours+":";

        }
        if (seconds<10){
            secondsString="0"+seconds;
        }
        else {
            secondsString=""+seconds;
        }
        timerString=timerString+minutes+":"+secondsString;
        return timerString;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
            simpleExoPlayer.getPlaybackState();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(true);
            simpleExoPlayer.getPlaybackState();
        }
    }

    private void commentsVisibilityCheckAndSettingCommentsValue(String value) {
        if (value.equals("0")) {
            CommentsTextView.setVisibility(View.GONE);
        } else {
            CommentsTextView.setVisibility(View.VISIBLE);
            if (value.equals("1")) {
                CommentsTextView.setText(value + " comment");
            } else {
                CommentsTextView.setText(value + " comments");
            }
        }
    }

    private void likesVisibilityCheckAndSettingLikesValue(String value) {
        if (value.equals("0")) {
            LikesTextview.setVisibility(View.GONE);
        } else {
            LikesTextview.setVisibility(View.VISIBLE);
            if (value.equals("1")) {
                LikesTextview.setText(value + " like");
            } else {
                LikesTextview.setText(value + " likes");
            }
        }
    }

    private void dislikesVisibilityCheckAndSettingDisLikesValue(String value) {
        if (value.equals("0")) {
            DisLikesTextview.setVisibility(View.GONE);
        } else {
            DisLikesTextview.setVisibility(View.VISIBLE);
            if (value.equals("1")) {
                DisLikesTextview.setText(value + " dislike");
            } else {
                DisLikesTextview.setText(value + " dislikes");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(ExplainingShoutActivity.this).unregisterReceiver(updatingCommentValueReceiver);
        if (mediaPlayer!=null && mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                    boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (write_str && read_str && record_str) {
                        Toast.makeText(ExplainingShoutActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ExplainingShoutActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }

    private BroadcastReceiver updatingCommentValueReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newComentVlue = intent.getStringExtra("newTotalCommentValues");
            commentsVisibilityCheckAndSettingCommentsValue(newComentVlue);
            totalUpdatedComentsValue = newComentVlue;
        }
    };

    Emitter.Listener get_comments_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //region GettingCommentsResponse
                    CommentsArraylist.clear();
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        for (int i = 0; i < jsonObject.getJSONArray("comments").length(); i++) {
                            String Commentid = jsonObject.getJSONArray("comments").getJSONObject(i).getString("id");
                            String shoutID = jsonObject.getJSONArray("comments").getJSONObject(i).getString("shoutID");
                            String username = jsonObject.getJSONArray("comments").getJSONObject(i).getString("username");
                            String name = jsonObject.getJSONArray("comments").getJSONObject(i).getString("name");
                            String dp = jsonObject.getJSONArray("comments").getJSONObject(i).getString("dp");
                            String role = jsonObject.getJSONArray("comments").getJSONObject(i).getString("role");
                            String age = jsonObject.getJSONArray("comments").getJSONObject(i).getString("age");
                            String gender = jsonObject.getJSONArray("comments").getJSONObject(i).getString("gender");
                            String text = jsonObject.getJSONArray("comments").getJSONObject(i).getString("text");
                            String time = jsonObject.getJSONArray("comments").getJSONObject(i).getString("time");
                            String owner = jsonObject.getJSONArray("comments").getJSONObject(i).getString("owner");
                            CommentsArraylist.add(new CommentsSectionModelClass(Commentid, shoutID, username, name, dp, role, age, gender, text, time, owner));
                        }
                        commentsRepliesAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //endregion
                }
            });
        }
    };

    Emitter.Listener get_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //region GettingShoutResponse
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {

                        String success = jsonObject.getString("success");
                        UserNameWhoseShoutIs = jsonObject.getString("username");
                        String name = jsonObject.getString("name");
                        String time = jsonObject.getString("time");
                        UserShoutedtext = jsonObject.getString("text");
                        String privacy = jsonObject.getString("privacy");
                        String comments = jsonObject.getString("comments");
                        String like = jsonObject.getString("like");
                        ismyLikeOnPostNewValue = like;
                        String dislike = jsonObject.getString("dislike");
                        ismyDisLikeOnPostNewValue = dislike;
                        String likes = jsonObject.getString("likes");
                        String dislikes = jsonObject.getString("dislikes");
                        String reshouts = jsonObject.getString("reshouts");
                        String follow = jsonObject.getString("follow");
                        String role = jsonObject.getString("role");
                        String age = jsonObject.getString("age");
                        String gender = jsonObject.getString("gender");
                        String id = jsonObject.getString("id");
                        String ProfilePicUrl = jsonObject.getString("dp");
                        if (success.equalsIgnoreCase("1")) {
                            UserRealName.setText(name);
                            UserAccountName.setText("@" + UserNameWhoseShoutIs);
                            //  Picasso.get().load(baseUrl + ProfilePicUrl).into(UsersPictureView);
                            GlideApp.with(ExplainingShoutActivity.this).load(baseUrl + ProfilePicUrl).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    return false;
                                }
                            }).
                                    into(UsersPictureView);
                            if (UserShoutedtext.contains("www.youtube.com") || UserShoutedtext.contains("https://youtu.be/")) {
                                final String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(UserShoutedtext);
                                // YoutubeVideoImage.setVisibility(View.VISIBLE);
                                YoutubeUrlImageViewCardview.setVisibility(View.VISIBLE);
                                SimpleImageView.setVisibility(View.GONE);
                                SimpleImageViewCardview.setVisibility(View.GONE);
                                simpleVideoPlayerView.setVisibility(View.GONE);
                                audioViewCardview.setVisibility(View.GONE);
                                UserPostedText.setText(UserShoutedtext);

                                youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                                    @Override
                                    public void onReady(@NonNull YouTubePlayer initializdyouTubePlayer) {
                                        initializdyouTubePlayer.loadVideo(VideoIdOfYoutubeLink, 0);
                                    }
                                });
                                //  Picasso.get().load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/maxresdefault.jpg").into(YoutubeVideoImage);

                            } else if (UserShoutedtext.contains("[[") && UserShoutedtext.contains("]]")) {
                                //   else if (UserShoutedtext.matches("/\\[\\[(.+?)]]/g")) {
                                //  YoutubeVideoImage.setVisibility(View.GONE);
                                //check krna k text me Uri hy ya nahi, agr nahi hy to smiles me convrt krwana

                                if (UserShoutedtext.split("\\[\\[")[0].contains("http") &&
                                        UserShoutedtext.split("\\[\\[")[0].contains(".com")) {
                                    UserPostedText.setText(UserShoutedtext.split("\\[\\[")[0]);
                                } else {
                                    UserPostedText.setText(MyClient.shortnameToUnicode(UserShoutedtext.split("\\[\\[")[0]));
                                }
                                YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                                simpleVideoPlayerView.setVisibility(View.GONE);
                                SimpleImageView.setVisibility(View.VISIBLE);
                                SimpleImageViewCardview.setVisibility(View.VISIBLE);
                                audioViewCardview.setVisibility(View.GONE);

                                Glide.with(ExplainingShoutActivity.this)
                                        .load(baseUrl + UserShoutedtext.split("\\[\\[")[1].replace("]]", "").trim())
                                        .into(SimpleImageView);
                            } else if (UserShoutedtext.contains("|||")) {
                                YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                                SimpleImageView.setVisibility(View.GONE);
                                SimpleImageViewCardview.setVisibility(View.GONE);
                                simpleVideoPlayerView.setVisibility(View.VISIBLE);
                                audioViewCardview.setVisibility(View.GONE);

                                if (UserShoutedtext.split("\\|\\|\\|")[0].contains("http") &&
                                        UserShoutedtext.split("\\|\\|\\|")[0].contains(".com")) {
                                    UserPostedText.setText(UserShoutedtext.split("\\|\\|\\|")[0]);
                                } else {
                                    UserPostedText.setText(MyClient.shortnameToUnicode(UserShoutedtext.split("\\|\\|\\|")[0]));
                                }
                                runVideoOnVideoPlayer(UserShoutedtext.split("\\|\\|\\|")[1]);

                            } else if (UserShoutedtext.contains("{{") && UserShoutedtext.contains("}}") ) {
                                YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                                SimpleImageView.setVisibility(View.GONE);
                                SimpleImageViewCardview.setVisibility(View.GONE);
                                simpleVideoPlayerView.setVisibility(View.GONE);
                                audioViewCardview.setVisibility(View.VISIBLE);
                                Log.e("testing","inMethod");
                                UserPostedText.setText(MyClient.shortnameToUnicode(UserShoutedtext.split("\\{\\{")[0]));
                                prepareMediaPlayer(UserShoutedtext.split("\\{\\{")[1].replace("}}",""));

                            } else {
                                //  YoutubeVideoImage.setVisibility(View.GONE);
                                YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                                SimpleImageView.setVisibility(View.GONE);
                                SimpleImageViewCardview.setVisibility(View.GONE);

                                //check krna k text me Uri hy ya nahi, agr nahi hy to smiles me convrt krwana
                                if (UserShoutedtext.contains("http") && UserShoutedtext.contains(".com")) {
                                    Log.e("StringMatcher", "inIf");
                                    UserPostedText.setText(UserShoutedtext);
                                } else if (UserShoutedtext.contains("https://")) {
                                    Log.e("StringMatcher", "inelsewIf");
                                    UserPostedText.setText(UserShoutedtext);
                                } else {
                                    Log.e("StringMatcher", "InElse");
                                    UserPostedText.setText(MyClient.shortnameToUnicode(UserShoutedtext));
                                }
                            }
                            PostTimeTextview.setText(GettingPostingTime(Long.parseLong(time)));

                            likesVisibilityCheckAndSettingLikesValue(likes);
                            dislikesVisibilityCheckAndSettingDisLikesValue(dislikes);
                            commentsVisibilityCheckAndSettingCommentsValue(comments);
                            totalUpdatedLikesValue = likes;
                            totalUpdatedDisLikesValue = dislikes;
                            totalUpdatedComentsValue = comments;
                            Glide.with(ExplainingShoutActivity.this).load(baseUrl + mySingleTon.dp).into(NewCommentUserDp);

                            if (role.equals("1")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.user_icon).into(UserRank);
                            } else if (role.equals("2")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.mod_icon).into(UserRank);
                            } else if (role.equals("3")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.staff_icon).into(UserRank);
                            } else if (role.equals("4")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.mentor_icon).into(UserRank);
                            } else if (role.equals("5")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.merchant_icon).into(UserRank);
                            }
                        }

                        if (age.equals("1"))
                        {
                            if (gender.equals("M"))
                            {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.born_male).into(UserAge);
                            }
                            else {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.born_female).into(UserAge);
                            }
                        } else if (age.equals("2")) {
                            if (gender.equals("M")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.teen_male).into(UserAge);
                            } else {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.teen_female).into(UserAge);
                            }
                        } else if (age.equals("3")) {
                            if (gender.equals("M")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.young_male).into(UserAge);
                            } else {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.young_female).into(UserAge);
                            }
                        } else if (age.equals("4")) {
                            if (gender.equals("M")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.adult_male).into(UserAge);
                            } else {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.adult_female).into(UserAge);
                            }
                        } else if (age.equals("5")) {
                            if (gender.equals("M")) {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.mature_male).into(UserAge);
                            } else {
                                Glide.with(ExplainingShoutActivity.this).load(R.drawable.mature_female).into(UserAge);
                            }
                        }

                        if (like.equals("1")) {
                            Glide.with(ExplainingShoutActivity.this).load(R.drawable.heart_solid).into(LikeImageview);
                            LikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                    R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                        } else {
                            Glide.with(ExplainingShoutActivity.this).load(R.drawable.heart_blank).into(LikeImageview);
                            LikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                    R.color.dullwhite), PorterDuff.Mode.SRC_IN);
                        }
                        if (dislike.equals("1")) {
                            Glide.with(ExplainingShoutActivity.this).load(R.drawable.dislike_black).into(DislikeImageview);
                            DislikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                    R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                        } else {
                            Glide.with(ExplainingShoutActivity.this).load(R.drawable.dislike_gray).into(DislikeImageview);
                            DislikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                    R.color.dullwhite), PorterDuff.Mode.SRC_IN);
                        }

                        hidingLoader();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //endregion
                }
            });
        }
    };

    Emitter.Listener add_comments_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("ComntTestng", "add_comments_response");
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        String success = jsonObject.getString("success");
                        String message = jsonObject.getString("message");
                        String id = jsonObject.getString("id");
                        String total = jsonObject.getString("total");
                        hidingCommentGif();
                        ReplyBtn.setEnabled(true);
                        totalUpdatedComentsValue = total;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener delete_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        String success = jsonObject.getString("success");
                        String id = jsonObject.getString("id");

                        if (success.equals("1")) {

                            Toast.makeText(ExplainingShoutActivity.this, "DeleteThisShout", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("MyMESSAGE", id);
                            setResult(RESULT_OK, intent);
                            finish();//finishing activity
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener block_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];

                    try {
                        String success = jsonObject.getString("success");
                        String id = jsonObject.getString("id");

                        if (success.equals("1")) {
                            Intent intent = new Intent();
                            intent.putExtra("MyMESSAGE", id);
                            setResult(RESULT_OK, intent);
                            finish();//finishing activitres
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    Emitter.Listener block_shout_user_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];

                    try {
                        String success = jsonObject.getString("success");
                        String blockingusername = jsonObject.getString("username");

                        if (success.equals("1")) {
                            Intent intent = new Intent();
                            intent.putExtra("blockedShoutsOfUsername", blockingusername);
                            setResult(RESULT_FIRST_USER, intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("BlockingUser", "exception");
                    }
                }
            });


        }
    };

    Emitter.Listener like_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("LikesChecking", "inListener");
            Log.e("testingLike", "inLikesListener");
            runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];

                @Override
                public void run() {

                    try {
                        Log.e("LikesChecking", "inTry");
                        String success = jsonObject.getString("success");
                        String total = jsonObject.getString("total");
                        String id = jsonObject.getString("id");
                        String type = jsonObject.getString("type");

                        if (success.equals("1")) {
                            Log.e("LikesChecking", id);
                            Log.e("LikesChecking", "inSuccess");

                            if (type.equals("like")) {
                                Log.e("LikesChecking", "inIf");
                                LikeImageview.setImageResource(R.drawable.heart_solid);
                                LikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                        R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                                likesVisibilityCheckAndSettingLikesValue(total);
                                totalUpdatedLikesValue = total;
                                ismyLikeOnPostNewValue = "1";
                                //  sendingBroadcastToUpdateValues(id,total,"1","updateLikes");
                            } else {
                                Log.e("LikesChecking", "inElse");
                                LikeImageview.setImageResource(R.drawable.heart_blank);
                                LikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                        R.color.dullwhite), PorterDuff.Mode.SRC_IN);
                                likesVisibilityCheckAndSettingLikesValue(total);
                                totalUpdatedLikesValue = total;
                                ismyLikeOnPostNewValue = "0";
                                //   sendingBroadcastToUpdateValues(id,total,"0","updateLikes");
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("LikesChecking", "inException");
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void sendingBroadcastToUpdateValues(String MainShoutID2, String totalUpdatedLikesValue2, String totalUpdatedDisLikesValue2,
                                                String totalUpdatedComentsValue2, String ismyLikeOnPostNewValue2, String ismyDisLikeOnPostNewValue2) {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(ExplainingShoutActivity.this);
        Intent intent = new Intent("updateLikesEtcOnPost");
        intent.putExtra("MainShoutID2", MainShoutID2);
        intent.putExtra("totalUpdatedLikesValue2", totalUpdatedLikesValue2);
        intent.putExtra("totalUpdatedDisLikesValue2", totalUpdatedDisLikesValue2);
        intent.putExtra("totalUpdatedComentsValue2", totalUpdatedComentsValue2);
        intent.putExtra("ismyLikeOnPostNewValue2", ismyLikeOnPostNewValue2);
        intent.putExtra("ismyDisLikeOnPostNewValue2", ismyDisLikeOnPostNewValue2);
        localBroadcastManager.sendBroadcast(intent);
    }

    Emitter.Listener dislike_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];
                @Override
                public void run() {
                    try {
                        Log.e("testingLike", "inDisLikesListener");
                        String success = jsonObject.getString("success");
                        String total = jsonObject.getString("total");
                        String id = jsonObject.getString("id");
                        String type = jsonObject.getString("type");

                        if (success.equals("1")) {

                            if (type.equals("dislike_yes")) {
                                DislikeImageview.setImageResource(R.drawable.dislike_black);
                                DislikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                        R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                                dislikesVisibilityCheckAndSettingDisLikesValue(total);
                                totalUpdatedDisLikesValue = total;
                                ismyDisLikeOnPostNewValue = "1";
                                //  sendingBroadcastToUpdateValues(id,total,"1","updateDisLikes");
                            } else {
                                DislikeImageview.setImageResource(R.drawable.dislike_gray);
                                DislikeImageview.setColorFilter(ContextCompat.getColor(ExplainingShoutActivity.this,
                                        R.color.dullwhite), PorterDuff.Mode.SRC_IN);
                                dislikesVisibilityCheckAndSettingDisLikesValue(total);
                                totalUpdatedDisLikesValue = total;
                                ismyDisLikeOnPostNewValue = "0";
                                //     sendingBroadcastToUpdateValues(id,total,"0","updateDisLikes");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener new_comment = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("ComntTestng", "InNewComnt");
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        String NewCommentedShoutID = jsonObject.getString("shoutID");
                        if (NewCommentedShoutID.equals(MainShoutID)) {
                            String NewCommentedowner = jsonObject.getString("owner");
                            String NewCommentedcommenter = jsonObject.getString("commenter");
                            String NewCommentedtotalcomments = jsonObject.getString("total");
                            String NewlyCommente = jsonObject.getString("comment");
                            String NewCommentedcommentID = jsonObject.getString("commentID");
                            String NewCommentedtime = jsonObject.getString("time");
                            String NewCommentedrole = jsonObject.getString("role");
                            String NewCommentedage = jsonObject.getString("age");
                            String NewCommentedusername = jsonObject.getString("name");
                            String NewCommentedgender = jsonObject.getString("gender");
                            String NewCommenteddp = jsonObject.getString("dp");
                            commentsVisibilityCheckAndSettingCommentsValue(NewCommentedtotalcomments);
                            CommentsArraylist.add(new CommentsSectionModelClass(NewCommentedcommentID, NewCommentedShoutID, NewCommentedcommenter,
                                    NewCommentedusername, NewCommenteddp,
                                    NewCommentedrole, NewCommentedage, NewCommentedgender, NewlyCommente, NewCommentedtime, NewCommentedowner));
                            commentsRepliesAdapter.notifyDataSetChanged();
                            CommentsRecyclerview.smoothScrollToPosition(CommentsArraylist.size() - 1);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener update_shout_details = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e("testingLike", "inupdate_shout_details");
                        JSONObject jsonObject = (JSONObject) args[0];
                        String Updated_shoutId = jsonObject.getString("shoutID");
                        String Updated_shoutType = jsonObject.getString("type");
                        String Updated_shoutValue = jsonObject.getString("value");

                        if (Updated_shoutId.equals(MainShoutID)) {
                            if (Updated_shoutType.equals("total_likes")) {
                                likesVisibilityCheckAndSettingLikesValue(Updated_shoutValue);
                                totalUpdatedLikesValue = Updated_shoutValue;
                                Log.e("ddd", "like");
                            } else if (Updated_shoutType.equals("total_comment")) {
                                commentsVisibilityCheckAndSettingCommentsValue(Updated_shoutValue);
                                totalUpdatedComentsValue = Updated_shoutValue;
                                Log.e("ddd", "cmnt");
                            } else if (Updated_shoutType.equals("total_dislikes")) {
                                dislikesVisibilityCheckAndSettingDisLikesValue(Updated_shoutValue);
                                totalUpdatedDisLikesValue = Updated_shoutValue;
                                Log.e("ddd", "dislike");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

}
