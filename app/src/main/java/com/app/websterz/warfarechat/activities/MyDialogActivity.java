package com.app.websterz.warfarechat.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.appolica.flubber.Flubber;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class MyDialogActivity extends Activity {


    MediaPlayer mediaPlayer;
    String type, userdp, userbuzzed;
    Button update_status, cancel_update_status;
    ImageView errorDialogVerificationImg, errorDialogHeaderImg;
    TextView validation_textview_status;
    RelativeLayout parent_alert;
    MySingleTon mySingleTon;
    CircleImageView circle_errorDialogHeaderImg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dialog);


        mySingleTon = MySingleTon.getInstance();
        Intent intent = getIntent();
        type = intent.getExtras().getString("type");
        userdp = intent.getExtras().getString("userdp");
        userbuzzed = intent.getExtras().getString("userbuzzed");
        Log.e("buzzing","oncreate buzz");

            parent_alert=findViewById(R.id.parent_alert);
        MySocketsClass.static_objects.global_layout=parent_alert;
            cancel_update_status = findViewById(R.id.cancel_update_status);
            cancel_update_status.setText("Close");
            cancel_update_status.setBackground(ContextCompat.getDrawable(MyDialogActivity.this, R.drawable.error_dialog_white_btn));
            update_status = findViewById(R.id.update_status);
            update_status.setText("Buzz Back");
            update_status.setBackground(ContextCompat.getDrawable(MyDialogActivity.this, R.drawable.error_dialog_green_btn));
            errorDialogVerificationImg = findViewById(R.id.errorDialogVerificationImg);
        errorDialogVerificationImg.setVisibility(View.GONE);
            validation_textview_status = findViewById(R.id.validation_textview_status);
            validation_textview_status.setVisibility(View.VISIBLE);
            validation_textview_status.setText(userbuzzed + " buzzed you!!!");
            validation_textview_status.setTextColor(getResources().getColor(R.color.colorBlack));
            errorDialogHeaderImg = findViewById(R.id.errorDialogHeaderImg);
            circle_errorDialogHeaderImg=findViewById(R.id.circle_errorDialogHeaderImg);
            circle_errorDialogHeaderImg.setVisibility(View.VISIBLE);
        Glide.with(MyDialogActivity.this).load(baseUrl + userdp).into(circle_errorDialogHeaderImg);
            final EditText edit_text_status_update = findViewById(R.id.edit_text_status_update);
            edit_text_status_update.setVisibility(View.GONE);
        Flubber.with()
                .animation(Flubber.AnimationPreset.SWING)
                .repeatCount(2)
                .duration(1000)
                .createFor(errorDialogHeaderImg)
                .start();
            vibration();
            sound();
            cancel_update_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MySocketsClass.static_objects.one_time_buzzer=null;
                    finish();

                }
            });
            update_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("username", userbuzzed);
                        mSocket.emit(MySocketsClass.EmmittersClass.REQUESTING_BUZZ, jsonObject);
                        MySocketsClass.static_objects.one_time_buzzer=null;
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
    }
        public void vibration() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
// Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
    }

    public void sound() {
        AudioManager audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.getRingerMode()==AudioManager.RINGER_MODE_NORMAL) {
            mediaPlayer = MediaPlayer.create(this, R.raw.buzz_tune);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer.release();
                }
            });
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        MySocketsClass.static_objects.one_time_buzzer=null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MySocketsClass.static_objects.one_time_buzzer=null;
    }
    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity=MyDialogActivity.this;
    }
}

