package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.Liking_DislikingShoutDetailsModelClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;

public class DislikeShoutDetailAdapter extends RecyclerView.Adapter<DislikeShoutDetailAdapter.ViewHolder> {
    ArrayList<Liking_DislikingShoutDetailsModelClass> dislikesArraylist;
    Context myContext;


    public DislikeShoutDetailAdapter(ArrayList<Liking_DislikingShoutDetailsModelClass> dislikesArraylist, Context myContext) {
        this.myContext = myContext;
        this.dislikesArraylist = dislikesArraylist;
    }

    @NonNull
    @Override
    public DislikeShoutDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.liking_dislikng_shout_details_row_layout, parent, false);
        return new DislikeShoutDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DislikeShoutDetailAdapter.ViewHolder holder, final int position) {

        if (dislikesArraylist.size() == 0) {
            holder.nodisLikesTextView.setVisibility(View.VISIBLE);
            holder.mainParentLayout.setVisibility(View.GONE);
            holder.nodisLikesTextView.setText("There are 0 dislikes on this shout.");
        } else {
            holder.nodisLikesTextView.setVisibility(View.GONE);
            holder.mainParentLayout.setVisibility(View.VISIBLE);
            holder.ProfileavatarLoader.setVisibility(View.VISIBLE);
            holder.ProfileavatarLoader.setAnimating(true);

            Glide.with(holder.itemView.getContext()).load(baseUrl + dislikesArraylist.get(position).getDp()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    holder.ProfileavatarLoader.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.userDp);
            holder.user_name.setText(dislikesArraylist.get(position).getUserName());
            holder.detailTime.setText(GettingPostingTime(Long.parseLong(dislikesArraylist.get(position).getDate())) + GettingExactTime(Long.parseLong(dislikesArraylist.get(position).getDate())));
        }
    }

    @Override
    public int getItemCount() {
        int sizeTotal;
        if (dislikesArraylist.size() > 0) {
            sizeTotal = dislikesArraylist.size();
        } else {
            sizeTotal = 1;
        }
        return sizeTotal;
    }

    private String GettingPostingTime(long time_reg) {
        String last_final_time_reg = null;
        long time_reg2 = time_reg * 1000;
        long now = System.currentTimeMillis();
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "shouted just now at ";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "shouted a minute ago at ";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg = "shouted " + diff2 / MINUTE_MILLIS + " minutes ago at ";
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "shouted an hour ago at ";
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago at ";
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "shouted yesterday at ";
        } else {

            long calculating_months = diff2 / DAY_MILLIS;
            if (calculating_months < 30) {
                last_final_time_reg = "shouted " + diff2 / DAY_MILLIS + " days ago on ";
            } else if (calculating_months >= 30 && calculating_months <= 360) {
                last_final_time_reg = "shouted " + calculating_months / 30 + " Month(s) ago on ";
            } else if (calculating_months >= 30 && calculating_months > 360) {
                last_final_time_reg = "shouted " + calculating_months / 360 + " Year(s) ago on ";
            }
        }
        return last_final_time_reg;
    }

    private String GettingExactTime(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("EEE, d MMM yyyy hh:mm:ss a", cal).toString();
        return date;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userDp;
        TextView user_name, detailTime, nodisLikesTextView;
        AvatarView ProfileavatarLoader;
        LinearLayout mainParentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            user_name = itemView.findViewById(R.id.user_name);
            userDp = itemView.findViewById(R.id.userDp);
            detailTime = itemView.findViewById(R.id.detailTime);
            ProfileavatarLoader = itemView.findViewById(R.id.ProfileavatarLoader);
            nodisLikesTextView = itemView.findViewById(R.id.noLikesTextView);
            mainParentLayout = itemView.findViewById(R.id.mainParentLayout);
        }
    }
}
