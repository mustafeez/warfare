package com.app.websterz.warfarechat.activities.ShopActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.BuyShields.BuyShieldActivity;
import com.app.websterz.warfarechat.activities.BuyShields.BuyShieldModelClass;
import com.app.websterz.warfarechat.activities.BuyShields.BuyShieldRecyclerViewAdapter;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketAddress;
import java.util.ArrayList;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class ShopActivity extends AppCompatActivity {

    RelativeLayout parentRelativeLayout;
    RecyclerView recyclerView;
    SocialPackModelClass socialPackModelClass;
    ArrayList<SocialPackModelClass> SocialPackArrayList;
    SocialPackRecyclerViewAdapter socialPackRecyclerViewAdapter;
    Context context;

    String  socialpackImage,socialpackName,socialpackDetail, socialpackValidity,socialpackSlug,socialpackCost,socialpackDate,socialpackRemaning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        context = ShopActivity.this;
        initializations();
        initview();
    }

    private void initview() {

        mSocket.emit(MySocketsClass.EmmittersClass.GET_SHOP_PACKAGES);
        mSocket.on(MySocketsClass.ListenersClass.GET_SHOP_PACKAGES_RESPONSE, get_shop_packages_response);
    }

    private void initializations() {

        MySocketsClass.static_objects.MyStaticActivity = ShopActivity.this;
        SocialPackArrayList = new ArrayList<>();
        parentRelativeLayout = findViewById(R.id.parent_layout_shop);
        MySocketsClass.static_objects.global_layout=parentRelativeLayout;
        recyclerView = findViewById(R.id.ShopRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    private Emitter.Listener get_shop_packages_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        for(int i =0; i < jsonObject.getJSONArray("packages").length(); i++){

                            socialpackName  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("name");
                            socialpackDetail  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("details");
                            socialpackCost  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("cost");
                            socialpackValidity  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("validity");
                            socialpackSlug  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("slug");
                            socialpackImage  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("img");
                            socialpackDate  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("date");
                            socialpackRemaning  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("remaining");

                            socialPackModelClass = new SocialPackModelClass(socialpackImage,socialpackName,socialpackDetail,socialpackValidity,socialpackCost,socialpackSlug,socialpackDate,socialpackRemaning);
                            SocialPackArrayList.add(socialPackModelClass);

                        }
                        setAdapter(SocialPackArrayList);

                        jsonObject.getString("success");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    private void setAdapter(ArrayList<SocialPackModelClass> SocialPackArrayList2) {
        ArrayList<SocialPackModelClass> socialPackModelClasses =  new ArrayList<>();
        socialPackModelClasses = SocialPackArrayList2;
        socialPackRecyclerViewAdapter = new SocialPackRecyclerViewAdapter(socialPackModelClasses,  this);
        recyclerView.setAdapter(socialPackRecyclerViewAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mSocket.off(MySocketsClass.ListenersClass.GET_SHOP_PACKAGES_RESPONSE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity=ShopActivity.this;
    }

    public void FinishShopActivity(Activity activity){
        activity.finish();
    }
}
