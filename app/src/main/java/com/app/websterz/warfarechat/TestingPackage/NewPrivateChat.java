package com.app.websterz.warfarechat.TestingPackage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.schwaab.avvylib.AvatarView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.Models.UserPricateChatImageUploading;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.Rest.ApiClient;
import com.app.websterz.warfarechat.Rest.ApiInterface;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsModelClass;
import com.app.websterz.warfarechat.activities.PrivateChat.UserPrivateChatGiftsRecyclerAdpater;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.NewShoutActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.MultiPartsClassesForWallImagePost.WallImagePostModelClass;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ScrollingClass.MediaLoader;
import com.app.websterz.warfarechat.homeScreen.fragments.chats.NewChatModelClass;
import com.app.websterz.warfarechat.myRoomDatabase.UserPrivateChatTables;
import com.app.websterz.warfarechat.myRoomDatabase.WarfareDatabase;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.adapter.image.impl.GlideAdapter;
import com.sangcomz.fishbun.adapter.image.impl.PicassoAdapter;
import com.sangcomz.fishbun.define.Define;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessagesList;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;
import static com.stfalcon.chatkit.messages.MessageHolders.IncomingTextMessageViewHolder.incomingVoicePlayer;
import static com.stfalcon.chatkit.messages.MessageHolders.OutcomingTextMessageViewHolder.outcomingVoicePlayer;

public class NewPrivateChat extends AppCompatActivity {

    RecyclerView GiftsRecyclerView;
    public static MessagesList privateChatRecyclerView;
    MySingleTon mySingleTon = MySingleTon.getInstance();
    public WarfareDatabase warfareDatabase;
    UserPrivateChatTables userPrivateChatTables = null;
    String friendChatName, username, reqStatus, presence, BackActivity, UserDp, PersnalChatMessage, ImageStatus;
    CircleImageView userDpImageProfile;
    TextView userFriendName, friend_name;
    ImageView friendRoleIcon, friendAgeIcon, smileySendButton, privateChatSettingIcon, ScrollToBottomImageViewChats, imageSendButton,
            giftSendButton, messageSendButton, voiceSendButton;
    EmojIconActions emojIcon;
    EmojiconEditText messageText;
    LinearLayout gifts_view;
    NewChatModelClass newChatModelClass;
    ArrayList<GiftsModelClass> GiftsList = new ArrayList<GiftsModelClass>();
    UserPrivateChatGiftsRecyclerAdpater userPrivateChatGiftsRecyclerAdpater;
    GiftsModelClass giftsModelClass;
    LinearLayout returning_imageview = null;
    RelativeLayout ParentView;
    AvatarView ProfileAvatarLoader, FriendRoleAvatarLoader, FriendAgeAvatarLoader;
    Client MyClient;
    MediaRecorder mediaRecorder;
    private float x1, x2;
    static final int MIN_DISTANCE = 230;
    LinearLayout swipeToCancelLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_private_chat);

        Initializations();
        MyCode();

        privateChatRecyclerView.addOnScrollListener(new MessagesList.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    ScrollToBottomImageViewChats.setVisibility(View.GONE);
                } else {
                    ScrollToBottomImageViewChats.setVisibility(View.VISIBLE);
                }
            }
        });
        ScrollToBottomImageViewChats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                privateChatRecyclerView.scrollToPosition(0);
                ScrollToBottomImageViewChats.setVisibility(View.GONE);
            }
        });

    }

    public void Initializations() {
        ProfileAvatarLoader = findViewById(R.id.ChatProfileAvatarLoader);
        FriendRoleAvatarLoader = findViewById(R.id.FriendRoleAvatarLoader);
        FriendAgeAvatarLoader = findViewById(R.id.FriendAgeAvatarLoader);
        swipeToCancelLayout = findViewById(R.id.swipeToCancelLayout);
        ParentView = findViewById(R.id.ParentView);
        ScrollToBottomImageViewChats = findViewById(R.id.ScrollToBottomImageViewChats);
        privateChatRecyclerView = findViewById(R.id.privatechatrv);
        gifts_view = findViewById(R.id.gifts_view);
        userDpImageProfile = findViewById(R.id.userDp);
        userFriendName = findViewById(R.id.userFriendName);
        friendRoleIcon = findViewById(R.id.friendRoleIcon);
        friendAgeIcon = findViewById(R.id.friendAgeIcon);
        userPrivateChatTables = new UserPrivateChatTables();
        warfareDatabase = Room.databaseBuilder(NewPrivateChat.this, WarfareDatabase.class, "warfare_chat_db").allowMainThreadQueries().build();
        messageText = findViewById(R.id.messageText);
        smileySendButton = findViewById(R.id.smileySendButton);
        voiceSendButton = findViewById(R.id.voiceSendButton);
        privateChatSettingIcon = findViewById(R.id.privateChatSettingIcon);
        messageSendButton = findViewById(R.id.messageSendButton);
        imageSendButton = findViewById(R.id.imageSendButton);
        Glide.with(NewPrivateChat.this).load(R.drawable.private_chat_sending_gifts).into(imageSendButton);
        giftSendButton = findViewById(R.id.giftSendButton);
        GiftsRecyclerView = findViewById(R.id.gifts_recyclerview);
        GridLayoutManager giftslayoutmanager = new GridLayoutManager(NewPrivateChat.this, 4);
        GiftsRecyclerView.setLayoutManager(giftslayoutmanager);
        userPrivateChatGiftsRecyclerAdpater = new UserPrivateChatGiftsRecyclerAdpater(GiftsList, GiftsRecyclerView, NewPrivateChat.this);
        GiftsRecyclerView.setAdapter(userPrivateChatGiftsRecyclerAdpater);
        MyClient = new Client(NewPrivateChat.this);

    }

    @SuppressLint("ClickableViewAccessibility")
    public void MyCode() {

        Intent intent = getIntent();
        friendChatName = intent.getStringExtra("FriendChatName");
        BackActivity = intent.getStringExtra("BackActivity");
        MySocketsClass.static_objects.CurrrentPrivateChatUsername = friendChatName;
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        emojIcon = new EmojIconActions(this, viewGroup, messageText, smileySendButton);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);

        final JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject2 = new JSONObject();

        try {
            jsonObject.put("username", friendChatName);
            jsonObject.put("type", "online");
            jsonObject.put("direction", "1");
            jsonObject2.put("username", friendChatName);
            jsonObject2.put("type", "offline");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.off(MySocketsClass.ListenersClass.START_PRIVATE_CHAT_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.START_PRIVATE_CHAT, jsonObject);
        mSocket.on(MySocketsClass.ListenersClass.START_PRIVATE_CHAT_RESPONSE, start_private_chat_response);
        List<UserPrivateChatTables> totalDataChatContactsList = MySocketsClass.getInstance().getUserChat(friendChatName, NewPrivateChat.this);
        if (totalDataChatContactsList.size() == 0 || BackActivity.equals("contactsChats")) {
            mSocket.emit(MySocketsClass.EmmittersClass.LOAD_OLD_MESSAGES, jsonObject2);
            returning_imageview = MySocketsClass.custom_layout.Loader_image(NewPrivateChat.this, ParentView);
            mSocket.on(MySocketsClass.ListenersClass.LOAD_OLD_MESSAGES, load_old_messages);
            Log.e("duplications1", "old messages");


        } else {

            Log.e("Chatting", friendChatName);
            List<UserPrivateChatTables> _userPrivateChatTables = MySocketsClass.getInstance().getUserChat(friendChatName, NewPrivateChat.this);
            MySocketsClass.static_objects.MessageListAdapter.clear();
            if (MySocketsClass.static_objects.TotaldatabaseMessagesHashMap == _userPrivateChatTables.size()) {
                // 1st time agr chat open ho to old mesge load krny
                mSocket.emit(MySocketsClass.EmmittersClass.LOAD_OLD_MESSAGES, jsonObject2);
                returning_imageview = MySocketsClass.custom_layout.Loader_image(NewPrivateChat.this, ParentView);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_OLD_MESSAGES, load_old_messages);
                Log.e("duplications1", "old messages but in database side");
            } else {
// retreiving friom db
                Log.e("duplications1", "db messages");
                for (UserPrivateChatTables userPrivateChatTables : _userPrivateChatTables) {
                    String myDatabasechattingwith;
                    String myDatabaseMsg = userPrivateChatTables.getChatMessage();
                    String myDatabaseDate = userPrivateChatTables.getMessageDate();
                    String myDatabaseDp = userPrivateChatTables.getMessageUserDp();
                    String myDatabaseMsgstatus = userPrivateChatTables.getChatStatus();
                    if (myDatabaseDate.equalsIgnoreCase("SystemMsg")) {
                        myDatabasechattingwith = "System";
                        Log.e("dbSys", "SystemMsgSave");
                    } else {
                        myDatabasechattingwith = userPrivateChatTables.getChatWith();
                    }
                    String messageType = userPrivateChatTables.getChatMessageType();

                    if (messageType.equals("1")) {
                        //simple messages....
                        if (!myDatabaseDate.contains("SystemMsg")) {
                            // Retriving simple message
                            MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(myDatabaseMsgstatus,
                                    new UserModelClass(myDatabaseMsgstatus, myDatabasechattingwith, myDatabaseDp, true),
                                    myDatabaseMsg, DateConvertor(Long.valueOf(myDatabaseDate) * 1000)), true);
                            Log.e("dbtestt", myDatabasechattingwith + "  " + myDatabaseMsg);
                        } else {
                            // Retriving system message
                            MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(myDatabaseMsgstatus,
                                    new UserModelClass(myDatabaseMsgstatus, "System", myDatabaseDp, true),
                                    myDatabaseMsg, DateConvertor(Long.valueOf(RetrivingSystemMeessageDate(myDatabaseDate)) * 1000)), true);

                        }
                    } else if (messageType.equals("3")) {
                        // retriving voice message
                        MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(myDatabaseMsgstatus,
                                new UserModelClass(myDatabaseMsgstatus, myDatabasechattingwith, myDatabaseDp, true),
                                "MyVoice_Note" + myDatabaseMsg, DateConvertor(Long.valueOf(myDatabaseDate) * 1000)), true);
                    } else {

                        // retriving image mesage
                        MessageModelClass messageModelClass = new MessageModelClass(myDatabaseMsgstatus,
                                new UserModelClass(myDatabaseMsgstatus, myDatabasechattingwith, myDatabaseDp, true), null);

                        messageModelClass.setImage(new MessageModelClass.Image(baseUrl + myDatabaseMsg));
                        MySocketsClass.static_objects.MessageListAdapter.addToStart(messageModelClass, true);
                    }
                    privateChatRecyclerView.setAdapter(MySocketsClass.static_objects.MessageListAdapter);
                }

            }
        }

        privateChatSettingIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenSettingPrivateChat();
            }
        });
        messageSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendingMessage();
            }
        });
        imageSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
        giftSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowingGifts();
            }
        });

//real
//        voiceSendButton.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//
//                if (CheckPermissions()) {
//
//                    if (ActivityCompat.checkSelfPermission(NewPrivateChat.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                        return false;
//                    } else {
//
//                        switch (event.getAction()) {
//                            case MotionEvent.ACTION_DOWN:
//
//                                voiceSendButton.setImageResource(R.drawable.microphone_clicked);
//                                startRecording();
//                                return true;
//
//                            case MotionEvent.ACTION_UP:
//                                voiceSendButton.setImageResource(R.drawable.microphone_unclicked);
//                                stopRecordingAndSendingEmit();
//                                return true;
//                        }
//                        return false;
//                    }
//                } else {
//                    requestPermissions();
//                }
//                return false;
//
//
//            }
//        });

//updated with animation
        voiceSendButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (CheckPermissions()) {

                    if (ActivityCompat.checkSelfPermission(NewPrivateChat.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        return false;
                    } else {

                        switch (motionEvent.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                Log.e("movingTest", "Action clicked ");
                                swipeToCancelLayout.setVisibility(View.VISIBLE);
                                messageText.setVisibility(View.GONE);
                                voiceSendButton.setImageResource(R.drawable.microphone_clicked);
                              //  startRecording();
                                x1 = motionEvent.getX();
                                break;
                            case MotionEvent.ACTION_UP:
                                x2 = motionEvent.getX();
                                float deltaX = x2 - x1;
                                if (Math.abs(deltaX) > MIN_DISTANCE) {
                                    Log.e("movingTest", "Action left ");
                                    swipeToCancelLayout.setVisibility(View.GONE);
                                    messageText.setVisibility(View.VISIBLE);
                                } else {
                                    // consider as something else - a screen tap for example
                                    Log.e("movingTest", "Action right or  up ");
                                    swipeToCancelLayout.setVisibility(View.GONE);
                                    messageText.setVisibility(View.VISIBLE);
                                }
                                break;
                        }
                        return true;

                    }


                } else {
                    requestPermissions();
                }

                return false;
            }
        });

    }

    private boolean CheckPermissions() {
        int recordingAudioPermissionResult = ContextCompat.checkSelfPermission(NewPrivateChat.this, RECORD_AUDIO);
        int savingAudioPermissionResult = ContextCompat.checkSelfPermission(NewPrivateChat.this, WRITE_EXTERNAL_STORAGE);
        return recordingAudioPermissionResult == PackageManager.PERMISSION_GRANTED &&
                savingAudioPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(NewPrivateChat.this, new String[]{
                RECORD_AUDIO,
                WRITE_EXTERNAL_STORAGE,
        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                    boolean record_aud = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean store_aud = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (record_aud && store_aud) {

                        Toast.makeText(NewPrivateChat.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NewPrivateChat.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }

    private String readBytesFromFile(String filePath) {
        File file = new File(filePath);
        String img64 = null;

        try {
            FileInputStream fis = new FileInputStream(file);
            byte imgByte[] = new byte[(int) file.length()];
            fis.read(imgByte);

            //convert byte array to base64 string
            img64 = Base64.encodeToString(imgByte, Base64.NO_WRAP);
            String img643 = Base64.encodeToString(imgByte, Base64.NO_WRAP);
            //send img64 to socket.io servr
        } catch (Exception e) {
            //
        }
        return img64;
    }

    private String generateRandomNumber() {
        Random random = new Random();
        return String.valueOf(random.nextInt());
    }

    public void OpenSettingPrivateChat() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View view = layoutInflater.inflate(R.layout.my_dialog_box, null);
        final AlertDialog.Builder adb = new AlertDialog.Builder(view.getContext());
        final AlertDialog alertDialog = adb.create();
        alertDialog.setView(view);
        ImageView block_unblock_friend;
        TextView block_friend_text, delete_textview;
        LinearLayout buzz, view_profile, block_friend, delete_friend;

        buzz = view.findViewById(R.id.buzz);
        view_profile = view.findViewById(R.id.view_profile);
        block_friend = view.findViewById(R.id.block_friend);
        block_unblock_friend = view.findViewById(R.id.block_unblock_friend);
        block_friend_text = view.findViewById(R.id.block_friend_text);
        delete_friend = view.findViewById(R.id.delete_friend);
        delete_textview = view.findViewById(R.id.delete_textview);
        delete_textview.setText("Close");

        friend_name = view.findViewById(R.id.user_name);
        friend_name.setText(username);

        if (reqStatus != null && reqStatus.startsWith("2")) {
            block_friend_text.setText("Unblock");
            block_unblock_friend.setImageResource(R.drawable.unblock_friend);
        } else if (reqStatus != null && reqStatus.startsWith("1") || reqStatus != null && reqStatus.startsWith("0")) {
            block_friend_text.setText("Block");
            block_unblock_friend.setImageResource(R.drawable.block_friend);
        }

        buzz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("username", username);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mSocket.emit(MySocketsClass.EmmittersClass.REQUESTING_BUZZ, jsonObject);
                alertDialog.dismiss();
            }
        });

        block_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                if (reqStatus.startsWith("2")) {

                    try {
                        jsonObject.put("username", username);
                        jsonObject.put("type", 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_USER, jsonObject);
                    reqStatus = "0:1";
                    alertDialog.dismiss();
                } else if (reqStatus.startsWith("0") || reqStatus.startsWith("1")) {
                    try {
                        jsonObject.put("username", username);
                        jsonObject.put("type", 2);
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                    mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_USER, jsonObject);
                    reqStatus = "2:1";
                    alertDialog.dismiss();
                }
            }
        });

        view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewPrivateChat.this, UserFriendProfile.class);
                intent.putExtra("username", username);
                startActivity(intent);
                alertDialog.dismiss();
            }
        });

        delete_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mSocket.off(MySocketsClass.ListenersClass.LOAD_OLD_MESSAGES, load_old_messages);
                MySocketsClass.static_objects.CurrrentPrivateChatUsername = "";
                finish();
            }
        });


        alertDialog.show();
    }

    public void SendingMessage() {

        VisiblityGoneForScroll();
        PersnalChatMessage = messageText.getText().toString();
        messageText.setText("");
        String GettingCurrentDate = GettingCurrentDtaeMethod();
        long TimeStampCurrentDate = Long.parseLong(GettingCurrentDate);

        if (PersnalChatMessage.equals("")) {
            Toast.makeText(this, "Message is empty.", Toast.LENGTH_SHORT).show();
        } else {
            if (MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(username)) {
                if (!PersnalChatMessage.startsWith("/")) {
                    if (!privateChatRecyclerView.canScrollVertically(1)) {
                        MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass("currentappUser",
                                new UserModelClass("currentappUser", friendChatName, null, true), PersnalChatMessage, DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp())), true);
                        privateChatRecyclerView.setAdapter(MySocketsClass.static_objects.MessageListAdapter);
                    } else {
                        MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass("currentappUser",
                                new UserModelClass("currentappUser", friendChatName, null, true), PersnalChatMessage, DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp())), false);
                        privateChatRecyclerView.setAdapter(MySocketsClass.static_objects.MessageListAdapter);
                    }

                    userPrivateChatTables.setChatWith(friendChatName);
                    userPrivateChatTables.setChatMessage(PersnalChatMessage);
                    userPrivateChatTables.setChatMessageType("1");
                    userPrivateChatTables.setMessageUserDp(mySingleTon.dp);
                    userPrivateChatTables.setMessageDate(GettingCurrentDate);
                    userPrivateChatTables.setChatStatus("currentappUser");
                    warfareDatabase.myDataBaseAccessObject().addChat(userPrivateChatTables);
                }
            }

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("username", friendChatName);
                jsonObject.put("message", PersnalChatMessage);
                jsonObject.put("chat_type", 1);
                mSocket.emit(MySocketsClass.EmmittersClass.SEND_PRIVATE, jsonObject);

                Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                while (MyIterator.hasNext()) {
                    NewChatModelClass newChatModelClass2 = MyIterator.next();

                    if (newChatModelClass2.getMessangerName().equals(friendChatName))
                        MyIterator.remove();
                }
                if (!PersnalChatMessage.startsWith("/")) {
                    newChatModelClass = new NewChatModelClass(UserDp, username, PersnalChatMessage, presence, 0, "Chat");
                    MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                    Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                    MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                }
                PersnalChatMessage = null;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isRecyclerScrollable(RecyclerView recyclerView) {
        return recyclerView.computeHorizontalScrollRange() > recyclerView.getWidth() || recyclerView.computeVerticalScrollRange() > recyclerView.getHeight();
    }

    private void openGallery() {

        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .build());

        Album.image(this)
                .singleChoice()
                .camera(false)
                .columnCount(3)
                .onResult(result -> {
                    if (result.size() > 0) {
                        Log.e("selector", result.toString());
                        String selectedImage = result.get(0).getPath();
                        String filePath;

                        if (selectedImage.contains(".gif")) {
                            filePath = selectedImage;
                        } else {
                            filePath = compressImage(selectedImage);
                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NewPrivateChat.this);
                        String username_service = prefs.getString("mine_user_name", null);
                        String Socket_ID = prefs.getString("sID", null);


                        List<MultipartBody.Part> parts = new ArrayList<>();
                        parts.add(prepareFilePart("image", filePath));

                        //avatarLoader.setVisibility(View.VISIBLE);

                        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                        Call<UserPricateChatImageUploading> call = apiInterface.privateChatImageSending(
                                createPartFromString(username_service),
                                createPartFromString(friendChatName),
                                parts,
                                createPartFromString(Socket_ID),
                                createPartFromString("send_image")
                        );
                        Log.e("ImageUploading", "In above call enque");

                        call.enqueue(new Callback<UserPricateChatImageUploading>() {
                            @Override
                            public void onResponse(Call<UserPricateChatImageUploading> call, Response<UserPricateChatImageUploading> response) {
                                Log.e("ImageUploading", "In response call enque");
                                String GettingCurrentDate = GettingCurrentDtaeMethod();
                                UserPricateChatImageUploading profileImage = response.body();
                                String ImageStatus = profileImage.getMessage();
                                String URL = profileImage.getUrl();
                                int Success = profileImage.getSuccess();
                                Log.e("ImageUploading", "success" + Success);
                                if (Success == 1) {
                                    Log.e("ImageUploading", "In success 1");

                                    if (MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(username)) {
                                        MessageModelClass messageModelClass = new MessageModelClass("currentappUser",
                                                new UserModelClass("currentappUser", friendChatName, null, true), null);

                                        messageModelClass.setImage(new MessageModelClass.Image(baseUrl + URL));
                                        MySocketsClass.static_objects.MessageListAdapter.addToStart(messageModelClass, true);


                                        userPrivateChatTables.setChatWith(friendChatName);
                                        userPrivateChatTables.setChatMessage(URL);
                                        userPrivateChatTables.setChatMessageType("2");
                                        userPrivateChatTables.setMessageUserDp(mySingleTon.dp);
                                        userPrivateChatTables.setMessageDate(GettingCurrentDate);
                                        userPrivateChatTables.setChatStatus("currentappUser");
                                        warfareDatabase.myDataBaseAccessObject().addChat(userPrivateChatTables);
                                    }

                                    JSONObject jsonObject = new JSONObject();
                                    try {
                                        jsonObject.put("username", friendChatName);
                                        jsonObject.put("message", PersnalChatMessage);
                                        jsonObject.put("chat_type", 2);
                                        mSocket.emit(MySocketsClass.EmmittersClass.SEND_PRIVATE, jsonObject);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                                    while (MyIterator.hasNext()) {
                                        NewChatModelClass newChatModelClass2 = MyIterator.next();

                                        if (newChatModelClass2.getMessangerName().equals(friendChatName))
                                            MyIterator.remove();
                                    }
                                    newChatModelClass = new NewChatModelClass(UserDp, username, "Image", presence, 0, "Chat");
                                    MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                                    Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                                    MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();

                                    Glide.with(NewPrivateChat.this).load(R.drawable.private_chat_sending_gifts).into(imageSendButton);

                                } else {
                                    Glide.with(NewPrivateChat.this).load(R.drawable.private_chat_sending_gifts).into(imageSendButton);
                                }
                            }

                            @Override
                            public void onFailure(Call<UserPricateChatImageUploading> call, Throwable t) {
                                Toast.makeText(NewPrivateChat.this, "Fail to send image.", Toast.LENGTH_SHORT).show();
                                Glide.with(NewPrivateChat.this).load(R.drawable.private_chat_sending_gifts).into(imageSendButton);
                            }
                        });
                    } else {
                        Glide.with(NewPrivateChat.this).load(R.drawable.private_chat_sending_gifts).into(imageSendButton);
                    }
                })
                .start();

    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String fileUri) {

        File file = new File(fileUri);

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        file
                );

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public String getPath(Uri uri) {
        int column_index;
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    @Override
    public void onBackPressed() {
        if (gifts_view.getVisibility() == View.VISIBLE) {
            gifts_view.setVisibility(View.GONE);
            giftSendButton.setImageResource(R.drawable.gift_solid);
        } else {
            mSocket.off(MySocketsClass.ListenersClass.LOAD_OLD_MESSAGES, load_old_messages);
            MySocketsClass.static_objects.CurrrentPrivateChatUsername = "";
            this.finish();
        }
        if (incomingVoicePlayer != null) {
            incomingVoicePlayer.stop();
            incomingVoicePlayer.release();
            incomingVoicePlayer = null;
        }
        if (outcomingVoicePlayer != null) {
            outcomingVoicePlayer.stop();
            outcomingVoicePlayer.release();
            outcomingVoicePlayer = null;
        }
    }

    public String GettingCurrentDtaeMethod() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String format = tsLong.toString();
        return format;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = NewPrivateChat.this;
        MySocketsClass.static_objects.global_layout = ParentView;
    }

    public void ShowingGifts() {
        if (gifts_view.getVisibility() == View.VISIBLE) {
            gifts_view.setVisibility(View.GONE);
            mSocket.off(MySocketsClass.ListenersClass.LOAD_GIFTS_RESPONSE, load_gifts_response);
            GiftsList.clear();
            giftSendButton.setImageResource(R.drawable.gift_solid);

        } else {
            giftSendButton.setImageResource(R.drawable.gift_solid_clicked);
            gifts_view.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userid", friendChatName);
                jsonObject.put("type", "user");
                mSocket.emit(MySocketsClass.EmmittersClass.LOAD_GIFTS, jsonObject);
                mSocket.on(MySocketsClass.ListenersClass.LOAD_GIFTS_RESPONSE, load_gifts_response);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private Date DateConvertor(long timeStamp) {
        Date d = new Date(timeStamp);
        return d;
    }

    private String GettingVideoIdFromYoutubeLink(String url) {
        if (url.contains("v=")) {
            if (url.contains("&")) {
                String[] u = url.split("v=");
                u = u[1].split("&");
                url = u[0];
            } else {
                String[] u = url.split("v=");
                url = u[1];
            }
        } else {
            String[] u = url.split("be/");
            url = u[1];
        }
        return url;
    }

    private void VisiblityGoneForScroll() {
        if (ScrollToBottomImageViewChats.getVisibility() == View.VISIBLE) {
            ScrollToBottomImageViewChats.setVisibility(View.GONE);
        }
    }

    private String RetrivingSystemMeessageDate(String CompleteDateText) {
        String GettingDate = CompleteDateText.split("\\|")[1];
        return GettingDate;
    }

    private void startRecording() {
        String audioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "AudioRecordingTest";

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(audioSavePathInDevice);

        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
            Toast.makeText(NewPrivateChat.this, "Recording", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopRecordingAndSendingEmit() {
        Log.e("RecordingTest", "stopr recording and emitting");
        if (mediaRecorder != null) {
            try {
                mediaRecorder.stop();
            } catch (RuntimeException stopException) {
                Toast.makeText(NewPrivateChat.this, "Hold to record, release to send.", Toast.LENGTH_SHORT).show();
                // handle cleanup here
            }
        }
        String fileFromStorage = readBytesFromFile(Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/" + "AudioRecordingTest");

        JSONObject mainJsonObject = new JSONObject();
        JSONObject video = new JSONObject();

        try {
            video.put("dataURL", fileFromStorage);
            video.put("time", MySocketsClass.getInstance().MakingCurrentDateTimeStamp());

            mainJsonObject.put("video", video);
            mainJsonObject.put("username", friendChatName);

            mSocket.off(MySocketsClass.ListenersClass.AUDIO_SENT);
            mSocket.emit(MySocketsClass.EmmittersClass.SEND_AUDIO, mainJsonObject);
            mSocket.on(MySocketsClass.ListenersClass.AUDIO_SENT, audio_sent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String compressImage(String imageUri) {
        Log.e("ComprsngLibrarytst", "Image Uri> " + imageUri);
        File file = new File(imageUri);
        String compresedImage = null;
        try {
            compresedImage = new Compressor(NewPrivateChat.this).compressToFile(file).getAbsolutePath();
            Log.e("ComprsngLibrarytst", "Resulted Uri> " + compresedImage);

        } catch (IOException e) {
            Log.e("ComprsngLibrarytst", "exceptn");
            e.printStackTrace();
        }
        return compresedImage;
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Socio/Sent items");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    Emitter.Listener audio_sent = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    Log.e("RecordingTest", "InListener");
                    try {
                        String senderUserName = jsonObject.getString("username");
                        String receivingUserName = jsonObject.getString("userid");
                        String message_type = jsonObject.getString("message_type");
                        String gender = jsonObject.getString("gender");
                        String age = jsonObject.getString("age");
                        String role = jsonObject.getString("role");
                        String dp = jsonObject.getString("dp");
                        String message = jsonObject.getString("message");
                        Log.e("RecordingTest", "Received");

                        VisiblityGoneForScroll();
                        String GettingCurrentDate = GettingCurrentDtaeMethod();
                        long TimeStampCurrentDate = Long.parseLong(GettingCurrentDate);


                        if (MySocketsClass.static_objects.CurrrentPrivateChatUsername.equals(receivingUserName)) {

                            if (!privateChatRecyclerView.canScrollVertically(1)) {
                                //   Log.e("idr")
                                MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(generateRandomNumber(),
                                        new UserModelClass("currentappUser", receivingUserName, null, true), "MyVoice_Note|" + message,
                                        DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp())), true);
                                privateChatRecyclerView.setAdapter(MySocketsClass.static_objects.MessageListAdapter);
                                Log.e("Scrolling", "Not scrollabale true");
                            } else {
                                MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(generateRandomNumber(),
                                        new UserModelClass("currentappUser", receivingUserName, null, true), "MyVoice_Note|" + message,
                                        DateConvertor(MySocketsClass.getInstance().MakingCurrentDateTimeStamp())), false);
                                privateChatRecyclerView.setAdapter(MySocketsClass.static_objects.MessageListAdapter);
                                Log.e("Scrolling", "scrollabale false");
                            }

                            userPrivateChatTables.setChatWith(receivingUserName);
                            userPrivateChatTables.setChatMessage("MyVoice_Note|" + message);
                            userPrivateChatTables.setChatMessageType("1");
                            userPrivateChatTables.setMessageUserDp(dp);
                            userPrivateChatTables.setMessageDate(GettingCurrentDate);
                            userPrivateChatTables.setChatStatus("currentappUser");
                            warfareDatabase.myDataBaseAccessObject().addChat(userPrivateChatTables);

                        }

                        Iterator<NewChatModelClass> MyIterator = MySocketsClass.static_objects.NewUserMessageList.iterator();
                        while (MyIterator.hasNext()) {
                            NewChatModelClass newChatModelClass2 = MyIterator.next();

                            if (newChatModelClass2.getMessangerName().equals(friendChatName))
                                MyIterator.remove();
                        }
                        if (!message.startsWith("/")) {
                            newChatModelClass = new NewChatModelClass(dp, receivingUserName, "Voice", presence, 0, "Chat");
                            MySocketsClass.static_objects.NewUserMessageList.add(newChatModelClass);
                            Collections.reverse(MySocketsClass.static_objects.NewUserMessageList);
                            MySocketsClass.static_objects.chatsRecyclerViewAdapter.notifyDataSetChanged();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener start_private_chat_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                Log.e("textng", jsonObject.getString("username"));
                username = jsonObject.getString("username");
                UserDp = jsonObject.getString("user_dp");
                String success = jsonObject.getString("success");
                String message = jsonObject.getString("message");
                String type = jsonObject.getString("type");
                reqStatus = jsonObject.getString("reqStatus");
                final String role = jsonObject.getString("role");
                final String age = jsonObject.getString("age");
                final String gender = jsonObject.getString("gender");
                String direction = jsonObject.getString("direction");
                presence = jsonObject.getString("presence");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userFriendName.setText(username);
                        ProfileAvatarLoader.setVisibility(View.VISIBLE);
                        ProfileAvatarLoader.setAnimating(true);
//                        Picasso.get().load(baseUrl + UserDp).into(userDpImageProfile, new com.squareup.picasso.Callback() {
//                            @Override
//                            public void onSuccess() {
//                                ProfileAvatarLoader.setVisibility(View.GONE);
//                            }
//
//                            @Override
//                            public void onError(Exception e) {
//                                ProfileAvatarLoader.setVisibility(View.GONE);
//                            }
//                        });
                        GlideApp.with(NewPrivateChat.this).load(baseUrl + UserDp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                ProfileAvatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        }).
                                into(userDpImageProfile);
                        if (presence.equals("0")) {
                            userDpImageProfile.setBorderColor(getResources().getColor(R.color.colorOffline));
                        } else if (presence.equals("1")) {
                            userDpImageProfile.setBorderColor(getResources().getColor(R.color.colorOnline));
                        } else if (presence.equals("2")) {
                            userDpImageProfile.setBorderColor(getResources().getColor(R.color.colorBusy));
                        } else if (presence.equals("3")) {
                            userDpImageProfile.setBorderColor(getResources().getColor(R.color.colorAway));
                        }

                        FriendRoleAvatarLoader.setVisibility(View.VISIBLE);
                        FriendRoleAvatarLoader.setAnimating(true);
                        if (role.equals("1")) {
                            Glide.with(NewPrivateChat.this).load(R.drawable.user_icon).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(friendRoleIcon);
                            userFriendName.setTextColor(getResources().getColor(R.color.colorBLue));
                        } else if (role.equals("2")) {
                            Glide.with(NewPrivateChat.this).load(R.drawable.mod_icon).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(friendRoleIcon);
                            userFriendName.setTextColor(getResources().getColor(R.color.colorRed));
                        } else if (role.equals("3")) {
                            Glide.with(NewPrivateChat.this).load(R.drawable.staff_icon).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(friendRoleIcon);
                            userFriendName.setTextColor(getResources().getColor(R.color.colorOrange));
                        } else if (role.equals("4")) {
                            Glide.with(NewPrivateChat.this).load(R.drawable.mentor_icon).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(friendRoleIcon);
                            userFriendName.setTextColor(getResources().getColor(R.color.greendark));
                        } else if (role.equals("5")) {

                            Glide.with(NewPrivateChat.this).load(R.drawable.merchant_icon).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    FriendRoleAvatarLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(friendRoleIcon);
                            userFriendName.setTextColor(getResources().getColor(R.color.violet));
                        }


                        FriendAgeAvatarLoader.setVisibility(View.VISIBLE);
                        FriendAgeAvatarLoader.setAnimating(true);
                        if (age.equals("1")) {
                            if (gender.equals("M")) {
                                Glide.with(NewPrivateChat.this).load(R.drawable.born_male).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            } else {
                                Glide.with(NewPrivateChat.this).load(R.drawable.born_female).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            }
                        } else if (age.equals("2")) {
                            if (gender.equals("M")) {
                                Glide.with(NewPrivateChat.this).load(R.drawable.teen_male).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            } else {
                                Glide.with(NewPrivateChat.this).load(R.drawable.teen_female).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            }
                        } else if (age.equals("3")) {
                            if (gender.equals("M")) {
                                Glide.with(NewPrivateChat.this).load(R.drawable.young_male).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            } else {
                                Glide.with(NewPrivateChat.this).load(R.drawable.young_female).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            }
                        } else if (age.equals("4")) {
                            if (gender.equals("M")) {
                                Glide.with(NewPrivateChat.this).load(R.drawable.adult_male).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            } else {
                                Glide.with(NewPrivateChat.this).load(R.drawable.adult_female).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            }
                        } else if (age.equals("5")) {
                            if (gender.equals("M")) {
                                Glide.with(NewPrivateChat.this).load(R.drawable.mature_male).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            } else {
                                Glide.with(NewPrivateChat.this).load(R.drawable.mature_female).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        FriendAgeAvatarLoader.setVisibility(View.GONE);
                                        return false;
                                    }
                                }).into(friendAgeIcon);
                            }
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener load_old_messages = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject jsonObject = (JSONObject) args[0];
            List<UserPrivateChatTables> totalDataChatContactsList = MySocketsClass.getInstance().getUserChat(friendChatName, NewPrivateChat.this);
            if (totalDataChatContactsList.size() > 0) {
                warfareDatabase.myDataBaseAccessObject().deleteAlreadyPresentChat(friendChatName);
            }
            try {

                for (int i = 0; i < jsonObject.getJSONArray("messages").length(); i++) {
                    final String message;
                    final String senderUsername = jsonObject.getJSONArray("messages").getJSONObject(i).getString("sender");
                    String receiverUsername = jsonObject.getJSONArray("messages").getJSONObject(i).getString("user");
                    if (jsonObject.getJSONArray("messages").getJSONObject(i).getString("message").contains("www.youtube.com") ||
                            jsonObject.getJSONArray("messages").getJSONObject(i).getString("message").contains("https://youtu.be/")) {
                        message = jsonObject.getJSONArray("messages").getJSONObject(i).getString("message");
                    } else {
                        message = MyClient.shortnameToUnicode(jsonObject.getJSONArray("messages").getJSONObject(i).getString("message"));
                    }
                    final String message_type = jsonObject.getJSONArray("messages").getJSONObject(i).getString("message_type");
                    final String dp = jsonObject.getJSONArray("messages").getJSONObject(i).getString("dp");
                    String type = jsonObject.getJSONArray("messages").getJSONObject(i).getString("type");
                    String role = jsonObject.getJSONArray("messages").getJSONObject(i).getString("role");
                    String gender = jsonObject.getJSONArray("messages").getJSONObject(i).getString("gender");
                    String age = jsonObject.getJSONArray("messages").getJSONObject(i).getString("age");
                    String chat_type = jsonObject.getJSONArray("messages").getJSONObject(i).getString("chat_type");
                    final String date = jsonObject.getJSONArray("messages").getJSONObject(i).getString("date");
                    Log.e("myTest", message + "  " + jsonObject.getJSONArray("messages").getJSONObject(i).getString("message"));
                    // js user ki chat open us k nam sy chat save hoi.
                    userPrivateChatTables.setChatWith(friendChatName);
                    if (message_type.equals("3")) {
                        userPrivateChatTables.setChatMessage("MyVoice_Note|" + message);
                    } else {
                        userPrivateChatTables.setChatMessage(message);
                    }
                    userPrivateChatTables.setChatMessageType(message_type);
                    userPrivateChatTables.setMessageUserDp(dp);
                    userPrivateChatTables.setMessageDate(date);

                    if (senderUsername.equals(friendChatName))
                        userPrivateChatTables.setChatStatus("otherUser");
                    else {
                        userPrivateChatTables.setChatStatus("currentappUser");
                    }

                    warfareDatabase.myDataBaseAccessObject().addChat(userPrivateChatTables);
                    Log.e("duplications", message);
                }
                //  retreiving the data from database
                final List<UserPrivateChatTables> _userPrivateChatTables = MySocketsClass.getInstance().getUserChat(friendChatName, NewPrivateChat.this);
                Log.e("databsess", "retreiving data");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ImageLoader imageLoader = new ImageLoader() {
                            @Override
                            public void loadImage(ImageView imageView, @Nullable String url, @Nullable Object payload) {
                                Glide.with(NewPrivateChat.this).load(url).into(imageView);
                            }
                        };
                        MySocketsClass.static_objects.MessageListAdapter.clear();
                        for (UserPrivateChatTables userPrivateChatTables : _userPrivateChatTables) {
                            String myDatabaseMsg = userPrivateChatTables.getChatMessage();
                            long myDatabaseDate = Long.parseLong(userPrivateChatTables.getMessageDate());
                            String myDatabaseDp = userPrivateChatTables.getMessageUserDp();
                            String myDatabaseMsgstatus = userPrivateChatTables.getChatStatus();
                            String myDatabasechattingwith = userPrivateChatTables.getChatWith();
                            String messageType = userPrivateChatTables.getChatMessageType();
//                            chatModelClass = new ChatModelClass(myDatabaseMsg, myDatabaseDate, myDatabaseDp, myDatabaseMsgstatus, myDatabasechattingwith, messageType);
//                            MySocketsClass.static_objects.SendingChatList.add(chatModelClass);

                            if (messageType.equals("1")) {
                                if (myDatabaseMsg.contains("www.youtube.com") || myDatabaseMsg.contains("https://youtu.be/")) {
                                    //  String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(myDatabaseMsg);
                                    MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(myDatabaseMsgstatus,
                                            new UserModelClass(myDatabaseMsgstatus, "YoutubeLinked", myDatabaseDp, true),
                                            myDatabaseMsg, DateConvertor(myDatabaseDate * 1000)), true);
                                } else {
                                    MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(myDatabaseMsgstatus,
                                            new UserModelClass(myDatabaseMsgstatus, myDatabasechattingwith, myDatabaseDp, true),
                                            myDatabaseMsg, DateConvertor(myDatabaseDate * 1000)), true);
                                }
                            } else if (messageType.equals("3")) {
                                //  idr
                                //voice message
                                MySocketsClass.static_objects.MessageListAdapter.addToStart(new MessageModelClass(generateRandomNumber(),
                                        new UserModelClass(myDatabaseMsgstatus, myDatabasechattingwith, myDatabaseDp, true),
                                        myDatabaseMsg, DateConvertor(myDatabaseDate * 1000)), true);

//                                MessageHolders holders=new MessageHolders().registerContentType(CONTENT_TYPE_VOICE,
//                                        IncomingVoiceMessageViewHolder.class,
//                                        R.layout.item_custom_incoming_voice_message,
//                                        OutcomingVoiceMessageViewHolder.class,
//                                        R.layout.item_custom_outcoming_voice_message,
//                                        contentChecker);

                            } else {
                                //image message
                                MessageModelClass messageModelClass = new MessageModelClass(myDatabaseMsgstatus,
                                        new UserModelClass(myDatabaseMsgstatus, myDatabasechattingwith, myDatabaseDp, true), null);
                                messageModelClass.setImage(new MessageModelClass.Image(baseUrl + myDatabaseMsg));
                                MySocketsClass.static_objects.MessageListAdapter.addToStart(messageModelClass, true);
                            }
                        }
                        privateChatRecyclerView.setAdapter(MySocketsClass.static_objects.MessageListAdapter);
                        if (returning_imageview.getVisibility() == View.VISIBLE) {
                            returning_imageview.setVisibility(View.GONE);
                        }
//                        MySocketsClass.static_objects.testingChatAdapter = new TestingChatAdapter(MySocketsClass.static_objects.SendingChatList);
//                        privateChatRecyclerView.setAdapter(MySocketsClass.static_objects.testingChatAdapter);
//                        privateChatRecyclerView.scrollToPosition(MySocketsClass.static_objects.SendingChatList.size() - 1);
                        Log.e("duplications", "abc");


                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener load_gifts_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String command = null, img = null, cost = null, name = null;
                        String username = jsonObject.getString("username");
                        String type = jsonObject.getString("type");
                        for (int i = 0; i < jsonObject.getJSONArray("gifts").length(); i++) {
                            command = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("command");
                            img = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("img");
                            cost = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("cost");
                            name = jsonObject.getJSONArray("gifts").getJSONObject(i).getString("name");

                            giftsModelClass = new GiftsModelClass(command, img, cost, name, friendChatName);
                            GiftsList.add(giftsModelClass);
                            userPrivateChatGiftsRecyclerAdpater.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    public static int dp(float value) {
        return (int) Math.ceil(1 * value);
    }


}

