package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.activities.UserFriendProfile;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.ExplainingShoutActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.MyShoutDetailsActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.ShoutSharingInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.UserNameClickedSearchShout;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.WallShoutingInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.UserWallLayoutModelClass;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.chibde.visualizer.CircleBarVisualizer;
import com.chibde.visualizer.LineBarVisualizer;
import com.chibde.visualizer.LineVisualizer;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.utils.Utils;
import com.tooltip.Tooltip;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class UserWallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Object> UserWallArraylist;
    private Context MyContext;
    private ViewHolder MyViewHolder;
    private MySingleTon mySingleTon = MySingleTon.getInstance();
    private int MyBlockShoutPosition;
    private String FollowButtonClickUserName;
    ShoutSharingInterface shoutSharingInterface;
    Client MyClient;
    UserNameClickedSearchShout userNameClickedSearchShout;
    WallShoutingInterface wallShoutingInterface;
    private RelativeLayout UpoadedShoutImageViewInstance;
    private ImageView ImageUploadingButtonForWallInstance;
    private String MyImageDownloadedPath = "/Socio/Shouts Images";
    private String myAudioShoutsDownloadedPath = "/Socio/Audio Shouts";
    int AD_TYPE = 0;
    int CONTENT_TYPE = 1;
    Tooltip tooltip;
    String fullDate;
    ViewHolder myViewHolderInstance = null;
    boolean isPaused = false, alredyAudioBtnClicked = true;

    public UserWallAdapter(ArrayList<Object> userWallArraylist, Context MyContext, ShoutSharingInterface shoutSharingInterface,
                           UserNameClickedSearchShout userNameClickedSearchShout, WallShoutingInterface wallShoutingInterface) {
        this.UserWallArraylist = userWallArraylist;
        this.MyContext = MyContext;
        this.shoutSharingInterface = shoutSharingInterface;
        this.userNameClickedSearchShout = userNameClickedSearchShout;
        this.wallShoutingInterface = wallShoutingInterface;
        MyClient = new Client(MyContext);
        LocalBroadcastManager.getInstance(MyContext).registerReceiver(stopingMediaPlayerListener, new IntentFilter("StopShoutsAudioMediaPlayer"));

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == AD_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.native_ad, parent, false);
            return new UnifiedNativeAdViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_wall_rowlayout, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        Log.e("positnCecker", "position > " + position);
        int viewType = getItemViewType(position);
        Log.e("positnCecker", "viewType > " + viewType);
        stoppingMediaPlayer();
        if (viewType == AD_TYPE) {
            UnifiedNativeAd nativeAd = (UnifiedNativeAd) UserWallArraylist.get(position);
            populateNativeAdView(nativeAd, ((UnifiedNativeAdViewHolder) holder).getAdView());

        } else {
            if (!(holder instanceof ViewHolder)) {
                return;
            }

            UserWallLayoutModelClass userWallLayoutModelClass = (UserWallLayoutModelClass) UserWallArraylist.get(holder.getAdapterPosition());

            if (userWallLayoutModelClass.getPostsUserText() != null) {
                // null nahi ka matlb k posts hyn jo k hm ny show krwani hyn
                ((ViewHolder) holder).ShoutExplainingRow.setVisibility(View.VISIBLE);

                if (userWallLayoutModelClass.getPostsUserText().contains("www.youtube.com") ||
                        userWallLayoutModelClass.getPostsUserText().contains("https://youtu.be/")) {
                    //For youtube Image Msg showing
                    final String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(userWallLayoutModelClass.getPostsUserText());
                    ((ViewHolder) holder).YoutubeUrlImageViewCardview.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).SimpleUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).ImageURL.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoader.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoaderLayout.setVisibility(View.GONE);
                    ((ViewHolder) holder).VideoImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageLoaderLayout.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageLoader.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageView.setVisibility(View.GONE);
                    ((ViewHolder) holder).audioViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText());
                    Log.e("youtube_Load", "LoadingId of yutubelink top view >" + VideoIdOfYoutubeLink);

                    Glide.with(((ViewHolder) holder).itemView.getContext()).
                            load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/maxresdefault.jpg")
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    new Handler().post(() -> {
                                        Glide.with(holder.itemView)
                                                .load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/mqdefault.jpg")
                                                .placeholder(R.color.whitecolor)
                                                .error(R.color.whitecolor)
                                                .into(((ViewHolder) holder).YoutubeVideoImage);
                                    });
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .into(((ViewHolder) holder).YoutubeVideoImage);

                }
 //               else if (userWallLayoutModelClass.getPostsUserText().matches(" /[[(.*)]]")){
                else if (userWallLayoutModelClass.getPostsUserText().contains("[[") &&
                        userWallLayoutModelClass.getPostsUserText().contains("]]")) {
                    //For simple Image Msg showing
                    ((ViewHolder) holder).YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).VideoImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageLoaderLayout.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageLoader.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageView.setVisibility(View.GONE);
                    ((ViewHolder) holder).SimpleUrlImageViewCardview.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).simpleImageLoaderLayout.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).simpleImageLoader.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).ImageURL.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).audioViewCardview.setVisibility(View.GONE);

                    //check krna k text me Uri hy ya nahi, agr nahi hy to smiles me convrt krwana
                    if (userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[0].contains("http") &&
                            userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[0].contains(".com")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[0]);
                    } else if (userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[0].contains("https://")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[0]);
                    } else {
                        ((ViewHolder) holder).UserPostedText.setText(MyClient.shortnameToUnicode(userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[0]));
                    }
                    Glide.with(holder.itemView.getContext())
                            .load(baseUrl + userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[1].replace("]]", "").trim())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    ((ViewHolder) holder).simpleImageLoaderLayout.setVisibility(View.GONE);
                                    ((ViewHolder) holder).simpleImageLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(((ViewHolder) holder).ImageURL);

                } else if (userWallLayoutModelClass.getPostsUserText().contains("{{") &&
                        userWallLayoutModelClass.getPostsUserText().contains("}}")) {
                    //For Audio Msg showing
                    ((ViewHolder) holder).YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).VideoImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageLoaderLayout.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageLoader.setVisibility(View.GONE);
                    ((ViewHolder) holder).videoImageView.setVisibility(View.GONE);
                    ((ViewHolder) holder).SimpleUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoaderLayout.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoader.setVisibility(View.GONE);
                    ((ViewHolder) holder).ImageURL.setVisibility(View.GONE);
                    ((ViewHolder) holder).audioViewCardview.setVisibility(View.VISIBLE);

                    //check krna k text me Uri hy ya nahi, agr nahi hy to smiles me convrt krwana
                    if (userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[0].contains("http") &&
                            userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[0].contains(".com")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[0]);
                    } else if (userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[0].contains("https://")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[0]);
                    } else {
                        ((ViewHolder) holder).UserPostedText.setText(MyClient.shortnameToUnicode(userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[0]));
                    }

                } else if (userWallLayoutModelClass.getPostsUserText().contains("|||")) {

                    //For video showing its thumbnail
                    ((ViewHolder) holder).VideoImageViewCardview.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).videoImageLoaderLayout.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).videoImageLoader.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).videoImageView.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).SimpleUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoaderLayout.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoader.setVisibility(View.GONE);
                    ((ViewHolder) holder).ImageURL.setVisibility(View.GONE);
                    ((ViewHolder) holder).audioViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).audioViewCardview.setVisibility(View.GONE);

                    //check krna k text me Uri hy ya nahi, agr nahi hy to smiles me convrt krwana
                    if (userWallLayoutModelClass.getPostsUserText().split("|||")[0].contains("http") &&
                            userWallLayoutModelClass.getPostsUserText().split("|||")[0].contains(".com")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText().split("|||")[0]);
                    } else if (userWallLayoutModelClass.getPostsUserText().split("|||")[0].contains("https://")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText().split("|||")[0]);
                    } else {
                        ((ViewHolder) holder).UserPostedText.setText(MyClient.shortnameToUnicode(userWallLayoutModelClass.getPostsUserText().split("\\[\\[")[0]));
                    }

                    GlideApp.with(holder.itemView.getContext()).load(R.drawable.video_icon).
                            listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    ((ViewHolder) holder).simpleImageLoaderLayout.setVisibility(View.GONE);
                                    ((ViewHolder) holder).simpleImageLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).
                            into(((ViewHolder) holder).videoImageView);
                } else {

                    //For Msg showing
                    ((ViewHolder) holder).SimpleUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).ImageURL.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoaderLayout.setVisibility(View.GONE);
                    ((ViewHolder) holder).simpleImageLoader.setVisibility(View.GONE);
                    ((ViewHolder) holder).YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    ((ViewHolder) holder).audioViewCardview.setVisibility(View.GONE);

                    //check krna k text me Uri hy ya nahi, agr nahi hy to smiles me convrt krwana
                    if (userWallLayoutModelClass.getPostsUserText().contains("http") &&
                            userWallLayoutModelClass.getPostsUserText().contains(".com")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText());
                    } else if (userWallLayoutModelClass.getPostsUserText().contains("https://")) {
                        ((ViewHolder) holder).UserPostedText.setText(userWallLayoutModelClass.getPostsUserText());
                    } else {
                        ((ViewHolder) holder).UserPostedText.setText(MyClient.shortnameToUnicode(userWallLayoutModelClass.getPostsUserText()));
                    }
                }


                ((ViewHolder) holder).UserPostedText.post(new Runnable() {
                    @Override
                    public void run() {

                        if (((ViewHolder) holder).UserPostedText.getLineCount() > 4) {
                            ((ViewHolder) holder).UserPostedText.setMaxLines(4);
                            ((ViewHolder) holder).UserPostedText.setEllipsize(TextUtils.TruncateAt.END);
                            ((ViewHolder) holder).btnSeeMore.setVisibility(View.VISIBLE);

                        } else
                            ((ViewHolder) holder).btnSeeMore.setVisibility(View.GONE);
                        ((ViewHolder) holder).UserPostedText.setMaxLines(4);
                        ((ViewHolder) holder).UserPostedText.setEllipsize(null);
                    }
                });
                ((ViewHolder) holder).UserPostedText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ViewHolder) holder).btnSeeMore.performClick();
                    }
                });
                ((ViewHolder) holder).btnSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (((ViewHolder) holder).btnSeeMore.getText().toString().equalsIgnoreCase(
                                ((ViewHolder) holder).btnSeeMore.getContext().getResources().getString(R.string.read_more))) {
                            ((ViewHolder) holder).UserPostedText.setMaxLines(99);
                            ((ViewHolder) holder).btnSeeMore.setText("Show less");
                            ((ViewHolder) holder).UserPostedText.setEllipsize(null);
                        } else {
                            ((ViewHolder) holder).UserPostedText.setEllipsize(TextUtils.TruncateAt.END);
                            ((ViewHolder) holder).UserPostedText.setMaxLines(4);
                            ((ViewHolder) holder).btnSeeMore.setText("Show more");

                        }
                    }
                });


                ((ViewHolder) holder).ContactavatarLoader.setAnimating(true);
                Glide.with(holder.itemView.getContext())
                        .load(baseUrl + userWallLayoutModelClass.getPostsUserDp())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                ((ViewHolder) holder).ContactavatarLoader.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(((ViewHolder) holder).UsersPictureView);

                ((ViewHolder) holder).UserRealName.setText(userWallLayoutModelClass.getPostsSimpleName() + "@");
                ((ViewHolder) holder).UserAccountName.setText(userWallLayoutModelClass.getPostsUsername());
                Log.e("BlockingUserOnBind", "position->" + position + "->name->" + (userWallLayoutModelClass.getPostsUsername()));
                if (userWallLayoutModelClass.getPostsUserRole().equals("1")) {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.user_icon).into(((ViewHolder) holder).UserRank);
                } else if (userWallLayoutModelClass.getPostsUserRole().equals("2")) {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.mod_icon).into(((ViewHolder) holder).UserRank);
                } else if (userWallLayoutModelClass.getPostsUserRole().equals("3")) {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.staff_icon).into(((ViewHolder) holder).UserRank);
                } else if (userWallLayoutModelClass.getPostsUserRole().equals("4")) {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.mentor_icon).into(((ViewHolder) holder).UserRank);
                } else if (userWallLayoutModelClass.getPostsUserRole().equals("5")) {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.merchant_icon).into(((ViewHolder) holder).UserRank);
                }
                if (userWallLayoutModelClass.getPostUserAge().equals("1")) {
                    if (userWallLayoutModelClass.getGender().equals("M")) {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.born_male).into(((ViewHolder) holder).UserAge);
                    } else {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.born_female).into(((ViewHolder) holder).UserAge);
                    }
                } else if (userWallLayoutModelClass.getPostUserAge().equals("2")) {
                    if (userWallLayoutModelClass.getGender().equals("M")) {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.teen_male).into(((ViewHolder) holder).UserAge);
                    } else {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.teen_female).into(((ViewHolder) holder).UserAge);
                    }
                } else if (userWallLayoutModelClass.getPostUserAge().equals("3")) {
                    if (userWallLayoutModelClass.getGender().equals("M")) {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.young_male).into(((ViewHolder) holder).UserAge);
                    } else {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.young_female).into(((ViewHolder) holder).UserAge);
                    }
                } else if (userWallLayoutModelClass.getPostUserAge().equals("4")) {
                    if (userWallLayoutModelClass.getGender().equals("M")) {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.adult_male).into(((ViewHolder) holder).UserAge);
                    } else {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.adult_female).into(((ViewHolder) holder).UserAge);
                    }
                } else if (userWallLayoutModelClass.getPostUserAge().equals("5")) {
                    if (userWallLayoutModelClass.getGender().equals("M")) {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.mature_male).into(((ViewHolder) holder).UserAge);
                    } else {
                        Glide.with(holder.itemView.getContext()).load(R.drawable.mature_female).into(((ViewHolder) holder).UserAge);
                    }
                }

                if (userWallLayoutModelClass.getMyLikeOnPost().equals("1")) {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.heart_solid).into(((ViewHolder) holder).LikeImageview);
                    ((ViewHolder) holder).LikeImageview.setColorFilter(ContextCompat.getColor(((ViewHolder) holder).itemView.getContext(),
                            R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                } else {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.heart_blank).into(((ViewHolder) holder).LikeImageview);
                    ((ViewHolder) holder).LikeImageview.setColorFilter(ContextCompat.getColor(((ViewHolder) holder).itemView.getContext(),
                            R.color.black), PorterDuff.Mode.SRC_IN);
                }

                if (userWallLayoutModelClass.getMyDisLikeOnPost().equals("1")) {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.dislike_black).into(((ViewHolder) holder).DislikeImageview);
                    ((ViewHolder) holder).DislikeImageview.setColorFilter(ContextCompat.getColor(((ViewHolder) holder).itemView.getContext(),
                            R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                } else {
                    Glide.with(holder.itemView.getContext()).load(R.drawable.dislike_gray).into(((ViewHolder) holder).DislikeImageview);
                    ((ViewHolder) holder).DislikeImageview.setColorFilter(ContextCompat.getColor(((ViewHolder) holder).itemView.getContext(),
                            R.color.black), PorterDuff.Mode.SRC_IN);
                }
                if (userWallLayoutModelClass.getPostsfollow().equals("1")){
                    ((ViewHolder) holder).followingTextview.setVisibility(View.VISIBLE);
                }
                else {
                    ((ViewHolder) holder).followingTextview.setVisibility(View.GONE);
                }
                likesVisibilityCheckAndSettingLikesValue(((ViewHolder) holder), userWallLayoutModelClass.getPostsTotallikes());
                dislikesVisibilityCheckAndSettingDisLikesValue(((ViewHolder) holder), userWallLayoutModelClass.getPostsTotaldislikes());
                ((ViewHolder) holder).CommentsTextview.setText("View " + userWallLayoutModelClass.getPostsTotalcomments() + " comments");
                ((ViewHolder) holder).PostTimeTextview.setText(GettingPostingTime(Long.parseLong(userWallLayoutModelClass.getPostsTime())));

            } else {
                // agr to null ara matlb k koi post nahi the, is lye rows ko gyb krwa dia hy
                ((ViewHolder) holder).ShoutExplainingRow.setVisibility(View.GONE);
            }

            ((ViewHolder) holder).CommentsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        shoutSharingInterface.shoutDeletingMethod(position);
                        ((ViewHolder) holder).CommentsButton.setEnabled(false);
                        stoppingMediaPlayer();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((ViewHolder) holder).CommentsButton.setEnabled(true);
                            }
                        }, 2000);
                    }
                }
            });

            ((ViewHolder) holder).VideoImageViewCardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        if (UserWallArraylist.get(position) instanceof UserWallLayoutModelClass) {
                            shoutSharingInterface.shoutDeletingMethod(position);
                            ((ViewHolder) holder).VideoImageViewCardview.setEnabled(false);
                            stoppingMediaPlayer();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ((ViewHolder) holder).VideoImageViewCardview.setEnabled(true);
                                }
                            }, 3000);
                        }
                    }
                }
            });
            ((ViewHolder) holder).YoutubeUrlImageViewCardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        if (UserWallArraylist.get(position) instanceof UserWallLayoutModelClass) {
                            shoutSharingInterface.shoutDeletingMethod(position);
                            ((ViewHolder) holder).YoutubeUrlImageViewCardview.setEnabled(false);
                            stoppingMediaPlayer();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ((ViewHolder) holder).YoutubeUrlImageViewCardview.setEnabled(true);
                                }
                            }, 3000);
                        }
                    }
                }
            });
            ((ViewHolder) holder).SimpleUrlImageViewCardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        if (UserWallArraylist.get(position) instanceof UserWallLayoutModelClass) {
                            shoutSharingInterface.shoutDeletingMethod(position);
                            ((ViewHolder) holder).SimpleUrlImageViewCardview.setEnabled(false);
                            stoppingMediaPlayer();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ((ViewHolder) holder).SimpleUrlImageViewCardview.setEnabled(true);
                                }
                            }, 3000);

                        }
                    }
                }
            });
            ((ViewHolder) holder).PostTimeTextview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        if (tooltip != null && tooltip.isShowing()) {
                            tooltip.dismiss();
                        }
                        if (gettingTime(Long.parseLong(userWallLayoutModelClass.getPostsTime())) != null) {
                            tooltip = new Tooltip.Builder(v)
                                    .setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.new_theme_green))
                                    .setTextColor(holder.itemView.getContext().getResources().getColor(R.color.colorWhite))
                                    .setText(gettingTime(Long.parseLong(userWallLayoutModelClass.getPostsTime()))).show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (tooltip.isShowing()) {
                                        tooltip.dismiss();
                                    }
                                }
                            }, 2000);
                        }
                    }
                }
            });
            ((ViewHolder) holder).LikeImageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        MyViewHolder = ((ViewHolder) holder);
                        Toast.makeText(MyContext, "Like post", Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("id", userWallLayoutModelClass.getPostsId());
                            mSocket.off(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE);
                            mSocket.off(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE);
                            mSocket.emit(MySocketsClass.EmmittersClass.LIKE_SHOUT, jsonObject);
                            mSocket.on(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE, like_shout_response);
                            mSocket.on(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE, dislike_shout_response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            ((ViewHolder) holder).DislikeImageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        MyViewHolder = ((ViewHolder) holder);
                        Toast.makeText(MyContext, "DisLike post", Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("id", userWallLayoutModelClass.getPostsId());
                            mSocket.off(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE);
                            mSocket.off(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE);
                            mSocket.emit(MySocketsClass.EmmittersClass.DISLIKE_SHOUT, jsonObject);
                            mSocket.on(MySocketsClass.ListenersClass.LIKE_SHOUT_RESPONSE, like_shout_response);
                            mSocket.on(MySocketsClass.ListenersClass.DISLIKE_SHOUT_RESPONSE, dislike_shout_response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            ((ViewHolder) holder).UsersPictureView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        if (!userWallLayoutModelClass.getPostsUsername().equals(mySingleTon.username)) {
                            Intent intent = new Intent(holder.itemView.getContext(), UserFriendProfile.class);
                            intent.putExtra("username", userWallLayoutModelClass.getPostsUsername());
                            holder.itemView.getContext().startActivity(intent);
                        }
                    }
                }
            });

            ((ViewHolder) holder).RowItemsMoreOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        Toast.makeText(MyContext, "OptionsShowing", Toast.LENGTH_SHORT).show();
                        MyBlockShoutPosition = position;

                        if (!mySingleTon.username.equals(userWallLayoutModelClass.getPostsUsername())) {
                            if (userWallLayoutModelClass.getPostsfollow().equals("1")) {
                                PopupMenu popupMenu = new PopupMenu(MyContext, v);
                                popupMenu.inflate(R.menu.shouts_row_popup_more_options_unfolw);
                                popupMenu.show();
                                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    @Override
                                    public boolean onMenuItemClick(MenuItem item) {
                                        if (item.getItemId() == R.id.HideThisShout) {
                                            Toast.makeText(MyContext, "Hiding post", Toast.LENGTH_SHORT).show();
                                            JSONObject jsonObject = new JSONObject();
                                            try {
                                                jsonObject.put("id", userWallLayoutModelClass.getPostsId());
                                                mSocket.off(MySocketsClass.ListenersClass.BLOCK_SHOUT_RESPONSE);
                                                mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_SHOUT, jsonObject);
                                                mSocket.on(MySocketsClass.ListenersClass.BLOCK_SHOUT_RESPONSE, block_shout_response);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            return true;
                                        } else if (item.getItemId() == R.id.BlockTheUser) {
                                            Toast.makeText(MyContext, "Blocking User all posts", Toast.LENGTH_SHORT).show();
                                            JSONObject jsonObject = new JSONObject();
                                            try {
                                                jsonObject.put("username", userWallLayoutModelClass.getPostsUsername());
                                                mSocket.off(MySocketsClass.ListenersClass.BLOCK_SHOUT_USER_RESPONSE);
                                                mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_SHOUT_USER, jsonObject);
                                                mSocket.on(MySocketsClass.ListenersClass.BLOCK_SHOUT_USER_RESPONSE, block_shout_user_response);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            return true;
                                        } else if (item.getItemId() == R.id.ReportThisShout) {
                                            ReportingShout(userWallLayoutModelClass.getPostsId());
                                            Toast.makeText(MyContext, "ReportThisShout", Toast.LENGTH_SHORT).show();
                                            return true;
                                        } else if (item.getItemId() == R.id.ViewProfile) {
                                            Intent intent = new Intent(MyContext, UserFriendProfile.class);
                                            intent.putExtra("username", userWallLayoutModelClass.getPostsUsername());
                                            MyContext.startActivity(intent);
                                            return true;
                                        } else if (item.getItemId() == R.id.unFollowUser) {
                                            Toast.makeText(MyContext, "Unfollowing", Toast.LENGTH_SHORT).show();
                                            FollowButtonClickUserName = userWallLayoutModelClass.getPostsUsername();
                                            JSONObject jsonObject = new JSONObject();
                                            try {
                                                jsonObject.put("username", userWallLayoutModelClass.getPostsUsername());
                                                mSocket.off(MySocketsClass.ListenersClass.FOLLOW_USER_RESPONSE);
                                                mSocket.emit(MySocketsClass.EmmittersClass.FOLLOW_USER, jsonObject);
                                                mSocket.on(MySocketsClass.ListenersClass.FOLLOW_USER_RESPONSE, follow_user_response);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            return true;
                                        }
                                        return false;
                                    }
                                });
                            } else {
                                PopupMenu popupMenu = new PopupMenu(MyContext, v);
                                popupMenu.inflate(R.menu.shouts_row_popup_more_options);
                                popupMenu.show();
                                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    @Override
                                    public boolean onMenuItemClick(MenuItem item) {
                                        if (item.getItemId() == R.id.HideThisShout) {
                                            Toast.makeText(MyContext, "HideThisShout", Toast.LENGTH_SHORT).show();
                                            Toast.makeText(MyContext, "Hiding post", Toast.LENGTH_SHORT).show();
                                            JSONObject jsonObject = new JSONObject();
                                            try {
                                                jsonObject.put("id", userWallLayoutModelClass.getPostsId());
                                                mSocket.off(MySocketsClass.ListenersClass.BLOCK_SHOUT_RESPONSE);
                                                mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_SHOUT, jsonObject);
                                                mSocket.on(MySocketsClass.ListenersClass.BLOCK_SHOUT_RESPONSE, block_shout_response);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            return true;
                                        } else if (item.getItemId() == R.id.BlockTheUser) {
                                            Toast.makeText(MyContext, "Blocking User all posts", Toast.LENGTH_SHORT).show();
                                            JSONObject jsonObject = new JSONObject();
                                            try {
                                                jsonObject.put("username", userWallLayoutModelClass.getPostsUsername());
                                                mSocket.off(MySocketsClass.ListenersClass.BLOCK_SHOUT_USER_RESPONSE);
                                                mSocket.emit(MySocketsClass.EmmittersClass.BLOCK_SHOUT_USER, jsonObject);
                                                mSocket.on(MySocketsClass.ListenersClass.BLOCK_SHOUT_USER_RESPONSE, block_shout_user_response);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            return true;
                                        } else if (item.getItemId() == R.id.ReportThisShout) {
                                            ReportingShout(userWallLayoutModelClass.getPostsId());
                                            Toast.makeText(MyContext, "ReportThisShout", Toast.LENGTH_SHORT).show();
                                            return true;
                                        } else if (item.getItemId() == R.id.ViewProfile) {
                                            Intent intent = new Intent(MyContext, UserFriendProfile.class);
                                            intent.putExtra("username", userWallLayoutModelClass.getPostsUsername());
                                            MyContext.startActivity(intent);
                                            return true;
                                        } else if (item.getItemId() == R.id.followUser) {
                                            Toast.makeText(MyContext, "Following", Toast.LENGTH_SHORT).show();
                                            FollowButtonClickUserName = userWallLayoutModelClass.getPostsUsername();
                                            JSONObject jsonObject = new JSONObject();
                                            try {
                                                jsonObject.put("username", userWallLayoutModelClass.getPostsUsername());
                                                mSocket.off(MySocketsClass.ListenersClass.FOLLOW_USER_RESPONSE);
                                                mSocket.emit(MySocketsClass.EmmittersClass.FOLLOW_USER, jsonObject);
                                                mSocket.on(MySocketsClass.ListenersClass.FOLLOW_USER_RESPONSE, follow_user_response);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            return true;
                                        }

                                        return false;
                                    }
                                });
                            }
                        } else {

                            PopupMenu popupMenu = new PopupMenu(MyContext, v);
                            popupMenu.inflate(R.menu.shouts_row_popup_hiding);
                            popupMenu.show();
                            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    if (item.getItemId() == R.id.DeleteThisShout) {

                                        Toast.makeText(MyContext, "Delete my own post", Toast.LENGTH_SHORT).show();
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("id", userWallLayoutModelClass.getPostsId());
                                            mSocket.off(MySocketsClass.ListenersClass.DELETE_SHOUT_RESPONSE);
                                            mSocket.emit(MySocketsClass.EmmittersClass.DELETE_SHOUT, jsonObject);
                                            mSocket.on(MySocketsClass.ListenersClass.DELETE_SHOUT_RESPONSE, delete_shout_response);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        return true;
                                    }
                                    else if (item.getItemId() == R.id.ViewShoutDetails) {

                                        Intent intent = new Intent(MyContext, MyShoutDetailsActivity.class);
                                        intent.putExtra("MyId", userWallLayoutModelClass.getPostsId());
                                        MyContext.startActivity(intent);

                                        return true;
                                    }
                                    return false;
                                }
                            });

                        }
                    }
                }
            });

            ((ViewHolder) holder).secondView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserWallArraylist.size() > 0) {
                        if (UserWallArraylist.get(position) instanceof UserWallLayoutModelClass) {
                            userNameClickedSearchShout.ShowingShoutsOfUsername(userWallLayoutModelClass.getPostsUsername());
                        }
                        ((ViewHolder) holder).secondView.setEnabled(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((ViewHolder) holder).secondView.setEnabled(true);
                            }
                        }, 2000);
                    }
                }
            });

            ((ViewHolder) holder).audioMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stoppingMediaPlayer();
                    mediaPlayer.reset();
                    isPaused = false;
                    Log.e("audioTest", "completes");
                }
            });

            ((ViewHolder) holder).audioImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CheckPermissions()) {
                        if (alredyAudioBtnClicked) {
                            if ((myViewHolderInstance != null && myViewHolderInstance.audioMediaPlayer.isPlaying()) || isPaused) {

                                if (myViewHolderInstance == (ViewHolder) holder) {
                                    if (myViewHolderInstance.audioMediaPlayer.isPlaying()) {
                                        myViewHolderInstance.audiohandler.removeCallbacks(updater);
                                        myViewHolderInstance.audioMediaPlayer.pause();
                                        Glide.with(MyContext).load(R.drawable.shout_play).into(myViewHolderInstance.audioImageView);
                                        isPaused = true;
                                    } else {
                                        myViewHolderInstance.audiohandler.postDelayed(updater, 1000);
                                        myViewHolderInstance.audioMediaPlayer.start();
                                        Glide.with(MyContext).load(R.drawable.shout_pause).into(myViewHolderInstance.audioImageView);
                                        isPaused = false;
                                    }
                                } else {
                                    stoppingMediaPlayer();
                                    hideAudioLoader();
                                    myViewHolderInstance = (ViewHolder) holder;
                                    showingAudioLoader();
                                    File sdCardDirectory = new File(Environment.getExternalStorageDirectory(), myAudioShoutsDownloadedPath);
                                    if (!sdCardDirectory.exists()) {
                                        sdCardDirectory.mkdirs();
                                    }
                                    new LongOperation().execute(baseUrl + userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[1].replace("}}", ""));
                                }

                            } else {
                                alredyAudioBtnClicked = true;
                                myViewHolderInstance = (ViewHolder) holder;
                                File sdCardDirectory = new File(Environment.getExternalStorageDirectory(), myAudioShoutsDownloadedPath);
                                if (!sdCardDirectory.exists()) {
                                    sdCardDirectory.mkdirs();
                                }
                                new LongOperation().execute(baseUrl + userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[1].replace("}}", ""));
                            }
                        } else {
                            Snackbar.make(view, "Shout already loading...", BaseTransientBottomBar.LENGTH_LONG).show();
                        }
//                        if ((myViewHolderInstance != null && myViewHolderInstance.audioMediaPlayer.isPlaying()) || isPaused) {
//
//                            if (myViewHolderInstance == (ViewHolder) holder) {
//                                Log.e("=", "click btn >" + "sameSong");
//                                if (myViewHolderInstance.audioMediaPlayer.isPlaying()) {
//                                    Log.e("audioTest", "click btn >" + "NotalreadyPaused");
//                                    myViewHolderInstance.audiohandler.removeCallbacks(updater);
//                                    myViewHolderInstance.audioMediaPlayer.pause();
//                                    Glide.with(MyContext).load(R.drawable.shout_play).into(myViewHolderInstance.audioImageView);
//                                    isPaused = true;
//                                } else {
//                                    Log.e("audioTest", "click btn >" + "alreadyPaused");
//                                    myViewHolderInstance.audiohandler.postDelayed(updater, 1000);
//                                    myViewHolderInstance.audioMediaPlayer.start();
//                                    Glide.with(MyContext).load(R.drawable.shout_pause).into(myViewHolderInstance.audioImageView);
//                                    isPaused = false;
//                                }
//                            } else {
//                                stoppingMediaPlayer();
//                                myViewHolderInstance = (ViewHolder) holder;
//                                showingAudioLoader();
//                                prepareMediaPlayer(baseUrl + userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[1].replace("}}", ""));
//                            }
//
//                        } else {
//                            //here
//                            myViewHolderInstance = null;
//                            myViewHolderInstance = (ViewHolder) holder;
//                            Log.e("audioTest", "click btn >" + "first Tym playing");
//                            showingAudioLoader();
//                            Log.e("audioTest", baseUrl + userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[1].replace("}}", ""));
//                            prepareMediaPlayer(baseUrl + userWallLayoutModelClass.getPostsUserText().split("\\{\\{")[1].replace("}}", ""));
//                            Log.e("audioTest", "Ended..");
//                        }

                    } else {
                        wallShoutingInterface.requestingPermissions();
                    }
                }
            });

        }
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... fileToBeDownloaded) {
            alredyAudioBtnClicked = false;
            String dwnldingUrl = fileToBeDownloaded[0];
            Log.e("dwnldng", dwnldingUrl);
            File destinationofDownlodedFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Audio Shouts/Audio_Shout.mp3");
            DownloadingFile(dwnldingUrl, destinationofDownlodedFile);
            return null;
        }

        private void DownloadingFile(String DownlodingFileUrlurl, File destinationOfOutputFile) {
            ((Activity) MyContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showingAudioLoader();
                }
            });

            try {

                URL u = new URL(DownlodingFileUrlurl);
                URLConnection conn = u.openConnection();
                int contentLength = conn.getContentLength();

                DataInputStream stream = new DataInputStream(u.openStream());

                byte[] buffer = new byte[contentLength];
                stream.readFully(buffer);
                stream.close();

                DataOutputStream fos = new DataOutputStream(new FileOutputStream(destinationOfOutputFile));
                fos.write(buffer);
                fos.flush();
                fos.close();
                //yahan pe file name change ho ga, user voice ka nam ay ga wo rkhna
                playingAudio(destinationOfOutputFile.getAbsolutePath());
            } catch (FileNotFoundException e) {
                return; // swallow a 404
            } catch (IOException e) {
                return; // swallow a 404
            }
//            }
//            else {
//                playingAudio(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + downloadedFileName, voiceID);
//            }
        }
    }

    private void playingAudio(final String voiceUri) {
        ((Activity) MyContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //here
                try {
                    myViewHolderInstance.audioMediaPlayer.setDataSource(voiceUri);
                    myViewHolderInstance.audioMediaPlayer.prepare();
                    myViewHolderInstance.audioMediaPlayer.start();
                    myViewHolderInstance.textTotalDuration.setText("/ " + milisecondsToTimer(myViewHolderInstance.audioMediaPlayer.getDuration()));
                    Glide.with(MyContext).load(R.drawable.shout_pause).into(myViewHolderInstance.audioImageView);
                    hideAudioLoader();
                    playVisuals();
                    updateSeekbar();
                    alredyAudioBtnClicked = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });


    }

    private void stoppingMediaPlayer() {
        if (myViewHolderInstance != null && myViewHolderInstance.audioMediaPlayer.isPlaying()) {
            myViewHolderInstance.audioMediaPlayer.stop();
            myViewHolderInstance.audioMediaPlayer.reset();
            Glide.with(MyContext).load(R.drawable.shout_play).into(myViewHolderInstance.audioImageView);
            myViewHolderInstance.textCurrentTime.setText("0:00 ");
            myViewHolderInstance.textTotalDuration.setText("/ 0:00");
            myViewHolderInstance = null;
            isPaused = false;
        }
    }

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateSeekbar();
        }
    };

    private void prepareMediaPlayer(String audioUrl) {

        try {
            myViewHolderInstance.audioMediaPlayer.setDataSource(audioUrl);
            myViewHolderInstance.audioMediaPlayer.prepare();
            myViewHolderInstance.audioMediaPlayer.start();
            myViewHolderInstance.textTotalDuration.setText("/ " + milisecondsToTimer(myViewHolderInstance.audioMediaPlayer.getDuration()));
            Glide.with(MyContext).load(R.drawable.shout_pause).into(myViewHolderInstance.audioImageView);
            updateSeekbar();
            playVisuals();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideAudioLoader();
                }
            }, 2000);

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(MyContext, "exceptn " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void playVisuals() {
        if (myViewHolderInstance != null) {
            myViewHolderInstance.circleBarVisualizer.setColor(ContextCompat.getColor(MyContext, R.color.new_theme_green));
            myViewHolderInstance.circleBarVisualizer.setPlayer(myViewHolderInstance.audioMediaPlayer.getAudioSessionId());
        }
    }

    private BroadcastReceiver stopingMediaPlayerListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stoppingMediaPlayer();
        }
    };


    private void updateSeekbar() {
        //  Log.e("audioTest", "updateSeekbar >" + "updateSeekbar");
        if (myViewHolderInstance != null && myViewHolderInstance.audioMediaPlayer.isPlaying()) {

            long currentDuration = myViewHolderInstance.audioMediaPlayer.getCurrentPosition();
            myViewHolderInstance.textCurrentTime.setText(milisecondsToTimer(currentDuration) + " ");

            myViewHolderInstance.audiohandler.postDelayed(updater, 1000);
        }
    }

    private String milisecondsToTimer(long milliseconds) {
        String timerString = "";
        String secondsString;

        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) (milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000;

        if (hours > 0) {
            timerString = hours + ":";

        }
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }
        timerString = timerString + minutes + ":" + secondsString;
        return timerString;
    }

    private void likesVisibilityCheckAndSettingLikesValue(ViewHolder holder, String value) {
        if (value.equals("0")) {
            ((ViewHolder) holder).LikesTextview.setVisibility(View.GONE);
        } else {
            ((ViewHolder) holder).LikesTextview.setVisibility(View.VISIBLE);
            if (value.equals("1")) {
                ((ViewHolder) holder).LikesTextview.setText(value + " like");
            } else {
                ((ViewHolder) holder).LikesTextview.setText(value + " likes");
            }
        }
    }

    private void dislikesVisibilityCheckAndSettingDisLikesValue(ViewHolder holder, String value) {
        if (value.equals("0")) {
            ((ViewHolder) holder).DisLikesTextview.setVisibility(View.GONE);
        } else {
            ((ViewHolder) holder).DisLikesTextview.setVisibility(View.VISIBLE);
            if (value.equals("1")) {
                ((ViewHolder) holder).DisLikesTextview.setText(value + " dislike");
            } else {
                ((ViewHolder) holder).DisLikesTextview.setText(value + " dislikes");
            }
        }
    }

    private PopupWindow setPopUpWindow() {
        LayoutInflater inflater = (LayoutInflater)
                MyContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_categrized_layout, null);
        LinearLayout ShowingFromPublic = view.findViewById(R.id.ShowingFromPublic);
        LinearLayout ShowingOnlyFromMe = view.findViewById(R.id.ShowingOnlyFromMe);
        final LinearLayout ShowingFromFollowing = view.findViewById(R.id.ShowingFromFollowing);
        final ImageView ShowingFromPublicImageView = view.findViewById(R.id.ShowingFromPublicImageView);
        final ImageView ShowingOnlyFromMeImageview = view.findViewById(R.id.ShowingOnlyFromMeImageview);
        final ImageView ShowingfromFollowingImageview = view.findViewById(R.id.ShowingfromFollowingImageview);

        ShowingFromPublicImageView.setImageResource(R.drawable.tick_image);
        ShowingOnlyFromMeImageview.setImageResource(R.drawable.round_filled_white);
        ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);


        ShowingFromFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MyContext, "clicked from following", Toast.LENGTH_SHORT).show();
                ShowingfromFollowingImageview.setImageResource(R.drawable.tick_image);
                ShowingFromPublicImageView.setImageResource(R.drawable.round_filled_white);
                ShowingOnlyFromMeImageview.setImageResource(R.drawable.round_filled_white);
            }
        });
        ShowingOnlyFromMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MyContext, "clicked Only from Me", Toast.LENGTH_SHORT).show();
                ShowingOnlyFromMeImageview.setImageResource(R.drawable.tick_image);
                ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);
                ShowingFromPublicImageView.setImageResource(R.drawable.round_filled_white);
            }
        });
        ShowingFromPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MyContext, "clicked from public", Toast.LENGTH_SHORT).show();
                ShowingFromPublicImageView.setImageResource(R.drawable.tick_image);
                ShowingOnlyFromMeImageview.setImageResource(R.drawable.round_filled_white);
                ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);
                ShowingfromFollowingImageview.setImageResource(R.drawable.round_filled_white);
            }
        });

        return new PopupWindow(view, 400, RelativeLayout.LayoutParams.WRAP_CONTENT, true);

    }

    private boolean CheckPermissions() {
        int writeStoragePermissionResult = ContextCompat.checkSelfPermission(MyContext, WRITE_EXTERNAL_STORAGE);
        int readStoragePermissionResult = ContextCompat.checkSelfPermission(MyContext, READ_EXTERNAL_STORAGE);
        int micPermissionResult = ContextCompat.checkSelfPermission(MyContext, RECORD_AUDIO);
        return writeStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                readStoragePermissionResult == PackageManager.PERMISSION_GRANTED &&
                micPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void ReportingShout(String ShoutID) {
        GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
        giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity,
                null, "reporting_shout", null, null, ShoutID);
    }

    private void populateNativeAdView(UnifiedNativeAd nativeAd,
                                      UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        NativeAd.Image icon = nativeAd.getIcon();

        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }

    @Override
    public int getItemViewType(int position) {
        Object recyclerViewItem = UserWallArraylist.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return AD_TYPE;
        }
        return CONTENT_TYPE;
    }

    @Override
    public int getItemCount() {
        return UserWallArraylist.size();
    }

    private String gettingTime(long time_reg) {
        //finding Time...
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time_reg * 1000);
        String date = DateFormat.format("EEE d, MMM yyyy hh:mm a", cal).toString();
        String today_time = DateFormat.format("hh:mm a", cal).toString();
        String date2 = DateFormat.format("yyyy / MM / d", cal).toString();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / d");
        String current_date = mdformat.format(calendar.getTime());


        //finding day/date/days ago...
        String last_final_time_reg = null;
        long time_reg2 = time_reg * 1000;
        long now = System.currentTimeMillis();
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            fullDate = null;
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            fullDate = null;
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            fullDate = null;
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            fullDate = null;
        } else if (diff2 < 24 * HOUR_MILLIS) {
            fullDate = today_time;
        } else if (diff2 < 48 * HOUR_MILLIS) {
            fullDate = today_time;
        } else {

            long calculating_months = diff2 / DAY_MILLIS;
            if (calculating_months < 30) {
                fullDate = date;
            } else if (calculating_months >= 30 && calculating_months <= 360) {
                fullDate = date;
            } else if (calculating_months >= 30 && calculating_months > 360) {
                fullDate = date;
            }
        }
        return fullDate;
    }

    private String GettingPostingTime(long time_reg) {

        //finding Time...
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time_reg * 1000);
        String date = DateFormat.format("EEE d, MMM yyyy hh:mm a", cal).toString();
        String today_time = DateFormat.format("hh:mm a", cal).toString();
        String date2 = DateFormat.format("yyyy / MM / d", cal).toString();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / d");
        String current_date = mdformat.format(calendar.getTime());


        //finding day/date/days ago...
        String last_final_time_reg = null;
        long time_reg2 = time_reg * 1000;
        long now = System.currentTimeMillis();
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "just now";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "a minute ago";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "an hour ago";
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago";
            fullDate = today_time;
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "yesterday";
            fullDate = today_time;
        } else {

            long calculating_months = diff2 / DAY_MILLIS;
            if (calculating_months < 30) {
                last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
                fullDate = date;
            } else if (calculating_months >= 30 && calculating_months <= 360) {
                last_final_time_reg = calculating_months / 30 + " Month(s) ago";
                fullDate = date;
            } else if (calculating_months >= 30 && calculating_months > 360) {
                last_final_time_reg = calculating_months / 360 + " Year(s) ago";
                fullDate = date;
            }
        }
        return last_final_time_reg;
    }

    private String GettingVideoIdFromYoutubeLink(String url) {
        if (url.contains("v=")) {
            if (url.contains("&")) {
                String[] u = url.split("v=");
                u = u[1].split("&");
                url = u[0];
            } else {
                String[] u = url.split("v=");
                url = u[1];
            }
        } else {
            String[] u = url.split("be/");
            url = u[1];
        }
        return url;
    }

    private void showingAudioLoader() {

        if (myViewHolderInstance.playAudioLoader.getVisibility() == View.GONE) {
            myViewHolderInstance.playAudioLoader.setAnimating(true);
            myViewHolderInstance.playAudioLoader.setVisibility(View.VISIBLE);
            Log.e("audioLoader", "visible");
        }
    }

    private void hideAudioLoader() {
        ((Activity) MyContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (myViewHolderInstance != null && myViewHolderInstance.playAudioLoader.getVisibility() == View.VISIBLE) {
                    myViewHolderInstance.playAudioLoader.setAnimating(false);
                    myViewHolderInstance.playAudioLoader.setVisibility(View.GONE);
                    Log.e("audioLoader", "Gone");
                }
            }
        });

    }

    Emitter.Listener follow_user_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            ((Activity) MyContext).runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];

                @Override
                public void run() {
                    Log.e("Folew", "listener");

                    try {
                        String success = jsonObject.getString("success");
                        String type = jsonObject.getString("type");
                        Log.e("FollowTest", "inListener");
                        if (success.equals("1") && type.equals("1")) {
                            Log.e("FollowTest", "InIf");

                            for (int i = 0; i < UserWallArraylist.size(); i++) {
                                Log.e("FollowTest", "InIf For loop");
                                if (UserWallArraylist.get(i) instanceof UserWallLayoutModelClass) {
                                    if (((UserWallLayoutModelClass) UserWallArraylist.get(i)).getPostsUsername().equals(FollowButtonClickUserName)) {
                                        Log.e("FollowTest", "InIf changing to unfoolow");
                                        ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setPostsfollow("1");
                                    }
                                }

                            }

                        } else {
                            Log.e("FollowTest", "Inelse");
                            for (int i = 0; i < UserWallArraylist.size(); i++) {
                                Log.e("FollowTest", "Inelse For loop");
                                if (UserWallArraylist.get(i) instanceof UserWallLayoutModelClass) {
                                    if (((UserWallLayoutModelClass) UserWallArraylist.get(i)).getPostsUsername().equals(FollowButtonClickUserName)) {
                                        Log.e("FollowTest", "Inelse changing to foolow");
                                        ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setPostsfollow("0");
                                    }
                                }

                            }

                        }
                        notifyDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener like_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("LikesChecking", "inListener");
            ((Activity) MyContext).runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];

                @Override
                public void run() {
                    try {
                        Log.e("LikesChecking", "inTry");
                        String success = jsonObject.getString("success");
                        String total = jsonObject.getString("total");
                        String id = jsonObject.getString("id");
                        String type = jsonObject.getString("type");

                        if (success.equals("1")) {
                            Log.e("LikesChecking", "inSuccess");

                            if (type.equals("like")) {
                                Log.e("LikesChecking", "inIf");
                                Glide.with(MyContext).load(R.drawable.heart_solid).into(MyViewHolder.LikeImageview);
                                MyViewHolder.LikeImageview.setColorFilter(ContextCompat.getColor(MyViewHolder.itemView.getContext(),
                                        R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                                //  MyViewHolder.LikesTextview.setText(total+" likes");
                                likesVisibilityCheckAndSettingLikesValue(MyViewHolder, total);

                                for (int i = 0; i < UserWallArraylist.size(); i++) {
                                    if (UserWallArraylist.get(i) instanceof UserWallLayoutModelClass) {
                                        if (((UserWallLayoutModelClass) UserWallArraylist.get(i)).getPostsId().equals(id)) {
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setPostsTotallikes(total);
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setMyLikeOnPost("1");
                                        }
                                    }

                                }
                            } else {
                                Log.e("LikesChecking", "inElse");
                                Glide.with(MyContext).load(R.drawable.heart_blank).into(MyViewHolder.LikeImageview);
                                MyViewHolder.LikeImageview.setColorFilter(ContextCompat.getColor(MyViewHolder.itemView.getContext(),
                                        R.color.black), PorterDuff.Mode.SRC_IN);
                                //  MyViewHolder.LikesTextview.setText(total+" likes");
                                likesVisibilityCheckAndSettingLikesValue(MyViewHolder, total);

                                for (int i = 0; i < UserWallArraylist.size(); i++) {
                                    if (UserWallArraylist.get(i) instanceof UserWallLayoutModelClass) {
                                        if (((UserWallLayoutModelClass) UserWallArraylist.get(i)).getPostsId().equals(id)) {
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setPostsTotallikes(total);
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setMyLikeOnPost("0");
                                        }
                                    }

                                }
                            }

                        }
                    } catch (JSONException e) {
                        Log.e("LikesChecking", "inException");
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener dislike_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            ((Activity) MyContext).runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];

                @Override
                public void run() {
                    try {
                        String success = jsonObject.getString("success");
                        String total = jsonObject.getString("total");
                        String id = jsonObject.getString("id");
                        String type = jsonObject.getString("type");

                        if (success.equals("1")) {

                            if (type.equals("dislike_yes")) {
                                Glide.with(MyContext).load(R.drawable.dislike_black).into(MyViewHolder.DislikeImageview);
                                MyViewHolder.DislikeImageview.setColorFilter(ContextCompat.getColor(MyViewHolder.itemView.getContext(),
                                        R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                                // MyViewHolder.DisLikesTextview.setText(total+" dislikes");
                                dislikesVisibilityCheckAndSettingDisLikesValue(MyViewHolder, total);

                                for (int i = 0; i < UserWallArraylist.size(); i++) {
                                    if (UserWallArraylist.get(i) instanceof UserWallLayoutModelClass) {
                                        if (((UserWallLayoutModelClass) UserWallArraylist.get(i)).getPostsId().equals(id)) {
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setPostsTotaldislikes(total);
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setMyDisLikeOnPost("1");
                                        }
                                    }

                                }
                            } else {
                                Glide.with(MyContext).load(R.drawable.dislike_gray).into(MyViewHolder.DislikeImageview);
                                MyViewHolder.DislikeImageview.setColorFilter(ContextCompat.getColor(MyViewHolder.itemView.getContext(),
                                        R.color.black), PorterDuff.Mode.SRC_IN);
                                // MyViewHolder.DisLikesTextview.setText(total+" dislikes");
                                dislikesVisibilityCheckAndSettingDisLikesValue(MyViewHolder, total);

                                for (int i = 0; i < UserWallArraylist.size(); i++) {
                                    if (UserWallArraylist.get(i) instanceof UserWallLayoutModelClass) {
                                        if (((UserWallLayoutModelClass) UserWallArraylist.get(i)).getPostsId().equals(id)) {
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setPostsTotaldislikes(total);
                                            ((UserWallLayoutModelClass) UserWallArraylist.get(i)).setMyDisLikeOnPost("0");
                                        }
                                    }

                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener delete_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            ((Activity) MyContext).runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];

                @Override
                public void run() {
                    try {
                        String success = jsonObject.getString("success");
                        String id = jsonObject.getString("id");

                        if (success.equals("1")) {

                            Toast.makeText(MyContext, "DeleteThisShout", Toast.LENGTH_SHORT).show();
                            UserWallArraylist.remove(MyBlockShoutPosition);

                            notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    Emitter.Listener block_shout_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            ((Activity) MyContext).runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];

                @Override
                public void run() {
                    try {
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {
                            Toast.makeText(MyContext, "blockThisShout", Toast.LENGTH_SHORT).show();
                            UserWallArraylist.remove(MyBlockShoutPosition);
                            //  notifyItemRemoved(MyBlockShoutPosition);
                            notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    Emitter.Listener block_shout_user_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            ((Activity) MyContext).runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];
                ArrayList<UserWallLayoutModelClass> DeletingObjectsArraylist = new ArrayList<>();

                @Override
                public void run() {
                    try {
                        String success = jsonObject.getString("success");
                        String blockingusername = jsonObject.getString("username");

                        if (success.equals("1")) {
                            for (int i = 0; i < UserWallArraylist.size(); i++) {
                                if (UserWallArraylist.get(i) instanceof UserWallLayoutModelClass) {
                                    if (((UserWallLayoutModelClass) UserWallArraylist.get(i)).getPostsUsername().equalsIgnoreCase(blockingusername)) {
                                        DeletingObjectsArraylist.add(((UserWallLayoutModelClass) UserWallArraylist.get(i)));
                                    }
                                }
                            }

                            UserWallArraylist.removeAll(DeletingObjectsArraylist);
                            notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView UsersPictureView;
        AvatarView ContactavatarLoader, playAudioLoader;
        MediaPlayer audioMediaPlayer;
        private Handler audiohandler = new Handler();
        TextView UserRealName, UserAccountName, UserPostingTime, UserPostedText, LikesTextview, DisLikesTextview, CommentsTextview,
                PostTimeTextview, btnSeeMore, textCurrentTime, textTotalDuration,followingTextview;
        ImageView UserRank, UserAge, LikeImageview, DislikeImageview, YoutubeVideoImage, RowItemsMoreOptions, ImageURL, ShowingFromPublicImageView, ShowingOnlyFromMeImageview,
                ShowingfromFollowingImageview, ImageUploadingButtonForWall, videoImageView, simpleImageLoader, videoImageLoader, audioImageView;
        LinearLayout LikeButton, CommentsButton, ShowingFromPublic, ShowingOnlyFromMe, ShowingFromFollowing, ShoutExplainingRow;
        RelativeLayout YoutubeUrlImageViewCardview, SimpleUrlImageViewCardview, secondView, UpoadedShoutImageView,
                VideoImageViewCardview, audioViewCardview, simpleImageLoaderLayout, videoImageLoaderLayout;
        CircleBarVisualizer circleBarVisualizer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            videoImageView = itemView.findViewById(R.id.videoImageView);
            VideoImageViewCardview = itemView.findViewById(R.id.VideoImageViewCardview);
            audioViewCardview = itemView.findViewById(R.id.audioViewCardview);
            videoImageLoader = itemView.findViewById(R.id.videoImageLoader);
            videoImageLoaderLayout = itemView.findViewById(R.id.videoImageLoaderLayout);
            Glide.with(itemView.getContext()).load(R.drawable.socio_loader).into(videoImageLoader);
            UsersPictureView = itemView.findViewById(R.id.UsersPictureView);
            UserRealName = itemView.findViewById(R.id.UserRealName);
            audioImageView = itemView.findViewById(R.id.audioImageView);
            textCurrentTime = itemView.findViewById(R.id.textCurrentTime);
            textTotalDuration = itemView.findViewById(R.id.textTotalDuration);
            audioMediaPlayer = new MediaPlayer();
            circleBarVisualizer = itemView.findViewById(R.id.visualizer);
            UserAccountName = itemView.findViewById(R.id.UserAccountName);
            UserPostingTime = itemView.findViewById(R.id.UserPostingTime);
            UserPostedText = itemView.findViewById(R.id.UserPostedText);
            btnSeeMore = itemView.findViewById(R.id.btnSeeMore);
            UserRank = itemView.findViewById(R.id.UserRank);
            UserAge = itemView.findViewById(R.id.UserAge);
            LikeButton = itemView.findViewById(R.id.LikeButton);
            followingTextview = itemView.findViewById(R.id.followingTextview);
            CommentsButton = itemView.findViewById(R.id.CommentsButton);
            LikesTextview = itemView.findViewById(R.id.LikesTextview);
            DisLikesTextview = itemView.findViewById(R.id.DisLikesTextview);
            LikeImageview = itemView.findViewById(R.id.LikeImageview);
            DislikeImageview = itemView.findViewById(R.id.DislikeImageview);
            YoutubeVideoImage = itemView.findViewById(R.id.YoutubeVideoImage);
            YoutubeUrlImageViewCardview = itemView.findViewById(R.id.YoutubeUrlImageViewCardview);
            RowItemsMoreOptions = itemView.findViewById(R.id.RowItemsMoreOptions);
            PostTimeTextview = itemView.findViewById(R.id.PostTimeTextview);
            CommentsTextview = itemView.findViewById(R.id.CommentsTextview);
            ImageURL = itemView.findViewById(R.id.ImageURL);
            SimpleUrlImageViewCardview = itemView.findViewById(R.id.SimpleUrlImageViewCardview);
            secondView = itemView.findViewById(R.id.secondView);
            ShowingFromPublic = itemView.findViewById(R.id.ShowingFromPublic);
            ShowingOnlyFromMe = itemView.findViewById(R.id.ShowingOnlyFromMe);
            ShowingFromFollowing = itemView.findViewById(R.id.ShowingFromFollowing);
            ShowingFromPublicImageView = itemView.findViewById(R.id.ShowingFromPublicImageView);
            ShowingOnlyFromMeImageview = itemView.findViewById(R.id.ShowingOnlyFromMeImageview);
            ShowingfromFollowingImageview = itemView.findViewById(R.id.ShowingfromFollowingImageview);
            ShoutExplainingRow = itemView.findViewById(R.id.ShoutExplainingRow);
            simpleImageLoader = itemView.findViewById(R.id.simpleImageLoader);
            simpleImageLoaderLayout = itemView.findViewById(R.id.simpleImageLoaderLayout);
            ContactavatarLoader = itemView.findViewById(R.id.ContactavatarLoader);
            playAudioLoader = itemView.findViewById(R.id.playAudioLoader);
            Glide.with(itemView.getContext()).load(R.drawable.socio_loader).into(simpleImageLoader);


        }
    }

    public class UnifiedNativeAdViewHolder extends RecyclerView.ViewHolder {

//        private final NativeExpressAdView mNativeAd;

        private UnifiedNativeAdView adView;

        public UnifiedNativeAdView getAdView() {
            return adView;
        }

        UnifiedNativeAdViewHolder(View view) {
            super(view);
            adView = (UnifiedNativeAdView) view.findViewById(R.id.ad_view);

            // The MediaView will display a video asset if one is present in the ad, and the
            // first image asset otherwise.
            adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

            // Register the view used for each individual asset.
            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
            adView.setBodyView(adView.findViewById(R.id.ad_body));
            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(adView.findViewById(R.id.ad_icon));
            adView.setPriceView(adView.findViewById(R.id.ad_price));
            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
            adView.setStoreView(adView.findViewById(R.id.ad_store));
            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
        }
    }
}
