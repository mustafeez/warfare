package com.app.websterz.warfarechat.ChatKitClasses;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayout;
import com.stfalcon.chatkit.R;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.ViewHolder;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;
import com.stfalcon.chatkit.messages.MessagesListStyle;
import com.stfalcon.chatkit.utils.DateFormatter;
import com.stfalcon.chatkit.utils.RoundedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.view.ViewCompat;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.CLIPBOARD_SERVICE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;


public class MessageHoldersChatRooms {
    private static final short VIEW_TYPE_DATE_HEADER = 130;
    private static final short VIEW_TYPE_TEXT_MESSAGE = 131;
    private static final short VIEW_TYPE_IMAGE_MESSAGE = 132;

    private Class<? extends ViewHolder<Date>> dateHeaderHolder;
    private int dateHeaderLayout;

    private HolderConfig<IMessage> incomingTextConfig;
    private HolderConfig<IMessage> outcomingTextConfig;
    private HolderConfig<MessageContentType.Image> incomingImageConfig;
    private HolderConfig<MessageContentType.Image> outcomingImageConfig;

    private List<ContentTypeConfig> customContentTypes = new ArrayList<>();
    private ContentChecker contentChecker;

    public MessageHoldersChatRooms() {
        this.dateHeaderHolder = DefaultDateHeaderViewHolder.class;
        this.dateHeaderLayout = R.layout.item_date_header;

        this.incomingTextConfig = new HolderConfig<>(DefaultIncomingTextMessageViewHolder.class, R.layout.chatrooms_item_incoming_text_message);
        this.outcomingTextConfig = new HolderConfig<>(DefaultOutcomingTextMessageViewHolder.class, R.layout.chatrooms_item_outcoming_text_message);
        this.incomingImageConfig = new HolderConfig<>(DefaultIncomingImageMessageViewHolder.class, R.layout.chatrooms_item_incoming_image_message);
        this.outcomingImageConfig = new HolderConfig<>(DefaultOutcomingImageMessageViewHolder.class, R.layout.chatrooms_item_outcoming_image_message);
    }

    public MessageHoldersChatRooms setIncomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout) {
        this.incomingTextConfig.holder = holder;
        this.incomingTextConfig.layout = layout;
        return this;
    }

    public MessageHoldersChatRooms setIncomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.incomingTextConfig.holder = holder;
        this.incomingTextConfig.layout = layout;
        this.incomingTextConfig.payload = payload;
        return this;
    }

    public MessageHoldersChatRooms setIncomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder) {
        this.incomingTextConfig.holder = holder;
        return this;
    }

    public MessageHoldersChatRooms setIncomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            Object payload) {
        this.incomingTextConfig.holder = holder;
        this.incomingTextConfig.payload = payload;
        return this;
    }

    public MessageHoldersChatRooms setIncomingTextLayout(@LayoutRes int layout) {
        this.incomingTextConfig.layout = layout;
        return this;
    }

    public MessageHoldersChatRooms setIncomingTextLayout(@LayoutRes int layout, Object payload) {
        this.incomingTextConfig.layout = layout;
        this.incomingTextConfig.payload = payload;
        return this;
    }


    public MessageHoldersChatRooms setOutcomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout) {
        this.outcomingTextConfig.holder = holder;
        this.outcomingTextConfig.layout = layout;
        return this;
    }

    public MessageHoldersChatRooms setOutcomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.outcomingTextConfig.holder = holder;
        this.outcomingTextConfig.layout = layout;
        this.outcomingTextConfig.payload = payload;
        return this;
    }

    public MessageHoldersChatRooms setOutcomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder) {
        this.outcomingTextConfig.holder = holder;
        return this;
    }

    public MessageHoldersChatRooms setOutcomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            Object payload) {
        this.outcomingTextConfig.holder = holder;
        this.outcomingTextConfig.payload = payload;
        return this;
    }

    public MessageHoldersChatRooms setOutcomingTextLayout(@LayoutRes int layout) {
        this.outcomingTextConfig.layout = layout;
        return this;
    }

    public MessageHoldersChatRooms setOutcomingTextLayout(@LayoutRes int layout, Object payload) {
        this.outcomingTextConfig.layout = layout;
        this.outcomingTextConfig.payload = payload;
        return this;
    }


    public MessageHoldersChatRooms setIncomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout) {
        this.incomingImageConfig.holder = holder;
        this.incomingImageConfig.layout = layout;
        return this;
    }


    public MessageHoldersChatRooms setIncomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.incomingImageConfig.holder = holder;
        this.incomingImageConfig.layout = layout;
        this.incomingImageConfig.payload = payload;
        return this;
    }

    public MessageHoldersChatRooms setIncomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder) {
        this.incomingImageConfig.holder = holder;
        return this;
    }


    public MessageHoldersChatRooms setIncomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            Object payload) {
        this.incomingImageConfig.holder = holder;
        this.incomingImageConfig.payload = payload;
        return this;
    }


    public MessageHoldersChatRooms setIncomingImageLayout(@LayoutRes int layout) {
        this.incomingImageConfig.layout = layout;
        return this;
    }


    public MessageHoldersChatRooms setIncomingImageLayout(@LayoutRes int layout, Object payload) {
        this.incomingImageConfig.layout = layout;
        this.incomingImageConfig.payload = payload;
        return this;
    }

    public MessageHoldersChatRooms setOutcomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout) {
        this.outcomingImageConfig.holder = holder;
        this.outcomingImageConfig.layout = layout;
        return this;
    }


    public MessageHoldersChatRooms setOutcomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.outcomingImageConfig.holder = holder;
        this.outcomingImageConfig.layout = layout;
        this.outcomingImageConfig.payload = payload;
        return this;
    }


    public MessageHoldersChatRooms setOutcomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder) {
        this.outcomingImageConfig.holder = holder;
        return this;
    }


    public MessageHoldersChatRooms setOutcomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            Object payload) {
        this.outcomingImageConfig.holder = holder;
        this.outcomingImageConfig.payload = payload;
        return this;
    }


    public MessageHoldersChatRooms setOutcomingImageLayout(@LayoutRes int layout) {
        this.outcomingImageConfig.layout = layout;
        return this;
    }


    public MessageHoldersChatRooms setOutcomingImageLayout(@LayoutRes int layout, Object payload) {
        this.outcomingImageConfig.layout = layout;
        this.outcomingImageConfig.payload = payload;
        return this;
    }


    public MessageHoldersChatRooms setDateHeaderConfig(
            @NonNull Class<? extends ViewHolder<Date>> holder,
            @LayoutRes int layout) {
        this.dateHeaderHolder = holder;
        this.dateHeaderLayout = layout;
        return this;
    }

    public MessageHoldersChatRooms setDateHeaderHolder(@NonNull Class<? extends ViewHolder<Date>> holder) {
        this.dateHeaderHolder = holder;
        return this;
    }


    public MessageHoldersChatRooms setDateHeaderLayout(@LayoutRes int layout) {
        this.dateHeaderLayout = layout;
        return this;
    }


    public <TYPE extends MessageContentType>
    MessageHoldersChatRooms registerContentType(
            byte type, @NonNull Class<? extends BaseMessageViewHolder<TYPE>> holder,
            @LayoutRes int incomingLayout,
            @LayoutRes int outcomingLayout,
            @NonNull ContentChecker contentChecker) {

        return registerContentType(type,
                holder, incomingLayout,
                holder, outcomingLayout,
                contentChecker);
    }

    public <TYPE extends MessageContentType>
    MessageHoldersChatRooms registerContentType(
            byte type,
            @NonNull Class<? extends BaseMessageViewHolder<TYPE>> incomingHolder, @LayoutRes int incomingLayout,
            @NonNull Class<? extends BaseMessageViewHolder<TYPE>> outcomingHolder, @LayoutRes int outcomingLayout,
            @NonNull ContentChecker contentChecker) {

        if (type == 0)
            throw new IllegalArgumentException("content type must be greater or less than '0'!");

        customContentTypes.add(
                new ContentTypeConfig<>(type,
                        new HolderConfig<>(incomingHolder, incomingLayout),
                        new HolderConfig<>(outcomingHolder, outcomingLayout)));
        this.contentChecker = contentChecker;
        return this;
    }


    public <TYPE extends MessageContentType>
    MessageHoldersChatRooms registerContentType(
            byte type,
            @NonNull Class<? extends MessageHoldersChatRooms.BaseMessageViewHolder<TYPE>> incomingHolder, Object incomingPayload, @LayoutRes int incomingLayout,
            @NonNull Class<? extends MessageHoldersChatRooms.BaseMessageViewHolder<TYPE>> outcomingHolder, Object outcomingPayload, @LayoutRes int outcomingLayout,
            @NonNull MessageHoldersChatRooms.ContentChecker contentChecker) {

        if (type == 0)
            throw new IllegalArgumentException("content type must be greater or less than '0'!");

        customContentTypes.add(
                new MessageHoldersChatRooms.ContentTypeConfig<>(type,
                        new MessageHoldersChatRooms.HolderConfig<>(incomingHolder, incomingLayout, incomingPayload),
                        new MessageHoldersChatRooms.HolderConfig<>(outcomingHolder, outcomingLayout, outcomingPayload)));
        this.contentChecker = contentChecker;
        return this;
    }


    public interface ContentChecker<MESSAGE extends IMessage> {


        boolean hasContentFor(MESSAGE message, byte type);
    }


    protected ViewHolder getHolder(ViewGroup parent, int viewType, MessagesListStyle messagesListStyle) {
        switch (viewType) {
            case VIEW_TYPE_DATE_HEADER:
                return getHolder(parent, dateHeaderLayout, dateHeaderHolder, messagesListStyle, null);
            case VIEW_TYPE_TEXT_MESSAGE:
                return getHolder(parent, incomingTextConfig, messagesListStyle);
            case -VIEW_TYPE_TEXT_MESSAGE:
                return getHolder(parent, outcomingTextConfig, messagesListStyle);
            case VIEW_TYPE_IMAGE_MESSAGE:
                return getHolder(parent, incomingImageConfig, messagesListStyle);
            case -VIEW_TYPE_IMAGE_MESSAGE:
                return getHolder(parent, outcomingImageConfig, messagesListStyle);
            default:
                for (ContentTypeConfig typeConfig : customContentTypes) {
                    if (Math.abs(typeConfig.type) == Math.abs(viewType)) {
                        if (viewType > 0)
                            return getHolder(parent, typeConfig.incomingConfig, messagesListStyle);
                        else
                            return getHolder(parent, typeConfig.outcomingConfig, messagesListStyle);
                    }
                }
        }
        throw new IllegalStateException("Wrong message view type. Please, report this issue on GitHub with full stacktrace in description.");
    }

    @SuppressWarnings("unchecked")
    protected void bind(final ViewHolder holder, final Object item, boolean isSelected,
                        final ImageLoader imageLoader,
                        final View.OnClickListener onMessageClickListener,
                        final View.OnLongClickListener onMessageLongClickListener,
                        final DateFormatter.Formatter dateHeadersFormatter,
                        final SparseArray<MessagesListAdapter_newChatrooms.OnMessageViewClickListener> clickListenersArray) {

        if (item instanceof IMessage) {
            ((MessageHoldersChatRooms.BaseMessageViewHolder) holder).isSelected = isSelected;
            ((MessageHoldersChatRooms.BaseMessageViewHolder) holder).imageLoader = imageLoader;
            holder.itemView.setOnLongClickListener(onMessageLongClickListener);
            holder.itemView.setOnClickListener(onMessageClickListener);

            for (int i = 0; i < clickListenersArray.size(); i++) {
                final int key = clickListenersArray.keyAt(i);
                final View view = holder.itemView.findViewById(key);
                if (view != null) {
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            clickListenersArray.get(key).onMessageViewClick(view, (IMessage) item);
                        }
                    });
                }
            }
        } else if (item instanceof Date) {
            ((MessageHoldersChatRooms.DefaultDateHeaderViewHolder) holder).dateHeadersFormatter = dateHeadersFormatter;
        }

        holder.onBind(item);
    }


    protected int getViewType(Object item, String senderId) {
        boolean isOutcoming = false;
        int viewType;

        if (item instanceof IMessage) {
            IMessage message = (IMessage) item;
            isOutcoming = message.getUser().getId().contentEquals(senderId);
            viewType = getContentViewType(message);

        } else viewType = VIEW_TYPE_DATE_HEADER;

        return isOutcoming ? viewType * -1 : viewType;
    }

    private ViewHolder getHolder(ViewGroup parent, HolderConfig holderConfig,
                                 MessagesListStyle style) {
        return getHolder(parent, holderConfig.layout, holderConfig.holder, style, holderConfig.payload);
    }

    private <HOLDER extends ViewHolder>
    ViewHolder getHolder(ViewGroup parent, @LayoutRes int layout, Class<HOLDER> holderClass,
                         MessagesListStyle style, Object payload) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        try {
            Constructor<HOLDER> constructor = null;
            HOLDER holder;
            try {
                constructor = holderClass.getDeclaredConstructor(View.class, Object.class);
                constructor.setAccessible(true);
                holder = constructor.newInstance(v, payload);
            } catch (NoSuchMethodException e) {
                constructor = holderClass.getDeclaredConstructor(View.class);
                constructor.setAccessible(true);
                holder = constructor.newInstance(v);
            }
            if (holder instanceof DefaultMessageViewHolder && style != null) {
                ((DefaultMessageViewHolder) holder).applyStyle(style);
            }
            return holder;
        } catch (Exception e) {
            throw new UnsupportedOperationException("Somehow we couldn't create the ViewHolder for message. Please, report this issue on GitHub with full stacktrace in description.", e);
        }
    }

    @SuppressWarnings("unchecked")
    private short getContentViewType(IMessage message) {
        if (message instanceof MessageContentType.Image
                && ((MessageContentType.Image) message).getImageUrl() != null) {
            return VIEW_TYPE_IMAGE_MESSAGE;
        }

        // other default types will be here

        if (message instanceof MessageContentType) {
            for (int i = 0; i < customContentTypes.size(); i++) {
                ContentTypeConfig config = customContentTypes.get(i);
                if (contentChecker == null) {
                    throw new IllegalArgumentException("ContentChecker cannot be null when using custom content types!");
                }
                boolean hasContent = contentChecker.hasContentFor(message, config.type);
                if (hasContent) return config.type;
            }
        }

        return VIEW_TYPE_TEXT_MESSAGE;
    }

    public static abstract class BaseMessageViewHolder<MESSAGE extends IMessage> extends ViewHolder<MESSAGE> {

        boolean isSelected;

        /**
         * For setting custom data to ViewHolder
         */
        protected Object payload;

        /**
         * Callback for implementing images loading in message list
         */
        protected ImageLoader imageLoader;

        @Deprecated
        public BaseMessageViewHolder(View itemView) {
            super(itemView);
        }

        public BaseMessageViewHolder(View itemView, Object payload) {
            super(itemView);
            this.payload = payload;
        }


        public boolean isSelected() {
            return isSelected;
        }


        public boolean isSelectionModeEnabled() {
            return MessagesListAdapter_newChatrooms.isSelectionModeEnabled;
        }


        public ImageLoader getImageLoader() {
            return imageLoader;
        }

        protected void configureLinksBehavior(final TextView text) {
            text.setLinksClickable(false);
            text.setMovementMethod(new LinkMovementMethod() {
                @Override
                public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
                    boolean result = false;
                    if (!MessagesListAdapter_newChatrooms.isSelectionModeEnabled) {
                        result = super.onTouchEvent(widget, buffer, event);
                    }
                    itemView.onTouchEvent(event);
                    return result;
                }
            });
        }
    }

    /**
     * Default view holder implementation for incoming text message
     */
    public static class IncomingTextMessageViewHolder<MESSAGE extends IMessage>
            extends BaseIncomingMessageViewHolder<MESSAGE> {

        protected ViewGroup bubble;
        protected TextView text, SenderTextUserName, system_message_text1, system_message_text2, main_system_message_text;
        protected ImageView SenderTextRankImage, SenderTextAgeImage, system_msg_image1, system_msg_image2;
        protected CircleImageView chatroom_image_receiver;
        protected LinearLayout chatroom_system_messageText, chatroom_main_system_messageText;
        protected RelativeLayout outcomingUserMessage;
        protected Button coins_catcher_button;
        protected String MyCoinsGrabberChatRoom;
        protected ImageView YoutubeUrlImageViewChatRoom;
        protected CardView YoutubeUrlImageViewCardviewChatRoom;
        private ClipboardManager myClipboard;
        private ClipData myClip;
        private FlexboxLayout GamesLayout,WarfareBotMessage;

        @Deprecated
        public IncomingTextMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public IncomingTextMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(final MESSAGE message) {
            super.onBind(message);


            // For Joining/Leaving chatroom message
            if (message.getText().equalsIgnoreCase("has left") ||
                    message.getText().equalsIgnoreCase("has joined") ||
                    message.getText().equalsIgnoreCase("disconnected")) {

                chatroom_system_messageText.setVisibility(View.VISIBLE);
                outcomingUserMessage.setVisibility(View.GONE);
                chatroom_main_system_messageText.setVisibility(View.GONE);
                coins_catcher_button.setVisibility(View.GONE);
                GamesLayout.setVisibility(View.GONE);

                system_message_text1.setText(message.getMyname());
                system_message_text1.setTextSize(16);
                if (message.getMyrole().equals("1")) {
                    Glide.with(itemView.getContext()).load(R.drawable.user_icon).into(system_msg_image1);
                } else if (message.getMyrole().equals("2")) {
                    Glide.with(itemView.getContext()).load(R.drawable.mod_icon).into(system_msg_image1);
                } else if (message.getMyrole().equals("3")) {
                    Glide.with(itemView.getContext()).load(R.drawable.staff_icon).into(system_msg_image1);
                } else if (message.getMyrole().equals("4")) {
                    Glide.with(itemView.getContext()).load(R.drawable.mentor_icon).into(system_msg_image1);
                } else if (message.getMyrole().equals("5")) {
                    Glide.with(itemView.getContext()).load(R.drawable.merchant_icon).into(system_msg_image1);
                }
                if (message.getMyage().equals("1")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.born_male).into(system_msg_image2);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.born_female).into(system_msg_image2);
                    }
                } else if (message.getMyage().equals("2")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.teen_male).into(system_msg_image2);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.teen_female).into(system_msg_image2);
                    }
                } else if (message.getMyage().equals("3")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.young_male).into(system_msg_image2);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.young_female).into(system_msg_image2);
                    }
                } else if (message.getMyage().equals("4")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.adult_male).into(system_msg_image2);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.adult_female).into(system_msg_image2);
                    }
                } else if (message.getMyage().equals("5")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.mature_male).into(system_msg_image2);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.mature_female).into(system_msg_image2);
                    }
                }
                system_message_text2.setTextSize(16);
                system_message_text2.setText(message.getText());
            }

            //For Main System Message Listener
            else if (message.getMyname().equalsIgnoreCase("system_msgs")) {

                if (!message.getText().contains("[[") &&
                        !message.getText().contains("]]"))
                {
                    // For Simple System Message
                    chatroom_main_system_messageText.setVisibility(View.VISIBLE);
                outcomingUserMessage.setVisibility(View.GONE);
                chatroom_system_messageText.setVisibility(View.GONE);
                GamesLayout.setVisibility(View.GONE);

                    main_system_message_text.setTextSize(16);
                main_system_message_text.setText(message.getText());
                if (message.getMygender().equals("coins")) {
                    coins_catcher_button.setVisibility(View.VISIBLE);
                    MyCoinsGrabberChatRoom = message.getUser().getName();

                } else {
                    coins_catcher_button.setVisibility(View.GONE);
                }
                if (message.getText().startsWith("**")) {
                    main_system_message_text.setTypeface(null, Typeface.BOLD);
                }
            }
                else {
                    //For Game System Message
                    GamesLayout.setVisibility(View.VISIBLE);
                    GamesLayout.removeAllViews();
                    chatroom_main_system_messageText.setVisibility(View.GONE);
                    outcomingUserMessage.setVisibility(View.GONE);
                    chatroom_system_messageText.setVisibility(View.GONE);

                    GamesLayout.setFlexDirection(FlexDirection.ROW);
                    String []SplitedMessageAttay = message.getText().split(" ");
                    for (String SingleWordOfText : SplitedMessageAttay)
                    {
                        if ( SingleWordOfText.startsWith("[["))
                        {
                            SingleWordOfText=SingleWordOfText.replace("[[","").replace("]]","");
                            GamesLayout.addView(MakeImageView(SingleWordOfText,itemView.getContext()));
                            GamesLayout.addView(MakeTextView("",itemView.getContext(),"System"));
                        }
                        else {
                            GamesLayout.addView(MakeTextView(SingleWordOfText,itemView.getContext(),"System"));
                        }
                    }

                }

            }


            // For Other user message
            else {
                outcomingUserMessage.setVisibility(View.VISIBLE);
                chatroom_system_messageText.setVisibility(View.GONE);
                chatroom_main_system_messageText.setVisibility(View.GONE);
                coins_catcher_button.setVisibility(View.GONE);
                GamesLayout.setVisibility(View.GONE);

                if (message.getMyrole().equals("1")) {
                    Glide.with(itemView.getContext()).load(R.drawable.user_icon).into(SenderTextRankImage);
                    SenderTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorBLue));
                } else if (message.getMyrole().equals("2")) {
                    Glide.with(itemView.getContext()).load(R.drawable.mod_icon).into(SenderTextRankImage);
                    SenderTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorRed));
                } else if (message.getMyrole().equals("3")) {
                    Glide.with(itemView.getContext()).load(R.drawable.staff_icon).into(SenderTextRankImage);
                    SenderTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorOrange));
                } else if (message.getMyrole().equals("4")) {
                    Glide.with(itemView.getContext()).load(R.drawable.mentor_icon).into(SenderTextRankImage);
                    SenderTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorGreen));
                } else if (message.getMyrole().equals("5")) {
                    Glide.with(itemView.getContext()).load(R.drawable.merchant_icon).into(SenderTextRankImage);
                    SenderTextUserName.setTextColor(itemView.getResources().getColor(R.color.violet));
                } else {
                    SenderTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorBLue));
                }

                if (message.getMyage().equals("1")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.born_male).into(SenderTextAgeImage);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.born_female).into(SenderTextAgeImage);
                    }
                } else if (message.getMyage().equals("2")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.teen_male).into(SenderTextAgeImage);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.teen_female).into(SenderTextAgeImage);
                    }
                } else if (message.getMyage().equals("3")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.young_male).into(SenderTextAgeImage);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.young_female).into(SenderTextAgeImage);
                    }
                } else if (message.getMyage().equals("4")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.adult_male).into(SenderTextAgeImage);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.adult_female).into(SenderTextAgeImage);
                    }
                } else if (message.getMyage().equals("5")) {
                    if (message.getMygender().equals("M")) {
                        Glide.with(itemView.getContext()).load(R.drawable.mature_male).into(SenderTextAgeImage);
                    } else {
                        Glide.with(itemView.getContext()).load(R.drawable.mature_female).into(SenderTextAgeImage);
                    }
                }
                Glide.with(itemView.getContext()).load(baseUrl + message.getUser().getAvatar()).into(chatroom_image_receiver);
                SenderTextUserName.setText(message.getMyname());

                if (bubble != null) {
                    bubble.setSelected(isSelected());
                }

                if (text != null) {

                    if (message.getText().contains("[[") && message.getText().contains("]]"))
                    {
                        // For Gaming Bot Message
                        WarfareBotMessage.setVisibility(View.VISIBLE);
                        time.setVisibility(View.VISIBLE);
                        text.setVisibility(View.GONE);

                        WarfareBotMessage.removeAllViews();
                        WarfareBotMessage.setFlexDirection(FlexDirection.ROW);
                        String []SplitedMessageAttay = message.getText().split(" ");
                        for (String SingleWordOfText : SplitedMessageAttay)
                        {
                            if ( SingleWordOfText.startsWith("[["))
                            {
                                SingleWordOfText=SingleWordOfText.replace("[[","").replace("]]","");
                                WarfareBotMessage.addView(MakeImageView(SingleWordOfText,itemView.getContext()));
                                WarfareBotMessage.addView(MakeTextView("",itemView.getContext(),"BotMessage"));
                            }
                            else {
                                WarfareBotMessage.addView(MakeTextView(SingleWordOfText,itemView.getContext(),"BotMessage"));
                            }
                        }

                    }
                    else {
                        // Other Message
                    if (message.getText().contains("www.youtube.com") || message.getText().contains("https://youtu.be/")) {
                        text.setVisibility(View.GONE);
                        time.setVisibility(View.GONE);
                        WarfareBotMessage.setVisibility(View.GONE);
                        YoutubeUrlImageViewCardviewChatRoom.setVisibility(View.VISIBLE);
                        YoutubeUrlImageViewChatRoom.setVisibility(View.VISIBLE);
                        String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(message.getText());
                        Glide.with(itemView.getContext()).load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/default.jpg").into(YoutubeUrlImageViewChatRoom);

                    } else {
                        text.setVisibility(View.VISIBLE);
                        time.setVisibility(View.VISIBLE);
                        WarfareBotMessage.setVisibility(View.GONE);
                        YoutubeUrlImageViewCardviewChatRoom.setVisibility(View.GONE);
                        YoutubeUrlImageViewChatRoom.setVisibility(View.GONE);
                        text.setText(message.getText());
                    }
                }
                }
            }

            coins_catcher_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("chatroom", MyCoinsGrabberChatRoom);
                        mSocket.emit("grab coins", jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            YoutubeUrlImageViewChatRoom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(message.getText())));
                    }
                    catch (Exception ignore){
                    }
                }
            });
            bubble.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    String textString;
                    myClipboard = (ClipboardManager)view.getContext().getSystemService(CLIPBOARD_SERVICE);
                    textString = message.getText();

                    myClip = ClipData.newPlainText("text", textString);
                    myClipboard.setPrimaryClip(myClip);

                    Toast.makeText(view.getContext(), "Text Copied",
                            Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            bubble.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MySocketsClass.static_objects.StaticEmojiconEditText.getText().toString().isEmpty()) {
                        MySocketsClass.static_objects.StaticEmojiconEditText.setText(message.getMyname());
                        MySocketsClass.static_objects.StaticEmojiconEditText.setSelection((message.getMyname()).length());
                    }
                    else {
                       String UserText= MySocketsClass.static_objects.StaticEmojiconEditText.getText().toString();
                        MySocketsClass.static_objects.StaticEmojiconEditText.setText(UserText+" "+message.getMyname());
                        MySocketsClass.static_objects.StaticEmojiconEditText.setSelection((UserText+" "+message.getMyname()).length());
                    }
                }
            });
        }

//        private String MakingUrlWithoutBrackets(String word)
//        {
//            String regx = "[[]]";
//            char[] ca = regx.toCharArray();
//            for (char c : ca) {
//                input = input.replace(""+c, "");
//            }
//        }

        private TextView MakeTextView(String SingleWord, Context context,String Type){

            TextView MyTextView = new TextView(context);
            RelativeLayout.LayoutParams LayoutParmsTextview=new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            LayoutParmsTextview.setMargins(0,8,0,0);
            MyTextView.setLayoutParams(LayoutParmsTextview);
            MyTextView.setText(SingleWord+" ");
            if (Type.equals("System")) {
                MyTextView.setTextSize(16);
            }
            else {

                MyTextView.setTextColor(context.getResources().getColor(R.color.dark_grey_two));
                MyTextView.setTextSize(17);
            }
            return MyTextView;
        }

        private ImageView MakeImageView(String SingleWord, Context context){


            ImageView MyImageView = new ImageView(context);
            RelativeLayout.LayoutParams LayoutParmsImageview=new RelativeLayout.LayoutParams(
                    dp2ToPx(18,context),dp2ToPx(16,context));
            LayoutParmsImageview.setMargins(0,8,0,0);
            MyImageView.setLayoutParams(LayoutParmsImageview);
            Log.e("testttta",baseUrl+SingleWord);
            Glide.with(itemView.getContext()).load(baseUrl+SingleWord).into(MyImageView);
            return MyImageView;
        }


        private String GettingVideoIdFromYoutubeLink(String url) {
            if (url.contains("v=")) {
                if (url.contains("&")) {
                    String[] u = url.split("v=");
                    u = u[1].split("&");
                    url = u[0];
                } else {
                    String[] u = url.split("v=");
                    url = u[1];
                }
            } else {
                String[] u = url.split("be/");
                url = u[1];
            }
            return url;
        }

        @Override
        public void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (bubble != null) {
                bubble.setPadding(15,
                        5,
                        15,
                        5);
                ViewCompat.setBackground(bubble, style.getIncomingBubbleDrawable());
            }

            if (text != null) {
                text.setTextColor(style.getIncomingTextColor());
                text.setTextSize(17);
                text.setTypeface(text.getTypeface(), style.getIncomingTextStyle());
                text.setAutoLinkMask(style.getTextAutoLinkMask());
                text.setLinkTextColor(style.getIncomingTextLinkColor());
                configureLinksBehavior(text);
            }
        }

        private void init(View itemView) {
            bubble = (ViewGroup) itemView.findViewById(R.id.bubble);
            text = (TextView) itemView.findViewById(R.id.messageText);
            SenderTextUserName = itemView.findViewById(R.id.SenderTextUserName);
            SenderTextRankImage = itemView.findViewById(R.id.SenderTextRankImage);
            SenderTextAgeImage = itemView.findViewById(R.id.SenderTextAgeImage);
            chatroom_image_receiver = itemView.findViewById(R.id.chatroom_image_receiver);
            outcomingUserMessage = itemView.findViewById(R.id.outcomingUserMessage);
            // Layout for System msgs joining/disconnecting
            system_message_text2 = itemView.findViewById(R.id.system_message_text2);
            system_msg_image2 = itemView.findViewById(R.id.system_msg_image2);
            system_msg_image1 = itemView.findViewById(R.id.system_msg_image1);
            system_message_text1 = itemView.findViewById(R.id.system_message_text1);
            chatroom_system_messageText = itemView.findViewById(R.id.chatroom_system_messageText);
            // For main System Message
            chatroom_main_system_messageText = itemView.findViewById(R.id.chatroom_main_system_messageText);
            main_system_message_text = itemView.findViewById(R.id.main_system_message_text);
            coins_catcher_button = itemView.findViewById(R.id.coins_catcher_button);
            YoutubeUrlImageViewChatRoom = itemView.findViewById(R.id.YoutubeUrlImageViewChatRoom);
            YoutubeUrlImageViewCardviewChatRoom = itemView.findViewById(R.id.YoutubeUrlImageViewCardviewChatRoom);
            GamesLayout = itemView.findViewById(R.id.GamesLayout);
            WarfareBotMessage = itemView.findViewById(R.id.WarfareBotMessage);

        }

        private int dp2ToPx(int dp,Context context) {
            float density = context.getResources()
                    .getDisplayMetrics()
                    .density;
            return Math.round((float) dp * density);
        }
    }

    /**
     * Default view holder implementation for outcoming text message
     */
    public static class OutcomingTextMessageViewHolder<MESSAGE extends IMessage>
            extends BaseOutcomingMessageViewHolder<MESSAGE> {

        protected ViewGroup bubble;
        protected TextView text, MyTextUserName;
        protected ImageView MyTextRankImage, MyTextAgeImage;
        protected CircleImageView chatroom_image_sender;
        protected CardView YoutubeUrlImageViewCardviewChatRoomOutComing;
        protected ImageView YoutubeUrlImageViewChatRoomOutComing;
        private ClipboardManager myClipboard;
        private ClipData myClip;

        @Deprecated
        public OutcomingTextMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public OutcomingTextMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(final MESSAGE message) {
            super.onBind(message);

            Log.e("messanger", message.getId());
            Log.e("messanger", message.getUser().getName());

            if (message.getMyrole().equals("1")) {
                Glide.with(itemView.getContext()).load(R.drawable.user_icon).into(MyTextRankImage);
                MyTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorBLue));
            } else if (message.getMyrole().equals("2")) {
                Glide.with(itemView.getContext()).load(R.drawable.mod_icon).into(MyTextRankImage);
                MyTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorRed));
            } else if (message.getMyrole().equals("3")) {
                Glide.with(itemView.getContext()).load(R.drawable.staff_icon).into(MyTextRankImage);
                MyTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorOrange));
            } else if (message.getMyrole().equals("4")) {
                Glide.with(itemView.getContext()).load(R.drawable.mentor_icon).into(MyTextRankImage);
                MyTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorGreen));
            } else if (message.getMyrole().equals("5")) {
                Glide.with(itemView.getContext()).load(R.drawable.merchant_icon).into(MyTextRankImage);
                MyTextUserName.setTextColor(itemView.getResources().getColor(R.color.violet));
            } else {
                MyTextUserName.setTextColor(itemView.getResources().getColor(R.color.colorBLue));
            }

            if (message.getMyage().equals("1")) {
                if (message.getMygender().equals("M")) {
                    Glide.with(itemView.getContext()).load(R.drawable.born_male).into(MyTextAgeImage);

                } else {
                    Glide.with(itemView.getContext()).load(R.drawable.born_female).into(MyTextAgeImage);
                }
            } else if (message.getMyage().equals("2")) {
                if (message.getMygender().equals("M")) {
                    Glide.with(itemView.getContext()).load(R.drawable.teen_male).into(MyTextAgeImage);
                } else {
                    Glide.with(itemView.getContext()).load(R.drawable.teen_female).into(MyTextAgeImage);
                }
            } else if (message.getMyage().equals("3")) {
                if (message.getMygender().equals("M")) {
                    Glide.with(itemView.getContext()).load(R.drawable.young_male).into(MyTextAgeImage);
                } else {
                    Glide.with(itemView.getContext()).load(R.drawable.young_female).into(MyTextAgeImage);
                }
            } else if (message.getMyage().equals("4")) {
                if (message.getMygender().equals("M")) {
                    Glide.with(itemView.getContext()).load(R.drawable.adult_male).into(MyTextAgeImage);
                } else {
                    Glide.with(itemView.getContext()).load(R.drawable.adult_female).into(MyTextAgeImage);
                }
            } else if (message.getMyage().equals("5")) {
                if (message.getMygender().equals("M")) {
                    Glide.with(itemView.getContext()).load(R.drawable.mature_male).into(MyTextAgeImage);
                } else {
                    Glide.with(itemView.getContext()).load(R.drawable.mature_female).into(MyTextAgeImage);
                }
            }
            Glide.with(itemView.getContext()).load(baseUrl + message.getUser().getAvatar()).into(chatroom_image_sender);
            MyTextUserName.setText(message.getMyname());

            if (bubble != null) {
                bubble.setSelected(isSelected());
            }

            if (text != null) {
                if (message.getText().contains("www.youtube.com") || message.getText().contains("https://youtu.be/")) {
                    String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(message.getText());
                    text.setVisibility(View.GONE);
                    YoutubeUrlImageViewCardviewChatRoomOutComing.setVisibility(View.VISIBLE);
                    YoutubeUrlImageViewChatRoomOutComing.setVisibility(View.VISIBLE);
                    Glide.with(itemView.getContext()).load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/default.jpg").into(YoutubeUrlImageViewChatRoomOutComing);

                } else {
                    text.setVisibility(View.VISIBLE);
                    YoutubeUrlImageViewCardviewChatRoomOutComing.setVisibility(View.GONE);
                    YoutubeUrlImageViewChatRoomOutComing.setVisibility(View.GONE);
                    text.setText(message.getText());
                }
            }
            YoutubeUrlImageViewChatRoomOutComing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(message.getText())));
                    }
                    catch (Exception ignore){
                    }
                }
            });
            bubble.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    String textString;
                    myClipboard = (ClipboardManager)view.getContext().getSystemService(CLIPBOARD_SERVICE);
                    textString = message.getText();

                    myClip = ClipData.newPlainText("text", textString);
                    myClipboard.setPrimaryClip(myClip);

                    Toast.makeText(view.getContext(), "Text Copied",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
            });
        }

        private String GettingVideoIdFromYoutubeLink(String url) {
            if (url.contains("v=")) {
                if (url.contains("&")) {
                    String[] u = url.split("v=");
                    u = u[1].split("&");
                    url = u[0];
                } else {
                    String[] u = url.split("v=");
                    url = u[1];
                }
            } else {
                String[] u = url.split("be/");
                url = u[1];
            }
            return url;
        }

        @Override
        public final void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (bubble != null) {
                bubble.setPadding(15,
                        5,
                        15,
                        5);
                ViewCompat.setBackground(bubble, style.getOutcomingBubbleDrawable());
            }

            if (text != null) {
                text.setTextColor(style.getOutcomingTextColor());
                text.setTextSize(17);
                text.setTypeface(text.getTypeface(), style.getOutcomingTextStyle());
                text.setAutoLinkMask(style.getTextAutoLinkMask());
                text.setLinkTextColor(style.getOutcomingTextLinkColor());
                configureLinksBehavior(text);
            }
        }

        private void init(View itemView) {
            bubble = (ViewGroup) itemView.findViewById(R.id.bubble);
            text = (TextView) itemView.findViewById(R.id.messageText);
            MyTextUserName = itemView.findViewById(R.id.MyTextUserName);
            MyTextRankImage = itemView.findViewById(R.id.MyTextRankImage);
            MyTextAgeImage = itemView.findViewById(R.id.MyTextAgeImage);
            chatroom_image_sender = itemView.findViewById(R.id.chatroom_image_sender);
            YoutubeUrlImageViewCardviewChatRoomOutComing = itemView.findViewById(R.id.YoutubeUrlImageViewCardviewChatRoomOutComing);
            YoutubeUrlImageViewChatRoomOutComing = itemView.findViewById(R.id.YoutubeUrlImageViewChatRoomOutComing);
        }
    }

    /**
     * Default view holder implementation for incoming image message
     */
    public static class IncomingImageMessageViewHolder<MESSAGE extends MessageContentType.Image>
            extends BaseIncomingMessageViewHolder<MESSAGE> {

        protected ImageView image;
        protected View imageOverlay;

        @Deprecated
        public IncomingImageMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public IncomingImageMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(MESSAGE message) {
            super.onBind(message);
            if (image != null && imageLoader != null) {
                imageLoader.loadImage(image, message.getImageUrl(), getPayloadForImageLoader(message));
            }

            if (imageOverlay != null) {
                imageOverlay.setSelected(isSelected());
            }
        }

        @Override
        public final void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getIncomingImageTimeTextStyle());
            }

            if (imageOverlay != null) {
                ViewCompat.setBackground(imageOverlay, style.getIncomingImageOverlayDrawable());
            }
        }

        /**
         * Override this method to have ability to pass custom data in ImageLoader for loading image(not avatar).
         *
         * @param message Message with image
         */
        protected Object getPayloadForImageLoader(MESSAGE message) {
            return null;
        }

        private void init(View itemView) {
            image = (ImageView) itemView.findViewById(R.id.image);
            imageOverlay = itemView.findViewById(R.id.imageOverlay);

            if (image instanceof RoundedImageView) {
                ((RoundedImageView) image).setCorners(
                        R.dimen.message_bubble_corners_radius,
                        R.dimen.message_bubble_corners_radius,
                        R.dimen.message_bubble_corners_radius,
                        0
                );
            }
        }
    }

    /**
     * Default view holder implementation for outcoming image message
     */
    public static class OutcomingImageMessageViewHolder<MESSAGE extends MessageContentType.Image>
            extends BaseOutcomingMessageViewHolder<MESSAGE> {

        protected ImageView image;
        protected View imageOverlay;

        @Deprecated
        public OutcomingImageMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public OutcomingImageMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(MESSAGE message) {
            super.onBind(message);
            if (image != null && imageLoader != null) {
                imageLoader.loadImage(image, message.getImageUrl(), getPayloadForImageLoader(message));
            }

            if (imageOverlay != null) {
                imageOverlay.setSelected(isSelected());
            }
        }

        @Override
        public final void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getOutcomingImageTimeTextStyle());
            }

            if (imageOverlay != null) {
                ViewCompat.setBackground(imageOverlay, style.getOutcomingImageOverlayDrawable());
            }
        }

        /**
         * Override this method to have ability to pass custom data in ImageLoader for loading image(not avatar).
         *
         * @param message Message with image
         */
        protected Object getPayloadForImageLoader(MESSAGE message) {
            return null;
        }

        private void init(View itemView) {
            image = (ImageView) itemView.findViewById(R.id.image);
            imageOverlay = itemView.findViewById(R.id.imageOverlay);

            if (image instanceof RoundedImageView) {
                ((RoundedImageView) image).setCorners(
                        R.dimen.message_bubble_corners_radius,
                        R.dimen.message_bubble_corners_radius,
                        0,
                        R.dimen.message_bubble_corners_radius
                );
            }
        }
    }

    /**
     * Default view holder implementation for date header
     */
    public static class DefaultDateHeaderViewHolder extends ViewHolder<Date>
            implements DefaultMessageViewHolder {

        protected TextView text;
        protected String dateFormat;
        protected DateFormatter.Formatter dateHeadersFormatter;

        public DefaultDateHeaderViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.messageText);
            text.setVisibility(View.GONE);
        }

        @Override
        public void onBind(Date date) {
            if (text != null) {
                String formattedDate = null;
                if (dateHeadersFormatter != null) formattedDate = dateHeadersFormatter.format(date);
                text.setText(formattedDate == null ? DateFormatter.format(date, dateFormat) : formattedDate);
            }
        }

        @Override
        public void applyStyle(MessagesListStyle style) {
            if (text != null) {
                text.setTextColor(style.getDateHeaderTextColor());
                text.setTextSize(14);
                text.setTypeface(text.getTypeface(), style.getDateHeaderTextStyle());
                text.setPadding(style.getDateHeaderPadding(), style.getDateHeaderPadding(),
                        style.getDateHeaderPadding(), style.getDateHeaderPadding());
            }
            dateFormat = style.getDateHeaderFormat();
            dateFormat = dateFormat == null ? DateFormatter.Template.STRING_DAY_MONTH_YEAR.get() : dateFormat;
        }
    }

    /**
     * Base view holder for incoming message
     */
    public abstract static class BaseIncomingMessageViewHolder<MESSAGE extends IMessage>
            extends BaseMessageViewHolder<MESSAGE> implements DefaultMessageViewHolder {

        protected TextView time;
        protected ImageView userAvatar;

        @Deprecated
        public BaseIncomingMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public BaseIncomingMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(MESSAGE message) {
            if (time != null) {
                time.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
            }

//            if (userAvatar != null) {
//                boolean isAvatarExists = imageLoader != null
//                        && message.getUser().getAvatar() != null
//                        && !message.getUser().getAvatar().isEmpty();
//
//                userAvatar.setVisibility(isAvatarExists ? View.VISIBLE : View.GONE);
//                if (isAvatarExists) {
//                    imageLoader.loadImage(userAvatar, message.getUser().getAvatar(), null);
//                }
//            }
        }

        @Override
        public void applyStyle(MessagesListStyle style) {
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getIncomingTimeTextStyle());
            }

//            if (userAvatar != null) {
//                userAvatar.getLayoutParams().width = style.getIncomingAvatarWidth();
//                userAvatar.getLayoutParams().height = style.getIncomingAvatarHeight();
//            }

        }

        private void init(View itemView) {
            time = (TextView) itemView.findViewById(R.id.messageTime);
            //     userAvatar = (ImageView) itemView.findViewById(R.id.messageUserAvatar);
        }
    }

    /**
     * Base view holder for outcoming message
     */
    public abstract static class BaseOutcomingMessageViewHolder<MESSAGE extends IMessage>
            extends BaseMessageViewHolder<MESSAGE> implements DefaultMessageViewHolder {

        protected TextView time;

        @Deprecated
        public BaseOutcomingMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public BaseOutcomingMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(MESSAGE message) {
            if (time != null) {
                time.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
            }
        }

        @Override
        public void applyStyle(MessagesListStyle style) {
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getOutcomingTimeTextStyle());
            }
        }

        private void init(View itemView) {
            time = (TextView) itemView.findViewById(R.id.messageTime);
        }
    }

    /*
     * DEFAULTS
     * */

    interface DefaultMessageViewHolder {
        void applyStyle(MessagesListStyle style);
    }

    private static class DefaultIncomingTextMessageViewHolder
            extends IncomingTextMessageViewHolder<IMessage> {

        public DefaultIncomingTextMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class DefaultOutcomingTextMessageViewHolder
            extends OutcomingTextMessageViewHolder<IMessage> {

        public DefaultOutcomingTextMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class DefaultIncomingImageMessageViewHolder
            extends IncomingImageMessageViewHolder<MessageContentType.Image> {

        public DefaultIncomingImageMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class DefaultOutcomingImageMessageViewHolder
            extends OutcomingImageMessageViewHolder<MessageContentType.Image> {

        public DefaultOutcomingImageMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class ContentTypeConfig<TYPE extends MessageContentType> {

        private byte type;
        private HolderConfig<TYPE> incomingConfig;
        private HolderConfig<TYPE> outcomingConfig;

        private ContentTypeConfig(
                byte type, HolderConfig<TYPE> incomingConfig, HolderConfig<TYPE> outcomingConfig) {

            this.type = type;
            this.incomingConfig = incomingConfig;
            this.outcomingConfig = outcomingConfig;
        }
    }

    private class HolderConfig<T extends IMessage> {

        protected Class<? extends BaseMessageViewHolder<? extends T>> holder;
        protected int layout;
        protected Object payload;

        HolderConfig(Class<? extends BaseMessageViewHolder<? extends T>> holder, int layout) {
            this.holder = holder;
            this.layout = layout;
        }

        HolderConfig(Class<? extends BaseMessageViewHolder<? extends T>> holder, int layout, Object payload) {
            this.holder = holder;
            this.layout = layout;
            this.payload = payload;
        }
    }


}
