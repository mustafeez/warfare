package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Interfaces.WallShoutingInterface;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.CategrizedShoutsModelClass;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CategrizedShoutsAdapter extends RecyclerView.Adapter<CategrizedShoutsAdapter.ViewHolder> {

    private ArrayList<CategrizedShoutsModelClass> categryShoutsArrayList=new ArrayList<>();
    Context myContext;
    WallShoutingInterface wallShoutingInterface;
    int shoutingCategryShowingNumber;

    public CategrizedShoutsAdapter(ArrayList<CategrizedShoutsModelClass> categryShoutsArrayList, Context myContext, int shoutingCategryShowingNumber,
                                   WallShoutingInterface wallShoutingInterface) {
        super();
        this.categryShoutsArrayList=categryShoutsArrayList;
        this.myContext=myContext;
        this.shoutingCategryShowingNumber=shoutingCategryShowingNumber;
        this.wallShoutingInterface=wallShoutingInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.categrized_shouts_layout, viewGroup, false);
        return new CategrizedShoutsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategrizedShoutsAdapter.ViewHolder viewHolder, final int i) {

        if (shoutingCategryShowingNumber==i){
            viewHolder.mainLayout.setBackgroundResource(R.drawable.darkgray_circle);
            viewHolder.categryShoutNameTextView.setTextColor(viewHolder.itemView.getResources().getColor(R.color.whitecolor));
        }
        else {
            viewHolder.mainLayout.setBackgroundResource(R.drawable.lytgray_circle_drkgray_corners);
            viewHolder.categryShoutNameTextView.setTextColor(viewHolder.itemView.getResources().getColor(R.color.black));
        }
        viewHolder.categryShoutNameTextView.setText(categryShoutsArrayList.get(i).getCategryName());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categryShoutsArrayList.get(i).getCategryName().equals("From User")){
                    //show shouts by specific username
                    wallShoutingInterface.ShowingFromdSpecialName();
                }
                else if (categryShoutsArrayList.get(i).getCategryName().equals("Following")){
                    //show shouts by Followings
                    wallShoutingInterface.ShowingFromFollowings();
                }
                else if (categryShoutsArrayList.get(i).getCategryName().equals("All")){
                    //show shouts by Public
                    wallShoutingInterface.ShowingFromPublic();
                }
                else if (categryShoutsArrayList.get(i).getCategryName().equals("My Shouts")){
                    //show shouts by me
                    wallShoutingInterface.ShowingOnlyMyPosts();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return categryShoutsArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView categryShoutNameTextView;
        RelativeLayout mainLayout;
        public ViewHolder( View itemView) {
            super(itemView);
            categryShoutNameTextView=itemView.findViewById(R.id.categryShoutNameTextView);
            mainLayout=itemView.findViewById(R.id.mainLayout);
        }
    }
}