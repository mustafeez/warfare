package com.app.websterz.warfarechat.activities.BuyShields;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.PrivateChat.GiftsReceivingActivityPopup;
import com.app.websterz.warfarechat.activities.SendAttacks.SendAttackModelClass;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class BuyShieldRecyclerViewAdapter extends RecyclerView.Adapter<BuyShieldRecyclerViewAdapter.ViewHolder> {

    ArrayList<BuyShieldModelClass> ShieldList ;
    String ProtectionRemaining;
    int CoinQuantity,CoinCost,FinalCost;
    Context context;
    int shieldNamePosition;

    public BuyShieldRecyclerViewAdapter(ArrayList<BuyShieldModelClass> attackList, Context context) {
        ShieldList = attackList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.buy_shield_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.shieldName.setText(ShieldList.get(position).getShieldName() );
        holder.shieldDetail.setText(ShieldList.get(position).getShieldDetail());
        ProtectionRemaining = ShieldList.get(position).getShieldRemaining();
        holder.shieldCoins.setText(ShieldList.get(position).getShieldCost() + " Points");
        if(ProtectionRemaining.equals("0"))
        {
            holder.shieldRemaining.setText("No protections purchased");
        }
        else if(ProtectionRemaining.equals("1")){
            holder.shieldRemaining.setText(ShieldList.get(position).getShieldRemaining() + " protection remaining");
        }
        else{holder.shieldRemaining.setText(ShieldList.get(position).getShieldRemaining() + " protections remaining");}
        String Image = ShieldList.get(position).getShieldImageView();

        //Add AvatarView Loader
        holder.ShieldAvatarLoader.setVisibility(View.VISIBLE);
        holder.ShieldAvatarLoader.setAnimating(true);

        Glide.with(holder.itemView.getContext()).load(baseUrl +Image).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.ShieldAvatarLoader.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.ShieldAvatarLoader.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.shieldImageView);

        holder.shieldBuyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shieldNamePosition = position;
                CoinCost = Integer.parseInt(ShieldList.get(shieldNamePosition).ShieldCost);
                String coinQ,test;

                test = holder.purchaseCoin.getText().toString();
                if (test.equals(""))
                {
                    coinQ ="0";
                }
                else {

                    coinQ = holder.purchaseCoin.getText().toString();

                }

                CoinQuantity = Integer.parseInt(coinQ);

                FinalCost = (CoinCost * CoinQuantity);


                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        GiftsReceivingActivityPopup giftsReceivingActivityPopup = new GiftsReceivingActivityPopup();
                        giftsReceivingActivityPopup.GiftsPopup(MySocketsClass.static_objects.MyStaticActivity, null,
                                "MyBuyingShield",
                                String.valueOf(CoinQuantity),
                                ShieldList.get(shieldNamePosition).getShieldSlug(),
                                String.valueOf(FinalCost));
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return ShieldList.size() ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        EditText purchaseCoin;
        ImageView shieldImageView;
        TextView shieldName,shieldDetail,shieldCoins,shieldRemaining;
        Button shieldBuyButton;
        AvatarView ShieldAvatarLoader;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ShieldAvatarLoader = itemView.findViewById(R.id.ShieldAvatarLoader);
            shieldImageView = itemView.findViewById(R.id.shieldImageView);

            shieldName = itemView.findViewById(R.id.shieldnameTextView);
            shieldDetail = itemView.findViewById(R.id.shieldDescriptionTextView);
            shieldCoins = itemView.findViewById(R.id.shieldCoinsNumberTextView);
            shieldRemaining = itemView.findViewById(R.id.shieldRemainingTextView);
            purchaseCoin = itemView.findViewById(R.id.shieldInputEditText);

            shieldBuyButton = itemView.findViewById(R.id.shieldBuyBtn);

        }
    }
}
