package com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments;


import android.content.Context;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.constants.AppConstants;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.adapters.ContactsRecyclerAdapter;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment implements View.OnClickListener {
    public static JSONArray jsonArray;
    public static List<JSONObject> jsonList;
    FriendRequestInterface friendRequestInterface;

    public static TextView contactsFragmentTotalContactCount;
    RelativeLayout homeFragment1AddFriendBtn, parent_layout;
    EditText contactsFragmentSearchBar;
    View view;
    CircleImageView toast_image;
    TextView toast_text;
    RecyclerView homeScreensContactsRv;
    ContactsRecyclerAdapter contactsRecyclerAdapter;
    AppConstants appConstants;
 //   CustomDialogError customDialogError;
    public static String total = null;
    JSONArray data;
    LinearLayout parent_child_layout, loader_imageview_Layout;

    public static String addContactUsername = "";
    private JSONArray sortedJsonArray;

    public ContactsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home_fragment1, container, false);
        friendRequestInterface = (FriendRequestInterface) view.getContext();
        Log.w(TAG, "onCreateView: called");
        parent_layout = view.findViewById(R.id.parent_layout);
        parent_child_layout = view.findViewById(R.id.parent_child_layout);
        initView();
        if (mSocket != null) {
            mSocket.on("update user status", update_user_status);
            getContacts();
            showingLoader();
            mSocket.on("load contact list", loadContacts);

        }
        return view;
    }

    private void initView() {
        contactsFragmentTotalContactCount = view.findViewById(R.id.contactsFragmentTotalContactCount);
 //       customDialogError = new CustomDialogError();
        homeFragment1AddFriendBtn = view.findViewById(R.id.homeFragment1AddFriendBtn);
        toast_text = view.findViewById(R.id.toast_text);
        toast_image = view.findViewById(R.id.toast_image);
        homeFragment1AddFriendBtn.setOnClickListener(this);
        data = new JSONArray();
        contactsRecyclerAdapter = new ContactsRecyclerAdapter(data);
        homeScreensContactsRv = view.findViewById(R.id.homeScreenContactsRv);
        homeScreensContactsRv.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        homeScreensContactsRv.setAdapter(contactsRecyclerAdapter);


        contactsFragmentSearchBar = view.findViewById(R.id.contactsFragmentSearchBar);
        contactsFragmentSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (contactsRecyclerAdapter != null) {
                    contactsRecyclerAdapter.getFilter().filter(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public void getContacts() {
        jsonArray = null;
        jsonArray = new JSONArray();
        mSocket.emit("get contact list", "");
    }

    private void test() throws JSONException {
        sortedJsonArray = new JSONArray();
        jsonList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            jsonList.add(jsonArray.getJSONObject(i));
        }

        Collections.sort(jsonList, new Comparator<JSONObject>() {
            public int compare(JSONObject a, JSONObject b) {
                String valA = "";
                String valB = "";

                try {
                    valA = String.valueOf(a.get("presence"));
                    valB = String.valueOf(b.get("presence"));

                } catch (JSONException e) {
                    Log.w(TAG, "valee: " + e);
                    //do something
                }
                return String.valueOf(valB).compareTo(String.valueOf(valA));
            }
        });

        for (int i = 0; i < jsonArray.length(); i++) {
            sortedJsonArray.put(jsonList.get(i));
        }

        Log.w(TAG, "sortedJson: " + sortedJsonArray);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.homeFragment1AddFriendBtn: {
                jsonArray = null;
                jsonArray = new JSONArray();
                MySocketsClass.getInstance().showErrorDialog(getActivity(), "", "", "addFriend", "");
                break;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        jsonArray = null;
        jsonArray = new JSONArray();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!contactsFragmentSearchBar.getText().equals("")) {
            contactsFragmentSearchBar.setText("");
        }
        jsonArray = null;
        jsonArray = new JSONArray();
        contactsFragmentSearchBar.setFocusable(true);
    }

    public interface FriendRequestInterface {
        public void requests(JSONArray jsonArray, int index, int type);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void showingLoader() {
        loader_imageview_Layout = MySocketsClass.custom_layout.Loader_image(getContext(), parent_layout);
    }

    private void hidingLoader() {
        if (loader_imageview_Layout != null && loader_imageview_Layout.getVisibility() == View.VISIBLE) {
            loader_imageview_Layout.setVisibility(View.GONE);
        }
    }

    private Emitter.Listener newAddContact = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    jsonArray = null;
                    jsonArray = new JSONArray();
                    Log.e("request", "request");

                    JSONObject jsonObject = (JSONObject) args[0];
                    final MediaPlayer mediaPlayer;
                    try {
                        addContactUsername = jsonObject.getString("username");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    CustomDialogError customDialogError = new CustomDialogError();
                    MySocketsClass.getInstance().showErrorDialog(getActivity(), "", "You received a new friend request from " + addContactUsername, "newAddContact", "");
                    Log.d(TAG, "new Add contact fragmnet");
                    mediaPlayer = MediaPlayer.create(getContext(), R.raw.add_request);

                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mediaPlayer.start();
                        }
                    });
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mediaPlayer.release();
                        }
                    });
                }
            });
        }
    };

    public Emitter.Listener loadContacts = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hidingLoader();
                        parent_child_layout.setVisibility(View.VISIBLE);
                        if (args.length == 0) {
                            Log.e("visbly", "sending 0");
                            contactsFragmentTotalContactCount.setText("(" + "0" + ")");
                            data = new JSONArray();
                            contactsRecyclerAdapter = new ContactsRecyclerAdapter(data);
                            contactsRecyclerAdapter.notifyDataSetChanged();
                            homeScreensContactsRv.setAdapter(contactsRecyclerAdapter);
                            Log.e("Loader", "loadcontacts in if");
                            parent_child_layout.setVisibility(View.VISIBLE);
                        } else {

                            Log.e("Loader", "loadcontacts 2");
                            parent_child_layout.setVisibility(View.VISIBLE);

                            data = (JSONArray) args[0];
                            try {
                                String total = data.getJSONObject(0).getString("total");
                                contactsFragmentTotalContactCount.setText("(" + total + ")");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //   }
                            contactsRecyclerAdapter = new ContactsRecyclerAdapter(data);
                            contactsRecyclerAdapter.notifyDataSetChanged();
                            homeScreensContactsRv.setAdapter(contactsRecyclerAdapter);
                            Log.e("Loader", "loadcontacts in else");
                            parent_child_layout.setVisibility(View.VISIBLE);

                            try {
                                test();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }
    };


    Emitter.Listener update_user_status = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getContacts();
                    }
                });
            }

        }
    };
}
