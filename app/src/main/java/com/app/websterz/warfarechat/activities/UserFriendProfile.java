package com.app.websterz.warfarechat.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;

import com.app.websterz.warfarechat.TestingPackage.NewPrivateChat;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.fragments.contacts.fragments.ContactsFragment;
import com.app.websterz.warfarechat.my_profile.AboutProfileActivity;
import com.app.websterz.warfarechat.my_profile.My_profile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.emojione.tools.Client;
import com.github.nkzawa.emitter.Emitter;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;
import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class
UserFriendProfile extends AppCompatActivity {
    private String emitted_username, isFollowd, userdpUrl,aboutString;
    private CircleImageView friends_profile_image, friend_role;
    private TextView friend_username, friend_status_text, friend_precense_text, friend_name, user_gender, users_country_imgs,
            user_country_name, user_footprints, user_heart_likes, user_gifts, followersTotal, shoutsTotal, friendsTotal, errorDialogText1,
            errorDialogText2, userRankText;
    private ImageView users_age, buzzButton, friend_added_imageview, topGreenView, footPrintImageView,request_status,
            giftsImageView, heartImageView, followersImageView, shoutsImageView, friendsImageView,abtButton;
    private LinearLayout returning_imageview, ParentLayout, parent_child_layout, privateProfileLayout;
    private Button  privateProfileBtn, errorDialogTryAgainBtn, followButton;
    private Client client;
    private SparkButton heart_img;
    private int liker = 0;
    private RelativeLayout parentLinearLayout;
    private AlertDialog MyalertDialog;
    private int OneTimeButtonClick = 0;
    private String reqStatus = null, add_request = null, MyImageDownloadedPath = "/Socio/Profile Images";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_friend_profile);

        initializations();
        mycode();

        buzzButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byzzingUser();
            }
        });
        friends_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userdpUrl != null) {
                    MySocketsClass.getInstance().openingZoomingActivity(UserFriendProfile.this,baseUrl +userdpUrl,"profile");
                }
            }
        });

        request_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestingOrChatting();
            }
        });
        followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followingUser();
            }
        });
        abtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openingAboutUserActivity();
            }
        });
    }

    private void initializations() {
        Intent intent = getIntent();
        emitted_username = intent.getExtras().getString("username");
        client = new Client(UserFriendProfile.this);

        friends_profile_image = findViewById(R.id.friends_profile_image);
        friend_role = findViewById(R.id.friend_role);
        friend_username = findViewById(R.id.friend_username);
        friend_status_text = findViewById(R.id.friend_status_text);
        friend_precense_text = findViewById(R.id.friend_precense_text);
        friend_name = findViewById(R.id.friend_name);
        user_gender = findViewById(R.id.user_gender);
        users_country_imgs = findViewById(R.id.users_country_imgs);
        user_country_name = findViewById(R.id.user_country_name);
        user_footprints = findViewById(R.id.user_footprints);
        user_heart_likes = findViewById(R.id.user_heart_likes);
        user_gifts = findViewById(R.id.user_gifts);
        shoutsTotal = findViewById(R.id.shoutsTotal);
        friendsTotal = findViewById(R.id.friendsTotal);
        followersTotal = findViewById(R.id.followersTotal);
        users_age = findViewById(R.id.users_age);
        request_status = findViewById(R.id.request_status);
        heart_img = findViewById(R.id.heart_img);
        abtButton = findViewById(R.id.abtButton);
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        parent_child_layout = findViewById(R.id.parent_child_layout);
        privateProfileLayout = findViewById(R.id.privateProfileLayout);
        privateProfileBtn = findViewById(R.id.privateProfileBtn);
        buzzButton = findViewById(R.id.buzzButton);
        topGreenView = findViewById(R.id.topGreenView);
        footPrintImageView = findViewById(R.id.footPrintImageView);
        giftsImageView = findViewById(R.id.giftsImageView);
        heartImageView = findViewById(R.id.heartImageView);
        followersImageView = findViewById(R.id.followersImageView);
        shoutsImageView = findViewById(R.id.shoutsImageView);
        friendsImageView = findViewById(R.id.friendsImageView);
        followButton = findViewById(R.id.followButton);
        userRankText = findViewById(R.id.userRankText);
        Glide.with(UserFriendProfile.this).load(R.drawable.green_background_top).into(topGreenView);
        Glide.with(UserFriendProfile.this).load(R.drawable.green_icon_footprint).into(footPrintImageView);
        Glide.with(UserFriendProfile.this).load(R.drawable.green_icon_gift).into(giftsImageView);
        Glide.with(UserFriendProfile.this).load(R.drawable.green_icon_heart).into(heartImageView);
        Glide.with(UserFriendProfile.this).load(R.drawable.green_icon_followers).into(followersImageView);
        Glide.with(UserFriendProfile.this).load(R.drawable.green_icon_shout).into(shoutsImageView);
        Glide.with(UserFriendProfile.this).load(R.drawable.green_user_filled).into(friendsImageView);
    }

    private void mycode() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", emitted_username);
            jsonObject.put("type", 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.off(MySocketsClass.ListenersClass.USER_PROFILE_RESPONSE);
        mSocket.emit(MySocketsClass.EmmittersClass.GETTING_USER_PROFILE, jsonObject);
        returning_imageview = MySocketsClass.custom_layout.Loader_image(UserFriendProfile.this, parentLinearLayout);
        mSocket.on(MySocketsClass.ListenersClass.USER_PROFILE_RESPONSE, user_profile_response);
        privateProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void requestingOrChatting() {
        //hereee
        if (reqStatus.equals("0:0") && add_request.equals("1")) {

            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("add_username", emitted_username);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.off("add contact response");
            mSocket.emit("add contact", jsonObject);
            mSocket.on("add contact response", adding_contact_response);

        } else if (reqStatus.equals("1:0")) {
            Snackbar.make(parentLinearLayout, "Already friend added", BaseTransientBottomBar.LENGTH_LONG).show();
        } else if (reqStatus.equals("0:1")) {
            //acpt krni hy request
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("username", emitted_username);
                mSocket.emit(MySocketsClass.EmmittersClass.APPROVE_ADD_REQUEST, jsonObject);
                reqStatus = "1:1";
                request_status.setBackgroundResource(R.drawable.error_circle_blue_gray);
                request_status.setImageResource(R.drawable.start_chat);

            } catch (Exception e) {

            }
        } else if (reqStatus.equals("1:1")) {
            Intent intent = new Intent(UserFriendProfile.this, NewPrivateChat.class);
            intent.putExtra("FriendChatName", emitted_username);
            intent.putExtra("BackActivity", "contactsChats");
            startActivity(intent);
        } else {
            Snackbar.make(parentLinearLayout, "Unable to access at this moment.", BaseTransientBottomBar.LENGTH_LONG).show();
        }
    }


    private void byzzingUser() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", emitted_username);
            mSocket.emit(MySocketsClass.EmmittersClass.REQUESTING_BUZZ, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0) {

                    boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (write_str && read_str && record_str) {
                        Toast.makeText(UserFriendProfile.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(UserFriendProfile.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }


    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    private String changingUnit(long number) {

        if (number < 1000) return Long.toString(number); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(number);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = number / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    private void followingUser() {

        Toast.makeText(UserFriendProfile.this, "Following", Toast.LENGTH_SHORT).show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", emitted_username);
            mSocket.off(MySocketsClass.ListenersClass.FOLLOW_USER_RESPONSE);
            mSocket.emit(MySocketsClass.EmmittersClass.FOLLOW_USER, jsonObject);
            mSocket.on(MySocketsClass.ListenersClass.FOLLOW_USER_RESPONSE, follow_user_response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openingAboutUserActivity(){
        Intent intent=new Intent(UserFriendProfile.this, AboutProfileActivity.class);
        intent.putExtra("AboutText",aboutString);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.MyStaticActivity = UserFriendProfile.this;
        MySocketsClass.static_objects.global_layout = parentLinearLayout;
    }


    Emitter.Listener follow_user_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                JSONObject jsonObject = (JSONObject) args[0];

                @Override
                public void run() {
                    Log.e("Folew", "listener");

                    try {
                        String success = jsonObject.getString("success");
                        String type = jsonObject.getString("type");
                        Log.e("FollowTest", "inListener");
                        if (success.equals("1")) {
                            Toast.makeText(UserFriendProfile.this, "Success > " + type, Toast.LENGTH_SHORT).show();
                            if (isFollowd.equals("1")) {
                                isFollowd = "2";
                                followButton.setText("Follow");
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(UserFriendProfile.this);
                                Intent intent = new Intent("followORunfoolow");
                                intent.putExtra("followStatus", "UnFollowed");
                                intent.putExtra("emittedUserName", emitted_username);
                                localBroadcastManager.sendBroadcast(intent);
                            } else {
                                isFollowd = "1";
                                followButton.setText("Unfollow");
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(UserFriendProfile.this);
                                Intent intent = new Intent("followORunfoolow");
                                intent.putExtra("followStatus", "Followed");
                                intent.putExtra("emittedUserName", emitted_username);
                                localBroadcastManager.sendBroadcast(intent);
                            }
                        }

//                        if (success.equals("1") && type.equals("1")) {
//                            Log.e("FollowTest", "InIf");
//
//                            for (int i = 0; i < UserWallArraylist.size(); i++) {
//                                Log.e("FollowTest", "InIf For loop");
//                                if (UserWallArraylist.get(i).getPostsUsername().equals(FollowButtonClickUserName)) {
//                                    Log.e("FollowTest", "InIf changing to unfoolow");
//                                    UserWallArraylist.get(i).setPostsfollow("1");
//                                    //   MyViewHolder.FollowButon.setText("Unfollow");
//                                }
//                            }
//
//                        } else {
//                            Log.e("FollowTest", "Inelse");
//                            for (int i = 0; i < UserWallArraylist.size(); i++) {
//                                Log.e("FollowTest", "Inelse For loop");
//                                if (UserWallArraylist.get(i).getPostsUsername().equals(FollowButtonClickUserName)) {
//                                    Log.e("FollowTest", "Inelse changing to foolow");
//                                    UserWallArraylist.get(i).setPostsfollow("0");
//                                    //  MyViewHolder.FollowButon.setText("Follow");
//                                }
//                            }
//
//                        }
//                        notifyDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    Emitter.Listener user_profile_response = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    String code = null, emoji = null, name = null;
                    try {
                        if (returning_imageview.getVisibility() == View.VISIBLE) {
                            returning_imageview.setVisibility(View.GONE);
                        }
                        if (jsonObject.getJSONObject("profile").has("error")) {
                            privateProfileLayout.setVisibility(View.VISIBLE);
                        } else {
                            parent_child_layout.setVisibility(View.VISIBLE);
                            final String username = jsonObject.getJSONObject("profile").getString("username");
                            userdpUrl = jsonObject.getJSONObject("profile").getString("dp");
                            String gender = jsonObject.getJSONObject("profile").getString("gender");
                            aboutString = jsonObject.getJSONObject("profile").getString("about");
                            if (jsonObject.getJSONObject("profile").has("name")) {
                                name = jsonObject.getJSONObject("profile").getString("name");
                            }
                            long time_reg = jsonObject.getJSONObject("profile").getLong("time_reg");
                            String role = jsonObject.getJSONObject("profile").getString("role");
                            String age = jsonObject.getJSONObject("profile").getString("age");
                            String statusMsg = jsonObject.getJSONObject("profile").getString("statusMsg");
                            add_request = jsonObject.getJSONObject("profile").getString("add_requests");
                            long last_disconnected = jsonObject.getJSONObject("profile").getLong("last_disconnected");
                            String presence = jsonObject.getString("presence");
                            final String liked = jsonObject.getString("liked");
                            final String like = jsonObject.getString("likes");
                            String gifts = jsonObject.getString("gifts");
                            String followers = jsonObject.getString("followers");
                            isFollowd = jsonObject.getString("follow");
                            String shouts = jsonObject.getString("shouts");
                            String friends = jsonObject.getString("friends");
                            String footprints = jsonObject.getString("footprints");
                            reqStatus = jsonObject.getString("reqStatus");
                            if (jsonObject.getJSONObject("country").has("code")) {
                                code = jsonObject.getJSONObject("country").getString("code");
                            }
                            if (jsonObject.getJSONObject("country").has("emoji")) {
                                emoji = jsonObject.getJSONObject("country").getString("emoji");
                            }

                            friend_username.setText("@" + username);
                            if (isFollowd.equals("1")) {
                                followButton.setText("Unfollow");
                            } else {
                                followButton.setText("Follow");
                            }

                            //   Picasso.get().load(baseUrl + dp).into(friends_profile_image);
                            GlideApp.with(UserFriendProfile.this).load(baseUrl + userdpUrl).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    return false;
                                }
                            }).
                                    into(friends_profile_image);

                            if (name != null) {
                                if (name.equals("")) {
                                    friend_name.setVisibility(View.GONE);
                                } else {
                                    friend_name.setText(name);
                                }
                            }
                            friend_status_text.setText(statusMsg);

                            if (gender.equals("M")) {
                                user_gender.setText("Male");
                            } else {
                                user_gender.setText("Female");
                            }
                            user_heart_likes.setText(changingUnit(Integer.parseInt(like)));
                            if (liked.equals("1")) {
                                heart_img.setChecked(true);
                            } else {
                                heart_img.setChecked(false);
                            }
                            heart_img.setEventListener(new SparkEventListener() {
                                @Override
                                public void onEvent(ImageView button, boolean buttonState) {
                                    if (liked.equals("1") || (liked.equals("0") && liker == 1)) {
                                        heart_img.setChecked(true);
                                    } else if (liked.equals("0") && liker == 0) {

                                        JSONObject jsonObject1 = new JSONObject();
                                        try {
                                            jsonObject1.put("username", emitted_username);
                                            mSocket.emit(MySocketsClass.EmmittersClass.ADD_LIKE, jsonObject1);
                                            mSocket.on(MySocketsClass.ListenersClass.ADD_LIKE_RESPONSE, add_like_response);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onEventAnimationEnd(ImageView button, boolean buttonState) {

                                }

                                @Override
                                public void onEventAnimationStart(ImageView button, boolean buttonState) {

                                }
                            });

                            if (role.equals("1")) {
                                Glide.with(UserFriendProfile.this).load(R.drawable.user_icon).into(friend_role);
                                userRankText.setText("User");
                            } else if (role.equals("2")) {
                                Glide.with(UserFriendProfile.this).load(R.drawable.mod_icon).into(friend_role);
                                userRankText.setText("Admin");
                            } else if (role.equals("3")) {
                                Glide.with(UserFriendProfile.this).load(R.drawable.staff_icon).into(friend_role);
                                userRankText.setText("Staff");
                            } else if (role.equals("4")) {
                                Glide.with(UserFriendProfile.this).load(R.drawable.mentor_icon).into(friend_role);
                                userRankText.setText("Mentor");
                            } else if (role.equals("5")) {
                                Glide.with(UserFriendProfile.this).load(R.drawable.merchant_icon).into(friend_role);
                                userRankText.setText("Merchant");
                            }

                            if (age.equals("1")) {
                                if (gender.equals("M")) {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.born_male).into(users_age);
                                } else {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.born_female).into(users_age);
                                }
                            } else if (age.equals("2")) {
                                if (gender.equals("M")) {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.teen_male).into(users_age);
                                } else {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.teen_female).into(users_age);
                                }
                            } else if (age.equals("3")) {
                                if (gender.equals("M")) {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.young_male).into(users_age);
                                } else {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.young_female).into(users_age);
                                }
                            } else if (age.equals("4")) {
                                if (gender.equals("M")) {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.adult_male).into(users_age);
                                } else {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.adult_female).into(users_age);
                                }
                            } else if (age.equals("5")) {
                                if (gender.equals("M")) {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.mature_male).into(users_age);
                                } else {
                                    Glide.with(UserFriendProfile.this).load(R.drawable.mature_female).into(users_age);
                                }
                            }
                            if (code != null) {
                                user_country_name.setText(code);
                            }
                            if (emoji != null) {
                                client.toImage(emoji, 50, new com.emojione.tools.Callback() {
                                    @Override
                                    public void onFailure(IOException e) {
                                    }

                                    @Override
                                    public void onSuccess(SpannableStringBuilder ssb) {
                                        users_country_imgs.setText(ssb);

                                    }
                                });
                            }

                            final int SECOND_MILLIS = 1000;
                            final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
                            final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
                            final int DAY_MILLIS = 24 * HOUR_MILLIS;
                            String last_final_time_reg = null, final_last_disconnected2;
                            long time_reg2 = time_reg * 1000;

                            long last_disconnected2 = last_disconnected * 1000;
                            long now = System.currentTimeMillis();
                            final long diff2 = now - time_reg2;
                            if (diff2 < MINUTE_MILLIS) {
                                last_final_time_reg = "just now";
                            } else if (diff2 < 2 * MINUTE_MILLIS) {
                                last_final_time_reg = "a minute ago";
                            } else if (diff2 < 50 * MINUTE_MILLIS) {
                                last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
                            } else if (diff2 < 90 * MINUTE_MILLIS) {
                                last_final_time_reg = "an hour ago";
                            } else if (diff2 < 24 * HOUR_MILLIS) {
                                last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago";
                            } else if (diff2 < 48 * HOUR_MILLIS) {
                                last_final_time_reg = "yesterday";
                            } else {
                                //  last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
                                long calculating_months = diff2 / DAY_MILLIS;
                                if (calculating_months < 30) {
                                    last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
                                } else if (calculating_months >= 30 && calculating_months <= 360) {
                                    last_final_time_reg = calculating_months / 30 + " Month(s) ago";
                                } else if (calculating_months >= 30 && calculating_months > 360) {
                                    last_final_time_reg = calculating_months / 360 + " Year(s) ago";
                                }
                            }

                            final long diff = now - last_disconnected2;
                            if (diff < MINUTE_MILLIS) {
                                final_last_disconnected2 = "just now";
                            } else if (diff < 2 * MINUTE_MILLIS) {
                                final_last_disconnected2 = "a minute ago";
                            } else if (diff < 50 * MINUTE_MILLIS) {
                                final_last_disconnected2 = diff / MINUTE_MILLIS + " minutes ago";
                            } else if (diff < 90 * MINUTE_MILLIS) {
                                final_last_disconnected2 = "an hour ago";
                            } else if (diff < 24 * HOUR_MILLIS) {
                                final_last_disconnected2 = diff / HOUR_MILLIS + " hours ago";
                            } else if (diff < 48 * HOUR_MILLIS) {
                                final_last_disconnected2 = "yesterday";
                            } else {
                                final_last_disconnected2 = diff / DAY_MILLIS + " days ago";
                            }


                            if (presence.equals("0")) {
                                friend_precense_text.setText("last seen " + final_last_disconnected2 + " , joined " + last_final_time_reg);
                                friends_profile_image.setBorderColor(getResources().getColor(R.color.colorOffline));
                            } else if (presence.equals("1")) {
                                friend_precense_text.setText("currently online" + " , joined " + last_final_time_reg);
                                friends_profile_image.setBorderColor(getResources().getColor(R.color.colorOnline));
                            } else if (presence.equals("2")) {
                                friend_precense_text.setText("Away" + " , joined " + last_final_time_reg);
                                friends_profile_image.setBorderColor(getResources().getColor(R.color.colorAway));
                            } else if (presence.equals("3")) {
                                friend_precense_text.setText("Busy" + " , joined " + last_final_time_reg);
                                friends_profile_image.setBorderColor(getResources().getColor(R.color.colorBusy));
                            }

                            user_footprints.setText(changingUnit(Integer.parseInt(footprints)));
                            user_gifts.setText(changingUnit(Integer.parseInt(gifts)));
                            followersTotal.setText(changingUnit(Integer.parseInt(followers)));
                            shoutsTotal.setText(changingUnit(Integer.parseInt(shouts)));
                            friendsTotal.setText(changingUnit(Integer.parseInt(friends)));

                            Log.e("requestngStatus", "reqStatus> " + reqStatus);
                            Log.e("requestngStatus", "add_request> " + add_request);

                            if (reqStatus.equals("0:0") && add_request.equals("1")) {
                                request_status.setBackgroundResource(R.drawable.error_circle_blue_gray);
                                request_status.setImageResource(R.drawable.adding_friend);
                            }
                            //hereee
                            else if (reqStatus.equals("1:0")) {
                                //show tick icon means frnd requst sended
                                request_status.setBackgroundResource(R.drawable.new_circle_light_gray);
                                request_status.setImageResource(R.drawable.adding_request_sent);
                            } else if (reqStatus.equals("0:1")) {
                                //show plus icon means to acpt requst
                                request_status.setBackgroundResource(R.drawable.error_circle_sharpgreen_gray);
                                request_status.setImageResource(R.drawable.accepting_request);
                            } else if (reqStatus.equals("1:1")) {
                                //show chat icon
                                request_status.setBackgroundResource(R.drawable.error_circle_blue_gray);
                                request_status.setImageResource(R.drawable.start_chat);
                            } else {
                                //show block icon
                                request_status.setBackgroundResource(R.drawable.error_circle_sharpgreen_gray);
                                request_status.setImageResource(R.drawable.accepting_request);

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("gettingex", "1" + e.getMessage());
                    }

                }

            });
        }
    };
    Emitter.Listener add_like_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            try {
                String success = jsonObject.getString("success");
                final String total = jsonObject.getString("total");
                if (success.equals("1")) {
                    liker = 1;
                    Snackbar.make(parentLinearLayout, "Liked", Snackbar.LENGTH_LONG).show();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            user_heart_likes.setText(changingUnit(Integer.parseInt(total)));

                        }
                    });

                } else if (success.equals("0")) {
                    Snackbar.make(parentLinearLayout, "Error while processing", Snackbar.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    Emitter.Listener adding_contact_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LayoutInflater layoutInflater = LayoutInflater.from(UserFriendProfile.this);
                    View myview = layoutInflater.inflate(R.layout.connection_error_dialog, null);
                    final AlertDialog.Builder adb = new AlertDialog.Builder(myview.getContext());
                    MyalertDialog = adb.create();
                    MyalertDialog.setView(myview);
                    try {
                        if (jsonObject.getString("success").equals("1")) {
                            reqStatus = "1:0";
                            request_status.setBackgroundResource(R.drawable.new_circle_light_gray);
                            request_status.setImageResource(R.drawable.adding_request_sent);
                            ParentLayout = myview.findViewById(R.id.ParentLayout);
                            ParentLayout.setBackgroundResource(R.drawable.square_new_theme_green);
                            errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                            errorDialogText1.setVisibility(View.GONE);
                            friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                            friend_added_imageview.setVisibility(View.VISIBLE);
                            friend_added_imageview.setImageResource(R.drawable.add_user_green);
                            friend_added_imageview.setColorFilter(ContextCompat.getColor(UserFriendProfile.this, R.color.new_theme_green), PorterDuff.Mode.SRC_IN);
                            errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                            try {
                                errorDialogText2.setText(jsonObject.getString("message"));
                                errorDialogText2.setTextColor(getResources().getColor(R.color.black));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                            errorDialogTryAgainBtn.setText("Ok");
                            errorDialogTryAgainBtn.setBackground(getResources().getDrawable(R.drawable.error_dialog_green_btn));
                        } else {
                            ParentLayout = myview.findViewById(R.id.ParentLayout);
                            ParentLayout.setBackgroundResource(R.drawable.square_full_body_red_popups);
                            errorDialogText1 = myview.findViewById(R.id.errorDialogText1);
                            errorDialogText1.setVisibility(View.GONE);
                            friend_added_imageview = myview.findViewById(R.id.friend_added_imageview);
                            friend_added_imageview.setVisibility(View.VISIBLE);
                            friend_added_imageview.setImageResource(R.drawable.sad_icon);
                            errorDialogText2 = myview.findViewById(R.id.errorDialogText2);
                            try {
                                errorDialogText2.setText(jsonObject.getString("message"));
                                errorDialogText2.setTextColor(getResources().getColor(R.color.black));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            errorDialogTryAgainBtn = myview.findViewById(R.id.errorDialogTryAgainBtn);
                            errorDialogTryAgainBtn.setText("Ok");
                            errorDialogTryAgainBtn.setBackground(getResources().getDrawable(R.drawable.error_dialog_red_btn));
                        }
                        errorDialogTryAgainBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MyalertDialog.dismiss();
                                ContactsFragment contactsFragment = new ContactsFragment();
                                contactsFragment.getContacts();

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!((Activity) UserFriendProfile.this).isFinishing())
                        MyalertDialog.show();
                }
            });

        }
    };


}
