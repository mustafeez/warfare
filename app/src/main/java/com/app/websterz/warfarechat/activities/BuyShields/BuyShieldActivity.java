package com.app.websterz.warfarechat.activities.BuyShields;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.activities.SendAttacks.SendAttackRecyclerViewAdapter;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.mSocket;

public class BuyShieldActivity extends AppCompatActivity {

    RelativeLayout parentRelativeLayout;
    RecyclerView recyclerView;
    BuyShieldModelClass buyShieldModelClass;
    ArrayList<BuyShieldModelClass> ShieldArrayList;
    BuyShieldRecyclerViewAdapter buyShieldRecyclerViewAdapter;
    Context context;
    String shieldName,shieldDetail, shieldCost,shieldSlug,shieldImage,shieldRemaning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_shield);

        context = BuyShieldActivity.this;
        initializations();
        initview();
    }

    private void initializations() {
        MySocketsClass.static_objects.MyStaticActivity = BuyShieldActivity.this;
        ShieldArrayList = new ArrayList<>();
        parentRelativeLayout = findViewById(R.id.parent_layout_shield);
        recyclerView = findViewById(R.id.BuyShieldRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initview() {
        mSocket.emit(MySocketsClass.EmmittersClass.GET_SHIELD_PACKAGES);
        mSocket.on(MySocketsClass.ListenersClass.GET_SHIELD_PACKAGES_RESPONSE, get_shield_packages_response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.global_layout=parentRelativeLayout;
    }

    private Emitter.Listener get_shield_packages_response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        for(int i =0; i < jsonObject.getJSONArray("packages").length(); i++){

                            shieldName  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("name");
                            shieldDetail  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("detail");
                            shieldCost  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("cost");
                            shieldSlug  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("slug");
                            shieldImage  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("img");
                            shieldRemaning  = jsonObject.getJSONArray("packages").getJSONObject(i).getString("remaining");

                            buyShieldModelClass = new BuyShieldModelClass(shieldImage,shieldName,shieldDetail,shieldCost,shieldSlug,shieldRemaning);
                            ShieldArrayList.add(buyShieldModelClass);

                        }
                        setAdapter();

                        jsonObject.getString("success");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    private void setAdapter() {
        buyShieldRecyclerViewAdapter = new BuyShieldRecyclerViewAdapter(ShieldArrayList,  this);
        recyclerView.setAdapter(buyShieldRecyclerViewAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mSocket.off(MySocketsClass.ListenersClass.GET_SHIELD_PACKAGES_RESPONSE);
    }


    public void FinishBuyShieldActivity(Activity activity){
        activity.finish();
    }
}
