package com.app.websterz.warfarechat.DataBaseChatRooms;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {DataBaseChatRoomTables.class},version =1)
public abstract class DataBase_ChatRooms extends RoomDatabase {
    public abstract ChatRoomDBAO chatRoomDBAO();
}
