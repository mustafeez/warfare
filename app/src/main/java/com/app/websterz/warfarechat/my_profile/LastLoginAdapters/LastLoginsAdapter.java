package com.app.websterz.warfarechat.my_profile.LastLoginAdapters;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LastLoginsAdapter extends RecyclerView.Adapter<LastLoginsAdapter.ViewHolder>{

    ArrayList<String> jsonArray;

    public LastLoginsAdapter(ArrayList<String> jsonArray) {
      this.jsonArray=jsonArray;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.last_logins_row_layout, parent, false);
        return new LastLoginsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            String IPAdress=jsonArray.get(position).split("\\|")[1];
            String Agent=jsonArray.get(position).split("\\|")[3];
            String TimeStampDate=jsonArray.get(position).split("\\|")[2];
         //   holder.LastLoginsTexts.setText(jsonArray.getJSONObject(position).getString("message").split("\\|")[3]);
            holder.LastLoginsTexts.setText(LoginStrings(Long.valueOf(TimeStampDate),IPAdress,Agent));


    }

    @Override
    public int getItemCount() {
        return jsonArray.size();
    }

    private String LoginStrings(long last_online_time, String IPAdress,String Agent ){

        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;
        String last_online_final_time, last_final_time_reg = null;
        long last_online_time2 = last_online_time * 1000;
     //   long time_reg2 = time_reg * 1000;

        long now = System.currentTimeMillis();
        final long diff = now - last_online_time2;
        if (diff < MINUTE_MILLIS) {
            last_online_final_time = "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            last_online_final_time = "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            last_online_final_time = diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            last_online_final_time = "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            last_online_final_time = diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            last_online_final_time = "yesterday";
        } else {
            last_online_final_time = diff / DAY_MILLIS + " days ago";
        }
//        final long diff2 = now - time_reg2;
//        if (diff2 < MINUTE_MILLIS) {
//            last_final_time_reg = "just now";
//        } else if (diff2 < 2 * MINUTE_MILLIS) {
//            last_final_time_reg = "a minute ago";
//        } else if (diff2 < 50 * MINUTE_MILLIS) {
//            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
//        } else if (diff2 < 90 * MINUTE_MILLIS) {
//            last_final_time_reg = "an hour ago";
//        } else if (diff2 < 24 * HOUR_MILLIS) {
//            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago";
//        } else if (diff2 < 48 * HOUR_MILLIS) {
//            last_final_time_reg = "yesterday";
//        } else {
//
//            long calculating_months=diff2 / DAY_MILLIS;
//            if (calculating_months<30)
//            {
//                last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
//            }
//            else if (calculating_months>=30&&calculating_months<=360)
//            {
//                last_final_time_reg=calculating_months/30 +" Month(s) ago";
//            }
//            else if (calculating_months>=30&&calculating_months>360)
//            {
//                last_final_time_reg=calculating_months/360 +" Year(s) ago";
//            }
//        }

        return "Logged in "+last_online_final_time+" from "+IPAdress+"("+Agent+") on "+FullTimeDate(String.valueOf(last_online_time));
    }

    private String FullTimeDate(String date_json){
        long time=Long.parseLong(date_json);
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("EEE, d MMM yyyy hh:mm:ss a", cal).toString();
        return date;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView LastLoginsTexts;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            LastLoginsTexts=itemView.findViewById(R.id.LastLoginsTexts);

        }
    }
}
