package com.app.websterz.warfarechat.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.login.Login;
import com.app.websterz.warfarechat.mySingleTon.MySingleTon;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class AgeUpActivity extends AppCompatActivity {

    ImageView AgeUpIV;
    TextView AgeUpServerMessage;
    Button CloseButton;
    MySingleTon mySingleTon;
    RelativeLayout ParentLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_age_up);

        AgeUpIV = findViewById(R.id.UserAgeUp_ImageView);
        ParentLayout = findViewById(R.id.ParentLayout);
        AgeUpServerMessage = findViewById(R.id.AgeUpServerMessage_TextView);
        CloseButton = findViewById(R.id.btn_close_AgeUp_activity);
        mySingleTon = MySingleTon.getInstance();


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AgeUpActivity.this);
        String gender = prefs.getString("gender", null);


        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("AgeUpReward"));
            String age = jsonObject.getString("age");
            int reward = jsonObject.getInt("reward");
            int total_coin = jsonObject.getInt("total");

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String formated_reward_coins = formatter.format(Double.parseDouble(String.valueOf(reward)));

            if(age.equals("1") && gender.equals("M")){
                AgeUpIV.setBackgroundResource(R.drawable.born_male);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins +" points for turning to New Born");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("1")&& gender.equals("F"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.born_female);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to New Born");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("2") && gender.equals("M"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.teen_male);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to Teen");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("2")&& gender.equals("F"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.teen_female);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to Teen");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("3")&& gender.equals("M"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.young_male);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins +" points for turning to Young");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("3") && gender.equals("F"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.young_female);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to Young");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("4") && gender.equals("M"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.adult_male);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to Adult");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("4") && gender.equals("F"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.adult_female);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to Adult");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("5")&& gender.equals("M"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.mature_male);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to Mature");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }
            else if(age.equals("5") && gender.equals("F"))
            {
                AgeUpIV.setBackgroundResource(R.drawable.mature_female);
                AgeUpServerMessage.setText("You have been rewarded " + formated_reward_coins  +" points for turning to Mature");
                mySingleTon.coins = String.valueOf(total_coin);
                SavingUpdatedCoins(String.valueOf(total_coin));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        CloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MySocketsClass.static_objects.global_layout=ParentLayout;
        MySocketsClass.static_objects.MyStaticActivity=AgeUpActivity.this;
    }

    private void SavingUpdatedCoins(String coins){
        SharedPreferences.Editor prefEditor55 = PreferenceManager.getDefaultSharedPreferences(AgeUpActivity.this).edit();
        prefEditor55.putString("coins", coins);
        prefEditor55.apply();
    }
}
