package com.app.websterz.warfarechat.activities.SendAttacks;

public class SendAttackModelClass {

    String AttackImageView, AttackName, AttackDescription, AttackPoints,Slug;

    public SendAttackModelClass(String attackImageView, String attackName, String attackDescription, String attackPoints, String slug) {
        AttackImageView = attackImageView;
        AttackName = attackName;
        AttackDescription = attackDescription;
        AttackPoints = attackPoints;
        Slug = slug;
    }


    public String getSlug() {
        return Slug;
    }

    public String getAttackImageView() {
        return AttackImageView;
    }

    public String getAttackName() {
        return AttackName;
    }

    public String getAttackDescription() {
        return AttackDescription;
    }

    public String getAttackPoints() {
        return AttackPoints;
    }

    public void setSlug(String slug) {
        Slug = slug;
    }

    public void setAttackImageView(String attackImageView) {
        AttackImageView = attackImageView;
    }

    public void setAttackName(String attackName) {
        AttackName = attackName;
    }

    public void setAttackDescription(String attackDescription) {
        AttackDescription = attackDescription;
    }

    public void setAttackPoints(String attackPoints) {
        AttackPoints = attackPoints;
    }
}
