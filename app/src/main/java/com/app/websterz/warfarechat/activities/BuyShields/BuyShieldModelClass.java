package com.app.websterz.warfarechat.activities.BuyShields;

public class BuyShieldModelClass {

    String ShieldImageView, ShieldName, ShieldDetail, ShieldCost,ShieldSlug, ShieldRemaining;

    public BuyShieldModelClass(String shieldImageView, String shieldName, String shieldDetail, String shieldCost, String shieldSlug, String shieldRemaining) {
        ShieldImageView = shieldImageView;
        ShieldName = shieldName;
        ShieldDetail = shieldDetail;
        ShieldCost = shieldCost;
        ShieldSlug = shieldSlug;
        ShieldRemaining = shieldRemaining;
    }

    public String getShieldImageView() {
        return ShieldImageView;
    }

    public void setShieldImageView(String shieldImageView) {
        ShieldImageView = shieldImageView;
    }

    public String getShieldName() {
        return ShieldName;
    }

    public void setShieldName(String shieldName) {
        ShieldName = shieldName;
    }

    public String getShieldDetail() {
        return ShieldDetail;
    }

    public void setShieldDetail(String shieldDetail) {
        ShieldDetail = shieldDetail;
    }

    public String getShieldCost() {
        return ShieldCost;
    }

    public void setShieldCost(String shieldCost) {
        ShieldCost = shieldCost;
    }

    public String getShieldSlug() {
        return ShieldSlug;
    }

    public void setShieldSlug(String shieldSlug) {
        ShieldSlug = shieldSlug;
    }

    public String getShieldRemaining() {
        return ShieldRemaining;
    }

    public void setShieldRemaining(String shieldRemaining) {
        ShieldRemaining = shieldRemaining;
    }
}
