package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses;

public class TrendingShoutsModelClass {
    String trendingShoutId;
    String trendingShoutUsername;
    String trendingShoutDp;
    String trendingShoutDate;
    String trendingShoutLikes;
    String trendingShoutComments;

    public TrendingShoutsModelClass(String trendingShoutId, String trendingShoutUsername,
                                    String trendingShoutDp, String trendingShoutDate,
                                    String trendingShoutLikes, String trendingShoutComments) {
        this.trendingShoutId = trendingShoutId;
        this.trendingShoutUsername = trendingShoutUsername;
        this.trendingShoutDp = trendingShoutDp;
        this.trendingShoutDate = trendingShoutDate;
        this.trendingShoutLikes = trendingShoutLikes;
        this.trendingShoutComments = trendingShoutComments;
    }

    public String getTrendingShoutId() {
        return trendingShoutId;
    }

    public void setTrendingShoutId(String trendingShoutId) {
        this.trendingShoutId = trendingShoutId;
    }

    public String getTrendingShoutUsername() {
        return trendingShoutUsername;
    }

    public void setTrendingShoutUsername(String trendingShoutUsername) {
        this.trendingShoutUsername = trendingShoutUsername;
    }

    public String getTrendingShoutDp() {
        return trendingShoutDp;
    }

    public void setTrendingShoutDp(String trendingShoutDp) {
        this.trendingShoutDp = trendingShoutDp;
    }

    public String getTrendingShoutDate() {
        return trendingShoutDate;
    }

    public void setTrendingShoutDate(String trendingShoutDate) {
        this.trendingShoutDate = trendingShoutDate;
    }

    public String getTrendingShoutLikes() {
        return trendingShoutLikes;
    }

    public void setTrendingShoutLikes(String trendingShoutLikes) {
        this.trendingShoutLikes = trendingShoutLikes;
    }

    public String getTrendingShoutComments() {
        return trendingShoutComments;
    }

    public void setTrendingShoutComments(String trendingShoutComments) {
        this.trendingShoutComments = trendingShoutComments;
    }
}
