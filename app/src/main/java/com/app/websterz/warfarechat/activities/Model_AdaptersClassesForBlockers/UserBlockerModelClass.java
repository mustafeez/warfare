package com.app.websterz.warfarechat.activities.Model_AdaptersClassesForBlockers;

import android.os.Parcel;
import android.os.Parcelable;

public class UserBlockerModelClass implements Parcelable {
    private String UserDp;
    private String UserName;

    public UserBlockerModelClass(String userDp, String userName) {
        UserDp = userDp;
        UserName = userName;
    }

    protected UserBlockerModelClass(Parcel in) {
        UserDp = in.readString();
        UserName = in.readString();
    }

    public static final Creator<UserBlockerModelClass> CREATOR = new Creator<UserBlockerModelClass>() {
        @Override
        public UserBlockerModelClass createFromParcel(Parcel in) {
            return new UserBlockerModelClass(in);
        }

        @Override
        public UserBlockerModelClass[] newArray(int size) {
            return new UserBlockerModelClass[size];
        }
    };

    public String getUserDp() {
        return UserDp;
    }

    public void setUserDp(String userDp) {
        UserDp = userDp;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UserDp);
        dest.writeString(UserName);
    }
}
