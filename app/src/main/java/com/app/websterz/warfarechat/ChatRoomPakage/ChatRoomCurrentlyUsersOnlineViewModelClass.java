package com.app.websterz.warfarechat.ChatRoomPakage;

public class ChatRoomCurrentlyUsersOnlineViewModelClass {
    private String ChatRoom_userlist_username,ChatRoom_userlist_username_role,ChatRoom_userlist_username_dp,ChatRoom_userlist_username_age,ChatRoom_userlist_username_gender;

    public ChatRoomCurrentlyUsersOnlineViewModelClass(String ChatRoom_userlist_username,
                                                      String ChatRoom_userlist_username_role,
                                                      String ChatRoom_userlist_username_dp,
                                                      String ChatRoom_userlist_username_age,
                                                      String ChatRoom_userlist_username_gender) {

        this.ChatRoom_userlist_username=ChatRoom_userlist_username;
        this.ChatRoom_userlist_username_role=ChatRoom_userlist_username_role;
        this.ChatRoom_userlist_username_dp=ChatRoom_userlist_username_dp;
        this.ChatRoom_userlist_username_age=ChatRoom_userlist_username_age;
        this.ChatRoom_userlist_username_gender=ChatRoom_userlist_username_gender;
    }

    public String getChatRoom_userlist_username() {
        return ChatRoom_userlist_username;
    }

    public String getChatRoom_userlist_username_role() {
        return ChatRoom_userlist_username_role;
    }

    public String getChatRoom_userlist_username_dp() {
        return ChatRoom_userlist_username_dp;
    }

    public String getChatRoom_userlist_username_age() {
        return ChatRoom_userlist_username_age;
    }

    public String getChatRoom_userlist_username_gender() {
        return ChatRoom_userlist_username_gender;
    }

    public void setChatRoom_userlist_username(String chatRoom_userlist_username) {
        ChatRoom_userlist_username = chatRoom_userlist_username;
    }

    public void setChatRoom_userlist_username_role(String chatRoom_userlist_username_role) {
        ChatRoom_userlist_username_role = chatRoom_userlist_username_role;
    }

    public void setChatRoom_userlist_username_dp(String chatRoom_userlist_username_dp) {
        ChatRoom_userlist_username_dp = chatRoom_userlist_username_dp;
    }

    public void setChatRoom_userlist_username_age(String chatRoom_userlist_username_age) {
        ChatRoom_userlist_username_age = chatRoom_userlist_username_age;
    }

    public void setChatRoom_userlist_username_gender(String chatRoom_userlist_username_gender) {
        ChatRoom_userlist_username_gender = chatRoom_userlist_username_gender;
    }
}
