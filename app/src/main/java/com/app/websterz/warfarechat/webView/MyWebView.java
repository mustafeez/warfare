package com.app.websterz.warfarechat.webView;

import android.annotation.TargetApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.mySingleTon.MySocketsClass;
import com.asksira.webviewsuite.WebViewSuite;

public class MyWebView extends AppCompatActivity {
    //WebView myWebView;
    WebViewSuite webViewSuite;
    LinearLayout webViewBackBtn;
    RelativeLayout parentLinearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_web_view);
        //myWebView = findViewById(R.id.myWebView);
        webViewSuite = findViewById(R.id.webView_webViewSuite);
        webViewBackBtn=findViewById(R.id.webViewBackBtn);
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        MySocketsClass.static_objects.global_layout=parentLinearLayout;
        MySocketsClass.static_objects.MyStaticActivity = MyWebView.this;
        webViewBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Intent intent=getIntent();
        int webview=intent.getIntExtra("webview" , 0);

        if(webview == 0){
            //myWebView.loadUrl("http://warfarechat.com:8080/api/tos.php");
            webViewSuite.startLoading("http://warfarechat.com:8080/api/tos.php");
            webViewSuite.setVisibility(View.VISIBLE);
        }
        else if(webview == 1){
            //myWebView.loadUrl("http://warfarechat.com:8080/api/privacy.php");
            webViewSuite.startLoading("http://warfarechat.com:8080/api/privacy.php");
            webViewSuite.setVisibility(View.VISIBLE);
        }
        else if (webview == 2){
            //myWebView.loadUrl("http://warfarechat.com:8080/api/story.php");
            webViewSuite.startLoading("http://warfarechat.com:8080/api/story.php");
            webViewSuite.setVisibility(View.VISIBLE);
        }
        else if (webview == 3){
            //myWebView.loadUrl("http://warfarechat.com:8080/api/support.php");
            webViewSuite.startLoading("http://warfarechat.com:8080/api/support.php");
            webViewSuite.setVisibility(View.VISIBLE);
        }
    }
}
