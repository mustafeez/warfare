package com.app.websterz.warfarechat.ChatRoomPakage;

public class ChatRoomChatModelClass {
    private String ChatMessage,Sender,Date,MessageType,ChattingUserDp,ChatroomName,Role,Age,Gender;

    public ChatRoomChatModelClass(String chatMessage,String sender,String date,String message_type,String chattingUserDp,String chatroom_name,String role,String age,String gender) {
        ChatMessage = chatMessage;
        Sender=sender;
        Date=date;
        MessageType=message_type;
        ChattingUserDp=chattingUserDp;
        ChatroomName=chatroom_name;
        Role=role;
        Gender=gender;
        Age=age;
    }


    public String getChatMessage() {
        return ChatMessage;
    }

    public String getSender() {
        return Sender;
    }

    public String getDate() {
        return Date;
    }

    public String getMessageType() {
        return MessageType;
    }

    public String getChattingUserDp() {
        return ChattingUserDp;
    }

    public String getChatroomName() {
        return ChatroomName;
    }

    public String getRole() {
        return Role;
    }

    public String getAge() {
        return Age;
    }

    public String getGender() {
        return Gender;
    }

    public void setChatMessage(String chatMessage) {
        ChatMessage = chatMessage;
    }

    public void setSender(String sender) {
        Sender = sender;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setMessageType(String messageType) {
        MessageType = messageType;
    }

    public void setChattingUserDp(String chattingUserDp) {
        ChattingUserDp = chattingUserDp;
    }

    public void setChatroomName(String chatroomName) {

        ChatroomName = chatroomName;
    }

    public void setRole(String role) {
        Role = role;
    }

    public void setAge(String age) {
        Age = age;
    }

    public void setGender(String gender) {
        Gender = gender;
    }
}
