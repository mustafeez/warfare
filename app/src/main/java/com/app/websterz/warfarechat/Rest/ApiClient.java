package com.app.websterz.warfarechat.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;

public class ApiClient {

   // private static final String BASE_URL="https://5.39.63.226/";
    private static final String BASE_URL="http://socio.chat/";
   // private static final String BASE_URL="http://warfarechat.com:8080/";
    private static Retrofit retrofit;
    public static Retrofit getApiClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
