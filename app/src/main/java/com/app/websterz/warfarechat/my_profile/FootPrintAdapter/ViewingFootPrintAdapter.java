package com.app.websterz.warfarechat.my_profile.FootPrintAdapter;

import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.app.websterz.warfarechat.homeScreen.adapter.CoinDetailsAdapter;
import com.app.websterz.warfarechat.my_profile.My_profile;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;


public class ViewingFootPrintAdapter extends RecyclerView.Adapter<ViewingFootPrintAdapter.ViewHolder>{
    JSONObject jsonObject;
    public ViewingFootPrintAdapter(JSONObject jsonObject) {
        try {
            this.jsonObject=jsonObject;
            JSONArray jsonArray=jsonObject.getJSONArray("footprints");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewing_footprint_row_layout, parent, false);
        return new ViewingFootPrintAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        try {
            holder.MyAvatarLoader.setVisibility(View.VISIBLE);
            holder.MyAvatarLoader.setAnimating(true);
            String username=jsonObject.getJSONArray("footprints").getJSONObject(position).getString("username");
            String dp=jsonObject.getJSONArray("footprints").getJSONObject(position).getString("dp");
            String message=jsonObject.getJSONArray("footprints").getJSONObject(position).getString("message");
            String date=jsonObject.getJSONArray("footprints").getJSONObject(position).getString("date");

//            Picasso.get().load(baseUrl+dp).into(holder.FootPrintUserImage, new Callback() {
//                @Override
//                public void onSuccess() {
//                    holder.MyAvatarLoader.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onError(Exception e) {
//
//                }
//            });


            GlideApp.with(holder.itemView.getContext()).load(baseUrl + dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    holder.MyAvatarLoader.setVisibility(View.GONE);
                    return false;
                }
            }).
                    into(holder.FootPrintUserImage);


            holder.FootPrintDetailText.setText(message);
            holder.FootPrintTime.setText(ActualTimeAge(Long.valueOf(date)));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String ActualTimeAge(long time_reg){

        long now = System.currentTimeMillis();
        long time_reg2 = time_reg * 1000;

        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;
        String last_final_time_reg=null;


        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "just now";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "a minute ago";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "an hour ago";
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago";
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "1 day ago";
        } else {

            long calculating_months=diff2 / DAY_MILLIS;
            if (calculating_months<30)
            {
                last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
            }
            else if (calculating_months>=30&&calculating_months<=360)
            {
                last_final_time_reg=calculating_months/30 +" Month(s) ago";
            }
            else if (calculating_months>=30&&calculating_months>360)
            {
                last_final_time_reg=calculating_months/360 +" Year(s) ago";
            }
        }

        return last_final_time_reg;
    }

    private String ActualDateAfterConverted(String date){
        long time=Long.parseLong(date);
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format("EEE d, MMM yyyy hh:mm:ss a", cal).toString();
    }
    @Override
    public int getItemCount() {
        int Total=0;
        try {
            Total=jsonObject.getInt("total");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Total;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView FootPrintUserImage;
        TextView FootPrintDetailText,FootPrintTime;
        AvatarView MyAvatarLoader;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            FootPrintUserImage=itemView.findViewById(R.id.FootPrintUserImage);
            FootPrintDetailText=itemView.findViewById(R.id.FootPrintDetailText);
            FootPrintTime=itemView.findViewById(R.id.FootPrintTime);
            MyAvatarLoader=itemView.findViewById(R.id.MyAvatarLoader);

        }
    }
}
