package com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.HomeScreen;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.Activities.ExplainingShoutActivity;
import com.app.websterz.warfarechat.homeScreen.fragments.HomeFragment.ModelClasses.TrendingShoutsModelClass;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;

public class TrendingShoutsAdapter extends RecyclerView.Adapter<TrendingShoutsAdapter.ViewHolder> {
    ArrayList <TrendingShoutsModelClass> trendingShoutsArraylist=new ArrayList<>();
    Context myContext;

    public TrendingShoutsAdapter(ArrayList <TrendingShoutsModelClass> trendingShoutsArraylist, Context myContext) {
        super();
        this.trendingShoutsArraylist=trendingShoutsArraylist;
        this.myContext=myContext;

    }

    @Override
    public TrendingShoutsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.suggested_shouts_layout, viewGroup, false);
        return new TrendingShoutsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TrendingShoutsAdapter.ViewHolder viewHolder, final int i) {
        Glide.with(myContext).load(baseUrl+trendingShoutsArraylist.get(i).getTrendingShoutDp()).into(viewHolder.trendingShoutsUserImagesDp);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.ProfileavatarLoader.getVisibility()==View.INVISIBLE){
                    viewHolder.ProfileavatarLoader.setVisibility(View.VISIBLE);
                    viewHolder.ProfileavatarLoader.setAnimating(true);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stoppingMediaPlayerOfAudioShouts();
                        Intent intent=new Intent(viewHolder.itemView.getContext(), ExplainingShoutActivity.class);
                        intent.putExtra("ShoutedText",trendingShoutsArraylist.get(i).getTrendingShoutId());
                        viewHolder.itemView.getContext().startActivity(intent);
                        if (viewHolder.ProfileavatarLoader.getVisibility()==View.VISIBLE){
                            viewHolder.ProfileavatarLoader.setVisibility(View.INVISIBLE);

                        }
                    }
                }, 500);

            }
        });
    }

    private void stoppingMediaPlayerOfAudioShouts() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(myContext);
        Intent intent = new Intent("StopShoutsAudioMediaPlayer");
        localBroadcastManager.sendBroadcast(intent);

    }
        @Override
    public int getItemCount() {
        return trendingShoutsArraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CircleImageView trendingShoutsUserImagesDp;
        AvatarView ProfileavatarLoader;

        public ViewHolder( View itemView) {
            super(itemView);
            trendingShoutsUserImagesDp=itemView.findViewById(R.id.trendingShoutsUserImagesDp);
            ProfileavatarLoader=itemView.findViewById(R.id.ProfileavatarLoader);
        }
    }
}

