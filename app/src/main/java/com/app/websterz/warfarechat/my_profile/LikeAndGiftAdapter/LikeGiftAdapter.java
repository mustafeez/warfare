package com.app.websterz.warfarechat.my_profile.LikeAndGiftAdapter;

import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.websterz.warfarechat.R;
import com.app.websterz.warfarechat.homeScreen.activities.TrstedServerPack2.GlideApp;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import xyz.schwaab.avvylib.AvatarView;

import static com.app.websterz.warfarechat.landingScreen.MainActivity.baseUrl;


public class LikeGiftAdapter extends RecyclerView.Adapter<LikeGiftAdapter.ViewHolder> {
    JSONObject jsonObject;
    JSONArray jsonArray;
    String IntentType;
    String ImageUrlPath = "images/gifts/";

    public LikeGiftAdapter(JSONObject jsonObject, String IntentType) {
        this.jsonObject = jsonObject;
        this.IntentType = IntentType;
        try {
            if (IntentType.equalsIgnoreCase("Likes")) {
                jsonArray = jsonObject.getJSONArray("likes");
            } else if (IntentType.equalsIgnoreCase("Gifts")) {
                jsonArray = jsonObject.getJSONArray("gifts");
            } else if (IntentType.equalsIgnoreCase("Followers")) {
                jsonArray = jsonObject.getJSONArray("followers");
            } else if (IntentType.equalsIgnoreCase("Friends")) {
                jsonArray = jsonObject.getJSONArray("friends");
                Log.e("friendsJson","Friends selectn");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public LikeGiftAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewing_likes_gifts_row_layout, parent, false);
        return new LikeGiftAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LikeGiftAdapter.ViewHolder holder, final int position) {

        holder.MyAvatarLoader.setVisibility(View.VISIBLE);
        holder.MyAvatarLoader.setAnimating(true);
        if (IntentType.equalsIgnoreCase("Likes")) {
            try {
                int Total = jsonObject.getInt("total");

                String username = jsonObject.getJSONArray("likes").getJSONObject(position).getString("username");
                String dp = jsonObject.getJSONArray("likes").getJSONObject(position).getString("dp");
                String message = jsonObject.getJSONArray("likes").getJSONObject(position).getString("message");
                String date = jsonObject.getJSONArray("likes").getJSONObject(position).getString("date");

                GlideApp.with(holder.itemView.getContext()).load(baseUrl + dp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.MyAvatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).
                        into(holder.MainDp);

                holder.UserNameTextview.setText(username);
                holder.TimesTextview.setText(ActualTimeAge(Long.valueOf(date)));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (IntentType.equalsIgnoreCase("Gifts"))  {
            try {
                int Total = jsonObject.getInt("total");
                String sender = jsonObject.getJSONArray("gifts").getJSONObject(position).getString("sender");
                String img = jsonObject.getJSONArray("gifts").getJSONObject(position).getString("img");
                String name = jsonObject.getJSONArray("gifts").getJSONObject(position).getString("name");
                String date = jsonObject.getJSONArray("gifts").getJSONObject(position).getString("date");

                GlideApp.with(holder.itemView.getContext()).load(baseUrl + ImageUrlPath + img).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.MyAvatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).
                        into(holder.MainDp);

                holder.UserNameTextview.setText(sender);
                if (!date.equals("undefined"))
                    holder.TimesTextview.setText(ExactTime(date));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if (IntentType.equalsIgnoreCase("Followers"))  {
            Log.e("folowersJson","In followers Method");
            try {
                int Total = jsonObject.getInt("total");
                String followerUserName = jsonObject.getJSONArray("followers").getJSONObject(position).getString("username");
                String followerdp = jsonObject.getJSONArray("followers").getJSONObject(position).getString("dp");
                String message = jsonObject.getJSONArray("followers").getJSONObject(position).getString("message");
                String date = jsonObject.getJSONArray("followers").getJSONObject(position).getString("date");

                GlideApp.with(holder.itemView.getContext()).load(baseUrl + followerdp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.MyAvatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).
                        into(holder.MainDp);

                holder.UserNameTextview.setText(followerUserName);
                if (!date.equals("undefined"))
                    holder.TimesTextview.setText(ExactTime(date));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if (IntentType.equalsIgnoreCase("Friends"))  {
            Log.e("friendsJson","In followers Method");
            try {
                int Total = jsonObject.getInt("total");
                String friendsUserName = jsonObject.getJSONArray("friends").getJSONObject(position).getString("username");
                String friendsdp = jsonObject.getJSONArray("friends").getJSONObject(position).getString("dp");
                String message = jsonObject.getJSONArray("friends").getJSONObject(position).getString("message");

                GlideApp.with(holder.itemView.getContext()).load(baseUrl + friendsdp).apply(RequestOptions.circleCropTransform()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.MyAvatarLoader.setVisibility(View.GONE);
                        return false;
                    }
                }).
                        into(holder.MainDp);

                holder.UserNameTextview.setText(friendsUserName);
                holder.TimesTextview.setText(message);
//                if (!date.equals("undefined"))
//                    holder.TimesTextview.setText(ExactTime(date));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String ExactTime(String date) {
        long time = Long.parseLong(date);
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format("EEE d, MMM yyyy hh:mm:ss a", cal).toString();
    }

    private String ActualTimeAge(long time_reg) {

        long now = System.currentTimeMillis();
        long time_reg2 = time_reg * 1000;

        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;
        String last_final_time_reg = null;


        final long diff2 = now - time_reg2;
        if (diff2 < MINUTE_MILLIS) {
            last_final_time_reg = "just now";
        } else if (diff2 < 2 * MINUTE_MILLIS) {
            last_final_time_reg = "a minute ago";
        } else if (diff2 < 50 * MINUTE_MILLIS) {
            last_final_time_reg = diff2 / MINUTE_MILLIS + " minutes ago";
        } else if (diff2 < 90 * MINUTE_MILLIS) {
            last_final_time_reg = "an hour ago";
        } else if (diff2 < 24 * HOUR_MILLIS) {
            last_final_time_reg = diff2 / HOUR_MILLIS + " hours ago";
        } else if (diff2 < 48 * HOUR_MILLIS) {
            last_final_time_reg = "1 day ago";
        } else {

            long calculating_months = diff2 / DAY_MILLIS;
            if (calculating_months < 30) {
                last_final_time_reg = diff2 / DAY_MILLIS + " days ago";
            } else if (calculating_months >= 30 && calculating_months <= 360) {
                last_final_time_reg = calculating_months / 30 + " Month(s) ago";
            } else if (calculating_months >= 30 && calculating_months > 360) {
                last_final_time_reg = calculating_months / 360 + " Year(s) ago";
            }
        }

        return last_final_time_reg;
    }

    @Override
    public int getItemCount() {
        int Total = 0;
        try {
            Total = jsonObject.getInt("total");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Total;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView MainDp;
        TextView UserNameTextview, TimesTextview;
        AvatarView MyAvatarLoader;

        public ViewHolder(View itemView) {
            super(itemView);
            MainDp = itemView.findViewById(R.id.MainDp);
            UserNameTextview = itemView.findViewById(R.id.UserNameTextview);
            MainDp = itemView.findViewById(R.id.MainDp);
            TimesTextview = itemView.findViewById(R.id.TimesTextview);
            MyAvatarLoader = itemView.findViewById(R.id.MyAvatarLoader);
        }
    }

}
