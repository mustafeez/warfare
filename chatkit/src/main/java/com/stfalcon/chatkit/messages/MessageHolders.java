package com.stfalcon.chatkit.messages;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayout;
import com.stfalcon.chatkit.R;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.ViewHolder;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;
import com.stfalcon.chatkit.utils.DateFormatter;
import com.stfalcon.chatkit.utils.RoundedImageView;
import com.stfalcon.chatkit.utils.Utils;
import com.stfalcon.chatkit.views.FullScreenImageView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import xyz.schwaab.avvylib.AvatarView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.Context.CLIPBOARD_SERVICE;
import static com.stfalcon.chatkit.messages.MessageHolders.IncomingTextMessageViewHolder.incomingVoicePlayer;
import static com.stfalcon.chatkit.messages.MessageHolders.OutcomingTextMessageViewHolder.outcomingVoicePlayer;
import static com.stfalcon.chatkit.messages.MessageHolders.OutcomingTextMessageViewHolder.resumeVoicePlayerOutcoming;

public class MessageHolders {
    private static final short VIEW_TYPE_DATE_HEADER = 130;
    private static final short VIEW_TYPE_TEXT_MESSAGE = 131;
    private static final short VIEW_TYPE_IMAGE_MESSAGE = 132;

    private Class<? extends ViewHolder<Date>> dateHeaderHolder;
    private int dateHeaderLayout;

    private HolderConfig<IMessage> incomingTextConfig;
    private HolderConfig<IMessage> outcomingTextConfig;
    private HolderConfig<MessageContentType.Image> incomingImageConfig;
    private HolderConfig<MessageContentType.Image> outcomingImageConfig;

    private List<ContentTypeConfig> customContentTypes = new ArrayList<>();
    private ContentChecker contentChecker;
    public static String baseUrlChatKit;

    public MessageHolders() {
        this.dateHeaderHolder = DefaultDateHeaderViewHolder.class;
        this.dateHeaderLayout = R.layout.item_date_header;

        this.incomingTextConfig = new HolderConfig<>(DefaultIncomingTextMessageViewHolder.class, R.layout.item_incoming_text_message);
        this.outcomingTextConfig = new HolderConfig<>(DefaultOutcomingTextMessageViewHolder.class, R.layout.item_outcoming_text_message);
        this.incomingImageConfig = new HolderConfig<>(DefaultIncomingImageMessageViewHolder.class, R.layout.item_incoming_image_message);
        this.outcomingImageConfig = new HolderConfig<>(DefaultOutcomingImageMessageViewHolder.class, R.layout.item_outcoming_image_message);
    }

    /**
     * Sets both of custom view holder class and layout resource for incoming text message.
     *
     * @param holder holder class.
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout) {
        this.incomingTextConfig.holder = holder;
        this.incomingTextConfig.layout = layout;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for incoming text message.
     *
     * @param holder  holder class.
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.incomingTextConfig.holder = holder;
        this.incomingTextConfig.layout = layout;
        this.incomingTextConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom view holder class for incoming text message.
     *
     * @param holder holder class.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder) {
        this.incomingTextConfig.holder = holder;
        return this;
    }

    /**
     * Sets custom view holder class for incoming text message.
     *
     * @param holder  holder class.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            Object payload) {
        this.incomingTextConfig.holder = holder;
        this.incomingTextConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom layout resource for incoming text message.
     *
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingTextLayout(@LayoutRes int layout) {
        this.incomingTextConfig.layout = layout;
        return this;
    }

    /**
     * Sets custom layout resource for incoming text message.
     *
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingTextLayout(@LayoutRes int layout, Object payload) {
        this.incomingTextConfig.layout = layout;
        this.incomingTextConfig.payload = payload;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for outcoming text message.
     *
     * @param holder holder class.
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout) {
        this.outcomingTextConfig.holder = holder;
        this.outcomingTextConfig.layout = layout;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for outcoming text message.
     *
     * @param holder  holder class.
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingTextConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.outcomingTextConfig.holder = holder;
        this.outcomingTextConfig.layout = layout;
        this.outcomingTextConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom view holder class for outcoming text message.
     *
     * @param holder holder class.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder) {
        this.outcomingTextConfig.holder = holder;
        return this;
    }

    /**
     * Sets custom view holder class for outcoming text message.
     *
     * @param holder  holder class.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingTextHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
            Object payload) {
        this.outcomingTextConfig.holder = holder;
        this.outcomingTextConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom layout resource for outcoming text message.
     *
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingTextLayout(@LayoutRes int layout) {
        this.outcomingTextConfig.layout = layout;
        return this;
    }

    /**
     * Sets custom layout resource for outcoming text message.
     *
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingTextLayout(@LayoutRes int layout, Object payload) {
        this.outcomingTextConfig.layout = layout;
        this.outcomingTextConfig.payload = payload;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for incoming image message.
     *
     * @param holder holder class.
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout) {
        this.incomingImageConfig.holder = holder;
        this.incomingImageConfig.layout = layout;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for incoming image message.
     *
     * @param holder  holder class.
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.incomingImageConfig.holder = holder;
        this.incomingImageConfig.layout = layout;
        this.incomingImageConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom view holder class for incoming image message.
     *
     * @param holder holder class.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder) {
        this.incomingImageConfig.holder = holder;
        return this;
    }

    /**
     * Sets custom view holder class for incoming image message.
     *
     * @param holder  holder class.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            Object payload) {
        this.incomingImageConfig.holder = holder;
        this.incomingImageConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom layout resource for incoming image message.
     *
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingImageLayout(@LayoutRes int layout) {
        this.incomingImageConfig.layout = layout;
        return this;
    }

    /**
     * Sets custom layout resource for incoming image message.
     *
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setIncomingImageLayout(@LayoutRes int layout, Object payload) {
        this.incomingImageConfig.layout = layout;
        this.incomingImageConfig.payload = payload;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for outcoming image message.
     *
     * @param holder holder class.
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout) {
        this.outcomingImageConfig.holder = holder;
        this.outcomingImageConfig.layout = layout;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for outcoming image message.
     *
     * @param holder  holder class.
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingImageConfig(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            @LayoutRes int layout,
            Object payload) {
        this.outcomingImageConfig.holder = holder;
        this.outcomingImageConfig.layout = layout;
        this.outcomingImageConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom view holder class for outcoming image message.
     *
     * @param holder holder class.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder) {
        this.outcomingImageConfig.holder = holder;
        return this;
    }

    /**
     * Sets custom view holder class for outcoming image message.
     *
     * @param holder  holder class.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingImageHolder(
            @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
            Object payload) {
        this.outcomingImageConfig.holder = holder;
        this.outcomingImageConfig.payload = payload;
        return this;
    }

    /**
     * Sets custom layout resource for outcoming image message.
     *
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingImageLayout(@LayoutRes int layout) {
        this.outcomingImageConfig.layout = layout;
        return this;
    }

    /**
     * Sets custom layout resource for outcoming image message.
     *
     * @param layout  layout resource.
     * @param payload custom data.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setOutcomingImageLayout(@LayoutRes int layout, Object payload) {
        this.outcomingImageConfig.layout = layout;
        this.outcomingImageConfig.payload = payload;
        return this;
    }

    /**
     * Sets both of custom view holder class and layout resource for date header.
     *
     * @param holder holder class.
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setDateHeaderConfig(
            @NonNull Class<? extends ViewHolder<Date>> holder,
            @LayoutRes int layout) {
        this.dateHeaderHolder = holder;
        this.dateHeaderLayout = layout;
        return this;
    }

    /**
     * Sets custom view holder class for date header.
     *
     * @param holder holder class.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setDateHeaderHolder(@NonNull Class<? extends ViewHolder<Date>> holder) {
        this.dateHeaderHolder = holder;
        return this;
    }

    /**
     * Sets custom layout reource for date header.
     *
     * @param layout layout resource.
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public MessageHolders setDateHeaderLayout(@LayoutRes int layout) {
        this.dateHeaderLayout = layout;
        return this;
    }

    /**
     * Registers custom content type (e.g. multimedia, events etc.)
     *
     * @param type            unique id for content type
     * @param holder          holder class for incoming and outcoming messages
     * @param incomingLayout  layout resource for incoming message
     * @param outcomingLayout layout resource for outcoming message
     * @param contentChecker  {@link ContentChecker} for registered type
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public <TYPE extends MessageContentType>
    MessageHolders registerContentType(
            byte type, @NonNull Class<? extends BaseMessageViewHolder<TYPE>> holder,
            @LayoutRes int incomingLayout,
            @LayoutRes int outcomingLayout,
            @NonNull ContentChecker contentChecker) {

        return registerContentType(type,
                holder, incomingLayout,
                holder, outcomingLayout,
                contentChecker);
    }

    /**
     * Registers custom content type (e.g. multimedia, events etc.)
     *
     * @param type            unique id for content type
     * @param incomingHolder  holder class for incoming message
     * @param outcomingHolder holder class for outcoming message
     * @param incomingLayout  layout resource for incoming message
     * @param outcomingLayout layout resource for outcoming message
     * @param contentChecker  {@link ContentChecker} for registered type
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public <TYPE extends MessageContentType>
    MessageHolders registerContentType(
            byte type,
            @NonNull Class<? extends BaseMessageViewHolder<TYPE>> incomingHolder, @LayoutRes int incomingLayout,
            @NonNull Class<? extends BaseMessageViewHolder<TYPE>> outcomingHolder, @LayoutRes int outcomingLayout,
            @NonNull ContentChecker contentChecker) {

        if (type == 0)
            throw new IllegalArgumentException("content type must be greater or less than '0'!");

        customContentTypes.add(
                new ContentTypeConfig<>(type,
                        new HolderConfig<>(incomingHolder, incomingLayout),
                        new HolderConfig<>(outcomingHolder, outcomingLayout)));
        this.contentChecker = contentChecker;
        return this;
    }

    /**
     * Registers custom content type (e.g. multimedia, events etc.)
     *
     * @param type             unique id for content type
     * @param incomingHolder   holder class for incoming message
     * @param outcomingHolder  holder class for outcoming message
     * @param incomingPayload  payload for incoming message
     * @param outcomingPayload payload for outcoming message
     * @param incomingLayout   layout resource for incoming message
     * @param outcomingLayout  layout resource for outcoming message
     * @param contentChecker   {@link MessageHolders.ContentChecker} for registered type
     * @return {@link MessageHolders} for subsequent configuration.
     */
    public <TYPE extends MessageContentType>
    MessageHolders registerContentType(
            byte type,
            @NonNull Class<? extends MessageHolders.BaseMessageViewHolder<TYPE>> incomingHolder, Object incomingPayload, @LayoutRes int incomingLayout,
            @NonNull Class<? extends MessageHolders.BaseMessageViewHolder<TYPE>> outcomingHolder, Object outcomingPayload, @LayoutRes int outcomingLayout,
            @NonNull MessageHolders.ContentChecker contentChecker) {

        if (type == 0)
            throw new IllegalArgumentException("content type must be greater or less than '0'!");

        customContentTypes.add(
                new MessageHolders.ContentTypeConfig<>(type,
                        new MessageHolders.HolderConfig<>(incomingHolder, incomingLayout, incomingPayload),
                        new MessageHolders.HolderConfig<>(outcomingHolder, outcomingLayout, outcomingPayload)));
        this.contentChecker = contentChecker;
        return this;
    }

    /*
     * INTERFACES
     * */

    /**
     * The interface, which contains logic for checking the availability of content.
     */
    public interface ContentChecker<MESSAGE extends IMessage> {

        /**
         * Checks the availability of content.
         *
         * @param message current message in list.
         * @param type    content type, for which content availability is determined.
         * @return weather the message has content for the current message.
         */
        boolean hasContentFor(MESSAGE message, byte type);
    }

    /*
     * PRIVATE METHODS
     * */

    protected ViewHolder getHolder(ViewGroup parent, int viewType, MessagesListStyle messagesListStyle) {
        switch (viewType) {
            case VIEW_TYPE_DATE_HEADER:
                return getHolder(parent, dateHeaderLayout, dateHeaderHolder, messagesListStyle, null);
            case VIEW_TYPE_TEXT_MESSAGE:
                return getHolder(parent, incomingTextConfig, messagesListStyle);
            case -VIEW_TYPE_TEXT_MESSAGE:
                return getHolder(parent, outcomingTextConfig, messagesListStyle);
            case VIEW_TYPE_IMAGE_MESSAGE:
                return getHolder(parent, incomingImageConfig, messagesListStyle);
            case -VIEW_TYPE_IMAGE_MESSAGE:
                return getHolder(parent, outcomingImageConfig, messagesListStyle);
            default:
                for (ContentTypeConfig typeConfig : customContentTypes) {
                    if (Math.abs(typeConfig.type) == Math.abs(viewType)) {
                        if (viewType > 0)
                            return getHolder(parent, typeConfig.incomingConfig, messagesListStyle);
                        else
                            return getHolder(parent, typeConfig.outcomingConfig, messagesListStyle);
                    }
                }
        }
        throw new IllegalStateException("Wrong message view type. Please, report this issue on GitHub with full stacktrace in description.");
    }

    @SuppressWarnings("unchecked")
    protected void bind(final ViewHolder holder, final Object item, boolean isSelected,
                        final ImageLoader imageLoader,
                        final View.OnClickListener onMessageClickListener,
                        final View.OnLongClickListener onMessageLongClickListener,
                        final DateFormatter.Formatter dateHeadersFormatter,
                        final SparseArray<MessagesListAdapter.OnMessageViewClickListener> clickListenersArray) {

        if (item instanceof IMessage) {
            ((MessageHolders.BaseMessageViewHolder) holder).isSelected = isSelected;
            ((MessageHolders.BaseMessageViewHolder) holder).imageLoader = imageLoader;
            holder.itemView.setOnLongClickListener(onMessageLongClickListener);
            holder.itemView.setOnClickListener(onMessageClickListener);

            for (int i = 0; i < clickListenersArray.size(); i++) {
                final int key = clickListenersArray.keyAt(i);
                final View view = holder.itemView.findViewById(key);
                if (view != null) {
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            clickListenersArray.get(key).onMessageViewClick(view, (IMessage) item);
                        }
                    });
                }
            }
        } else if (item instanceof Date) {
            ((MessageHolders.DefaultDateHeaderViewHolder) holder).dateHeadersFormatter = dateHeadersFormatter;
        }

        holder.onBind(item);
    }


    protected int getViewType(Object item, String senderId) {
        boolean isOutcoming = false;
        int viewType;

        if (item instanceof IMessage) {
            IMessage message = (IMessage) item;
            isOutcoming = message.getUser().getId().contentEquals(senderId);
            viewType = getContentViewType(message);

        } else viewType = VIEW_TYPE_DATE_HEADER;

        return isOutcoming ? viewType * -1 : viewType;
    }

    private ViewHolder getHolder(ViewGroup parent, HolderConfig holderConfig,
                                 MessagesListStyle style) {
        return getHolder(parent, holderConfig.layout, holderConfig.holder, style, holderConfig.payload);
    }

    private <HOLDER extends ViewHolder>
    ViewHolder getHolder(ViewGroup parent, @LayoutRes int layout, Class<HOLDER> holderClass,
                         MessagesListStyle style, Object payload) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        try {
            Constructor<HOLDER> constructor = null;
            HOLDER holder;
            try {
                constructor = holderClass.getDeclaredConstructor(View.class, Object.class);
                constructor.setAccessible(true);
                holder = constructor.newInstance(v, payload);
            } catch (NoSuchMethodException e) {
                constructor = holderClass.getDeclaredConstructor(View.class);
                constructor.setAccessible(true);
                holder = constructor.newInstance(v);
            }
            if (holder instanceof DefaultMessageViewHolder && style != null) {
                ((DefaultMessageViewHolder) holder).applyStyle(style);
            }
            return holder;
        } catch (Exception e) {
            throw new UnsupportedOperationException("Somehow we couldn't create the ViewHolder for message. Please, report this issue on GitHub with full stacktrace in description.", e);
        }
    }

    @SuppressWarnings("unchecked")
    private short getContentViewType(IMessage message) {
        if (message instanceof MessageContentType.Image
                && ((MessageContentType.Image) message).getImageUrl() != null) {
            return VIEW_TYPE_IMAGE_MESSAGE;
        }

        // other default types will be here

        if (message instanceof MessageContentType) {
            for (int i = 0; i < customContentTypes.size(); i++) {
                ContentTypeConfig config = customContentTypes.get(i);
                if (contentChecker == null) {
                    throw new IllegalArgumentException("ContentChecker cannot be null when using custom content types!");
                }
                boolean hasContent = contentChecker.hasContentFor(message, config.type);
                if (hasContent) return config.type;
            }
        }

        return VIEW_TYPE_TEXT_MESSAGE;
    }

    /*
     * HOLDERS
     * */

    /**
     * The base class for view holders for incoming and outcoming message.
     * You can extend it to create your own holder in conjuction with custom layout or even using default layout.
     */
    public static abstract class BaseMessageViewHolder<MESSAGE extends IMessage> extends ViewHolder<MESSAGE> {

        boolean isSelected;

        /**
         * For setting custom data to ViewHolder
         */
        protected Object payload;

        /**
         * Callback for implementing images loading in message list
         */
        protected ImageLoader imageLoader;

        @Deprecated
        public BaseMessageViewHolder(View itemView) {
            super(itemView);
        }

        public BaseMessageViewHolder(View itemView, Object payload) {
            super(itemView);
            this.payload = payload;
        }

        /**
         * Returns whether is item selected
         *
         * @return weather is item selected.
         */
        public boolean isSelected() {
            return isSelected;
        }

        /**
         * Returns weather is selection mode enabled
         *
         * @return weather is selection mode enabled.
         */
        public boolean isSelectionModeEnabled() {
            return MessagesListAdapter.isSelectionModeEnabled;
        }

        /**
         * Getter for {@link #imageLoader}
         *
         * @return image loader interface.
         */
        public ImageLoader getImageLoader() {
            return imageLoader;
        }

        protected void configureLinksBehavior(final TextView text) {
            text.setLinksClickable(false);
            text.setMovementMethod(new LinkMovementMethod() {
                @Override
                public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
                    boolean result = false;
                    if (!MessagesListAdapter.isSelectionModeEnabled) {
                        result = super.onTouchEvent(widget, buffer, event);
                    }
                    itemView.onTouchEvent(event);
                    return result;
                }
            });
        }
    }

    /**
     * Default view holder implementation for incoming text message
     */
    public static class IncomingTextMessageViewHolder<MESSAGE extends IMessage>
            extends BaseIncomingMessageViewHolder<MESSAGE> {

        protected ViewGroup bubble;
        protected TextView text;
        protected TextView system_messageText_privateChat, remainingTimeTextIncoming, totalTimeTextIncoming;
        protected ImageView YoutubeUrlImageView, voiceNotePlayButton;
        protected CardView YoutubeUrlImageViewCardview;
        private ClipboardManager myClipboard;
        private ClipData myClip;
        protected FlexboxLayout GamesLayoutChat, MessageTextWithImage;
        protected RelativeLayout voiceNoteIncomingView;
        protected TextView voiceTimeIncoming;
        protected AvatarView voiceIncomingLoader;
        private SeekBar voiceIncomingSeekBar;
        private Runnable myRunnable;
        private Handler mHandler = new Handler();
        public static MediaPlayer incomingVoicePlayer;
        public static boolean resumeVoicePlayerIncoming;
        private String MyImageDownloadedPath = "/Socio/Voice Notes";

        @Deprecated
        public IncomingTextMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public IncomingTextMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(final MESSAGE message) {
            super.onBind(message);

            if (message.getUser().getName().equalsIgnoreCase("System")) {
                if (message.getText().contains("[[") && message.getText().contains("]]")) {
                    //game like view.message contain images
                    system_messageText_privateChat.setVisibility(View.GONE);
                    MessageTextWithImage.setVisibility(View.GONE);
                    bubble.setVisibility(View.GONE);
                    GamesLayoutChat.setVisibility(View.VISIBLE);
                    YoutubeUrlImageView.setVisibility(View.GONE);
                    YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    voiceNoteIncomingView.setVisibility(View.GONE);

                    GamesLayoutChat.removeAllViews();
                    GamesLayoutChat.setFlexDirection(FlexDirection.ROW);
                    String[] SplitedMessageAttay = message.getText().split(" ");
                    for (String SingleWordOfText : SplitedMessageAttay) {
                        if (SingleWordOfText.startsWith("[[")) {
                            SingleWordOfText = SingleWordOfText.replace("[[", "").replace("]]", "");
                            GamesLayoutChat.addView(MakeImageView(SingleWordOfText, itemView.getContext()));
                            GamesLayoutChat.addView(MakeTextView("", itemView.getContext(), "System"));
                        } else {
                            GamesLayoutChat.addView(MakeTextView(SingleWordOfText, itemView.getContext(), "System"));
                        }
                    }
                } else {
                    //simple system message
                    system_messageText_privateChat.setVisibility(View.VISIBLE);
                    bubble.setVisibility(View.GONE);
                    MessageTextWithImage.setVisibility(View.GONE);
                    GamesLayoutChat.setVisibility(View.GONE);
                    YoutubeUrlImageView.setVisibility(View.GONE);
                    YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    voiceNoteIncomingView.setVisibility(View.GONE);
                    system_messageText_privateChat.setTextSize(16);
                    system_messageText_privateChat.setText(message.getText());
                    if (message.getText().startsWith("**")) {
                        system_messageText_privateChat.setTypeface(null, Typeface.BOLD);
                    }
                }
            } else if (message.getText().contains("www.youtube.com") || message.getText().contains("https://youtu.be/")) {
                String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(message.getText());
                system_messageText_privateChat.setVisibility(View.GONE);
                MessageTextWithImage.setVisibility(View.GONE);
                bubble.setVisibility(View.GONE);
                voiceNoteIncomingView.setVisibility(View.GONE);
                GamesLayoutChat.setVisibility(View.GONE);
                YoutubeUrlImageViewCardview.setVisibility(View.VISIBLE);
                YoutubeUrlImageView.setVisibility(View.VISIBLE);
                Glide.with(itemView.getContext()).load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/maxresdefault.jpg").into(YoutubeUrlImageView);

            } else if (message.getText().contains("MyVoice_Note")) {
                //Voice Note layout...

                system_messageText_privateChat.setVisibility(View.GONE);
                MessageTextWithImage.setVisibility(View.GONE);
                bubble.setVisibility(View.GONE);
                voiceNoteIncomingView.setBackgroundResource(R.drawable.shape_incoming_voice_notes);
                voiceNoteIncomingView.setVisibility(View.VISIBLE);
                GamesLayoutChat.setVisibility(View.GONE);
                YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                YoutubeUrlImageView.setVisibility(View.GONE);
                voiceTimeIncoming.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));

                voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
            } else {
                system_messageText_privateChat.setVisibility(View.GONE);
                if (message.getText().contains("[[") && message.getText().contains("]]")) {
                    //Message contain images

                    YoutubeUrlImageView.setVisibility(View.GONE);
                    GamesLayoutChat.setVisibility(View.GONE);
                    MessageTextWithImage.setVisibility(View.VISIBLE);
                    YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    text.setVisibility(View.GONE);
                    voiceNoteIncomingView.setVisibility(View.GONE);

                    if (bubble != null) {
                        bubble.setSelected(isSelected());
                    }

                    MessageTextWithImage.removeAllViews();
                    MessageTextWithImage.setFlexDirection(FlexDirection.ROW);
                    String[] SplitedMessageAttay = message.getText().split(" ");
                    for (String SingleWordOfText : SplitedMessageAttay) {
                        if (SingleWordOfText.startsWith("[[")) {
                            SingleWordOfText = SingleWordOfText.replace("[[", "").replace("]]", "");
                            MessageTextWithImage.addView(MakeImageView(SingleWordOfText, itemView.getContext()));
                            MessageTextWithImage.addView(MakeTextView("", itemView.getContext(), "BotMessage"));
                        } else {
                            MessageTextWithImage.addView(MakeTextView(SingleWordOfText, itemView.getContext(), "BotMessage"));
                        }
                    }

                } else {
                    YoutubeUrlImageView.setVisibility(View.GONE);
                    GamesLayoutChat.setVisibility(View.GONE);
                    MessageTextWithImage.setVisibility(View.GONE);
                    YoutubeUrlImageViewCardview.setVisibility(View.GONE);
                    text.setVisibility(View.VISIBLE);
                    voiceNoteIncomingView.setVisibility(View.GONE);
                    if (bubble != null) {
                        bubble.setSelected(isSelected());
                    }

                    if (text != null) {

                        text.setText(message.getText());
                    }
                }
            }
            YoutubeUrlImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(message.getText())));
                    } catch (Exception ignore) {
                    }
                }
            });
            bubble.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    String textString;
                    myClipboard = (ClipboardManager) view.getContext().getSystemService(CLIPBOARD_SERVICE);
                    textString = message.getText();

                    myClip = ClipData.newPlainText("text", textString);
                    myClipboard.setPrimaryClip(myClip);

                    Toast.makeText(view.getContext(), "Text Copied",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            voiceNotePlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CheckPermissions()) {
                        if (outcomingVoicePlayer != null) {
                            outcomingVoicePlayer.stop();
                            outcomingVoicePlayer.release();
                            outcomingVoicePlayer = null;
                            resumeVoicePlayerOutcoming = false;
                            Utils.staticSeekBar.setProgress(0);
                            Utils.staticItemView.setImageResource(R.drawable.play_voicenote);
                        }
                        File sdCardDirectory = new File(Environment.getExternalStorageDirectory(), MyImageDownloadedPath);

                        if (!sdCardDirectory.exists()) {
                            sdCardDirectory.mkdirs();
                        }
                        new LongOperation().execute(message.getText().split("\\|")[1], message.getId());
                    } else {
                        requestPermissions();
                    }

                }
            });
        }

        private boolean CheckPermissions() {
            int recordingAudioPermissionResult = ContextCompat.checkSelfPermission(itemView.getContext(), RECORD_AUDIO);
            int savingAudioPermissionResult = ContextCompat.checkSelfPermission(itemView.getContext(), WRITE_EXTERNAL_STORAGE);
            return recordingAudioPermissionResult == PackageManager.PERMISSION_GRANTED &&
                    savingAudioPermissionResult == PackageManager.PERMISSION_GRANTED;
        }

        private void requestPermissions() {
            ActivityCompat.requestPermissions(((Activity) itemView.getContext()), new String[]{
                    RECORD_AUDIO,
                    WRITE_EXTERNAL_STORAGE,
            }, 1);
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case 1:
                    if (grantResults.length > 0) {

                        boolean record_aud = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean store_aud = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                        if (record_aud && store_aud) {

                            Toast.makeText(itemView.getContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(itemView.getContext(), "Permission Denied", Toast.LENGTH_LONG).show();

                        }
                    }
                    break;
            }
        }


        private String createTimerLabel(int duration) {
            String timerLabel = "";
            int min = duration / 1000 / 60;
            int sec = duration / 1000 % 60;
            timerLabel += min + ":";
            if (sec < 10) {
                timerLabel += "0";
            }
            timerLabel += sec;
            return timerLabel;
        }

        private class LongOperation extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... fileToBeDownloaded) {

                Log.e("NewLog", "incoming side");
                String[] mySplit = fileToBeDownloaded[0].split("/");
                File destinationofDownlodedFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + mySplit[mySplit.length - 1]);
                Log.e("dwnldng",fileToBeDownloaded[0]);
                DownloadingFile(fileToBeDownloaded[0], destinationofDownlodedFile, fileToBeDownloaded[1], mySplit[mySplit.length - 1]);
                return null;
            }
        }

        private void DownloadingFile(String DownlodingFileUrlurl, File destinationOfOutputFile, String voiceID, String downloadedFileName) {
            Log.e("dwnldng","DownlodingFileUrlurl "+DownlodingFileUrlurl);
            Log.e("dwnldng","destinationOfOutputFile "+destinationOfOutputFile);
            Log.e("dwnldng","downloadedFileName "+downloadedFileName);
            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + downloadedFileName;
            File file = new File(filePath);
            if (!file.exists()) {

                ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        voiceIncomingLoader.setVisibility(View.VISIBLE);
                        voiceIncomingLoader.setAnimating(true);
                    }
                });

                try {

                    URL u = new URL(baseUrlChatKit + DownlodingFileUrlurl);
                    URLConnection conn = u.openConnection();
                    int contentLength = conn.getContentLength();

                    DataInputStream stream = new DataInputStream(u.openStream());

                    byte[] buffer = new byte[contentLength];
                    stream.readFully(buffer);
                    stream.close();

                    DataOutputStream fos = new DataOutputStream(new FileOutputStream(destinationOfOutputFile));
                    fos.write(buffer);
                    fos.flush();
                    fos.close();
                    //yahan pe file name change ho ga, user voice ka nam ay ga wo rkhna
                    playingAudio(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + downloadedFileName, voiceID);
                } catch (FileNotFoundException e) {
                    return; // swallow a 404
                } catch (IOException e) {
                    return; // swallow a 404
                }
            } else {
                playingAudio(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + downloadedFileName, voiceID);
            }
        }

        private void playingAudio(final String voiceUri, final String voiceID) {
            ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    voiceIncomingLoader.setVisibility(View.GONE);
                }
            });
            if (incomingVoicePlayer != null && incomingVoicePlayer.isPlaying() ||
                    incomingVoicePlayer != null && resumeVoicePlayerIncoming) {

                //agar media pehly sy chal raha hy koi bhi.

                ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (voiceID.equals(Utils.previousVoiceID)) {
                            Log.e("PlayPau", "Same Voice...");


                            if (incomingVoicePlayer != null && incomingVoicePlayer.isPlaying()) {
                                voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
                                incomingVoicePlayer.pause();
                                resumeVoicePlayerIncoming = true;
                                Log.e("PlayPau", "same voice Paused");

                            } else {
                                voiceNotePlayButton.setImageResource(R.drawable.pause_voicenote);
                                incomingVoicePlayer.start();
                                resumeVoicePlayerIncoming = false;
                                Log.e("PlayPau", " same voice Resume");
                            }

                        } else {
                            Log.e("PlayPau", "Some other voice ");
                            Utils.staticSeekBar.setProgress(0);
                            Utils.staticSeekBar.removeCallbacks(Utils.staticRunnable);
                            Utils.staticItemView.setImageResource(R.drawable.play_voicenote);
                            incomingVoicePlayer.stop();
                            incomingVoicePlayer.release();
                            incomingVoicePlayer = null;
                            voiceIncomingSeekBar.setProgress(0);
                            Utils.staticSeekBar = voiceIncomingSeekBar;


                            voiceNotePlayButton.setImageResource(R.drawable.pause_voicenote);
                            incomingVoicePlayer = new MediaPlayer();
                            Utils.previousVoiceID = voiceID;
                            resumeVoicePlayerIncoming = false;
                            Utils.staticMediaPlayer = incomingVoicePlayer;
                            Utils.staticItemView = voiceNotePlayButton;
                            try {
                                incomingVoicePlayer.setDataSource(voiceUri);
                                incomingVoicePlayer.prepare();
                                incomingVoicePlayer.start();
                                updateProgressIncoming();
                                playingSeekBarImcoming();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            incomingVoicePlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    incomingVoicePlayer.stop();
                                    incomingVoicePlayer.release();
                                    incomingVoicePlayer = null;
                                    voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
                                    resumeVoicePlayerIncoming = false;
                                    voiceIncomingSeekBar.setProgress(0);

                                }
                            });
                        }
                    }
                });
            } else {
                ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("PlayPau", "newly media ya first time");
                        //first time ya newly media chalany k lye.
                        voiceNotePlayButton.setImageResource(R.drawable.pause_voicenote);
                        incomingVoicePlayer = new MediaPlayer();
                        Utils.previousVoiceID = voiceID;
                        Log.e("PlayPau", Utils.previousVoiceID + "<>" + voiceID);
                        resumeVoicePlayerIncoming = false;
                        Utils.staticMediaPlayer = incomingVoicePlayer;
                        Utils.staticSeekBar = voiceIncomingSeekBar;
                        Utils.staticItemView = voiceNotePlayButton;
                        try {
                            incomingVoicePlayer.setDataSource(voiceUri);
                            incomingVoicePlayer.prepare();
                            incomingVoicePlayer.start();
                            updateProgressIncoming();
                            playingSeekBarImcoming();
                            totalTimeTextIncoming.setText(" / " + createTimerLabel(incomingVoicePlayer.getDuration()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        incomingVoicePlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                incomingVoicePlayer.stop();
                                incomingVoicePlayer.release();
                                incomingVoicePlayer = null;
                                voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
                                resumeVoicePlayerIncoming = false;
                                voiceIncomingSeekBar.setProgress(0);
                                // previousVoiceID = null;
//                                voiceSeekBar = itemView.findViewById(R.id.voiceSeekBar);

                            }
                        });
                    }
                });
            }

        }

        SeekBarUpdater seekBarUpdaterIncoming;

        private class SeekBarUpdater implements Runnable {

            @Override
            public void run() {

                if (incomingVoicePlayer != null) {

                    if (incomingVoicePlayer.getDuration() == incomingVoicePlayer.getCurrentPosition()) {
                        voiceIncomingSeekBar.removeCallbacks(seekBarUpdaterIncoming);
                    } else {
                        voiceIncomingSeekBar.setMax(incomingVoicePlayer.getDuration());
                        voiceIncomingSeekBar.setProgress(incomingVoicePlayer.getCurrentPosition());
                        remainingTimeTextIncoming.setText(createTimerLabel(incomingVoicePlayer.getCurrentPosition()));
                        voiceIncomingSeekBar.postDelayed(seekBarUpdaterIncoming, 100);
                    }
                }
            }
        }

        private void updateProgressIncoming() {
            Log.e("aasdsa", "krna hy...");
            seekBarUpdaterIncoming = null;
            seekBarUpdaterIncoming = new SeekBarUpdater();
            Utils.staticRunnable = seekBarUpdaterIncoming;
            voiceIncomingSeekBar.setMax(incomingVoicePlayer.getDuration());
            voiceIncomingSeekBar.post(seekBarUpdaterIncoming);

        }

        private void playingSeekBarImcoming() {

            // voiceSeekBar.setMax(totalVoiceTime);
            voiceIncomingSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        incomingVoicePlayer.seekTo(progress);
                        voiceIncomingSeekBar.setProgress(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });


//            voiceSeekBar.setMax(voicePlayer.getDuration()/100);
//            myRunnable=new Runnable() {
//                @Override
//                public void run() {
//                    if (voicePlayer!=null)
//                    {
//                        int currentPosition=voicePlayer.getCurrentPosition()/100;
//                        voiceSeekBar.setProgress(currentPosition);
//                    }
//                    myHandler.postDelayed(myRunnable,100);
//                }
//            };
//            myHandler.postDelayed(myRunnable,100);

//            voiceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                @Override
//                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                    if (voicePlayer!=null) {
//                        voicePlayer.seekTo(progress * 100);
//                    }
//                    Log.e("Seekinm","seek1");
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {
//                    Log.e("Seekinm","seek2");
//
//                }
//
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//                    Log.e("Seekinm","seek3");
//
//                    if (voicePlayer != null && voicePlayer.isPlaying()) {
//                        voicePlayer.seekTo(seekBar.getProgress());
//                    }
//                }
//            });

        }

        private TextView MakeTextView(String SingleWord, Context context, String Type) {

            TextView MyTextView = new TextView(context);
            RelativeLayout.LayoutParams LayoutParmsTextview = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            LayoutParmsTextview.setMargins(0, 0, 0, 0);
            MyTextView.setLayoutParams(LayoutParmsTextview);
            MyTextView.setText(SingleWord + " ");
            if (Type.equals("System")) {
                MyTextView.setTextSize(16);
            } else {

                MyTextView.setTextColor(context.getResources().getColor(R.color.dark_grey_two));
                MyTextView.setTextSize(17);
            }
            return MyTextView;
        }

        private ImageView MakeImageView(String SingleWord, Context context) {


            ImageView MyImageView = new ImageView(context);
            RelativeLayout.LayoutParams LayoutParmsImageview = new RelativeLayout.LayoutParams(
                    dp2ToPx(18, context), dp2ToPx(16, context));
            LayoutParmsImageview.setMargins(0, 0, 0, 0);
            MyImageView.setLayoutParams(LayoutParmsImageview);
            //  MyImageView.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
//            Picasso.get().load(baseUrlChatKit + SingleWord).into(MyImageView);
            Glide.with(context).load(baseUrlChatKit + SingleWord).into(MyImageView);
            return MyImageView;
        }

        private String GettingVideoIdFromYoutubeLink(String url) {
            if (url.contains("v=")) {
                if (url.contains("&")) {
                    String[] u = url.split("v=");
                    u = u[1].split("&");
                    url = u[0];
                } else {
                    String[] u = url.split("v=");
                    url = u[1];
                }
            } else {
                String[] u = url.split("be/");
                url = u[1];
            }
            return url;
        }

        @Override
        public void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (bubble != null) {
                bubble.setPadding(style.getIncomingDefaultBubblePaddingLeft(),
                        style.getIncomingDefaultBubblePaddingTop(),
                        style.getIncomingDefaultBubblePaddingRight(),
                        style.getIncomingDefaultBubblePaddingBottom());
                ViewCompat.setBackground(bubble, style.getIncomingBubbleDrawable());
            }

            if (text != null) {
                text.setTextColor(style.getIncomingTextColor());
                text.setTextSize(17);
                text.setTypeface(text.getTypeface(), style.getIncomingTextStyle());
                text.setAutoLinkMask(style.getTextAutoLinkMask());
                text.setLinkTextColor(style.getIncomingTextLinkColor());
                configureLinksBehavior(text);
            }
        }

        private void init(View itemView) {
            bubble = (ViewGroup) itemView.findViewById(R.id.bubble);
            text = (TextView) itemView.findViewById(R.id.messageText);
            system_messageText_privateChat = (TextView) itemView.findViewById(R.id.system_messageText_privateChat);
            YoutubeUrlImageView = (ImageView) itemView.findViewById(R.id.YoutubeUrlImageView);

            YoutubeUrlImageViewCardview = itemView.findViewById(R.id.YoutubeUrlImageViewCardview);
            GamesLayoutChat = itemView.findViewById(R.id.GamesLayoutChat);
            MessageTextWithImage = itemView.findViewById(R.id.MessageTextWithImage);
            voiceNotePlayButton = itemView.findViewById(R.id.voiceNotePlayButton);
            voiceNoteIncomingView = itemView.findViewById(R.id.voiceNoteIncomingView);
            voiceTimeIncoming = itemView.findViewById(R.id.voiceTimeIncoming);
            voiceIncomingLoader = itemView.findViewById(R.id.voiceIncomingLoader);
            voiceIncomingSeekBar = itemView.findViewById(R.id.voiceIncomingSeekBar);
            remainingTimeTextIncoming = itemView.findViewById(R.id.remainingTimeTextIncoming);
            totalTimeTextIncoming = itemView.findViewById(R.id.totalTimeTextIncoming);
        }

        private int dp2ToPx(int dp, Context context) {
            float density = context.getResources()
                    .getDisplayMetrics()
                    .density;
            return Math.round((float) dp * density);
        }
    }

    /**
     * Default view holder implementation for outcoming text message
     */
    public static class OutcomingTextMessageViewHolder<MESSAGE extends IMessage>
            extends BaseOutcomingMessageViewHolder<MESSAGE> {

        protected ViewGroup bubble;
        protected TextView text;
        protected TextView system_messageText_privateChat;
        protected ImageView YoutubeUrlImageViewinComing, voiceNotePlayButton;
        protected CardView YoutubeUrlImageViewCardviewinComing;
        private ClipboardManager myClipboard;
        private ClipData myClip;
        private FlexboxLayout MessageTextWithImageOutcoming;
        private RelativeLayout voiceNoteView;
        private SeekBar voiceSeekBar;
        public static MediaPlayer outcomingVoicePlayer;
        private TextView voiceTime;
        public static boolean resumeVoicePlayerOutcoming = false;
        private AvatarView voiceOutcomingLoader;
        private TextView totalTimeText, remainingTimeText;
        private String MyImageDownloadedPath = "/Socio/Voice Notes";


        @Deprecated
        public OutcomingTextMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public OutcomingTextMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(final MESSAGE message) {
            super.onBind(message);

            if (bubble != null) {
                bubble.setSelected(isSelected());
            }


            if (text != null) {
                if (message.getText().contains("www.youtube.com") || message.getText().contains("https://youtu.be/")) {
                    String VideoIdOfYoutubeLink = GettingVideoIdFromYoutubeLink(message.getText());
                    bubble.setVisibility(View.GONE);
                    MessageTextWithImageOutcoming.setVisibility(View.GONE);
                    voiceNoteView.setVisibility(View.GONE);
                    voiceSeekBar.setVisibility(View.GONE);
                    YoutubeUrlImageViewinComing.setVisibility(View.VISIBLE);
                    YoutubeUrlImageViewCardviewinComing.setVisibility(View.VISIBLE);
                    Glide.with(itemView.getContext()).load("http://img.youtube.com/vi/" + VideoIdOfYoutubeLink + "/maxresdefault.jpg").into(YoutubeUrlImageViewinComing);

                } else if (message.getText().contains("MyVoice_Note")) {
                    //voice msg layout...
                    voiceNoteView.setVisibility(View.VISIBLE);
                    voiceSeekBar.setVisibility(View.VISIBLE);
                    voiceNoteView.setBackgroundResource(R.drawable.shape_outcoming_voicenote);
                    bubble.setVisibility(View.GONE);
                    MessageTextWithImageOutcoming.setVisibility(View.GONE);
                    YoutubeUrlImageViewinComing.setVisibility(View.GONE);
                    YoutubeUrlImageViewCardviewinComing.setVisibility(View.GONE);
                    text.setVisibility(View.GONE);
                    voiceTime.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));

                    voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
                } else {
                    if (message.getText().contains("[[") && message.getText().contains("]]")) {
                        // message with images
                        YoutubeUrlImageViewinComing.setVisibility(View.GONE);
                        YoutubeUrlImageViewCardviewinComing.setVisibility(View.GONE);
                        text.setVisibility(View.GONE);
                        voiceSeekBar.setVisibility(View.GONE);
                        voiceNoteView.setVisibility(View.GONE);
                        MessageTextWithImageOutcoming.setVisibility(View.VISIBLE);

                        MessageTextWithImageOutcoming.removeAllViews();
                        MessageTextWithImageOutcoming.setFlexDirection(FlexDirection.ROW);
                        String[] SplitedMessageAttay = message.getText().split(" ");
                        for (String SingleWordOfText : SplitedMessageAttay) {
                            if (SingleWordOfText.startsWith("[[")) {
                                SingleWordOfText = SingleWordOfText.replace("[[", "").replace("]]", "");
                                MessageTextWithImageOutcoming.addView(MakeImageView(SingleWordOfText, itemView.getContext()));
                                MessageTextWithImageOutcoming.addView(MakeTextView("", itemView.getContext(), "BotMessage"));
                            } else {
                                MessageTextWithImageOutcoming.addView(MakeTextView(SingleWordOfText, itemView.getContext(), "BotMessage"));
                            }
                        }

                    } else {
                        //simple message
                        text.setVisibility(View.VISIBLE);
                        text.setText(message.getText());
                        YoutubeUrlImageViewinComing.setVisibility(View.GONE);
                        YoutubeUrlImageViewCardviewinComing.setVisibility(View.GONE);
                        MessageTextWithImageOutcoming.setVisibility(View.GONE);
                        voiceSeekBar.setVisibility(View.GONE);
                        voiceNoteView.setVisibility(View.GONE);
                    }
                }
            }
            YoutubeUrlImageViewinComing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(message.getText())));
                    } catch (Exception ignore) {
                    }
                }
            });
            bubble.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    String textString;
                    myClipboard = (ClipboardManager) view.getContext().getSystemService(CLIPBOARD_SERVICE);
                    textString = message.getText();

                    myClip = ClipData.newPlainText("text", textString);
                    myClipboard.setPrimaryClip(myClip);

                    Toast.makeText(view.getContext(), "Text Copied",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
            });
            voiceNotePlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CheckPermissions()) {
                        if (incomingVoicePlayer != null) {
                            incomingVoicePlayer.stop();
                            incomingVoicePlayer.release();
                            incomingVoicePlayer = null;
                            resumeVoicePlayerOutcoming = false;
                            Utils.staticSeekBar.setProgress(0);
                            Utils.staticItemView.setImageResource(R.drawable.play_voicenote);
                        }
                        File sdCardDirectory = new File(Environment.getExternalStorageDirectory(), MyImageDownloadedPath);

                        if (!sdCardDirectory.exists()) {
                            sdCardDirectory.mkdirs();
                        }
                        new LongOperation().execute(message.getText().split("\\|")[1], message.getId());
                    } else {
                        requestPermissions();
                    }

                }
            });
        }

        private boolean CheckPermissions() {
            int recordingAudioPermissionResult = ContextCompat.checkSelfPermission(itemView.getContext(), RECORD_AUDIO);
            int savingAudioPermissionResult = ContextCompat.checkSelfPermission(itemView.getContext(), WRITE_EXTERNAL_STORAGE);
            return recordingAudioPermissionResult == PackageManager.PERMISSION_GRANTED &&
                    savingAudioPermissionResult == PackageManager.PERMISSION_GRANTED;
        }

        private void requestPermissions() {
            ActivityCompat.requestPermissions(((Activity) itemView.getContext()), new String[]{
                    RECORD_AUDIO,
                    WRITE_EXTERNAL_STORAGE,
            }, 1);
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case 1:
                    if (grantResults.length > 0) {

                        boolean record_aud = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean store_aud = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                        if (record_aud && store_aud) {

                            Toast.makeText(itemView.getContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(itemView.getContext(), "Permission Denied", Toast.LENGTH_LONG).show();

                        }
                    }
                    break;
            }
        }


        private class LongOperation extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... fileToBeDownloaded) {
                String[] mySplit = fileToBeDownloaded[0].split("/");
                //yahan pe file name change ho ga, user voice ka nam ay ga wo rkhna
                File destinationofDownlodedFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + mySplit[mySplit.length - 1]);
                DownloadingFile(fileToBeDownloaded[0], destinationofDownlodedFile, fileToBeDownloaded[1], mySplit[mySplit.length - 1]);
                return null;
            }
        }

        private void DownloadingFile(String DownlodingFileUrlurl, File destinationOfOutputFile, String voiceID, String downloadedFileName) {

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + downloadedFileName;
            File file = new File(filePath);
            if (!file.exists()) {
                //agr pehly sy file nahi pri v hy to usy dwnlod krana hy or phr play krana hy
                // yahan sy..
                try {
                    ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            voiceOutcomingLoader.setVisibility(View.VISIBLE);
                            voiceOutcomingLoader.setAnimating(true);
                        }
                    });

                    Log.e("PlayDownld", "In Downloading File Method");
                    URL u = new URL(baseUrlChatKit + DownlodingFileUrlurl);
                    Log.e("Finasl", "File not exist so download it from Url> " + baseUrlChatKit + DownlodingFileUrlurl);
                    URLConnection conn = u.openConnection();
                    int contentLength = conn.getContentLength();

                    DataInputStream stream = new DataInputStream(u.openStream());

                    byte[] buffer = new byte[contentLength];
                    stream.readFully(buffer);
                    stream.close();

                    DataOutputStream fos = new DataOutputStream(new FileOutputStream(destinationOfOutputFile));
                    fos.write(buffer);
                    fos.flush();
                    fos.close();
                    Log.e("Finasl", "complt dwnlod now playing");
                    //yahan pe file name change ho ga, user voice ka nam ay ga wo rkhna
                    playingAudio(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + downloadedFileName, voiceID);
                } catch (FileNotFoundException e) {
                    Log.e("Finasl", "exceptin " + e.getMessage());
                    return; // swallow a 404
                } catch (IOException e) {
                    Log.e("Finasl", "exceptin " + e.getMessage());
                    return; // swallow a 404
                }
                //yahan tk..

            } else {
                Log.e("Finasl", "already file exist");
//agr pehly sy file nahi pri v hy to usy dwnlod krana hy or phr play krana hy
                playingAudio(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Socio/Voice Notes/" + downloadedFileName, voiceID);
            }
        }

        private void playingAudio(final String voiceUri, final String voiceID) {

            ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    voiceOutcomingLoader.setVisibility(View.GONE);
                    Log.e("Finasl", "now playing> " + voiceUri);
                    Log.e("MyFinal", "Previous> " + Utils.previousVoiceID);
                    Log.e("MyFinal", "Newly> " + voiceID);
                    if (outcomingVoicePlayer != null && outcomingVoicePlayer.isPlaying() ||
                            outcomingVoicePlayer != null && resumeVoicePlayerOutcoming) {
                        ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //agar media pehly sy chal raha hy koi bhi.
                                Log.e("MyFinal", "Already some audio playing");
                                if (voiceID.equals(Utils.previousVoiceID)) {
                                    Log.e("MyFinal", "Same Voice...");


                                    if (outcomingVoicePlayer != null && outcomingVoicePlayer.isPlaying()) {
                                        voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
                                        outcomingVoicePlayer.pause();
                                        resumeVoicePlayerOutcoming = true;
                                        Log.e("MyFinal", "same voice Paused");

                                    } else {
                                        voiceNotePlayButton.setImageResource(R.drawable.pause_voicenote);
                                        outcomingVoicePlayer.start();
                                        resumeVoicePlayerOutcoming = false;
                                        Log.e("MyFinal", " same voice Resume");
                                    }

                                } else {
                                    Log.e("MyFinal", "Some other voice ");
                                    Utils.staticSeekBar.setProgress(0);
                                    Utils.staticSeekBar.removeCallbacks(Utils.staticRunnable);
                                    Utils.staticItemView.setImageResource(R.drawable.play_voicenote);
                                    outcomingVoicePlayer.stop();
                                    outcomingVoicePlayer.release();
                                    outcomingVoicePlayer = null;
                                    voiceSeekBar.setProgress(0);
                                    Utils.staticSeekBar = voiceSeekBar;


                                    voiceNotePlayButton.setImageResource(R.drawable.pause_voicenote);
                                    outcomingVoicePlayer = new MediaPlayer();
                                    Utils.previousVoiceID = voiceID;
                                    resumeVoicePlayerOutcoming = false;
                                    Utils.staticMediaPlayer = outcomingVoicePlayer;
                                    Utils.staticItemView = voiceNotePlayButton;
                                    try {
                                        outcomingVoicePlayer.setDataSource(voiceUri);
                                        outcomingVoicePlayer.prepare();
                                        outcomingVoicePlayer.start();
                                        updateProgress();
                                        playingSeekBar();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    outcomingVoicePlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            outcomingVoicePlayer.stop();
                                            outcomingVoicePlayer.release();
                                            outcomingVoicePlayer = null;
                                            voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
                                            resumeVoicePlayerOutcoming = false;
                                            voiceSeekBar.setProgress(0);

                                        }
                                    });

                                }
                            }
                        });
                    } else {
                        ((Activity) itemView.getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("Finasl", "in first time> " + voiceUri);
                                Log.e("MyFinal", "newly media ya first time");
                                //first time ya newly media chalany k lye.
                                voiceNotePlayButton.setImageResource(R.drawable.pause_voicenote);
                                outcomingVoicePlayer = new MediaPlayer();
                                Utils.previousVoiceID = voiceID;
                                Log.e("MyFinal", Utils.previousVoiceID + "<>" + voiceID);
                                resumeVoicePlayerOutcoming = false;
                                Utils.staticMediaPlayer = outcomingVoicePlayer;
                                Utils.staticSeekBar = voiceSeekBar;
                                Utils.staticItemView = voiceNotePlayButton;
                                try {
                                    outcomingVoicePlayer.setDataSource(voiceUri);
                                    outcomingVoicePlayer.prepare();
                                    outcomingVoicePlayer.start();
                                    updateProgress();
                                    playingSeekBar();
                                    totalTimeText.setText(" / " + createTimerLabel(outcomingVoicePlayer.getDuration()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                outcomingVoicePlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mediaPlayer) {
                                        outcomingVoicePlayer.stop();
                                        outcomingVoicePlayer.release();
                                        outcomingVoicePlayer = null;
                                        voiceNotePlayButton.setImageResource(R.drawable.play_voicenote);
                                        resumeVoicePlayerOutcoming = false;
                                        voiceSeekBar.setProgress(0);

                                    }
                                });
                            }
                        });
                    }
                }
            });

        }

        private String createTimerLabel(int duration) {
            String timerLabel = "";
            int min = duration / 1000 / 60;
            int sec = duration / 1000 % 60;
            timerLabel += min + ":";
            if (sec < 10) {
                timerLabel += "0";
            }
            timerLabel += sec;
            return timerLabel;
        }

        SeekBarUpdater seekBarUpdater;

        private class SeekBarUpdater implements Runnable {

            @Override
            public void run() {

                if (outcomingVoicePlayer != null) {

                    if (outcomingVoicePlayer.getDuration() == outcomingVoicePlayer.getCurrentPosition()) {
                        voiceSeekBar.removeCallbacks(seekBarUpdater);
                    } else {

                        voiceSeekBar.setMax(outcomingVoicePlayer.getDuration());
                        voiceSeekBar.setProgress(outcomingVoicePlayer.getCurrentPosition());
                        remainingTimeText.setText(createTimerLabel(outcomingVoicePlayer.getCurrentPosition()));
                        voiceSeekBar.postDelayed(seekBarUpdater, 100);
                    }
                }
            }
        }

        private void updateProgress() {
            seekBarUpdater = null;
            seekBarUpdater = new SeekBarUpdater();
            Utils.staticRunnable = seekBarUpdater;
            voiceSeekBar.setMax(outcomingVoicePlayer.getDuration());
            voiceSeekBar.post(seekBarUpdater);
//
//            if (voiceSeekBar != null) {
//
//                voiceSeekBar.setMax(outcomingVoicePlayer.getDuration() / 100);
//
//                Log.e("PlayPau", "seekbar max=" + outcomingVoicePlayer.getDuration() / 100);
//                myRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        if (outcomingVoicePlayer != null && voiceSeekBar != null) {
//
//                            int mCurrentPosition = outcomingVoicePlayer.getCurrentPosition() / 100; // In milliseconds
//                            voiceSeekBar.setProgress(mCurrentPosition);
//                            Log.e("PlayPau", "seekbar pos->" + mCurrentPosition);
//                            if (outcomingVoicePlayer.getDuration() == outcomingVoicePlayer.getCurrentPosition()) {
//                                voiceSeekBar.setProgress(0);
//                                Log.e("PlayPau", "Complted");
//                                voiceSeekBar = null;
//                            }
//                        } else {
//                            Log.e("PlayPau", "seekbar is null :P");
//                        }
//                        mHandler.postDelayed(myRunnable, 100);
//                    }
//                };
//                mHandler.postDelayed(myRunnable, 100);
//
//            } else {
//                Log.e("PlayPau", "seekbar is null :D");
//            }
        }

        private void playingSeekBar() {

            // voiceSeekBar.setMax(totalVoiceTime);
            voiceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        if (outcomingVoicePlayer != null) {
                            outcomingVoicePlayer.seekTo(progress);
                            voiceSeekBar.setProgress(progress);
                        }
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            //            voiceSeekBar.setMax(voicePlayer.getDuration()/100);
//            myRunnable=new Runnable() {
//                @Override
//                public void run() {
//                    if (voicePlayer!=null)
//                    {
//                        int currentPosition=voicePlayer.getCurrentPosition()/100;
//                        voiceSeekBar.setProgress(currentPosition);
//                    }
//                    myHandler.postDelayed(myRunnable,100);
//                }
//            };
//            myHandler.postDelayed(myRunnable,100);

//            voiceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                @Override
//                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                    if (voicePlayer!=null) {
//                        voicePlayer.seekTo(progress * 100);
//                    }
//                    Log.e("Seekinm","seek1");
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {
//                    Log.e("Seekinm","seek2");
//
//                }
//
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//                    Log.e("Seekinm","seek3");
//
//                    if (voicePlayer != null && voicePlayer.isPlaying()) {
//                        voicePlayer.seekTo(seekBar.getProgress());
//                    }
//                }
//            });
        }

        private TextView MakeTextView(String SingleWord, Context context, String Type) {

            TextView MyTextView = new TextView(context);
            RelativeLayout.LayoutParams LayoutParmsTextview = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            LayoutParmsTextview.setMargins(0, 0, 0, 0);
            MyTextView.setLayoutParams(LayoutParmsTextview);
            MyTextView.setText(SingleWord + " ");
            if (Type.equals("System")) {
                MyTextView.setTextSize(16);
            } else {

                MyTextView.setTextColor(context.getResources().getColor(R.color.dark_grey_two));
                MyTextView.setTextSize(17);
            }
            return MyTextView;
        }

        private ImageView MakeImageView(String SingleWord, Context context) {

            ImageView MyImageView = new ImageView(context);
            RelativeLayout.LayoutParams LayoutParmsImageview = new RelativeLayout.LayoutParams(
                    dp2ToPx(18, context), dp2ToPx(16, context));
            LayoutParmsImageview.setMargins(0, 0, 0, 0);
            MyImageView.setLayoutParams(LayoutParmsImageview);
            Glide.with(context).load(baseUrlChatKit + SingleWord).into(MyImageView);
            return MyImageView;
        }

        private int dp2ToPx(int dp, Context context) {
            float density = context.getResources()
                    .getDisplayMetrics()
                    .density;
            return Math.round((float) dp * density);
        }

        private String GettingVideoIdFromYoutubeLink(String url) {
            //yahan change krna yutube url
            if (url.contains("v=")) {
                if (url.contains("&")) {
                    String[] u = url.split("v=");
                    u = u[1].split("&");
                    url = u[0];
                } else {
                    String[] u = url.split("v=");
                    url = u[1];
                }
            } else {
                String[] u = url.split("be/");
                url = u[1];
            }
            return url;

        }

        @Override
        public final void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (bubble != null) {
                bubble.setPadding(style.getOutcomingDefaultBubblePaddingLeft(),
                        style.getOutcomingDefaultBubblePaddingTop(),
                        style.getOutcomingDefaultBubblePaddingRight(),
                        style.getOutcomingDefaultBubblePaddingBottom());
                ViewCompat.setBackground(bubble, style.getOutcomingBubbleDrawable());
            }

            if (text != null) {
                text.setTextColor(style.getOutcomingTextColor());
                text.setTextSize(17);
                text.setTypeface(text.getTypeface(), style.getOutcomingTextStyle());
                text.setAutoLinkMask(style.getTextAutoLinkMask());
                text.setLinkTextColor(style.getOutcomingTextLinkColor());
                configureLinksBehavior(text);
            }
        }

        private void init(View itemView) {
            bubble = (ViewGroup) itemView.findViewById(R.id.bubble);
            text = (TextView) itemView.findViewById(R.id.messageText);
            system_messageText_privateChat = (TextView) itemView.findViewById(R.id.system_messageText_privateChat);
            YoutubeUrlImageViewinComing = itemView.findViewById(R.id.YoutubeUrlImageViewinComing);
            YoutubeUrlImageViewCardviewinComing = itemView.findViewById(R.id.YoutubeUrlImageViewCardviewinComing);
            MessageTextWithImageOutcoming = itemView.findViewById(R.id.MessageTextWithImageOutcoming);
            voiceNoteView = itemView.findViewById(R.id.voiceNoteView);
            voiceNotePlayButton = itemView.findViewById(R.id.voiceNotePlayButton);
            voiceSeekBar = itemView.findViewById(R.id.voiceSeekBar);
            voiceTime = itemView.findViewById(R.id.voiceTime);
            voiceOutcomingLoader = itemView.findViewById(R.id.voiceOutcomingLoader);
            totalTimeText = itemView.findViewById(R.id.totalTimeText);
            remainingTimeText = itemView.findViewById(R.id.remainingTimeText);
        }
    }

    /**
     * Default view holder implementation for incoming image message
     */
    public static class IncomingImageMessageViewHolder<MESSAGE extends MessageContentType.Image>
            extends BaseIncomingMessageViewHolder<MESSAGE> {

        protected ImageView image;
        protected View imageOverlay;

        @Deprecated
        public IncomingImageMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public IncomingImageMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(final MESSAGE message) {
            super.onBind(message);
            if (image != null && imageLoader != null) {
                imageLoader.loadImage(image, message.getImageUrl(), getPayloadForImageLoader(message));
            }

            if (imageOverlay != null) {
                imageOverlay.setSelected(isSelected());
            }
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent =new Intent(view.getContext(), FullScreenImageView.class);
                    intent.putExtra("photoUrl",message.getImageUrl());
                    intent.putExtra("folderType","receivingSide");
                    view.getContext().startActivity(intent);
                }
            });
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case 1:
                    if (grantResults.length > 0) {

                        boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                        if (write_str && read_str && record_str) {
                            Toast.makeText(itemView.getContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(itemView.getContext(), "Permission Denied", Toast.LENGTH_LONG).show();

                        }
                    }
                    break;
            }
        }

        @Override
        public final void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getIncomingImageTimeTextStyle());
            }

            if (imageOverlay != null) {
                ViewCompat.setBackground(imageOverlay, style.getIncomingImageOverlayDrawable());
            }
        }

        /**
         * Override this method to have ability to pass custom data in ImageLoader for loading image(not avatar).
         *
         * @param message Message with image
         */
        protected Object getPayloadForImageLoader(MESSAGE message) {
            return null;
        }

        private void init(View itemView) {
            image = (ImageView) itemView.findViewById(R.id.image);
            imageOverlay = itemView.findViewById(R.id.imageOverlay);

            if (image instanceof RoundedImageView) {
                ((RoundedImageView) image).setCorners(
                        R.dimen.message_bubble_corners_radius,
                        R.dimen.message_bubble_corners_radius,
                        R.dimen.message_bubble_corners_radius,
                        0
                );
            }
        }
    }

    /**
     * Default view holder implementation for outcoming image message
     */
    public static class OutcomingImageMessageViewHolder<MESSAGE extends MessageContentType.Image>
            extends BaseOutcomingMessageViewHolder<MESSAGE> {

        protected ImageView image;
        protected View imageOverlay;

        @Deprecated
        public OutcomingImageMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public OutcomingImageMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(final MESSAGE message) {
            super.onBind(message);
            if (image != null && imageLoader != null) {
                imageLoader.loadImage(image, message.getImageUrl(), getPayloadForImageLoader(message));
            }

            if (imageOverlay != null) {
                imageOverlay.setSelected(isSelected());
            }
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent =new Intent(view.getContext(), FullScreenImageView.class);
                    intent.putExtra("photoUrl",message.getImageUrl());
                    intent.putExtra("folderType","sendingSide");
                    view.getContext().startActivity(intent);
                }
            });
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case 1:
                    if (grantResults.length > 0) {

                        boolean write_str = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean read_str = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        boolean record_str = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                        if (write_str && read_str && record_str) {
                            Toast.makeText(itemView.getContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(itemView.getContext(), "Permission Denied", Toast.LENGTH_LONG).show();

                        }
                    }
                    break;
            }
        }

        @Override
        public final void applyStyle(MessagesListStyle style) {
            super.applyStyle(style);
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getOutcomingImageTimeTextStyle());
            }

            if (imageOverlay != null) {
                ViewCompat.setBackground(imageOverlay, style.getOutcomingImageOverlayDrawable());
            }
        }

        /**
         * Override this method to have ability to pass custom data in ImageLoader for loading image(not avatar).
         *
         * @param message Message with image
         */
        protected Object getPayloadForImageLoader(MESSAGE message) {
            return null;
        }

        private void init(View itemView) {
            image = (ImageView) itemView.findViewById(R.id.image);
            imageOverlay = itemView.findViewById(R.id.imageOverlay);

            if (image instanceof RoundedImageView) {
                ((RoundedImageView) image).setCorners(
                        R.dimen.message_bubble_corners_radius,
                        R.dimen.message_bubble_corners_radius,
                        0,
                        R.dimen.message_bubble_corners_radius
                );
            }
        }
    }

    /**
     * Default view holder implementation for date header
     */
    public static class DefaultDateHeaderViewHolder extends ViewHolder<Date>
            implements DefaultMessageViewHolder {

        protected TextView text;
        protected String dateFormat;
        protected DateFormatter.Formatter dateHeadersFormatter;

        public DefaultDateHeaderViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.messageText);
        }

        @Override
        public void onBind(Date date) {
            if (text != null) {
                String formattedDate = null;
                if (dateHeadersFormatter != null) formattedDate = dateHeadersFormatter.format(date);
                text.setText(formattedDate == null ? DateFormatter.format(date, dateFormat) : formattedDate);
            }
        }

        @Override
        public void applyStyle(MessagesListStyle style) {
            if (text != null) {
                text.setTextColor(style.getDateHeaderTextColor());
                text.setTextSize(14);
                text.setTypeface(text.getTypeface(), style.getDateHeaderTextStyle());
                text.setPadding(style.getDateHeaderPadding(), style.getDateHeaderPadding(),
                        style.getDateHeaderPadding(), style.getDateHeaderPadding());
            }
            dateFormat = style.getDateHeaderFormat();
            dateFormat = dateFormat == null ? DateFormatter.Template.STRING_DAY_MONTH_YEAR.get() : dateFormat;
        }
    }

    /**
     * Base view holder for incoming message
     */
    public abstract static class BaseIncomingMessageViewHolder<MESSAGE extends IMessage>
            extends BaseMessageViewHolder<MESSAGE> implements DefaultMessageViewHolder {

        protected TextView time;
        protected ImageView userAvatar;

        @Deprecated
        public BaseIncomingMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public BaseIncomingMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(MESSAGE message) {
            if (time != null) {
                time.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
            }

//            if (userAvatar != null) {
//                boolean isAvatarExists = imageLoader != null
//                        && message.getUser().getAvatar() != null
//                        && !message.getUser().getAvatar().isEmpty();
//
//                userAvatar.setVisibility(isAvatarExists ? View.VISIBLE : View.GONE);
//                if (isAvatarExists) {
//                    imageLoader.loadImage(userAvatar, message.getUser().getAvatar(), null);
//                }
//            }
        }

        public abstract void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);

        @Override
        public void applyStyle(MessagesListStyle style) {
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getIncomingTimeTextStyle());
            }

//            if (userAvatar != null) {
//                userAvatar.getLayoutParams().width = style.getIncomingAvatarWidth();
//                userAvatar.getLayoutParams().height = style.getIncomingAvatarHeight();
//            }

        }

        private void init(View itemView) {
            time = (TextView) itemView.findViewById(R.id.messageTime);
            //     userAvatar = (ImageView) itemView.findViewById(R.id.messageUserAvatar);
        }
    }

    /**
     * Base view holder for outcoming message
     */
    public abstract static class BaseOutcomingMessageViewHolder<MESSAGE extends IMessage>
            extends BaseMessageViewHolder<MESSAGE> implements DefaultMessageViewHolder {

        protected TextView time;

        @Deprecated
        public BaseOutcomingMessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);
        }

        public BaseOutcomingMessageViewHolder(View itemView, Object payload) {
            super(itemView, payload);
            init(itemView);
        }

        @Override
        public void onBind(MESSAGE message) {
            if (time != null) {
                time.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
            }
        }

        public abstract void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);

        @Override
        public void applyStyle(MessagesListStyle style) {
            if (time != null) {
                time.setTextColor(style.getIncomingImageTimeTextColor());
                time.setTextSize(12);
                time.setTypeface(time.getTypeface(), style.getOutcomingTimeTextStyle());
            }
        }

        private void init(View itemView) {
            time = (TextView) itemView.findViewById(R.id.messageTime);
        }
    }

    /*
     * DEFAULTS
     * */

    interface DefaultMessageViewHolder {
        void applyStyle(MessagesListStyle style);
    }

    private static class DefaultIncomingTextMessageViewHolder
            extends IncomingTextMessageViewHolder<IMessage> {

        public DefaultIncomingTextMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class DefaultOutcomingTextMessageViewHolder
            extends OutcomingTextMessageViewHolder<IMessage> {

        public DefaultOutcomingTextMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class DefaultIncomingImageMessageViewHolder
            extends IncomingImageMessageViewHolder<MessageContentType.Image> {

        public DefaultIncomingImageMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class DefaultOutcomingImageMessageViewHolder
            extends OutcomingImageMessageViewHolder<MessageContentType.Image> {

        public DefaultOutcomingImageMessageViewHolder(View itemView) {
            super(itemView, null);
        }
    }

    private static class ContentTypeConfig<TYPE extends MessageContentType> {

        private byte type;
        private HolderConfig<TYPE> incomingConfig;
        private HolderConfig<TYPE> outcomingConfig;

        private ContentTypeConfig(
                byte type, HolderConfig<TYPE> incomingConfig, HolderConfig<TYPE> outcomingConfig) {

            this.type = type;
            this.incomingConfig = incomingConfig;
            this.outcomingConfig = outcomingConfig;
        }
    }

    private class HolderConfig<T extends IMessage> {

        protected Class<? extends BaseMessageViewHolder<? extends T>> holder;
        protected int layout;
        protected Object payload;

        HolderConfig(Class<? extends BaseMessageViewHolder<? extends T>> holder, int layout) {
            this.holder = holder;
            this.layout = layout;
        }

        HolderConfig(Class<? extends BaseMessageViewHolder<? extends T>> holder, int layout, Object payload) {
            this.holder = holder;
            this.layout = layout;
            this.payload = payload;
        }
    }
}
