package com.stfalcon.chatkit.utils;

import android.media.MediaPlayer;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

public class Utils {

    public static MediaPlayer staticMediaPlayer = null;
    public static ImageView staticItemView = null;
    public static SeekBar staticSeekBar = null;
    public static Runnable staticRunnable = null;
    public static String previousVoiceID;
}
